# KiraMed Web

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.5.4.
And this requires the NodeJs to be installed. You can download the latest version of NodeJs [Here](https://nodejs.org/en/)

## Quick Installation Instructions

* Install cli globally (version should match the one in package.json)

`npm install -g @angular/cli@1.5.4`

* Install npm packages

`npm install`

* Run the application using npm start (this command will automatically open the app in a new window on http://localhost:4210/)

`npm start`
