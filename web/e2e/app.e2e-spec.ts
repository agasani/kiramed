import { KiraPage } from './app.po';

describe('login App', () => {
  let page: KiraPage;

  beforeEach(() => {
    page = new KiraPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
