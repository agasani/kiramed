export function arrayContainsArray(superset: string[], subset: string[] | string): boolean {
  if (typeof subset == 'string') subset = [subset];
  return subset.every((value: any) => superset.indexOf(value) >= 0);
}
