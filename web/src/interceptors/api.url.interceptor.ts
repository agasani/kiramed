import { Injectable, Injector } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { AuthConfiguration, AuthService } from '../modules/shared/services';
import { ApiConfiguration } from '../modules/api/api-configuration';

@Injectable()
export class ApiUrlInterceptor implements HttpInterceptor {

  private apiBaseUrl: ApiConfiguration;
  private authConfig: AuthConfiguration;
  private auth: AuthService;

  constructor(private injector: Injector) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    this.auth = this.injector.get(AuthService);
    this.authConfig = this.injector.get(AuthConfiguration);
    this.apiBaseUrl = this.injector.get(ApiConfiguration);

    req = req.clone({ url: this.prepareUrl(req.url) });

    return next.handle(req);
  }

  private prepareUrl(url: string): string {

    const mainUrl = url === this.auth.baseUrl ? this.authConfig.Authority : this.apiBaseUrl.rootUrl;

    url = this.isAbsoluteUrl(url) ? url : mainUrl + url;

    return url.replace(/([^:]\/)\/+/g, '$1');
  }

  private isAbsoluteUrl(url: string): boolean {

    const absolutePattern = /^https?:\/\//i;

    return absolutePattern.test(url);
  }
}
