import { Injectable, Injector } from '@angular/core';
import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpResponse
} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../environments/environment';
import { AuthService } from '../modules/shared/services';
import { LoaderService } from '../modules/shared/components/loader/loader.service';
import { SpinnerService } from '../modules/shared/components/spinner/spinner.service';

@Injectable()
export class HttpClientInterceptor implements HttpInterceptor {

  constructor(private injector: Injector) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const auth = this.injector.get(AuthService);
    const loader = this.injector.get(LoaderService);
    const spinner = this.injector.get(SpinnerService);
    const isRequestMethodGet = req.method == 'GET';
    const defaultHeaders = { 'Accept': 'application/json' };

    req = !req.headers.has('Content-Type') && req.clone({
      headers: req.headers.set('Content-Type', 'application/json; charset=utf-8')
    });

    req = req.clone({ setHeaders: defaultHeaders });

    isRequestMethodGet ? loader.show() : spinner.show();

    return next.handle(req)
        .do((event: HttpEvent<any>) => {

          if (event instanceof HttpResponse)
            isRequestMethodGet ? loader.show() : spinner.hide();

        }, (err: any) => {

          if (err instanceof HttpErrorResponse) {
            isRequestMethodGet ? loader.hide() : spinner.hide();

            if (err.status === 401) {
              console.log(err);
              auth.collectFailedRequest(req);
              auth.refreshToken().subscribe(resp => {
                if (!resp) console.log('Invalid');
                else auth.retryFailedRequests();
              });
            }
            // else if (err.status === 404) return router.navigate(['not-found']);
          }
        })
        .catch((err: HttpErrorResponse | any, caught: Observable<any>) => {
          isRequestMethodGet ? loader.hide() : spinner.hide();

          let errorMessage: string = '';

          if (err.hasOwnProperty('status')) {
            if (environment.httpErrors.hasOwnProperty(err.status)) errorMessage = environment.httpErrors[err.status].msg;
            else {
              errorMessage = `Error status: ${err.status}`;
              if (err.hasOwnProperty('message')) errorMessage += err.message;
            }
          }
          if (errorMessage === '') {
            if (err.hasOwnProperty('error') && err.error.hasOwnProperty('message')) errorMessage = `Error: ${err.error.message}`;
            else errorMessage = environment.httpErrors[0].msg;
          }
          // https://diw112.github.io/semanticUiAlert/
          console.error(errorMessage);
          return Observable.throw(errorMessage);
        });
  }
}