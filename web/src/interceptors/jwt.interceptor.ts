import 'rxjs/add/operator/do';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Injector } from '@angular/core';
import { AuthService } from '../modules/shared/services';

export class JwtInterceptor implements HttpInterceptor {

  constructor(private injector: Injector) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const auth = this.injector.get(AuthService);
    const token: string = auth.getToken();

    if (token) {

      if (req.body) req = req.clone({ body: JSON.stringify(req.body) });

      req = req.clone({ headers: req.headers.set('Authorization', `Bearer ${token}`) });

    } else if (req.url == auth.baseUrl && req.method == 'POST') {
      const headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': `Basic ${btoa(`${auth.clientId}:${auth.clientSecret}`)}`
      };

      req = req.clone({ setHeaders: headers });
    }

    return next.handle(req);
  }
}
