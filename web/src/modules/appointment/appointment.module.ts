﻿import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppointmentComponent } from './components/appointment.component';
import { SharedModule } from '../shared/shared.module';

const routes: Routes = [{ path: '', component: AppointmentComponent }];

@NgModule({ imports: [SharedModule, RouterModule.forChild(routes)], declarations: [AppointmentComponent] })

export class AppointmentModule {
}