import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { MockBackendAppointmentsService } from '../../mock-backend/services';
import { MockAppointmentService } from '../services/mock/mock-appointment.service';
import { AppointmentViewModel } from '../services/appointment.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'kira-appointment',
  templateUrl: '../views/appointment.component.html',
  styleUrls: ['../sass/appointment.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [MockAppointmentService]
})

export class AppointmentComponent implements OnInit, OnDestroy {

  public appointments: AppointmentViewModel[];
  private mySubscription: Subscription;

  constructor(private mockBackendService: MockBackendAppointmentsService,
              private appointmentService: MockAppointmentService) {
    this.mockBackendService.start();
  }

  ngOnInit() {
    this.mySubscription = this.appointmentService.getAppointments().subscribe(
        appoints => this.appointments = appoints,
        error => console.error(error),
        () => console.log('Appointments:', this.appointments)
    );
  }

  ngOnDestroy() {
    this.mySubscription.unsubscribe();
  }
}
