﻿import { Injectable } from '@angular/core';
import { AppointmentViewModel } from '../appointment.service';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class MockAppointmentService {
  private appointmentsUrl = 'http://localhost:8080/appointments';

  constructor(private http: Http) {
  }

  getAppointments(): Observable<AppointmentViewModel[]> {
    return this.http.get(this.appointmentsUrl)
        .map(response => response.json() as AppointmentViewModel[])
        .catch(this.handleError);
  }

  private handleError(error: any): Observable<any> {
    console.error('An error occured', error); // for demo purposes only
    return Observable.throw(error.message || error);
  }
}
