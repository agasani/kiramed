﻿/* tslint:disable */
//----------------------
// <auto-generated>
//     Generated using the NSwag toolchain v11.12.7.0 (NJsonSchema v9.10.6.0 (Newtonsoft.Json v9.0.0.0)) (http://NSwag.org)
// </auto-generated>
//----------------------
// ReSharper disable InconsistentNaming

import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/catch';

import { Observable } from 'rxjs/Observable';
import { Inject, Injectable, InjectionToken, Optional } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';

export const API_BASE_URL = new InjectionToken<string>('API_BASE_URL');

export interface IAppointmentClient {
  get(): Observable<KiraResponse<FileResponse | null>>;

  post(vModel?: AppointmentCreationViewModel | null): Observable<KiraResponse<FileResponse | null>>;

  put(id: number, vModel?: AppointmentViewModel | null): Observable<KiraResponse<FileResponse | null>>;

  detail(id: number): Observable<KiraResponse<FileResponse | null>>;

  getByNationalId(nationalId: string | null): Observable<KiraResponse<FileResponse | null>>;

  delete(id: number): Observable<KiraResponse<FileResponse | null>>;
}

@Injectable()
export class AppointmentClient implements IAppointmentClient {
  protected jsonParseReviver: ((key: string, value: any) => any) | undefined = undefined;
  private http: HttpClient;
  private baseUrl: string;

  constructor(@Inject(HttpClient) http: HttpClient, @Optional() @Inject(API_BASE_URL) baseUrl?: string) {
    this.http = http;
    this.baseUrl = baseUrl ? baseUrl : '';
  }

  get(): Observable<KiraResponse<FileResponse | null>> {
    let url_ = this.baseUrl + '/api/Appointment';
    url_ = url_.replace(/[?&]$/, '');

    let options_: any = {
      observe: 'response',
      responseType: 'blob',
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };

    return this.http.request('get', url_, options_).flatMap((response_: any) => {
      return this.processGet(response_);
    }).catch((response_: any) => {
      if (response_ instanceof HttpResponse) {
        try {
          return this.processGet(response_);
        } catch (e) {
          return <Observable<KiraResponse<FileResponse | null>>><any>Observable.throw(e);
        }
      } else
        return <Observable<KiraResponse<FileResponse | null>>><any>Observable.throw(response_);
    });
  }

  post(vModel?: AppointmentCreationViewModel | null): Observable<KiraResponse<FileResponse | null>> {
    let url_ = this.baseUrl + '/api/Appointment';
    url_ = url_.replace(/[?&]$/, '');

    const content_ = JSON.stringify(vModel);

    let options_: any = {
      body: content_,
      observe: 'response',
      responseType: 'blob',
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };

    return this.http.request('post', url_, options_).flatMap((response_: any) => {
      return this.processPost(response_);
    }).catch((response_: any) => {
      if (response_ instanceof HttpResponse) {
        try {
          return this.processPost(response_);
        } catch (e) {
          return <Observable<KiraResponse<FileResponse | null>>><any>Observable.throw(e);
        }
      } else
        return <Observable<KiraResponse<FileResponse | null>>><any>Observable.throw(response_);
    });
  }

  put(id: number, vModel?: AppointmentViewModel | null): Observable<KiraResponse<FileResponse | null>> {
    let url_ = this.baseUrl + '/api/Appointment?';
    if (id === undefined || id === null)
      throw new Error('The parameter \'id\' must be defined and cannot be null.');
    else
      url_ += 'id=' + encodeURIComponent('' + id) + '&';
    url_ = url_.replace(/[?&]$/, '');

    const content_ = JSON.stringify(vModel);

    let options_: any = {
      body: content_,
      observe: 'response',
      responseType: 'blob',
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };

    return this.http.request('put', url_, options_).flatMap((response_: any) => {
      return this.processPut(response_);
    }).catch((response_: any) => {
      if (response_ instanceof HttpResponse) {
        try {
          return this.processPut(response_);
        } catch (e) {
          return <Observable<KiraResponse<FileResponse | null>>><any>Observable.throw(e);
        }
      } else
        return <Observable<KiraResponse<FileResponse | null>>><any>Observable.throw(response_);
    });
  }

  detail(id: number): Observable<KiraResponse<FileResponse | null>> {
    let url_ = this.baseUrl + '/api/Appointment/Detail/{id}';
    if (id === undefined || id === null)
      throw new Error('The parameter \'id\' must be defined.');
    url_ = url_.replace('{id}', encodeURIComponent('' + id));
    url_ = url_.replace(/[?&]$/, '');

    let options_: any = {
      observe: 'response',
      responseType: 'blob',
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };

    return this.http.request('get', url_, options_).flatMap((response_: any) => {
      return this.processDetail(response_);
    }).catch((response_: any) => {
      if (response_ instanceof HttpResponse) {
        try {
          return this.processDetail(response_);
        } catch (e) {
          return <Observable<KiraResponse<FileResponse | null>>><any>Observable.throw(e);
        }
      } else
        return <Observable<KiraResponse<FileResponse | null>>><any>Observable.throw(response_);
    });
  }

  getByNationalId(nationalId: string | null): Observable<KiraResponse<FileResponse | null>> {
    let url_ = this.baseUrl + '/api/Appointment/GetByNationalId?';
    if (nationalId === undefined)
      throw new Error('The parameter \'nationalId\' must be defined.');
    else
      url_ += 'nationalId=' + encodeURIComponent('' + nationalId) + '&';
    url_ = url_.replace(/[?&]$/, '');

    let options_: any = {
      observe: 'response',
      responseType: 'blob',
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };

    return this.http.request('get', url_, options_).flatMap((response_: any) => {
      return this.processGetByNationalId(response_);
    }).catch((response_: any) => {
      if (response_ instanceof HttpResponse) {
        try {
          return this.processGetByNationalId(response_);
        } catch (e) {
          return <Observable<KiraResponse<FileResponse | null>>><any>Observable.throw(e);
        }
      } else
        return <Observable<KiraResponse<FileResponse | null>>><any>Observable.throw(response_);
    });
  }

  delete(id: number): Observable<KiraResponse<FileResponse | null>> {
    let url_ = this.baseUrl + '/api/Appointment/{id}';
    if (id === undefined || id === null)
      throw new Error('The parameter \'id\' must be defined.');
    url_ = url_.replace('{id}', encodeURIComponent('' + id));
    url_ = url_.replace(/[?&]$/, '');

    let options_: any = {
      observe: 'response',
      responseType: 'blob',
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };

    return this.http.request('delete', url_, options_).flatMap((response_: any) => {
      return this.processDelete(response_);
    }).catch((response_: any) => {
      if (response_ instanceof HttpResponse) {
        try {
          return this.processDelete(response_);
        } catch (e) {
          return <Observable<KiraResponse<FileResponse | null>>><any>Observable.throw(e);
        }
      } else
        return <Observable<KiraResponse<FileResponse | null>>><any>Observable.throw(response_);
    });
  }

  protected processGet(response: HttpResponse<Blob>): Observable<KiraResponse<FileResponse | null>> {
    const status = response.status;

    let _headers: any = {};
    if (response.headers) {
      for (let key of response.headers.keys()) {
        _headers[key] = response.headers.get(key);
      }
    }
    ;
    if (status === 200 || status === 206) {
      const contentDisposition = response.headers ? response.headers.get('content-disposition') : undefined;
      const fileNameMatch = contentDisposition ? /filename="?([^"]*?)"?(;|$)/g.exec(contentDisposition) : undefined;
      const fileName = fileNameMatch && fileNameMatch.length > 1 ? fileNameMatch[1] : undefined;
      return Observable.of(new KiraResponse(status, _headers, {
        fileName: fileName,
        data: <any>response.body,
        status: status,
        headers: _headers
      }));
    } else if (status !== 200 && status !== 204) {
      return blobToText(response.body).flatMap(_responseText => {
        return throwException('An unexpected server error occurred.', status, _responseText, _headers);
      });
    }
    return Observable.of<KiraResponse<FileResponse | null>>(new KiraResponse(status, _headers, <any>null));
  }

  protected processPost(response: HttpResponse<Blob>): Observable<KiraResponse<FileResponse | null>> {
    const status = response.status;

    let _headers: any = {};
    if (response.headers) {
      for (let key of response.headers.keys()) {
        _headers[key] = response.headers.get(key);
      }
    }
    ;
    if (status === 200 || status === 206) {
      const contentDisposition = response.headers ? response.headers.get('content-disposition') : undefined;
      const fileNameMatch = contentDisposition ? /filename="?([^"]*?)"?(;|$)/g.exec(contentDisposition) : undefined;
      const fileName = fileNameMatch && fileNameMatch.length > 1 ? fileNameMatch[1] : undefined;
      return Observable.of(new KiraResponse(status, _headers, {
        fileName: fileName,
        data: <any>response.body,
        status: status,
        headers: _headers
      }));
    } else if (status !== 200 && status !== 204) {
      return blobToText(response.body).flatMap(_responseText => {
        return throwException('An unexpected server error occurred.', status, _responseText, _headers);
      });
    }
    return Observable.of<KiraResponse<FileResponse | null>>(new KiraResponse(status, _headers, <any>null));
  }

  protected processPut(response: HttpResponse<Blob>): Observable<KiraResponse<FileResponse | null>> {
    const status = response.status;

    let _headers: any = {};
    if (response.headers) {
      for (let key of response.headers.keys()) {
        _headers[key] = response.headers.get(key);
      }
    }
    ;
    if (status === 200 || status === 206) {
      const contentDisposition = response.headers ? response.headers.get('content-disposition') : undefined;
      const fileNameMatch = contentDisposition ? /filename="?([^"]*?)"?(;|$)/g.exec(contentDisposition) : undefined;
      const fileName = fileNameMatch && fileNameMatch.length > 1 ? fileNameMatch[1] : undefined;
      return Observable.of(new KiraResponse(status, _headers, {
        fileName: fileName,
        data: <any>response.body,
        status: status,
        headers: _headers
      }));
    } else if (status !== 200 && status !== 204) {
      return blobToText(response.body).flatMap(_responseText => {
        return throwException('An unexpected server error occurred.', status, _responseText, _headers);
      });
    }
    return Observable.of<KiraResponse<FileResponse | null>>(new KiraResponse(status, _headers, <any>null));
  }

  protected processDetail(response: HttpResponse<Blob>): Observable<KiraResponse<FileResponse | null>> {
    const status = response.status;

    let _headers: any = {};
    if (response.headers) {
      for (let key of response.headers.keys()) {
        _headers[key] = response.headers.get(key);
      }
    }
    ;
    if (status === 200 || status === 206) {
      const contentDisposition = response.headers ? response.headers.get('content-disposition') : undefined;
      const fileNameMatch = contentDisposition ? /filename="?([^"]*?)"?(;|$)/g.exec(contentDisposition) : undefined;
      const fileName = fileNameMatch && fileNameMatch.length > 1 ? fileNameMatch[1] : undefined;
      return Observable.of(new KiraResponse(status, _headers, {
        fileName: fileName,
        data: <any>response.body,
        status: status,
        headers: _headers
      }));
    } else if (status !== 200 && status !== 204) {
      return blobToText(response.body).flatMap(_responseText => {
        return throwException('An unexpected server error occurred.', status, _responseText, _headers);
      });
    }
    return Observable.of<KiraResponse<FileResponse | null>>(new KiraResponse(status, _headers, <any>null));
  }

  protected processGetByNationalId(response: HttpResponse<Blob>): Observable<KiraResponse<FileResponse | null>> {
    const status = response.status;

    let _headers: any = {};
    if (response.headers) {
      for (let key of response.headers.keys()) {
        _headers[key] = response.headers.get(key);
      }
    }
    ;
    if (status === 200 || status === 206) {
      const contentDisposition = response.headers ? response.headers.get('content-disposition') : undefined;
      const fileNameMatch = contentDisposition ? /filename="?([^"]*?)"?(;|$)/g.exec(contentDisposition) : undefined;
      const fileName = fileNameMatch && fileNameMatch.length > 1 ? fileNameMatch[1] : undefined;
      return Observable.of(new KiraResponse(status, _headers, {
        fileName: fileName,
        data: <any>response.body,
        status: status,
        headers: _headers
      }));
    } else if (status !== 200 && status !== 204) {
      return blobToText(response.body).flatMap(_responseText => {
        return throwException('An unexpected server error occurred.', status, _responseText, _headers);
      });
    }
    return Observable.of<KiraResponse<FileResponse | null>>(new KiraResponse(status, _headers, <any>null));
  }

  protected processDelete(response: HttpResponse<Blob>): Observable<KiraResponse<FileResponse | null>> {
    const status = response.status;

    let _headers: any = {};
    if (response.headers) {
      for (let key of response.headers.keys()) {
        _headers[key] = response.headers.get(key);
      }
    }
    ;
    if (status === 200 || status === 206) {
      const contentDisposition = response.headers ? response.headers.get('content-disposition') : undefined;
      const fileNameMatch = contentDisposition ? /filename="?([^"]*?)"?(;|$)/g.exec(contentDisposition) : undefined;
      const fileName = fileNameMatch && fileNameMatch.length > 1 ? fileNameMatch[1] : undefined;
      return Observable.of(new KiraResponse(status, _headers, {
        fileName: fileName,
        data: <any>response.body,
        status: status,
        headers: _headers
      }));
    } else if (status !== 200 && status !== 204) {
      return blobToText(response.body).flatMap(_responseText => {
        return throwException('An unexpected server error occurred.', status, _responseText, _headers);
      });
    }
    return Observable.of<KiraResponse<FileResponse | null>>(new KiraResponse(status, _headers, <any>null));
  }
}

export class AppointmentCreationViewModel implements IAppointmentCreationViewModel {
  dateTimeModified: Date;
  appointmentDate: Date;
  organizationId: number;
  status: EAppointmentStatus;
  firstName: string;
  lastName: string;
  email?: string | undefined;
  phoneNumber: string;
  nationalId: string;
  requireInitialPayment: boolean;
  visitReasons?: VisitReason[] | undefined;
  patientRelationship: number;

  constructor(data?: IAppointmentCreationViewModel) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  static fromJS(data: any): AppointmentCreationViewModel {
    let result = new AppointmentCreationViewModel();
    result.init(data);
    return result;
  }

  init(data?: any) {
    if (data) {
      this.dateTimeModified = data['DateTimeModified'] ? new Date(data['DateTimeModified'].toString()) : <any>undefined;
      this.appointmentDate = data['AppointmentDate'] ? new Date(data['AppointmentDate'].toString()) : <any>undefined;
      this.organizationId = data['OrganizationId'];
      this.status = data['Status'];
      this.firstName = data['FirstName'];
      this.lastName = data['LastName'];
      this.email = data['Email'];
      this.phoneNumber = data['PhoneNumber'];
      this.nationalId = data['NationalId'];
      this.requireInitialPayment = data['RequireInitialPayment'];
      if (data['VisitReasons'] && data['VisitReasons'].constructor === Array) {
        this.visitReasons = [];
        for (let item of data['VisitReasons'])
          this.visitReasons.push(VisitReason.fromJS(item));
      }
      this.patientRelationship = data['PatientRelationship'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data['DateTimeModified'] = this.dateTimeModified ? this.dateTimeModified.toISOString() : <any>undefined;
    data['AppointmentDate'] = this.appointmentDate ? this.appointmentDate.toISOString() : <any>undefined;
    data['OrganizationId'] = this.organizationId;
    data['Status'] = this.status;
    data['FirstName'] = this.firstName;
    data['LastName'] = this.lastName;
    data['Email'] = this.email;
    data['PhoneNumber'] = this.phoneNumber;
    data['NationalId'] = this.nationalId;
    data['RequireInitialPayment'] = this.requireInitialPayment;
    if (this.visitReasons && this.visitReasons.constructor === Array) {
      data['VisitReasons'] = [];
      for (let item of this.visitReasons)
        data['VisitReasons'].push(item.toJSON());
    }
    data['PatientRelationship'] = this.patientRelationship;
    return data;
  }

  clone() {
    const json = this.toJSON();
    let result = new AppointmentCreationViewModel();
    result.init(json);
    return result;
  }
}

export interface IAppointmentCreationViewModel {
  dateTimeModified: Date;
  appointmentDate: Date;
  organizationId: number;
  status: EAppointmentStatus;
  firstName: string;
  lastName: string;
  email?: string | undefined;
  phoneNumber: string;
  nationalId: string;
  requireInitialPayment: boolean;
  visitReasons?: VisitReason[] | undefined;
  patientRelationship: number;
}

export enum EAppointmentStatus {
  Pending = 1,
  Canceled = 2,
  Processing = 3,
}

export class VisitReason implements IVisitReason {
  id: number;
  reason?: string | undefined;
  relatedKnownIssue: number;

  constructor(data?: IVisitReason) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  static fromJS(data: any): VisitReason {
    let result = new VisitReason();
    result.init(data);
    return result;
  }

  init(data?: any) {
    if (data) {
      this.id = data['Id'];
      this.reason = data['Reason'];
      this.relatedKnownIssue = data['RelatedKnownIssue'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data['Id'] = this.id;
    data['Reason'] = this.reason;
    data['RelatedKnownIssue'] = this.relatedKnownIssue;
    return data;
  }

  clone() {
    const json = this.toJSON();
    let result = new VisitReason();
    result.init(json);
    return result;
  }
}

export interface IVisitReason {
  id: number;
  reason?: string | undefined;
  relatedKnownIssue: number;
}

export class AppointmentViewModel implements IAppointmentViewModel {
  dateTimeModified: Date;
  appointmentDate: Date;
  organizationId: number;
  status: EAppointmentStatus;
  firstName?: string | undefined;
  lastName?: string | undefined;
  email?: string | undefined;
  phoneNumber?: string | undefined;
  nationalId?: string | undefined;
  requireInitialPayment: boolean;
  visitReasons?: VisitReason[] | undefined;
  relationship: EPatientRelationShip;
  id: number;
  isDeleted: Date;
  dateTimeCreated: Date;

  constructor(data?: IAppointmentViewModel) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  static fromJS(data: any): AppointmentViewModel {
    let result = new AppointmentViewModel();
    result.init(data);
    return result;
  }

  init(data?: any) {
    if (data) {
      this.dateTimeModified = data['DateTimeModified'] ? new Date(data['DateTimeModified'].toString()) : <any>undefined;
      this.appointmentDate = data['AppointmentDate'] ? new Date(data['AppointmentDate'].toString()) : <any>undefined;
      this.organizationId = data['OrganizationId'];
      this.status = data['Status'];
      this.firstName = data['FirstName'];
      this.lastName = data['LastName'];
      this.email = data['Email'];
      this.phoneNumber = data['PhoneNumber'];
      this.nationalId = data['NationalId'];
      this.requireInitialPayment = data['RequireInitialPayment'];
      if (data['VisitReasons'] && data['VisitReasons'].constructor === Array) {
        this.visitReasons = [];
        for (let item of data['VisitReasons'])
          this.visitReasons.push(VisitReason.fromJS(item));
      }
      this.relationship = data['Relationship'];
      this.id = data['Id'];
      this.isDeleted = data['IsDeleted'] ? new Date(data['IsDeleted'].toString()) : <any>undefined;
      this.dateTimeCreated = data['DateTimeCreated'] ? new Date(data['DateTimeCreated'].toString()) : <any>undefined;
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data['DateTimeModified'] = this.dateTimeModified ? this.dateTimeModified.toISOString() : <any>undefined;
    data['AppointmentDate'] = this.appointmentDate ? this.appointmentDate.toISOString() : <any>undefined;
    data['OrganizationId'] = this.organizationId;
    data['Status'] = this.status;
    data['FirstName'] = this.firstName;
    data['LastName'] = this.lastName;
    data['Email'] = this.email;
    data['PhoneNumber'] = this.phoneNumber;
    data['NationalId'] = this.nationalId;
    data['RequireInitialPayment'] = this.requireInitialPayment;
    if (this.visitReasons && this.visitReasons.constructor === Array) {
      data['VisitReasons'] = [];
      for (let item of this.visitReasons)
        data['VisitReasons'].push(item.toJSON());
    }
    data['Relationship'] = this.relationship;
    data['Id'] = this.id;
    data['IsDeleted'] = this.isDeleted ? this.isDeleted.toISOString() : <any>undefined;
    data['DateTimeCreated'] = this.dateTimeCreated ? this.dateTimeCreated.toISOString() : <any>undefined;
    return data;
  }

  clone() {
    const json = this.toJSON();
    let result = new AppointmentViewModel();
    result.init(json);
    return result;
  }
}

export interface IAppointmentViewModel {
  dateTimeModified: Date;
  appointmentDate: Date;
  organizationId: number;
  status: EAppointmentStatus;
  firstName?: string | undefined;
  lastName?: string | undefined;
  email?: string | undefined;
  phoneNumber?: string | undefined;
  nationalId?: string | undefined;
  requireInitialPayment: boolean;
  visitReasons?: VisitReason[] | undefined;
  relationship: EPatientRelationShip;
  id: number;
  isDeleted: Date;
  dateTimeCreated: Date;
}

export enum EPatientRelationShip {
  Self = 1,
  Child = 2,
  Spouse = 3,
  Parent = 4,
  GrandParent = 5,
  Guardian = 6,
  Prisoner = 7,
}

export class KiraResponse<TResult> {
  status: number;
  headers: { [key: string]: any; };
  result: TResult;

  constructor(status: number, headers: { [key: string]: any; }, result: TResult) {
    this.status = status;
    this.headers = headers;
    this.result = result;
  }
}

export interface FileResponse {
  data: Blob;
  status: number;
  fileName?: string;
  headers?: { [name: string]: any };
}

export class SwaggerException extends Error {
  message: string;
  status: number;
  response: string;
  headers: { [key: string]: any; };
  result: any;
  protected isSwaggerException = true;

  constructor(message: string, status: number, response: string, headers: { [key: string]: any; }, result: any) {
    super();

    this.message = message;
    this.status = status;
    this.response = response;
    this.headers = headers;
    this.result = result;
  }

  static isSwaggerException(obj: any): obj is SwaggerException {
    return obj.isSwaggerException === true;
  }
}

function throwException(message: string, status: number, response: string, headers: { [key: string]: any; }, result?: any): Observable<any> {
  return Observable.throw(new SwaggerException(message, status, response, headers, result));
}

function blobToText(blob: any): Observable<string> {
  return new Observable<string>((observer: any) => {
    if (!blob) {
      observer.next('');
      observer.complete();
    } else {
      let reader = new FileReader();
      reader.onload = function () {
        observer.next(this.result);
        observer.complete();
      }
      reader.readAsText(blob);
    }
  });
}