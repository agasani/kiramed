import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { MockBackendVisitQueuesService } from '../../mock-backend/services';
import { MockVisitQueue, MockVisitQueueService } from '../services/mock/mock-visit-queue.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'kira-visit-queue',
  templateUrl: '../views/visit-queue.component.html',
  styleUrls: ['../sass/visit-queue.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [MockVisitQueueService]
})

export class VisitQueueComponent implements OnInit, OnDestroy {

  public visitQueues: MockVisitQueue[];
  private orgId: number;
  private mySubscription: Subscription;

  constructor(private mockBackendService: MockBackendVisitQueuesService,
              private visitQueueService: MockVisitQueueService) {
    this.mockBackendService.start();
    this.orgId = 1; // Example OrgId
  }

  ngOnInit() {
    this.mySubscription = this.visitQueueService.getVisitQueues().subscribe(
        visits => {
          this.visitQueues = visits.filter(q => q.organizationId === this.orgId)
        },
        error => console.error(error),
        () => console.log('Our visit queues: ', this.visitQueues)
    )
  }

  ngOnDestroy() {
    this.mySubscription.unsubscribe();
  }
}
