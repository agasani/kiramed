﻿import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VisitQueueComponent } from './components/visit-queue.component';
import { SharedModule } from '../shared/shared.module';

const ROUTES: Routes = [{ path: '', component: VisitQueueComponent }];

@NgModule({
  imports: [SharedModule, RouterModule.forChild(ROUTES)],
  declarations: [VisitQueueComponent]
})

export class VisitQueueModule {
}
