import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';

export class MockVisitQueue {
  'queueOrderNumber': 1;
  'arrivalDateTime': Date;
  'firstName': string;
  'lastName': string;
  'patientId': string;
  'visitType': number;
  'relationship': number;
  'organizationId': number; // For dummy
}

@Injectable()
export class MockVisitQueueService {
  private queuesUrl = 'http://localhost:8080/visitQueues';

  constructor(private http: Http) {
  }

  getVisitQueues(): Observable<MockVisitQueue[]> {
    return this.http.get(this.queuesUrl)
        .map(response => response.json() as MockVisitQueue[])
        .catch(this.handleError);
  }

  private handleError(error: any): Observable<any> {
    console.error('An error occured', error); // for demo purposes only
    return Observable.throw(error.message || error);
  }
}
