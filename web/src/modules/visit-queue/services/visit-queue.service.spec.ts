import { VisitQueueClient } from './visit-queue.service';

describe('VisitQueueClient', () => {
  let service: VisitQueueClient;

  beforeEach(() => {
    service = new VisitQueueClient();
  });

  it('works', () => {
    expect(1).toEqual(2);
  });

});
