import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgForm } from '@angular/forms';

import { Subscription } from 'rxjs/Subscription';
import { LocalStorageService } from 'ngx-webstorage';
import { BaseComponent } from '../../shared/components';
import { OrganizationService } from '../../api/services/organization.service';
import { UserService } from '../../api/services/user.service';
import { CountryService } from '../../api/services/country.service';
import { OrganizationViewModel } from '../../api/models/organization-view-model';
import { PatientViewModel } from '../../api/models/patient-view-model';
import { TransferService } from '../../api/services/transfer.service';
import { PatientService } from '../../api/services/patient.service';
import { TransferCreateViewModel } from '../../api/models/transfer-create-view-model';
import { MedService } from '../../api/services/med.service';

@Component({
  selector: 'clinical-patient',
  templateUrl: './../views/patient.html',
  styleUrls: ['../sass/clinical.scss'],
  providers: [PatientService, TransferService, OrganizationService, UserService, CountryService]
})

export class ClinicalPatientComponent extends BaseComponent {
  med;
  patient: PatientViewModel;
  hospitals: OrganizationViewModel[];
  private subscription: Subscription;
  private age: number;
  private transfer: TransferCreateViewModel;

  constructor(private _route: ActivatedRoute,
              private _itemService: PatientService,
              private _router: Router,
              private transferService: TransferService,
              private medService: MedService,
              private organizationService: OrganizationService,
              private userService: UserService,
              private countryService: CountryService,
              private storage: LocalStorageService) {
    super();
    this.itemService = _itemService;
    this.route = _route;
    this.router = _router;
  }

  ngOnInit(): void {
    this.subscription = this.route.params.subscribe((param: any) => {
          const id = param['id'];
          if (id) this.storage.store('patientId', '' + id);
      this.medService.ApiPatientsMedByIdGet(id).subscribe(med => this.med = med);
      this.itemService.ApiPatientDetailsByIdGet(id).subscribe(
          pat => {
                this.patient = pat;
                this.age = new Date().getFullYear() - new Date(this.patient.dob).getFullYear();
              });
        }
    );
  }

  ngAfterViewInit(): void {
    $('.ui.dropdown').dropdown();
  }

  showTransferModal() {

    this.countryService.ApiCountryGetCountryOrganizationsByCountryIdGet(this.userCountryId).subscribe(
        countryOrgs => this.hospitals = countryOrgs.filter(org => org.id !== this.userOrgId),
        err => console.error('Error in orgs listing: ', err),
    );

    $('.ui.modal.transfer.clinic').modal('show');
    $('.modals').css('z-index', 10001);
  }

  add(transfer: NgForm): void {

    console.log('Form: ', transfer.value);

    this.transfer.arrivalDateTime = new Date().toISOString();
    this.transfer.receivingOrganizationId = transfer.value.to;
    this.transfer.relationship = 1;
    this.transfer.originOrganizationId = this.userOrgId;
    this.transfer.nationalPatientId = '';
    this.transfer.transferPhysicianUserId = this.userId;
    // this.transfer.note = transfer.value.note

    // const transfer = this.transferService.addTransfer('Byo Centre de Sante', f.value.to, f.value.note);
    this.transferService.ApiTransferPost({ vModel: this.transfer }).subscribe(
        () => {
          console.log('Patient transfered successfully!');
          $('#save').addClass('loading')
        },
        err => {
          console.error('ERROR: ', err);
          $('#save').removeClass('loading');
          $('.error.nag').nag('show');
        },
        () => {
          $('#save').removeClass('loading');
          $('.ui.modal').modal('hide');
          $('.success.nag').nag('show');
        }
    );
  }
}
