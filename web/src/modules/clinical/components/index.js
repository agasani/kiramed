"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(require("./clinical.component"));
__export(require("./clinical-encounter.component"));
__export(require("./clinical-forms.component"));
//# sourceMappingURL=index.js.map