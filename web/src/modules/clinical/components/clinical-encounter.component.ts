import { AfterViewInit, Component, OnInit } from '@angular/core';

@Component({
  selector: 'kira-encounter',
  templateUrl: '../views/encounter.html',
  styleUrls: ['../sass/clinical.scss']
})

export class ClinicalEncounterComponent implements OnInit, AfterViewInit {

  ngOnInit() {
    console.log('oninit..');
  }

  ngAfterViewInit() {
    ////Setup the sidebar..
    /*$('.ui.sidebar')
     .sidebar({
     context: $('.bottom.segment')
     })
     .sidebar('attach events', '.menu .item', 'toggle');*/
  }
}
