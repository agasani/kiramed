import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Subscription } from 'rxjs/Subscription';
import { BaseComponent } from '../../shared/components';


@Component({
  selector: 'clinical-patient',
  templateUrl: '../views/forms.html',
  styleUrls: ['../sass/clinical.scss']
})

export class ClinicalFormsComponent extends BaseComponent {

  private subscription: Subscription;

  constructor(private activatedRoute: ActivatedRoute) {
    super()
  }

  ngOnInit(): void {

  }

  ngOnDestroy(): void {
    // this.subscription.unsubscribe();
  }
}