﻿import { Component } from '@angular/core';
import { BaseComponent } from '../../shared/components';

@Component({
  selector: 'kira-clinical',
  templateUrl: '../views/clinical.html',
  styleUrls: ['../sass/clinical.scss']
})

export class ClinicalComponent extends BaseComponent {

  constructor() {
    super();
  }

  ngOnInit() {
    console.log('oninit..');
  }

  ngAfterViewInit() {
    this.sideBarMenuTrigger();
  }
}
