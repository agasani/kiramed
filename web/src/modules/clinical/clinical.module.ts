﻿import { NgModule } from '@angular/core';

import { ClinicalPatientComponent } from './clinical-patient';
import { ClinicalComponent, ClinicalEncounterComponent, ClinicalFormsComponent } from './components';
import { PatientModule } from '../patient/patient.module';
import { clinicalRouting$ } from './clinical.routing';
import { SharedModule } from '../shared/shared.module';
import { PatientService } from '../patient/services/patient.service';
import { MedService } from './modules/med/med.service';

@NgModule({
  imports: [
    SharedModule,
    clinicalRouting$,
    PatientModule
  ],
  declarations: [
    ClinicalComponent,
    ClinicalEncounterComponent,
    ClinicalPatientComponent,
    ClinicalFormsComponent,
    ClinicalPatientComponent,
  ],
  providers: [PatientService, MedService]
})

export class ClinicalModule {
}
