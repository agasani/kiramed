﻿import { RouterModule, Routes } from '@angular/router';
import { ClinicalComponent, ClinicalFormsComponent } from './components';

import {
  PatientAllergiesComponent,
  PatientConditionsComponent,
  PatientDocsComponent,
  PatientFitnessComponent,
  PatientLabsComponent,
  PatientMedsComponent,
  PatientSurgeryComponent,
  PatientTransfersComponent,
  PatientVaxComponent,
  PatientVisitsComponent,
  PatientVitalsComponent,
  PatientWellnessComponent
} from '../patient/components';
import { ClinicalPatientComponent } from './clinical-patient';
import { AccessGuard, AuthGuard } from '../shared/guards';

const ROUTES: Routes = [
  {
    path: '',
    canActivate: [AuthGuard],
    component: ClinicalComponent,
    children: [
      { path: 'patient-view/:id', component: ClinicalPatientComponent },
      {
        path: 'forms',
        component: ClinicalFormsComponent,
        canActivate: [AccessGuard],
        data: { accessGuardClaims: ['v.form'] }
      },
      {
        path: 'meds',
        component: PatientMedsComponent,
        canActivate: [AccessGuard],
        data: { accessGuardClaims: ['v.meds'] }
      },
      {
        path: 'vitals',
        component: PatientVitalsComponent,
        canActivate: [AccessGuard],
        data: { accessGuardClaims: ['v.vit'] }
      },
      {
        path: 'labs',
        component: PatientLabsComponent,
        canActivate: [AccessGuard],
        data: { accessGuardClaims: ['v.labs'] }
      },
      {
        path: 'surgeries',
        component: PatientSurgeryComponent,
        canActivate: [AccessGuard],
        data: { accessGuardClaims: ['v.surg'] }
      },
      {
        path: 'conditions',
        component: PatientConditionsComponent,
        canActivate: [AccessGuard],
        data: { accessGuardClaims: ['v.cond'] }
      },
      {
        path: 'visits',
        component: PatientVisitsComponent,
        canActivate: [AccessGuard],
        data: { accessGuardClaims: ['v.visit'] }
      },
      {
        path: 'docs',
        component: PatientDocsComponent,
        canActivate: [AccessGuard],
        data: { accessGuardClaims: ['v.docs'] }
      },
      {
        path: 'vaccinations',
        component: PatientVaxComponent,
        canActivate: [AccessGuard],
        data: { accessGuardClaims: ['v.vax'] }
      },
      {
        path: 'transfers',
        component: PatientTransfersComponent,
        canActivate: [AccessGuard],
        data: { accessGuardClaims: ['v.trans'] }
      },
      {
        path: 'allergies',
        component: PatientAllergiesComponent,
        canActivate: [AccessGuard],
        data: { accessGuardClaims: ['v.allg'] }
      },
      {
        path: 'wellness',
        component: PatientWellnessComponent,
        canActivate: [AccessGuard],
        data: { accessGuardClaims: ['v.fit'] }
      },
      {
        path: 'fitness',
        component: PatientFitnessComponent,
        canActivate: [AccessGuard],
        data: { accessGuardClaims: ['v.fit'] }
      },
      { path: 'new-encounter', loadChildren: './modules/encounter/encounter.module#EncounterModule' }
    ]
  }
];

export const clinicalRouting$ = RouterModule.forRoot(ROUTES);
