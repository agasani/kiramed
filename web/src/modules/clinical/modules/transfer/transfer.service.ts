﻿import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/catch';

import { Observable } from 'rxjs/Observable';
import { Inject, Injectable, InjectionToken, Optional } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';

export const API_BASE_URL = new InjectionToken<string>('API_BASE_URL');

export interface ITransferService {
  getIncoming(orgId: number): Observable<KiraResponse<FileResponse | null>>;

  getOutGoing(orgId: number): Observable<KiraResponse<FileResponse | null>>;

  post(vModel?: TransferCreateViewModel | null): Observable<KiraResponse<FileResponse | null>>;
}

@Injectable()
export class TransferService implements ITransferService {
  protected jsonParseReviver: ((key: string, value: any) => any) | undefined = undefined;
  private http: HttpClient;
  private baseUrl: string;

  constructor(@Inject(HttpClient) http: HttpClient, @Optional() @Inject(API_BASE_URL) baseUrl?: string) {
    this.http = http;
    this.baseUrl = baseUrl ? baseUrl : '';
  }

  getIncoming(orgId: number): Observable<KiraResponse<FileResponse | null>> {
    let url_ = this.baseUrl + '/api/Transfer/GetIncoming/{orgId}';
    if (orgId === undefined || orgId === null)
      throw new Error('The parameter \'orgId\' must be defined.');
    url_ = url_.replace('{orgId}', encodeURIComponent('' + orgId));
    url_ = url_.replace(/[?&]$/, '');

    let options_: any = {
      observe: 'response',
      responseType: 'blob',
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };

    return this.http.request('get', url_, options_).flatMap((response_: any) => {
      return this.processGetIncoming(response_);
    }).catch((response_: any) => {
      if (response_ instanceof HttpResponse) {
        try {
          return this.processGetIncoming(response_);
        } catch (e) {
          return <Observable<KiraResponse<FileResponse | null>>><any>Observable.throw(e);
        }
      } else
        return <Observable<KiraResponse<FileResponse | null>>><any>Observable.throw(response_);
    });
  }

  getOutGoing(orgId: number): Observable<KiraResponse<FileResponse | null>> {
    let url_ = this.baseUrl + '/api/Transfer/GetOutGoing/{orgId}';
    if (orgId === undefined || orgId === null)
      throw new Error('The parameter \'orgId\' must be defined.');
    url_ = url_.replace('{orgId}', encodeURIComponent('' + orgId));
    url_ = url_.replace(/[?&]$/, '');

    let options_: any = {
      observe: 'response',
      responseType: 'blob',
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };

    return this.http.request('get', url_, options_).flatMap((response_: any) => {
      return this.processGetOutGoing(response_);
    }).catch((response_: any) => {
      if (response_ instanceof HttpResponse) {
        try {
          return this.processGetOutGoing(response_);
        } catch (e) {
          return <Observable<KiraResponse<FileResponse | null>>><any>Observable.throw(e);
        }
      } else
        return <Observable<KiraResponse<FileResponse | null>>><any>Observable.throw(response_);
    });
  }

  post(vModel?: TransferCreateViewModel | null): Observable<KiraResponse<FileResponse | null>> {
    let url_ = this.baseUrl + '/api/Transfer';
    url_ = url_.replace(/[?&]$/, '');

    const content_ = JSON.stringify(vModel);

    let options_: any = {
      body: content_,
      observe: 'response',
      responseType: 'blob',
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };

    return this.http.request('post', url_, options_).flatMap((response_: any) => {
      return this.processPost(response_);
    }).catch((response_: any) => {
      if (response_ instanceof HttpResponse) {
        try {
          return this.processPost(response_);
        } catch (e) {
          return <Observable<KiraResponse<FileResponse | null>>><any>Observable.throw(e);
        }
      } else
        return <Observable<KiraResponse<FileResponse | null>>><any>Observable.throw(response_);
    });
  }

  protected processGetIncoming(response: HttpResponse<Blob>): Observable<KiraResponse<FileResponse | null>> {
    const status = response.status;

    let _headers: any = {};
    if (response.headers) {
      for (let key of response.headers.keys()) {
        _headers[key] = response.headers.get(key);
      }
    }
    if (status === 200 || status === 206) {
      const contentDisposition = response.headers ? response.headers.get('content-disposition') : undefined;
      const fileNameMatch = contentDisposition ? /filename="?([^"]*?)"?(;|$)/g.exec(contentDisposition) : undefined;
      const fileName = fileNameMatch && fileNameMatch.length > 1 ? fileNameMatch[1] : undefined;
      return Observable.of(new KiraResponse(status, _headers, {
        fileName: fileName,
        data: <any>response.body,
        status: status,
        headers: _headers
      }));
    } else if (status !== 200 && status !== 204) {
      return blobToText(response.body).flatMap(_responseText => {
        return throwException('An unexpected server error occurred.', status, _responseText, _headers);
      });
    }
    return Observable.of<KiraResponse<FileResponse | null>>(new KiraResponse(status, _headers, <any>null));
  }

  protected processGetOutGoing(response: HttpResponse<Blob>): Observable<KiraResponse<FileResponse | null>> {
    const status = response.status;

    let _headers: any = {};
    if (response.headers) {
      for (let key of response.headers.keys()) {
        _headers[key] = response.headers.get(key);
      }
    }
    if (status === 200 || status === 206) {
      const contentDisposition = response.headers ? response.headers.get('content-disposition') : undefined;
      const fileNameMatch = contentDisposition ? /filename="?([^"]*?)"?(;|$)/g.exec(contentDisposition) : undefined;
      const fileName = fileNameMatch && fileNameMatch.length > 1 ? fileNameMatch[1] : undefined;
      return Observable.of(new KiraResponse(status, _headers, {
        fileName: fileName,
        data: <any>response.body,
        status: status,
        headers: _headers
      }));
    } else if (status !== 200 && status !== 204) {
      return blobToText(response.body).flatMap(_responseText => {
        return throwException('An unexpected server error occurred.', status, _responseText, _headers);
      });
    }
    return Observable.of<KiraResponse<FileResponse | null>>(new KiraResponse(status, _headers, <any>null));
  }

  protected processPost(response: HttpResponse<Blob>): Observable<KiraResponse<FileResponse | null>> {
    const status = response.status;

    let _headers: any = {};
    if (response.headers) {
      for (let key of response.headers.keys()) {
        _headers[key] = response.headers.get(key);
      }
    }
    if (status === 200 || status === 206) {
      const contentDisposition = response.headers ? response.headers.get('content-disposition') : undefined;
      const fileNameMatch = contentDisposition ? /filename="?([^"]*?)"?(;|$)/g.exec(contentDisposition) : undefined;
      const fileName = fileNameMatch && fileNameMatch.length > 1 ? fileNameMatch[1] : undefined;
      return Observable.of(new KiraResponse(status, _headers, {
        fileName: fileName,
        data: <any>response.body,
        status: status,
        headers: _headers
      }));
    } else if (status !== 200 && status !== 204) {
      return blobToText(response.body).flatMap(_responseText => {
        return throwException('An unexpected server error occurred.', status, _responseText, _headers);
      });
    }
    return Observable.of<KiraResponse<FileResponse | null>>(new KiraResponse(status, _headers, <any>null));
  }
}

export class TransferCreateViewModel implements ITransferCreateViewModel {
  arrivalDateTime: Date;
  receivingOrganizationId: number;
  relationship: EPatientRelationShip;
  originOrganizationId: number;
  nationalPatientId?: string | null;
  transferPhysicianUserId?: string | null;

  constructor(data?: ITransferCreateViewModel) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  static fromJS(data: any): TransferCreateViewModel {
    let result = new TransferCreateViewModel();
    result.init(data);
    return result;
  }

  init(data?: any) {
    if (data) {
      this.arrivalDateTime = data['ArrivalDateTime'] ? new Date(data['ArrivalDateTime'].toString()) : <any>null;
      this.receivingOrganizationId = data['ReceivingOrganizationId'] !== undefined ? data['ReceivingOrganizationId'] : <any>null;
      this.relationship = data['Relationship'] !== undefined ? data['Relationship'] : <any>null;
      this.originOrganizationId = data['OriginOrganizationId'] !== undefined ? data['OriginOrganizationId'] : <any>null;
      this.nationalPatientId = data['NationalPatientId'] !== undefined ? data['NationalPatientId'] : <any>null;
      this.transferPhysicianUserId = data['TransferPhysicianUserId'] !== undefined ? data['TransferPhysicianUserId'] : <any>null;
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data['ArrivalDateTime'] = this.arrivalDateTime ? this.arrivalDateTime.toISOString() : <any>null;
    data['ReceivingOrganizationId'] = this.receivingOrganizationId !== undefined ? this.receivingOrganizationId : <any>null;
    data['Relationship'] = this.relationship !== undefined ? this.relationship : <any>null;
    data['OriginOrganizationId'] = this.originOrganizationId !== undefined ? this.originOrganizationId : <any>null;
    data['NationalPatientId'] = this.nationalPatientId !== undefined ? this.nationalPatientId : <any>null;
    data['TransferPhysicianUserId'] = this.transferPhysicianUserId !== undefined ? this.transferPhysicianUserId : <any>null;
    return data;
  }

  clone() {
    const json = this.toJSON();
    let result = new TransferCreateViewModel();
    result.init(json);
    return result;
  }
}

export interface ITransferCreateViewModel {
  arrivalDateTime: Date;
  receivingOrganizationId: number;
  relationship: EPatientRelationShip;
  originOrganizationId: number;
  nationalPatientId?: string | null;
  transferPhysicianUserId?: string | null;
}

export enum EPatientRelationShip {
  Self = 1,
  Child = 2,
  Spouse = 3,
  Parent = 4,
  GrandParent = 5,
  Guardian = 6,
  Prisoner = 7,
}

export class KiraResponse<TResult> {
  status: number;
  headers: { [key: string]: any; };
  result: TResult;

  constructor(status: number, headers: { [key: string]: any; }, result: TResult) {
    this.status = status;
    this.headers = headers;
    this.result = result;
  }
}

export interface FileResponse {
  data: Blob;
  status: number;
  fileName?: string;
  headers?: { [name: string]: any };
}

export class KiraException extends Error {
  message: string;
  status: number;
  response: string;
  headers: { [key: string]: any; };
  result: any;
  protected isKiraException = true;

  constructor(message: string, status: number, response: string, headers: { [key: string]: any; }, result: any) {
    super();

    this.message = message;
    this.status = status;
    this.response = response;
    this.headers = headers;
    this.result = result;
  }

  static isKiraException(obj: any): obj is KiraException {
    return obj.isKiraException === true;
  }
}

function throwException(message: string, status: number, response: string, headers: { [key: string]: any; }, result?: any): Observable<any> {
  return Observable.throw(new KiraException(message, status, response, headers, result));
}

function blobToText(blob: any): Observable<string> {
  return new Observable<string>((observer: any) => {
    if (!blob) {
      observer.next('');
      observer.complete();
    } else {
      let reader = new FileReader();
      reader.onload = function () {
        observer.next(this.result);
        observer.complete();
      };
      reader.readAsText(blob);
    }
  });
}
