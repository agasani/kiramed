"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var components_1 = require("./components");
var routes = [
    {
        path: '',
        component: components_1.EncounterComponent,
        children: [
            { path: '', redirectTo: 'vx', pathMatch: 'full' },
            { path: 'vx', component: components_1.VitalsComponent },
            { path: 'sx', component: components_1.SymptomsComponent },
            { path: 'hx', component: components_1.HistoryComponent },
            { path: 'note', component: components_1.EncounterNoteComponent },
            { path: 'px', component: components_1.EncounterPhysicalComponent },
            { path: 'tx', component: components_1.EncounterTestComponent },
            { path: 'dx', component: components_1.EncounterDiagnosComponent },
            { path: 'rx', component: components_1.EncounterTherapyComponent },
        ]
    }
];
var EncounterRoutingModule = /** @class */ (function () {
    function EncounterRoutingModule() {
    }
    EncounterRoutingModule = __decorate([
        core_1.NgModule({
            imports: [router_1.RouterModule.forChild(routes)],
            exports: [router_1.RouterModule]
        })
    ], EncounterRoutingModule);
    return EncounterRoutingModule;
}());
exports.EncounterRoutingModule = EncounterRoutingModule;
//# sourceMappingURL=encounter-routing.module.js.map