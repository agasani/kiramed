"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var symptom_1 = require("../models/symptom");
var Observable_1 = require("rxjs/Observable");
var symptoms = [];
var sx1 = new symptom_1.Symptom(1, 'Encounter background', '1111', 'ICD-101');
var sx11 = new symptom_1.Symptom(2, 'Chief Complaint', '11112', 'ICD-102', '1111');
sx1.subSymptoms.push(sx11);
var sx12 = new symptom_1.Symptom(3, 'Visit For', '11113', 'ICD-103', '1111');
var sx121 = new symptom_1.Symptom(4, 'Examination', '111131', 'ICD-104', '11113');
sx12.subSymptoms.push(sx121);
var sx122 = new symptom_1.Symptom(5, 'Laboratory', '111132', 'ICD-105', '11113');
sx12.subSymptoms.push(sx122);
var sx123 = new symptom_1.Symptom(6, 'Chemotherapy', '111133', 'ICD-106', '11113');
sx12.subSymptoms.push(sx123);
var sx124 = new symptom_1.Symptom(7, 'Radiotherapy', '111134', 'ICD-107', '11113');
sx12.subSymptoms.push(sx124);
sx1.subSymptoms.push(sx12);
var sx13 = new symptom_1.Symptom(8, 'ROS free text', '11114', 'ICD-108', '1111');
sx1.subSymptoms.push(sx13);
symptoms.push(sx1);
var sx2 = new symptom_1.Symptom(9, 'System symptoms', '2222', 'ICD-202');
symptoms.push(sx2);
var sx3 = new symptom_1.Symptom(10, 'Head-related Symptoms', '3333', 'ICD-303');
symptoms.push(sx3);
var sx4 = new symptom_1.Symptom(11, 'Neck symptoms', '3333', 'ICD-404');
symptoms.push(sx4);
var sx5 = new symptom_1.Symptom(12, 'Cardiovascular symptoms', '5555', 'ICD-505');
symptoms.push(sx5);
var SymptomService = /** @class */ (function () {
    function SymptomService(_http) {
        this._http = _http;
        // super(_http);
    }
    SymptomService.prototype.getSymptoms = function () {
        return Observable_1.Observable.of(symptoms);
    };
    SymptomService.prototype.getSymptomsCount = function () {
        return symptoms.length;
    };
    SymptomService.prototype.addSymptom = function (symptom) {
        symptoms.push(symptom);
    };
    SymptomService.prototype.getSymptom = function (id) {
        console.log('Id: ', id);
        return Observable_1.Observable.of(symptoms[id]);
    };
    SymptomService.prototype.getSympt = function (id) {
        console.log('Id: ', id);
        return symptoms[id];
    };
    SymptomService.prototype.searchSymptom = function (term) {
        return symptoms;
    };
    SymptomService = __decorate([
        core_1.Injectable()
    ], SymptomService);
    return SymptomService;
}());
exports.SymptomService = SymptomService;
//# sourceMappingURL=sx.service.js.map