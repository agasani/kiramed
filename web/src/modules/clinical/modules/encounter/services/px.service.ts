﻿import { Injectable } from '@angular/core';

import { Physical } from '../models/physical';
import { HttpClient } from '@angular/common/http';

const physicals: Physical[] = [];

const px1 = new Physical(1, 'Vital Signs', '1111', 'ICD-101');

const px11 = new Physical(2, 'Temperature', '11112', 'ICD-102', '1111');
px1.subPhysicals.push(px11);

const px12 = new Physical(3, 'Pulse Rate', '11113', 'ICD-103', '1111');
const px121 = new Physical(5, 'While Standing', '111131', 'ICD-104', '11113');
px12.subPhysicals.push(px121);
const px122 = new Physical(4, 'While Sitting', '111132', 'ICD-105', '11113');
px12.subPhysicals.push(px122);
const px123 = new Physical(6, 'Radial', '111133', 'ICD-106', '11113');
px12.subPhysicals.push(px123);
const px124 = new Physical(7, 'Subclavian', '111134', 'ICD-107', '11113');
px12.subPhysicals.push(px124);
px1.subPhysicals.push(px12);

const px13 = new Physical(8, 'Blood Pressure', '11114', 'ICD-108', '1111');
px1.subPhysicals.push(px13);

physicals.push(px1);

const px2 = new Physical(9, 'Standard Measurements', '2222', 'ICD-202');
physicals.push(px2);

const px3 = new Physical(10, 'Head', '3333', 'ICD-303');
physicals.push(px3);

const px4 = new Physical(11, 'Eyes', '3333', 'ICD-404');
physicals.push(px4);

const px5 = new Physical(12, 'Neck', '5555', 'ICD-505');
physicals.push(px5);


@Injectable()
export class PhysicalService {

  constructor(private http: HttpClient) {
  }

  getPhysicals(): Promise<Physical[]> {

    return Promise.resolve(physicals);
  }

  getPhysicalsCount(): number {

    return physicals.length;
  }

  addPhysical(symptom: Physical): void {

    physicals.push(symptom);
  }

  getPhysical(id: number): Promise<Physical> {

    console.log('Id: ', id);
    return Promise.resolve(physicals[id]);
  }

  getPhys(id: number): Physical {

    console.log('Id: ', id);

    return physicals[id];
  }

  searchPhysical(term: string): Physical[] {

    return physicals;
  }
}
