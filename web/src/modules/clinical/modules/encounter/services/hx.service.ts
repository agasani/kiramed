﻿import { Injectable } from '@angular/core';

import { History } from '../models/history';
import { HttpClient } from '@angular/common/http';

const allHistory: History[] = [];

const hx1 = new History(1, 'Past medical history', '1111', 'ICD-101');

const hx11 = new History(2, 'Medical', '11112', 'ICD-102', '1111');
hx1.subHistory.push(hx11);

const hx12 = new History(3, 'Pregnancy history', '11113', 'ICD-103', '1111');
const hx121 = new History(4, 'Currently pregnant', '111131', 'ICD-104', '11113');
hx12.subHistory.push(hx121);
const hx122 = new History(5, 'Recent pregnance', '111132', 'ICD-105', '11113');
hx12.subHistory.push(hx122);
const hx123 = new History(6, 'Duration of last Pregnancy', '111133', 'ICD-106', '11113');
hx12.subHistory.push(hx123);
const hx124 = new History(7, 'Teenage Pregnancy', '111134', 'ICD-107', '11113');
hx12.subHistory.push(hx124);
hx1.subHistory.push(hx12);

const hx13 = new History(8, 'Surgical / Procedural', '11114', 'ICD-108', '1111');
hx1.subHistory.push(hx13);

allHistory.push(hx1);

const hx2 = new History(9, 'Family Medical History', '2222', 'ICD-202');
allHistory.push(hx2);

const hx3 = new History(10, 'Social History', '3333', 'ICD-303');
allHistory.push(hx3);


@Injectable()
export class HistoryService {

  constructor(private http: HttpClient) {
  }

  getAllHistory(): Promise<History[]> {

    return Promise.resolve(allHistory);
  }

  getHistoryCount(): number {

    return allHistory.length;
  }

  addHistory(symptom: History): void {

    allHistory.push(symptom);
  }

  getHistory(id: number): Promise<History> {

    console.log('Id: ', id);
    return Promise.resolve(allHistory[id]);
  }

  getHist(id: number): History {

    console.log('Id: ', id);

    return allHistory[id];
  }

  searchHistory(term: string): History[] {

    return allHistory;
  }
}
