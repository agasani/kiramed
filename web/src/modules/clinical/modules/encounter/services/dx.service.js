"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var diagnos_1 = require("../models/diagnos");
var diagnoses = [];
var dx1 = new diagnos_1.Diagnos(1, 'Pedriatric Disorder', '1111', 'ICD-101');
var dx11 = new diagnos_1.Diagnos(2, 'Microcephallus', '11112', 'ICD-102', '1111');
dx1.subDiagnoses.push(dx11);
var dx12 = new diagnos_1.Diagnos(3, 'Encephalocele', '11113', 'ICD-103', '1111');
var dx121 = new diagnos_1.Diagnos(5, 'Cranial Meningocele', '111131', 'ICD-104', '11113');
dx12.subDiagnoses.push(dx121);
var dx122 = new diagnos_1.Diagnos(4, 'Encephalocystocele', '111132', 'ICD-105', '11113');
dx12.subDiagnoses.push(dx122);
var dx123 = new diagnos_1.Diagnos(6, 'Encephalomyelocele', '111133', 'ICD-106', '11113');
dx12.subDiagnoses.push(dx123);
var dx124 = new diagnos_1.Diagnos(7, 'Meningoencephlocele', '111134', 'ICD-107', '11113');
dx12.subDiagnoses.push(dx124);
dx1.subDiagnoses.push(dx12);
var dx13 = new diagnos_1.Diagnos(8, 'Macrogyria', '11114', 'ICD-108', '1111');
dx1.subDiagnoses.push(dx13);
diagnoses.push(dx1);
var dx2 = new diagnos_1.Diagnos(9, 'Endocrine Disorder', '2222', 'ICD-202');
diagnoses.push(dx2);
var dx3 = new diagnos_1.Diagnos(10, 'Immunologic Disorder', '3333', 'ICD-303');
diagnoses.push(dx3);
var dx4 = new diagnos_1.Diagnos(11, 'Injuries / Accidents', '3333', 'ICD-404');
diagnoses.push(dx4);
var dx5 = new diagnos_1.Diagnos(12, 'Metabolic Disorder', '5555', 'ICD-505');
diagnoses.push(dx5);
var DiagnosService = /** @class */ (function () {
    function DiagnosService(http) {
        this.http = http;
    }
    DiagnosService.prototype.getDiagnoss = function () {
        return Promise.resolve(diagnoses);
    };
    DiagnosService.prototype.getDiagnosesCount = function () {
        return diagnoses.length;
    };
    DiagnosService.prototype.addDiagnos = function (symptom) {
        diagnoses.push(symptom);
    };
    DiagnosService.prototype.getDiagnos = function (id) {
        console.log('Id: ', id);
        return Promise.resolve(diagnoses[id]);
    };
    DiagnosService.prototype.getSympt = function (id) {
        console.log('Id: ', id);
        return diagnoses[id];
    };
    DiagnosService.prototype.searchDiagnos = function (term) {
        return diagnoses;
    };
    DiagnosService = __decorate([
        core_1.Injectable()
    ], DiagnosService);
    return DiagnosService;
}());
exports.DiagnosService = DiagnosService;
//# sourceMappingURL=dx.service.js.map