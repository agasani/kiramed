"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var therapy_1 = require("../models/therapy");
var therapies = [];
var rx1 = new therapy_1.Therapy(1, 'Surgery', '1111', 'ICD-101');
var rx11 = new therapy_1.Therapy(2, 'Eyes', '11112', 'ICD-102', '1111');
rx1.subTherapies.push(rx11);
var rx12 = new therapy_1.Therapy(3, 'Cornea', '11113', 'ICD-103', '1111');
var rx121 = new therapy_1.Therapy(5, 'excision of Lesion', '111131', 'ICD-104', '11113');
rx12.subTherapies.push(rx121);
var rx122 = new therapy_1.Therapy(4, 'Repair of Cornes', '111132', 'ICD-105', '11113');
rx12.subTherapies.push(rx122);
var rx123 = new therapy_1.Therapy(6, 'Transplant', '111133', 'ICD-106', '11113');
rx12.subTherapies.push(rx123);
var rx124 = new therapy_1.Therapy(7, 'Removal of Epithelium', '111134', 'ICD-107', '11113');
rx12.subTherapies.push(rx124);
rx1.subTherapies.push(rx12);
var rx13 = new therapy_1.Therapy(8, 'Gynecologic', '11114', 'ICD-108', '1111');
rx1.subTherapies.push(rx13);
therapies.push(rx1);
var rx2 = new therapy_1.Therapy(9, 'Meds & Vaccines', '2222', 'ICD-202');
therapies.push(rx2);
var rx3 = new therapy_1.Therapy(10, 'Radiation Therapy', '3333', 'ICD-303');
therapies.push(rx3);
var rx4 = new therapy_1.Therapy(11, 'Nursing Care', '3333', 'ICD-404');
therapies.push(rx4);
var rx5 = new therapy_1.Therapy(12, 'Social Services', '5555', 'ICD-505');
therapies.push(rx5);
var TherapyService = /** @class */ (function () {
    function TherapyService(http) {
        this.http = http;
    }
    TherapyService.prototype.getTherapies = function () {
        return Promise.resolve(therapies);
    };
    TherapyService.prototype.getTherapiesCount = function () {
        return therapies.length;
    };
    TherapyService.prototype.addTherapy = function (symptom) {
        therapies.push(symptom);
    };
    TherapyService.prototype.getTherapy = function (id) {
        console.log('Id: ', id);
        return Promise.resolve(therapies[id]);
    };
    TherapyService.prototype.getTher = function (id) {
        console.log('Id: ', id);
        return therapies[id];
    };
    TherapyService.prototype.searchTherapy = function (term) {
        return therapies;
    };
    TherapyService = __decorate([
        core_1.Injectable()
    ], TherapyService);
    return TherapyService;
}());
exports.TherapyService = TherapyService;
//# sourceMappingURL=rx.service.js.map