﻿import { Injectable } from '@angular/core';

import { Symptom } from '../models/symptom';
import { History } from '../models/history';
import { Physical } from '../models/physical';
import { Test } from '../models/test';
import { Diagnos } from '../models/diagnos';
import { Therapy } from '../models/therapy';

import { EncounterNote } from '../models/encounter-note';
import { HttpClient } from '@angular/common/http';


@Injectable()
export class EncounterService {

  encounter: EncounterNote;

  constructor(protected _http: HttpClient) {
    this.encounter = EncounterNote.getInstance();
  }

  getEncounter(): EncounterNote {
    return this.encounter;
  }

  addSymptom(symptom: Symptom): void {

    console.log('in adding symptom: ', this.encounter.symptoms);

    const // 20 generations tree
        enco = this.encounter,

        p1 = enco.symptoms.find(sx => '' + sx.id === symptom.parentId),
        p2 = p1.subSymptoms.find(sx => '' + sx.id === symptom.parentId),
        p3 = p2.subSymptoms.find(sx => '' + sx.id === symptom.parentId),
        p4 = p3.subSymptoms.find(sx => '' + sx.id === symptom.parentId),
        p5 = p4.subSymptoms.find(sx => '' + sx.id === symptom.parentId),
        p6 = p5.subSymptoms.find(sx => '' + sx.id === symptom.parentId),
        p7 = p6.subSymptoms.find(sx => '' + sx.id === symptom.parentId),
        p8 = p7.subSymptoms.find(sx => '' + sx.id === symptom.parentId),
        p9 = p8.subSymptoms.find(sx => '' + sx.id === symptom.parentId),
        p10 = p9.subSymptoms.find(sx => '' + sx.id === symptom.parentId),
        p11 = p10.subSymptoms.find(sx => '' + sx.id === symptom.parentId),
        p12 = p11.subSymptoms.find(sx => '' + sx.id === symptom.parentId),
        p13 = p12.subSymptoms.find(sx => '' + sx.id === symptom.parentId),
        p14 = p13.subSymptoms.find(sx => '' + sx.id === symptom.parentId),
        p15 = p14.subSymptoms.find(sx => '' + sx.id === symptom.parentId),
        p16 = p15.subSymptoms.find(sx => '' + sx.id === symptom.parentId),
        p17 = p16.subSymptoms.find(sx => '' + sx.id === symptom.parentId),
        p18 = p17.subSymptoms.find(sx => '' + sx.id === symptom.parentId),
        p19 = p18.subSymptoms.find(sx => '' + sx.id === symptom.parentId),
        p20 = p19.subSymptoms.find(sx => '' + sx.id === symptom.parentId);

    const parent =
        p1 ? p1 :
            p2 ? p2 :
                p3 ? p3 :
                    p4 ? p4 :
                        p5 ? p5 :
                            p6 ? p6 :
                                p7 ? p7 :
                                    p8 ? p8 :
                                        p9 ? p9 :
                                            p10 ? p10 :
                                                p11 ? p11 :
                                                    p12 ? p12 :
                                                        p13 ? p13 :
                                                            p14 ? p14 :
                                                                p15 ? p15 :
                                                                    p16 ? p16 :
                                                                        p17 ? p17 :
                                                                            p18 ? p18 :
                                                                                p19 ? p19 : p20;

    if (parent) {
      parent.subSymptoms.push(symptom);
    }
    else {
      if (!this.encounter.symptoms) {
        this.encounter.symptoms = Array<Symptom>();
      }
      this.encounter.symptoms.push(symptom);
    }

    if (!this.encounter.symptoms)
      this.encounter.symptoms = Array<Symptom>();
    this.encounter.symptoms.push(symptom);

    console.log('after adding symptom: ', this.encounter.symptoms);
    console.log('The encounter is: ', this.encounter);
  }

  removeSymptom(symptomCodeId: string): void {

    console.log('in removing symptom: ', symptomCodeId, this.encounter.symptoms);
    if (this.encounter.symptoms) {
      this.encounter.symptoms = this.encounter.symptoms.filter(x => x.codeId !== symptomCodeId);
    }
    console.log('after removing symptom: ', this.encounter.symptoms);
  }

  addHistory(hist: History): void {

    console.log('in adding history: ', this.encounter.allHistory);
    if (!this.encounter.allHistory)
      this.encounter.allHistory = Array<History>();

    this.encounter.allHistory.push(hist);

    console.log('after adding history: ', this.encounter.allHistory);
    console.log('The encounter is: ', this.encounter);
  }

  removeHistory(histCodeId: string): void {

    console.log('in removing history: ', histCodeId, this.encounter.allHistory);
    if (this.encounter.allHistory) {
      this.encounter.allHistory = this.encounter.allHistory.filter(x => x.codeId !== histCodeId);
    }
    console.log('after removing history: ', this.encounter.allHistory);
  }

  addPhysical(phys: Physical): void {

    console.log('in adding physical: ', this.encounter.physicals);
    if (!this.encounter.physicals)
      this.encounter.physicals = Array<Physical>();

    this.encounter.physicals.push(phys);

    console.log('after adding physical: ', this.encounter.physicals);
    console.log('The encounter is: ', this.encounter);
  }

  removePhysical(physCodeId: string): void {

    console.log('in removing physical: ', physCodeId, this.encounter.physicals);
    if (this.encounter.physicals) {
      this.encounter.physicals = this.encounter.physicals.filter(x => x.codeId !== physCodeId);
    }
    console.log('after removing physical: ', this.encounter.physicals);
  }

  addTest(phys: Test): void {

    console.log('in adding test: ', this.encounter.tests);
    if (!this.encounter.tests)
      this.encounter.tests = Array<Test>();

    this.encounter.tests.push(phys);

    console.log('after adding test: ', this.encounter.tests);
    console.log('The encounter is: ', this.encounter);
  }

  removeTest(testCodeId: string): void {

    console.log('in removing test: ', testCodeId, this.encounter.tests);
    if (this.encounter.tests) {
      this.encounter.tests = this.encounter.tests.filter(x => x.codeId !== testCodeId);
    }
    console.log('after removing test: ', this.encounter.tests);
  }

  addDiagnos(diag: Diagnos): void {

    console.log('in adding diagnos: ', this.encounter.diagnoses);
    if (!this.encounter.diagnoses)
      this.encounter.diagnoses = Array<Diagnos>();

    this.encounter.diagnoses.push(diag);

    console.log('after adding diagnos: ', this.encounter.diagnoses);
    console.log('The encounter is: ', this.encounter);
  }

  removeDiagnos(diagnosCodeId: string): void {

    console.log('in removing diagnos: ', diagnosCodeId, this.encounter.diagnoses);
    if (this.encounter.diagnoses) {
      this.encounter.diagnoses = this.encounter.diagnoses.filter(x => x.codeId !== diagnosCodeId);
    }
    console.log('after removing test: ', this.encounter.diagnoses);
  }

  addTherapy(rx: Therapy): void {

    console.log('in adding therapy: ', this.encounter.therapies);
    if (!this.encounter.therapies)
      this.encounter.therapies = Array<Therapy>();

    this.encounter.therapies.push(rx);

    console.log('after adding therapy: ', this.encounter.therapies);
    console.log('The encounter is: ', this.encounter);
  }

  removeTherapy(therapyCodeId: string): void {

    console.log('in removing therapy: ', therapyCodeId, this.encounter.therapies);
    if (this.encounter.therapies) {
      this.encounter.therapies = this.encounter.therapies.filter(x => x.codeId !== therapyCodeId);
    }
    console.log('after removing therapy: ', this.encounter.therapies);
  }
}

