﻿import { Injectable } from '@angular/core';

import { Therapy } from '../models/therapy';
import { HttpClient } from '@angular/common/http';

const therapies: Therapy[] = [];

const rx1 = new Therapy(1, 'Surgery', '1111', 'ICD-101');

const rx11 = new Therapy(2, 'Eyes', '11112', 'ICD-102', '1111');
rx1.subTherapies.push(rx11);

const rx12 = new Therapy(3, 'Cornea', '11113', 'ICD-103', '1111');
const rx121 = new Therapy(5, 'excision of Lesion', '111131', 'ICD-104', '11113');
rx12.subTherapies.push(rx121);
const rx122 = new Therapy(4, 'Repair of Cornes', '111132', 'ICD-105', '11113');
rx12.subTherapies.push(rx122);
const rx123 = new Therapy(6, 'Transplant', '111133', 'ICD-106', '11113');
rx12.subTherapies.push(rx123);
const rx124 = new Therapy(7, 'Removal of Epithelium', '111134', 'ICD-107', '11113');
rx12.subTherapies.push(rx124);
rx1.subTherapies.push(rx12);

const rx13 = new Therapy(8, 'Gynecologic', '11114', 'ICD-108', '1111');
rx1.subTherapies.push(rx13);

therapies.push(rx1);

const rx2 = new Therapy(9, 'Meds & Vaccines', '2222', 'ICD-202');
therapies.push(rx2);

const rx3 = new Therapy(10, 'Radiation Therapy', '3333', 'ICD-303');
therapies.push(rx3);

const rx4 = new Therapy(11, 'Nursing Care', '3333', 'ICD-404');
therapies.push(rx4);

const rx5 = new Therapy(12, 'Social Services', '5555', 'ICD-505');
therapies.push(rx5);


@Injectable()
export class TherapyService {

  constructor(private http: HttpClient) {
  }

  getTherapies(): Promise<Therapy[]> {

    return Promise.resolve(therapies);
  }

  getTherapiesCount(): number {

    return therapies.length;
  }

  addTherapy(symptom: Therapy): void {

    therapies.push(symptom);
  }

  getTherapy(id: number): Promise<Therapy> {

    console.log('Id: ', id);
    return Promise.resolve(therapies[id]);
  }

  getTher(id: number): Therapy {

    console.log('Id: ', id);

    return therapies[id];
  }

  searchTherapy(term: string): Therapy[] {

    return therapies;
  }
}
