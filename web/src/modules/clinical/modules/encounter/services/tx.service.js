"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var test_1 = require("../models/test");
var tests = [];
var tx1 = new test_1.Test(1, 'Blood Analysis', '1111', 'ICD-101');
var tx11 = new test_1.Test(2, 'Blood Drawn', '11112', 'ICD-102', '1111');
tx1.subTests.push(tx11);
var tx12 = new test_1.Test(3, 'Hematology', '11113', 'ICD-103', '1111');
var tx121 = new test_1.Test(5, 'Blood Cells Counts', '111131', 'ICD-104', '11113');
tx12.subTests.push(tx121);
var tx122 = new test_1.Test(4, 'Hemaglobin Study', '111132', 'ICD-105', '11113');
tx12.subTests.push(tx122);
var tx123 = new test_1.Test(6, 'Blood Typing', '111133', 'ICD-106', '11113');
tx12.subTests.push(tx123);
var tx124 = new test_1.Test(7, 'Blood Viscosity', '111134', 'ICD-107', '11113');
tx12.subTests.push(tx124);
tx1.subTests.push(tx12);
var tx13 = new test_1.Test(8, 'Blood Chemistry', '11114', 'ICD-108', '1111');
tx1.subTests.push(tx13);
tests.push(tx1);
var tx2 = new test_1.Test(9, 'Pathology', '2222', 'ICD-202');
tests.push(tx2);
var tx3 = new test_1.Test(10, 'Microbiology', '3333', 'ICD-303');
tests.push(tx3);
var tx4 = new test_1.Test(11, 'Genetic Analysis', '3333', 'ICD-404');
tests.push(tx4);
var tx5 = new test_1.Test(12, 'Microbiology', '5555', 'ICD-505');
tests.push(tx5);
var TestService = /** @class */ (function () {
    function TestService(http) {
        this.http = http;
    }
    TestService.prototype.getTests = function () {
        return Promise.resolve(tests);
    };
    TestService.prototype.getTestsCount = function () {
        return tests.length;
    };
    TestService.prototype.addTest = function (Test) {
        tests.push(Test);
    };
    TestService.prototype.getTest = function (id) {
        console.log('Id: ', id);
        return Promise.resolve(tests[id]);
    };
    TestService.prototype.getSympt = function (id) {
        console.log('Id: ', id);
        return tests[id];
    };
    TestService.prototype.searchTest = function (term) {
        return tests;
    };
    TestService = __decorate([
        core_1.Injectable()
    ], TestService);
    return TestService;
}());
exports.TestService = TestService;
//# sourceMappingURL=tx.service.js.map