"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var history_1 = require("../models/history");
var allHistory = [];
var hx1 = new history_1.History(1, 'Past medical history', '1111', 'ICD-101');
var hx11 = new history_1.History(2, 'Medical', '11112', 'ICD-102', '1111');
hx1.subHistory.push(hx11);
var hx12 = new history_1.History(3, 'Pregnancy history', '11113', 'ICD-103', '1111');
var hx121 = new history_1.History(4, 'Currently pregnant', '111131', 'ICD-104', '11113');
hx12.subHistory.push(hx121);
var hx122 = new history_1.History(5, 'Recent pregnance', '111132', 'ICD-105', '11113');
hx12.subHistory.push(hx122);
var hx123 = new history_1.History(6, 'Duration of last Pregnancy', '111133', 'ICD-106', '11113');
hx12.subHistory.push(hx123);
var hx124 = new history_1.History(7, 'Teenage Pregnancy', '111134', 'ICD-107', '11113');
hx12.subHistory.push(hx124);
hx1.subHistory.push(hx12);
var hx13 = new history_1.History(8, 'Surgical / Procedural', '11114', 'ICD-108', '1111');
hx1.subHistory.push(hx13);
allHistory.push(hx1);
var hx2 = new history_1.History(9, 'Family Medical History', '2222', 'ICD-202');
allHistory.push(hx2);
var hx3 = new history_1.History(10, 'Social History', '3333', 'ICD-303');
allHistory.push(hx3);
var HistoryService = /** @class */ (function () {
    function HistoryService(http) {
        this.http = http;
    }
    HistoryService.prototype.getAllHistory = function () {
        return Promise.resolve(allHistory);
    };
    HistoryService.prototype.getHistoryCount = function () {
        return allHistory.length;
    };
    HistoryService.prototype.addHistory = function (symptom) {
        allHistory.push(symptom);
    };
    HistoryService.prototype.getHistory = function (id) {
        console.log('Id: ', id);
        return Promise.resolve(allHistory[id]);
    };
    HistoryService.prototype.getHist = function (id) {
        console.log('Id: ', id);
        return allHistory[id];
    };
    HistoryService.prototype.searchHistory = function (term) {
        return allHistory;
    };
    HistoryService = __decorate([
        core_1.Injectable()
    ], HistoryService);
    return HistoryService;
}());
exports.HistoryService = HistoryService;
//# sourceMappingURL=hx.service.js.map