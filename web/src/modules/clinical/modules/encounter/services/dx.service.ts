﻿import { Injectable } from '@angular/core';

import { Diagnos } from '../models/diagnos';
import { HttpClient } from '@angular/common/http';

const diagnoses: Diagnos[] = [];

const dx1 = new Diagnos(1, 'Pedriatric Disorder', '1111', 'ICD-101');

const dx11 = new Diagnos(2, 'Microcephallus', '11112', 'ICD-102', '1111');
dx1.subDiagnoses.push(dx11);

const dx12 = new Diagnos(3, 'Encephalocele', '11113', 'ICD-103', '1111');
const dx121 = new Diagnos(5, 'Cranial Meningocele', '111131', 'ICD-104', '11113');
dx12.subDiagnoses.push(dx121);
const dx122 = new Diagnos(4, 'Encephalocystocele', '111132', 'ICD-105', '11113');
dx12.subDiagnoses.push(dx122);
const dx123 = new Diagnos(6, 'Encephalomyelocele', '111133', 'ICD-106', '11113');
dx12.subDiagnoses.push(dx123);
const dx124 = new Diagnos(7, 'Meningoencephlocele', '111134', 'ICD-107', '11113');
dx12.subDiagnoses.push(dx124);
dx1.subDiagnoses.push(dx12);

const dx13 = new Diagnos(8, 'Macrogyria', '11114', 'ICD-108', '1111');
dx1.subDiagnoses.push(dx13);

diagnoses.push(dx1);

const dx2 = new Diagnos(9, 'Endocrine Disorder', '2222', 'ICD-202');
diagnoses.push(dx2);

const dx3 = new Diagnos(10, 'Immunologic Disorder', '3333', 'ICD-303');
diagnoses.push(dx3);

const dx4 = new Diagnos(11, 'Injuries / Accidents', '3333', 'ICD-404');
diagnoses.push(dx4);

const dx5 = new Diagnos(12, 'Metabolic Disorder', '5555', 'ICD-505');
diagnoses.push(dx5);


@Injectable()
export class DiagnosService {

  constructor(private http: HttpClient) {
  }

  getDiagnoss(): Promise<Diagnos[]> {

    return Promise.resolve(diagnoses);
  }

  getDiagnosesCount(): number {

    return diagnoses.length;
  }

  addDiagnos(symptom: Diagnos): void {

    diagnoses.push(symptom);
  }

  getDiagnos(id: number): Promise<Diagnos> {

    console.log('Id: ', id);
    return Promise.resolve(diagnoses[id]);
  }

  getSympt(id: number): Diagnos {

    console.log('Id: ', id);

    return diagnoses[id];
  }

  searchDiagnos(term: string): Diagnos[] {

    return diagnoses;
  }
}
