"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var encounter_note_1 = require("../models/encounter-note");
var EncounterService = /** @class */ (function () {
    function EncounterService(_http) {
        this._http = _http;
        this.encounter = encounter_note_1.EncounterNote.getInstance();
    }
    EncounterService.prototype.getEncounter = function () {
        return this.encounter;
    };
    EncounterService.prototype.addSymptom = function (symptom) {
        console.log('in adding symptom: ', this.encounter.symptoms);
        var // 20 generations tree
        enco = this.encounter, p1 = enco.symptoms.find(function (sx) { return '' + sx.id === symptom.parentId; }), p2 = p1.subSymptoms.find(function (sx) { return '' + sx.id === symptom.parentId; }), p3 = p2.subSymptoms.find(function (sx) { return '' + sx.id === symptom.parentId; }), p4 = p3.subSymptoms.find(function (sx) { return '' + sx.id === symptom.parentId; }), p5 = p4.subSymptoms.find(function (sx) { return '' + sx.id === symptom.parentId; }), p6 = p5.subSymptoms.find(function (sx) { return '' + sx.id === symptom.parentId; }), p7 = p6.subSymptoms.find(function (sx) { return '' + sx.id === symptom.parentId; }), p8 = p7.subSymptoms.find(function (sx) { return '' + sx.id === symptom.parentId; }), p9 = p8.subSymptoms.find(function (sx) { return '' + sx.id === symptom.parentId; }), p10 = p9.subSymptoms.find(function (sx) { return '' + sx.id === symptom.parentId; }), p11 = p10.subSymptoms.find(function (sx) { return '' + sx.id === symptom.parentId; }), p12 = p11.subSymptoms.find(function (sx) { return '' + sx.id === symptom.parentId; }), p13 = p12.subSymptoms.find(function (sx) { return '' + sx.id === symptom.parentId; }), p14 = p13.subSymptoms.find(function (sx) { return '' + sx.id === symptom.parentId; }), p15 = p14.subSymptoms.find(function (sx) { return '' + sx.id === symptom.parentId; }), p16 = p15.subSymptoms.find(function (sx) { return '' + sx.id === symptom.parentId; }), p17 = p16.subSymptoms.find(function (sx) { return '' + sx.id === symptom.parentId; }), p18 = p17.subSymptoms.find(function (sx) { return '' + sx.id === symptom.parentId; }), p19 = p18.subSymptoms.find(function (sx) { return '' + sx.id === symptom.parentId; }), p20 = p19.subSymptoms.find(function (sx) { return '' + sx.id === symptom.parentId; });
        var parent = p1 ? p1 :
            p2 ? p2 :
                p3 ? p3 :
                    p4 ? p4 :
                        p5 ? p5 :
                            p6 ? p6 :
                                p7 ? p7 :
                                    p8 ? p8 :
                                        p9 ? p9 :
                                            p10 ? p10 :
                                                p11 ? p11 :
                                                    p12 ? p12 :
                                                        p13 ? p13 :
                                                            p14 ? p14 :
                                                                p15 ? p15 :
                                                                    p16 ? p16 :
                                                                        p17 ? p17 :
                                                                            p18 ? p18 :
                                                                                p19 ? p19 : p20;
        if (parent) {
            parent.subSymptoms.push(symptom);
        }
        else {
            if (!this.encounter.symptoms) {
                this.encounter.symptoms = Array();
            }
            this.encounter.symptoms.push(symptom);
        }
        if (!this.encounter.symptoms)
            this.encounter.symptoms = Array();
        this.encounter.symptoms.push(symptom);
        console.log('after adding symptom: ', this.encounter.symptoms);
        console.log('The encounter is: ', this.encounter);
    };
    EncounterService.prototype.removeSymptom = function (symptomCodeId) {
        console.log('in removing symptom: ', symptomCodeId, this.encounter.symptoms);
        if (this.encounter.symptoms) {
            this.encounter.symptoms = this.encounter.symptoms.filter(function (x) { return x.codeId !== symptomCodeId; });
        }
        console.log('after removing symptom: ', this.encounter.symptoms);
    };
    EncounterService.prototype.addHistory = function (hist) {
        console.log('in adding history: ', this.encounter.allHistory);
        if (!this.encounter.allHistory)
            this.encounter.allHistory = Array();
        this.encounter.allHistory.push(hist);
        console.log('after adding history: ', this.encounter.allHistory);
        console.log('The encounter is: ', this.encounter);
    };
    EncounterService.prototype.removeHistory = function (histCodeId) {
        console.log('in removing history: ', histCodeId, this.encounter.allHistory);
        if (this.encounter.allHistory) {
            this.encounter.allHistory = this.encounter.allHistory.filter(function (x) { return x.codeId !== histCodeId; });
        }
        console.log('after removing history: ', this.encounter.allHistory);
    };
    EncounterService.prototype.addPhysical = function (phys) {
        console.log('in adding physical: ', this.encounter.physicals);
        if (!this.encounter.physicals)
            this.encounter.physicals = Array();
        this.encounter.physicals.push(phys);
        console.log('after adding physical: ', this.encounter.physicals);
        console.log('The encounter is: ', this.encounter);
    };
    EncounterService.prototype.removePhysical = function (physCodeId) {
        console.log('in removing physical: ', physCodeId, this.encounter.physicals);
        if (this.encounter.physicals) {
            this.encounter.physicals = this.encounter.physicals.filter(function (x) { return x.codeId !== physCodeId; });
        }
        console.log('after removing physical: ', this.encounter.physicals);
    };
    EncounterService.prototype.addTest = function (phys) {
        console.log('in adding test: ', this.encounter.tests);
        if (!this.encounter.tests)
            this.encounter.tests = Array();
        this.encounter.tests.push(phys);
        console.log('after adding test: ', this.encounter.tests);
        console.log('The encounter is: ', this.encounter);
    };
    EncounterService.prototype.removeTest = function (testCodeId) {
        console.log('in removing test: ', testCodeId, this.encounter.tests);
        if (this.encounter.tests) {
            this.encounter.tests = this.encounter.tests.filter(function (x) { return x.codeId !== testCodeId; });
        }
        console.log('after removing test: ', this.encounter.tests);
    };
    EncounterService.prototype.addDiagnos = function (diag) {
        console.log('in adding diagnos: ', this.encounter.diagnoses);
        if (!this.encounter.diagnoses)
            this.encounter.diagnoses = Array();
        this.encounter.diagnoses.push(diag);
        console.log('after adding diagnos: ', this.encounter.diagnoses);
        console.log('The encounter is: ', this.encounter);
    };
    EncounterService.prototype.removeDiagnos = function (diagnosCodeId) {
        console.log('in removing diagnos: ', diagnosCodeId, this.encounter.diagnoses);
        if (this.encounter.diagnoses) {
            this.encounter.diagnoses = this.encounter.diagnoses.filter(function (x) { return x.codeId !== diagnosCodeId; });
        }
        console.log('after removing test: ', this.encounter.diagnoses);
    };
    EncounterService.prototype.addTherapy = function (rx) {
        console.log('in adding therapy: ', this.encounter.therapies);
        if (!this.encounter.therapies)
            this.encounter.therapies = Array();
        this.encounter.therapies.push(rx);
        console.log('after adding therapy: ', this.encounter.therapies);
        console.log('The encounter is: ', this.encounter);
    };
    EncounterService.prototype.removeTherapy = function (therapyCodeId) {
        console.log('in removing therapy: ', therapyCodeId, this.encounter.therapies);
        if (this.encounter.therapies) {
            this.encounter.therapies = this.encounter.therapies.filter(function (x) { return x.codeId !== therapyCodeId; });
        }
        console.log('after removing therapy: ', this.encounter.therapies);
    };
    EncounterService = __decorate([
        core_1.Injectable()
    ], EncounterService);
    return EncounterService;
}());
exports.EncounterService = EncounterService;
//# sourceMappingURL=encounter.service.js.map