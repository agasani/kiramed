﻿import { Injectable } from '@angular/core';

import { Test } from '../models/test';
import { HttpClient } from '@angular/common/http';

const tests: Test[] = [];

const tx1 = new Test(1, 'Blood Analysis', '1111', 'ICD-101');

const tx11 = new Test(2, 'Blood Drawn', '11112', 'ICD-102', '1111');
tx1.subTests.push(tx11);

const tx12 = new Test(3, 'Hematology', '11113', 'ICD-103', '1111');
const tx121 = new Test(5, 'Blood Cells Counts', '111131', 'ICD-104', '11113');
tx12.subTests.push(tx121);
const tx122 = new Test(4, 'Hemaglobin Study', '111132', 'ICD-105', '11113');
tx12.subTests.push(tx122);
const tx123 = new Test(6, 'Blood Typing', '111133', 'ICD-106', '11113');
tx12.subTests.push(tx123);
const tx124 = new Test(7, 'Blood Viscosity', '111134', 'ICD-107', '11113');
tx12.subTests.push(tx124);
tx1.subTests.push(tx12);

const tx13 = new Test(8, 'Blood Chemistry', '11114', 'ICD-108', '1111');
tx1.subTests.push(tx13);

tests.push(tx1);

const tx2 = new Test(9, 'Pathology', '2222', 'ICD-202');
tests.push(tx2);

const tx3 = new Test(10, 'Microbiology', '3333', 'ICD-303');
tests.push(tx3);

const tx4 = new Test(11, 'Genetic Analysis', '3333', 'ICD-404');
tests.push(tx4);

const tx5 = new Test(12, 'Microbiology', '5555', 'ICD-505');
tests.push(tx5);


@Injectable()
export class TestService {

  constructor(private http: HttpClient) {
  }

  getTests(): Promise<Test[]> {

    return Promise.resolve(tests);
  }

  getTestsCount(): number {

    return tests.length;
  }

  addTest(Test: Test): void {

    tests.push(Test);
  }

  getTest(id: number): Promise<Test> {

    console.log('Id: ', id);
    return Promise.resolve(tests[id]);
  }

  getSympt(id: number): Test {

    console.log('Id: ', id);

    return tests[id];
  }

  searchTest(term: string): Test[] {

    return tests;
  }
}
