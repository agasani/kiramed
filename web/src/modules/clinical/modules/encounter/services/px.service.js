"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var physical_1 = require("../models/physical");
var physicals = [];
var px1 = new physical_1.Physical(1, 'Vital Signs', '1111', 'ICD-101');
var px11 = new physical_1.Physical(2, 'Temperature', '11112', 'ICD-102', '1111');
px1.subPhysicals.push(px11);
var px12 = new physical_1.Physical(3, 'Pulse Rate', '11113', 'ICD-103', '1111');
var px121 = new physical_1.Physical(5, 'While Standing', '111131', 'ICD-104', '11113');
px12.subPhysicals.push(px121);
var px122 = new physical_1.Physical(4, 'While Sitting', '111132', 'ICD-105', '11113');
px12.subPhysicals.push(px122);
var px123 = new physical_1.Physical(6, 'Radial', '111133', 'ICD-106', '11113');
px12.subPhysicals.push(px123);
var px124 = new physical_1.Physical(7, 'Subclavian', '111134', 'ICD-107', '11113');
px12.subPhysicals.push(px124);
px1.subPhysicals.push(px12);
var px13 = new physical_1.Physical(8, 'Blood Pressure', '11114', 'ICD-108', '1111');
px1.subPhysicals.push(px13);
physicals.push(px1);
var px2 = new physical_1.Physical(9, 'Standard Measurements', '2222', 'ICD-202');
physicals.push(px2);
var px3 = new physical_1.Physical(10, 'Head', '3333', 'ICD-303');
physicals.push(px3);
var px4 = new physical_1.Physical(11, 'Eyes', '3333', 'ICD-404');
physicals.push(px4);
var px5 = new physical_1.Physical(12, 'Neck', '5555', 'ICD-505');
physicals.push(px5);
var PhysicalService = /** @class */ (function () {
    function PhysicalService(http) {
        this.http = http;
    }
    PhysicalService.prototype.getPhysicals = function () {
        return Promise.resolve(physicals);
    };
    PhysicalService.prototype.getPhysicalsCount = function () {
        return physicals.length;
    };
    PhysicalService.prototype.addPhysical = function (symptom) {
        physicals.push(symptom);
    };
    PhysicalService.prototype.getPhysical = function (id) {
        console.log('Id: ', id);
        return Promise.resolve(physicals[id]);
    };
    PhysicalService.prototype.getPhys = function (id) {
        console.log('Id: ', id);
        return physicals[id];
    };
    PhysicalService.prototype.searchPhysical = function (term) {
        return physicals;
    };
    PhysicalService = __decorate([
        core_1.Injectable()
    ], PhysicalService);
    return PhysicalService;
}());
exports.PhysicalService = PhysicalService;
//# sourceMappingURL=px.service.js.map