﻿import { Injectable } from '@angular/core';

import { Symptom } from '../models/symptom';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

const symptoms: Symptom[] = [];

const sx1 = new Symptom(1, 'Encounter background', '1111', 'ICD-101');

const sx11 = new Symptom(2, 'Chief Complaint', '11112', 'ICD-102', '1111');
sx1.subSymptoms.push(sx11);

const sx12 = new Symptom(3, 'Visit For', '11113', 'ICD-103', '1111');

const sx121 = new Symptom(4, 'Examination', '111131', 'ICD-104', '11113');
sx12.subSymptoms.push(sx121);

const sx122 = new Symptom(5, 'Laboratory', '111132', 'ICD-105', '11113');
sx12.subSymptoms.push(sx122);

const sx123 = new Symptom(6, 'Chemotherapy', '111133', 'ICD-106', '11113');
sx12.subSymptoms.push(sx123);

const sx124 = new Symptom(7, 'Radiotherapy', '111134', 'ICD-107', '11113');
sx12.subSymptoms.push(sx124);

sx1.subSymptoms.push(sx12);

const sx13 = new Symptom(8, 'ROS free text', '11114', 'ICD-108', '1111');
sx1.subSymptoms.push(sx13);

symptoms.push(sx1);

const sx2 = new Symptom(9, 'System symptoms', '2222', 'ICD-202');
symptoms.push(sx2);

const sx3 = new Symptom(10, 'Head-related Symptoms', '3333', 'ICD-303');
symptoms.push(sx3);

const sx4 = new Symptom(11, 'Neck symptoms', '3333', 'ICD-404');
symptoms.push(sx4);

const sx5 = new Symptom(12, 'Cardiovascular symptoms', '5555', 'ICD-505');
symptoms.push(sx5);


@Injectable()
export class SymptomService {

  constructor(protected _http: HttpClient) {
    // super(_http);
  }

  getSymptoms(): Observable<Symptom[]> {

    return Observable.of(symptoms);
  }

  getSymptomsCount(): number {

    return symptoms.length;
  }

  addSymptom(symptom: Symptom): void {

    symptoms.push(symptom);
  }

  getSymptom(id: number): Observable<Symptom> {

    console.log('Id: ', id);
    return Observable.of(symptoms[id]);
  }

  getSympt(id: number): Symptom {

    console.log('Id: ', id);

    return symptoms[id];
  }

  searchSymptom(term: string): Symptom[] {

    return symptoms;
  }
}
