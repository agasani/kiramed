﻿import { Component } from '@angular/core';

import { Test } from '../models/test';
import { EncounterService } from '../services/encounter.service';
import { TestService } from '../services/tx.service';
import { BaseComponent } from '../../../../shared/components';

@Component({
  selector: 'encounter-tests',
  templateUrl: '../views/tx.html',
  styleUrls: ['../sass/encounter.scss'],
  providers: [TestService, EncounterService]
})

export class EncounterTestComponent extends BaseComponent {

  tests: Test[];
  allChecked: string[];
  textDisplay = 'Tests';

  constructor(private testService: TestService,
              private encounterService: EncounterService) {
    super();
  }

  ngOnInit(): void {
    this.tests = this.testService.searchTest('');
    this.allChecked = Array<string>();
    const reportedTests = this.encounterService.getEncounter().tests;
    for (const idx in reportedTests) {
      if (this.hasProperty(reportedTests, idx)) {
        this.allChecked.push(reportedTests[idx].codeId);
      }
    }
    console.log('Tests: ', this.tests);
  }

  ngAfterViewInit(): void {

    console.log('Tx: ', this.tests);

    try {
      $('.ui.accordion').accordion();
    } catch (e) {
      console.log('Error initiating UIs in Tx components: ', e);
    }
  }

  searchTest(codeId: string, tests: Test[], cb: any): Test {

    if (tests.length > 0) {
      for (const idx in tests) {
        if (tests[idx].codeId === codeId) {
          console.log('NM: ', tests[idx].name, 'codeId ', tests[idx].codeId);
          cb(tests[idx]);
        } else if (tests[idx].subTests.length > 0) {
          this.searchTest(codeId, tests[idx].subTests, cb);
        }
      }
    } else {
      console.log('in else');
      return;
    }
  }

  onChecked(testCodeId: string): void {

    if (!this.allChecked) {
      this.allChecked = Array<string>();
    }

    const index = this.allChecked.indexOf(testCodeId);

    if (index === -1) {
      this.allChecked.push(testCodeId);
      this.addTest(testCodeId);
    } else {
      this.removeTest(testCodeId);
      this.allChecked.splice(index, 1);
    }
  }

  addTest(testCodeId: string): void {

    const $this = this;
    this.searchTest(testCodeId, this.tests, (test: any) => {
      if (test) {
        test.checked = true;
        $this.encounterService.addTest(test);
      }
    });

  }

  removeTest(testCodeId: string): void {

    this.encounterService.removeTest(testCodeId);

  }
}
