"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({__proto__: []} instanceof Array && function (d, b) {
            d.__proto__ = b;
        }) ||
        function (d, b) {
            for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
        };
    return function (d, b) {
        extendStatics(d, b);

        function __() {
            this.constructor = d;
        }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length,
        r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", {value: true});
var core_1 = require("@angular/core");
var base_1 = require("../../../../shared/components/base");
var diagnosis_service_1 = require("../../../../api/services/diagnosis.service");
var encounter_service_1 = require("../../../../api/services/encounter.service");
var EncounterDiagnosComponent = /** @class */ (function (_super) {
    __extends(EncounterDiagnosComponent, _super);
    function EncounterDiagnosComponent(diagService, encounterService) {
        var _this = _super.call(this) || this;
        _this.diagService = diagService;
        _this.encounterService = encounterService;
        _this.textDisplay = 'Diagnosnoses, Syndroms & Conditions';
        return _this;
    }
    EncounterDiagnosComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.diagService.ApiDiagnosisSearchBySearchGet({search: ''}).subscribe(function (diagnoses) {
            return _this.diagnoses = diagnoses;
        });
        this.allChecked = Array();
        var orgId = this.settings['userOrgId'];
        // const reportedDiagnoses: DiagnosisViewModel[] = this.encounterService.ApiEncounterOrganizationIdGet(orgId); //.diagnoses
        // for (const idx in reportedDiagnoses) {
        //   if (this.hasProperty(reportedDiagnoses, idx)) {
        //     this.allChecked.push(reportedDiagnoses[idx].codingId);
        //   }
        // }
        console.log('allchecked: ', this.allChecked);
    };
    EncounterDiagnosComponent.prototype.ngAfterViewInit = function () {
        console.log('Dx: ', this.diagnoses);
        try {
            $('.ui.accordion').accordion();
        }
        catch (e) {
            console.log('Error initiating UIs in Dx components: ', e);
        }
    };
    EncounterDiagnosComponent.prototype.searchDiagnos = function (codeId, diagnoses, cb) {
        if (diagnoses.length > 0) {
            for (var idx in diagnoses) {
                if ("" + diagnoses[idx].codingId === codeId) {
                    console.log('NM: ', diagnoses[idx].name, 'codingId: ', diagnoses[idx].codingId);
                    cb(diagnoses[idx]);
                }
                // else if (diagnoses[idx].subDiagnoses.length > 0)
                //   this.searchDiagnos(codeId, diagnoses[idx].subDiagnoses, cb);
            }
        }
        else {
            console.log('in else');

        }
    };
    EncounterDiagnosComponent.prototype.onChecked = function (diagnosCodeId) {
        if (!this.allChecked)
            this.allChecked = Array();
        var index = this.allChecked.indexOf(diagnosCodeId);
        if (index === -1) {
            this.allChecked.push(diagnosCodeId);
            this.addDiagnos(diagnosCodeId);
        }
        else {
            this.removeDiagnos(diagnosCodeId);
            this.allChecked.splice(index, 1);
        }
    };
    EncounterDiagnosComponent.prototype.addDiagnos = function (diagnosCodeId) {
        // const $this = this;
        // this.searchDiagnos(diagnosCodeId, this.diagnoses, (diag: any) => {
        //   if (diag) {
        //     diag.checked = true;
        //     $this.encounterService.addDiagnos(diag);
        //   }
        // });
    };
    EncounterDiagnosComponent.prototype.removeDiagnos = function (diagnosCodeId) {
        // this.encounterService.removeDiagnos(diagnosCodeId);
    };
    EncounterDiagnosComponent = __decorate([
        core_1.Component({
            selector: 'encounter-diagnoses',
            templateUrl: '../views/dx.html',
            styleUrls: ['../sass/encounter.scss'],
            providers: [diagnosis_service_1.DiagnosisService, encounter_service_1.EncounterService]
        })
    ], EncounterDiagnosComponent);
    return EncounterDiagnosComponent;
}(base_1.BaseComponent));
exports.EncounterDiagnosComponent = EncounterDiagnosComponent;
//# sourceMappingURL=encounter-diagnos.component.js.map