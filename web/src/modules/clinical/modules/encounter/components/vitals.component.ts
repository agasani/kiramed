import { Component } from '@angular/core';
import { BaseComponent } from '../../../../shared/components/base';

@Component({
  selector: 'encounter-vitals',
  templateUrl: '../views/vx.html',
  styleUrls: ['../sass/encounter.scss'],
})

export class VitalsComponent extends BaseComponent {

  textDisplay = 'Vital Signs.';

  constructor() {
    super();
  }
}
