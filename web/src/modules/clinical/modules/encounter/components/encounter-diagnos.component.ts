﻿import { Component } from '@angular/core';
import { BaseComponent } from '../../../../shared/components/base';
import { DiagnosisViewModel } from '../../../../api/models/diagnosis-view-model';
import { DiagnosisService } from '../../../../api/services/diagnosis.service';
import { EncounterService } from '../../../../api/services/encounter.service';

@Component({
  selector: 'encounter-diagnoses',
  templateUrl: '../views/dx.html',
  styleUrls: ['../sass/encounter.scss'],
  providers: [DiagnosisService, EncounterService]
})

export class EncounterDiagnosComponent extends BaseComponent {

  diagnoses: DiagnosisViewModel[];
  allChecked: string[];
  textDisplay = 'Diagnosnoses, Syndroms & Conditions';

  constructor(private diagService: DiagnosisService,
              private encounterService: EncounterService) {
    super();
  }

  ngOnInit(): void {
    this.diagService.ApiDiagnosisSearchBySearchGet({ search: '' }).subscribe(diagnoses => this.diagnoses = diagnoses);
    this.allChecked = Array<string>();
    let orgId = this.settings['userOrgId'];
    // const reportedDiagnoses: DiagnosisViewModel[] = this.encounterService.ApiEncounterOrganizationIdGet(orgId); //.diagnoses
    // for (const idx in reportedDiagnoses) {
    //   if (this.hasProperty(reportedDiagnoses, idx)) {
    //     this.allChecked.push(reportedDiagnoses[idx].codingId);
    //   }
    // }
    console.log('allchecked: ', this.allChecked);
  }

  ngAfterViewInit(): void {

    console.log('Dx: ', this.diagnoses);

    try {

      $('.ui.accordion').accordion();

    } catch (e) {
      console.log('Error initiating UIs in Dx components: ', e);
    }
  }

  searchDiagnos(codeId: string, diagnoses: DiagnosisViewModel[], cb: any): DiagnosisViewModel {

    if (diagnoses.length > 0) {
      for (const idx in diagnoses) {
        if (`${diagnoses[idx].codingId}` === codeId) {
          console.log('NM: ', diagnoses[idx].name, 'codingId: ', diagnoses[idx].codingId);
          cb(diagnoses[idx]);
        }
        // else if (diagnoses[idx].subDiagnoses.length > 0)
        //   this.searchDiagnos(codeId, diagnoses[idx].subDiagnoses, cb);
      }
    } else {
      console.log('in else');
      return;
    }
  }

  onChecked(diagnosCodeId: string): void {

    if (!this.allChecked)
      this.allChecked = Array<string>();

    const index = this.allChecked.indexOf(diagnosCodeId);

    if (index === -1) {
      this.allChecked.push(diagnosCodeId);
      this.addDiagnos(diagnosCodeId);
    } else {
      this.removeDiagnos(diagnosCodeId);
      this.allChecked.splice(index, 1);
    }
  }

  addDiagnos(diagnosCodeId: string): void {

    // const $this = this;
    // this.searchDiagnos(diagnosCodeId, this.diagnoses, (diag: any) => {
    //   if (diag) {
    //     diag.checked = true;
    //     $this.encounterService.addDiagnos(diag);
    //   }
    // });

  }

  removeDiagnos(diagnosCodeId: string): void {

    // this.encounterService.removeDiagnos(diagnosCodeId);

  }
}
