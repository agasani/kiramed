﻿import { Component } from '@angular/core';

import { EncounterNote } from '../models/encounter-note';
import { EncounterService } from '../services/encounter.service';
import { BaseComponent } from '../../../../shared/components';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'encounter-note',
  templateUrl: '../views/encounter_note.html',
  styleUrls: ['../sass/encounter.scss'],
  providers: [EncounterService]
})

export class EncounterNoteComponent extends BaseComponent {

  note: EncounterNote;
  patientId: string;

  constructor(private encounterService: EncounterService) {
    super();
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngOnInit(): void {
    console.log('initing Hx...');
    this.patientId = localStorage.getItem('patientId');
    this.note = this.encounterService.getEncounter();
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngAfterViewInit(): void {

    console.log('Encounter Note: ', this.note);

  }

  saveNote(): void {

    console.log('Here I save the patient note...');

    this.router.navigate(['/patient-view', this.patientId]);
  }
}
