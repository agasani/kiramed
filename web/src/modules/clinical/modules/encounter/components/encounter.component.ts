﻿import { AfterViewInit, Component } from '@angular/core';
import { HasStepsComponent } from '../../../../shared/components/steps-nav/has-steps.component';

@Component({
  selector: 'clinical-encounter',
  templateUrl: '../views/encounter.html',
  styleUrls: ['../sass/encounter.scss']
})

export class EncounterComponent extends HasStepsComponent implements AfterViewInit {

  constructor() {
    super();
    this.steps = [
      { 'title': 'Vitals', 'descr': 'Patient vital signs', 'icon': 'treatment', 'url': '/new-encounter/vx' },
      { 'title': 'Symptoms', 'descr': 'Patient\'s Symptoms', 'icon': 'warning', 'url': '/new-encounter/sx' },
      { 'title': 'History', 'descr': 'Illness History', 'icon': 'folder open outline', 'url': '/new-encounter/hx' },
      { 'title': 'Physical', 'descr': 'Phisical Exam.', 'icon': 'unhide', 'url': '/new-encounter/px' },
      { 'title': 'Tests', 'descr': 'Order Tests', 'icon': 'help', 'url': '/new-encounter/tx' },
      { 'title': 'Diagnosis', 'descr': 'Assessment', 'icon': 'doctor', 'url': '/new-encounter/dx' },
      { 'title': 'Therapy', 'descr': 'Patient Therapy', 'icon': 'first aid', 'url': '/new-encounter/rx' },
    ];
    this.review = { 'title': 'Review Note', 'icon': 'file text outline', 'url': '/new-encounter/note' };
    this.head = { 'title': 'Encounter', 'icon': 'ellipsis vertical' };
    this.nav = { 'steps': this.steps, 'review': this.review, 'head': this.head, 'chackable': false };
  }

  ngAfterViewInit(): void {
    try {
      $('.ui.dropdown').dropdown();
    } catch (e) {
      console.log('Error initiating UIs in Encounter components: ', e);
    }
  }
}
