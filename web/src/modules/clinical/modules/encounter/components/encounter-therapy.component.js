"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({__proto__: []} instanceof Array && function (d, b) {
            d.__proto__ = b;
        }) ||
        function (d, b) {
            for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
        };
    return function (d, b) {
        extendStatics(d, b);

        function __() {
            this.constructor = d;
        }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length,
        r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", {value: true});
var core_1 = require("@angular/core");
var encounter_service_1 = require("../services/encounter.service");
var rx_service_1 = require("../services/rx.service");
var base_1 = require("../../../../shared/components/base");
var EncounterTherapyComponent = /** @class */ (function (_super) {
    __extends(EncounterTherapyComponent, _super);
    function EncounterTherapyComponent(therapyService, encounterService) {
        var _this = _super.call(this) || this;
        _this.therapyService = therapyService;
        _this.encounterService = encounterService;
        _this.textDisplay = 'Therapy';
        return _this;
    }
    EncounterTherapyComponent.prototype.ngOnInit = function () {
        this.therapies = this.therapyService.searchTherapy('');
        this.allChecked = Array();
        var reportedTherapys = this.encounterService.getEncounter().therapies;
        for (var idx in reportedTherapys) {
            if (this.hasProperty(reportedTherapys, idx)) {
                this.allChecked.push(reportedTherapys[idx].codeId);
            }
        }
        console.log('allchecked: ', this.allChecked);
    };
    EncounterTherapyComponent.prototype.ngAfterViewInit = function () {
        console.log('Rx: ', this.therapies);
        try {
            $('.ui.accordion').accordion();
        }
        catch (e) {
            console.log('Error initiating UIs in Rx components: ', e);
        }
    };
    EncounterTherapyComponent.prototype.searchTherapy = function (codeId, therapies, cb) {
        if (therapies.length > 0) {
            for (var idx in therapies) {
                if (therapies[idx].codeId === codeId) {
                    console.log('NM: ', therapies[idx].name, 'codeId ', therapies[idx].codeId);
                    cb(therapies[idx]);
                }
                else if (therapies[idx].subTherapies.length > 0)
                    this.searchTherapy(codeId, therapies[idx].subTherapies, cb);
            }
        }
        else {
            console.log('in else');

        }
    };
    EncounterTherapyComponent.prototype.onChecked = function (therapyCodeId) {
        if (!this.allChecked)
            this.allChecked = Array();
        var index = this.allChecked.indexOf(therapyCodeId);
        if (index === -1) {
            this.allChecked.push(therapyCodeId);
            this.addTherapy(therapyCodeId);
        }
        else {
            this.removeTherapy(therapyCodeId);
            this.allChecked.splice(index, 1);
        }
    };
    EncounterTherapyComponent.prototype.addTherapy = function (therapyCodeId) {
        var $this = this;
        this.searchTherapy(therapyCodeId, this.therapies, function (ther) {
            if (ther) {
                ther.checked = true;
                $this.encounterService.addTherapy(ther);
            }
        });
    };
    EncounterTherapyComponent.prototype.removeTherapy = function (therapyCodeId) {
        this.encounterService.removeTherapy(therapyCodeId);
    };
    EncounterTherapyComponent = __decorate([
        core_1.Component({
            selector: 'encounter-therapies',
            templateUrl: '../views/rx.html',
            styleUrls: ['../sass/encounter.scss'],
            providers: [rx_service_1.TherapyService, encounter_service_1.EncounterService]
        })
    ], EncounterTherapyComponent);
    return EncounterTherapyComponent;
}(base_1.BaseComponent));
exports.EncounterTherapyComponent = EncounterTherapyComponent;
//# sourceMappingURL=encounter-therapy.component.js.map