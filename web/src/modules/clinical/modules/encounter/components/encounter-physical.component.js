"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var encounter_service_1 = require("../services/encounter.service");
var px_service_1 = require("../services/px.service");
var components_1 = require("../../../../shared/components");
var EncounterPhysicalComponent = /** @class */ (function (_super) {
    __extends(EncounterPhysicalComponent, _super);
    function EncounterPhysicalComponent(physicalService, encounterService) {
        var _this = _super.call(this) || this;
        _this.physicalService = physicalService;
        _this.encounterService = encounterService;
        _this.textDisplay = 'Physical Examination';
        return _this;
    }
    EncounterPhysicalComponent.prototype.ngOnInit = function () {
        this.physicals = this.physicalService.searchPhysical('');
        this.allChecked = Array();
        var reportedPhysicals = this.encounterService.getEncounter().physicals;
        for (var idx in reportedPhysicals) {
            if (this.hasProperty(reportedPhysicals, idx)) {
                this.allChecked.push(reportedPhysicals[idx].codeId);
            }
        }
        console.log('allchecked: ', this.allChecked);
    };
    EncounterPhysicalComponent.prototype.ngAfterViewInit = function () {
        console.log('Px: ', this.physicals);
        try {
            $('.ui.accordion').accordion();
        }
        catch (e) {
            console.log('Error initiating UIs in Px components: ', e);
        }
    };
    EncounterPhysicalComponent.prototype.searchPhysical = function (codeId, physicals, cb) {
        if (physicals.length > 0) {
            for (var idx in physicals) {
                if (physicals[idx].codeId === codeId) {
                    console.log('NM: ', physicals[idx].name, 'codeId ', physicals[idx].codeId);
                    cb(physicals[idx]);
                }
                else if (physicals[idx].subPhysicals.length > 0)
                    this.searchPhysical(codeId, physicals[idx].subPhysicals, cb);
            }
        }
        else {
            console.log('in else');
            return;
        }
    };
    EncounterPhysicalComponent.prototype.onChecked = function (physCodeId) {
        if (!this.allChecked)
            this.allChecked = Array();
        var index = this.allChecked.indexOf(physCodeId);
        if (index === -1) {
            this.allChecked.push(physCodeId);
            this.addPhysical(physCodeId);
        }
        else {
            this.removePhysical(physCodeId);
            this.allChecked.splice(index, 1);
        }
    };
    EncounterPhysicalComponent.prototype.addPhysical = function (physCodeId) {
        var $this = this;
        this.searchPhysical(physCodeId, this.physicals, function (phys) {
            if (phys) {
                phys.checked = true;
                $this.encounterService.addPhysical(phys);
            }
        });
    };
    EncounterPhysicalComponent.prototype.removePhysical = function (physCodeId) {
        this.encounterService.removePhysical(physCodeId);
    };
    EncounterPhysicalComponent = __decorate([
        core_1.Component({
            selector: 'encounter-physs',
            templateUrl: '../views/px.html',
            styleUrls: ['../sass/encounter.scss'],
            providers: [px_service_1.PhysicalService, encounter_service_1.EncounterService]
        })
    ], EncounterPhysicalComponent);
    return EncounterPhysicalComponent;
}(components_1.BaseComponent));
exports.EncounterPhysicalComponent = EncounterPhysicalComponent;
//# sourceMappingURL=encounter-physical.component.js.map