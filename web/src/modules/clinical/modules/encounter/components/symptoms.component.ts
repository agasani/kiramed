﻿import { Component } from '@angular/core';

import { Symptom } from '../models/symptom';
import { EncounterService } from '../services/encounter.service';
import { SymptomService } from '../services/sx.service';
import { BaseComponent } from '../../../../shared/components';

@Component({
  selector: 'encounter-symptoms',
  templateUrl: '../views/sx.html',
  styleUrls: ['../sass/encounter.scss'],
  providers: [SymptomService, EncounterService]
})

export class SymptomsComponent extends BaseComponent {

  symptoms: Symptom[];
  allChecked: string[];
  textDisplay = 'Symptoms';

  constructor(private symptomService: SymptomService,
              private encounterService: EncounterService) {
    super();
  }

  ngOnInit(): void {

    this.symptoms = this.symptomService.searchSymptom('');
    this.allChecked = Array<string>();

    const reportedSymptoms = this.encounterService.getEncounter().symptoms;

    for (const idx in reportedSymptoms) {
      if (this.hasProperty(reportedSymptoms, idx)) {
        this.allChecked.push(reportedSymptoms[idx].codeId);
      }
    }
    console.log('allchecked: ', this.allChecked);
  }

  ngAfterViewInit(): void {

    console.log('Sx: ', this.symptoms);

    try {
      $('.ui.accordion').accordion();

    } catch (e) {
      console.log('Error initiating UIs in Sx components: ', e);
    }
  }

  searchSymptom(codeId: string, symptoms: Symptom[], cb: any): Symptom {

    if (symptoms.length > 0) {
      for (const idx in symptoms) {
        if (symptoms[idx].codeId === codeId) {
          console.log('NM: ', symptoms[idx].name, 'codeId ', symptoms[idx].codeId);
          cb(symptoms[idx]);
        } else if (symptoms[idx].subSymptoms.length > 0) {
          this.searchSymptom(codeId, symptoms[idx].subSymptoms, cb);
        }
      }
    } else {
      console.log('in else');
      return;
    }
  }

  onChecked(symptomCodeId: string): void {

    if (!this.allChecked)
      this.allChecked = Array<string>();

    const index = this.allChecked.indexOf(symptomCodeId);

    console.log('Index: ', index);

    if (index === -1) {
      this.addSymptom(symptomCodeId);
      this.allChecked.push(symptomCodeId);
    } else {
      this.removeSymptom(symptomCodeId);
      this.allChecked.splice(index, 1);
    }
  }

  addSymptom(symptomCodeId: string): void {

    const $this = this;
    this.searchSymptom(symptomCodeId, this.symptoms, (sympt: any) => {
      if (sympt) {
        sympt.checked = true;
        $this.encounterService.addSymptom(sympt);
      }
    });

  }

  removeSymptom(symptomCodeId: string): void {

    this.encounterService.removeSymptom(symptomCodeId);
  }
}
