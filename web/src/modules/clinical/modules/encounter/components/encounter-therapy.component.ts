﻿import { Component } from '@angular/core';

import { Therapy } from '../models/therapy';
import { EncounterService } from '../services/encounter.service';
import { TherapyService } from '../services/rx.service';
import { BaseComponent } from '../../../../shared/components/base';

@Component({
  selector: 'encounter-therapies',
  templateUrl: '../views/rx.html',
  styleUrls: ['../sass/encounter.scss'],
  providers: [TherapyService, EncounterService]
})

export class EncounterTherapyComponent extends BaseComponent {

  therapies: Therapy[];
  allChecked: string[];
  textDisplay = 'Therapy';

  constructor(private therapyService: TherapyService,
              private encounterService: EncounterService) {
    super();
  }

  ngOnInit(): void {
    this.therapies = this.therapyService.searchTherapy('');
    this.allChecked = Array<string>();
    const reportedTherapys = this.encounterService.getEncounter().therapies;
    for (const idx in reportedTherapys) {
      if (this.hasProperty(reportedTherapys, idx)) {
        this.allChecked.push(reportedTherapys[idx].codeId);
      }
    }
    console.log('allchecked: ', this.allChecked);
  }

  ngAfterViewInit(): void {

    console.log('Rx: ', this.therapies);

    try {

      $('.ui.accordion').accordion();

    } catch (e) {
      console.log('Error initiating UIs in Rx components: ', e);
    }
  }

  searchTherapy(codeId: string, therapies: Therapy[], cb: any): Therapy {

    if (therapies.length > 0) {
      for (const idx in therapies) {
        if (therapies[idx].codeId === codeId) {
          console.log('NM: ', therapies[idx].name, 'codeId ', therapies[idx].codeId);
          cb(therapies[idx]);
        } else if (therapies[idx].subTherapies.length > 0)
          this.searchTherapy(codeId, therapies[idx].subTherapies, cb);
      }
    } else {
      console.log('in else');
      return;
    }
  }

  onChecked(therapyCodeId: string): void {

    if (!this.allChecked)
      this.allChecked = Array<string>();

    const index = this.allChecked.indexOf(therapyCodeId);

    if (index === -1) {
      this.allChecked.push(therapyCodeId);
      this.addTherapy(therapyCodeId);
    } else {
      this.removeTherapy(therapyCodeId);
      this.allChecked.splice(index, 1);
    }
  }

  addTherapy(therapyCodeId: string): void {

    const $this = this;
    this.searchTherapy(therapyCodeId, this.therapies, function (ther: any) {
      if (ther) {
        ther.checked = true;
        $this.encounterService.addTherapy(ther);
      }
    });

  }

  removeTherapy(therapyCodeId: string): void {

    this.encounterService.removeTherapy(therapyCodeId);

  }
}
