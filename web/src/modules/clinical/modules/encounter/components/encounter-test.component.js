"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({__proto__: []} instanceof Array && function (d, b) {
            d.__proto__ = b;
        }) ||
        function (d, b) {
            for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
        };
    return function (d, b) {
        extendStatics(d, b);

        function __() {
            this.constructor = d;
        }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length,
        r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", {value: true});
var core_1 = require("@angular/core");
var encounter_service_1 = require("../services/encounter.service");
var tx_service_1 = require("../services/tx.service");
var components_1 = require("../../../../shared/components");
var EncounterTestComponent = /** @class */ (function (_super) {
    __extends(EncounterTestComponent, _super);
    function EncounterTestComponent(testService, encounterService) {
        var _this = _super.call(this) || this;
        _this.testService = testService;
        _this.encounterService = encounterService;
        _this.textDisplay = 'Tests';
        return _this;
    }
    EncounterTestComponent.prototype.ngOnInit = function () {
        this.tests = this.testService.searchTest('');
        this.allChecked = Array();
        var reportedTests = this.encounterService.getEncounter().tests;
        for (var idx in reportedTests) {
            if (this.hasProperty(reportedTests, idx)) {
                this.allChecked.push(reportedTests[idx].codeId);
            }
        }
        console.log('Tests: ', this.tests);
    };
    EncounterTestComponent.prototype.ngAfterViewInit = function () {
        console.log('Tx: ', this.tests);
        try {
            $('.ui.accordion').accordion();
        }
        catch (e) {
            console.log('Error initiating UIs in Tx components: ', e);
        }
    };
    EncounterTestComponent.prototype.searchTest = function (codeId, tests, cb) {
        if (tests.length > 0) {
            for (var idx in tests) {
                if (tests[idx].codeId === codeId) {
                    console.log('NM: ', tests[idx].name, 'codeId ', tests[idx].codeId);
                    cb(tests[idx]);
                }
                else if (tests[idx].subTests.length > 0) {
                    this.searchTest(codeId, tests[idx].subTests, cb);
                }
            }
        }
        else {
            console.log('in else');

        }
    };
    EncounterTestComponent.prototype.onChecked = function (testCodeId) {
        if (!this.allChecked) {
            this.allChecked = Array();
        }
        var index = this.allChecked.indexOf(testCodeId);
        if (index === -1) {
            this.allChecked.push(testCodeId);
            this.addTest(testCodeId);
        }
        else {
            this.removeTest(testCodeId);
            this.allChecked.splice(index, 1);
        }
    };
    EncounterTestComponent.prototype.addTest = function (testCodeId) {
        var $this = this;
        this.searchTest(testCodeId, this.tests, function (test) {
            if (test) {
                test.checked = true;
                $this.encounterService.addTest(test);
            }
        });
    };
    EncounterTestComponent.prototype.removeTest = function (testCodeId) {
        this.encounterService.removeTest(testCodeId);
    };
    EncounterTestComponent = __decorate([
        core_1.Component({
            selector: 'encounter-tests',
            templateUrl: '../views/tx.html',
            styleUrls: ['../sass/encounter.scss'],
            providers: [tx_service_1.TestService, encounter_service_1.EncounterService]
        })
    ], EncounterTestComponent);
    return EncounterTestComponent;
}(components_1.BaseComponent));
exports.EncounterTestComponent = EncounterTestComponent;
//# sourceMappingURL=encounter-test.component.js.map