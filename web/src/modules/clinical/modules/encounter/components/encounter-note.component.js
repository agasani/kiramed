"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var encounter_service_1 = require("../services/encounter.service");
var components_1 = require("../../../../shared/components");
var EncounterNoteComponent = /** @class */ (function (_super) {
    __extends(EncounterNoteComponent, _super);
    function EncounterNoteComponent(encounterService) {
        var _this = _super.call(this) || this;
        _this.encounterService = encounterService;
        return _this;
    }
    // tslint:disable-next-line:use-life-cycle-interface
    EncounterNoteComponent.prototype.ngOnInit = function () {
        console.log('initing Hx...');
        this.patientId = localStorage.getItem('patientId');
        this.note = this.encounterService.getEncounter();
    };
    // tslint:disable-next-line:use-life-cycle-interface
    EncounterNoteComponent.prototype.ngAfterViewInit = function () {
        console.log('Encounter Note: ', this.note);
    };
    EncounterNoteComponent.prototype.saveNote = function () {
        console.log('Here I save the patient note...');
        this.router.navigate(['/patient-view', this.patientId]);
    };
    EncounterNoteComponent = __decorate([
        core_1.Component({
            // tslint:disable-next-line:component-selector
            selector: 'encounter-note',
            templateUrl: '../views/encounter_note.html',
            styleUrls: ['../sass/encounter.scss'],
            providers: [encounter_service_1.EncounterService]
        })
    ], EncounterNoteComponent);
    return EncounterNoteComponent;
}(components_1.BaseComponent));
exports.EncounterNoteComponent = EncounterNoteComponent;
//# sourceMappingURL=encounter-note.component.js.map