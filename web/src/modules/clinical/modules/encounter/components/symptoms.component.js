"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({__proto__: []} instanceof Array && function (d, b) {
            d.__proto__ = b;
        }) ||
        function (d, b) {
            for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
        };
    return function (d, b) {
        extendStatics(d, b);

        function __() {
            this.constructor = d;
        }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length,
        r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", {value: true});
var core_1 = require("@angular/core");
var encounter_service_1 = require("../services/encounter.service");
var sx_service_1 = require("../services/sx.service");
var components_1 = require("../../../../shared/components");
var SymptomsComponent = /** @class */ (function (_super) {
    __extends(SymptomsComponent, _super);
    function SymptomsComponent(symptomService, encounterService) {
        var _this = _super.call(this) || this;
        _this.symptomService = symptomService;
        _this.encounterService = encounterService;
        _this.textDisplay = 'Symptoms';
        return _this;
    }
    SymptomsComponent.prototype.ngOnInit = function () {
        this.symptoms = this.symptomService.searchSymptom('');
        this.allChecked = Array();
        var reportedSymptoms = this.encounterService.getEncounter().symptoms;
        for (var idx in reportedSymptoms) {
            if (this.hasProperty(reportedSymptoms, idx)) {
                this.allChecked.push(reportedSymptoms[idx].codeId);
            }
        }
        console.log('allchecked: ', this.allChecked);
    };
    SymptomsComponent.prototype.ngAfterViewInit = function () {
        console.log('Sx: ', this.symptoms);
        try {
            $('.ui.accordion').accordion();
        }
        catch (e) {
            console.log('Error initiating UIs in Sx components: ', e);
        }
    };
    SymptomsComponent.prototype.searchSymptom = function (codeId, symptoms, cb) {
        if (symptoms.length > 0) {
            for (var idx in symptoms) {
                if (symptoms[idx].codeId === codeId) {
                    console.log('NM: ', symptoms[idx].name, 'codeId ', symptoms[idx].codeId);
                    cb(symptoms[idx]);
                }
                else if (symptoms[idx].subSymptoms.length > 0) {
                    this.searchSymptom(codeId, symptoms[idx].subSymptoms, cb);
                }
            }
        }
        else {
            console.log('in else');

        }
    };
    SymptomsComponent.prototype.onChecked = function (symptomCodeId) {
        if (!this.allChecked)
            this.allChecked = Array();
        var index = this.allChecked.indexOf(symptomCodeId);
        console.log('Index: ', index);
        if (index === -1) {
            this.addSymptom(symptomCodeId);
            this.allChecked.push(symptomCodeId);
        }
        else {
            this.removeSymptom(symptomCodeId);
            this.allChecked.splice(index, 1);
        }
    };
    SymptomsComponent.prototype.addSymptom = function (symptomCodeId) {
        var $this = this;
        this.searchSymptom(symptomCodeId, this.symptoms, function (sympt) {
            if (sympt) {
                sympt.checked = true;
                $this.encounterService.addSymptom(sympt);
            }
        });
    };
    SymptomsComponent.prototype.removeSymptom = function (symptomCodeId) {
        this.encounterService.removeSymptom(symptomCodeId);
    };
    SymptomsComponent = __decorate([
        core_1.Component({
            selector: 'encounter-symptoms',
            templateUrl: '../views/sx.html',
            styleUrls: ['../sass/encounter.scss'],
            providers: [sx_service_1.SymptomService, encounter_service_1.EncounterService]
        })
    ], SymptomsComponent);
    return SymptomsComponent;
}(components_1.BaseComponent));
exports.SymptomsComponent = SymptomsComponent;
//# sourceMappingURL=symptoms.component.js.map