"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({__proto__: []} instanceof Array && function (d, b) {
            d.__proto__ = b;
        }) ||
        function (d, b) {
            for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
        };
    return function (d, b) {
        extendStatics(d, b);

        function __() {
            this.constructor = d;
        }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length,
        r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", {value: true});
var core_1 = require("@angular/core");
var encounter_service_1 = require("../services/encounter.service");
var hx_service_1 = require("../services/hx.service");
var base_1 = require("../../../../shared/components/base");
var HistoryComponent = /** @class */ (function (_super) {
    __extends(HistoryComponent, _super);
    function HistoryComponent(historyService, encounterService) {
        var _this = _super.call(this) || this;
        _this.historyService = historyService;
        _this.encounterService = encounterService;
        _this.textDisplay = 'History';
        return _this;
    }
    HistoryComponent.prototype.ngOnInit = function () {
        this.allHistory = this.historyService.searchHistory('');
        this.allChecked = Array();
        var reportedHistory = this.encounterService.getEncounter().allHistory;
        for (var idx in reportedHistory) {
            if (this.hasProperty(reportedHistory, idx)) {
                this.allChecked.push(reportedHistory[idx].codeId);
            }
        }
        console.log('allchecked: ', this.allChecked);
    };
    HistoryComponent.prototype.ngAfterViewInit = function () {
        $('#new-encounter').removeClass('loading');
        console.log('Hx: ', this.allHistory);
        try {
            $('.ui.accordion').accordion();
        }
        catch (e) {
            console.log('Error initiating UIs in Hx components: ', e);
        }
    };
    HistoryComponent.prototype.searchHistory = function (codeId, amastory, cb) {
        if (amastory.length > 0) {
            for (var idx in amastory) {
                if (amastory[idx].codeId === codeId) {
                    cb(amastory[idx]);
                }
                else if (amastory[idx].subHistory.length > 0) {
                    this.searchHistory(codeId, amastory[idx].subHistory, cb);
                }
            }
        }
        else {
            console.log('in else');

        }
    };
    HistoryComponent.prototype.onChecked = function (histCodeId) {
        if (!this.allChecked) {
            this.allChecked = Array();
        }
        var index = this.allChecked.indexOf(histCodeId);
        if (index === -1) {
            this.addHistory(histCodeId);
            this.allChecked.push(histCodeId);
        }
        else {
            this.removeHistory(histCodeId);
            this.allChecked.splice(index, 1);
        }
    };
    HistoryComponent.prototype.addHistory = function (histCodeId) {
        var $this = this;
        this.searchHistory(histCodeId, this.allHistory, function (hist) {
            if (hist) {
                console.log('got the story: ', hist);
                hist.checked = true;
                $this.encounterService.addHistory(hist);
            }
        });
    };
    HistoryComponent.prototype.removeHistory = function (histCodeId) {
        var hist = this.allHistory.find(function (x) {
            return x.codeId === histCodeId;
        });
        if (hist) {
            hist.checked = false;
        }
        this.encounterService.removeHistory(histCodeId);
    };
    HistoryComponent = __decorate([
        core_1.Component({
            selector: 'encounter-history',
            templateUrl: '../views/hx.html',
            styleUrls: ['../sass/encounter.scss'],
            providers: [hx_service_1.HistoryService, encounter_service_1.EncounterService]
        })
    ], HistoryComponent);
    return HistoryComponent;
}(base_1.BaseComponent));
exports.HistoryComponent = HistoryComponent;
//# sourceMappingURL=history.component.js.map