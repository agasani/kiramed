﻿import { Component } from '@angular/core';

import { Physical } from '../models/physical';
import { EncounterService } from '../services/encounter.service';
import { PhysicalService } from '../services/px.service';
import { BaseComponent } from '../../../../shared/components';

@Component({
  selector: 'encounter-physs',
  templateUrl: '../views/px.html',
  styleUrls: ['../sass/encounter.scss'],
  providers: [PhysicalService, EncounterService]
})

export class EncounterPhysicalComponent extends BaseComponent {

  physicals: Physical[];
  allChecked: string[];
  textDisplay = 'Physical Examination';

  constructor(private physicalService: PhysicalService,
              private encounterService: EncounterService) {
    super();
  }

  ngOnInit(): void {
    this.physicals = this.physicalService.searchPhysical('');
    this.allChecked = Array<string>();
    const reportedPhysicals = this.encounterService.getEncounter().physicals;
    for (const idx in reportedPhysicals) {
      if (this.hasProperty(reportedPhysicals, idx)) {
        this.allChecked.push(reportedPhysicals[idx].codeId);
      }
    }
    console.log('allchecked: ', this.allChecked);
  }

  ngAfterViewInit(): void {

    console.log('Px: ', this.physicals);

    try {

      $('.ui.accordion').accordion();

    } catch (e) {
      console.log('Error initiating UIs in Px components: ', e);
    }
  }

  searchPhysical(codeId: string, physicals: Physical[], cb: any): Physical {

    if (physicals.length > 0) {
      for (const idx in physicals) {
        if (physicals[idx].codeId === codeId) {
          console.log('NM: ', physicals[idx].name, 'codeId ', physicals[idx].codeId);
          cb(physicals[idx]);
        } else if (physicals[idx].subPhysicals.length > 0)
          this.searchPhysical(codeId, physicals[idx].subPhysicals, cb);
      }
    } else {
      console.log('in else');
      return;
    }
  }

  onChecked(physCodeId: string): void {

    if (!this.allChecked)
      this.allChecked = Array<string>();

    const index = this.allChecked.indexOf(physCodeId);

    if (index === -1) {
      this.allChecked.push(physCodeId);
      this.addPhysical(physCodeId);
    } else {
      this.removePhysical(physCodeId);
      this.allChecked.splice(index, 1);
    }
  }

  addPhysical(physCodeId: string): void {

    const $this = this;
    this.searchPhysical(physCodeId, this.physicals, (phys: any) => {
      if (phys) {
        phys.checked = true;
        $this.encounterService.addPhysical(phys);
      }
    });

  }

  removePhysical(physCodeId: string): void {

    this.encounterService.removePhysical(physCodeId);

  }
}
