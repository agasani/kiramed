﻿import { Component } from '@angular/core';

import { History } from '../models/history';
import { EncounterService } from '../services/encounter.service';
import { HistoryService } from '../services/hx.service';
import { BaseComponent } from '../../../../shared/components/base';

@Component({
  selector: 'encounter-history',
  templateUrl: '../views/hx.html',
  styleUrls: ['../sass/encounter.scss'],
  providers: [HistoryService, EncounterService]
})

export class HistoryComponent extends BaseComponent {

  allHistory: History[];
  allChecked: string[];
  textDisplay = 'History';

  constructor(private historyService: HistoryService,
              private encounterService: EncounterService) {
    super();
  }

  ngOnInit(): void {

    this.allHistory = this.historyService.searchHistory('');
    this.allChecked = Array<string>();

    const reportedHistory = this.encounterService.getEncounter().allHistory;

    for (const idx in reportedHistory) {
      if (this.hasProperty(reportedHistory, idx)) {
        this.allChecked.push(reportedHistory[idx].codeId);
      }
    }
    console.log('allchecked: ', this.allChecked);
  }

  ngAfterViewInit(): void {

    $('#new-encounter').removeClass('loading');
    console.log('Hx: ', this.allHistory);

    try {

      $('.ui.accordion').accordion();

    } catch (e) {
      console.log('Error initiating UIs in Hx components: ', e);
    }
  }

  searchHistory(codeId: string, amastory: History[], cb: any): History {

    if (amastory.length > 0) {
      for (const idx in amastory) {
        if (amastory[idx].codeId === codeId) {
          cb(amastory[idx]);
        } else if (amastory[idx].subHistory.length > 0) {
          this.searchHistory(codeId, amastory[idx].subHistory, cb);
        }
      }
    } else {
      console.log('in else');
      return;
    }
  }


  onChecked(histCodeId: string): void {

    if (!this.allChecked) {
      this.allChecked = Array<string>();

    }

    const index = this.allChecked.indexOf(histCodeId);
    if (index === -1) {
      this.addHistory(histCodeId);
      this.allChecked.push(histCodeId);
    } else {
      this.removeHistory(histCodeId);
      this.allChecked.splice(index, 1);
    }
  }

  addHistory(histCodeId: string): void {

    const $this = this;
    this.searchHistory(histCodeId, this.allHistory, (hist: any) => {
          if (hist) {
            console.log('got the story: ', hist);
            hist.checked = true;
            $this.encounterService.addHistory(hist);
          }
        }
    );
  }

  removeHistory(histCodeId: string): void {

    const hist = this.allHistory.find(x => x.codeId === histCodeId);
    if (hist) {
      hist.checked = false;
    }

    this.encounterService.removeHistory(histCodeId);
  }
}
