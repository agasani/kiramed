"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var has_steps_component_1 = require("../../../../shared/components/steps-nav/has-steps.component");
var EncounterComponent = /** @class */ (function (_super) {
    __extends(EncounterComponent, _super);
    function EncounterComponent() {
        var _this = _super.call(this) || this;
        _this.steps = [
            { 'title': 'Vitals', 'descr': 'Patient vital signs', 'icon': 'treatment', 'url': '/new-encounter/vx' },
            { 'title': 'Symptoms', 'descr': 'Patient\'s Symptoms', 'icon': 'warning', 'url': '/new-encounter/sx' },
            { 'title': 'History', 'descr': 'Illness History', 'icon': 'folder open outline', 'url': '/new-encounter/hx' },
            { 'title': 'Physical', 'descr': 'Phisical Exam.', 'icon': 'unhide', 'url': '/new-encounter/px' },
            { 'title': 'Tests', 'descr': 'Order Tests', 'icon': 'help', 'url': '/new-encounter/tx' },
            { 'title': 'Diagnosis', 'descr': 'Assessment', 'icon': 'doctor', 'url': '/new-encounter/dx' },
            { 'title': 'Therapy', 'descr': 'Patient Therapy', 'icon': 'first aid', 'url': '/new-encounter/rx' },
        ];
        _this.review = { 'title': 'Review Note', 'icon': 'file text outline', 'url': '/new-encounter/note' };
        _this.head = { 'title': 'Encounter', 'icon': 'ellipsis vertical' };
        _this.nav = { 'steps': _this.steps, 'review': _this.review, 'head': _this.head, 'chackable': false };
        return _this;
    }
    EncounterComponent.prototype.ngAfterViewInit = function () {
        try {
            $('.ui.dropdown').dropdown();
        }
        catch (e) {
            console.log('Error initiating UIs in Encounter components: ', e);
        }
    };
    EncounterComponent = __decorate([
        core_1.Component({
            selector: 'clinical-encounter',
            templateUrl: '../views/encounter.html',
            styleUrls: ['../sass/encounter.scss']
        })
    ], EncounterComponent);
    return EncounterComponent;
}(has_steps_component_1.HasStepsComponent));
exports.EncounterComponent = EncounterComponent;
//# sourceMappingURL=encounter.component.js.map