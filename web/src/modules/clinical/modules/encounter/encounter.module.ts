﻿import { NgModule } from '@angular/core';

import { PatientModule } from '../../../patient/patient.module';
import { SharedModule } from '../../../shared/shared.module';

import { EncounterRoutingModule } from './encounter-routing.module';

import {
  EncounterComponent,
  EncounterDiagnosComponent,
  EncounterNoteComponent,
  EncounterPhysicalComponent,
  EncounterTestComponent,
  EncounterTherapyComponent,
  HistoryComponent,
  SymptomsComponent,
  VitalsComponent,
} from './components';


@NgModule({
  imports: [
    SharedModule,
    EncounterRoutingModule,
    PatientModule,
  ],
  declarations: [
    EncounterComponent,
    EncounterDiagnosComponent,
    EncounterNoteComponent,
    EncounterPhysicalComponent,
    EncounterTestComponent,
    EncounterTherapyComponent,
    HistoryComponent,
    SymptomsComponent,
    VitalsComponent,
  ]
})
export class EncounterModule {
}
