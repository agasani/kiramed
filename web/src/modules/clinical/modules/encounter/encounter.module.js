"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var patient_module_1 = require("../../../patient/patient.module");
var shared_module_1 = require("../../../shared/shared.module");
var encounter_routing_module_1 = require("./encounter-routing.module");
var components_1 = require("./components");
var EncounterModule = /** @class */ (function () {
    function EncounterModule() {
    }
    EncounterModule = __decorate([
        core_1.NgModule({
            imports: [
                shared_module_1.SharedModule,
                encounter_routing_module_1.EncounterRoutingModule,
                patient_module_1.PatientModule,
            ],
            declarations: [
                components_1.EncounterComponent,
                components_1.EncounterDiagnosComponent,
                components_1.EncounterNoteComponent,
                components_1.EncounterPhysicalComponent,
                components_1.EncounterTestComponent,
                components_1.EncounterTherapyComponent,
                components_1.HistoryComponent,
                components_1.SymptomsComponent,
                components_1.VitalsComponent,
            ]
        })
    ], EncounterModule);
    return EncounterModule;
}());
exports.EncounterModule = EncounterModule;
//# sourceMappingURL=encounter.module.js.map