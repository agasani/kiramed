﻿import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {
  EncounterComponent,
  EncounterDiagnosComponent,
  EncounterNoteComponent,
  EncounterPhysicalComponent,
  EncounterTestComponent,
  EncounterTherapyComponent,
  HistoryComponent,
  SymptomsComponent,
  VitalsComponent,
} from './components';

const routes: Routes = [
  {
    path: '',
    component: EncounterComponent,
    children: [
      { path: '', redirectTo: 'vx', pathMatch: 'full' },
      { path: 'vx', component: VitalsComponent },
      { path: 'sx', component: SymptomsComponent },
      { path: 'hx', component: HistoryComponent },
      { path: 'note', component: EncounterNoteComponent },
      { path: 'px', component: EncounterPhysicalComponent },
      { path: 'tx', component: EncounterTestComponent },
      { path: 'dx', component: EncounterDiagnosComponent },
      { path: 'rx', component: EncounterTherapyComponent },
      // { path: '**', redirectTo: '../../not-found-404' },
    ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class EncounterRoutingModule {
}
