﻿export class Therapy {
  id: number;
  name: string;
  code: string;
  codeId: string;
  parentId: string;
  subTherapies: Therapy[];
  checked: boolean;

  constructor(id: number, name: string, code: string, codeId: string, parentId: string = '', subRx: Therapy[] = []) {
    this.id = id;
    this.name = name;
    this.code = code;
    this.codeId = codeId;
    this.parentId = parentId;
    this.subTherapies = subRx;
    this.checked = false;
  }

}
