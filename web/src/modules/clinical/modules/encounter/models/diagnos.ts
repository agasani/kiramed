﻿export class Diagnos {

  id: number;
  name: string;
  code: string;
  codeId: string;
  parentId: string;
  subDiagnoses: Diagnos[];
  checked: boolean;

  constructor(id: number, name: string, code: string, codeId: string, parentId: string = '', subHx: Diagnos[] = []) {
    this.id = id;
    this.name = name;
    this.code = code;
    this.codeId = codeId;
    this.parentId = parentId;
    this.subDiagnoses = subHx;
    this.checked = false;
  }

}
