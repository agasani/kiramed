"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Therapy = /** @class */ (function () {
    function Therapy(id, name, code, codeId, parentId, subRx) {
        if (parentId === void 0) { parentId = ''; }
        if (subRx === void 0) { subRx = []; }
        this.id = id;
        this.name = name;
        this.code = code;
        this.codeId = codeId;
        this.parentId = parentId;
        this.subTherapies = subRx;
        this.checked = false;
    }
    return Therapy;
}());
exports.Therapy = Therapy;
//# sourceMappingURL=therapy.js.map