"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Test = /** @class */ (function () {
    function Test(id, name, code, codeId, parentId, subSx) {
        if (parentId === void 0) { parentId = ''; }
        if (subSx === void 0) { subSx = []; }
        this.id = id;
        this.name = name;
        this.code = code;
        this.codeId = codeId;
        this.parentId = parentId;
        this.subTests = subSx;
        this.checked = false;
    }
    return Test;
}());
exports.Test = Test;
//# sourceMappingURL=test.js.map