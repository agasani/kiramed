"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Diagnos = /** @class */ (function () {
    function Diagnos(id, name, code, codeId, parentId, subHx) {
        if (parentId === void 0) { parentId = ''; }
        if (subHx === void 0) { subHx = []; }
        this.id = id;
        this.name = name;
        this.code = code;
        this.codeId = codeId;
        this.parentId = parentId;
        this.subDiagnoses = subHx;
        this.checked = false;
    }
    return Diagnos;
}());
exports.Diagnos = Diagnos;
//# sourceMappingURL=diagnos.js.map