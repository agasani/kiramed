﻿export class History {

  id: number;
  name: string;
  code: string;
  codeId: string;
  parentId: string;
  subHistory: History[];
  checked: boolean;

  constructor(id: number, name: string, code: string, codeId: string, parentId: string = '', subHx: History[] = []) {
    this.id = id;
    this.name = name;
    this.code = code;
    this.codeId = codeId;
    this.parentId = parentId;
    this.subHistory = subHx;
    this.checked = false;
  }

}
