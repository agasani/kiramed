﻿export class Test {
  id: number;
  name: string;
  code: string;
  codeId: string;
  parentId: string;
  subTests: Test[];
  checked: boolean;

  constructor(id: number, name: string, code: string, codeId: string, parentId: string = '', subSx: Test[] = []) {
    this.id = id;
    this.name = name;
    this.code = code;
    this.codeId = codeId;
    this.parentId = parentId;
    this.subTests = subSx;
    this.checked = false;
  }

}
