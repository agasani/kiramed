"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var History = /** @class */ (function () {
    function History(id, name, code, codeId, parentId, subHx) {
        if (parentId === void 0) { parentId = ''; }
        if (subHx === void 0) { subHx = []; }
        this.id = id;
        this.name = name;
        this.code = code;
        this.codeId = codeId;
        this.parentId = parentId;
        this.subHistory = subHx;
        this.checked = false;
    }
    return History;
}());
exports.History = History;
//# sourceMappingURL=history.js.map