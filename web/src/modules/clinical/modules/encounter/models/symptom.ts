﻿export class Symptom {
  id: number;
  name: string;
  code: string;
  codeId: string;
  parentId: string;
  subSymptoms: Symptom[];
  checked: boolean;

  constructor(id: number, name: string, code: string, codeId: string, parentId: string = '', subSx: Symptom[] = []) {
    this.id = id;
    this.name = name;
    this.code = code;
    this.codeId = codeId;
    this.parentId = parentId;
    this.subSymptoms = subSx;
    this.checked = false;
  }

}
