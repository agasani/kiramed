﻿export class Physical {

  id: number;
  name: string;
  code: string;
  codeId: string;
  parentId: string;
  subPhysicals: Physical[];
  checked: boolean;

  constructor(id: number, name: string, code: string, codeId: string, parentId: string = '', subPx: Physical[] = []) {
    this.id = id;
    this.name = name;
    this.code = code;
    this.codeId = codeId;
    this.parentId = parentId;
    this.subPhysicals = subPx;
    this.checked = false;
  }

}
