"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var EncounterNote = /** @class */ (function () {
    function EncounterNote() {
        if (EncounterNote._encounter)
            throw new Error('Error: Instation failed: Use EncounterNode.getInstance() instead of new');
        EncounterNote._encounter = this;
    }
    EncounterNote.getInstance = function () {
        return EncounterNote._encounter;
    };
    EncounterNote._encounter = new EncounterNote();
    return EncounterNote;
}());
exports.EncounterNote = EncounterNote;
//# sourceMappingURL=encounter-note.js.map