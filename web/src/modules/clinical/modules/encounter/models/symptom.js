"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Symptom = /** @class */ (function () {
    function Symptom(id, name, code, codeId, parentId, subSx) {
        if (parentId === void 0) { parentId = ''; }
        if (subSx === void 0) { subSx = []; }
        this.id = id;
        this.name = name;
        this.code = code;
        this.codeId = codeId;
        this.parentId = parentId;
        this.subSymptoms = subSx;
        this.checked = false;
    }
    return Symptom;
}());
exports.Symptom = Symptom;
//# sourceMappingURL=symptom.js.map