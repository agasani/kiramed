"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Physical = /** @class */ (function () {
    function Physical(id, name, code, codeId, parentId, subPx) {
        if (parentId === void 0) { parentId = ''; }
        if (subPx === void 0) { subPx = []; }
        this.id = id;
        this.name = name;
        this.code = code;
        this.codeId = codeId;
        this.parentId = parentId;
        this.subPhysicals = subPx;
        this.checked = false;
    }
    return Physical;
}());
exports.Physical = Physical;
//# sourceMappingURL=physical.js.map