﻿import { Symptom } from './symptom';
import { History } from './history';
import { Physical } from './physical';
import { Test } from './test';
import { Diagnos } from './diagnos';
import { Therapy } from './therapy';


export class EncounterNote {

  private static _encounter: EncounterNote = new EncounterNote();

  symptoms: Symptom[];
  allHistory: History[];
  physicals: Physical[];
  tests: Test[];
  diagnoses: Diagnos[];
  therapies: Therapy[];

  constructor() {
    if (EncounterNote._encounter)
      throw new Error('Error: Instation failed: Use EncounterNode.getInstance() instead of new');

    EncounterNote._encounter = this;
  }

  public static getInstance(): EncounterNote {
    return EncounterNote._encounter;
  }
}
