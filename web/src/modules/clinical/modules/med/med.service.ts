﻿import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/catch';

import { Observable } from 'rxjs/Observable';
import { Inject, Injectable, InjectionToken, Optional } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';

export const API_BASE_URL = new InjectionToken<string>('API_BASE_URL');

export interface IMedService {
  getAll(): Observable<KiraResponse<string[] | null>>;

  post(value?: string | null): Observable<KiraResponse<void>>;

  get(id: number): Observable<KiraResponse<string | null>>;

  put(id: number, value?: string | null): Observable<KiraResponse<void>>;

  delete(id: number): Observable<KiraResponse<void>>;
}

@Injectable()
export class MedService implements IMedService {
  protected jsonParseReviver: ((key: string, value: any) => any) | undefined = undefined;
  private http: HttpClient;
  private baseUrl: string;

  constructor(@Inject(HttpClient) http: HttpClient, @Optional() @Inject(API_BASE_URL) baseUrl?: string) {
    this.http = http;
    this.baseUrl = baseUrl ? baseUrl : '';
  }

  getAll(): Observable<KiraResponse<string[] | null>> {
    let url_ = this.baseUrl + '/api/patients/Med';
    url_ = url_.replace(/[?&]$/, '');

    let options_: any = {
      observe: 'response',
      responseType: 'blob',
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };

    return this.http.request('get', url_, options_).flatMap((response_: any) => {
      return this.processGetAll(response_);
    }).catch((response_: any) => {
      if (response_ instanceof HttpResponse) {
        try {
          return this.processGetAll(response_);
        } catch (e) {
          return <Observable<KiraResponse<string[] | null>>><any>Observable.throw(e);
        }
      } else
        return <Observable<KiraResponse<string[] | null>>><any>Observable.throw(response_);
    });
  }

  post(value?: string | null): Observable<KiraResponse<void>> {
    let url_ = this.baseUrl + '/api/patients/Med';
    url_ = url_.replace(/[?&]$/, '');

    const content_ = JSON.stringify(value);

    let options_: any = {
      body: content_,
      observe: 'response',
      responseType: 'blob',
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    };

    return this.http.request('post', url_, options_).flatMap((response_: any) => {
      return this.processPost(response_);
    }).catch((response_: any) => {
      if (response_ instanceof HttpResponse) {
        try {
          return this.processPost(response_);
        } catch (e) {
          return <Observable<KiraResponse<void>>><any>Observable.throw(e);
        }
      } else
        return <Observable<KiraResponse<void>>><any>Observable.throw(response_);
    });
  }

  get(id: number): Observable<KiraResponse<string | null>> {
    let url_ = this.baseUrl + '/api/patients/Med/{id}';
    if (id === undefined || id === null)
      throw new Error('The parameter \'id\' must be defined.');
    url_ = url_.replace('{id}', encodeURIComponent('' + id));
    url_ = url_.replace(/[?&]$/, '');

    let options_: any = {
      observe: 'response',
      responseType: 'blob',
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };

    return this.http.request('get', url_, options_).flatMap((response_: any) => {
      return this.processGet(response_);
    }).catch((response_: any) => {
      if (response_ instanceof HttpResponse) {
        try {
          return this.processGet(response_);
        } catch (e) {
          return <Observable<KiraResponse<string | null>>><any>Observable.throw(e);
        }
      } else
        return <Observable<KiraResponse<string | null>>><any>Observable.throw(response_);
    });
  }

  put(id: number, value?: string | null): Observable<KiraResponse<void>> {
    let url_ = this.baseUrl + '/api/patients/Med/{id}';
    if (id === undefined || id === null)
      throw new Error('The parameter \'id\' must be defined.');
    url_ = url_.replace('{id}', encodeURIComponent('' + id));
    url_ = url_.replace(/[?&]$/, '');

    const content_ = JSON.stringify(value);

    let options_: any = {
      body: content_,
      observe: 'response',
      responseType: 'blob',
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    };

    return this.http.request('put', url_, options_).flatMap((response_: any) => {
      return this.processPut(response_);
    }).catch((response_: any) => {
      if (response_ instanceof HttpResponse) {
        try {
          return this.processPut(response_);
        } catch (e) {
          return <Observable<KiraResponse<void>>><any>Observable.throw(e);
        }
      } else
        return <Observable<KiraResponse<void>>><any>Observable.throw(response_);
    });
  }

  delete(id: number): Observable<KiraResponse<void>> {
    let url_ = this.baseUrl + '/api/patients/Med/{id}';
    if (id === undefined || id === null)
      throw new Error('The parameter \'id\' must be defined.');
    url_ = url_.replace('{id}', encodeURIComponent('' + id));
    url_ = url_.replace(/[?&]$/, '');

    let options_: any = {
      observe: 'response',
      responseType: 'blob',
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    };

    return this.http.request('delete', url_, options_).flatMap((response_: any) => {
      return this.processDelete(response_);
    }).catch((response_: any) => {
      if (response_ instanceof HttpResponse) {
        try {
          return this.processDelete(response_);
        } catch (e) {
          return <Observable<KiraResponse<void>>><any>Observable.throw(e);
        }
      } else
        return <Observable<KiraResponse<void>>><any>Observable.throw(response_);
    });
  }

  protected processGetAll(response: HttpResponse<Blob>): Observable<KiraResponse<string[] | null>> {
    const status = response.status;

    let _headers: any = {};
    if (response.headers) {
      for (let key of response.headers.keys()) {
        _headers[key] = response.headers.get(key);
      }
    }
    if (status === 200) {
      return blobToText(response.body).flatMap(_responseText => {
        let result200: any = null;
        let resultData200 = _responseText === '' ? null : JSON.parse(_responseText, this.jsonParseReviver);
        if (resultData200 && resultData200.constructor === Array) {
          result200 = [];
          for (let item of resultData200)
            result200.push(item);
        }
        return Observable.of(new KiraResponse(status, _headers, result200));
      });
    } else if (status !== 200 && status !== 204) {
      return blobToText(response.body).flatMap(_responseText => {
        return throwException('An unexpected server error occurred.', status, _responseText, _headers);
      });
    }
    return Observable.of<KiraResponse<string[] | null>>(new KiraResponse(status, _headers, <any>null));
  }

  protected processPost(response: HttpResponse<Blob>): Observable<KiraResponse<void>> {
    const status = response.status;

    let _headers: any = {};
    if (response.headers) {
      for (let key of response.headers.keys()) {
        _headers[key] = response.headers.get(key);
      }
    }
    if (status === 204) {
      return blobToText(response.body).flatMap(_responseText => {
        return Observable.of<KiraResponse<void>>(new KiraResponse(status, _headers, <any>null));
      });
    } else if (status !== 200 && status !== 204) {
      return blobToText(response.body).flatMap(_responseText => {
        return throwException('An unexpected server error occurred.', status, _responseText, _headers);
      });
    }
    return Observable.of<KiraResponse<void>>(new KiraResponse(status, _headers, <any>null));
  }

  protected processGet(response: HttpResponse<Blob>): Observable<KiraResponse<string | null>> {
    const status = response.status;

    let _headers: any = {};
    if (response.headers) {
      for (let key of response.headers.keys()) {
        _headers[key] = response.headers.get(key);
      }
    }
    if (status === 200) {
      return blobToText(response.body).flatMap(_responseText => {
        let result200: any = null;
        let resultData200 = _responseText === '' ? null : JSON.parse(_responseText, this.jsonParseReviver);
        result200 = resultData200 !== undefined ? resultData200 : <any>null;
        return Observable.of(new KiraResponse(status, _headers, result200));
      });
    } else if (status !== 200 && status !== 204) {
      return blobToText(response.body).flatMap(_responseText => {
        return throwException('An unexpected server error occurred.', status, _responseText, _headers);
      });
    }
    return Observable.of<KiraResponse<string | null>>(new KiraResponse(status, _headers, <any>null));
  }

  protected processPut(response: HttpResponse<Blob>): Observable<KiraResponse<void>> {
    const status = response.status;

    let _headers: any = {};
    if (response.headers) {
      for (let key of response.headers.keys()) {
        _headers[key] = response.headers.get(key);
      }
    }
    if (status === 204) {
      return blobToText(response.body).flatMap(_responseText => {
        return Observable.of<KiraResponse<void>>(new KiraResponse(status, _headers, <any>null));
      });
    } else if (status !== 200 && status !== 204) {
      return blobToText(response.body).flatMap(_responseText => {
        return throwException('An unexpected server error occurred.', status, _responseText, _headers);
      });
    }
    return Observable.of<KiraResponse<void>>(new KiraResponse(status, _headers, <any>null));
  }

  protected processDelete(response: HttpResponse<Blob>): Observable<KiraResponse<void>> {
    const status = response.status;

    let _headers: any = {};
    if (response.headers) {
      for (let key of response.headers.keys()) {
        _headers[key] = response.headers.get(key);
      }
    }
    if (status === 204) {
      return blobToText(response.body).flatMap(_responseText => {
        return Observable.of<KiraResponse<void>>(new KiraResponse(status, _headers, <any>null));
      });
    } else if (status !== 200 && status !== 204) {
      return blobToText(response.body).flatMap(_responseText => {
        return throwException('An unexpected server error occurred.', status, _responseText, _headers);
      });
    }
    return Observable.of<KiraResponse<void>>(new KiraResponse(status, _headers, <any>null));
  }
}


export class KiraResponse<TResult> {
  status: number;
  headers: { [key: string]: any; };
  result: TResult;

  constructor(status: number, headers: { [key: string]: any; }, result: TResult) {
    this.status = status;
    this.headers = headers;
    this.result = result;
  }
}

export class KiraException extends Error {
  message: string;
  status: number;
  response: string;
  headers: { [key: string]: any; };
  result: any;
  protected isKiraException = true;

  constructor(message: string, status: number, response: string, headers: { [key: string]: any; }, result: any) {
    super();

    this.message = message;
    this.status = status;
    this.response = response;
    this.headers = headers;
    this.result = result;
  }

  static isKiraException(obj: any): obj is KiraException {
    return obj.isKiraException === true;
  }
}

function throwException(message: string, status: number, response: string, headers: { [key: string]: any; }, result?: any): Observable<any> {
  return Observable.throw(new KiraException(message, status, response, headers, result));
}

function blobToText(blob: any): Observable<string> {
  return new Observable<string>((observer: any) => {
    if (!blob) {
      observer.next('');
      observer.complete();
    } else {
      let reader = new FileReader();
      reader.onload = function () {
        observer.next(this.result);
        observer.complete();
      };
      reader.readAsText(blob);
    }
  });
}
