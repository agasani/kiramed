import { TransferClient } from './transfer.service';

describe('TransferClient', () => {
  let service: TransferClient;

  beforeEach(() => {
    service = new TransferClient();
  });

  it('works', () => {
    expect(1).toEqual(2);
  });

});
