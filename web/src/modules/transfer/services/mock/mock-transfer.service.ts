﻿import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';

export class MockTransfer {
  'dateTimeCreated': Date;
  'arrivalDateTime': Date;
  'receivingOrganizationId': number;
  'relationship': number;
  'originOrganizationId': number;
  'nationalPatientId': string;
  'transferPhysicianUserId': string;
}

@Injectable()
export class MockTransferService {
  private transfersUrl = 'http://localhost:8080/transfers';

  constructor(private http: Http) {
  }

  getTransfers(): Observable<MockTransfer[]> {
    return this.http.get(this.transfersUrl)
        .map(response => response.json() as MockTransfer[])
        .catch(this.handleError);
  }

  private handleError(error: any): Observable<any> {
    console.error('An error occured', error); // for demo purposes only
    return Observable.throw(error.message || error);
  }
}
