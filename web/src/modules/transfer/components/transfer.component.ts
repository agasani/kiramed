import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { MockBackendTransfersService } from '../../mock-backend/services';
import { MockTransfer, MockTransferService } from '../services/mock/mock-transfer.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'kira-transfer',
  templateUrl: '../views/transfer.component.html',
  styleUrls: ['../sass/transfer.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [MockTransferService]
})

export class TransferComponent implements OnInit, OnDestroy {

  public outTransfers: MockTransfer[];
  public inTransfers: MockTransfer[];
  public allTransfers: MockTransfer[];
  private orgId: number;
  private mySubscription: Subscription;

  constructor(private mockBackendService: MockBackendTransfersService,
              private transferService: MockTransferService) {
    this.mockBackendService.start();
    this.orgId = 1; // Example OrgId
  }

  ngOnInit() {
    this.mySubscription = this.transferService.getTransfers().subscribe(
        transfers => {
          this.allTransfers = transfers;
          this.outTransfers = transfers.filter(t => t.originOrganizationId === this.orgId);
          this.inTransfers = transfers.filter(t => t.receivingOrganizationId === this.orgId);
        },
        error => console.error(error),
        () => {
          console.log('All transfers: ', this.allTransfers);
          console.log('Out transfers: ', this.outTransfers);
          console.log('In transfers: ', this.inTransfers);
        });
  }

  ngOnDestroy() {
    this.mySubscription.unsubscribe();
  }
}
