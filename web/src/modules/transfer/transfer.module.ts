﻿import { NgModule } from '@angular/core';
import { TransferComponent } from './components/transfer.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../shared/shared.module';

const routes: Routes = [{ path: '', component: TransferComponent }];

@NgModule({ imports: [SharedModule, RouterModule.forChild(routes)], declarations: [TransferComponent] })

export class TransferModule {
}