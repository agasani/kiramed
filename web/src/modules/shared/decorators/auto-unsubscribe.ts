// https://netbasal.com/automagically-unsubscribe-in-angular-4487e9853a88
export function AutoUnsubscribe(blackList: string[] = []) {

  return function (constructor) {
    const original = constructor.prototype.ngOnDestroy;

    if (typeof original !== 'function')
      console.warn(`${constructor.name} is using @AutoUnsubscribe() but does not implement OnDestroy!`);

    constructor.prototype.ngOnDestroy = function () {
      for (let prop in this) {

        const property = this[prop];

        if (!blackList.indexOf(prop)) {
          if (property && (typeof property.unsubscribe === 'function'))
            property.unsubscribe();
        }
      }
      original && typeof original === 'function' && original.apply(this, arguments);
    };
  }
}