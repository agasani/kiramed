import { Injectable, Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchFilter'
})

// <tr *ngFor="let item of mylst | searchfilter: 'fname' : txtFname.value">
@Injectable()
export class SearchFilterPipe implements PipeTransform {
  transform(items: any[], field: string, value: string): any[] {
    if (!items) return [];
    return items.filter(item => item[field] == value);
  }
}
