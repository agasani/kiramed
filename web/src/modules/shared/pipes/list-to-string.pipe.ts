﻿import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'listToStringPipe' })
export class ListToStringPipe implements PipeTransform {

  transform(value: Array<any>): string {
    return value.join(', ');
  }
}
