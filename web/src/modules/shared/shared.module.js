"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
var core_1 = require("@angular/core");
var capitalize_pipe_1 = require("./pipes/capitalize.pipe");
var spinner_service_1 = require("./components/spinner/spinner.service");
var common_1 = require("@angular/common");
var forms_1 = require("@angular/forms");
var router_1 = require("@angular/router");
var ngx_webstorage_1 = require("ngx-webstorage");
var components_1 = require("./components");
var services_1 = require("./services");
var guards_1 = require("./guards");
var directives_1 = require("./directives");
var search_1 = require("./components/search");
var loader_component_1 = require("./components/loader/loader.component");
var loader_service_1 = require("./components/loader/loader.service");
var alert_module_1 = require("./modules/alert/alert.module");
var page_header_service_1 = require("./services/page-header.service");
var resolve_module_1 = require("./modules/resolve/resolve.module");
var SharedModule = /** @class */ (function () {
    function SharedModule() {
    }
    SharedModule_1 = SharedModule;
    SharedModule.forRoot = function () {
        return {
            ngModule: SharedModule_1,
            providers: [
                guards_1.AccessGuard,
                guards_1.AuthGuard,
                services_1.AuthService,
                ngx_webstorage_1.LocalStorageService,
                guards_1.RoleGuard,
                services_1.RouteSegment,
                services_1.SharedService,
                spinner_service_1.SpinnerService,
                services_1.ValidationService,
                loader_service_1.LoaderService,
                page_header_service_1.PageHeaderService,
            ]
        };
    };
    SharedModule = SharedModule_1 = __decorate([
        core_1.NgModule({
            imports: [
                router_1.RouterModule,
                common_1.CommonModule,
                forms_1.FormsModule,
                forms_1.ReactiveFormsModule,
                alert_module_1.AlertModule,
                resolve_module_1.ResolveModule,
            ],
            declarations: [
                components_1.ActionMessageComponent,
                components_1.AddressFieldsComponent,
                capitalize_pipe_1.CapitalizePipe,
                components_1.ContactFieldsComponent,
                components_1.ControlMessagesComponent,
                components_1.CountrySelectComponent,
                components_1.PagerComponent,
                components_1.SpinnerComponent,
                components_1.MenuItemComponent,
                directives_1.ClaimsAccessDirective,
                directives_1.DebounceClickDirective,
                components_1.StepsNavComponent,
                components_1.SuiCalendarComponent,
                components_1.HasStepsComponent,
                search_1.ChecklistSearchComponent,
                loader_component_1.LoaderComponent,
            ],
            exports: [
                router_1.RouterModule,
                common_1.CommonModule,
                forms_1.FormsModule,
                forms_1.ReactiveFormsModule,
                alert_module_1.AlertModule,
                resolve_module_1.ResolveModule,
                //
                components_1.ActionMessageComponent,
                components_1.AddressFieldsComponent,
                capitalize_pipe_1.CapitalizePipe,
                components_1.ContactFieldsComponent,
                components_1.ControlMessagesComponent,
                components_1.CountrySelectComponent,
                components_1.PagerComponent,
                components_1.SpinnerComponent,
                components_1.MenuItemComponent,
                directives_1.ClaimsAccessDirective,
                directives_1.DebounceClickDirective,
                components_1.StepsNavComponent,
                components_1.SuiCalendarComponent,
                components_1.HasStepsComponent,
                search_1.ChecklistSearchComponent,
                loader_component_1.LoaderComponent,
            ]
        })
    ], SharedModule);
    return SharedModule;
    var SharedModule_1;
}());
exports.SharedModule = SharedModule;
