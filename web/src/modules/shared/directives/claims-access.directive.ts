import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';
import { LocalStorage } from 'ngx-webstorage';
import { arrayContainsArray } from '../../../helpers';

@Directive({ selector: '[allowedClaims]' })

export class ClaimsAccessDirective {
  @LocalStorage()
  private userSettings;

  constructor(private container: ViewContainerRef,
              private template: TemplateRef<any>) {
  }

  private _allowedClaims: string[] | string;

  @Input()
  get allowedClaims(): string[] | string {
    return this._allowedClaims;
  }

  set allowedClaims(arr: string[] | string) {
    if (!arr) arr = ['default'];
    else if (typeof arr === 'string') arr = [arr];
    arr && arr.map(str => str.trim());
    this._allowedClaims = Array.from(new Set(arr)) // Remove duplicate values
  }

  ngOnInit() {
    if (this.isAccessAllowed()) this.container.createEmbeddedView(this.template);
  }

  private isAccessAllowed(): boolean {
    let settings: string[] = JSON.parse(this.userSettings);
    let userClaims: string[] = settings && settings['claims'] || ['default', 'is.clinical'];

    return arrayContainsArray(userClaims, this.allowedClaims)
  }
}
