import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { Observable } from 'rxjs/Observable';
import { LocalStorage } from 'ngx-webstorage';
import '../../../extensions/rxjs-extensions';

@Injectable()
// USAGE EXAMPLE:
//==============
// const appRoutes: Routes = [
//   {
//     path: 'restricted',
//     component: RestrictedComponent,
//     canActivate: [AuthGuard],
//     data: {
//       authGuardRedirect: '/custom-redirect'
//     }
//   }
// ];

//http://www.kirjai.com/dynamic-guard-redirects-angular/
export class AuthGuard implements CanActivate {
  @LocalStorage() private userSettings: string;

  constructor(private authService: AuthService, private router: Router) {
  }

  canActivate(snapshot: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    const customRedirect = snapshot.data['authGuardRedirect'];
    const isAuthenticated$ = Observable.of(!!this.userSettings);
    const redirect = !!customRedirect ? customRedirect : 'login';

    this.authService.redirectUrl = state.url;

    isAuthenticated$
        .filter(isAuthenticated => !isAuthenticated)
        .subscribe(() => this.router.navigate([redirect]));

    return isAuthenticated$;
  }
}
