import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Location } from '@angular/common';
import { LocalStorage } from 'ngx-webstorage';
import { arrayContainsArray } from '../../../helpers';

@Injectable()
// USAGE EXAMPLE:
//==============
// const appRoutes: Routes = [
//   {
//     path: 'restricted',
//     component: RestrictedComponent,
//     canActivate: [AuthGuard],
//     data: {
//       accessGuardClaims: ['v.vit']
//     }
//   }
// ];

export class AccessGuard implements CanActivate {
  redirectUrl: string;
  allowed: Observable<boolean> = Observable.of(false);
  @LocalStorage() private userSettings: string;

  constructor(private location: Location, private router: Router) {
    this.router.events.subscribe(
        () => this.redirectUrl = this.location.path() || 'emr'
    );
  }

  canActivate(snapshot: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {

    const settings = JSON.parse(this.userSettings);
    if (!!settings && !!settings['claims']) {

      const allowedClaims = snapshot.data['accessGuardClaims'];
      this.allowed = Observable.of(arrayContainsArray(settings['claims'], allowedClaims));

      this.allowed.filter(isAllowed => !isAllowed).subscribe(
          () => this.router.navigate([this.redirectUrl])
      );
    }

    return this.allowed;
  }
}
