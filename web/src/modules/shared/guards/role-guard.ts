import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { JwtHelper } from 'angular2-jwt';

@Injectable()
//USAGE EXAMPLE
//=============
// path: 'admin',
//   component: AdminComponent,
//   canActivate: [RoleGuard],
//   data: {
//   expectedRole: 'admin'
// }
export class RoleGuard implements CanActivate {

  constructor(public auth: AuthService, public router: Router, protected jwt: JwtHelper) {
  }

  canActivate(route: ActivatedRouteSnapshot): boolean {

    // this will be passed from the route config
    // on the data property
    const expectedRole = route.data.expectedRole;

    const token = localStorage.getItem('token');

    // decode the token to get its payload
    const tokenPayload = this.jwt.decodeToken(token);

    if (!this.auth.isAuthenticated() || tokenPayload.role !== expectedRole) {
      this.router.navigate(['login']);
      return false;
    }
    return true;
  }

}
