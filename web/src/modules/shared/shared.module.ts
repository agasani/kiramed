﻿import { ModuleWithProviders, NgModule } from '@angular/core';
import { CapitalizePipe } from './pipes/capitalize.pipe';
import { SpinnerService } from './components/spinner/spinner.service';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { LocalStorageService } from 'ngx-webstorage';
import {
  ActionMessageComponent,
  AddressFieldsComponent,
  ContactFieldsComponent,
  ControlMessagesComponent,
  CountrySelectComponent,
  HasStepsComponent,
  MenuItemComponent,
  PagerComponent,
  SpinnerComponent,
  StepsNavComponent,
  SuiCalendarComponent
} from './components';
import { AuthService, RouteSegment, SharedService, ValidationService } from './services';
import { AccessGuard, AuthGuard, RoleGuard } from './guards';
import { ClaimsAccessDirective, DebounceClickDirective } from './directives';
import { ChecklistSearchComponent } from './components/search';
import { LoaderComponent } from './components/loader/loader.component';
import { LoaderService } from './components/loader/loader.service';
import { AlertModule } from './modules/alert/alert.module';
import { PageHeaderService } from './services/page-header.service';
import { ResolveModule } from './modules/resolve/resolve.module';

@NgModule({
  imports: [
    RouterModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AlertModule,
    ResolveModule,
  ],
  declarations: [
    ActionMessageComponent,
    AddressFieldsComponent,
    CapitalizePipe,
    ContactFieldsComponent,
    ControlMessagesComponent,
    CountrySelectComponent,
    PagerComponent,
    SpinnerComponent,
    MenuItemComponent,
    ClaimsAccessDirective,
    DebounceClickDirective,
    StepsNavComponent,
    SuiCalendarComponent,
    HasStepsComponent,
    ChecklistSearchComponent,
    LoaderComponent,
  ],
  exports: [
    RouterModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AlertModule,
    ResolveModule,
    //
    ActionMessageComponent,
    AddressFieldsComponent,
    CapitalizePipe,
    ContactFieldsComponent,
    ControlMessagesComponent,
    CountrySelectComponent,
    PagerComponent,
    SpinnerComponent,
    MenuItemComponent,
    ClaimsAccessDirective,
    DebounceClickDirective,
    StepsNavComponent,
    SuiCalendarComponent,
    HasStepsComponent,
    ChecklistSearchComponent,
    LoaderComponent,
  ],
})
export class SharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [
        AccessGuard,
        AuthGuard,
        AuthService,
        LocalStorageService,
        RoleGuard,
        RouteSegment,
        SharedService,
        SpinnerService,
        ValidationService,
        LoaderService,
        PageHeaderService,
      ]
    };
  }
}
