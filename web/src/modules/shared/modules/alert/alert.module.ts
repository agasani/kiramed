import { NgModule } from '@angular/core';
import { AlertComponent } from './directives';
import { AlertService } from './services';

@NgModule({
  imports: [/**/],
  declarations: [AlertComponent],
  exports: [AlertComponent],
  providers: [AlertService],
})

export class AlertModule {
}