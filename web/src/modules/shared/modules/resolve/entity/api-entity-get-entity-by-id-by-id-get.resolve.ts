import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Feedback } from '../../../models/Feedback';
import { EntityService } from '../../../../api/services/entity.service';
import { EntityViewModel } from '../../../../api/models/entity-view-model';

@Injectable()
export class ApiEntityGetEntityByIdByIdGetResolve implements Resolve<string | EntityViewModel> {

  constructor(private entityService: EntityService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<string | EntityViewModel> {
    return this.entityService.ApiEntityGetEntityByIdByIdGet({ id: +route.paramMap.get('id') }).catch((error: Feedback) => {
      return Observable.of(error.message);
    });
  }
}