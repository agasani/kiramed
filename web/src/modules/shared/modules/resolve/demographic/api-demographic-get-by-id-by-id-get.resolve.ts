import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Feedback } from '../../../models/Feedback';
import { DemographicService } from '../../../../api/services/demographic.service';
import { EntityDemographicViewModel } from '../../../../api/models/entity-demographic-view-model';

@Injectable()
export class ApiDemographicGetByIdByIdGetResolve implements Resolve<string | EntityDemographicViewModel[]> {

  constructor(private demographicService: DemographicService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<string | EntityDemographicViewModel[]> {
    return this.demographicService.ApiDemographicGetByIdByIdGet({ id: +route.paramMap.get('id') }).catch((error: Feedback) => {
      return Observable.of(error.message);
    });
  }
}