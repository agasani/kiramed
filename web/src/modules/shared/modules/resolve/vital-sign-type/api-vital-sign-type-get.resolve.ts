import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Feedback } from '../../../models/Feedback';
import { VitalSignTypeService } from '../../../../api/services/vital-sign-type.service';
import { VitalSignTypeViewModel } from '../../../../api/models/vital-sign-type-view-model';

@Injectable()
export class ApiVitalSignTypeGetResolve implements Resolve<string | VitalSignTypeViewModel[]> {

  constructor(private vitalSignTypeService: VitalSignTypeService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<string | VitalSignTypeViewModel[]> {
    return this.vitalSignTypeService.ApiVitalSignTypeGet().catch((error: Feedback) => {
      return Observable.of(error.message);
    });
  }
}