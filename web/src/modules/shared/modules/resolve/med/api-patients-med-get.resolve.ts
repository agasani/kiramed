import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Feedback } from '../../../models/Feedback';
import { MedService } from '../../../../api/services/med.service';

@Injectable()
export class ApiPatientsMedGetResolve implements Resolve<string | string[]> {

  constructor(private medService: MedService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<string | string[]> {
    return this.medService.ApiPatientsMedGet().catch((error: Feedback) => {
      return Observable.of(error.message);
    });
  }
}