import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Feedback } from '../../../models/Feedback';
import { MedService } from '../../../../api/services/med.service';

@Injectable()
export class ApiPatientsMedByIdGetResolve implements Resolve<string> {

  constructor(private medService: MedService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<string> {
    return this.medService.ApiPatientsMedByIdGet({ id: +route.paramMap.get('id') }).catch((error: Feedback) => {
      return Observable.of(error.message);
    });
  }
}