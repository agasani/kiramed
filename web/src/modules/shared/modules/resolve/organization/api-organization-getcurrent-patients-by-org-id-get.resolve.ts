import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Feedback } from '../../../models/Feedback';
import { OrganizationService } from '../../../../api/services/organization.service';
import { VisitViewModel } from '../../../../api/models/visit-view-model';

@Injectable()
export class ApiOrganizationGetcurrentPatientsByOrgIdGetResolve implements Resolve<string | VisitViewModel[]> {

  constructor(private organizationService: OrganizationService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<string | VisitViewModel[]> {
    return this.organizationService.ApiOrganizationGetcurrentPatientsByOrgIdGet({ orgId: +route.paramMap.get('orgId') }).catch((error: Feedback) => {
      return Observable.of(error.message);
    });
  }
}