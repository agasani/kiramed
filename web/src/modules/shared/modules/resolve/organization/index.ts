export * from './api-organization-detail-by-org-id-get.resolve';
export * from './api-organization-get.resolve';
export * from './api-organization-get-admitted-patients-by-org-id-get.resolve';
export * from './api-organization-get-by-national-id-by-national-id-get.resolve';
export * from './api-organization-get-waiting-patients-by-org-id-get.resolve';
export * from './api-organization-getcurrent-patients-by-org-id-get.resolve';