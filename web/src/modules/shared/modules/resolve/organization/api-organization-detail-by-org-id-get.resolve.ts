import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Feedback } from '../../../models/Feedback';
import { OrganizationService } from '../../../../api/services/organization.service';
import { OrganizationViewModel } from '../../../../api/models/organization-view-model';

@Injectable()
export class ApiOrganizationDetailByOrgIdGetResolve implements Resolve<string | OrganizationViewModel> {

  constructor(private organizationService: OrganizationService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<string | OrganizationViewModel> {
    return this.organizationService.ApiOrganizationDetailByOrgIdGet({ orgId: +route.paramMap.get('orgId') }).catch((error: Feedback) => {
      return Observable.of(error.message);
    });
  }
}