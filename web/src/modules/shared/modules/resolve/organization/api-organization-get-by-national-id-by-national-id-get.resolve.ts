import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Feedback } from '../../../models/Feedback';
import { OrganizationViewModel } from '../../../../api/models/organization-view-model';
import { OrganizationService } from '../../../../api/services/organization.service';

@Injectable()
export class ApiOrganizationGetByNationalIdByNationalIdGetResolve implements Resolve<string | OrganizationViewModel> {

  constructor(private organizationService: OrganizationService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<string | OrganizationViewModel> {
    return this.organizationService.ApiOrganizationGetByNationalIdByNationalIdGet({ nationalId: route.paramMap.get('nationalId') }).catch((error: Feedback) => {
      return Observable.of(error.message);
    });
  }
}