import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Feedback } from '../../../models/Feedback';
import { VitalSignService } from '../../../../api/services/vital-sign.service';
import { VitalSignViewModel } from '../../../../api/models/vital-sign-view-model';

@Injectable()
export class ApiVitalSignGetByEncounterByEncounterIdGetResolve implements Resolve<string | VitalSignViewModel[]> {

  constructor(private vitalSignService: VitalSignService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<string | VitalSignViewModel[]> {
    return this.vitalSignService.ApiVitalSignGetByEncounterByEncounterIdGet({ encounterId: +route.paramMap.get('encounterId') }).catch((error: Feedback) => {
      return Observable.of(error.message);
    });
  }
}