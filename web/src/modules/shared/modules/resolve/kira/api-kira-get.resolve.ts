import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Feedback } from '../../../models/Feedback';
import { KiraService } from '../../../../api/services/kira.service';
import { Patient } from '../../../../api/models/patient';

@Injectable()
export class ApiKiraGetResolve implements Resolve<string | Patient[]> {

  constructor(private kiraService: KiraService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<string | Patient[]> {
    return this.kiraService.ApiKiraGet().catch((error: Feedback) => {
      return Observable.of(error.message);
    });
  }
}