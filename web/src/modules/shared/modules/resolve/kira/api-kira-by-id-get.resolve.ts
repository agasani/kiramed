import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Feedback } from '../../../models/Feedback';
import { KiraService } from '../../../../api/services/kira.service';

@Injectable()
export class ApiKiraByIdGetResolve implements Resolve<string> {

  constructor(private kiraService: KiraService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<string> {
    return this.kiraService.ApiKiraByIdGet({ id: +route.paramMap.get('id') }).catch((error: Feedback) => {
      return Observable.of(error.message);
    });
  }
}