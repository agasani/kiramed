import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Feedback } from '../../../models/Feedback';
import { EncounterService } from '../../../../api/services/encounter.service';
import { EncounterViewModel } from '../../../../api/models/encounter-view-model';

@Injectable()
export class ApiEncounterOrganizationIdGetResolve implements Resolve<string | EncounterViewModel[]> {

  constructor(private encounterService: EncounterService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<string | EncounterViewModel[]> {
    return this.encounterService.ApiEncounterOrganizationIdGet({ organizationId: +route.paramMap.get('organizationId') }).catch((error: Feedback) => {
      return Observable.of(error.message);
    });
  }
}