import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Feedback } from '../../../models/Feedback';
import { VisitService } from '../../../../api/services/visit.service';
import { VisitViewModel } from '../../../../api/models/visit-view-model';

@Injectable()
export class ApiVisitGetPatientVisitsByPatientIdGetResolve implements Resolve<string | VisitViewModel[]> {

  constructor(private visitService: VisitService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<string | VisitViewModel[]> {
    return this.visitService.ApiVisitGetPatientVisitsByPatientIdGet({ patientId: +route.paramMap.get('patientId') }).catch((error: Feedback) => {
      return Observable.of(error.message);
    });
  }
}