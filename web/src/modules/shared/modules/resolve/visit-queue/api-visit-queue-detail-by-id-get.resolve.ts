import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Feedback } from '../../../models/Feedback';
import { VisitQueueService } from '../../../../api/services/visit-queue.service';
import { VisitQueueViewModel } from '../../../../api/models/visit-queue-view-model';

@Injectable()
export class ApiVisitQueueDetailByIdGetResolve implements Resolve<string | VisitQueueViewModel> {

  constructor(private visitQueueService: VisitQueueService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<string | VisitQueueViewModel> {
    return this.visitQueueService.ApiVisitQueueDetailByIdGet({ id: +route.paramMap.get('id') }).catch((error: Feedback) => {
      return Observable.of(error.message);
    });
  }
}