import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Feedback } from '../../../models/Feedback';
import { AppointmentViewModel } from '../../../../api/models/appointment-view-model';
import { AppointmentService } from '../../../../api/services/appointment.service';

@Injectable()
export class ApiAppointmentDetailByIdGetResolve implements Resolve<string | AppointmentViewModel> {

  constructor(private appointmentService: AppointmentService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<string | AppointmentViewModel> {
    return this.appointmentService.ApiAppointmentDetailByIdGet({ id: +route.paramMap.get('id') }).catch((error: Feedback) => {
      return Observable.of(error.message);
    });
  }
}