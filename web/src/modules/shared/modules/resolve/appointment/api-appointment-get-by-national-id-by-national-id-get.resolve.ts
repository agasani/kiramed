import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Feedback } from '../../../models/Feedback';
import { AppointmentService } from '../../../../api/services/appointment.service';
import { AppointmentViewModel } from '../../../../api/models/appointment-view-model';

@Injectable()
export class ApiAppointmentGetByNationalIdByNationalIdGetResolve implements Resolve<string | AppointmentViewModel[]> {

  constructor(private appointmentService: AppointmentService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<string | AppointmentViewModel[]> {
    return this.appointmentService.ApiAppointmentGetByNationalIdByNationalIdGet({ nationalId: route.paramMap.get('nationalId') }).catch((error: Feedback) => {
      return Observable.of(error.message);
    });
  }
}