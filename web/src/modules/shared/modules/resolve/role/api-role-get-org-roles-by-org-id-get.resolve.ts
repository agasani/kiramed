import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { RoleViewModel } from '../../../../api/models/role-view-model';
import { RoleService } from '../../../../api/services/role.service';
import { Observable } from 'rxjs/Observable';
import { Feedback } from '../../../models/Feedback';

@Injectable()
export class ApiRoleGetOrgRolesByOrgIdGetResolve implements Resolve<string | RoleViewModel[]> {

  constructor(private roleService: RoleService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<string | RoleViewModel[]> {
    return this.roleService.ApiRoleGetOrgRolesByOrgIdGet({ orgId: +route.paramMap.get('orgId') }).catch((error: Feedback) => {
      return Observable.of(error.message);
    });
  }
}