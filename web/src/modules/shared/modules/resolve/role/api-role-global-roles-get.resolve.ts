import { RoleService } from '../../../../api/services/role.service';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { RoleViewModel } from '../../../../api/models/role-view-model';
import { Observable } from 'rxjs/Observable';
import { FeedBack } from '../../../../api/models/feed-back';

@Injectable()
export class ApiRoleGlobalRolesGetResolve implements Resolve<string | RoleViewModel[]> {

  constructor(private roleService: RoleService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<string | RoleViewModel[]> {
    return this.roleService.ApiRoleGlobalRolesGet().catch((error: FeedBack) => {
      return Observable.of(error.message);
    });
  }
}