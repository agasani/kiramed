export * from './api-role-country-roles-by-country-id-get.resolve';
export * from './api-role-details-by-role-id-get.resolve';
export * from './api-role-get.resolve';
export * from './api-role-get-org-roles-by-org-id-get.resolve';
export * from './api-role-global-roles-get.resolve';