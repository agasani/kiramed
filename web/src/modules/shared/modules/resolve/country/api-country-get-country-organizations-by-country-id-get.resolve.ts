import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Feedback } from '../../../models/Feedback';
import { CountryService } from '../../../../api/services/country.service';
import { OrganizationViewModel } from '../../../../api/models/organization-view-model';

@Injectable()
export class ApiCountryGetCountryOrganizationsByCountryIdGetResolve implements Resolve<string | OrganizationViewModel[]> {

  constructor(private countryService: CountryService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<string | OrganizationViewModel[]> {
    return this.countryService.ApiCountryGetCountryOrganizationsByCountryIdGet({ countryId: +route.paramMap.get('countryId') }).catch((error: Feedback) => {
      return Observable.of(error.message);
    });
  }
}