import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Feedback } from '../../../models/Feedback';
import { CountryViewModel } from '../../../../api/models/country-view-model';
import { CountryService } from '../../../../api/services/country.service';

@Injectable()
export class ApiCountryGetByCodeByCodeGetResolve implements Resolve<string | CountryViewModel> {

  constructor(private countryService: CountryService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<string | CountryViewModel> {
    return this.countryService.ApiCountryGetByCodeByCodeGet({ code: route.paramMap.get('code') }).catch((error: Feedback) => {
      return Observable.of(error.message);
    });
  }
}