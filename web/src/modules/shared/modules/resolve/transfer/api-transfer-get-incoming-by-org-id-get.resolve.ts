import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Feedback } from '../../../models/Feedback';
import { TransferService } from '../../../../api/services/transfer.service';

@Injectable()
export class ApiTransferGetIncomingByOrgIdGetResolve implements Resolve<string | void> {

  constructor(private transferService: TransferService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<string | void> {
    return this.transferService.ApiTransferGetIncomingByOrgIdGet({ orgId: +route.paramMap.get('orgId') }).catch((error: Feedback) => {
      return Observable.of(error.message);
    });
  }
}