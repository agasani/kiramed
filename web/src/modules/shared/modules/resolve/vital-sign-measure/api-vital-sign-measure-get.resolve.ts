import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Feedback } from '../../../models/Feedback';
import { VitalSignMeasureService } from '../../../../api/services/vital-sign-measure.service';
import { VitalSignMeasureViewModel } from '../../../../api/models/vital-sign-measure-view-model';

@Injectable()
export class ApiVitalSignMeasureGetResolve implements Resolve<string | VitalSignMeasureViewModel[]> {

  constructor(private vitalSignMeasureService: VitalSignMeasureService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<string | VitalSignMeasureViewModel[]> {
    return this.vitalSignMeasureService.ApiVitalSignMeasureGet().catch((error: Feedback) => {
      return Observable.of(error.message);
    });
  }
}