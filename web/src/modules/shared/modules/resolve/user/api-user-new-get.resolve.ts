import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { UserService } from '../../../../api/services/user.service';
import { FeedBack } from '../../../../api/models/feed-back';

@Injectable()
export class ApiUserNewGetResolve implements Resolve<string | FeedBack> {

  constructor(private userService: UserService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<string | FeedBack> {
    return this.userService.ApiUserNewGet().catch((error: FeedBack) => {
      return Observable.of(error.message);
    });
  }
}