import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Feedback } from '../../../models/Feedback';
import { UserViewModel } from '../../../../api/models/user-view-model';
import { UserService } from '../../../../api/services/user.service';

@Injectable()
export class ApiUserDetailByEmailGetResolve implements Resolve<string | UserViewModel> {

  constructor(private userService: UserService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<string | UserViewModel> {
    return this.userService.ApiUserDetailByEmailGet({ email: route.paramMap.get('email') }).catch((error: Feedback) => {
      return Observable.of(error.message);
    });
  }
}