import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Feedback } from '../../../models/Feedback';
import { UserService } from '../../../../api/services/user.service';
import { UserViewModel } from '../../../../api/models/user-view-model';

@Injectable()
export class ApiUserOrganizationUsersByRoleByOrgIdByRoleNameGetResolve implements Resolve<string | UserViewModel[]> {

  constructor(private userService: UserService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<string | UserViewModel[]> {
    return this.userService.ApiUserOrganizationUsersByRoleByOrgIdByRoleNameGet({
      roleName: route.paramMap.get('roleName'),
      orgId: +route.paramMap.get('orgId')
    }).catch((error: Feedback) => {
      return Observable.of(error.message);
    });
  }
}