import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Feedback } from '../../../models/Feedback';
import { OrganizationViewModel } from '../../../../api/models/organization-view-model';
import { UserService } from '../../../../api/services/user.service';

@Injectable()
export class ApiUserGetUserOrgsByUserIdGetResolve implements Resolve<string | OrganizationViewModel[]> {

  constructor(private userService: UserService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<string | OrganizationViewModel[]> {
    return this.userService.ApiUserGetUserOrgsByUserIdGet({ userId: route.paramMap.get('userId') }).catch((error: Feedback) => {
      return Observable.of(error.message);
    });
  }
}