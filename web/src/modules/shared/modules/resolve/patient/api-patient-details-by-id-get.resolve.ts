import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Feedback } from '../../../models/Feedback';
import { PatientService } from '../../../../api/services/patient.service';
import { PatientViewModel } from '../../../../api/models/patient-view-model';

@Injectable()
export class ApiPatientDetailsByIdGetResolve implements Resolve<string | PatientViewModel[]> {

  constructor(private patientService: PatientService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<string | PatientViewModel[]> {
    return this.patientService.ApiPatientDetailsByIdGet({ id: +route.paramMap.get('id') }).catch((error: Feedback) => {
      return Observable.of(error.message);
    });
  }
}