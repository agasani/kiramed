import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Feedback } from '../../../models/Feedback';
import { PatientSearchService } from '../../../../api/services/patient-search.service';
import { PatientSearchViewModel } from '../../../../api/models/patient-search-view-model';

@Injectable()
export class ApiPatientsPatientSearchBySearchInputGetResolve implements Resolve<string | PatientSearchViewModel[]> {

  constructor(private patientSearchService: PatientSearchService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<string | PatientSearchViewModel[]> {
    return this.patientSearchService.ApiPatientsPatientSearchBySearchInputGet({ searchInput: route.paramMap.get('searchInput') }).catch((error: Feedback) => {
      return Observable.of(error.message);
    });
  }
}