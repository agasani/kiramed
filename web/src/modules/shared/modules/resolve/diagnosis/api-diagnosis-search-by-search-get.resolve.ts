import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Feedback } from '../../../models/Feedback';
import { DiagnosisService } from '../../../../api/services/diagnosis.service';
import { DiagnosisViewModel } from '../../../../api/models/diagnosis-view-model';

@Injectable()
export class ApiDiagnosisSearchBySearchGetResolve implements Resolve<string | DiagnosisViewModel[]> {

  constructor(private diagnosisService: DiagnosisService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<string | DiagnosisViewModel[]> {
    return this.diagnosisService.ApiDiagnosisSearchBySearchGet({ search: route.paramMap.get('search') }).catch((error: Feedback) => {
      return Observable.of(error.message);
    });
  }
}