import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Feedback } from '../../../models/Feedback';
import { SystemActionService } from '../../../../api/services/system-action.service';
import { ActionViewModel } from '../../../../api/models/action-view-model';

@Injectable()
export class ApiSystemActionGetResolve implements Resolve<string | ActionViewModel[]> {

  constructor(private systemActionService: SystemActionService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<string | ActionViewModel[]> {
    return this.systemActionService.ApiSystemActionGet().catch((error: Feedback) => {
      return Observable.of(error.message);
    });
  }
}