import { NgModule } from '@angular/core';
import {
  ApiAppointmentDetailByIdGetResolve,
  ApiAppointmentGetByNationalIdByNationalIdGetResolve,
  ApiAppointmentGetResolve
} from './appointment';
import {
  ApiCountryGetByCodeByCodeGetResolve,
  ApiCountryGetByIdByIdGetResolve,
  ApiCountryGetCountryOrganizationsByCountryIdGetResolve,
  ApiCountryGetResolve
} from './country';
import { ApiDemographicGetByIdByIdGetResolve } from './demographic';
import { ApiDiagnosisSearchBySearchGetResolve } from './diagnosis';
import { ApiEncounterGetByPatientByPatientIdGetResolve, ApiEncounterOrganizationIdGetResolve } from './encounter';
import { ApiEntityGetEntityByIdByIdGetResolve } from './entity';
import { ApiEntityAddressGetEntityByIdByIdGetResolve } from './entity-address';
import { ApiKiraByIdGetResolve, ApiKiraGetResolve } from './kira';
import { ApiPatientsMedByIdGetResolve, ApiPatientsMedGetResolve } from './med';
import {
  ApiOrganizationDetailByOrgIdGetResolve,
  ApiOrganizationGetAdmittedPatientsByOrgIdGetResolve,
  ApiOrganizationGetByNationalIdByNationalIdGetResolve,
  ApiOrganizationGetcurrentPatientsByOrgIdGetResolve,
  ApiOrganizationGetResolve,
  ApiOrganizationGetWaitingPatientsByOrgIdGetResolve
} from './organization';
import { ApiPatientDetailsByIdGetResolve, ApiPatientGetResolve } from './patient';
import {
  ApiPatientDiagnosisDetailByIdGetResolve,
  ApiPatientDiagnosisGetByPatientByPatientIdGetResolve,
  ApiPatientDiagnosisGetResolve
} from './patient-diagnosis';
import { ApiPatientsPatientSearchBySearchInputGetResolve } from './patient-search';
import {
  ApiRoleCountryRolesByCountryIdGetResolve,
  ApiRoleDetailsByRoleIdGetResolve,
  ApiRoleGetOrgRolesByOrgIdGetResolve,
  ApiRoleGetResolve,
  ApiRoleGlobalRolesGetResolve
} from './role';
import { ApiSystemActionGetResolve } from './system-action';
import { ApiTransferGetIncomingByOrgIdGetResolve, ApiTransferGetOutGoingByOrgIdGetResolve } from './transfer';
import {
  ApiUserDetailByEmailGetResolve,
  ApiUserGetResolve,
  ApiUserGetUserOrgsByUserIdGetResolve,
  ApiUserNewGetResolve,
  ApiUserOrganizationUsersByOrgIdGetResolve,
  ApiUserOrganizationUsersByRoleByOrgIdByRoleNameGetResolve
} from './user';
import {
  ApiVisitDetailByVisitIdGetResolve,
  ApiVisitGetOrgVisitsByOrgIdGetResolve,
  ApiVisitGetPatientVisitsByPatientIdGetResolve
} from './visit';
import { ApiVisitQueueDetailByIdGetResolve, ApiVisitQueueGetResolve } from './visit-queue';
import {
  ApiVitalSignDetailByIdGetResolve,
  ApiVitalSignGetByEncounterByEncounterIdGetResolve,
  ApiVitalSignGetResolve
} from './vital-sign';
import { ApiVitalSignMeasureDetailByIdGetResolve, ApiVitalSignMeasureGetResolve } from './vital-sign-measure';
import { ApiVitalSignTypeDetailByIdGetResolve, ApiVitalSignTypeGetResolve } from './vital-sign-type';

@NgModule({
  providers: [
    ApiAppointmentDetailByIdGetResolve,
    ApiAppointmentGetByNationalIdByNationalIdGetResolve,
    ApiAppointmentGetResolve,
    ApiCountryGetCountryOrganizationsByCountryIdGetResolve,
    ApiCountryGetByIdByIdGetResolve,
    ApiCountryGetByCodeByCodeGetResolve,
    ApiCountryGetResolve,
    ApiDemographicGetByIdByIdGetResolve,
    ApiDiagnosisSearchBySearchGetResolve,
    ApiEncounterGetByPatientByPatientIdGetResolve,
    ApiEncounterOrganizationIdGetResolve,
    ApiEntityGetEntityByIdByIdGetResolve,
    ApiEntityAddressGetEntityByIdByIdGetResolve,
    ApiKiraByIdGetResolve,
    ApiKiraGetResolve,
    ApiPatientsMedByIdGetResolve,
    ApiPatientsMedGetResolve,
    ApiOrganizationDetailByOrgIdGetResolve,
    ApiOrganizationGetWaitingPatientsByOrgIdGetResolve,
    ApiOrganizationGetResolve,
    ApiOrganizationGetcurrentPatientsByOrgIdGetResolve,
    ApiOrganizationGetByNationalIdByNationalIdGetResolve,
    ApiOrganizationGetAdmittedPatientsByOrgIdGetResolve,
    ApiPatientDetailsByIdGetResolve,
    ApiPatientGetResolve,
    ApiPatientDiagnosisDetailByIdGetResolve,
    ApiPatientDiagnosisGetByPatientByPatientIdGetResolve,
    ApiPatientDiagnosisGetResolve,
    ApiPatientsPatientSearchBySearchInputGetResolve,
    ApiRoleDetailsByRoleIdGetResolve,
    ApiRoleGetOrgRolesByOrgIdGetResolve,
    ApiRoleGlobalRolesGetResolve,
    ApiRoleCountryRolesByCountryIdGetResolve,
    ApiRoleGetResolve,
    ApiSystemActionGetResolve,
    ApiTransferGetIncomingByOrgIdGetResolve,
    ApiTransferGetOutGoingByOrgIdGetResolve,
    ApiUserDetailByEmailGetResolve,
    ApiUserOrganizationUsersByOrgIdGetResolve,
    ApiUserNewGetResolve,
    ApiUserGetUserOrgsByUserIdGetResolve,
    ApiUserGetResolve,
    ApiUserOrganizationUsersByRoleByOrgIdByRoleNameGetResolve,
    ApiVisitDetailByVisitIdGetResolve,
    ApiVisitGetOrgVisitsByOrgIdGetResolve,
    ApiVisitGetPatientVisitsByPatientIdGetResolve,
    ApiVisitQueueDetailByIdGetResolve,
    ApiVisitQueueGetResolve,
    ApiVitalSignDetailByIdGetResolve,
    ApiVitalSignGetByEncounterByEncounterIdGetResolve,
    ApiVitalSignGetResolve,
    ApiVitalSignMeasureDetailByIdGetResolve,
    ApiVitalSignMeasureGetResolve,
    ApiVitalSignTypeDetailByIdGetResolve,
    ApiVitalSignTypeGetResolve
  ],
})

export class ResolveModule {
}