import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Feedback } from '../../../models/Feedback';
import { EntityAddressService } from '../../../../api/services/entity-address.service';
import { EntityAddressViewModel } from '../../../../api/models/entity-address-view-model';

@Injectable()
export class ApiEntityAddressGetEntityByIdByIdGetResolve implements Resolve<string | EntityAddressViewModel[]> {

  constructor(private entityAddressService: EntityAddressService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<string | EntityAddressViewModel[]> {
    return this.entityAddressService.ApiEntityAddressGetEntityByIdByIdGet({ id: +route.paramMap.get('id') }).catch((error: Feedback) => {
      return Observable.of(error.message);
    });
  }
}