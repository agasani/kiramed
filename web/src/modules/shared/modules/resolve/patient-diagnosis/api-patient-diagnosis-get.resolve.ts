import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Feedback } from '../../../models/Feedback';
import { PatientDiagnosisService } from '../../../../api/services/patient-diagnosis.service';
import { PatientDiagnosisViewModel } from '../../../../api/models/patient-diagnosis-view-model';

@Injectable()
export class ApiPatientDiagnosisGetResolve implements Resolve<string | PatientDiagnosisViewModel> {

  constructor(private patientDiagnosisService: PatientDiagnosisService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<string | PatientDiagnosisViewModel> {
    return this.patientDiagnosisService.ApiPatientDiagnosisGet({ encounterId: +route.paramMap.get('encounterId') }).catch((error: Feedback) => {
      return Observable.of(error.message);
    });
  }
}