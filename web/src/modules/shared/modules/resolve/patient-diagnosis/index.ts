export * from './api-patient-diagnosis-detail-by-id-get.resolve';
export * from './api-patient-diagnosis-get-by-patient-by-patientId-get.resolve';
export * from './api-patient-diagnosis-get.resolve';