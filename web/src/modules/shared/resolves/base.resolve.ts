import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';

// import { ContactsService } from './contacts.service';

@Injectable()
export class BaseResolve implements Resolve<any> {

  constructor(/*private contactsService: ContactsService*/) {
  }

  resolve(route: ActivatedRouteSnapshot) {
    // return this.contactsService.getContact(route.paramMap.get('id'));
  }
}