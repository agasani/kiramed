import { AfterViewInit, Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'kira-address-fields',
  templateUrl: './address-fields.component.html'
})
export class AddressFieldsComponent implements OnInit, AfterViewInit {
  @Input() group: FormGroup;
  controls: any;

  constructor() {
  }

  ngOnInit() {
    this.controls = this.group.controls;
    console.log('Address fields ...')
  }

  ngAfterViewInit() {
    $('.ui.dropdown').dropdown();
  }
}
