import { AfterViewInit, Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'kira-contact-fields',
  templateUrl: './contact-fields.component.html'
})
export class ContactFieldsComponent implements OnInit, AfterViewInit {
  @Input() group: FormGroup;
  controls: any;
  email: boolean;
  phone: boolean;
  twitter: boolean;
  placeholder: string = 'Select the contact type first...';
  disabled: boolean = true;

  types: ContactType[] = [
    { 'value': '', 'name': 'Contact type' },
    { 'value': 'email', 'name': 'Email address' },
    { 'value': 'phone', 'name': 'Phone number' },
    { 'value': 'twitter', 'name': 'Twitter account' },
  ];
  selectedType: ContactType = this.types[0];

  constructor() {
  }

  ngOnInit() {
    this.controls = this.group.controls;
    console.log('Contact fields ...');
  }

  ngAfterViewInit() {
    $('.ui.dropdown').dropdown();
  }

  onSelect(value) {
    this.selectedType = null;
    this.email = this.phone = this.twitter = false;

    for (let i = 0; i < this.types.length; i++) {
      if (this.types[i].value == value)
        this.selectedType = this.types[i];
    }
    this.disabled = false;

    console.log(`${this.selectedType.value} contact type selected!`);

    switch (this.selectedType.value) {
      case 'phone':
        this.phone = true;
        this.placeholder = 'Enter phone number here...';
        break;

      case 'email':
        this.email = true;
        this.placeholder = 'Enter email address here...';
        break;

      default:
        this.twitter = true;
        this.placeholder = 'Enter twitter account here...';
    }
  }
}
