"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
var core_1 = require("@angular/core");
var ngx_webstorage_1 = require("ngx-webstorage");
var Observable_1 = require("rxjs/Observable");
var auto_unsubscribe_1 = require("../decorators/auto-unsubscribe");
var BaseComponent = /** @class */ (function () {
    function BaseComponent() {
        this.settings = JSON.parse(this.userSettings) || '';
        this.userId = this.settings['userId'] || '';
        this.userOrgId = this.settings['userOrgId'] || '';
        this.userCountryId = this.settings['countryOrgId'] || '';
        this.userClaims = this.settings['claims'] || [''];
        this.message = new core_1.EventEmitter();
        this.errorMsg = '';
        this.successMsg = '';
        this.redirect = false;
        this.creatingItem = false;
        this.editingItem = false;
        this.allItems = [];
        this.pageSize = 2;
        this.firstUrlPart = '';
        this.secondUrlPart = '';
        this.respDataIdProperty = 'id';
        this.httpMethod = '';
        this.noHeader = { type: 0, content: null };
    }
    BaseComponent.prototype.ngOnInit = function () {
        //  Don't put any thing here!
    };
    BaseComponent.prototype.ngAfterViewInit = function () {
        //  Don't put any thing here!
    };
    BaseComponent.prototype.ngOnDestroy = function () {
        //  Don't put any thing here!
    };
    BaseComponent.prototype.initCreateForm = function () {
        //  Don't put any thing here!
    };
    BaseComponent.prototype.initEditForm = function (item) {
        //  Don't put any thing here!
    };
    // CRUD METHODS
    // ITEMS_COMPONENT METHODS:
    BaseComponent.prototype.getAll = function (data) {
        var _this = this;
        this.httpMethod = 'get';
        Observable_1.Observable.of(data).subscribe(function (response) { return _this.onSuccessResponse(response); }, function (error) { return _this.onFailorResponse(error); }, function () { return console.log(_this.itemName + " items listed"); });
    };
    BaseComponent.prototype.create = function () {
        var _this = this;
        this.httpMethod = 'post';
        this.postObservable.subscribe(function (response) { return _this.onSuccessResponse(response); }, function (error) { return _this.onFailorResponse(error); }, function () { return console.log(_this.itemName + " item create aborted :)"); });
    };
    BaseComponent.prototype.showInfo = function (item, id$) {
        if (id$ === void 0) { id$ = item.id; }
        this.selectedItem = item;
        this.router.navigate([this.firstUrlPart + '/' + id$ + '/' + this.secondUrlPart]);
    };
    BaseComponent.prototype.onPageChange = function (event) {
        this.pagedItems = event;
    };
    // ITEM_INFO_COMPONENT METHODS:
    BaseComponent.prototype.getItem = function (data) {
        var _this = this;
        this.httpMethod = 'detail';
        Observable_1.Observable.of(data).subscribe(function (response) { return _this.onSuccessResponse(response); }, function (error) { return _this.onFailorResponse(error); }, function () { return console.log(_this.itemName + " item detailed"); });
    };
    BaseComponent.prototype.edit = function () {
        var _this = this;
        this.httpMethod = 'put';
        this.putObservable.subscribe(function (response) { return _this.onSuccessResponse(response); }, function (error) { return _this.onFailorResponse(error); }, function () { return console.log(_this.itemName + " item edit aborted"); });
    };
    BaseComponent.prototype.destroy = function (item) {
        var _this = this;
        this.item = item;
        this.httpMethod = 'delete';
        confirm("Are you sure you want to delete this \"" + this.itemName + "\" of id: " + this.itemId + " ?") &&
            this.deleteObservable.subscribe(function (response) { return _this.onSuccessResponse(response); }, function (error) { return _this.onFailorResponse(error); }, function () { return console.log(_this.itemName + " item deleted"); });
    };
    BaseComponent.prototype.sideBarMenuTrigger = function () {
        // Setup the sidebar..
        try {
            $('#clinical-sidebar')
                .sidebar({
                context: '#clinical-main',
                transition: 'overlay',
                mobileTransition: 'overlay'
            })
                .sidebar('attach events', '.amasidemenu', 'toggle');
        }
        catch (e) {
            console.log('An error happened initiating clinical sidebar: ', e);
        }
    };
    BaseComponent.prototype.alertMessageTrigger = function () {
        try {
            $('.message .close').on('click', function () {
                $(this).closest('.message').transition('fade');
            });
        }
        catch (e) {
            console.log('Error occured on alert show!');
        }
    };
    BaseComponent.prototype.onSuccessResponse = function (results) {
        var _this = this;
        switch (this.httpMethod) {
            case 'get':
                (_a = this.allItems).push.apply(_a, results);
                this.initCreateForm();
                this.controls = this.formGroup.controls;
                break;
            case 'post':
                this.allItems.push(this.item);
                this.itemId = JSON.parse(results.data)[this.respDataIdProperty];
                this.redirectUponCreate = [this.firstUrlPart + "/" + this.itemId + "/" + this.secondUrlPart];
                this.redirect && this.router.navigate(this.redirectUponCreate);
                this.creatingItem = false;
                this.formReset();
                this.send_msg(results.message);
                this.httpMethod = 'detail';
                break;
            case 'detail':
                this.item = results;
                this.selectedItem = this.item;
                this.item && this.initEditForm(this.selectedItem);
                this.controls = this.formGroup.controls;
                break;
            case 'put':
                this.editingItem = false;
                this.formReset();
                this.send_msg(results.message);
                this.httpMethod = 'detail';
                break;
            case 'delete':
                this.redirect && setTimeout(function () { return _this.router.navigate(["" + _this.redirectUponDelete]); }, 0);
                this.send_msg(results.message);
                this.httpMethod = 'get';
                break;
            default:
                console.log('No other option!');
        }
        var _a;
    };
    BaseComponent.prototype.onFailorResponse = function (error) {
        console.error('ERROR: ', error.message);
        this.send_msg(error.message);
    };
    BaseComponent.prototype.showForm = function (item) {
        if (item === void 0) { item = null; }
        this.clearMessage();
        switch (this.httpMethod) {
            case 'get':
            case 'post':
                this.creatingItem = true;
                console.log(this.itemName + " being created...");
                break;
            case 'detail':
            case 'put':
                this.selectedItem = item;
                this.editingItem = true;
                console.log(this.itemName + " being edited...");
                break;
            default:
                console.log('No form to show!');
        }
    };
    BaseComponent.prototype.hideForm = function () {
        this.formReset();
        this.creatingItem = false;
        console.log(this.itemName + " " + (this.httpMethod == 'get' ? 'create' : 'edit') + " aborted :)");
    };
    BaseComponent.prototype.formReset = function () {
        this.formGroup.reset();
        this.clearMessage();
    };
    BaseComponent.prototype.onSuccess = function (event) {
        this.successMsg = event;
    };
    BaseComponent.prototype.onFailor = function (event) {
        this.errorMsg = event;
    };
    BaseComponent.prototype.send_msg = function (msg) {
        this.message.emit(msg);
    };
    BaseComponent.prototype.clearMessage = function () {
        this.errorMsg = this.successMsg = '';
    };
    BaseComponent.prototype.setHeader = function (header) {
        var _this = this;
        setTimeout(function () {
            _this.header.setHeader({
                type: header.type,
                content: header.content
            });
        });
    };
    // REF: https://toddmotto.com/methods-to-determine-if-an-object-has-a-given-property/
    // For IE compatibility
    BaseComponent.prototype.hasProperty = function (obj, prop) {
        return Object.prototype.hasOwnProperty.call(obj, prop);
    };
    __decorate([
        ngx_webstorage_1.LocalStorage()
    ], BaseComponent.prototype, "userSettings");
    __decorate([
        core_1.Output()
    ], BaseComponent.prototype, "message");
    BaseComponent = __decorate([
        auto_unsubscribe_1.AutoUnsubscribe()
        // tslint:disable-next-line:component-selector
        ,
        core_1.Component({ selector: '', template: '' })
    ], BaseComponent);
    return BaseComponent;
}());
exports.BaseComponent = BaseComponent;
