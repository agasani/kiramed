import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { PagerService } from './pager.service';

// import { PagerService } from './pager.service';

@Component({
  selector: 'kira-pager',
  providers: [PagerService],
  templateUrl: './pager.component.html',
})

// <login-pager [allItems]="allItems" (pageChange)="onPageChange($event)"></login-pager>

export class PagerComponent implements OnInit {

  // pager object
  pager: any = {};

  // paged items
  @Output() pageChange: EventEmitter<any[]> = new EventEmitter<any[]>();

  // array of all items to be paged
  @Input() allItems: any[];

  // number of items per page
  @Input() pageSize: number;

  constructor(private pagerService: PagerService) {
  }

  ngOnInit() {
    this.setPage(1);
  }

  setPage(page: number) {

    if (page < 1 || page > this.pager.totalPages) return;

    // get pager object from service
    this.pager = this.pagerService.getPager(this.allItems.length, page, this.pageSize);

    // get current page of items
    setTimeout(() => this.pageChange
            .emit(this.allItems.slice(this.pager.startIndex, this.pager.endIndex + 1)),
        0);
    console.log(`Page no. ${this.pager.currentPage}`)
  }
}
