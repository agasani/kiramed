import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { Component } from '@angular/core';

@Component({ selector: '', template: '' })

export class HtmlElmComponent {
  protected route: ActivatedRoute;
  protected router: Router;
  protected titleService: Title;

  // USAGE EXAMPLE:
  //==============
  // ngOnInit() {
  //   this.pageTitle();
  // }
  //
  // ngAfterViewInit() {
  //   this.loadScript('../../../assets/demo/plugins/cookie/js.cookie.js');
  //   this.loadScript('../../../assets/demo/plugins/nicescrool/jquery.nicescroll.min.js');
  //   this.loadScript('../../../assets/demo/plugins/pacejs/pace.js');
  //   this.loadScript('../../../assets/demo/js/main.js');
  // }
  protected constructor() {
  }

  public loadScript(url: string) {
    const node = document.createElement('script');
    node.src = url;
    node.type = 'text/javascript';
    document.getElementsByTagName('body')[0].appendChild(node);
  }

  public pageTitle() {
    this.router.events
        .filter((event) => event instanceof NavigationEnd)
        .map(() => this.route)
        .map((route) => {
          while (route.firstChild) route = route.firstChild;
          return route;
        })
        .filter((route) => route.outlet === 'primary')
        .mergeMap((route) => route.data)
        .subscribe((event) => this.titleService.setTitle(event['title']));
  }
}
