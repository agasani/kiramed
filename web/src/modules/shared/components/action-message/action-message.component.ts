import { Component, Input } from '@angular/core';

@Component({
  selector: 'action-message',
  templateUrl: './action-message.component.html',
})

export class ActionMessageComponent {

  @Input() errorMsg: string;
  @Input() successMsg: string;

  constructor() {
  }
}
