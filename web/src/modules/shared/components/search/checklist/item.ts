interface IItem {
  name: string;
  value?: string;
  code?: string;
}

export class Item implements IItem {
  name: string;
  value?: string = 'nothing';
  code?: string;
}
