import { AfterViewInit, Component, Input, OnInit } from '@angular/core';
import { Item } from './item';

@Component({
  selector: 'kira-checklist-search',
  templateUrl: './checklist-search.component.html',
  styleUrls: ['checklist-search.component.css']
})

export class ChecklistSearchComponent implements OnInit, AfterViewInit {

  @Input() displayName;
  itemList: Item[];
  contentList: Item[] = [
    { value: 'css', name: 'CSS', code: 'ICD101111' },
    { value: 'design', name: 'Graphic Design', code: 'ICD101112' },
    { value: 'ember', name: 'Ember', code: 'ICD101113' },
    { value: 'html', name: 'HTML', code: 'ICD101114' },
    { value: 'ia', name: 'Information Architecture', code: 'ICD101115' },
    { value: 'javascript', name: 'Javascript', code: 'ICD101116' },
    { value: 'mech', name: 'Mechanical Engineering', code: 'ICD101117' },
    { value: 'meteor', name: 'Meteor', code: 'ICD101118' },
    { value: 'node', name: 'NodeJS', code: 'ICD101119' },
    { value: 'plumbing', name: 'Plumbing', code: 'ICD101129' },
    { value: 'python', name: 'Python', code: 'ICD101121' },
    { value: 'rails', name: 'Rails', code: 'ICD101122' },
    { value: 'react', name: 'React', code: 'ICD101123' },
    { value: 'repair', name: 'Kitchen Repair', code: 'ICD101124' },
    { value: 'ruby', name: 'Ruby', code: 'ICD101125' },
    { value: 'ui', name: 'UI Design', code: 'ICD101126' },
    { value: 'ux', name: 'User Experience', code: 'ICD101127' },
  ];
  private itemName: string;
  private itemCode: string;

  constructor() {
  }

  ngOnInit() {
    this.itemList = [];
  }

  ngAfterViewInit() {
    $('.ui.dropdown').dropdown({ allowAdditions: true });
  }

  addItem() {
    let index = this.itemList.findIndex(x => x.name == this.itemName);
    if (this.itemName) {
      if (index == -1) {
        this.itemCode = this.contentList.find(item => item.name == this.itemName).code;
        this.itemList.unshift({ name: this.itemName, code: this.itemCode });
        this.contentList = this.contentList.filter(obj => obj.name !== this.itemName);
      }
    }
  }

  deleteItem(name) {
    console.log(name);
    for (let i = 0; i < this.itemList.length; i++) {
      if (this.itemList[i]['name'] == name) {
        this.itemList.splice(i, 1);
      }
    }
  }
}
