import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { SpinnerService } from './spinner.service';

@Component({
  selector: 'spinner',
  templateUrl: './spinner.component.html'
})
// <spinner [loadingImage]="path/to/loading.gif" [show]="true"></spinner>
// <spinner name="mySpinner" [(show)]="isShowing"></spinner>
export class SpinnerComponent implements OnInit {
  @Input() name: string;
  @Input() group: string;
  @Input() loadingImage: string;
  @Output() showChange = new EventEmitter<boolean>();
  private isShowing = false;

  constructor(private spinnerService: SpinnerService) {
  }

  @Input()
  get show(): boolean {
    return this.isShowing;
  }

  set show(val: boolean) {
    this.isShowing = val;
    this.showChange.emit(this.isShowing);
  }

  ngOnInit(): void {
    if (!this.name) throw new Error('Spinner must have a \'name\' attribute.');

    this.spinnerService._register(this);
  }

  ngOnDestroy(): void {
    this.spinnerService._unregister(this);
  }
}
