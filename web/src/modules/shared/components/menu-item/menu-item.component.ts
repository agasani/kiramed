import { Component, Input } from '@angular/core';
import { BaseComponent } from '../base';

@Component({
  selector: 'kira-menu-item',
  templateUrl: './menu-item.component.html'
})
// +kira_menu_item(['v.dash', 'c.pat'], 'home', 'Dashboard', 'dashboard')
export class MenuItemComponent extends BaseComponent {

  @Input('item_label') menu_item_label: string;
  @Input('icon_class') menu_item_icon_class: string;
  @Input('router_link') menu_item_router_link: string;

  constructor() {
    super();
  }
}
