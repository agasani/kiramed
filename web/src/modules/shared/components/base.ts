import { AfterViewInit, Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { LocalStorage } from 'ngx-webstorage';
import { Observable } from 'rxjs/Observable';
import { FeedBack } from '../../api/models/feed-back';
import { AutoUnsubscribe } from '../decorators/auto-unsubscribe';
import { PageHeader, PageHeaderService } from '../services/page-header.service';

@AutoUnsubscribe()
// tslint:disable-next-line:component-selector
@Component({ selector: '', template: '' })
export class BaseComponent implements OnInit, AfterViewInit, OnDestroy {

  @LocalStorage()
  public userSettings: string;
  public settings = JSON.parse(this.userSettings) || '';
  public userId = this.settings['userId'] || '';
  public userOrgId = this.settings['userOrgId'] || '';
  public userCountryId = this.settings['countryOrgId'] || '';
  public userClaims = this.settings['claims'] || [''];
  @Output()
  public message: EventEmitter<string> = new EventEmitter<string>();
  public formGroup: FormGroup;
  public errorMsg = '';
  public successMsg = '';
  public redirect = false;
  public itemService: any;
  public itemName: string;
  public selectedItem: any;
  public creatingItem = false;
  public editingItem = false;
  public allItems: Array<any> = [];
  public pagedItems: Array<any>;
  public item: any;
  public itemId: any;
  public newItem: any;
  public route: ActivatedRoute;
  public router: Router;
  public header: PageHeaderService;
  public is_details: boolean;
  public is_system: boolean;
  public controls: any;
  public pageSize: number = 2;
  public redirectUponCreate: Array<any>;
  public redirectUponDelete: string;
  public firstUrlPart: string = '';
  public secondUrlPart: string = '';
  public getObservable: Observable<any>;
  public detailObservable: Observable<any>;
  public postObservable: Observable<FeedBack>;
  public putObservable: Observable<FeedBack>;
  public deleteObservable: Observable<FeedBack>;
  public respDataIdProperty: string = 'id';
  public httpMethod: string = '';
  public noHeader: PageHeader = { type: 0, content: null };
  public resolveData: Array<any> | any;

  protected constructor() {
  }

  ngOnInit() {
    //  Don't put any thing here!
  }

  ngAfterViewInit() {
    //  Don't put any thing here!
  }

  ngOnDestroy() {
    //  Don't put any thing here!
  }

  initCreateForm() {
    //  Don't put any thing here!
  }

  initEditForm(item: any): void {
    //  Don't put any thing here!
  }

  // CRUD METHODS
  // ITEMS_COMPONENT METHODS:
  getAll(data) {
    this.httpMethod = 'get';
    Observable.of(data).subscribe(
        (response) => this.onSuccessResponse(response),
        (error: FeedBack) => this.onFailorResponse(error),
        () => console.log(`${this.itemName} items listed`)
    )
  }

  create() {
    this.httpMethod = 'post';
    this.postObservable.subscribe(
        (response: FeedBack) => this.onSuccessResponse(response),
        (error: FeedBack) => this.onFailorResponse(error),
        () => console.log(`${this.itemName} item create aborted :)`)
    )
  }

  showInfo(item: any, id$: any = item.id): void {
    this.selectedItem = item;
    this.router.navigate([this.firstUrlPart + '/' + id$ + '/' + this.secondUrlPart]);
  }

  onPageChange(event) {
    this.pagedItems = event;
  }

  // ITEM_INFO_COMPONENT METHODS:
  getItem(data): any {
    this.httpMethod = 'detail';
    Observable.of(data).subscribe(
        (response: any) => this.onSuccessResponse(response),
        (error: FeedBack) => this.onFailorResponse(error),
        () => console.log(`${this.itemName} item detailed`)
    )
  }

  edit(): void {
    this.httpMethod = 'put';
    this.putObservable.subscribe(
        (response: FeedBack) => this.onSuccessResponse(response),
        (error: FeedBack) => this.onFailorResponse(error),
        () => console.log(`${this.itemName} item edit aborted`)
    )
  }

  destroy(item): void {
    this.item = item;
    this.httpMethod = 'delete';
    confirm(`Are you sure you want to delete this "${this.itemName}" of id: ${this.itemId} ?`) &&
    this.deleteObservable.subscribe(
        (response: FeedBack) => this.onSuccessResponse(response),
        (error: FeedBack) => this.onFailorResponse(error),
        () => console.log(`${this.itemName} item deleted`)
    )
  }

  protected sideBarMenuTrigger() {
    // Setup the sidebar..
    try {
      $('#clinical-sidebar')
          .sidebar({
            context: '#clinical-main',
            transition: 'overlay',
            mobileTransition: 'overlay'
          })
          .sidebar('attach events', '.amasidemenu', 'toggle');
    } catch (e) {
      console.log('An error happened initiating clinical sidebar: ', e);
    }
  }

  protected alertMessageTrigger() {
    try {
      $('.message .close').on('click', function () {
        $(this).closest('.message').transition('fade');
      });
    } catch (e) {
      console.log('Error occured on alert show!')
    }
  }

  protected onSuccessResponse(results: any[] | any | FeedBack) {

    switch (this.httpMethod) {

      case 'get':
        this.allItems.push(...results);
        this.initCreateForm();
        this.controls = this.formGroup.controls;
        break;

      case 'post':
        this.allItems.push(this.item);
        this.itemId = JSON.parse(results.data)[this.respDataIdProperty];
        this.redirectUponCreate = [`${this.firstUrlPart}/${this.itemId}/${this.secondUrlPart}`];
        this.redirect && this.router.navigate(this.redirectUponCreate);
        this.creatingItem = false;
        this.formReset();
        this.send_msg(results.message);
        this.httpMethod = 'detail';
        break;

      case 'detail':
        this.item = results;
        this.selectedItem = this.item;
        this.item && this.initEditForm(this.selectedItem);
        this.controls = this.formGroup.controls;
        break;

      case 'put':
        this.editingItem = false;
        this.formReset();
        this.send_msg(results.message);
        this.httpMethod = 'detail';
        break;

      case 'delete':
        this.redirect && setTimeout(() => this.router.navigate([`${this.redirectUponDelete}`]), 0);
        this.send_msg(results.message);
        this.httpMethod = 'get';
        break;

      default:
        console.log('No other option!');
    }
  }

  protected onFailorResponse(error: FeedBack) {
    console.error('ERROR: ', error.message);
    this.send_msg(error.message);
  }

  protected showForm(item = null): void {
    this.clearMessage();
    switch (this.httpMethod) {

      case 'get':
      case 'post':
        this.creatingItem = true;
        console.log(`${this.itemName} being created...`);
        break;

      case 'detail':
      case 'put':
        this.selectedItem = item;
        this.editingItem = true;
        console.log(`${this.itemName} being edited...`);
        break;

      default:
        console.log('No form to show!');
    }
  }

  protected hideForm(): void { // To abort the create action
    this.formReset();
    this.creatingItem = false;
    console.log(`${this.itemName} ${this.httpMethod == 'get' ? 'create' : 'edit'} aborted :)`)
  }

  protected formReset(): void {
    this.formGroup.reset();
    this.clearMessage()
  }

  protected onSuccess(event): void {
    this.successMsg = event;
  }

  protected onFailor(event): void {
    this.errorMsg = event;
  }

  protected send_msg(msg: string): void {
    this.message.emit(msg);
  }

  protected clearMessage(): void {
    this.errorMsg = this.successMsg = '';
  }

  protected setHeader(header: PageHeader) {
    setTimeout(() => {
      this.header.setHeader({
        type: header.type,
        content: header.content
      })
    });
  }

  // REF: https://toddmotto.com/methods-to-determine-if-an-object-has-a-given-property/
  // For IE compatibility
  protected hasProperty(obj: any, prop: any): boolean {
    return Object.prototype.hasOwnProperty.call(obj, prop);
  }
}
