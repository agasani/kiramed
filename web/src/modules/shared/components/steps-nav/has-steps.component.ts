import { Component } from '@angular/core';
import { Step } from './step';

@Component({ selector: '', template: '' })

// We'll use Typescript Mixins if a Component need to inherit from both HasStepsComponent and BaseComponent
// REF:
// ===
// https://stackoverflow.com/questions/26948400/typescript-how-to-extend-two-classes?answertab=active#tab-top
// https://www.typescriptlang.org/docs/handbook/mixins.html
export class HasStepsComponent {
  steps: Step[];
  review: Step; // no descr
  head: Step; // no descr & url
  nav: any; // exporter for nav options

  constructor() {
  }
}
