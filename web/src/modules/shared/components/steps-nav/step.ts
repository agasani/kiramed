export class Step {
  'title': string;
  'descr'?: string | null;
  'url'?: string | null;
  'icon': string;
  'active'?: boolean = true; // The step will be active only if it's previous is valid
}
