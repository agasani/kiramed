import { Component, Input } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { SharedService } from '../../services/shared-service';

@Component({
  selector: 'kira-steps-nav',
  templateUrl: './steps-nav.component.html',
})
export class StepsNavComponent {
  @Input() nav: any;
  @Input() checkable: boolean;
  validStep: number = 0;
  subscription: Subscription;

  constructor(private _sharedService: SharedService) {
    this.subscription = this._sharedService.changeEmitted$.subscribe(step => {
      this.validStep = step;
      console.log('Received: ', this.validStep)
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
