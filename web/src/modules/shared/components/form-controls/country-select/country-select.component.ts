import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { CountrySelectService } from './country-select.service';
import { IcountrySelect } from './Icountry-select';

@Component({
  selector: 'kira-country-select',
  templateUrl: './country-select.component.html',
  providers: [CountrySelectService]
})
export class CountrySelectComponent implements OnInit {
  @Input()
  group: FormGroup;
  countries: IcountrySelect[];

  constructor(private _countrySelect: CountrySelectService) {
  }

  ngOnInit() {
    this.getCountries();
  }

  getCountries() {
    this._countrySelect.get().subscribe(
        countries => this.countries = countries,
        error => console.error('Error in country select!'),
        () => console.log('Countries select list complete!')
    );
  }
}
