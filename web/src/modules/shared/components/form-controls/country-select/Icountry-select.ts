export interface IcountrySelect {
  value: string;
  flag: string;
  name: string
}
