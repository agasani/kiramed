export interface JQueryEx extends JQuery {
  calendar(setting: Settings): void;

  calendar(behavior: string, ...args: any[]): void;
}

export interface Settings {
  type?: string;
  firstDayOfWeek?: number;
  constantHeight?: boolean;
  today?: boolean;
  closable?: boolean;
  monthFirst?: boolean;
  touchReadonly?: boolean;
  inline?: boolean;
  on?: string;
  initialDate?: Date;
  startMode?: boolean;
  minDate?: Date;
  maxDate?: Date;
  ampm?: boolean;
  disableYear?: boolean;
  disableMonth?: boolean;
  disableMinute?: boolean;
  formatInput?: boolean;
  startCalendar?: JQueryEx;
  endCalendar?: JQueryEx;
  multiMonth?: number;
  popupOptions?: {
    position?: string,
    lastResort?: string,
    prefer?: string,
    hideOnScroll?: boolean,
  };

  text?: {
    days?: string[],
    months?: string[],
    monthsShort?: string[],
    today?: string,
    now?: string,
    am?: string,
    pm?: string,
  };

  formatter?: {
    header?: (date: Date, mode: string, settings: Settings) => string,
    yearHeader?: (date: Date, settings: Settings) => string,
    monthHeader?: (date: Date, settings: Settings) => string,
    dayHeader?: (date: Date, settings: Settings) => string,
    hourHeader?: (date: Date, settings: Settings) => string,
    minuteHeader?: (date: Date, settings: Settings) => string,
    dayColumnHeader?: (day: Date, settings: Settings) => string,
    datetime?: (date: Date, settings: Settings) => string,
    date?: (date: Date, settings: Settings) => string,
    time?: (date: Date, settings: Settings, forCalendar: JQueryEx) => string,
    today?: (settings: Settings) => string,
  };

  parser?: {
    date: (text: string, settings: Settings) => Date,
  };

  onChange?: (date: Date, text: string, mode: string) => any;
  onShow?: () => any;
  onVisible?: () => any;
  onHide?: () => any;
  onHidden?: () => any;
  isDisabled?: (date: Date, mode: string) => boolean;

  selector?: {
    popup?: string,
    input?: string,
    activator?: string,
  };

  regExp?: {
    dateWords?: RegExp,
    dateNumbers?: RegExp,
  };

  error?: {
    popup?: string,
    method?: string,
  };

  className?: {
    calendar?: string,
    active?: string,
    popup?: string,
    grid?: string,
    column?: string,
    table?: string,
    prev?: string,
    next?: string,
    prevIcon?: string,
    nextIcon?: string,
    link?: string,
    cell?: string,
    disabledCell?: string,
    adjacentCell?: string,
    activeCell?: string,
    rangeCell?: string,
    focusCell?: string,
    todayCell?: string,
    today?: string,
  };

  metadata?: {
    date?: string,
    focusDate?: string,
    startDate?: string,
    endDate?: string,
    mode?: string,
    monthOffset?: string,
  };
}
