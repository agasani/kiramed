import { AfterContentInit, AfterViewInit, Component, ElementRef, forwardRef, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { JQueryEx, Settings } from './calendar.options';
import { HtmlElmComponent } from '../../html-elm/html-elm.component';

@Component({
  selector: 'kira-sui-calendar',
  templateUrl: './sui-calendar.component.html',
  styleUrls: ['./sui-calendar.component.scss'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => SuiCalendarComponent),
    multi: true
  }]
})

// REF:
// https://embed.plnkr.co/fnCJwq/
// https://gist.github.com/icepeng/b3c13b21a1781a7159a1f0cfe7e47ba3
// https://gist.github.com/ihadeed/5f73e703897318e86521d5e5008347d8
// https://coryrylan.com/blog/angular-custom-form-controls-with-reactive-forms-and-ngmodel
// https://material.angular.io/guide/creating-a-custom-form-field-control
// https://codepen.io/SaadRegal/pen/ZOABQr
export class SuiCalendarComponent extends HtmlElmComponent implements AfterViewInit, AfterContentInit, ControlValueAccessor {

  public onChange: any = Function.prototype;
  public onTouched: any = Function.prototype;
  @Input() startCalendar: SuiCalendarComponent;
  @Input() endCalendar: SuiCalendarComponent;
  @Input() initialDate: Date;
  @Input() minDate: Date;
  @Input() maxDate: Date;
  @Input() placeholder: string;
  @Input() type = 'date';
  private _elementRef: ElementRef;
  private selected: Date;
  private element: JQueryEx;

  constructor(elementRef: ElementRef) {
    super();
    this._elementRef = elementRef;
  }

  private _dateValue: string;

  get dateValue(): string {
    return this._dateValue;
  }

  set dateValue(value: string) {
    this._dateValue = value;
  }

  ngAfterViewInit() {
    // The calendar script may be only available for this component.
    this.loadScript('./../../../../../resources/vendors/semantic-ui/calendar/calendar.min.js');
  }

  ngAfterContentInit(): void {
    const item = this._elementRef.nativeElement;
    const setting = this.buildSettings();

    this.element = <JQueryEx>jQuery(item).children('.ui.calendar');
    this.element.calendar(setting);
    // this.element.calendar('set date', '');
  }

  writeValue(value: Date): void {
    if (value === this.selected) return;
    this.selected = value;
    this.dateValue = this.formatDate(this.selected);
  }

  registerOnChange(fn: (date: Date) => void): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: () => void): void {
    this.onTouched = fn;
  }

  onBlur(): void {
    this.onTouched();
  }

  private buildSettings(): Settings {
    const self = this;
    const settings: Settings = {
      type: self.type,
      minDate: self.minDate,
      maxDate: self.maxDate,
      formatter: { date: self.formatDate },
      onChange: (date: Date) => {
        self.writeValue(date);
        self.onChange(date);
      },
    };

    if (this.startCalendar) settings.startCalendar = <JQueryEx>jQuery(this.startCalendar._elementRef.nativeElement);
    else if (this.endCalendar) settings.endCalendar = <JQueryEx>jQuery(this.endCalendar._elementRef.nativeElement);

    return settings;
  }

  private formatDate(date: Date): string {
    if (!date) return '';

    let day = date.getDate() + '', month = (date.getMonth() + 1) + '', year = date.getFullYear();
    if (day.length < 2) day = '0' + day;
    if (month.length < 2) month = '0' + month;

    return year + '/' + month + '/' + day;
  }
}
