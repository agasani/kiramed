import { Component, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ValidationService } from '../../../services/validation';
import { BaseComponent } from '../../base';

@Component({
  selector: 'control-messages',
  templateUrl: './control-messages.component.html'
})

export class ControlMessagesComponent extends BaseComponent {

  @Input() control: FormControl;

  constructor() {
    super();
  }

  get errorMessage(): string | null {

    if (!this.control) {
      return '';
    }

    for (let propertyName in this.control.errors) {
      if (this.hasProperty(this.control.errors, propertyName) && this.control.touched)
        return ValidationService.getValidatorErrorMessage(propertyName, this.control.errors[propertyName]);
    }

    return null;
  }
}
