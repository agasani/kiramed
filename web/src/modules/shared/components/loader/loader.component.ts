import { Component, OnDestroy, OnInit } from '@angular/core';

import { LoaderService } from './loader.service';
import { AutoUnsubscribe } from '../../decorators/auto-unsubscribe';

@AutoUnsubscribe()
@Component({
  selector: 'kira-loader',
  templateUrl: 'loader.component.html',
  styleUrls: ['loader.component.scss'],
})
export class LoaderComponent implements OnInit, OnDestroy {

  private isLoading = false;

  constructor(private loaderService: LoaderService) {
  }

  ngOnInit() {
    this.loaderService.loaderState.map(state => state.show)
        .subscribe(loading => this.isLoading = loading);
  }

  ngOnDestroy() {
  }
}