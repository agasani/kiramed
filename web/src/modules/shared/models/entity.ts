export class Entity {
  id?: number;
  externalId: string;
  type: string; // [StringLength(2)]
  category: string;
  lastName: string;
  firstName: string;
  middleName: string;
  organizationId: number;
  isActive: boolean;
  entryDate?: string;
  specialty: string; // [StringLength(3)]
  demographic: EntityDemographicViewModel;
  contacts: EntityContactViewModel[];
  addresses: EntityAddressViewModel[];
}

export class EntityRegistrationViewModel {
  countryId: number; // [Required]
  firstName: string; // [Required]
  secondName: string; // [Required]
  nationalId: string; // [Required]
  parentId?: number;
  email: string; // [Required] [EmailAddress]
  phoneNumber: string; // [Required]
  middleName?: string;
  // demographic ?: EntityDemographicViewModel;
  // contacts ?: EntityContactViewModel[];
  // addresses ?: EntityAddressViewModel[];
}

export class EntityDemographicViewModel {
  id?: number; // [Required]
  secondaryLanguage?: string; // [StringLength(2)]
  primaryLanguage?: string; // [StringLength(2)]
  nationalID?: string;
  dob: string; // [Required]
  sex?: string; // [StringLength(2)]
  martalStatus?: string; // [StringLength(2)]
  religion?: string; // [StringLength(2)]
  education?: string; // [StringLength(2)]
  income: number; // [Required]
}

export class EntityContactViewModel {
  id?: number; // [Required]
  contactTypeId: number; // [Required]
  value?: string;
  label?: string;
  beginDate: string; // [Required]
  endDate?: string;
  deactivate: boolean;

  constructor() {
    this.beginDate = new Date().toISOString();
    this.endDate = new Date(this.endDate).toISOString();
  }
}

export class EntityAddressViewModel {
  id?: number; // [Required]
  label?: string;
  directions?: string;
  address1?: string;
  address2?: string;
  city?: string;
  state?: string;
  location?: string;
  timeZone: number; // [Required]
  timeZoneId: number; // [Required]
  hasDaylightSaving: boolean;
  zip?: string;
  district?: string;
  isMailingAddress: boolean;
  country: string; // [Required] [StringLength(2)]
  beginDate: string; // [Required]
  endDate?: string;
  deactivate: boolean;
}