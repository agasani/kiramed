"use strict";
exports.__esModule = true;
var Email = /** @class */ (function () {
    function Email(msg, sbj, fromEmail, fromContact) {
        this.Body = msg;
        this.Subject = sbj;
        this.From = fromEmail;
        this.FromContact = fromContact;
    }
    return Email;
}());
exports.Email = Email;
