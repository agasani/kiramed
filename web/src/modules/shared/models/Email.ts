﻿export class Email {

  public Body: string;
  public Subject: string;
  public From: string;
  public FromContact: string;

  constructor(msg: string, sbj: string, fromEmail: string, fromContact: string) {

    this.Body = msg;
    this.Subject = sbj;
    this.From = fromEmail;
    this.FromContact = fromContact;
  }

}