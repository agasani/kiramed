export class Feedback {
  public err: boolean = false;
  public message: string = '';
  public data: string = ''
}
