"use strict";
Object.defineProperty(exports, "__esModule", {value: true});
var Entity = /** @class */ (function () {
    function Entity() {
    }

    return Entity;
}());
exports.Entity = Entity;
var EntityRegistrationViewModel = /** @class */ (function () {
    function EntityRegistrationViewModel() {
    }

    return EntityRegistrationViewModel;
}());
exports.EntityRegistrationViewModel = EntityRegistrationViewModel;
var EntityDemographicViewModel = /** @class */ (function () {
    function EntityDemographicViewModel() {
    }

    return EntityDemographicViewModel;
}());
exports.EntityDemographicViewModel = EntityDemographicViewModel;
var EntityContactViewModel = /** @class */ (function () {
    function EntityContactViewModel() {
        this.beginDate = new Date().toISOString();
        this.endDate = new Date(this.endDate).toISOString();
    }

    return EntityContactViewModel;
}());
exports.EntityContactViewModel = EntityContactViewModel;
var EntityAddressViewModel = /** @class */ (function () {
    function EntityAddressViewModel() {
    }

    return EntityAddressViewModel;
}());
exports.EntityAddressViewModel = EntityAddressViewModel;
//# sourceMappingURL=entity.js.map