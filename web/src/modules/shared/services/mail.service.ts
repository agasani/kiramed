﻿import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { Email } from '../Models/Email';

import 'rxjs/add/operator/toPromise';


@Injectable()
export class MailService {

  private mailUrl = 'http://kiramed.azurewebsites.net/api/Mail';
  private headers = new Headers({ 'Content-Type': 'application/json' });

  constructor(private http: Http) {
  }

  emailKiramed(msg: string, subject: string, fromEmail: string, fromPhone: string, cb: any): void {
    const email = new Email(msg, subject, fromEmail, fromPhone);

    this.http
        .post(this.mailUrl, JSON.stringify(email), { headers: this.headers })
        .toPromise()
        .then(function (resp) {

          console.log('got the response: ', resp);

          //TODO fix the following, resp.json() is throwing an error in mozilla..
          const response = resp.json();
          //console.log('after json(): ', response);

          if (response == 'success') {
            $('.ui.modal').modal('hide');
          }
          else {
            console.log('FAILED: ', resp);
          }

          cb(resp);
        })
        .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

}
