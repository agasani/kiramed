import { Injectable } from '@angular/core';
import { JwtHelper, tokenNotExpired } from 'angular2-jwt';
import { HttpClient, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
import { ActivatedRoute, Router } from '@angular/router';
import { DEFAULT_INTERRUPTSOURCES, Idle } from '@ng-idle/core';
import { LocalStorage, LocalStorageService } from 'ngx-webstorage';
import { UserLoginViewModel } from '../../api/models/user-login-view-model';
import { UserService } from '../../api/services/user.service';

//http://www.enhgo.com/snippet/typescript/authenticationservicets_joshharms_typescript
//https://gist.github.com/joshharms/00d8159900897dc5bed45757e30405f9
@Injectable()
export class AuthService {

  public urlsAuthConfig: any = environment.urls.authConfig;
  public clientId: string = this.urlsAuthConfig.clientId;
  public clientSecret: string = this.urlsAuthConfig.clientSecret;
  private audienceId: string = this.urlsAuthConfig.audienceId;
  private grantType: string = this.urlsAuthConfig.grantType;
  private scope: string = this.urlsAuthConfig.scope;
  public redirectUrl: string = '/emr';
  public baseUrl: string = '/connect/token';
  @LocalStorage()
  private userSettings: string;
  private settings = JSON.parse(this.userSettings);
  private cachedRequests: Array<HttpRequest<any>> = [];
  private timedOut: boolean;

  constructor(private _http: HttpClient,
              private jwt: JwtHelper,
              private router: Router,
              private route: ActivatedRoute,
              private storage: LocalStorageService,
              private userService: UserService,
              private idle: Idle) {
  }

  public getToken(): string {
    return !!this.settings ? this.settings['accessToken'] : '';
  }

  public getRefreshToken(): string {
    return this.settings['refreshToken'];
  }

  public isAuthenticated(): Observable<boolean> {
    const token: string = this.getToken();
    return Observable.of(tokenNotExpired(null, token));
  }

  public refreshToken(): Observable<any> {

    const bodyContent: any = {
      'refresh_token': this.getRefreshToken() as string,
      'grant_type': 'refresh_token' as string,
      'client_id': this.clientId as string,
    };

    return this._http
        .post(this.baseUrl, bodyContent).map((resp: any) => {
              if (resp) {
                this.storage.store('access_token', resp.json().access_token);
                this.storage.store('refresh_token', resp.json().refresh_token);
              }
            }
        ).catch(err => Observable.of(false));
  }

  public collectFailedRequest(request): void {
    this.cachedRequests.push(request);
  }

  public retryFailedRequests(): void {
    const headers = { 'Authorization': `Bearer ${ this.getToken() }` };
    this.cachedRequests.forEach(request => request.clone({ setHeaders: headers }));
  }

  public trackUserActivity() {
    const timeoutPeriod: number = environment.idleTimeout;
    this.idle.setIdle(5);
    this.idle.setTimeout(timeoutPeriod);
    this.idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);
    this.idle.onTimeout.subscribe(() => {
      this.logout();
      this.timedOut = true;
      console.log(`You have been inactive for more than ${timeoutPeriod / 60} min, you are logged out!`);
    });
    this.idleTimeoutReset();
  }

  public post(item: UserLoginViewModel): Observable<any> {

    const bodyContent = `${
        encodeURIComponent('grant_type')}=${encodeURIComponent(this.grantType)}&${
        encodeURIComponent('scope')}=${encodeURIComponent(this.scope)}&${
        encodeURIComponent('username')}=${encodeURIComponent(item.userName)}&${
        encodeURIComponent('password')}=${encodeURIComponent(item.password)}`;

    return this._http.post(this.baseUrl, bodyContent).map(resp => this.storeUserSettings(resp));
  }

  public logout() {
    this.idle.stop();
    this.storage.clear('userSettings');
    this.router.navigate(['/login']);
    console.log('Logged out!')
  }

  private storeUserSettings(response): void {

    const token = response['access_token'];
    const expiry = response['expires_in'];
    const refreshToken = response['refresh_token'];
    const tokenType = response['token_type'];
    const decodedToken = this.jwt.decodeToken(token);
    const claims = decodedToken['http://schemas.xmlsoap.org/ws/2005/05/identity/claims/authorizationdecision'];
    const userId = decodedToken['sub'];

    this.getUserOrg(userId);
    const userOrgId = this.storage.retrieve('userOrgId');
    const userCountryId = this.storage.retrieve('userCountryId');

    this.storage.clear('userOrgId') && this.storage.clear('userCountryId');

    if (this.settings) {
      this.settings['accessToken'] = token;
      this.settings['refreshToken'] = refreshToken;
      this.settings['expiresIn'] = expiry;
      this.settings['tokenType'] = tokenType;
      this.settings['claims'] = claims;
      this.settings['userId'] = userId;
      this.settings['userOrgId'] = userOrgId;
      this.settings['userCountryId'] = userCountryId;

      this.storage.store('userSettings', JSON.stringify(this.settings)); // Update userSettings
    } else {
      let new_settings = {
        'accessToken': token,
        'refreshToken': refreshToken,
        'expiresIn': expiry,
        'tokenType': tokenType,
        'claims': claims,
        'userId': userId,
        'userOrgId': userOrgId,
        'userCountryId': userCountryId,
      };

      this.storage.store('userSettings', JSON.stringify(new_settings)); // Create userSettings
    }

    this.trackUserActivity();
  }

  private getUserOrg(userId: string) {
    this.userService.ApiUserGetUserOrgsByUserIdGet({ userId: userId })
        .subscribe(
            result => {
              let userOrgs = result;
              this.storage.store('userOrgId', userOrgs[0].id);
              this.storage.store('userCountryId', userOrgs[0].countryId);
            },
            error => console.error(error.message)
        )
    ;
  }

  private idleTimeoutReset() {
    this.idle.watch();
    this.timedOut = false;
    console.log('User activity tracking started.');
  }
}
