import { Injectable } from '@angular/core';
import { PRIMARY_OUTLET, Router, UrlSegment, UrlSegmentGroup, UrlTree } from '@angular/router';

@Injectable()
export class RouteSegment {

  constructor(private router: Router) {
  }

  path(position: number): string {
    // REF: https://angular.io/api/router/UrlSegment
    const tree: UrlTree = this.router.parseUrl(this.router.url);
    const g: UrlSegmentGroup = tree.root.children[PRIMARY_OUTLET];
    const s: UrlSegment[] = g.segments;

    return s[position].path;
  }
}
