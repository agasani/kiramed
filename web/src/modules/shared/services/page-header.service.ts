import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

export interface PageHeader {
  type: number,
  content: string,
}

@Injectable()
export class PageHeaderService {
  public header: BehaviorSubject<PageHeader> = new BehaviorSubject<PageHeader>({ type: 0, content: null });

  setHeader(value: any) {
    this.header.next(value);
  }
}
