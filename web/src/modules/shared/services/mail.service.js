"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var Email_1 = require("../Models/Email");
require("rxjs/add/operator/toPromise");
var MailService = /** @class */ (function () {
    function MailService(http) {
        this.http = http;
        this.mailUrl = 'http://kiramed.azurewebsites.net/api/Mail';
        this.headers = new http_1.Headers({ 'Content-Type': 'application/json' });
    }
    MailService.prototype.emailKiramed = function (msg, subject, fromEmail, fromPhone, cb) {
        var email = new Email_1.Email(msg, subject, fromEmail, fromPhone);
        this.http
            .post(this.mailUrl, JSON.stringify(email), { headers: this.headers })
            .toPromise()
            .then(function (resp) {
            console.log('got the response: ', resp);
            //TODO fix the following, resp.json() is throwing an error in mozilla..
            var response = resp.json();
            //console.log('after json(): ', response);
            if (response == 'success') {
                $('.ui.modal').modal('hide');
            }
            else {
                console.log('FAILED: ', resp);
            }
            cb(resp);
        })["catch"](this.handleError);
    };
    MailService.prototype.handleError = function (error) {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    };
    MailService = __decorate([
        core_1.Injectable()
    ], MailService);
    return MailService;
}());
exports.MailService = MailService;
