import { environment } from '../../../environments/environment';
import { Injectable } from '@angular/core';
import { ApiConfiguration } from '../../api/api-configuration';

export function initApiConfiguration(config: ApiConfiguration): Function {
  return () => {
    config.rootUrl = environment.urls.apiServer;
  };
}

@Injectable()
export class AuthConfiguration {
  public Authority: string = environment.urls.authConfig.authority;
}

