"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
var core_1 = require("@angular/core");
var shared_module_1 = require("../shared/shared.module");
var components_1 = require("./components");
var kira_routing_1 = require("./kira.routing");
var KiraModule = /** @class */ (function () {
    function KiraModule() {
    }
    KiraModule = __decorate([
        core_1.NgModule({
            imports: [shared_module_1.SharedModule, kira_routing_1.kiraRouting],
            declarations: [components_1.KiraComponent, components_1.LandingPageComponent, components_1.LoginComponent, components_1.PageNotFound404Component]
        })
    ], KiraModule);
    return KiraModule;
}());
exports.KiraModule = KiraModule;
