﻿import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { KiraComponent, LandingPageComponent, LoginComponent, PageNotFound404Component } from './components';

const ROUTES: Routes = [
  {
    path: '',
    component: KiraComponent,
    children: [
      { path: 'login', redirectTo: 'login', pathMatch: 'full' },
      { path: 'logout', redirectTo: 'login', pathMatch: 'full' },
      { path: 'not-found', component: PageNotFound404Component, data: { title: 'Page not found' } },
      { path: 'login', component: LoginComponent, data: { title: 'User login' } },
      { path: '', component: LandingPageComponent },
    ]
  },
];

export const kiraRouting: ModuleWithProviders = RouterModule.forChild(ROUTES);
