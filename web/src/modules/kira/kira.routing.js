"use strict";
exports.__esModule = true;
var router_1 = require("@angular/router");
var components_1 = require("./components");
var ROUTES = [
    {
        path: '',
        component: components_1.KiraComponent,
        children: [
            { path: 'login', redirectTo: 'login', pathMatch: 'full' },
            { path: 'logout', redirectTo: 'login', pathMatch: 'full' },
            { path: 'not-found', component: components_1.PageNotFound404Component, data: { title: 'Page not found' } },
            { path: 'login', component: components_1.LoginComponent, data: { title: 'User login' } },
            { path: '', component: components_1.LandingPageComponent },
        ]
    },
];
exports.kiraRouting = router_1.RouterModule.forChild(ROUTES);
