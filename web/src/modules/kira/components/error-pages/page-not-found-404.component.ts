import { Component } from '@angular/core';

@Component({
  selector: 'not-found-404',
  templateUrl: './page-not-found-404.component.html',
})

export class PageNotFound404Component {

  message = 'ERROR 404.';

  constructor() {
  }
}
