import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../../shared/services/auth.service';

@Component({ selector: 'kira-landing-page', template: '' })

export class LandingPageComponent implements OnInit {

  constructor(private auth: AuthService, private router: Router) {
  }

  ngOnInit() {
    const returnUrl = this.auth.isAuthenticated() ? 'emr' : 'login';
    this.router.navigate([returnUrl])
  }
}
