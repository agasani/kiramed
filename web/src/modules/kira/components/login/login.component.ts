﻿import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { UserLoginViewModel } from '../../../api/models/user-login-view-model';
import { BaseComponent } from '../../../shared/components';
import { AuthService } from '../../../shared/services';
import { KiraService } from '../../../api/services/kira.service';
import { Patient } from '../../../api/models/patient';
import { UserService } from '../../../api/services/user.service';
import { PageHeaderService } from '../../../shared/services/page-header.service';

@Component({
  selector: 'login',
  templateUrl: './login.html',
  styleUrls: ['../kira.scss']
})

export class LoginComponent extends BaseComponent {
  failorMessage: string;
  myPatients: Patient[];

  constructor(router: Router,
              userService: UserService,
              route: ActivatedRoute,
              private fb: FormBuilder,
              private auth: AuthService,
              private kiraService: KiraService,
              headerService: PageHeaderService) {
    super();
    this.router = router;
    this.route = route;
    this.itemService = userService;
    this.failorMessage = '';
    this.header = headerService;
  }

  ngOnInit() {
    if (this.auth.isAuthenticated()) this.auth.logout();
    this.initLoginForm();
    this.controls = this.formGroup.controls;
    this.setHeader(this.noHeader);
  }

  initLoginForm(): void {
    this.formGroup = this.fb.group({
      userName: ['', [Validators.required, Validators.minLength(6)]],
      password: ['', [Validators.required, Validators.minLength(6)]],
    });
  }

  onLogin(user: UserLoginViewModel): void {
    this.auth.post(user).subscribe(
        auth_resp => this.kiraService.ApiKiraGet().subscribe(kira_resp => this.myPatients = kira_resp),
        auth_err => this.failorMessage = auth_err,
        () => this.router.navigate([this.auth.redirectUrl])
    );
  }
}