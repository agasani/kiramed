"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
exports.__esModule = true;
__export(require("./error-pages/page-not-found-404.component"));
__export(require("./kira.component"));
__export(require("./landing-page/landing-page.component"));
__export(require("./login/login.component"));
