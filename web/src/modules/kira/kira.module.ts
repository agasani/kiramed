﻿import { NgModule } from '@angular/core';

import { SharedModule } from '../shared/shared.module';

import { KiraComponent, LandingPageComponent, LoginComponent, PageNotFound404Component } from './components';
import { kiraRouting } from './kira.routing';

@NgModule({
  imports: [SharedModule, kiraRouting],
  declarations: [KiraComponent, LandingPageComponent, LoginComponent, PageNotFound404Component]
})
export class KiraModule {
}
