import { Injectable } from '@angular/core';
import { MockBackend, MockConnection } from '@angular/http/testing';
import { APPOINTMENTS } from '../data';
import { Response, ResponseOptions } from '@angular/http';

@Injectable()
export class MockBackendAppointmentsService {

  private endPoint: string;
  private data: any;
  private serviceName: string;

  constructor(private backend: MockBackend) {
    this.data = APPOINTMENTS;
    this.endPoint = 'appointments';
    this.serviceName = 'MockBackendTransfersService'
  }

  start(): void {
    console.log(`${this.serviceName} starts`);
    this.backend.connections.subscribe((c: MockConnection) => {
      console.log('mockConnection url:: ' + c.request.url);
      const URL = `http://localhost:8080/${this.endPoint}`;

      if (c.request.url === URL && c.request.method === 0) {
        console.log(JSON.stringify(this.data));
        c.mockRespond(new Response(new ResponseOptions({
          body: JSON.stringify(this.data)
        })));
      }
    });
  }
}
