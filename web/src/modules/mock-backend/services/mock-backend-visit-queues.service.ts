import { Injectable } from '@angular/core';
import { MockBackend, MockConnection } from '@angular/http/testing';
import { VISIT_QUEUES } from '../data';
import { Response, ResponseOptions } from '@angular/http';

@Injectable()
export class MockBackendVisitQueuesService {

  private endPoint: string;
  private data: any;
  private serviceName: string;

  constructor(private backend: MockBackend) {
    this.data = VISIT_QUEUES;
    this.endPoint = 'visitQueues';
    this.serviceName = 'MockBackendVisitQueuesService'
  }

  start(): void {
    console.log(`${this.serviceName} start`);
    this.backend.connections.subscribe((c: MockConnection) => {
      console.log('mockConnection url:: ' + c.request.url);
      const URL = `http://localhost:8080/${this.endPoint}`;

      if (c.request.url === URL && c.request.method === 0) {
        console.log(JSON.stringify(this.data));
        c.mockRespond(new Response(new ResponseOptions({
          body: JSON.stringify(this.data)
        })));
      }
    });
  }
}
