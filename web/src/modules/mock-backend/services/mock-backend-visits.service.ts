import { Injectable } from '@angular/core';
import { MockBackend, MockConnection } from '@angular/http/testing';
import { VISITS } from '../data';
import { Response, ResponseOptions } from '@angular/http';

@Injectable()
export class MockBackendVisitsService {
  constructor(private backend: MockBackend) {
  }

  start(): void {
    console.log('MockBackendVisitsService start');
    this.backend.connections.subscribe((c: MockConnection) => {
      console.log('mockConnection url:: ' + c.request.url);
      const URL = 'http://localhost:8080/visits';
      let visitsIdRegex = /\/visits\/([0-9]+)/i;

      if (c.request.url === URL && c.request.method === 0) {
        console.log(JSON.stringify(VISITS));
        c.mockRespond(new Response(new ResponseOptions({
          body: JSON.stringify(VISITS)
        })));
      } else if (c.request.url.match(visitsIdRegex) && c.request.method === 0) {
        let matches = VISITS.filter((visit) => {
          return visit.id === +(c.request.url.match(visitsIdRegex)[1])
        });
        c.mockRespond(new Response(new ResponseOptions({
          body: JSON.stringify(matches[0])
        })));
      }
    });
  }
}