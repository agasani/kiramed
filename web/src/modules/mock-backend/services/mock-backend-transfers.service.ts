import { Injectable } from '@angular/core';
import { MockBackend, MockConnection } from '@angular/http/testing';
import { Response, ResponseOptions } from '@angular/http';
import { TRANSFERS } from '../data';

@Injectable()
export class MockBackendTransfersService {

  private endPoint: string;
  private data: any;
  private serviceName: string;

  constructor(private backend: MockBackend) {
    this.data = TRANSFERS;
    this.endPoint = 'transfers';
    this.serviceName = 'MockBackendTransfersService'
  }

  start(): void {
    console.log(`${this.serviceName} start`);
    this.backend.connections.subscribe((c: MockConnection) => {
      console.log('mockConnection url:: ' + c.request.url);
      const URL = `http://localhost:8080/${this.endPoint}`;

      if (c.request.url === URL && c.request.method === 0) {
        console.log(JSON.stringify(this.data));
        c.mockRespond(new Response(new ResponseOptions({
          body: JSON.stringify(this.data)
        })));
      }
    });
  }
}
