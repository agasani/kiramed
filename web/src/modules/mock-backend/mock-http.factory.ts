import { MockBackend } from '@angular/http/testing';
import { BaseRequestOptions, Http } from '@angular/http';

export function mockHTTPFactory(backend: MockBackend, options: BaseRequestOptions) {
  return new Http(backend, options);
}