export const TRANSFERS: any[] = [
  {
    'dateTimeCreated': '2017-11-11T15:08:20.7175857',
    'arrivalDateTime': '2017-11-12T09:08:20.7175857',
    'receivingOrganizationId': 1,
    'relationship': 1,
    'originOrganizationId': 2,
    'nationalPatientId': 'RW6589878547',
    'transferPhysicianUserId': null
  },
  {
    'dateTimeCreated': '2017-11-11T15:08:20.7185868',
    'arrivalDateTime': '2017-11-12T09:08:20.7185868',
    'receivingOrganizationId': 2,
    'relationship': 1,
    'originOrganizationId': 1,
    'nationalPatientId': 'RW7757489339',
    'transferPhysicianUserId': null
  },
  {
    'dateTimeCreated': '2017-11-11T15:08:20.7175857',
    'arrivalDateTime': '2017-11-12T09:08:20.7175857',
    'receivingOrganizationId': 1,
    'relationship': 1,
    'originOrganizationId': 3,
    'nationalPatientId': 'RW5689878548',
    'transferPhysicianUserId': null
  },
  {
    'dateTimeCreated': '2017-11-11T15:08:20.7185868',
    'arrivalDateTime': '2017-11-12T09:08:20.7185868',
    'receivingOrganizationId': 2,
    'relationship': 1,
    'originOrganizationId': 1,
    'nationalPatientId': 'RW7895898757',
    'transferPhysicianUserId': null
  },
  {
    'dateTimeCreated': '2017-11-11T15:08:20.7175857',
    'arrivalDateTime': '2017-11-12T09:08:20.7175857',
    'receivingOrganizationId': 1,
    'relationship': 1,
    'originOrganizationId': 2,
    'nationalPatientId': 'RW4587898587',
    'transferPhysicianUserId': null
  },
  {
    'dateTimeCreated': '2017-11-11T15:08:20.7185868',
    'arrivalDateTime': '2017-11-12T09:08:20.7185868',
    'receivingOrganizationId': 2,
    'relationship': 1,
    'originOrganizationId': 3,
    'nationalPatientId': 'RW5686878958',
    'transferPhysicianUserId': null
  },
  {
    'dateTimeCreated': '2017-11-11T15:08:20.7175857',
    'arrivalDateTime': '2017-11-12T09:08:20.7175857',
    'receivingOrganizationId': 1,
    'relationship': 1,
    'originOrganizationId': 2,
    'nationalPatientId': 'RW5458987547',
    'transferPhysicianUserId': null
  },
  {
    'dateTimeCreated': '2017-11-11T15:08:20.7185868',
    'arrivalDateTime': '2017-11-12T09:08:20.7185868',
    'receivingOrganizationId': 1,
    'relationship': 1,
    'originOrganizationId': 1,
    'nationalPatientId': 'RW4587985789',
    'transferPhysicianUserId': null
  }
];