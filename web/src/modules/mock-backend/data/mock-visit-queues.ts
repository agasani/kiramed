export const VISIT_QUEUES: any[] = [
  {
    'queueOrderNumber': 1,
    'arrivalDateTime': '2017-11-11T15:08:20.5145831',
    'firstName': 'Matayo',
    'lastName': 'Kalisa',
    'patientId': null,
    'visitType': 0,
    'relationship': 1,
    'organizationId': 1, // For dummy
  },
  {
    'queueOrderNumber': 2,
    'arrivalDateTime': '2017-11-11T15:18:20.5145831',
    'firstName': 'Dancilla',
    'lastName': 'Nyandwi',
    'patientId': null,
    'visitType': 0,
    'relationship': 2,
    'organizationId': 2, // For dummy
  },
  {
    'queueOrderNumber': 3,
    'arrivalDateTime': '2017-11-11T16:08:20.5145831',
    'firstName': 'John',
    'lastName': 'Minani',
    'patientId': null,
    'visitType': 0,
    'relationship': 2,
    'organizationId': 1, // For dummy
  },

  {
    'queueOrderNumber': 4,
    'arrivalDateTime': '2017-11-11T15:08:20.5145831',
    'firstName': 'Matayo',
    'lastName': 'Kalisa',
    'patientId': null,
    'visitType': 0,
    'relationship': 1,
    'organizationId': 1, // For dummy
  },
  {
    'queueOrderNumber': 5,
    'arrivalDateTime': '2017-11-11T15:18:20.5145831',
    'firstName': 'Rangwida',
    'lastName': 'Hallo',
    'patientId': null,
    'visitType': 0,
    'relationship': 2,
    'organizationId': 2, // For dummy
  },
  {
    'queueOrderNumber': 6,
    'arrivalDateTime': '2017-11-11T16:08:20.5145831',
    'firstName': 'Byumvuhore',
    'lastName': 'Timothee',
    'patientId': null,
    'visitType': 0,
    'relationship': 2,
    'organizationId': 1, // For dummy
  },

  {
    'queueOrderNumber': 7,
    'arrivalDateTime': '2017-11-11T15:08:20.5145831',
    'firstName': 'Maniragaba',
    'lastName': 'Yonas',
    'patientId': null,
    'visitType': 0,
    'relationship': 1,
    'organizationId': 1, // For dummy
  },
  {
    'queueOrderNumber': 8,
    'arrivalDateTime': '2017-11-11T15:18:20.5145831',
    'firstName': 'Ukwishatse',
    'lastName': 'Claire',
    'patientId': null,
    'visitType': 0,
    'relationship': 2,
    'organizationId': 2, // For dummy
  },
  {
    'queueOrderNumber': 9,
    'arrivalDateTime': '2107-11-11T16:08:20.5145831',
    'firstName': 'Haleluya',
    'lastName': 'Amen',
    'patientId': null,
    'visitType': 0,
    'relationship': 2,
    'organizationId': 1, // For dummy
  }
];