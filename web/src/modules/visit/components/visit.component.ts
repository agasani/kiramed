import { AfterViewInit, Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'kira-visit',
  templateUrl: '../views/visit.component.html',
  styleUrls: ['../sass/visit.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class VisitComponent implements OnInit, AfterViewInit {

  constructor() {
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    $('.visits.menu .item').tab({ history: false });
    $('.transfers.menu .item').tab();
  }
}
