import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { MockVisit, MockVisitService } from '../services/mock/mock-visit.service';
import { MockBackendVisitsService } from '../../mock-backend/services/mock-backend-visits.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'kira-visit-list',
  templateUrl: './visit-list.component.html',
  styleUrls: ['./visit-list.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [MockVisitService]
})
export class VisitListComponent implements OnInit, OnDestroy {

  public visits: MockVisit[];
  private orgId: number;
  private mySubscription: Subscription;

  constructor(private mockBackendService: MockBackendVisitsService,
              private visitService: MockVisitService) {
    this.mockBackendService.start();
    this.orgId = 1; // Example OrgId
  }

  ngOnInit() {
    this.mySubscription = this.visitService.getVisits().subscribe(
        visits => this.visits = visits.filter(q => q.organizationId === this.orgId),
        error => console.error(error),
        () => console.log('Work in visits:', this.visits)
    );
  }

  ngOnDestroy() {
    this.mySubscription.unsubscribe();
  }
}
