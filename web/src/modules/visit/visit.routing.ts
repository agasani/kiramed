﻿import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VisitComponent } from './components/visit.component';
import { VisitListComponent } from './components/visit-list.component';

const ROUTES: Routes = [
  {
    path: '',
    component: VisitComponent,
    children: [
      { path: '', redirectTo: 'list', pathMatch: 'full' },
      { path: 'list', component: VisitListComponent },
      { path: 'queue', loadChildren: '../visit-queue/visit-queue.module#VisitQueueModule' },
      { path: 'appointment', loadChildren: '../appointment/appointment.module#AppointmentModule' },
      { path: 'transfer', loadChildren: '../transfer/transfer.module#TransferModule' },
    ]
  },
];

export const visitRouting: ModuleWithProviders = RouterModule.forRoot(ROUTES);
