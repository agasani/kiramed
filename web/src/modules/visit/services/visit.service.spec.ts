import { VisitClient } from './visit.service';

describe('VisitClient', () => {
  let service: VisitClient;

  beforeEach(() => {
    service = new VisitClient();
  });

  it('works', () => {
    expect(1).toEqual(2);
  });

});
