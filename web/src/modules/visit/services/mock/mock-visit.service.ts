import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';

export class MockVisit {
  'id': number;
  'organizationId': number;
  'patientId': number;
  'isReferral': boolean;
  'isFollowUp': boolean;
  'isPatientEnsured': boolean;
  'receptionistId'?: string;
  'nurserId'?: string;
  'doctorId'?: string;
  'encounterId'?: number;
  'visitDateTime': Date;
  'visitReason': string;
  'firstName': string;
  'lastName': string;
  'medicalRecordNumber': string;
  'trackNetNumber': string;
  'appointmentId'?: number;
  'status': number;
}

@Injectable()
export class MockVisitService {
  private visitsUrl = 'http://localhost:8080/visits';

  constructor(private http: Http) {
  }

  getVisits(): Observable<MockVisit[]> {
    return this.http.get(this.visitsUrl)
        .map(response => response.json() as MockVisit[])
        .catch(this.handleError);
  }

  private handleError(error: any): Observable<any> {
    console.error('An error occured', error); // for demo purposes only
    return Observable.throw(error.message || error);
  }
}
