﻿import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { visitRouting } from './visit.routing';
import { VisitComponent } from './components/visit.component';
import { VisitListComponent } from './components/visit-list.component';
import { ApComponent, TrComponent, VqComponent } from './sub-modules/components';
import { MockBackend } from '@angular/http/testing';
import { BaseRequestOptions, Http } from '@angular/http';
import {
  MockBackendAppointmentsService,
  MockBackendTransfersService,
  MockBackendVisitQueuesService,
  MockBackendVisitsService
} from '../mock-backend/services';
import { mockHTTPFactory } from '../mock-backend/mock-http.factory';

@NgModule({
  imports: [
    SharedModule,
    visitRouting,
  ],
  declarations: [
    VisitComponent,
    VisitListComponent,
    ApComponent,
    VqComponent,
    TrComponent
  ],
  providers: [
    // https://keyholesoftware.com/2017/01/09/setting-up-angular-2-mockbackend/
    MockBackend,
    BaseRequestOptions,
    MockBackendAppointmentsService,
    MockBackendTransfersService,
    MockBackendVisitQueuesService,
    MockBackendVisitsService,
    { provide: Http, useFactory: mockHTTPFactory, deps: [MockBackend, BaseRequestOptions] }
  ]
})

export class VisitModule {
}
