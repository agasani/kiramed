import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { MockBackendAppointmentsService } from '../../../mock-backend/services/mock-backend-appointments.service';
import { MockAppointmentService } from '../../../appointment/services/mock/mock-appointment.service';
import { AppointmentViewModel } from '../../../appointment/services/appointment.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'kira-ap',
  templateUrl: '../views/ap.html',
  styleUrls: ['../sass/ap.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [MockAppointmentService]
})

export class ApComponent implements OnInit, OnDestroy {

  public appointments: AppointmentViewModel[];
  private mySubscription: Subscription;

  constructor(private mockBackendService: MockBackendAppointmentsService,
              private appointmentService: MockAppointmentService) {
    this.mockBackendService.start();
  }

  ngOnInit() {
    this.mySubscription = this.appointmentService.getAppointments().subscribe(
        appoints => this.appointments = appoints,
        error => console.error(error),
        () => console.log('Appointments:', this.appointments)
    );
  }

  ngOnDestroy() {
    this.mySubscription.unsubscribe();
  }
}
