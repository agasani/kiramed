import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { MockVisitQueue, MockVisitQueueService } from '../../../visit-queue/services/mock/mock-visit-queue.service';
import { MockBackendVisitQueuesService } from '../../../mock-backend/services/mock-backend-visit-queues.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'kira-vq',
  templateUrl: '../views/vq.html',
  styleUrls: ['../sass/vq.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [MockVisitQueueService]
})

export class VqComponent implements OnInit, OnDestroy {

  public visitQueues: MockVisitQueue[];
  private orgId: number;
  private mySubscription: Subscription;

  constructor(private mockBackendService: MockBackendVisitQueuesService,
              private visitQueueService: MockVisitQueueService) {
    this.mockBackendService.start();
    this.orgId = 1; // Example OrgId
  }

  ngOnInit() {
    this.mySubscription = this.visitQueueService.getVisitQueues().subscribe(
        visits => {
          this.visitQueues = visits.filter(q => q.organizationId === this.orgId)
        },
        error => console.error(error),
        () => console.log('Our visit queues: ', this.visitQueues)
    )
  }

  ngOnDestroy() {
    this.mySubscription.unsubscribe();
  }

}
