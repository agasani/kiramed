﻿import { NgModule } from '@angular/core';
import { VisitComponent } from './components/visit.component';
import { SharedModule } from '../shared/shared.module';
import { visitRouting } from './visit.routing';
import { VisitQueueModule } from '../visit-queue/visit-queue.module';
import { TransferModule } from '../transfer/transfer.module';
import { AppointmentModule } from '../appointment/appointment.module';
import { VisitQueueComponent } from '../visit-queue/components/visit-queue.component';
import { TransferComponent } from '../transfer/components/transfer.component';
import { AppointmentComponent } from '../appointment/components/appointment.component';
import { VisitListComponent } from './components/visit-list.component';

@NgModule({
  imports: [
    SharedModule,
    visitRouting,
    VisitQueueModule,
    TransferModule,
    AppointmentModule
  ],
  declarations: [
    VisitComponent,
    VisitListComponent,
    VisitQueueComponent,
    TransferComponent,
    AppointmentComponent
  ],
})

export class VisitListModule {
}
