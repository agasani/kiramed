﻿import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {
  PatientAllergiesComponent,
  PatientComponent,
  PatientConditionsComponent,
  PatientDocsComponent,
  PatientFitnessComponent,
  PatientLabsComponent,
  PatientMedsComponent,
  PatientProfileComponent,
  PatientSurgeryComponent,
  PatientTransfersComponent,
  PatientVaxComponent,
  PatientVisitsComponent,
  PatientVitalsComponent,
  PatientWellnessComponent
} from './components';
import { AuthGuard } from '../shared/guards';

const routes: Routes = [
  {
    path: '',
    canActivate: [AuthGuard],
    component: PatientComponent,
    children: [
      { path: 'patient/:id', component: PatientProfileComponent },
      { path: 'patient-meds/:id', component: PatientMedsComponent },
      { path: 'patient-vitals/:id', component: PatientVitalsComponent },
      { path: 'patient-labs/:id', component: PatientLabsComponent },
      { path: 'patient-surgeries/:id', component: PatientSurgeryComponent },
      { path: 'patient-conditions/:id', component: PatientConditionsComponent },
      { path: 'patient-visits/:id', component: PatientVisitsComponent },
      { path: 'patient-docs/:id', component: PatientDocsComponent },
      { path: 'patient-vax/:id', component: PatientVaxComponent },
      { path: 'patient-transfers/:id', component: PatientTransfersComponent },
      { path: 'patient-allergies/:id', component: PatientAllergiesComponent },
      { path: 'patient-wellness/:id', component: PatientWellnessComponent },
      { path: 'patient-fitness/:id', component: PatientFitnessComponent }

    ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class PatientRoutingModule {
}
