﻿import { NgModule } from '@angular/core';

import { SharedModule } from '../shared/shared.module';
import { ListToStringPipe } from '../shared/pipes/list-to-string.pipe';

import {
  PatientAllergiesComponent,
  PatientComponent,
  PatientConditionsComponent,
  PatientDocsComponent,
  PatientFitnessComponent,
  PatientLabsComponent,
  PatientMedsComponent,
  PatientProfileComponent,
  PatientSurgeryComponent,
  PatientTransfersComponent,
  PatientVaxComponent,
  PatientVisitsComponent,
  PatientVitalsComponent,
  PatientWellnessComponent
} from './components';
import { PatientRoutingModule } from './patient-routing.module';


@NgModule({
  imports: [
    SharedModule,
    PatientRoutingModule
  ],
  declarations: [
    PatientAllergiesComponent,
    PatientComponent,
    PatientConditionsComponent,
    PatientDocsComponent,
    PatientFitnessComponent,
    PatientLabsComponent,
    PatientMedsComponent,
    PatientProfileComponent,
    PatientSurgeryComponent,
    PatientTransfersComponent,
    PatientVaxComponent,
    PatientVisitsComponent,
    PatientVitalsComponent,
    PatientWellnessComponent,
    ListToStringPipe
  ],
  exports: [
    PatientAllergiesComponent,
    // PatientComponent,
    PatientConditionsComponent,
    PatientDocsComponent,
    PatientFitnessComponent,
    PatientLabsComponent,
    PatientMedsComponent,
    // PatientProfileComponent,
    PatientSurgeryComponent,
    PatientTransfersComponent,
    PatientVaxComponent,
    PatientVisitsComponent,
    PatientVitalsComponent,
    PatientWellnessComponent
    // ListToStringPipe
  ]
})

export class PatientModule {
}
