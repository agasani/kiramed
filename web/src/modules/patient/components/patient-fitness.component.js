"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var workout_service_1 = require("../services/workout.service");
var PatientFitnessComponent = /** @class */ (function () {
    function PatientFitnessComponent(activatedRoute, workoutService) {
        this.activatedRoute = activatedRoute;
        this.workoutService = workoutService;
    }
    PatientFitnessComponent.prototype.ngOnInit = function () {
        var _this = this;
        console.log('in fitness oninit..');
        this.subscription = this.activatedRoute.params.subscribe(function (param) {
            var id = param['id'];
            if (!id)
                id = localStorage.getItem('patientId');
            console.log('fitness param in oninit: ', param);
            // this.patientService.getPatient(id).then(pat => this.patient = pat);
            _this.workouts = _this.workoutService.searchWorkouts('');
        });
    };
    PatientFitnessComponent.prototype.ngAfterViewInit = function () {
        $('.ui.modal').modal();
        $('.ui.dropdown').dropdown();
        $('#add-fitness').hide();
        var userSettings = localStorage.getItem('userSettings');
        console.log('settings: ', userSettings);
        if (userSettings) {
            var settings = JSON.parse(userSettings);
            if (settings) {
                if (settings['userType'] === 'patient') {
                    $('#add-fitness').show();
                    $('#add-fitness').on('click', function () {
                        $('.ui.modal.fitness').modal('show');
                        $('.modals').css('z-index', 10001);
                    });
                }
            }
        }
    };
    PatientFitnessComponent.prototype.ngOnDestroy = function () {
        this.subscription.unsubscribe();
    };
    PatientFitnessComponent.prototype.add = function (f) {
        try {
            console.log('Form: ', f.value);
            $('#save').addClass('loading');
            var meal = this.workoutService.addWorkout(f.value.type, f.value.activity, f.value.date, f.value.duration, f.value.durationUnit, f.value.calories, f.value.feeling, f.value.note);
            $('#save').removeClass('loading');
            $('.ui.modal').modal('hide');
            $('.success.nag').nag('show');
        }
        catch (e) {
            console.error('ERROR: ', e);
            $('#save').removeClass('loading');
            $('.error.nag').nag('show');
        }
    };
    PatientFitnessComponent = __decorate([
        core_1.Component({
            selector: 'patient',
            templateUrl: '../views/fitness.html',
            styleUrls: ['../sass/patient.scss'],
            providers: [workout_service_1.WorkoutService]
        })
    ], PatientFitnessComponent);
    return PatientFitnessComponent;
}());
exports.PatientFitnessComponent = PatientFitnessComponent;
//# sourceMappingURL=patient-fitness.component.js.map