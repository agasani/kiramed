﻿import { Component, Input } from '@angular/core';
import { NgForm } from '@angular/forms';
import { PatientService } from '../services/patient.service';
import { Vital } from '../models/vital';
import { VitalService } from '../services/vital.service';

import { ActivatedRoute } from '@angular/router';

import { Subscription } from 'rxjs/Subscription';
import { LocalStorageService } from 'ngx-webstorage';
import { BaseComponent } from '../../shared/components';

@Component({
  selector: 'clinical-vitals',
  templateUrl: '../views/vitals.html',
  styleUrls: ['../sass/patient.scss'],
  providers: [PatientService, VitalService]
})

export class PatientVitalsComponent extends BaseComponent {

  @Input() textDisplay: string = 'Vital Signs';
  vitals: Vital[];
  newVital: Vital;
  // showForm: boolean;
  private subscription: Subscription;

  constructor(private activatedRoute: ActivatedRoute,
              private patientService: PatientService,
              private vitalService: VitalService,
              private storage: LocalStorageService) {
    super();
    this.itemService = this.vitalService;
    // this.showForm = false;
  }

  ngOnInit() {
    console.log('yo oninit...');
    this.subscription = this.activatedRoute.params.subscribe((param: any) => {
      let id = param['id'];
      if (!id) id = this.storage.retrieve('patientId');
      console.log('id in oninit: ', id);
      this.vitals = this.vitalService.searchVitals('');

      // this.itemService.getItems(id).then(items => this.vitals = items);
    });
  }

  ngAfterViewInit(): void {

    $('.ui.modal').modal();
    $('.ui.dropdown').dropdown();
    $('#add-vital').hide();

    if (this.settings)
      if (this.settings['claims'].includes('is.clinical') || this.settings['claims'].includes('is.doctor')) {
        $('#add-vital').show();
        $('#add-vital').on('click', function () {
          $('.ui.modal.vitals').modal('show');
          $('.modals').css('z-index', 10001);
        });
      }
  }

  add(f: NgForm): void {

    try {
      console.log('Form: ', f.value);
      $('#save').addClass('loading');
      const v = f.value;
      const meal = this.vitalService.addVital(v.temp, v.resp, v.oral, v.pulse, v.pain, v.dia, v.sys, v.height, v.weight);
      $('#save').removeClass('loading');
      $('.ui.modal').modal('hide');
      $('.success.nag').nag('show');
    } catch (e) {
      console.error('ERROR: ', e);
      $('#save').removeClass('loading');
      $('.error.nag').nag('show');
    }
  }

}
