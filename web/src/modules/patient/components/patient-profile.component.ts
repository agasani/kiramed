﻿import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { Patient } from '../models/patient';
import { PatientService } from '../services/patient.service';
import { ActivatedRoute } from '@angular/router';

import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'patient',
  templateUrl: '../views/profile.html',
  styleUrls: ['../sass/patient.scss'],
  providers: [PatientService]
})

export class PatientProfileComponent implements OnInit, AfterViewInit, OnDestroy {

  patient: Patient;
  private subscription: Subscription;
  private age: number;

  constructor(private activatedRoute: ActivatedRoute, private patientService: PatientService) {
  }

  ngOnInit() {
    console.log('in profile oninit...');
    this.subscription = this.activatedRoute.params.subscribe(
        (param: any) => {
          let id: string = param['id'];

          if (!id) id = localStorage.getItem('patientId');
          else localStorage.setItem('patientId', '' + id);

          console.log('profile param in oninit: ', param);
          // this.patientService.getPat(+id).subscribe(
          //     pat => {
          //       this.patient = pat;
          //       this.age = new Date().getFullYear() - new Date(this.patient.dob).getFullYear();
          //       console.log('The patient obj: ', this.patient);
          //     }
          // );
        });
  }

  ngAfterViewInit() {

  }

  ngOnDestroy(): void {

    this.subscription.unsubscribe();

  }
}
