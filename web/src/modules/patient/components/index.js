"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}

Object.defineProperty(exports, "__esModule", {value: true});
__export(require("./patient.component"));
__export(require("./patient-allergies.component"));
__export(require("./patient-conditions.component"));
__export(require("./patient-docs.component"));
__export(require("./patient-fitness.component"));
__export(require("./patient-labs.component"));
__export(require("./patient-meds.component"));
__export(require("./patient-profile.component"));
__export(require("./patient-surgery.component"));
__export(require("./patient-transfers.component"));
__export(require("./patient-vax.component"));
__export(require("./patient-visits.component"));
__export(require("./patient-vitals.component"));
__export(require("./patient-wellness.component"));
//# sourceMappingURL=index.js.map