"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var patient_service_1 = require("../services/patient.service");
require("rxjs/add/operator/switchMap");
var components_1 = require("../../shared/components");
var PatientComponent = /** @class */ (function (_super) {
    __extends(PatientComponent, _super);
    function PatientComponent(activatedRoute, patientService) {
        var _this = _super.call(this) || this;
        _this.activatedRoute = activatedRoute;
        _this.patientService = patientService;
        return _this;
    }
    PatientComponent.prototype.ngOnInit = function () {
        console.log('yo oninit in patient, params: ', this.activatedRoute.params);
        this.subscription = this.activatedRoute.firstChild.paramMap.subscribe(function (params) {
            var id = params.get('id');
            console.log('param in oninit patient: ', params);
            // this.patientService.getPat(+id).subscribe(pat => this.patient = pat);
        });
    };
    PatientComponent.prototype.ngAfterViewInit = function () {
        this.sideBarMenuTrigger();
    };
    PatientComponent = __decorate([
        core_1.Component({
            selector: 'patient',
            templateUrl: '../views/patient.html',
            styleUrls: ['../sass/patient.scss'],
            providers: [patient_service_1.PatientService]
        })
    ], PatientComponent);
    return PatientComponent;
}(components_1.BaseComponent));
exports.PatientComponent = PatientComponent;
//# sourceMappingURL=patient.component.js.map