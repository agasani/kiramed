"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length,
        r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", {value: true});
var core_1 = require("@angular/core");
var conditions_service_1 = require("../services/conditions.service");
var PatientConditionsComponent = /** @class */ (function () {
    function PatientConditionsComponent(activatedRoute, conditionService) {
        this.activatedRoute = activatedRoute;
        this.conditionService = conditionService;
    }

    PatientConditionsComponent.prototype.ngOnInit = function () {
        var _this = this;
        console.log('yo oninit..');
        this.subscription = this.activatedRoute.params.subscribe(function (param) {
            var id = param['id'];
            if (!id)
                id = localStorage.getItem('patientId');
            console.log('id in oninit: ', id);
            // this.patientService.getcondition(id).then(pat => this.condition = pat);
            _this.conditions = _this.conditionService.searchConditions('');
        });
    };
    PatientConditionsComponent.prototype.ngAfterViewInit = function () {
        $('.ui.modal').modal();
        $('.ui.dropdown').dropdown();
        $('#add-cond').hide();
        var userSettings = localStorage.getItem('userSettings');
        if (userSettings) {
            var settings = JSON.parse(userSettings);
            if (settings) {
                if (settings['userType'] === 'clinic') {
                    $('#add-cond').show();
                    $('#add-cond').on('click', function () {
                        $('.ui.modal.condition').modal('show');
                        $('.modals').css('z-index', 10001);
                    });
                }
            }
        }
    };
    PatientConditionsComponent.prototype.addCondition = function (f) {
        $('#add-condition').addClass('loading');
        try {
            console.log('adding ne cond. form is: ', f.value);
            var id = parseInt(f.value.condition, 10) - 1;
            var cond = this.conditionService.getCond(id);
            cond.meds = [f.value.meds];
            cond.sinceDate = f.value.date;
            cond.status = 'Acrive';
            console.log('The condition is: ', cond);
            this.conditions.push(cond);
            $('.ui.modal').modal('hide');
            $('.success.nag').nag('show');
        }
        catch (e) {
            console.error('ERROR: ', e);
            $('#add-condition').removeClass('loading');
            $('.error.nag').nag('show');
        }
    };
    PatientConditionsComponent = __decorate([
        core_1.Component({
            selector: 'patient-conditions',
            templateUrl: '../views/conditions.html',
            styleUrls: ['../sass/patient.scss'],
            providers: [conditions_service_1.ConditionsService]
        })
    ], PatientConditionsComponent);
    return PatientConditionsComponent;
}());
exports.PatientConditionsComponent = PatientConditionsComponent;
//# sourceMappingURL=patient-conditions.component.js.map