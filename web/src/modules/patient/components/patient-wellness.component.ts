﻿import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

import { Meal } from '../models/meal';
import { MealsService } from '../services/meal.service';
import { ActivatedRoute } from '@angular/router';

import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'patient',
  templateUrl: '../views/wellness.html',
  styleUrls: ['../sass/patient.scss'],
  providers: [MealsService]
})

export class PatientWellnessComponent implements OnInit, AfterViewInit, OnDestroy {

  meals: Meal[];
  private subscription: Subscription;

  constructor(private activatedRoute: ActivatedRoute, private mealsService: MealsService) {
  }

  ngOnInit() {
    console.log('in wellness oninit..');
    this.subscription = this.activatedRoute.params.subscribe((param: any) => {
      let id = param['id'];
      if (!id)
        id = localStorage.getItem('patientId');
      console.log('wellness param in oninit: ', param);
      // this.patientService.getPatient(id).then(pat => this.patient = pat);
      this.meals = this.mealsService.searchMeals('');
    });
  }

  ngAfterViewInit(): void {

    $('.ui.modal').modal();
    $('.ui.dropdown').dropdown();

    $('#add-well').hide();
    const userSettings = localStorage.getItem('userSettings');
    console.log('settings: ', userSettings);

    if (userSettings) {
      const settings = JSON.parse(userSettings);
      if (settings) {
        if (settings['userType'] === 'patient') {
          $('#add-well').show();
          $('#add-well').on('click', function () {
            $('.ui.modal.wellness').modal('show');
            $('.modals').css('z-index', 10001);
          });
        }
      }
    }
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  addMeal(f: NgForm): void {

    $('#save').addClass('loading');
    try {
      const meal = this.mealsService.addMeal(f.value.description, f.value.foodType, f.value.date, f.value.calories);
      $('#save').removeClass('loading');
      $('.ui.modal').modal('hide');
      $('.success.nag').nag('show');
    } catch (e) {
      console.error('ERROR: ', e);
      $('#save').removeClass('loading');
      $('.error.nag').nag('show');
    }
  }
}
