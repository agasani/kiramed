"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var patient_service_1 = require("../services/patient.service");
var PatientVaxComponent = /** @class */ (function () {
    function PatientVaxComponent(activatedRoute, patientService) {
        this.activatedRoute = activatedRoute;
        this.patientService = patientService;
    }
    PatientVaxComponent.prototype.ngOnInit = function () {
        console.log('yo oninit..');
        this.subscription = this.activatedRoute.params.subscribe(function (param) {
            var id = param['id'];
            if (!id)
                id = localStorage.getItem('patientId');
            console.log('id in oninit: ', id);
            // this.patientService.getPat(id).subscribe(pat => this.patient = pat);
            // this.patient = this.patientService.getPat(id);
        });
    };
    PatientVaxComponent = __decorate([
        core_1.Component({
            selector: 'patient-vax',
            templateUrl: '../views/vax.html',
            styleUrls: ['../sass/patient.scss'],
            providers: [patient_service_1.PatientService]
        })
    ], PatientVaxComponent);
    return PatientVaxComponent;
}());
exports.PatientVaxComponent = PatientVaxComponent;
//# sourceMappingURL=patient-vax.component.js.map