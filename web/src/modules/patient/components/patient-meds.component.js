"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var meds_service_1 = require("../services/meds.service");
// import { Patient } from '../models/patient';
var patient_service_1 = require("../services/patient.service");
var PatientMedsComponent = /** @class */ (function () {
    function PatientMedsComponent(activatedRoute, patientService, medsService) {
        this.activatedRoute = activatedRoute;
        this.patientService = patientService;
        this.medsService = medsService;
    }
    PatientMedsComponent.prototype.ngOnInit = function () {
        var _this = this;
        console.log('yo oninit..');
        this.subscription = this.activatedRoute.params.subscribe(function (param) {
            var id = param['id'];
            if (!id)
                id = localStorage.getItem('patientId');
            console.log('id in oninit: ', id);
            // this.patientService.getPatient(id).then(pat => this.patient = pat);
            // this.patient = this.patientService.getPat(id);
            _this.meds = _this.medsService.searchMeds('');
        });
    };
    PatientMedsComponent.prototype.ngAfterViewInit = function () {
        $('.ui.modal').modal();
        $('.ui.dropdown').dropdown();
        $('#add-med').hide();
        var userSettings = localStorage.getItem('userSettings');
        console.log('settings: ', userSettings);
        if (userSettings) {
            var settings = JSON.parse(userSettings);
            if (settings) {
                if (settings['userType'] === 'clinic') {
                    $('#add-med').show();
                    $('#add-med').on('click', function () {
                        $('.ui.modal.meds').modal('show');
                        $('.modals').css('z-index', 10001);
                    });
                }
            }
        }
    };
    PatientMedsComponent.prototype.add = function (f) {
        try {
            console.log('Form: ', f.value);
            $('#save').addClass('loading');
            var meal = this.medsService.addMed(f.value.name, f.value.strength, f.value.unit, f.value.form, f.value.diagnosis);
            $('#save').removeClass('loading');
            $('.ui.modal').modal('hide');
            $('.success.nag').nag('show');
        }
        catch (e) {
            console.error('ERROR: ', e);
            $('#save').removeClass('loading');
            $('.error.nag').nag('show');
        }
    };
    PatientMedsComponent = __decorate([
        core_1.Component({
            selector: 'patient-meds',
            templateUrl: '../views/meds.html',
            styleUrls: ['../sass/patient.scss'],
            providers: [patient_service_1.PatientService, meds_service_1.MedsService]
        })
    ], PatientMedsComponent);
    return PatientMedsComponent;
}());
exports.PatientMedsComponent = PatientMedsComponent;
//# sourceMappingURL=patient-meds.component.js.map