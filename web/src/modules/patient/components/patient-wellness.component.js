"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var meal_service_1 = require("../services/meal.service");
var PatientWellnessComponent = /** @class */ (function () {
    function PatientWellnessComponent(activatedRoute, mealsService) {
        this.activatedRoute = activatedRoute;
        this.mealsService = mealsService;
    }
    PatientWellnessComponent.prototype.ngOnInit = function () {
        var _this = this;
        console.log('in wellness oninit..');
        this.subscription = this.activatedRoute.params.subscribe(function (param) {
            var id = param['id'];
            if (!id)
                id = localStorage.getItem('patientId');
            console.log('wellness param in oninit: ', param);
            // this.patientService.getPatient(id).then(pat => this.patient = pat);
            _this.meals = _this.mealsService.searchMeals('');
        });
    };
    PatientWellnessComponent.prototype.ngAfterViewInit = function () {
        $('.ui.modal').modal();
        $('.ui.dropdown').dropdown();
        $('#add-well').hide();
        var userSettings = localStorage.getItem('userSettings');
        console.log('settings: ', userSettings);
        if (userSettings) {
            var settings = JSON.parse(userSettings);
            if (settings) {
                if (settings['userType'] === 'patient') {
                    $('#add-well').show();
                    $('#add-well').on('click', function () {
                        $('.ui.modal.wellness').modal('show');
                        $('.modals').css('z-index', 10001);
                    });
                }
            }
        }
    };
    PatientWellnessComponent.prototype.ngOnDestroy = function () {
        this.subscription.unsubscribe();
    };
    PatientWellnessComponent.prototype.addMeal = function (f) {
        $('#save').addClass('loading');
        try {
            var meal = this.mealsService.addMeal(f.value.description, f.value.foodType, f.value.date, f.value.calories);
            $('#save').removeClass('loading');
            $('.ui.modal').modal('hide');
            $('.success.nag').nag('show');
        }
        catch (e) {
            console.error('ERROR: ', e);
            $('#save').removeClass('loading');
            $('.error.nag').nag('show');
        }
    };
    PatientWellnessComponent = __decorate([
        core_1.Component({
            selector: 'patient',
            templateUrl: '../views/wellness.html',
            styleUrls: ['../sass/patient.scss'],
            providers: [meal_service_1.MealsService]
        })
    ], PatientWellnessComponent);
    return PatientWellnessComponent;
}());
exports.PatientWellnessComponent = PatientWellnessComponent;
//# sourceMappingURL=patient-wellness.component.js.map