﻿import { Component } from '@angular/core';
import { Patient } from '../models/patient';
import { PatientService } from '../services/patient.service';
import { ActivatedRoute } from '@angular/router';

import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/switchMap';
import { BaseComponent } from '../../shared/components';

@Component({
  selector: 'patient',
  templateUrl: '../views/patient.html',
  styleUrls: ['../sass/patient.scss'],
  providers: [PatientService]
})

export class PatientComponent extends BaseComponent {

  patient: Patient;
  private subscription: Subscription;

  constructor(private activatedRoute: ActivatedRoute, private patientService: PatientService) {
    super()
  }

  ngOnInit() {
    console.log('yo oninit in patient, params: ', this.activatedRoute.params);

    this.subscription = this.activatedRoute.firstChild.paramMap.subscribe((params: any) => {

      const id: number = params.get('id');
      console.log('param in oninit patient: ', params);

      // this.patientService.getPat(+id).subscribe(pat => this.patient = pat);
    });

  }

  ngAfterViewInit() {
    this.sideBarMenuTrigger();
  }
}
