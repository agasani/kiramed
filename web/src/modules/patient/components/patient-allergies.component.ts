﻿import { AfterViewInit, Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
// import { Patient } from '../models/patient';
import { Allergy } from '../models/allergy';

import { AllergyService } from '../services/allergy.service';
import { ActivatedRoute } from '@angular/router';

import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'patient-allergies',
  templateUrl: '../views/allergies.html',
  styleUrls: ['../sass/patient.scss'],
  providers: [AllergyService]
})

export class PatientAllergiesComponent implements OnInit, AfterViewInit {

  // patient: Patient;
  allergies: Allergy[];
  private subscription: Subscription;

  constructor(private activatedRoute: ActivatedRoute, private allergyService: AllergyService) {
  }

  ngOnInit() {
    console.log('yo oninit..');
    this.subscription = this.activatedRoute.params.subscribe((param: any) => {
      let id = param['id'];
      if (!id)
        id = localStorage.getItem('patientId');
      console.log('id in oninit: ', id);
      // this.patientService.getPatient(id).then(pat => this.patient = pat);
      // this.patient = this.allergyService.getPat(id);
      this.allergies = this.allergyService.searchAllergies('');
    });
  }

  ngAfterViewInit(): void {

    $('.ui.modal').modal();
    $('.ui.dropdown').dropdown();

    $('#add-allergy').hide();
    const userSettings = localStorage.getItem('userSettings');
    console.log('settings: ', userSettings);

    if (userSettings) {
      const settings = JSON.parse(userSettings);
      if (settings) {
        if (settings['userType'] === 'patient' || settings['userType'] === 'clinic') {
          $('#add-allergy').show();
          $('#add-allergy').on('click', function () {
            $('.ui.modal.allergy').modal('show');
            $('.modals').css('z-index', 10001);
          });
        }
      }
    }
  }

  add(f: NgForm): void {

    try {
      console.log('Form: ', f.value);
      $('#save').addClass('loading');
      const meal = this.allergyService.addAllergy(f.value.name, f.value.startDate, f.value.endDate, f.value.meds.split(','));
      $('#save').removeClass('loading');
      $('.ui.modal').modal('hide');
      $('.success.nag').nag('show');
    } catch (e) {
      console.error('ERROR: ', e);
      $('#save').removeClass('loading');
      $('.error.nag').nag('show');
    }
  }

}
