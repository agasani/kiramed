﻿import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

import { Workout } from '../models/workout';
import { WorkoutService } from '../services/workout.service';
import { ActivatedRoute } from '@angular/router';

import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'patient',
  templateUrl: '../views/fitness.html',
  styleUrls: ['../sass/patient.scss'],
  providers: [WorkoutService]
})

export class PatientFitnessComponent implements OnInit, AfterViewInit, OnDestroy {

  workouts: Workout[];

  private subscription: Subscription;

  constructor(private activatedRoute: ActivatedRoute, private workoutService: WorkoutService) {
  }

  ngOnInit() {
    console.log('in fitness oninit..');
    this.subscription = this.activatedRoute.params.subscribe((param: any) => {
      let id = param['id'];
      if (!id)
        id = localStorage.getItem('patientId');
      console.log('fitness param in oninit: ', param);
      // this.patientService.getPatient(id).then(pat => this.patient = pat);
      this.workouts = this.workoutService.searchWorkouts('');
    });
  }

  ngAfterViewInit(): void {

    $('.ui.modal').modal();
    $('.ui.dropdown').dropdown();

    $('#add-fitness').hide();
    const userSettings = localStorage.getItem('userSettings');
    console.log('settings: ', userSettings);

    if (userSettings) {
      const settings = JSON.parse(userSettings);
      if (settings) {
        if (settings['userType'] === 'patient') {
          $('#add-fitness').show();
          $('#add-fitness').on('click', function () {
            $('.ui.modal.fitness').modal('show');
            $('.modals').css('z-index', 10001);
          });
        }
      }
    }
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  add(f: NgForm): void {

    try {
      console.log('Form: ', f.value);
      $('#save').addClass('loading');
      const meal = this.workoutService.addWorkout(f.value.type, f.value.activity, f.value.date, f.value.duration, f.value.durationUnit
          , f.value.calories, f.value.feeling, f.value.note);
      $('#save').removeClass('loading');
      $('.ui.modal').modal('hide');
      $('.success.nag').nag('show');
    } catch (e) {
      console.error('ERROR: ', e);
      $('#save').removeClass('loading');
      $('.error.nag').nag('show');
    }
  }
}
