"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var patient_service_1 = require("../services/patient.service");
var vital_service_1 = require("../services/vital.service");
var components_1 = require("../../shared/components");
var PatientVitalsComponent = /** @class */ (function (_super) {
    __extends(PatientVitalsComponent, _super);
    function PatientVitalsComponent(activatedRoute, patientService, vitalService, storage) {
        var _this = _super.call(this) || this;
        _this.activatedRoute = activatedRoute;
        _this.patientService = patientService;
        _this.vitalService = vitalService;
        _this.storage = storage;
        _this.textDisplay = 'Vital Signs';
        _this.itemService = _this.vitalService;
        return _this;
        // this.showForm = false;
    }
    PatientVitalsComponent.prototype.ngOnInit = function () {
        var _this = this;
        console.log('yo oninit...');
        this.subscription = this.activatedRoute.params.subscribe(function (param) {
            var id = param['id'];
            if (!id)
                id = _this.storage.retrieve('patientId');
            console.log('id in oninit: ', id);
            _this.vitals = _this.vitalService.searchVitals('');
            // this.itemService.getItems(id).then(items => this.vitals = items);
        });
    };
    PatientVitalsComponent.prototype.ngAfterViewInit = function () {
        $('.ui.modal').modal();
        $('.ui.dropdown').dropdown();
        $('#add-vital').hide();
        if (this.settings)
            if (this.settings['claims'].includes('is.clinical') || this.settings['claims'].includes('is.doctor')) {
                $('#add-vital').show();
                $('#add-vital').on('click', function () {
                    $('.ui.modal.vitals').modal('show');
                    $('.modals').css('z-index', 10001);
                });
            }
    };
    PatientVitalsComponent.prototype.add = function (f) {
        try {
            console.log('Form: ', f.value);
            $('#save').addClass('loading');
            var v = f.value;
            var meal = this.vitalService.addVital(v.temp, v.resp, v.oral, v.pulse, v.pain, v.dia, v.sys, v.height, v.weight);
            $('#save').removeClass('loading');
            $('.ui.modal').modal('hide');
            $('.success.nag').nag('show');
        }
        catch (e) {
            console.error('ERROR: ', e);
            $('#save').removeClass('loading');
            $('.error.nag').nag('show');
        }
    };
    __decorate([
        core_1.Input()
    ], PatientVitalsComponent.prototype, "textDisplay", void 0);
    PatientVitalsComponent = __decorate([
        core_1.Component({
            selector: 'clinical-vitals',
            templateUrl: '../views/vitals.html',
            styleUrls: ['../sass/patient.scss'],
            providers: [patient_service_1.PatientService, vital_service_1.VitalService]
        })
    ], PatientVitalsComponent);
    return PatientVitalsComponent;
}(components_1.BaseComponent));
exports.PatientVitalsComponent = PatientVitalsComponent;
//# sourceMappingURL=patient-vitals.component.js.map