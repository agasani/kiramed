﻿import { Component, OnInit } from '@angular/core';
import { Patient } from '../models/patient';
import { PatientService } from '../services/patient.service';
import { ActivatedRoute } from '@angular/router';

import { Subscription } from 'rxjs/Subscription';


@Component({
  selector: 'patient-labs',
  templateUrl: '../views/labs.html',
  styleUrls: ['../sass/patient.scss'],
  providers: [PatientService]
})

export class PatientLabsComponent implements OnInit {

  patient: Patient;
  private subscription: Subscription;

  constructor(private activatedRoute: ActivatedRoute, private patientService: PatientService) {
  }

  ngOnInit() {
    console.log('yo oninit..');
    this.subscription = this.activatedRoute.params.subscribe((param: any) => {
      let id = param['id'];
      if (!id)
        id = localStorage.getItem('patientId');
      console.log('id in oninit: ', id);
      // this.patientService.getPat(id).subscribe(pat => this.patient = pat);
      // this.patient = this.patientService.getPat(id);
    });
  }
}
