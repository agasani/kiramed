﻿import { AfterViewInit, Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

import { TransferService } from '../services/transfer.service';
import { Transfer } from '../models/transfer';
import { Patient } from '../models/patient';
import { PatientService } from '../services/patient.service';
import { ActivatedRoute } from '@angular/router';

import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'patient-transfers',
  templateUrl: '../views/transfers.html',
  styleUrls: ['../sass/patient.scss'],
  providers: [PatientService, TransferService]
})

export class PatientTransfersComponent implements OnInit, AfterViewInit {

  transfers: Transfer[];
  patient: Patient;
  private subscription: Subscription;

  constructor(private activatedRoute: ActivatedRoute, private patientService: PatientService, private transferService: TransferService) {
  }

  ngOnInit() {
    console.log('yo oninit..');
    this.subscription = this.activatedRoute.params.subscribe((param: any) => {
      let id = param['id'];
      if (!id)
        id = localStorage.getItem('patientId');
      console.log('id in oninit: ', id);
      // this.patientService.getPatient(id).then(pat => this.patient = pat);
      // this.patient = this.patientService.getPat(id);
      this.transfers = this.transferService.searchTransfers('');
    });
  }

  ngAfterViewInit(): void {

    $('.ui.modal').modal();
    $('.ui.dropdown').dropdown();

    $('#add-transfer').hide();
    const userSettings = localStorage.getItem('userSettings');
    console.log('settings: ', userSettings);

    if (userSettings) {
      const settings = JSON.parse(userSettings);
      if (settings) {
        if (settings['userType'] === 'clinic') {
          $('#add-transfer').show();
          $('#add-transfer').on('click', function () {
            $('.ui.modal.transfer').modal('show');
            $('.modals').css('z-index', 10001);
          });
        }
      }
    }
  }

  add(f: NgForm): void {

    try {
      console.log('Form: ', f.value);
      $('#save').addClass('loading');
      const transfer = this.transferService.addTransfer('Byo Centre de Sante', f.value.to, f.value.note);
      $('#save').removeClass('loading');
      $('.ui.modal').modal('hide');
      $('.success.nag').nag('show');
    } catch (e) {
      console.error('ERROR: ', e);
      $('#save').removeClass('loading');
      $('.error.nag').nag('show');
    }
  }
}
