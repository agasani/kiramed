"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var transfer_service_1 = require("../services/transfer.service");
var patient_service_1 = require("../services/patient.service");
var PatientTransfersComponent = /** @class */ (function () {
    function PatientTransfersComponent(activatedRoute, patientService, transferService) {
        this.activatedRoute = activatedRoute;
        this.patientService = patientService;
        this.transferService = transferService;
    }
    PatientTransfersComponent.prototype.ngOnInit = function () {
        var _this = this;
        console.log('yo oninit..');
        this.subscription = this.activatedRoute.params.subscribe(function (param) {
            var id = param['id'];
            if (!id)
                id = localStorage.getItem('patientId');
            console.log('id in oninit: ', id);
            // this.patientService.getPatient(id).then(pat => this.patient = pat);
            // this.patient = this.patientService.getPat(id);
            _this.transfers = _this.transferService.searchTransfers('');
        });
    };
    PatientTransfersComponent.prototype.ngAfterViewInit = function () {
        $('.ui.modal').modal();
        $('.ui.dropdown').dropdown();
        $('#add-transfer').hide();
        var userSettings = localStorage.getItem('userSettings');
        console.log('settings: ', userSettings);
        if (userSettings) {
            var settings = JSON.parse(userSettings);
            if (settings) {
                if (settings['userType'] === 'clinic') {
                    $('#add-transfer').show();
                    $('#add-transfer').on('click', function () {
                        $('.ui.modal.transfer').modal('show');
                        $('.modals').css('z-index', 10001);
                    });
                }
            }
        }
    };
    PatientTransfersComponent.prototype.add = function (f) {
        try {
            console.log('Form: ', f.value);
            $('#save').addClass('loading');
            var transfer = this.transferService.addTransfer('Byo Centre de Sante', f.value.to, f.value.note);
            $('#save').removeClass('loading');
            $('.ui.modal').modal('hide');
            $('.success.nag').nag('show');
        }
        catch (e) {
            console.error('ERROR: ', e);
            $('#save').removeClass('loading');
            $('.error.nag').nag('show');
        }
    };
    PatientTransfersComponent = __decorate([
        core_1.Component({
            selector: 'patient-transfers',
            templateUrl: '../views/transfers.html',
            styleUrls: ['../sass/patient.scss'],
            providers: [patient_service_1.PatientService, transfer_service_1.TransferService]
        })
    ], PatientTransfersComponent);
    return PatientTransfersComponent;
}());
exports.PatientTransfersComponent = PatientTransfersComponent;
//# sourceMappingURL=patient-transfers.component.js.map