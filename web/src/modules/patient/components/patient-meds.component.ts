﻿import { AfterViewInit, Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

import { Med } from '../models/med';
import { MedsService } from '../services/meds.service';
// import { Patient } from '../models/patient';
import { PatientService } from '../services/patient.service';
import { ActivatedRoute } from '@angular/router';

import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'patient-meds',
  templateUrl: '../views/meds.html',
  styleUrls: ['../sass/patient.scss'],
  providers: [PatientService, MedsService]
})

export class PatientMedsComponent implements OnInit, AfterViewInit {

  meds: Med[];
  // patient: Patient;
  private subscription: Subscription;

  constructor(private activatedRoute: ActivatedRoute, private patientService: PatientService, private medsService: MedsService) {
  }

  ngOnInit() {
    console.log('yo oninit..');
    this.subscription = this.activatedRoute.params.subscribe((param: any) => {
      let id = param['id'];
      if (!id)
        id = localStorage.getItem('patientId');
      console.log('id in oninit: ', id);
      // this.patientService.getPatient(id).then(pat => this.patient = pat);
      // this.patient = this.patientService.getPat(id);
      this.meds = this.medsService.searchMeds('');
    });
  }


  ngAfterViewInit(): void {

    $('.ui.modal').modal();
    $('.ui.dropdown').dropdown();

    $('#add-med').hide();
    const userSettings = localStorage.getItem('userSettings');
    console.log('settings: ', userSettings);

    if (userSettings) {
      const settings = JSON.parse(userSettings);
      if (settings) {
        if (settings['userType'] === 'clinic') {
          $('#add-med').show();
          $('#add-med').on('click', function () {
            $('.ui.modal.meds').modal('show');
            $('.modals').css('z-index', 10001);
          });
        }
      }
    }
  }

  add(f: NgForm): void {

    try {
      console.log('Form: ', f.value);
      $('#save').addClass('loading');
      const meal = this.medsService.addMed(f.value.name, f.value.strength, f.value.unit, f.value.form, f.value.diagnosis);
      $('#save').removeClass('loading');
      $('.ui.modal').modal('hide');
      $('.success.nag').nag('show');
    } catch (e) {
      console.error('ERROR: ', e);
      $('#save').removeClass('loading');
      $('.error.nag').nag('show');
    }
  }
}
