﻿import { AfterViewInit, Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

import { Condition } from '../models/condition';
import { ConditionsService } from '../services/conditions.service';
import { ActivatedRoute } from '@angular/router';

import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'patient-conditions',
  templateUrl: '../views/conditions.html',
  styleUrls: ['../sass/patient.scss'],
  providers: [ConditionsService]
})

export class PatientConditionsComponent implements OnInit, AfterViewInit {

  condition: Condition;
  conditions: Condition[];
  private subscription: Subscription;

  constructor(private activatedRoute: ActivatedRoute, private conditionService: ConditionsService) {
  }

  ngOnInit(): void {
    console.log('yo oninit..');
    this.subscription = this.activatedRoute.params.subscribe((param: any) => {
      let id = param['id'];
      if (!id)
        id = localStorage.getItem('patientId');

      console.log('id in oninit: ', id);
      // this.patientService.getcondition(id).then(pat => this.condition = pat);
      this.conditions = this.conditionService.searchConditions('');
    });
  }

  ngAfterViewInit(): void {

    $('.ui.modal').modal();
    $('.ui.dropdown').dropdown();

    $('#add-cond').hide();
    const userSettings = localStorage.getItem('userSettings');
    if (userSettings) {
      const settings = JSON.parse(userSettings);
      if (settings) {
        if (settings['userType'] === 'clinic') {
          $('#add-cond').show();
          $('#add-cond').on('click', function () {
            $('.ui.modal.condition').modal('show');
            $('.modals').css('z-index', 10001);
          });
        }
      }
    }
  }

  addCondition(f: NgForm): void {
    $('#add-condition').addClass('loading');
    try {
      console.log('adding ne cond. form is: ', f.value);
      const id = parseInt(f.value.condition, 10) - 1;
      const cond = this.conditionService.getCond(id);
      cond.meds = [f.value.meds];
      cond.sinceDate = f.value.date;
      cond.status = 'Acrive';

      console.log('The condition is: ', cond);

      this.conditions.push(cond);

      $('.ui.modal').modal('hide');
      $('.success.nag').nag('show');

    } catch (e) {
      console.error('ERROR: ', e);
      $('#add-condition').removeClass('loading');
      $('.error.nag').nag('show');
    }

  }
}
