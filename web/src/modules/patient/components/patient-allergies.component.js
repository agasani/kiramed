"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var allergy_service_1 = require("../services/allergy.service");
var PatientAllergiesComponent = /** @class */ (function () {
    function PatientAllergiesComponent(activatedRoute, allergyService) {
        this.activatedRoute = activatedRoute;
        this.allergyService = allergyService;
    }
    PatientAllergiesComponent.prototype.ngOnInit = function () {
        var _this = this;
        console.log('yo oninit..');
        this.subscription = this.activatedRoute.params.subscribe(function (param) {
            var id = param['id'];
            if (!id)
                id = localStorage.getItem('patientId');
            console.log('id in oninit: ', id);
            // this.patientService.getPatient(id).then(pat => this.patient = pat);
            // this.patient = this.allergyService.getPat(id);
            _this.allergies = _this.allergyService.searchAllergies('');
        });
    };
    PatientAllergiesComponent.prototype.ngAfterViewInit = function () {
        $('.ui.modal').modal();
        $('.ui.dropdown').dropdown();
        $('#add-allergy').hide();
        var userSettings = localStorage.getItem('userSettings');
        console.log('settings: ', userSettings);
        if (userSettings) {
            var settings = JSON.parse(userSettings);
            if (settings) {
                if (settings['userType'] === 'patient' || settings['userType'] === 'clinic') {
                    $('#add-allergy').show();
                    $('#add-allergy').on('click', function () {
                        $('.ui.modal.allergy').modal('show');
                        $('.modals').css('z-index', 10001);
                    });
                }
            }
        }
    };
    PatientAllergiesComponent.prototype.add = function (f) {
        try {
            console.log('Form: ', f.value);
            $('#save').addClass('loading');
            var meal = this.allergyService.addAllergy(f.value.name, f.value.startDate, f.value.endDate, f.value.meds.split(','));
            $('#save').removeClass('loading');
            $('.ui.modal').modal('hide');
            $('.success.nag').nag('show');
        }
        catch (e) {
            console.error('ERROR: ', e);
            $('#save').removeClass('loading');
            $('.error.nag').nag('show');
        }
    };
    PatientAllergiesComponent = __decorate([
        core_1.Component({
            selector: 'patient-allergies',
            templateUrl: '../views/allergies.html',
            styleUrls: ['../sass/patient.scss'],
            providers: [allergy_service_1.AllergyService]
        })
    ], PatientAllergiesComponent);
    return PatientAllergiesComponent;
}());
exports.PatientAllergiesComponent = PatientAllergiesComponent;
//# sourceMappingURL=patient-allergies.component.js.map