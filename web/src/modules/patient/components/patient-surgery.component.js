"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var patient_service_1 = require("../services/patient.service");
var PatientSurgeryComponent = /** @class */ (function () {
    function PatientSurgeryComponent(activatedRoute, patientService) {
        this.activatedRoute = activatedRoute;
        this.patientService = patientService;
    }
    PatientSurgeryComponent.prototype.ngOnInit = function () {
        console.log('yo oninit..');
        this.subscription = this.activatedRoute.params.subscribe(function (param) {
            var id = param['id'];
            console.log('id in oninit: ', id);
            if (!id)
                id = localStorage.getItem('patientId');
            // this.patientService.getPat(id).subscribe(pat => this.patient = pat);
            // this.patient = this.patientService.getPat(id);
        });
    };
    PatientSurgeryComponent = __decorate([
        core_1.Component({
            selector: 'patient-surgeries',
            templateUrl: '../views/surgeries.html',
            styleUrls: ['../sass/patient.scss'],
            providers: [patient_service_1.PatientService]
        })
    ], PatientSurgeryComponent);
    return PatientSurgeryComponent;
}());
exports.PatientSurgeryComponent = PatientSurgeryComponent;
//# sourceMappingURL=patient-surgery.component.js.map