﻿export class Workout {

  id: number;
  type: string;
  activity: string;
  date: string;
  duration: number;
  durationUnit: string;
  durationDisplay: string;
  calories: number;
  feeling: string;
  note: string;

  constructor(id: number, type: string, activity: string, date: string, duration: number, durationUnit: string, calories: number, feeling: string, note: string) {

    this.id = id;
    this.type = type;
    this.activity = activity;
    this.date = date;
    this.duration = duration;
    this.durationUnit = durationUnit;
    this.durationDisplay = duration.toString() + ' ' + (duration > 1 ? durationUnit + 's' : durationUnit);
    this.calories = calories;
    this.feeling = feeling;
    this.note = note;
  }
}
