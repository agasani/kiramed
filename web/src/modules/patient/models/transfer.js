"use strict";
Object.defineProperty(exports, "__esModule", {value: true});
var Transfer = /** @class */ (function () {
    function Transfer(id, from, to, note) {
        this.id = id;
        this.from = from;
        this.to = to;
        this.note = note;
        this.date = new Date().toDateString();
    }

    return Transfer;
}());
exports.Transfer = Transfer;
//# sourceMappingURL=transfer.js.map