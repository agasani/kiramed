"use strict";
Object.defineProperty(exports, "__esModule", {value: true});
var Condition = /** @class */ (function () {
    function Condition(id, name, code, since, meds, status) {
        this.id = id;
        this.name = name;
        this.code = code;
        this.sinceDate = since;
        this.meds = meds;
        this.status = status;
    }

    return Condition;
}());
exports.Condition = Condition;
//# sourceMappingURL=condition.js.map