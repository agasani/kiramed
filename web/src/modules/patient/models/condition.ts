﻿export class Condition {

  id: number;
  name: string;
  code: string;
  sinceDate: string;
  meds: Array<string>;
  status: string;

  constructor(id: number, name: string, code: string, since: string, meds: Array<string>, status: string) {

    this.id = id;
    this.name = name;
    this.code = code;
    this.sinceDate = since;
    this.meds = meds;
    this.status = status;
  }
}
