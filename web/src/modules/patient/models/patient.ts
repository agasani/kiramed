﻿export interface Patient {
  id: number;
  entityId: number;
  deathDate?: Date | null;
  medicalRecordNumber: string;
  tracknetNumber?: string | null;
  allergyNote?: string | null;
  nonClinicalNote?: string | null;
  acceptedHIE: boolean;
  noKnownAllergy: boolean;
  noKnownMed: boolean;
  isDeleted: Date;
  nationalPatientId: string;
  dob: Date;
  sex: null;
  firstName: string;
  lastName: string;
  middleName?: string | null;
  email: string;
  phoneNumber: string;
  nationalId: string;
}

// export class PatientSearch {
//   id: number;
//   firstName: string;
//   lastName: string;
//   entityId: number;
//   demographicId: number;
//   nationalID?: string | null;
//   dob: Date;
// }

export interface PatientRegistrationViewModel extends EntityRegistrationViewModel {
  deathDate?: Date | null;
  medicalRecordNumber: string;
  tracknetNumber?: string | null;
  allergyNote?: string | null;
  nonClinicalNote?: string | null;
  acceptedHIE: boolean;
  noKnownAllergy: boolean;
  noKnownMed: boolean;
  nationalPatientId: string;
  forceCreate: boolean;
}

export interface EntityRegistrationViewModel {
  countryId: number;
  firstName: string;
  secondName: string;
  nationalId: string;
  parentId: number;
  email: string;
  phoneNumber: string;
  middleName?: string | null;
  demographic?: EntityDemographicViewModel | null;
  contacts?: EntityContactViewModel[] | null;
  addresses?: EntityAddressViewModel[] | null;
}

export interface EntityDemographicViewModel {
  // id: number;
  secondaryLanguage?: string | null;
  primaryLanguage?: string | null;
  nationalID?: string | null;
  dOB: Date;
  sex?: string | null;
  martalStatus?: string | null;
  religion?: string | null;
  education?: string | null;
  income: number;
}

export interface EntityContactViewModel {
  // id: number;
  contactTypeId: number;
  value?: string | null;
  label?: string | null;
  beginDate: Date;
  endDate?: Date | null;
  deactivate: boolean;
}

export interface EntityAddressViewModel {
  // id: number;
  label?: string | null;
  directions?: string | null;
  address1?: string | null;
  address2?: string | null;
  city?: string | null;
  state?: string | null;
  location?: string | null;
  timeZone: number;
  timeZoneId: number;
  hasDaylightSaving: boolean;
  zip?: string | null;
  district?: string | null;
  isMailingAddress: boolean;
  country: string;
  beginDate: Date;
  endDate?: Date | null;
  deactivate: boolean;
}

export class PatientDetailViewModel {
  entityId: number;
  countryId: number;
  entity: string | null;
  deathDate: Date | null;
  medicalRecordNumber: string;
  tracknetNumber: string;
  allergyNote: string;
  nonClinicalNote: string | null;
  acceptedHIE: boolean;
  noKnownAllergy: boolean;
  noKnownMed: boolean;
  meds: string | null;
  nationalPatientId: string;
  id: number;
  dateTimeCreated: Date;
  isDeleted: Date;
}