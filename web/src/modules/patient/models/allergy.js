"use strict";
Object.defineProperty(exports, "__esModule", {value: true});
var Allergy = /** @class */ (function () {
    function Allergy(id, name, startDate, endDate, meds) {
        this.id = id;
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.meds = meds;
    }

    return Allergy;
}());
exports.Allergy = Allergy;
//# sourceMappingURL=allergy.js.map