﻿export class Meal {

  id: number;
  description: string;
  foodType: string;
  date: string;
  calories: number;

  constructor(id: number, desc: string, type: string, date: string, calories: number) {

    this.id = id;
    this.description = desc;
    this.foodType = type;
    this.date = date;
    this.calories = calories;
  }
}
