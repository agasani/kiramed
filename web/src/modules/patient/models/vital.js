"use strict";
Object.defineProperty(exports, "__esModule", {value: true});
var Vital = /** @class */ (function () {
    function Vital(id, temp, oral, resp, pulse, pain, dia, sys, height, weight) {
        this.id = id;
        this.temp = temp;
        this.oralTemp = oral;
        this.respRate = resp;
        this.pulse = pulse;
        this.pain = pain;
        this.diastolic = dia;
        this.systolic = sys;
        this.height = height;
        this.weight = weight;
        this.date = new Date().toDateString();
    }

    return Vital;
}());
exports.Vital = Vital;
//# sourceMappingURL=vital.js.map