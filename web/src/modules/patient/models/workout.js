"use strict";
Object.defineProperty(exports, "__esModule", {value: true});
var Workout = /** @class */ (function () {
    function Workout(id, type, activity, date, duration, durationUnit, calories, feeling, note) {
        this.id = id;
        this.type = type;
        this.activity = activity;
        this.date = date;
        this.duration = duration;
        this.durationUnit = durationUnit;
        this.durationDisplay = duration.toString() + ' ' + (duration > 1 ? durationUnit + 's' : durationUnit);
        this.calories = calories;
        this.feeling = feeling;
        this.note = note;
    }

    return Workout;
}());
exports.Workout = Workout;
//# sourceMappingURL=workout.js.map