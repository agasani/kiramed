﻿export class Transfer {

  id: number;
  from: string;
  to: string;
  date: string;
  note: string;

  constructor(id: number, from: string, to: string, note: string) {

    this.id = id;
    this.from = from;
    this.to = to;
    this.note = note;
    this.date = new Date().toDateString();
  }
}
