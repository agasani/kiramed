﻿export class Med {

  id: number;
  name: string;
  diagnosis: string;
  prescriptionDate: string;
  strength: number;
  unit: string;
  form: string;
  ivAdditive: string;
  sig: Sig;
  dispense: Dispense;
}

export class Sig {

  id: number;
  quantity: string;
  unit: string;
  route: string;
  days: number;
  frequency: string;
  perDay: number;
  when: string;
  site: string;
}

export class Dispense {

  id: number;
  amount: string;
  form: string;
  genericAllowed: boolean;
  numRefills: number;
  brand: string;
  manufacturer: number;
  lot: string;
  note: string;
}
