﻿export class Allergy {

  id: number;
  name: string;
  startDate: string;
  endDate: string;
  meds: Array<string>;

  constructor(id: number, name: string, startDate: string, endDate: string, meds: Array<string>) {

    this.id = id;
    this.name = name;
    this.startDate = startDate;
    this.endDate = endDate;
    this.meds = meds;
  }
}
