﻿export class Vital {

  id: number;
  temp: number;
  oralTemp: number;
  respRate: number;
  date: string;
  pulse: number;
  diastolic: number;
  systolic: number;
  pain: number;
  height: number;
  weight: number;

  constructor(id: number, temp: number, oral: number, resp: number, pulse: number, pain: number, dia: number, sys: number, height: number, weight: number) {

    this.id = id;
    this.temp = temp;
    this.oralTemp = oral;
    this.respRate = resp;
    this.pulse = pulse;
    this.pain = pain;
    this.diastolic = dia;
    this.systolic = sys;
    this.height = height;
    this.weight = weight;
    this.date = new Date().toDateString();

  }
}
