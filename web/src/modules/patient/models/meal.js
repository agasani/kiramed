"use strict";
Object.defineProperty(exports, "__esModule", {value: true});
var Meal = /** @class */ (function () {
    function Meal(id, desc, type, date, calories) {
        this.id = id;
        this.description = desc;
        this.foodType = type;
        this.date = date;
        this.calories = calories;
    }

    return Meal;
}());
exports.Meal = Meal;
//# sourceMappingURL=meal.js.map