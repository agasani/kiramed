"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length,
        r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", {value: true});
var core_1 = require("@angular/core");
var shared_module_1 = require("../shared/shared.module");
var list_to_string_pipe_1 = require("../shared/pipes/list-to-string.pipe");
var components_1 = require("./components");
var patient_routing_module_1 = require("./patient-routing.module");
var PatientModule = /** @class */ (function () {
    function PatientModule() {
    }

    PatientModule = __decorate([
        core_1.NgModule({
            imports: [
                shared_module_1.SharedModule,
                patient_routing_module_1.PatientRoutingModule
            ],
            declarations: [
                components_1.PatientAllergiesComponent,
                components_1.PatientComponent,
                components_1.PatientConditionsComponent,
                components_1.PatientDocsComponent,
                components_1.PatientFitnessComponent,
                components_1.PatientLabsComponent,
                components_1.PatientMedsComponent,
                components_1.PatientProfileComponent,
                components_1.PatientSurgeryComponent,
                components_1.PatientTransfersComponent,
                components_1.PatientVaxComponent,
                components_1.PatientVisitsComponent,
                components_1.PatientVitalsComponent,
                components_1.PatientWellnessComponent,
                list_to_string_pipe_1.ListToStringPipe
            ],
            exports: [
                components_1.PatientAllergiesComponent,
                // PatientComponent,
                components_1.PatientConditionsComponent,
                components_1.PatientDocsComponent,
                components_1.PatientFitnessComponent,
                components_1.PatientLabsComponent,
                components_1.PatientMedsComponent,
                // PatientProfileComponent,
                components_1.PatientSurgeryComponent,
                components_1.PatientTransfersComponent,
                components_1.PatientVaxComponent,
                components_1.PatientVisitsComponent,
                components_1.PatientVitalsComponent,
                components_1.PatientWellnessComponent
                // ListToStringPipe
            ]
        })
    ], PatientModule);
    return PatientModule;
}());
exports.PatientModule = PatientModule;
//# sourceMappingURL=patient.module.js.map