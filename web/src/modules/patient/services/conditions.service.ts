﻿import { Injectable } from '@angular/core';

import { Condition } from '../models/condition';
import { HttpClient } from '@angular/common/http';

const conditions: Condition[] = [

  { id: 3, name: 'Type II Diabetes', code: 'PN07-43', sinceDate: '5 years ago', meds: ['Metformin'], status: 'Active' },
  { id: 1, name: 'Hypertension', code: 'FT99-51', sinceDate: '7 years ago', meds: ['Condesartan'], status: 'Active' }

];

const allConditions: Condition[] = [

  { id: 1, name: 'Hypertension', code: 'CD1', sinceDate: '', meds: [], status: '' },
  { id: 2, name: 'Type I Diabetes', code: 'CD2', sinceDate: '', meds: [], status: '' },
  { id: 3, name: 'Type II Diabetes', code: 'CD3', sinceDate: '', meds: [], status: '' },
  { id: 4, name: 'Asthma', code: 'CD4', sinceDate: '', meds: [], status: '' },
  { id: 5, name: 'Alzheimer\'s', code: 'CD5', sinceDate: '', meds: [], status: '' },
  { id: 6, name: 'Cancer', code: 'CD6', sinceDate: '', meds: [], status: '' },


];


@Injectable()
export class ConditionsService {

  constructor(private http: HttpClient) {
  }

  getConditions(): Promise<Condition[]> {

    return Promise.resolve(conditions);
  }

  getConditionsCount(): number {

    return conditions.length;
  }

  addCondition(Condition: Condition): void {

    conditions.push(Condition);
  }

  getCondition(id: number): Promise<Condition> {

    return Promise.resolve(conditions[id]);
    //if (id)
    //    return Promise.resolve(conditions[id]);
    //else
    //    return {};
  }

  getCond(id: number): Condition {

    return allConditions[id];
  }

  searchConditions(term: string): Condition[] {

    return conditions;
  }
}
