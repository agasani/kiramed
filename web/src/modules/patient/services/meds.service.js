"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var med_1 = require("../models/med");
var meds = [];
var med1 = new med_1.Med();
med1.name = 'Quinine';
med1.strength = 250;
med1.unit = 'mg';
med1.diagnosis = 'Malaria';
med1.prescriptionDate = 'Jan 8th, 2017';
meds.push(med1);
var med2 = new med_1.Med();
med2.name = 'Pyrazinamide';
med2.strength = 500;
med2.unit = 'mg';
med2.diagnosis = 'Tuberculosis';
med2.prescriptionDate = 'Jan 21st, 2017';
meds.push(med2);
var MedsService = /** @class */ (function () {
    function MedsService(http) {
        this.http = http;
    }
    MedsService.prototype.getMeds = function () {
        return Promise.resolve(meds);
    };
    MedsService.prototype.getMedsCount = function () {
        return meds.length;
    };
    MedsService.prototype.addMed = function (name, stregth, unit, form, diagnosis) {
        var med = new med_1.Med();
        med.name = name;
        med.strength = stregth;
        med.unit = unit;
        med.form = form;
        med.diagnosis = diagnosis;
        med.prescriptionDate = new Date().toDateString();
        meds.push(med);
        return med;
    };
    MedsService.prototype.getMed = function (id) {
        return Promise.resolve(meds[id]);
        //if (id)
        //    return Promise.resolve(workouts[id]);
        //else
        //    return {};
    };
    MedsService.prototype.getMedi = function (id) {
        return meds[id];
    };
    MedsService.prototype.searchMeds = function (term) {
        return meds;
    };
    MedsService = __decorate([
        core_1.Injectable()
    ], MedsService);
    return MedsService;
}());
exports.MedsService = MedsService;
//# sourceMappingURL=meds.service.js.map