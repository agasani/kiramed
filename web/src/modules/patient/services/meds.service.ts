﻿import { Injectable } from '@angular/core';

import { Med } from '../models/med';
import { HttpClient } from '@angular/common/http';

const meds: Med[] = [];
const med1 = new Med();
med1.name = 'Quinine';
med1.strength = 250;
med1.unit = 'mg';
med1.diagnosis = 'Malaria';
med1.prescriptionDate = 'Jan 8th, 2017';
meds.push(med1);

const med2 = new Med();
med2.name = 'Pyrazinamide';
med2.strength = 500;
med2.unit = 'mg';
med2.diagnosis = 'Tuberculosis';
med2.prescriptionDate = 'Jan 21st, 2017';
meds.push(med2);

@Injectable()
export class MedsService {

  constructor(private http: HttpClient) {
  }

  getMeds(): Promise<Med[]> {

    return Promise.resolve(meds);
  }

  getMedsCount(): number {

    return meds.length;
  }

  addMed(name: string, stregth: number, unit: string, form: string, diagnosis: string): Med {

    const med = new Med();
    med.name = name;
    med.strength = stregth;
    med.unit = unit;
    med.form = form;
    med.diagnosis = diagnosis;
    med.prescriptionDate = new Date().toDateString();
    meds.push(med);

    return med;
  }

  getMed(id: number): Promise<Med> {

    return Promise.resolve(meds[id]);
    //if (id)
    //    return Promise.resolve(workouts[id]);
    //else
    //    return {};
  }

  getMedi(id: number): Med {

    return meds[id];
  }

  searchMeds(term: string): Med[] {

    return meds;
  }
}
