"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length,
        r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", {value: true});
var core_1 = require("@angular/core");
var transfer_1 = require("../models/transfer");
var transfers = [
    {
        id: 1,
        from: 'Byo Centre de Sante',
        to: 'CHUK Hospital',
        date: 'Jan 4th, 2017',
        note: 'The patient has needs an endoscropy test. She has severe pain in her abdomen.'
    },
];
var TransferService = /** @class */ (function () {
    function TransferService(http) {
        this.http = http;
    }

    TransferService.prototype.getTransfers = function () {
        return Promise.resolve(transfers);
    };
    TransferService.prototype.getTransfersCount = function () {
        return transfers.length;
    };
    TransferService.prototype.addTransfer = function (from, to, note) {
        var transfer = new transfer_1.Transfer(transfers.length, from, to, note);
        transfers.push(transfer);
        return transfer;
    };
    TransferService.prototype.getTransfer = function (id) {
        return Promise.resolve(transfers[id]);
        // if (id)
        //    return Promise.resolve(transfers[id]);
        // else
        //    return {};
    };
    TransferService.prototype.getTrans = function (id) {
        return transfers[id];
    };
    TransferService.prototype.searchTransfers = function (term) {
        return transfers;
    };
    TransferService = __decorate([
        core_1.Injectable()
    ], TransferService);
    return TransferService;
}());
exports.TransferService = TransferService;
//# sourceMappingURL=transfer.service.js.map