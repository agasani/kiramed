﻿import { Injectable } from '@angular/core';

import { Vital } from '../models/vital';
import { HttpClient } from '@angular/common/http';

const vitals: Vital[] = [

  {
    id: 1,
    temp: 27,
    respRate: 18,
    oralTemp: 26,
    pulse: 72,
    pain: 0,
    diastolic: 121,
    systolic: 88,
    height: 172,
    weight: 68,
    date: new Date().toDateString()
  },
  {
    id: 2,
    temp: 25,
    respRate: 22,
    oralTemp: 29,
    pulse: 83,
    pain: 4,
    diastolic: 132,
    systolic: 90,
    height: 172,
    weight: 68,
    date: new Date().toDateString()
  },

];

@Injectable()
export class VitalService {

  constructor(private http: HttpClient) {
  }

  getVitals(): Promise<Vital[]> {

    return Promise.resolve(vitals);
  }

  getVitalsCount(): number {

    return vitals.length;
  }

  addVital(temp: number, resp: number, oral: number, pulse: number, pain: number, dia: number, sys: number, height: number, weight: number): Vital {

    const vital = new Vital(vitals.length, temp, oral, resp, pulse, pain, dia, sys, height, weight);
    vitals.push(vital);

    return vital;
  }

  getVital(id: number): Promise<Vital> {

    return Promise.resolve(vitals[id]);
    //if (id)
    //    return Promise.resolve(vitals[id]);
    //else
    //    return {};
  }

  getVita(id: number): Vital {

    return vitals[id];
  }

  searchVitals(term: string): Vital[] {

    return vitals;
  }

}
