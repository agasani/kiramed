﻿import { Injectable } from '@angular/core';

import { Allergy } from '../models/allergy';
import { HttpClient } from '@angular/common/http';

const allergies: Allergy[] = [

  { id: 1, name: 'Lactose-intolerant', startDate: 'Jan 4th, 2002', endDate: '', meds: ['-'] },
  { id: 1, name: 'Aspirin-allergic', startDate: 'Mar 4th, 2004', endDate: '', meds: ['-'] }

];

@Injectable()
export class AllergyService {

  constructor(private http: HttpClient) {
  }

  getAllergies(): Promise<Allergy[]> {

    return Promise.resolve(allergies);
  }

  getAllergiesCount(): number {

    return allergies.length;
  }

  addAllergy(name: string, startDate: string, endDate: string, meds: Array<string>): Allergy {

    var allerga = new Allergy(allergies.length, name, startDate, endDate, meds);
    allergies.push(allerga);

    return allerga;
  }

  getAllergy(id: number): Promise<Allergy> {

    return Promise.resolve(allergies[id]);
    //if (id)
    //    return Promise.resolve(allergies[id]);
    //else
    //    return {};
  }

  getAllerg(id: number): Allergy {

    return allergies[id];
  }

  searchAllergies(term: string): Allergy[] {

    return allergies;
  }
}
