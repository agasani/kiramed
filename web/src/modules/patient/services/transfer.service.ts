﻿import { Injectable } from '@angular/core';

import { Transfer } from '../models/transfer';
import { HttpClient } from '@angular/common/http';

const transfers: Transfer[] = [

  {
    id: 1,
    from: 'Byo Centre de Sante',
    to: 'CHUK Hospital',
    date: 'Jan 4th, 2017',
    note: 'The patient has needs an endoscropy test. She has severe pain in her abdomen.'
  },

];

@Injectable()
export class TransferService {

  constructor(private http: HttpClient) {
  }

  getTransfers(): Promise<Transfer[]> {

    return Promise.resolve(transfers);
  }

  getTransfersCount(): number {

    return transfers.length;
  }

  addTransfer(from: string, to: string, note: string): Transfer {

    const transfer = new Transfer(transfers.length, from, to, note);
    transfers.push(transfer);

    return transfer;
  }

  getTransfer(id: number): Promise<Transfer> {

    return Promise.resolve(transfers[id]);
    // if (id)
    //    return Promise.resolve(transfers[id]);
    // else
    //    return {};
  }

  getTrans(id: number): Transfer {

    return transfers[id];
  }

  searchTransfers(term: string): Transfer[] {

    return transfers;
  }
}
