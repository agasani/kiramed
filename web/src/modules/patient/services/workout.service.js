"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var workout_1 = require("../models/workout");
var workouts = [
    {
        id: 1,
        type: 'Endurance',
        activity: 'Jogging',
        date: 'Jan 4th, 2017',
        duration: 30,
        durationUnit: 'Minute',
        durationDisplay: '30 Minutes',
        calories: 190,
        feeling: 'Energized',
        note: 'This is was a great workout'
    },
    {
        id: 2,
        type: 'Endurance',
        activity: 'Bicycling',
        date: 'Jan 3rd, 2017',
        duration: 1,
        durationUnit: 'Hour',
        durationDisplay: '1 Hour',
        calories: 260,
        feeling: 'Painful',
        note: 'My left knee was painful'
    },
];
var WorkoutService = /** @class */ (function () {
    function WorkoutService(http) {
        this.http = http;
    }
    WorkoutService.prototype.getWorkouts = function () {
        return Promise.resolve(workouts);
    };
    WorkoutService.prototype.getWorkoutsCount = function () {
        return workouts.length;
    };
    WorkoutService.prototype.addWorkout = function (type, activity, date, duration, durationUnit, calories, feeling, note) {
        var workout = new workout_1.Workout(workouts.length, type, activity, date, duration, durationUnit, calories, feeling, note);
        workouts.push(workout);
        return workout;
    };
    WorkoutService.prototype.getWorkout = function (id) {
        return Promise.resolve(workouts[id]);
        //if (id)
        //    return Promise.resolve(workouts[id]);
        //else
        //    return {};
    };
    WorkoutService.prototype.getWork = function (id) {
        return workouts[id];
    };
    WorkoutService.prototype.searchWorkouts = function (term) {
        return workouts;
    };
    WorkoutService = __decorate([
        core_1.Injectable()
    ], WorkoutService);
    return WorkoutService;
}());
exports.WorkoutService = WorkoutService;
//# sourceMappingURL=workout.service.js.map