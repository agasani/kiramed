﻿import { Injectable } from '@angular/core';

import { Meal } from '../models/meal';
import { HttpClient } from '@angular/common/http';

const meals: Meal[] = [

  { id: 1, description: 'Home Cooked Diner', foodType: 'Veggies, White Meal', date: 'Jan 3rd, 2017', calories: 800 },
  { id: 1, description: 'Heavy Breakfast', foodType: 'Protein, Whole grain', date: 'Jan 16th, 2017', calories: 460 }

];

@Injectable()
export class MealsService {

  constructor(private http: HttpClient) {
  }

  getMeals(): Promise<Meal[]> {

    return Promise.resolve(meals);
  }

  getMealsCount(): number {

    return meals.length;
  }

  addMeal(desc: string, type: string, date: string, calories: number): Meal {

    const meal = new Meal(meals.length, desc, type, date, calories);
    meals.push(meal);

    return meal;
  }

  getMeal(id: number): Promise<Meal> {

    return Promise.resolve(meals[id]);
    //if (id)
    //    return Promise.resolve(meals[id]);
    //else
    //    return {};
  }

  getMea(id: number): Meal {

    return meals[id];
  }

  searchMeals(term: string): Meal[] {

    return meals;
  }
}
