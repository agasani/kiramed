﻿import { Injectable } from '@angular/core';

import { Workout } from '../models/workout';
import { HttpClient } from '@angular/common/http';

const workouts: Workout[] = [

  {
    id: 1,
    type: 'Endurance',
    activity: 'Jogging',
    date: 'Jan 4th, 2017',
    duration: 30,
    durationUnit: 'Minute',
    durationDisplay: '30 Minutes',
    calories: 190,
    feeling: 'Energized',
    note: 'This is was a great workout'
  },
  {
    id: 2,
    type: 'Endurance',
    activity: 'Bicycling',
    date: 'Jan 3rd, 2017',
    duration: 1,
    durationUnit: 'Hour',
    durationDisplay: '1 Hour',
    calories: 260,
    feeling: 'Painful',
    note: 'My left knee was painful'
  },

];

@Injectable()
export class WorkoutService {

  constructor(private http: HttpClient) {
  }

  getWorkouts(): Promise<Workout[]> {

    return Promise.resolve(workouts);
  }

  getWorkoutsCount(): number {

    return workouts.length;
  }

  addWorkout(type: string, activity: string, date: string, duration: number, durationUnit: string, calories: number, feeling: string, note: string): Workout {

    const workout = new Workout(workouts.length, type, activity, date, duration, durationUnit, calories, feeling, note);
    workouts.push(workout);

    return workout;
  }

  getWorkout(id: number): Promise<Workout> {

    return Promise.resolve(workouts[id]);
    //if (id)
    //    return Promise.resolve(workouts[id]);
    //else
    //    return {};
  }

  getWork(id: number): Workout {

    return workouts[id];
  }

  searchWorkouts(term: string): Workout[] {

    return workouts;
  }
}
