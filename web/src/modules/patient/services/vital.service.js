"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var vital_1 = require("../models/vital");
var vitals = [
    {
        id: 1,
        temp: 27,
        respRate: 18,
        oralTemp: 26,
        pulse: 72,
        pain: 0,
        diastolic: 121,
        systolic: 88,
        height: 172,
        weight: 68,
        date: new Date().toDateString()
    },
    {
        id: 2,
        temp: 25,
        respRate: 22,
        oralTemp: 29,
        pulse: 83,
        pain: 4,
        diastolic: 132,
        systolic: 90,
        height: 172,
        weight: 68,
        date: new Date().toDateString()
    },
];
var VitalService = /** @class */ (function () {
    function VitalService(http) {
        this.http = http;
    }
    VitalService.prototype.getVitals = function () {
        return Promise.resolve(vitals);
    };
    VitalService.prototype.getVitalsCount = function () {
        return vitals.length;
    };
    VitalService.prototype.addVital = function (temp, resp, oral, pulse, pain, dia, sys, height, weight) {
        var vital = new vital_1.Vital(vitals.length, temp, oral, resp, pulse, pain, dia, sys, height, weight);
        vitals.push(vital);
        return vital;
    };
    VitalService.prototype.getVital = function (id) {
        return Promise.resolve(vitals[id]);
        //if (id)
        //    return Promise.resolve(vitals[id]);
        //else
        //    return {};
    };
    VitalService.prototype.getVita = function (id) {
        return vitals[id];
    };
    VitalService.prototype.searchVitals = function (term) {
        return vitals;
    };
    VitalService = __decorate([
        core_1.Injectable()
    ], VitalService);
    return VitalService;
}());
exports.VitalService = VitalService;
//# sourceMappingURL=vital.service.js.map