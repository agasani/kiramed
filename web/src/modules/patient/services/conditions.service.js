"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var conditions = [
    { id: 3, name: 'Type II Diabetes', code: 'PN07-43', sinceDate: '5 years ago', meds: ['Metformin'], status: 'Active' },
    { id: 1, name: 'Hypertension', code: 'FT99-51', sinceDate: '7 years ago', meds: ['Condesartan'], status: 'Active' }
];
var allConditions = [
    { id: 1, name: 'Hypertension', code: 'CD1', sinceDate: '', meds: [], status: '' },
    { id: 2, name: 'Type I Diabetes', code: 'CD2', sinceDate: '', meds: [], status: '' },
    { id: 3, name: 'Type II Diabetes', code: 'CD3', sinceDate: '', meds: [], status: '' },
    { id: 4, name: 'Asthma', code: 'CD4', sinceDate: '', meds: [], status: '' },
    { id: 5, name: 'Alzheimer\'s', code: 'CD5', sinceDate: '', meds: [], status: '' },
    { id: 6, name: 'Cancer', code: 'CD6', sinceDate: '', meds: [], status: '' },
];
var ConditionsService = /** @class */ (function () {
    function ConditionsService(http) {
        this.http = http;
    }
    ConditionsService.prototype.getConditions = function () {
        return Promise.resolve(conditions);
    };
    ConditionsService.prototype.getConditionsCount = function () {
        return conditions.length;
    };
    ConditionsService.prototype.addCondition = function (Condition) {
        conditions.push(Condition);
    };
    ConditionsService.prototype.getCondition = function (id) {
        return Promise.resolve(conditions[id]);
        //if (id)
        //    return Promise.resolve(conditions[id]);
        //else
        //    return {};
    };
    ConditionsService.prototype.getCond = function (id) {
        return allConditions[id];
    };
    ConditionsService.prototype.searchConditions = function (term) {
        return conditions;
    };
    ConditionsService = __decorate([
        core_1.Injectable()
    ], ConditionsService);
    return ConditionsService;
}());
exports.ConditionsService = ConditionsService;
//# sourceMappingURL=conditions.service.js.map