"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var allergy_1 = require("../models/allergy");
var allergies = [
    { id: 1, name: 'Lactose-intolerant', startDate: 'Jan 4th, 2002', endDate: '', meds: ['-'] },
    { id: 1, name: 'Aspirin-allergic', startDate: 'Mar 4th, 2004', endDate: '', meds: ['-'] }
];
var AllergyService = /** @class */ (function () {
    function AllergyService(http) {
        this.http = http;
    }
    AllergyService.prototype.getAllergies = function () {
        return Promise.resolve(allergies);
    };
    AllergyService.prototype.getAllergiesCount = function () {
        return allergies.length;
    };
    AllergyService.prototype.addAllergy = function (name, startDate, endDate, meds) {
        var allerga = new allergy_1.Allergy(allergies.length, name, startDate, endDate, meds);
        allergies.push(allerga);
        return allerga;
    };
    AllergyService.prototype.getAllergy = function (id) {
        return Promise.resolve(allergies[id]);
        //if (id)
        //    return Promise.resolve(allergies[id]);
        //else
        //    return {};
    };
    AllergyService.prototype.getAllerg = function (id) {
        return allergies[id];
    };
    AllergyService.prototype.searchAllergies = function (term) {
        return allergies;
    };
    AllergyService = __decorate([
        core_1.Injectable()
    ], AllergyService);
    return AllergyService;
}());
exports.AllergyService = AllergyService;
//# sourceMappingURL=allergy.service.js.map