"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var meal_1 = require("../models/meal");
var meals = [
    { id: 1, description: 'Home Cooked Diner', foodType: 'Veggies, White Meal', date: 'Jan 3rd, 2017', calories: 800 },
    { id: 1, description: 'Heavy Breakfast', foodType: 'Protein, Whole grain', date: 'Jan 16th, 2017', calories: 460 }
];
var MealsService = /** @class */ (function () {
    function MealsService(http) {
        this.http = http;
    }
    MealsService.prototype.getMeals = function () {
        return Promise.resolve(meals);
    };
    MealsService.prototype.getMealsCount = function () {
        return meals.length;
    };
    MealsService.prototype.addMeal = function (desc, type, date, calories) {
        var meal = new meal_1.Meal(meals.length, desc, type, date, calories);
        meals.push(meal);
        return meal;
    };
    MealsService.prototype.getMeal = function (id) {
        return Promise.resolve(meals[id]);
        //if (id)
        //    return Promise.resolve(meals[id]);
        //else
        //    return {};
    };
    MealsService.prototype.getMea = function (id) {
        return meals[id];
    };
    MealsService.prototype.searchMeals = function (term) {
        return meals;
    };
    MealsService = __decorate([
        core_1.Injectable()
    ], MealsService);
    return MealsService;
}());
exports.MealsService = MealsService;
//# sourceMappingURL=meal.service.js.map