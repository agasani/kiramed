"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length,
        r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", {value: true});
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var components_1 = require("./components");
var guards_1 = require("../shared/guards");
var routes = [
    {
        path: '',
        canActivate: [guards_1.AuthGuard],
        component: components_1.PatientComponent,
        children: [
            {path: 'patient/:id', component: components_1.PatientProfileComponent},
            {path: 'patient-meds/:id', component: components_1.PatientMedsComponent},
            {path: 'patient-vitals/:id', component: components_1.PatientVitalsComponent},
            {path: 'patient-labs/:id', component: components_1.PatientLabsComponent},
            {path: 'patient-surgeries/:id', component: components_1.PatientSurgeryComponent},
            {path: 'patient-conditions/:id', component: components_1.PatientConditionsComponent},
            {path: 'patient-visits/:id', component: components_1.PatientVisitsComponent},
            {path: 'patient-docs/:id', component: components_1.PatientDocsComponent},
            {path: 'patient-vax/:id', component: components_1.PatientVaxComponent},
            {path: 'patient-transfers/:id', component: components_1.PatientTransfersComponent},
            {path: 'patient-allergies/:id', component: components_1.PatientAllergiesComponent},
            {path: 'patient-wellness/:id', component: components_1.PatientWellnessComponent},
            {path: 'patient-fitness/:id', component: components_1.PatientFitnessComponent}
        ]
    }
];
var PatientRoutingModule = /** @class */ (function () {
    function PatientRoutingModule() {
    }

    PatientRoutingModule = __decorate([
        core_1.NgModule({
            imports: [router_1.RouterModule.forChild(routes)],
            exports: [router_1.RouterModule]
        })
    ], PatientRoutingModule);
    return PatientRoutingModule;
}());
exports.PatientRoutingModule = PatientRoutingModule;
//# sourceMappingURL=patient-routing.module.js.map