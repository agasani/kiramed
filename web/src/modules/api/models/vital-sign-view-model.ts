/* tslint:disable */

export interface VitalSignViewModel {

  type?: string;

  dateTimeTaken?: string;

  unit?: string;

  encounterId?: number;

  value?: number;

  severity?: string;

  id?: number;
}
