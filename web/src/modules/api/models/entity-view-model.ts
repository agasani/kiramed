/* tslint:disable */
import { EntityDemographicViewModel } from './entity-demographic-view-model';
import { EntityContactViewModel } from './entity-contact-view-model';
import { EntityAddressViewModel } from './entity-address-view-model';

export interface EntityViewModel {

  organizationId?: number;

  id?: number;

  type?: string;

  category?: string;

  lastname?: string;

  firstname?: string;

  middlename?: string;

  externalId?: string;

  isActive?: boolean;

  entryDate?: string;

  specialty?: string;

  demographic?: EntityDemographicViewModel;

  contacts?: EntityContactViewModel[];

  addresses?: EntityAddressViewModel[];
}
