/* tslint:disable */

export interface VisitReason {

  id?: number;

  reason?: string;

  relatedKnownIssue?: number;
}
