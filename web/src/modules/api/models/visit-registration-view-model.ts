/* tslint:disable */

export interface VisitRegistrationViewModel {

  visitDateTime: string;

  id?: number;

  patientId: number;

  isReferral?: boolean;

  isFollowUp?: boolean;

  isPatientEnsured: boolean;

  receptionistId: string;

  organizationId: number;

  visitReason: string;

  nurserId?: string;

  doctorId?: string;

  encounterId?: number;

  scheduledDateTime?: string;

  ischeduled?: boolean;
}
