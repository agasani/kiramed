/* tslint:disable */

export interface ActionViewModel {

  id?: number;

  claimNumber?: number;

  actionName?: string;

  controllerName?: string;

  dateTimeCreated?: string;
}
