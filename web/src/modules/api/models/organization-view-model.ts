/* tslint:disable */

export interface OrganizationViewModel {

  nationalId?: string;

  id?: number;

  countryId?: number;

  name: string;

  shortName: string;

  entityId?: number;

  isDeleted?: string;

  dateTimeCreated?: string;

  parentOrganizationId?: number;

  orgSystemId?: string;

  systemId?: number;
}
