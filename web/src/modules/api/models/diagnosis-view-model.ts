/* tslint:disable */

export interface DiagnosisViewModel {

  id?: number;

  codingName?: string;

  codingId?: number;

  name?: string;

  codeFullName?: string;

  code?: string;

  prefix?: string;
}
