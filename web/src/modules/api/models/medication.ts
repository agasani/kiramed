/* tslint:disable */
import { Patient } from './patient';

export interface Medication {

  screenDescription?: string;

  id?: number;

  patient?: Patient;

  medicationNameId?: number;

  medicationName?: string;

  isCustom?: boolean;

  beginDate?: string;

  endDate?: string;

  lastUpdateDate?: string;

  screeningCount?: number;

  patientId: number;

  classification?: string;

  isActinRequired?: boolean;

  isHighRisk?: boolean;

  newChangeExists?: string;

  medicationCustomId?: number;

  financialResponsable?: string;

  relatedToTerminalIllness?: boolean;

  relatedDiagnosis?: string;

  overTheCounter?: boolean;

  orderDate?: string;
}
