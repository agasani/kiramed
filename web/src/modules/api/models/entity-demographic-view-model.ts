/* tslint:disable */

export interface EntityDemographicViewModel {

  dob?: string;

  id?: number;

  secondaryLanguage?: string;

  primaryLanguage?: string;

  nationalID?: string;

  entityId?: number;

  sex?: string;

  martalStatus?: string;

  religion?: string;

  education?: string;

  income?: number;
}
