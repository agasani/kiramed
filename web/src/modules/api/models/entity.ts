/* tslint:disable */
import { EntityDemographic } from './entity-demographic';
import { EntityContact } from './entity-contact';
import { EntityAddress } from './entity-address';

export interface Entity {

  entityDemographicId: number;

  externalId?: string;

  category?: string;

  lastname?: string;

  firstname?: string;

  middlename?: string;

  isActive?: boolean;

  entryDate?: string;

  type?: string;

  specialty?: string;

  demographic?: EntityDemographic;

  contacts?: EntityContact[];

  addresses?: EntityAddress[];

  countryId: number;

  isDeleted?: string;

  id?: number;

  dateTimeCreated?: string;
}
