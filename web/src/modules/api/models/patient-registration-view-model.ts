/* tslint:disable */
import { EntityDemographicViewModel } from './entity-demographic-view-model';
import { EntityContactViewModel } from './entity-contact-view-model';
import { EntityAddressViewModel } from './entity-address-view-model';

export interface PatientRegistrationViewModel {

  countryId: number;

  deathDate?: string;

  tracknetNumber?: string;

  allergyNote?: string;

  nonClinicalNote?: string;

  acceptedHIE?: boolean;

  noKnownAllergy?: boolean;

  noKnownMed?: boolean;

  nationalPatientId: string;

  forceCreate?: boolean;

  medicalRecordNumber: string;

  firstName: string;

  secondName: string;

  nationalId: string;

  parentId?: number;

  email: string;

  phoneNumber: string;

  middleName?: string;

  demographic?: EntityDemographicViewModel;

  contacts?: EntityContactViewModel[];

  addresses?: EntityAddressViewModel[];
}
