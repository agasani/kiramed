/* tslint:disable */

export interface VisitQueueViewModel {

  queueOrderNumber?: number;

  arrivalDateTime?: string;

  firstName?: string;

  lastName?: string;

  patientId?: string;

  visitType?: number;

  relationship?: number;
}
