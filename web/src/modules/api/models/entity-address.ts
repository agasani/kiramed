/* tslint:disable */

export interface EntityAddress {

  timeZoneId?: number;

  id?: number;

  label?: string;

  directions?: string;

  address1?: string;

  address2?: string;

  city?: string;

  state?: string;

  location?: string;

  timeZone?: number;

  entityId: number;

  hasDaylightSaving?: boolean;

  zip?: string;

  district?: string;

  isMailingAddress?: boolean;

  country: string;

  beginDate?: string;

  endDate?: string;

  isDeleted?: string;

  dateTimeCreated?: string;
}
