/* tslint:disable */
import { VisitReason } from './visit-reason';

export interface AppointmentCreationViewModel {

  email?: string;

  dateTimeModified?: string;

  organizationId: number;

  status?: number;

  firstName: string;

  lastName: string;

  appointmentDate: string;

  phoneNumber: string;

  nationalId: string;

  requireInitialPayment?: boolean;

  visitReasons?: VisitReason[];

  patientRelationship?: number;
}
