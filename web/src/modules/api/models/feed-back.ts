/* tslint:disable */

export interface FeedBack {

  err?: boolean;

  message?: string;

  data?: string;
}
