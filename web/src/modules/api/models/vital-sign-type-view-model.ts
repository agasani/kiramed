/* tslint:disable */

export interface VitalSignTypeViewModel {

  name: string;

  description: string;

  unit: string;

  vitalSignTypeId?: number;
}
