/* tslint:disable */

export interface CountryRegistrationViewModel {

  currency?: string;

  id?: number;

  code: string;

  abbreviation: string;

  officialLanguage?: string;

  name: string;

  adminFirstName: string;

  adminSecondName: string;

  adminNationalId: string;

  adminEmail: string;

  adminPhoneNumber: string;
}
