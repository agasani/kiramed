/* tslint:disable */

export interface CountryViewModel {

  id?: number;

  name: string;

  code: string;

  abbreviation: string;

  officialLanguage?: string;

  currency?: string;

  isDeleted?: boolean;
}
