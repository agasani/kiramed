/* tslint:disable */

export interface UserRegisterViewModel {

  parentId?: number;

  userName: string;

  countryId: number;

  firstName: string;

  secondName: string;

  nationalId: string;

  password?: string;

  email: string;

  phoneNumber: string;

  roles: string[];

  startDate?: string;

  organizationId?: number;

  middleName?: string;
}
