/* tslint:disable */
import { EntityDemographicViewModel } from './entity-demographic-view-model';
import { EntityContactViewModel } from './entity-contact-view-model';
import { EntityAddressViewModel } from './entity-address-view-model';

export interface OrganizationRegistrationViewModel {

  email: string;

  countryId: number;

  secondName: string;

  nationalId: string;

  parentId?: number;

  firstName: string;

  phoneNumber: string;

  middleName?: string;

  demographic?: EntityDemographicViewModel;

  contacts?: EntityContactViewModel[];

  addresses?: EntityAddressViewModel[];
}
