/* tslint:disable */

export interface DiagnosisInfo {

  codingVersion?: number;

  name?: string;

  code?: string;
}
