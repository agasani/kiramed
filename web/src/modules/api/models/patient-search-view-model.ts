/* tslint:disable */

export interface PatientSearchViewModel {

  firstName?: string;

  lastName?: string;

  id?: number;

  entityId?: number;

  demographicId?: number;

  nationalID?: string;

  dob?: string;
}
