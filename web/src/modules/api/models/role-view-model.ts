/* tslint:disable */

export interface RoleViewModel {

  /**
   * Role required string name
   */
  name: string;

  /**
   * Role required maxlength 100 string description
   */
  description: string;

  /**
   * Role string roleId
   */
  roleId?: string;

  /**
   * Role parent string organization id
   */
  organizationId: number;

  /**
   * Role created date
   */
  createdDate?: string;

  /**
   * Role modified date
   */
  modifiedDate?: string;

  /**
   * Role srting type
   */
  type?: number;

  /**
   * Role integer id
   */
  id?: number;

  /**
   * Role array of string claims
   */
  claims?: string[];
}
