/* tslint:disable */

export interface VisitQueueCreateViewModel {

  organizationId?: number;

  firstName?: string;

  lastName?: string;

  arrivedAt?: string;

  visitType?: number;

  relationship?: number;
}
