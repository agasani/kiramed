/* tslint:disable */

export interface UserRoleViewModel {

  email?: string;

  role?: string;

  organizationId?: number;

  date?: string;
}
