/* tslint:disable */
import { Entity } from './entity';
import { Medication } from './medication';

export interface Patient {

  acceptedHIE?: boolean;

  entityId: number;

  entity?: Entity;

  deathDate?: string;

  medicalRecordNumber: string;

  tracknetNumber?: string;

  allergyNote?: string;

  nonClinicalNote?: string;

  countryId?: number;

  noKnownAllergy?: boolean;

  noKnownMed?: boolean;

  meds?: Medication[];

  nationalPatientId: string;

  id?: number;

  isDeleted?: string;

  dateTimeCreated?: string;
}
