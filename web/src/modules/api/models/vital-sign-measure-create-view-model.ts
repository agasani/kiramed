/* tslint:disable */

export interface VitalSignMeasureCreateViewModel {

  vitalSignTypeId?: number;

  ageRange: number;

  minAge: number;

  maxAge: number;

  minValue: number;

  maxValue: number;
}
