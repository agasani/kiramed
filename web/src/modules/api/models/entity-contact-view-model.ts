/* tslint:disable */

export interface EntityContactViewModel {

  id?: number;

  contactTypeId?: number;

  value?: string;

  label?: string;

  beginDate?: string;

  endDate?: string;

  deactivate?: boolean;
}
