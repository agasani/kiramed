/* tslint:disable */

export interface EntityAddressViewModel {

  timeZoneId?: number;

  id?: number;

  directions?: string;

  address1?: string;

  address2?: string;

  city?: string;

  state?: string;

  location?: string;

  timeZone?: number;

  label?: string;

  hasDaylightSaving?: boolean;

  zip?: string;

  district?: string;

  isMailingAddress?: boolean;

  country: string;

  beginDate?: string;

  endDate?: string;

  deactivate?: boolean;
}
