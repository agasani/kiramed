/* tslint:disable */
import { PatientViewModel } from './patient-view-model';

export interface EncounterViewModel {

  patientId?: number;

  patientViewModel?: PatientViewModel;

  organizationId?: number;

  admittingPhysicianID?: string;

  admittingPhysicianName?: string;

  admissionDate?: string;

  visitId?: number;
}
