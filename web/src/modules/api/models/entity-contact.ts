/* tslint:disable */

export interface EntityContact {

  id?: number;

  entityId?: number;

  contactTypeId?: number;

  value?: string;

  label?: string;

  beginDate?: string;

  endDate?: string;
}
