/* tslint:disable */
import { DiagnosisInfo } from './diagnosis-info';

export interface PatientDiagnosisCreateViewModel {

  encounterId?: number;

  createdBy?: string;

  description?: string;

  type?: number;

  rank?: number;

  diagnosisInfo?: DiagnosisInfo;
}
