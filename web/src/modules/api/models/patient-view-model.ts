/* tslint:disable */
import { EntityViewModel } from './entity-view-model';

export interface PatientViewModel {

  isDeleted?: string;

  id?: number;

  deathDate?: string;

  medicalRecordNumber: string;

  tracknetNumber?: string;

  allergyNote?: string;

  nonClinicalNote?: string;

  acceptedHIE?: boolean;

  noKnownAllergy?: boolean;

  noKnownMed?: boolean;

  entityId?: number;

  nationalPatientId: string;

  dob?: string;

  sex?: string;

  entity?: EntityViewModel;

  firstName?: string;

  lastName?: string;

  middleName?: string;

  email?: string;

  phoneNumber?: string;

  nationalId?: string;
}
