/* tslint:disable */

export interface PatientDiagnosisViewModel {

  type?: number;

  id?: number;

  visitDateTime?: string;

  createdBy?: string;

  rank?: number;

  encounterId?: number;

  typeName?: string;

  codingVersion?: number;

  versionName?: string;

  code?: string;

  name?: string;
}
