/* tslint:disable */

export interface TransferCreateViewModel {

  arrivalDateTime?: string;

  receivingOrganizationId?: number;

  relationship?: number;

  originOrganizationId?: number;

  nationalPatientId?: string;

  transferPhysicianUserId?: string;

  patientId?: number;

  encounterId?: number;
}
