/* tslint:disable */

export interface VisitViewModel {

  encounterId?: number;

  id?: number;

  patientId: number;

  isReferral?: boolean;

  isFollowUp?: boolean;

  isPatientEnsured: boolean;

  receptionistId: string;

  nurserId?: string;

  doctorId?: string;

  organizationId: number;

  visitDateTime: string;

  visitReason: string;

  firstName?: string;

  lastName?: string;

  medicalRecordNumber?: string;

  trackNetNumber?: string;

  appointmentId?: number;

  status?: number;
}
