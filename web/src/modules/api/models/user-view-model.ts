/* tslint:disable */

export interface UserViewModel {

  lastLoggedIn?: string;

  id: string;

  userName: string;

  email?: string;

  isLocked?: boolean;

  createdDate?: string;

  entityId: number;

  modifiedDate?: string;

  isDeleted?: boolean;

  isDisabled?: boolean;

  roles?: string[];

  claims?: string[];
}
