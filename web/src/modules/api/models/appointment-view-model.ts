/* tslint:disable */
import { VisitReason } from './visit-reason';

export interface AppointmentViewModel {

  phoneNumber?: string;

  dateTimeModified?: string;

  organizationId?: number;

  status?: number;

  firstName?: string;

  lastName?: string;

  email?: string;

  appointmentDate?: string;

  nationalId?: string;

  requireInitialPayment?: boolean;

  visitReasons?: VisitReason[];

  relationship?: number;

  id?: number;

  isDeleted?: string;

  dateTimeCreated?: string;
}
