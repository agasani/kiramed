/* tslint:disable */

export interface EntityDemographic {

  martalStatus?: string;

  secondaryLanguage?: string;

  primaryLanguage?: string;

  nationalID?: string;

  dob?: string;

  sex?: string;

  entityId: number;

  religion?: string;

  education?: string;

  income?: number;

  isDeleted?: string;

  id?: number;

  dateTimeCreated?: string;
}
