/* tslint:disable */

export interface VitalSignCreateViewModel {

  vitalSignTypeId: number;

  encounterId: number;

  value: number;

  notes?: string;
}
