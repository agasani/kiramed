/* tslint:disable */

export interface VitalSignMeasureViewModel {

  vitalSignTypeId?: number;

  type?: string;

  ageRange?: string;

  minAge?: number;

  maxAge?: number;

  minValue?: number;

  maxValue?: number;
}
