/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators/map';
import { filter } from 'rxjs/operators/filter';

import { EntityAddressViewModel } from '../models/entity-address-view-model';
import { FeedBack } from '../models/feed-back';

@Injectable()
export class EntityAddressService extends BaseService {
  constructor(config: ApiConfiguration,
              http: HttpClient) {
    super(config, http);
  }

  /**
   * @param params The `EntityAddressService.ApiEntityAddressGetEntityByIdByIdGetParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully retrieved
   */
  ApiEntityAddressGetEntityByIdByIdGetResponse(params: EntityAddressService.ApiEntityAddressGetEntityByIdByIdGetParams): Observable<HttpResponse<EntityAddressViewModel[]>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'GET',
        this.rootUrl + `/api/EntityAddress/GetEntityById/${params.id}`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: EntityAddressViewModel[] = null;
          _body = _resp.body as EntityAddressViewModel[];
          return _resp.clone({ body: _body }) as HttpResponse<EntityAddressViewModel[]>;
        })
    );
  }

  /**
   * @param params The `EntityAddressService.ApiEntityAddressGetEntityByIdByIdGetParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully retrieved
   */
  ApiEntityAddressGetEntityByIdByIdGet(params: EntityAddressService.ApiEntityAddressGetEntityByIdByIdGetParams): Observable<EntityAddressViewModel[]> {
    return this.ApiEntityAddressGetEntityByIdByIdGetResponse(params).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `EntityAddressService.ApiEntityAddressByIdPutParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `vModel`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully updated
   */
  ApiEntityAddressByIdPutResponse(params: EntityAddressService.ApiEntityAddressByIdPutParams): Observable<HttpResponse<FeedBack>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.vModel;
    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'PUT',
        this.rootUrl + `/api/EntityAddress/${params.id}`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: FeedBack = null;
          _body = _resp.body as FeedBack;
          return _resp.clone({ body: _body }) as HttpResponse<FeedBack>;
        })
    );
  }

  /**
   * @param params The `EntityAddressService.ApiEntityAddressByIdPutParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `vModel`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully updated
   */
  ApiEntityAddressByIdPut(params: EntityAddressService.ApiEntityAddressByIdPutParams): Observable<FeedBack> {
    return this.ApiEntityAddressByIdPutResponse(params).pipe(
        map(_r => _r.body)
    );
  }
}

export module EntityAddressService {

  /**
   * Parameters for ApiEntityAddressGetEntityByIdByIdGet
   */
  export interface ApiEntityAddressGetEntityByIdByIdGetParams {

    id: number;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }

  /**
   * Parameters for ApiEntityAddressByIdPut
   */
  export interface ApiEntityAddressByIdPutParams {

    id: number;

    vModel?: EntityAddressViewModel;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }
}
