/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators/map';
import { filter } from 'rxjs/operators/filter';

import { UserViewModel } from '../models/user-view-model';
import { FeedBack } from '../models/feed-back';
import { UserRegisterViewModel } from '../models/user-register-view-model';
import { UserLoginViewModel } from '../models/user-login-view-model';
import { UserRoleViewModel } from '../models/user-role-view-model';
import { EntityViewModel } from '../models/entity-view-model';
import { OrganizationViewModel } from '../models/organization-view-model';

@Injectable()
export class UserService extends BaseService {
  constructor(config: ApiConfiguration,
              http: HttpClient) {
    super(config, http);
  }

  /**
   * @param correlationId Correlation Id for the request
   * @return Successfully listed
   */
  ApiUserGetResponse(correlationId?: string): Observable<HttpResponse<UserViewModel[]>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (correlationId != null) __headers = __headers.set('correlationId', correlationId.toString());
    let req = new HttpRequest<any>(
        'GET',
        this.rootUrl + `/api/User`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: UserViewModel[] = null;
          _body = _resp.body as UserViewModel[];
          return _resp.clone({ body: _body }) as HttpResponse<UserViewModel[]>;
        })
    );
  }

  /**
   * @param correlationId Correlation Id for the request
   * @return Successfully listed
   */
  ApiUserGet(correlationId?: string): Observable<UserViewModel[]> {
    return this.ApiUserGetResponse(correlationId).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `UserService.ApiUserPostParams` containing the following parameters:
   *
   * - `model`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return User successfully created
   */
  ApiUserPostResponse(params: UserService.ApiUserPostParams): Observable<HttpResponse<FeedBack>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = params.model;
    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'POST',
        this.rootUrl + `/api/User`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: FeedBack = null;
          _body = _resp.body as FeedBack;
          return _resp.clone({ body: _body }) as HttpResponse<FeedBack>;
        })
    );
  }

  /**
   * @param params The `UserService.ApiUserPostParams` containing the following parameters:
   *
   * - `model`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return User successfully created
   */
  ApiUserPost(params: UserService.ApiUserPostParams): Observable<FeedBack> {
    return this.ApiUserPostResponse(params).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `UserService.ApiUserOrganizationUsersByOrgIdGetParams` containing the following parameters:
   *
   * - `orgId`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully listed
   */
  ApiUserOrganizationUsersByOrgIdGetResponse(params: UserService.ApiUserOrganizationUsersByOrgIdGetParams): Observable<HttpResponse<UserViewModel[]>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'GET',
        this.rootUrl + `/api/User/OrganizationUsers/${params.orgId}`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: UserViewModel[] = null;
          _body = _resp.body as UserViewModel[];
          return _resp.clone({ body: _body }) as HttpResponse<UserViewModel[]>;
        })
    );
  }

  /**
   * @param params The `UserService.ApiUserOrganizationUsersByOrgIdGetParams` containing the following parameters:
   *
   * - `orgId`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully listed
   */
  ApiUserOrganizationUsersByOrgIdGet(params: UserService.ApiUserOrganizationUsersByOrgIdGetParams): Observable<UserViewModel[]> {
    return this.ApiUserOrganizationUsersByOrgIdGetResponse(params).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `UserService.ApiUserOrganizationUsersByRoleByOrgIdByRoleNameGetParams` containing the following parameters:
   *
   * - `roleName`:
   *
   * - `orgId`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully listed
   */
  ApiUserOrganizationUsersByRoleByOrgIdByRoleNameGetResponse(params: UserService.ApiUserOrganizationUsersByRoleByOrgIdByRoleNameGetParams): Observable<HttpResponse<UserViewModel[]>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;


    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'GET',
        this.rootUrl + `/api/User/OrganizationUsersByRole/${params.orgId}/${params.roleName}`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: UserViewModel[] = null;
          _body = _resp.body as UserViewModel[];
          return _resp.clone({ body: _body }) as HttpResponse<UserViewModel[]>;
        })
    );
  }

  /**
   * @param params The `UserService.ApiUserOrganizationUsersByRoleByOrgIdByRoleNameGetParams` containing the following parameters:
   *
   * - `roleName`:
   *
   * - `orgId`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully listed
   */
  ApiUserOrganizationUsersByRoleByOrgIdByRoleNameGet(params: UserService.ApiUserOrganizationUsersByRoleByOrgIdByRoleNameGetParams): Observable<UserViewModel[]> {
    return this.ApiUserOrganizationUsersByRoleByOrgIdByRoleNameGetResponse(params).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `UserService.ApiUserLoginPostParams` containing the following parameters:
   *
   * - `loginModel`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully listed
   */
  ApiUserLoginPostResponse(params: UserService.ApiUserLoginPostParams): Observable<HttpResponse<FeedBack>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = params.loginModel;
    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'POST',
        this.rootUrl + `/api/User/Login`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: FeedBack = null;
          _body = _resp.body as FeedBack;
          return _resp.clone({ body: _body }) as HttpResponse<FeedBack>;
        })
    );
  }

  /**
   * @param params The `UserService.ApiUserLoginPostParams` containing the following parameters:
   *
   * - `loginModel`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully listed
   */
  ApiUserLoginPost(params: UserService.ApiUserLoginPostParams): Observable<FeedBack> {
    return this.ApiUserLoginPostResponse(params).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `UserService.ApiUserDetailByEmailGetParams` containing the following parameters:
   *
   * - `email`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully retrieved
   */
  ApiUserDetailByEmailGetResponse(params: UserService.ApiUserDetailByEmailGetParams): Observable<HttpResponse<UserViewModel>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'GET',
        this.rootUrl + `/api/User/Detail/${params.email}`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: UserViewModel = null;
          _body = _resp.body as UserViewModel;
          return _resp.clone({ body: _body }) as HttpResponse<UserViewModel>;
        })
    );
  }

  /**
   * @param params The `UserService.ApiUserDetailByEmailGetParams` containing the following parameters:
   *
   * - `email`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully retrieved
   */
  ApiUserDetailByEmailGet(params: UserService.ApiUserDetailByEmailGetParams): Observable<UserViewModel> {
    return this.ApiUserDetailByEmailGetResponse(params).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param correlationId Correlation Id for the request
   * @return Create form opened
   */
  ApiUserNewGetResponse(correlationId?: string): Observable<HttpResponse<FeedBack>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (correlationId != null) __headers = __headers.set('correlationId', correlationId.toString());
    let req = new HttpRequest<any>(
        'GET',
        this.rootUrl + `/api/User/New`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: FeedBack = null;
          _body = _resp.body as FeedBack;
          return _resp.clone({ body: _body }) as HttpResponse<FeedBack>;
        })
    );
  }

  /**
   * @param correlationId Correlation Id for the request
   * @return Create form opened
   */
  ApiUserNewGet(correlationId?: string): Observable<FeedBack> {
    return this.ApiUserNewGetResponse(correlationId).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `UserService.ApiUserAddToRolePostParams` containing the following parameters:
   *
   * - `model`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return User added to role
   */
  ApiUserAddToRolePostResponse(params: UserService.ApiUserAddToRolePostParams): Observable<HttpResponse<FeedBack>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = params.model;
    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'POST',
        this.rootUrl + `/api/User/AddToRole`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: FeedBack = null;
          _body = _resp.body as FeedBack;
          return _resp.clone({ body: _body }) as HttpResponse<FeedBack>;
        })
    );
  }

  /**
   * @param params The `UserService.ApiUserAddToRolePostParams` containing the following parameters:
   *
   * - `model`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return User added to role
   */
  ApiUserAddToRolePost(params: UserService.ApiUserAddToRolePostParams): Observable<FeedBack> {
    return this.ApiUserAddToRolePostResponse(params).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `UserService.ApiUserAddClaimsToRolePostParams` containing the following parameters:
   *
   * - `roleName`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * - `claims`:
   */
  ApiUserAddClaimsToRolePostResponse(params: UserService.ApiUserAddClaimsToRolePostParams): Observable<HttpResponse<void>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());

    let req = new HttpRequest<any>(
        'POST',
        this.rootUrl + `/api/User/addClaimsToRole`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'text'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: void = null;

          return _resp.clone({ body: _body }) as HttpResponse<void>;
        })
    );
  }

  /**
   * @param params The `UserService.ApiUserAddClaimsToRolePostParams` containing the following parameters:
   *
   * - `roleName`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * - `claims`:
   */
  ApiUserAddClaimsToRolePost(params: UserService.ApiUserAddClaimsToRolePostParams): Observable<void> {
    return this.ApiUserAddClaimsToRolePostResponse(params).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `UserService.ApiUserRemoveFromRolePostParams` containing the following parameters:
   *
   * - `model`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return User removed from role
   */
  ApiUserRemoveFromRolePostResponse(params: UserService.ApiUserRemoveFromRolePostParams): Observable<HttpResponse<FeedBack>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = params.model;
    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'POST',
        this.rootUrl + `/api/User/RemoveFromRole`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: FeedBack = null;
          _body = _resp.body as FeedBack;
          return _resp.clone({ body: _body }) as HttpResponse<FeedBack>;
        })
    );
  }

  /**
   * @param params The `UserService.ApiUserRemoveFromRolePostParams` containing the following parameters:
   *
   * - `model`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return User removed from role
   */
  ApiUserRemoveFromRolePost(params: UserService.ApiUserRemoveFromRolePostParams): Observable<FeedBack> {
    return this.ApiUserRemoveFromRolePostResponse(params).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `UserService.ApiUserLockPostParams` containing the following parameters:
   *
   * - `email`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return User locked successfully
   */
  ApiUserLockPostResponse(params: UserService.ApiUserLockPostParams): Observable<HttpResponse<FeedBack>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'POST',
        this.rootUrl + `/api/User/Lock`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: FeedBack = null;
          _body = _resp.body as FeedBack;
          return _resp.clone({ body: _body }) as HttpResponse<FeedBack>;
        })
    );
  }

  /**
   * @param params The `UserService.ApiUserLockPostParams` containing the following parameters:
   *
   * - `email`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return User locked successfully
   */
  ApiUserLockPost(params: UserService.ApiUserLockPostParams): Observable<FeedBack> {
    return this.ApiUserLockPostResponse(params).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `UserService.ApiUserUnlockPostParams` containing the following parameters:
   *
   * - `email`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return User unlocked successfully
   */
  ApiUserUnlockPostResponse(params: UserService.ApiUserUnlockPostParams): Observable<HttpResponse<FeedBack>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'POST',
        this.rootUrl + `/api/User/Unlock`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: FeedBack = null;
          _body = _resp.body as FeedBack;
          return _resp.clone({ body: _body }) as HttpResponse<FeedBack>;
        })
    );
  }

  /**
   * @param params The `UserService.ApiUserUnlockPostParams` containing the following parameters:
   *
   * - `email`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return User unlocked successfully
   */
  ApiUserUnlockPost(params: UserService.ApiUserUnlockPostParams): Observable<FeedBack> {
    return this.ApiUserUnlockPostResponse(params).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `UserService.ApiUserByUserIdPutParams` containing the following parameters:
   *
   * - `userId`:
   *
   * - `model`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return User unlocked successfully
   */
  ApiUserByUserIdPutResponse(params: UserService.ApiUserByUserIdPutParams): Observable<HttpResponse<FeedBack>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.model;
    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'PUT',
        this.rootUrl + `/api/User/${params.userId}`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: FeedBack = null;
          _body = _resp.body as FeedBack;
          return _resp.clone({ body: _body }) as HttpResponse<FeedBack>;
        })
    );
  }

  /**
   * @param params The `UserService.ApiUserByUserIdPutParams` containing the following parameters:
   *
   * - `userId`:
   *
   * - `model`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return User unlocked successfully
   */
  ApiUserByUserIdPut(params: UserService.ApiUserByUserIdPutParams): Observable<FeedBack> {
    return this.ApiUserByUserIdPutResponse(params).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `UserService.ApiUserGetUserOrgsByUserIdGetParams` containing the following parameters:
   *
   * - `userId`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Organization listed successfully
   */
  ApiUserGetUserOrgsByUserIdGetResponse(params: UserService.ApiUserGetUserOrgsByUserIdGetParams): Observable<HttpResponse<OrganizationViewModel[]>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'GET',
        this.rootUrl + `/api/User/GetUserOrgs/${params.userId}`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: OrganizationViewModel[] = null;
          _body = _resp.body as OrganizationViewModel[];
          return _resp.clone({ body: _body }) as HttpResponse<OrganizationViewModel[]>;
        })
    );
  }

  /**
   * @param params The `UserService.ApiUserGetUserOrgsByUserIdGetParams` containing the following parameters:
   *
   * - `userId`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Organization listed successfully
   */
  ApiUserGetUserOrgsByUserIdGet(params: UserService.ApiUserGetUserOrgsByUserIdGetParams): Observable<OrganizationViewModel[]> {
    return this.ApiUserGetUserOrgsByUserIdGetResponse(params).pipe(
        map(_r => _r.body)
    );
  }
}

export module UserService {

  /**
   * Parameters for ApiUserPost
   */
  export interface ApiUserPostParams {

    model?: UserRegisterViewModel;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }

  /**
   * Parameters for ApiUserOrganizationUsersByOrgIdGet
   */
  export interface ApiUserOrganizationUsersByOrgIdGetParams {

    orgId: number;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }

  /**
   * Parameters for ApiUserOrganizationUsersByRoleByOrgIdByRoleNameGet
   */
  export interface ApiUserOrganizationUsersByRoleByOrgIdByRoleNameGetParams {

    roleName: string;

    orgId: number;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }

  /**
   * Parameters for ApiUserLoginPost
   */
  export interface ApiUserLoginPostParams {

    loginModel?: UserLoginViewModel;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }

  /**
   * Parameters for ApiUserDetailByEmailGet
   */
  export interface ApiUserDetailByEmailGetParams {

    email: string;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }

  /**
   * Parameters for ApiUserAddToRolePost
   */
  export interface ApiUserAddToRolePostParams {

    model?: UserRoleViewModel;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }

  /**
   * Parameters for ApiUserAddClaimsToRolePost
   */
  export interface ApiUserAddClaimsToRolePostParams {

    roleName?: string;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;

    claims?: string;
  }

  /**
   * Parameters for ApiUserRemoveFromRolePost
   */
  export interface ApiUserRemoveFromRolePostParams {

    model?: UserRoleViewModel;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }

  /**
   * Parameters for ApiUserLockPost
   */
  export interface ApiUserLockPostParams {

    email?: string;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }

  /**
   * Parameters for ApiUserUnlockPost
   */
  export interface ApiUserUnlockPostParams {

    email?: string;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }

  /**
   * Parameters for ApiUserByUserIdPut
   */
  export interface ApiUserByUserIdPutParams {

    userId: string;

    model?: EntityViewModel;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }

  /**
   * Parameters for ApiUserGetUserOrgsByUserIdGet
   */
  export interface ApiUserGetUserOrgsByUserIdGetParams {

    userId: string;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }
}
