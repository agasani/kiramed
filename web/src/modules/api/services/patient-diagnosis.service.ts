/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators/map';
import { filter } from 'rxjs/operators/filter';

import { PatientDiagnosisViewModel } from '../models/patient-diagnosis-view-model';
import { FeedBack } from '../models/feed-back';
import { PatientDiagnosisCreateViewModel } from '../models/patient-diagnosis-create-view-model';

@Injectable()
export class PatientDiagnosisService extends BaseService {
  constructor(config: ApiConfiguration,
              http: HttpClient) {
    super(config, http);
  }

  /**
   * @param params The `PatientDiagnosisService.ApiPatientDiagnosisGetParams` containing the following parameters:
   *
   * - `encounterId`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Listed successfully
   */
  ApiPatientDiagnosisGetResponse(params: PatientDiagnosisService.ApiPatientDiagnosisGetParams): Observable<HttpResponse<PatientDiagnosisViewModel>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (params.encounterId != null) __params = __params.set('encounterId', params.encounterId.toString());
    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'GET',
        this.rootUrl + `/api/PatientDiagnosis`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: PatientDiagnosisViewModel = null;
          _body = _resp.body as PatientDiagnosisViewModel;
          return _resp.clone({ body: _body }) as HttpResponse<PatientDiagnosisViewModel>;
        })
    );
  }

  /**
   * @param params The `PatientDiagnosisService.ApiPatientDiagnosisGetParams` containing the following parameters:
   *
   * - `encounterId`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Listed successfully
   */
  ApiPatientDiagnosisGet(params: PatientDiagnosisService.ApiPatientDiagnosisGetParams): Observable<PatientDiagnosisViewModel> {
    return this.ApiPatientDiagnosisGetResponse(params).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `PatientDiagnosisService.ApiPatientDiagnosisPostParams` containing the following parameters:
   *
   * - `vModel`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Created successfully
   */
  ApiPatientDiagnosisPostResponse(params: PatientDiagnosisService.ApiPatientDiagnosisPostParams): Observable<HttpResponse<FeedBack>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = params.vModel;
    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'POST',
        this.rootUrl + `/api/PatientDiagnosis`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: FeedBack = null;
          _body = _resp.body as FeedBack;
          return _resp.clone({ body: _body }) as HttpResponse<FeedBack>;
        })
    );
  }

  /**
   * @param params The `PatientDiagnosisService.ApiPatientDiagnosisPostParams` containing the following parameters:
   *
   * - `vModel`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Created successfully
   */
  ApiPatientDiagnosisPost(params: PatientDiagnosisService.ApiPatientDiagnosisPostParams): Observable<FeedBack> {
    return this.ApiPatientDiagnosisPostResponse(params).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `PatientDiagnosisService.ApiPatientDiagnosisGetByPatientByPatientIdGetParams` containing the following parameters:
   *
   * - `patientId`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Listed successfully
   */
  ApiPatientDiagnosisGetByPatientByPatientIdGetResponse(params: PatientDiagnosisService.ApiPatientDiagnosisGetByPatientByPatientIdGetParams): Observable<HttpResponse<PatientDiagnosisViewModel[]>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'GET',
        this.rootUrl + `/api/PatientDiagnosis/GetByPatient/${params.patientId}`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: PatientDiagnosisViewModel[] = null;
          _body = _resp.body as PatientDiagnosisViewModel[];
          return _resp.clone({ body: _body }) as HttpResponse<PatientDiagnosisViewModel[]>;
        })
    );
  }

  /**
   * @param params The `PatientDiagnosisService.ApiPatientDiagnosisGetByPatientByPatientIdGetParams` containing the following parameters:
   *
   * - `patientId`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Listed successfully
   */
  ApiPatientDiagnosisGetByPatientByPatientIdGet(params: PatientDiagnosisService.ApiPatientDiagnosisGetByPatientByPatientIdGetParams): Observable<PatientDiagnosisViewModel[]> {
    return this.ApiPatientDiagnosisGetByPatientByPatientIdGetResponse(params).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `PatientDiagnosisService.ApiPatientDiagnosisDetailByIdGetParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Listed successfully
   */
  ApiPatientDiagnosisDetailByIdGetResponse(params: PatientDiagnosisService.ApiPatientDiagnosisDetailByIdGetParams): Observable<HttpResponse<PatientDiagnosisViewModel>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'GET',
        this.rootUrl + `/api/PatientDiagnosis/Detail/${params.id}`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: PatientDiagnosisViewModel = null;
          _body = _resp.body as PatientDiagnosisViewModel;
          return _resp.clone({ body: _body }) as HttpResponse<PatientDiagnosisViewModel>;
        })
    );
  }

  /**
   * @param params The `PatientDiagnosisService.ApiPatientDiagnosisDetailByIdGetParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Listed successfully
   */
  ApiPatientDiagnosisDetailByIdGet(params: PatientDiagnosisService.ApiPatientDiagnosisDetailByIdGetParams): Observable<PatientDiagnosisViewModel> {
    return this.ApiPatientDiagnosisDetailByIdGetResponse(params).pipe(
        map(_r => _r.body)
    );
  }
}

export module PatientDiagnosisService {

  /**
   * Parameters for ApiPatientDiagnosisGet
   */
  export interface ApiPatientDiagnosisGetParams {

    encounterId: number;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }

  /**
   * Parameters for ApiPatientDiagnosisPost
   */
  export interface ApiPatientDiagnosisPostParams {

    vModel?: PatientDiagnosisCreateViewModel;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }

  /**
   * Parameters for ApiPatientDiagnosisGetByPatientByPatientIdGet
   */
  export interface ApiPatientDiagnosisGetByPatientByPatientIdGetParams {

    patientId: number;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }

  /**
   * Parameters for ApiPatientDiagnosisDetailByIdGet
   */
  export interface ApiPatientDiagnosisDetailByIdGetParams {

    id: number;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }
}
