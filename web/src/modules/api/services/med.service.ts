/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators/map';
import { filter } from 'rxjs/operators/filter';


@Injectable()
export class MedService extends BaseService {
  constructor(config: ApiConfiguration,
              http: HttpClient) {
    super(config, http);
  }

  /**
   * @param correlationId Correlation Id for the request
   * @return Success
   */
  ApiPatientsMedGetResponse(correlationId?: string): Observable<HttpResponse<string[]>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (correlationId != null) __headers = __headers.set('correlationId', correlationId.toString());
    let req = new HttpRequest<any>(
        'GET',
        this.rootUrl + `/api/patients/Med`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: string[] = null;
          _body = _resp.body as string[];
          return _resp.clone({ body: _body }) as HttpResponse<string[]>;
        })
    );
  }

  /**
   * @param correlationId Correlation Id for the request
   * @return Success
   */
  ApiPatientsMedGet(correlationId?: string): Observable<string[]> {
    return this.ApiPatientsMedGetResponse(correlationId).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `MedService.ApiPatientsMedPostParams` containing the following parameters:
   *
   * - `value`:
   *
   * - `correlationId`: Correlation Id for the request
   */
  ApiPatientsMedPostResponse(params: MedService.ApiPatientsMedPostParams): Observable<HttpResponse<void>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = params.value;
    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'POST',
        this.rootUrl + `/api/patients/Med`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'text'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: void = null;

          return _resp.clone({ body: _body }) as HttpResponse<void>;
        })
    );
  }

  /**
   * @param params The `MedService.ApiPatientsMedPostParams` containing the following parameters:
   *
   * - `value`:
   *
   * - `correlationId`: Correlation Id for the request
   */
  ApiPatientsMedPost(params: MedService.ApiPatientsMedPostParams): Observable<void> {
    return this.ApiPatientsMedPostResponse(params).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `MedService.ApiPatientsMedByIdGetParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Success
   */
  ApiPatientsMedByIdGetResponse(params: MedService.ApiPatientsMedByIdGetParams): Observable<HttpResponse<string>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'GET',
        this.rootUrl + `/api/patients/Med/${params.id}`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'text'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: string = null;
          _body = _resp.body as string;
          return _resp.clone({ body: _body }) as HttpResponse<string>;
        })
    );
  }

  /**
   * @param params The `MedService.ApiPatientsMedByIdGetParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Success
   */
  ApiPatientsMedByIdGet(params: MedService.ApiPatientsMedByIdGetParams): Observable<string> {
    return this.ApiPatientsMedByIdGetResponse(params).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `MedService.ApiPatientsMedByIdPutParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `value`:
   *
   * - `correlationId`: Correlation Id for the request
   */
  ApiPatientsMedByIdPutResponse(params: MedService.ApiPatientsMedByIdPutParams): Observable<HttpResponse<void>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.value;
    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'PUT',
        this.rootUrl + `/api/patients/Med/${params.id}`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'text'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: void = null;

          return _resp.clone({ body: _body }) as HttpResponse<void>;
        })
    );
  }

  /**
   * @param params The `MedService.ApiPatientsMedByIdPutParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `value`:
   *
   * - `correlationId`: Correlation Id for the request
   */
  ApiPatientsMedByIdPut(params: MedService.ApiPatientsMedByIdPutParams): Observable<void> {
    return this.ApiPatientsMedByIdPutResponse(params).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `MedService.ApiPatientsMedByIdDeleteParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `correlationId`: Correlation Id for the request
   */
  ApiPatientsMedByIdDeleteResponse(params: MedService.ApiPatientsMedByIdDeleteParams): Observable<HttpResponse<void>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'DELETE',
        this.rootUrl + `/api/patients/Med/${params.id}`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'text'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: void = null;

          return _resp.clone({ body: _body }) as HttpResponse<void>;
        })
    );
  }

  /**
   * @param params The `MedService.ApiPatientsMedByIdDeleteParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `correlationId`: Correlation Id for the request
   */
  ApiPatientsMedByIdDelete(params: MedService.ApiPatientsMedByIdDeleteParams): Observable<void> {
    return this.ApiPatientsMedByIdDeleteResponse(params).pipe(
        map(_r => _r.body)
    );
  }
}

export module MedService {

  /**
   * Parameters for ApiPatientsMedPost
   */
  export interface ApiPatientsMedPostParams {

    value?: string;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }

  /**
   * Parameters for ApiPatientsMedByIdGet
   */
  export interface ApiPatientsMedByIdGetParams {

    id: number;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }

  /**
   * Parameters for ApiPatientsMedByIdPut
   */
  export interface ApiPatientsMedByIdPutParams {

    id: number;

    value?: string;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }

  /**
   * Parameters for ApiPatientsMedByIdDelete
   */
  export interface ApiPatientsMedByIdDeleteParams {

    id: number;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }
}
