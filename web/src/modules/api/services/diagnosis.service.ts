/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators/map';
import { filter } from 'rxjs/operators/filter';

import { DiagnosisViewModel } from '../models/diagnosis-view-model';

@Injectable()
export class DiagnosisService extends BaseService {
  constructor(config: ApiConfiguration,
              http: HttpClient) {
    super(config, http);
  }

  /**
   * @param params The `DiagnosisService.ApiDiagnosisSearchBySearchGetParams` containing the following parameters:
   *
   * - `search`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Model found
   */
  ApiDiagnosisSearchBySearchGetResponse(params: DiagnosisService.ApiDiagnosisSearchBySearchGetParams): Observable<HttpResponse<DiagnosisViewModel[]>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'GET',
        this.rootUrl + `/api/Diagnosis/Search/${params.search}`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: DiagnosisViewModel[] = null;
          _body = _resp.body as DiagnosisViewModel[];
          return _resp.clone({ body: _body }) as HttpResponse<DiagnosisViewModel[]>;
        })
    );
  }

  /**
   * @param params The `DiagnosisService.ApiDiagnosisSearchBySearchGetParams` containing the following parameters:
   *
   * - `search`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Model found
   */
  ApiDiagnosisSearchBySearchGet(params: DiagnosisService.ApiDiagnosisSearchBySearchGetParams): Observable<DiagnosisViewModel[]> {
    return this.ApiDiagnosisSearchBySearchGetResponse(params).pipe(
        map(_r => _r.body)
    );
  }
}

export module DiagnosisService {

  /**
   * Parameters for ApiDiagnosisSearchBySearchGet
   */
  export interface ApiDiagnosisSearchBySearchGetParams {

    search: string;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }
}
