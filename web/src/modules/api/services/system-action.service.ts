/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators/map';
import { filter } from 'rxjs/operators/filter';

import { ActionViewModel } from '../models/action-view-model';
import { FeedBack } from '../models/feed-back';

@Injectable()
export class SystemActionService extends BaseService {
  constructor(config: ApiConfiguration,
              http: HttpClient) {
    super(config, http);
  }

  /**
   * @param correlationId Correlation Id for the request
   * @return Success
   */
  ApiSystemActionGetResponse(correlationId?: string): Observable<HttpResponse<ActionViewModel[]>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (correlationId != null) __headers = __headers.set('correlationId', correlationId.toString());
    let req = new HttpRequest<any>(
        'GET',
        this.rootUrl + `/api/SystemAction`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: ActionViewModel[] = null;
          _body = _resp.body as ActionViewModel[];
          return _resp.clone({ body: _body }) as HttpResponse<ActionViewModel[]>;
        })
    );
  }

  /**
   * @param correlationId Correlation Id for the request
   * @return Success
   */
  ApiSystemActionGet(correlationId?: string): Observable<ActionViewModel[]> {
    return this.ApiSystemActionGetResponse(correlationId).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `SystemActionService.ApiSystemActionPostParams` containing the following parameters:
   *
   * - `model`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Created successfully
   */
  ApiSystemActionPostResponse(params: SystemActionService.ApiSystemActionPostParams): Observable<HttpResponse<FeedBack>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = params.model;
    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'POST',
        this.rootUrl + `/api/SystemAction`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: FeedBack = null;
          _body = _resp.body as FeedBack;
          return _resp.clone({ body: _body }) as HttpResponse<FeedBack>;
        })
    );
  }

  /**
   * @param params The `SystemActionService.ApiSystemActionPostParams` containing the following parameters:
   *
   * - `model`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Created successfully
   */
  ApiSystemActionPost(params: SystemActionService.ApiSystemActionPostParams): Observable<FeedBack> {
    return this.ApiSystemActionPostResponse(params).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `SystemActionService.ApiSystemActionByActionNameDeleteParams` containing the following parameters:
   *
   * - `actionName`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Deleted successfully
   */
  ApiSystemActionByActionNameDeleteResponse(params: SystemActionService.ApiSystemActionByActionNameDeleteParams): Observable<HttpResponse<FeedBack>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'DELETE',
        this.rootUrl + `/api/SystemAction/${params.actionName}`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: FeedBack = null;
          _body = _resp.body as FeedBack;
          return _resp.clone({ body: _body }) as HttpResponse<FeedBack>;
        })
    );
  }

  /**
   * @param params The `SystemActionService.ApiSystemActionByActionNameDeleteParams` containing the following parameters:
   *
   * - `actionName`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Deleted successfully
   */
  ApiSystemActionByActionNameDelete(params: SystemActionService.ApiSystemActionByActionNameDeleteParams): Observable<FeedBack> {
    return this.ApiSystemActionByActionNameDeleteResponse(params).pipe(
        map(_r => _r.body)
    );
  }
}

export module SystemActionService {

  /**
   * Parameters for ApiSystemActionPost
   */
  export interface ApiSystemActionPostParams {

    model?: ActionViewModel;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }

  /**
   * Parameters for ApiSystemActionByActionNameDelete
   */
  export interface ApiSystemActionByActionNameDeleteParams {

    actionName: string;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }
}
