/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators/map';
import { filter } from 'rxjs/operators/filter';

import { VisitViewModel } from '../models/visit-view-model';
import { FeedBack } from '../models/feed-back';
import { VisitRegistrationViewModel } from '../models/visit-registration-view-model';

@Injectable()
export class VisitService extends BaseService {
  constructor(config: ApiConfiguration,
              http: HttpClient) {
    super(config, http);
  }

  /**
   * @param params The `VisitService.ApiVisitGetOrgVisitsByOrgIdGetParams` containing the following parameters:
   *
   * - `orgId`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Listed successfully
   */
  ApiVisitGetOrgVisitsByOrgIdGetResponse(params: VisitService.ApiVisitGetOrgVisitsByOrgIdGetParams): Observable<HttpResponse<VisitViewModel[]>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'GET',
        this.rootUrl + `/api/Visit/GetOrgVisits/${params.orgId}`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: VisitViewModel[] = null;
          _body = _resp.body as VisitViewModel[];
          return _resp.clone({ body: _body }) as HttpResponse<VisitViewModel[]>;
        })
    );
  }

  /**
   * @param params The `VisitService.ApiVisitGetOrgVisitsByOrgIdGetParams` containing the following parameters:
   *
   * - `orgId`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Listed successfully
   */
  ApiVisitGetOrgVisitsByOrgIdGet(params: VisitService.ApiVisitGetOrgVisitsByOrgIdGetParams): Observable<VisitViewModel[]> {
    return this.ApiVisitGetOrgVisitsByOrgIdGetResponse(params).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `VisitService.ApiVisitDetailByVisitIdGetParams` containing the following parameters:
   *
   * - `visitId`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Retrieved successfully
   */
  ApiVisitDetailByVisitIdGetResponse(params: VisitService.ApiVisitDetailByVisitIdGetParams): Observable<HttpResponse<VisitViewModel>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'GET',
        this.rootUrl + `/api/Visit/Detail/${params.visitId}`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: VisitViewModel = null;
          _body = _resp.body as VisitViewModel;
          return _resp.clone({ body: _body }) as HttpResponse<VisitViewModel>;
        })
    );
  }

  /**
   * @param params The `VisitService.ApiVisitDetailByVisitIdGetParams` containing the following parameters:
   *
   * - `visitId`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Retrieved successfully
   */
  ApiVisitDetailByVisitIdGet(params: VisitService.ApiVisitDetailByVisitIdGetParams): Observable<VisitViewModel> {
    return this.ApiVisitDetailByVisitIdGetResponse(params).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `VisitService.ApiVisitGetPatientVisitsByPatientIdGetParams` containing the following parameters:
   *
   * - `patientId`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Listed successfully
   */
  ApiVisitGetPatientVisitsByPatientIdGetResponse(params: VisitService.ApiVisitGetPatientVisitsByPatientIdGetParams): Observable<HttpResponse<VisitViewModel[]>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'GET',
        this.rootUrl + `/api/Visit/GetPatientVisits/${params.patientId}`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: VisitViewModel[] = null;
          _body = _resp.body as VisitViewModel[];
          return _resp.clone({ body: _body }) as HttpResponse<VisitViewModel[]>;
        })
    );
  }

  /**
   * @param params The `VisitService.ApiVisitGetPatientVisitsByPatientIdGetParams` containing the following parameters:
   *
   * - `patientId`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Listed successfully
   */
  ApiVisitGetPatientVisitsByPatientIdGet(params: VisitService.ApiVisitGetPatientVisitsByPatientIdGetParams): Observable<VisitViewModel[]> {
    return this.ApiVisitGetPatientVisitsByPatientIdGetResponse(params).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `VisitService.ApiVisitPostParams` containing the following parameters:
   *
   * - `vModel`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Created successfully
   */
  ApiVisitPostResponse(params: VisitService.ApiVisitPostParams): Observable<HttpResponse<FeedBack>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = params.vModel;
    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'POST',
        this.rootUrl + `/api/Visit`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: FeedBack = null;
          _body = _resp.body as FeedBack;
          return _resp.clone({ body: _body }) as HttpResponse<FeedBack>;
        })
    );
  }

  /**
   * @param params The `VisitService.ApiVisitPostParams` containing the following parameters:
   *
   * - `vModel`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Created successfully
   */
  ApiVisitPost(params: VisitService.ApiVisitPostParams): Observable<FeedBack> {
    return this.ApiVisitPostResponse(params).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `VisitService.ApiVisitByIdPutParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `vModel`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Updated successfully
   */
  ApiVisitByIdPutResponse(params: VisitService.ApiVisitByIdPutParams): Observable<HttpResponse<FeedBack>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.vModel;
    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'PUT',
        this.rootUrl + `/api/Visit/${params.id}`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: FeedBack = null;
          _body = _resp.body as FeedBack;
          return _resp.clone({ body: _body }) as HttpResponse<FeedBack>;
        })
    );
  }

  /**
   * @param params The `VisitService.ApiVisitByIdPutParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `vModel`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Updated successfully
   */
  ApiVisitByIdPut(params: VisitService.ApiVisitByIdPutParams): Observable<FeedBack> {
    return this.ApiVisitByIdPutResponse(params).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `VisitService.ApiVisitByIdDeleteParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Deleted successfully
   */
  ApiVisitByIdDeleteResponse(params: VisitService.ApiVisitByIdDeleteParams): Observable<HttpResponse<FeedBack>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'DELETE',
        this.rootUrl + `/api/Visit/${params.id}`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: FeedBack = null;
          _body = _resp.body as FeedBack;
          return _resp.clone({ body: _body }) as HttpResponse<FeedBack>;
        })
    );
  }

  /**
   * @param params The `VisitService.ApiVisitByIdDeleteParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Deleted successfully
   */
  ApiVisitByIdDelete(params: VisitService.ApiVisitByIdDeleteParams): Observable<FeedBack> {
    return this.ApiVisitByIdDeleteResponse(params).pipe(
        map(_r => _r.body)
    );
  }
}

export module VisitService {

  /**
   * Parameters for ApiVisitGetOrgVisitsByOrgIdGet
   */
  export interface ApiVisitGetOrgVisitsByOrgIdGetParams {

    orgId: number;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }

  /**
   * Parameters for ApiVisitDetailByVisitIdGet
   */
  export interface ApiVisitDetailByVisitIdGetParams {

    visitId: number;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }

  /**
   * Parameters for ApiVisitGetPatientVisitsByPatientIdGet
   */
  export interface ApiVisitGetPatientVisitsByPatientIdGetParams {

    patientId: number;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }

  /**
   * Parameters for ApiVisitPost
   */
  export interface ApiVisitPostParams {

    vModel?: VisitRegistrationViewModel;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }

  /**
   * Parameters for ApiVisitByIdPut
   */
  export interface ApiVisitByIdPutParams {

    id: number;

    vModel?: VisitRegistrationViewModel;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }

  /**
   * Parameters for ApiVisitByIdDelete
   */
  export interface ApiVisitByIdDeleteParams {

    id: number;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }
}
