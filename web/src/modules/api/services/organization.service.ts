/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators/map';
import { filter } from 'rxjs/operators/filter';

import { OrganizationViewModel } from '../models/organization-view-model';
import { FeedBack } from '../models/feed-back';
import { OrganizationRegistrationViewModel } from '../models/organization-registration-view-model';
import { VisitViewModel } from '../models/visit-view-model';

@Injectable()
export class OrganizationService extends BaseService {
  constructor(config: ApiConfiguration,
              http: HttpClient) {
    super(config, http);
  }

  /**
   * @param correlationId Correlation Id for the request
   * @return Successfully listed
   */
  ApiOrganizationGetResponse(correlationId?: string): Observable<HttpResponse<OrganizationViewModel[]>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (correlationId != null) __headers = __headers.set('correlationId', correlationId.toString());
    let req = new HttpRequest<any>(
        'GET',
        this.rootUrl + `/api/Organization`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: OrganizationViewModel[] = null;
          _body = _resp.body as OrganizationViewModel[];
          return _resp.clone({ body: _body }) as HttpResponse<OrganizationViewModel[]>;
        })
    );
  }

  /**
   * @param correlationId Correlation Id for the request
   * @return Successfully listed
   */
  ApiOrganizationGet(correlationId?: string): Observable<OrganizationViewModel[]> {
    return this.ApiOrganizationGetResponse(correlationId).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `OrganizationService.ApiOrganizationPostParams` containing the following parameters:
   *
   * - `vModel`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully created
   */
  ApiOrganizationPostResponse(params: OrganizationService.ApiOrganizationPostParams): Observable<HttpResponse<FeedBack>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = params.vModel;
    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'POST',
        this.rootUrl + `/api/Organization`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: FeedBack = null;
          _body = _resp.body as FeedBack;
          return _resp.clone({ body: _body }) as HttpResponse<FeedBack>;
        })
    );
  }

  /**
   * @param params The `OrganizationService.ApiOrganizationPostParams` containing the following parameters:
   *
   * - `vModel`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully created
   */
  ApiOrganizationPost(params: OrganizationService.ApiOrganizationPostParams): Observable<FeedBack> {
    return this.ApiOrganizationPostResponse(params).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `OrganizationService.ApiOrganizationDetailByOrgIdGetParams` containing the following parameters:
   *
   * - `orgId`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully retrieved
   */
  ApiOrganizationDetailByOrgIdGetResponse(params: OrganizationService.ApiOrganizationDetailByOrgIdGetParams): Observable<HttpResponse<OrganizationViewModel>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'GET',
        this.rootUrl + `/api/Organization/Detail/${params.orgId}`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: OrganizationViewModel = null;
          _body = _resp.body as OrganizationViewModel;
          return _resp.clone({ body: _body }) as HttpResponse<OrganizationViewModel>;
        })
    );
  }

  /**
   * @param params The `OrganizationService.ApiOrganizationDetailByOrgIdGetParams` containing the following parameters:
   *
   * - `orgId`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully retrieved
   */
  ApiOrganizationDetailByOrgIdGet(params: OrganizationService.ApiOrganizationDetailByOrgIdGetParams): Observable<OrganizationViewModel> {
    return this.ApiOrganizationDetailByOrgIdGetResponse(params).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `OrganizationService.ApiOrganizationGetByNationalIdByNationalIdGetParams` containing the following parameters:
   *
   * - `nationalId`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully retrieved
   */
  ApiOrganizationGetByNationalIdByNationalIdGetResponse(params: OrganizationService.ApiOrganizationGetByNationalIdByNationalIdGetParams): Observable<HttpResponse<OrganizationViewModel>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'GET',
        this.rootUrl + `/api/Organization/GetByNationalId/${params.nationalId}`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: OrganizationViewModel = null;
          _body = _resp.body as OrganizationViewModel;
          return _resp.clone({ body: _body }) as HttpResponse<OrganizationViewModel>;
        })
    );
  }

  /**
   * @param params The `OrganizationService.ApiOrganizationGetByNationalIdByNationalIdGetParams` containing the following parameters:
   *
   * - `nationalId`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully retrieved
   */
  ApiOrganizationGetByNationalIdByNationalIdGet(params: OrganizationService.ApiOrganizationGetByNationalIdByNationalIdGetParams): Observable<OrganizationViewModel> {
    return this.ApiOrganizationGetByNationalIdByNationalIdGetResponse(params).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `OrganizationService.ApiOrganizationGetcurrentPatientsByOrgIdGetParams` containing the following parameters:
   *
   * - `orgId`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully retrieved
   */
  ApiOrganizationGetcurrentPatientsByOrgIdGetResponse(params: OrganizationService.ApiOrganizationGetcurrentPatientsByOrgIdGetParams): Observable<HttpResponse<VisitViewModel[]>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'GET',
        this.rootUrl + `/api/Organization/GetcurrentPatients/${params.orgId}`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: VisitViewModel[] = null;
          _body = _resp.body as VisitViewModel[];
          return _resp.clone({ body: _body }) as HttpResponse<VisitViewModel[]>;
        })
    );
  }

  /**
   * @param params The `OrganizationService.ApiOrganizationGetcurrentPatientsByOrgIdGetParams` containing the following parameters:
   *
   * - `orgId`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully retrieved
   */
  ApiOrganizationGetcurrentPatientsByOrgIdGet(params: OrganizationService.ApiOrganizationGetcurrentPatientsByOrgIdGetParams): Observable<VisitViewModel[]> {
    return this.ApiOrganizationGetcurrentPatientsByOrgIdGetResponse(params).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `OrganizationService.ApiOrganizationGetWaitingPatientsByOrgIdGetParams` containing the following parameters:
   *
   * - `orgId`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully retrieved
   */
  ApiOrganizationGetWaitingPatientsByOrgIdGetResponse(params: OrganizationService.ApiOrganizationGetWaitingPatientsByOrgIdGetParams): Observable<HttpResponse<VisitViewModel[]>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'GET',
        this.rootUrl + `/api/Organization/GetWaitingPatients/${params.orgId}`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: VisitViewModel[] = null;
          _body = _resp.body as VisitViewModel[];
          return _resp.clone({ body: _body }) as HttpResponse<VisitViewModel[]>;
        })
    );
  }

  /**
   * @param params The `OrganizationService.ApiOrganizationGetWaitingPatientsByOrgIdGetParams` containing the following parameters:
   *
   * - `orgId`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully retrieved
   */
  ApiOrganizationGetWaitingPatientsByOrgIdGet(params: OrganizationService.ApiOrganizationGetWaitingPatientsByOrgIdGetParams): Observable<VisitViewModel[]> {
    return this.ApiOrganizationGetWaitingPatientsByOrgIdGetResponse(params).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `OrganizationService.ApiOrganizationGetAdmittedPatientsByOrgIdGetParams` containing the following parameters:
   *
   * - `orgId`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully retrieved
   */
  ApiOrganizationGetAdmittedPatientsByOrgIdGetResponse(params: OrganizationService.ApiOrganizationGetAdmittedPatientsByOrgIdGetParams): Observable<HttpResponse<VisitViewModel[]>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'GET',
        this.rootUrl + `/api/Organization/GetAdmittedPatients/${params.orgId}`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: VisitViewModel[] = null;
          _body = _resp.body as VisitViewModel[];
          return _resp.clone({ body: _body }) as HttpResponse<VisitViewModel[]>;
        })
    );
  }

  /**
   * @param params The `OrganizationService.ApiOrganizationGetAdmittedPatientsByOrgIdGetParams` containing the following parameters:
   *
   * - `orgId`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully retrieved
   */
  ApiOrganizationGetAdmittedPatientsByOrgIdGet(params: OrganizationService.ApiOrganizationGetAdmittedPatientsByOrgIdGetParams): Observable<VisitViewModel[]> {
    return this.ApiOrganizationGetAdmittedPatientsByOrgIdGetResponse(params).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `OrganizationService.ApiOrganizationByIdPutParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `vModel`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully updated
   */
  ApiOrganizationByIdPutResponse(params: OrganizationService.ApiOrganizationByIdPutParams): Observable<HttpResponse<FeedBack>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.vModel;
    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'PUT',
        this.rootUrl + `/api/Organization/${params.id}`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: FeedBack = null;
          _body = _resp.body as FeedBack;
          return _resp.clone({ body: _body }) as HttpResponse<FeedBack>;
        })
    );
  }

  /**
   * @param params The `OrganizationService.ApiOrganizationByIdPutParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `vModel`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully updated
   */
  ApiOrganizationByIdPut(params: OrganizationService.ApiOrganizationByIdPutParams): Observable<FeedBack> {
    return this.ApiOrganizationByIdPutResponse(params).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `OrganizationService.ApiOrganizationByIdDeleteParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully deleted
   */
  ApiOrganizationByIdDeleteResponse(params: OrganizationService.ApiOrganizationByIdDeleteParams): Observable<HttpResponse<FeedBack>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'DELETE',
        this.rootUrl + `/api/Organization/${params.id}`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: FeedBack = null;
          _body = _resp.body as FeedBack;
          return _resp.clone({ body: _body }) as HttpResponse<FeedBack>;
        })
    );
  }

  /**
   * @param params The `OrganizationService.ApiOrganizationByIdDeleteParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully deleted
   */
  ApiOrganizationByIdDelete(params: OrganizationService.ApiOrganizationByIdDeleteParams): Observable<FeedBack> {
    return this.ApiOrganizationByIdDeleteResponse(params).pipe(
        map(_r => _r.body)
    );
  }
}

export module OrganizationService {

  /**
   * Parameters for ApiOrganizationPost
   */
  export interface ApiOrganizationPostParams {

    vModel?: OrganizationRegistrationViewModel;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }

  /**
   * Parameters for ApiOrganizationDetailByOrgIdGet
   */
  export interface ApiOrganizationDetailByOrgIdGetParams {

    orgId: number;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }

  /**
   * Parameters for ApiOrganizationGetByNationalIdByNationalIdGet
   */
  export interface ApiOrganizationGetByNationalIdByNationalIdGetParams {

    nationalId: string;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }

  /**
   * Parameters for ApiOrganizationGetcurrentPatientsByOrgIdGet
   */
  export interface ApiOrganizationGetcurrentPatientsByOrgIdGetParams {

    orgId: number;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }

  /**
   * Parameters for ApiOrganizationGetWaitingPatientsByOrgIdGet
   */
  export interface ApiOrganizationGetWaitingPatientsByOrgIdGetParams {

    orgId: number;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }

  /**
   * Parameters for ApiOrganizationGetAdmittedPatientsByOrgIdGet
   */
  export interface ApiOrganizationGetAdmittedPatientsByOrgIdGetParams {

    orgId: number;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }

  /**
   * Parameters for ApiOrganizationByIdPut
   */
  export interface ApiOrganizationByIdPutParams {

    id: number;

    vModel?: OrganizationViewModel;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }

  /**
   * Parameters for ApiOrganizationByIdDelete
   */
  export interface ApiOrganizationByIdDeleteParams {

    id: number;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }
}
