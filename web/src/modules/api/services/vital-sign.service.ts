/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators/map';
import { filter } from 'rxjs/operators/filter';

import { VitalSignViewModel } from '../models/vital-sign-view-model';
import { FeedBack } from '../models/feed-back';
import { VitalSignCreateViewModel } from '../models/vital-sign-create-view-model';

@Injectable()
export class VitalSignService extends BaseService {
  constructor(config: ApiConfiguration,
              http: HttpClient) {
    super(config, http);
  }

  /**
   * @param params The `VitalSignService.ApiVitalSignGetParams` containing the following parameters:
   *
   * - `patientId`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Listed successfully
   */
  ApiVitalSignGetResponse(params: VitalSignService.ApiVitalSignGetParams): Observable<HttpResponse<VitalSignViewModel[]>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (params.patientId != null) __params = __params.set('patientId', params.patientId.toString());
    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'GET',
        this.rootUrl + `/api/VitalSign`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: VitalSignViewModel[] = null;
          _body = _resp.body as VitalSignViewModel[];
          return _resp.clone({ body: _body }) as HttpResponse<VitalSignViewModel[]>;
        })
    );
  }

  /**
   * @param params The `VitalSignService.ApiVitalSignGetParams` containing the following parameters:
   *
   * - `patientId`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Listed successfully
   */
  ApiVitalSignGet(params: VitalSignService.ApiVitalSignGetParams): Observable<VitalSignViewModel[]> {
    return this.ApiVitalSignGetResponse(params).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `VitalSignService.ApiVitalSignPostParams` containing the following parameters:
   *
   * - `vModel`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Saved successfully
   */
  ApiVitalSignPostResponse(params: VitalSignService.ApiVitalSignPostParams): Observable<HttpResponse<FeedBack>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = params.vModel;
    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'POST',
        this.rootUrl + `/api/VitalSign`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: FeedBack = null;
          _body = _resp.body as FeedBack;
          return _resp.clone({ body: _body }) as HttpResponse<FeedBack>;
        })
    );
  }

  /**
   * @param params The `VitalSignService.ApiVitalSignPostParams` containing the following parameters:
   *
   * - `vModel`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Saved successfully
   */
  ApiVitalSignPost(params: VitalSignService.ApiVitalSignPostParams): Observable<FeedBack> {
    return this.ApiVitalSignPostResponse(params).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `VitalSignService.ApiVitalSignGetByEncounterByEncounterIdGetParams` containing the following parameters:
   *
   * - `encounterId`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Listed successfully
   */
  ApiVitalSignGetByEncounterByEncounterIdGetResponse(params: VitalSignService.ApiVitalSignGetByEncounterByEncounterIdGetParams): Observable<HttpResponse<VitalSignViewModel[]>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'GET',
        this.rootUrl + `/api/VitalSign/GetByEncounter/${params.encounterId}`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: VitalSignViewModel[] = null;
          _body = _resp.body as VitalSignViewModel[];
          return _resp.clone({ body: _body }) as HttpResponse<VitalSignViewModel[]>;
        })
    );
  }

  /**
   * @param params The `VitalSignService.ApiVitalSignGetByEncounterByEncounterIdGetParams` containing the following parameters:
   *
   * - `encounterId`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Listed successfully
   */
  ApiVitalSignGetByEncounterByEncounterIdGet(params: VitalSignService.ApiVitalSignGetByEncounterByEncounterIdGetParams): Observable<VitalSignViewModel[]> {
    return this.ApiVitalSignGetByEncounterByEncounterIdGetResponse(params).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `VitalSignService.ApiVitalSignDetailByIdGetParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Retrieved successfully
   */
  ApiVitalSignDetailByIdGetResponse(params: VitalSignService.ApiVitalSignDetailByIdGetParams): Observable<HttpResponse<VitalSignViewModel>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'GET',
        this.rootUrl + `/api/VitalSign/Detail/${params.id}`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: VitalSignViewModel = null;
          _body = _resp.body as VitalSignViewModel;
          return _resp.clone({ body: _body }) as HttpResponse<VitalSignViewModel>;
        })
    );
  }

  /**
   * @param params The `VitalSignService.ApiVitalSignDetailByIdGetParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Retrieved successfully
   */
  ApiVitalSignDetailByIdGet(params: VitalSignService.ApiVitalSignDetailByIdGetParams): Observable<VitalSignViewModel> {
    return this.ApiVitalSignDetailByIdGetResponse(params).pipe(
        map(_r => _r.body)
    );
  }
}

export module VitalSignService {

  /**
   * Parameters for ApiVitalSignGet
   */
  export interface ApiVitalSignGetParams {

    patientId: number;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }

  /**
   * Parameters for ApiVitalSignPost
   */
  export interface ApiVitalSignPostParams {

    vModel?: VitalSignCreateViewModel;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }

  /**
   * Parameters for ApiVitalSignGetByEncounterByEncounterIdGet
   */
  export interface ApiVitalSignGetByEncounterByEncounterIdGetParams {

    encounterId: number;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }

  /**
   * Parameters for ApiVitalSignDetailByIdGet
   */
  export interface ApiVitalSignDetailByIdGetParams {

    id: number;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }
}
