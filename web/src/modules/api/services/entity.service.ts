/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators/map';
import { filter } from 'rxjs/operators/filter';

import { EntityViewModel } from '../models/entity-view-model';

@Injectable()
export class EntityService extends BaseService {
  constructor(config: ApiConfiguration,
              http: HttpClient) {
    super(config, http);
  }

  /**
   * @param params The `EntityService.ApiEntityGetEntityByIdByIdGetParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully retrieved
   */
  ApiEntityGetEntityByIdByIdGetResponse(params: EntityService.ApiEntityGetEntityByIdByIdGetParams): Observable<HttpResponse<EntityViewModel>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'GET',
        this.rootUrl + `/api/Entity/GetEntityById/${params.id}`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: EntityViewModel = null;
          _body = _resp.body as EntityViewModel;
          return _resp.clone({ body: _body }) as HttpResponse<EntityViewModel>;
        })
    );
  }

  /**
   * @param params The `EntityService.ApiEntityGetEntityByIdByIdGetParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully retrieved
   */
  ApiEntityGetEntityByIdByIdGet(params: EntityService.ApiEntityGetEntityByIdByIdGetParams): Observable<EntityViewModel> {
    return this.ApiEntityGetEntityByIdByIdGetResponse(params).pipe(
        map(_r => _r.body)
    );
  }
}

export module EntityService {

  /**
   * Parameters for ApiEntityGetEntityByIdByIdGet
   */
  export interface ApiEntityGetEntityByIdByIdGetParams {

    id: number;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }
}
