/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators/map';
import { filter } from 'rxjs/operators/filter';

import { RoleViewModel } from '../models/role-view-model';
import { FeedBack } from '../models/feed-back';

@Injectable()
export class RoleService extends BaseService {
  constructor(config: ApiConfiguration,
              http: HttpClient) {
    super(config, http);
  }

  /**
   * @param correlationId Correlation Id for the request
   * @return Successfully listed
   */
  ApiRoleGetResponse(correlationId?: string): Observable<HttpResponse<RoleViewModel[]>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (correlationId != null) __headers = __headers.set('correlationId', correlationId.toString());
    let req = new HttpRequest<any>(
        'GET',
        this.rootUrl + `/api/Role`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: RoleViewModel[] = null;
          _body = _resp.body as RoleViewModel[];
          return _resp.clone({ body: _body }) as HttpResponse<RoleViewModel[]>;
        })
    );
  }

  /**
   * @param correlationId Correlation Id for the request
   * @return Successfully listed
   */
  ApiRoleGet(correlationId?: string): Observable<RoleViewModel[]> {
    return this.ApiRoleGetResponse(correlationId).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `RoleService.ApiRolePostParams` containing the following parameters:
   *
   * - `model`: The RoleViewModel object to be created.
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully created
   */
  ApiRolePostResponse(params: RoleService.ApiRolePostParams): Observable<HttpResponse<FeedBack>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = params.model;
    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'POST',
        this.rootUrl + `/api/Role`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: FeedBack = null;
          _body = _resp.body as FeedBack;
          return _resp.clone({ body: _body }) as HttpResponse<FeedBack>;
        })
    );
  }

  /**
   * @param params The `RoleService.ApiRolePostParams` containing the following parameters:
   *
   * - `model`: The RoleViewModel object to be created.
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully created
   */
  ApiRolePost(params: RoleService.ApiRolePostParams): Observable<FeedBack> {
    return this.ApiRolePostResponse(params).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `RoleService.ApiRoleByRoleIdPutParams` containing the following parameters:
   *
   * - `roleId`: The roleId of Object to be updated.
   *
   * - `model`: The updated RoleViewModel object.
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully updated
   */
  ApiRoleByRoleIdPutResponse(params: RoleService.ApiRoleByRoleIdPutParams): Observable<HttpResponse<FeedBack>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.model;
    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'PUT',
        this.rootUrl + `/api/Role/${params.roleId}`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: FeedBack = null;
          _body = _resp.body as FeedBack;
          return _resp.clone({ body: _body }) as HttpResponse<FeedBack>;
        })
    );
  }

  /**
   * @param params The `RoleService.ApiRoleByRoleIdPutParams` containing the following parameters:
   *
   * - `roleId`: The roleId of Object to be updated.
   *
   * - `model`: The updated RoleViewModel object.
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully updated
   */
  ApiRoleByRoleIdPut(params: RoleService.ApiRoleByRoleIdPutParams): Observable<FeedBack> {
    return this.ApiRoleByRoleIdPutResponse(params).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `RoleService.ApiRoleByRoleIdDeleteParams` containing the following parameters:
   *
   * - `roleId`: The roleId of Object to be deleted.
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully deleted
   */
  ApiRoleByRoleIdDeleteResponse(params: RoleService.ApiRoleByRoleIdDeleteParams): Observable<HttpResponse<FeedBack>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'DELETE',
        this.rootUrl + `/api/Role/${params.roleId}`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: FeedBack = null;
          _body = _resp.body as FeedBack;
          return _resp.clone({ body: _body }) as HttpResponse<FeedBack>;
        })
    );
  }

  /**
   * @param params The `RoleService.ApiRoleByRoleIdDeleteParams` containing the following parameters:
   *
   * - `roleId`: The roleId of Object to be deleted.
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully deleted
   */
  ApiRoleByRoleIdDelete(params: RoleService.ApiRoleByRoleIdDeleteParams): Observable<FeedBack> {
    return this.ApiRoleByRoleIdDeleteResponse(params).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `RoleService.ApiRoleGetOrgRolesByOrgIdGetParams` containing the following parameters:
   *
   * - `orgId`: The orgId of parent Organization of objects to be listed.
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully deleted
   */
  ApiRoleGetOrgRolesByOrgIdGetResponse(params: RoleService.ApiRoleGetOrgRolesByOrgIdGetParams): Observable<HttpResponse<RoleViewModel[]>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'GET',
        this.rootUrl + `/api/Role/GetOrgRoles/${params.orgId}`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: RoleViewModel[] = null;
          _body = _resp.body as RoleViewModel[];
          return _resp.clone({ body: _body }) as HttpResponse<RoleViewModel[]>;
        })
    );
  }

  /**
   * @param params The `RoleService.ApiRoleGetOrgRolesByOrgIdGetParams` containing the following parameters:
   *
   * - `orgId`: The orgId of parent Organization of objects to be listed.
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully deleted
   */
  ApiRoleGetOrgRolesByOrgIdGet(params: RoleService.ApiRoleGetOrgRolesByOrgIdGetParams): Observable<RoleViewModel[]> {
    return this.ApiRoleGetOrgRolesByOrgIdGetResponse(params).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `RoleService.ApiRoleDetailsByRoleIdGetParams` containing the following parameters:
   *
   * - `roleId`: The roleId of Object to be retrieved.
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully retrieved
   */
  ApiRoleDetailsByRoleIdGetResponse(params: RoleService.ApiRoleDetailsByRoleIdGetParams): Observable<HttpResponse<RoleViewModel>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'GET',
        this.rootUrl + `/api/Role/Details/${params.roleId}`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: RoleViewModel = null;
          _body = _resp.body as RoleViewModel;
          return _resp.clone({ body: _body }) as HttpResponse<RoleViewModel>;
        })
    );
  }

  /**
   * @param params The `RoleService.ApiRoleDetailsByRoleIdGetParams` containing the following parameters:
   *
   * - `roleId`: The roleId of Object to be retrieved.
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully retrieved
   */
  ApiRoleDetailsByRoleIdGet(params: RoleService.ApiRoleDetailsByRoleIdGetParams): Observable<RoleViewModel> {
    return this.ApiRoleDetailsByRoleIdGetResponse(params).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `RoleService.ApiRoleCountryRolesByCountryIdGetParams` containing the following parameters:
   *
   * - `countryId`: The countryId of parent Country of objects to be listed.
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully listed
   */
  ApiRoleCountryRolesByCountryIdGetResponse(params: RoleService.ApiRoleCountryRolesByCountryIdGetParams): Observable<HttpResponse<RoleViewModel[]>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'GET',
        this.rootUrl + `/api/Role/CountryRoles/${params.countryId}`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: RoleViewModel[] = null;
          _body = _resp.body as RoleViewModel[];
          return _resp.clone({ body: _body }) as HttpResponse<RoleViewModel[]>;
        })
    );
  }

  /**
   * @param params The `RoleService.ApiRoleCountryRolesByCountryIdGetParams` containing the following parameters:
   *
   * - `countryId`: The countryId of parent Country of objects to be listed.
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully listed
   */
  ApiRoleCountryRolesByCountryIdGet(params: RoleService.ApiRoleCountryRolesByCountryIdGetParams): Observable<RoleViewModel[]> {
    return this.ApiRoleCountryRolesByCountryIdGetResponse(params).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param correlationId Correlation Id for the request
   * @return Successfully listed
   */
  ApiRoleGlobalRolesGetResponse(correlationId?: string): Observable<HttpResponse<RoleViewModel[]>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (correlationId != null) __headers = __headers.set('correlationId', correlationId.toString());
    let req = new HttpRequest<any>(
        'GET',
        this.rootUrl + `/api/Role/GlobalRoles`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: RoleViewModel[] = null;
          _body = _resp.body as RoleViewModel[];
          return _resp.clone({ body: _body }) as HttpResponse<RoleViewModel[]>;
        })
    );
  }

  /**
   * @param correlationId Correlation Id for the request
   * @return Successfully listed
   */
  ApiRoleGlobalRolesGet(correlationId?: string): Observable<RoleViewModel[]> {
    return this.ApiRoleGlobalRolesGetResponse(correlationId).pipe(
        map(_r => _r.body)
    );
  }
}

export module RoleService {

  /**
   * Parameters for ApiRolePost
   */
  export interface ApiRolePostParams {

    /**
     * The RoleViewModel object to be created.
     */
    model?: RoleViewModel;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }

  /**
   * Parameters for ApiRoleByRoleIdPut
   */
  export interface ApiRoleByRoleIdPutParams {

    /**
     * The roleId of Object to be updated.
     */
    roleId: string;

    /**
     * The updated RoleViewModel object.
     */
    model?: RoleViewModel;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }

  /**
   * Parameters for ApiRoleByRoleIdDelete
   */
  export interface ApiRoleByRoleIdDeleteParams {

    /**
     * The roleId of Object to be deleted.
     */
    roleId: string;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }

  /**
   * Parameters for ApiRoleGetOrgRolesByOrgIdGet
   */
  export interface ApiRoleGetOrgRolesByOrgIdGetParams {

    /**
     * The orgId of parent Organization of objects to be listed.
     */
    orgId: number;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }

  /**
   * Parameters for ApiRoleDetailsByRoleIdGet
   */
  export interface ApiRoleDetailsByRoleIdGetParams {

    /**
     * The roleId of Object to be retrieved.
     */
    roleId: string;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }

  /**
   * Parameters for ApiRoleCountryRolesByCountryIdGet
   */
  export interface ApiRoleCountryRolesByCountryIdGetParams {

    /**
     * The countryId of parent Country of objects to be listed.
     */
    countryId: number;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }
}
