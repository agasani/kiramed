/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators/map';
import { filter } from 'rxjs/operators/filter';

import { CountryViewModel } from '../models/country-view-model';
import { FeedBack } from '../models/feed-back';
import { CountryRegistrationViewModel } from '../models/country-registration-view-model';
import { OrganizationViewModel } from '../models/organization-view-model';

@Injectable()
export class CountryService extends BaseService {
  constructor(config: ApiConfiguration,
              http: HttpClient) {
    super(config, http);
  }

  /**
   * @param correlationId Correlation Id for the request
   * @return Successfully listed
   */
  ApiCountryGetResponse(correlationId?: string): Observable<HttpResponse<CountryViewModel[]>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (correlationId != null) __headers = __headers.set('correlationId', correlationId.toString());
    let req = new HttpRequest<any>(
        'GET',
        this.rootUrl + `/api/Country`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: CountryViewModel[] = null;
          _body = _resp.body as CountryViewModel[];
          return _resp.clone({ body: _body }) as HttpResponse<CountryViewModel[]>;
        })
    );
  }

  /**
   * @param correlationId Correlation Id for the request
   * @return Successfully listed
   */
  ApiCountryGet(correlationId?: string): Observable<CountryViewModel[]> {
    return this.ApiCountryGetResponse(correlationId).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `CountryService.ApiCountryPostParams` containing the following parameters:
   *
   * - `vModel`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully created
   */
  ApiCountryPostResponse(params: CountryService.ApiCountryPostParams): Observable<HttpResponse<FeedBack>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = params.vModel;
    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'POST',
        this.rootUrl + `/api/Country`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: FeedBack = null;
          _body = _resp.body as FeedBack;
          return _resp.clone({ body: _body }) as HttpResponse<FeedBack>;
        })
    );
  }

  /**
   * @param params The `CountryService.ApiCountryPostParams` containing the following parameters:
   *
   * - `vModel`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully created
   */
  ApiCountryPost(params: CountryService.ApiCountryPostParams): Observable<FeedBack> {
    return this.ApiCountryPostResponse(params).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `CountryService.ApiCountryGetByIdByIdGetParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully retrieved
   */
  ApiCountryGetByIdByIdGetResponse(params: CountryService.ApiCountryGetByIdByIdGetParams): Observable<HttpResponse<CountryViewModel>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'GET',
        this.rootUrl + `/api/Country/GetById/${params.id}`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: CountryViewModel = null;
          _body = _resp.body as CountryViewModel;
          return _resp.clone({ body: _body }) as HttpResponse<CountryViewModel>;
        })
    );
  }

  /**
   * @param params The `CountryService.ApiCountryGetByIdByIdGetParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully retrieved
   */
  ApiCountryGetByIdByIdGet(params: CountryService.ApiCountryGetByIdByIdGetParams): Observable<CountryViewModel> {
    return this.ApiCountryGetByIdByIdGetResponse(params).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `CountryService.ApiCountryGetByCodeByCodeGetParams` containing the following parameters:
   *
   * - `code`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully retrieved
   */
  ApiCountryGetByCodeByCodeGetResponse(params: CountryService.ApiCountryGetByCodeByCodeGetParams): Observable<HttpResponse<CountryViewModel>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'GET',
        this.rootUrl + `/api/Country/GetByCode/${params.code}`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: CountryViewModel = null;
          _body = _resp.body as CountryViewModel;
          return _resp.clone({ body: _body }) as HttpResponse<CountryViewModel>;
        })
    );
  }

  /**
   * @param params The `CountryService.ApiCountryGetByCodeByCodeGetParams` containing the following parameters:
   *
   * - `code`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully retrieved
   */
  ApiCountryGetByCodeByCodeGet(params: CountryService.ApiCountryGetByCodeByCodeGetParams): Observable<CountryViewModel> {
    return this.ApiCountryGetByCodeByCodeGetResponse(params).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `CountryService.ApiCountryByIdPutParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `vModel`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully updated
   */
  ApiCountryByIdPutResponse(params: CountryService.ApiCountryByIdPutParams): Observable<HttpResponse<FeedBack>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.vModel;
    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'PUT',
        this.rootUrl + `/api/Country/${params.id}`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: FeedBack = null;
          _body = _resp.body as FeedBack;
          return _resp.clone({ body: _body }) as HttpResponse<FeedBack>;
        })
    );
  }

  /**
   * @param params The `CountryService.ApiCountryByIdPutParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `vModel`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully updated
   */
  ApiCountryByIdPut(params: CountryService.ApiCountryByIdPutParams): Observable<FeedBack> {
    return this.ApiCountryByIdPutResponse(params).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `CountryService.ApiCountryByIdDeleteParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully deleted
   */
  ApiCountryByIdDeleteResponse(params: CountryService.ApiCountryByIdDeleteParams): Observable<HttpResponse<FeedBack>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'DELETE',
        this.rootUrl + `/api/Country/${params.id}`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: FeedBack = null;
          _body = _resp.body as FeedBack;
          return _resp.clone({ body: _body }) as HttpResponse<FeedBack>;
        })
    );
  }

  /**
   * @param params The `CountryService.ApiCountryByIdDeleteParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully deleted
   */
  ApiCountryByIdDelete(params: CountryService.ApiCountryByIdDeleteParams): Observable<FeedBack> {
    return this.ApiCountryByIdDeleteResponse(params).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `CountryService.ApiCountryGetCountryOrganizationsByCountryIdGetParams` containing the following parameters:
   *
   * - `countryId`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully listed
   */
  ApiCountryGetCountryOrganizationsByCountryIdGetResponse(params: CountryService.ApiCountryGetCountryOrganizationsByCountryIdGetParams): Observable<HttpResponse<OrganizationViewModel[]>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'GET',
        this.rootUrl + `/api/Country/GetCountryOrganizations/${params.countryId}`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: OrganizationViewModel[] = null;
          _body = _resp.body as OrganizationViewModel[];
          return _resp.clone({ body: _body }) as HttpResponse<OrganizationViewModel[]>;
        })
    );
  }

  /**
   * @param params The `CountryService.ApiCountryGetCountryOrganizationsByCountryIdGetParams` containing the following parameters:
   *
   * - `countryId`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully listed
   */
  ApiCountryGetCountryOrganizationsByCountryIdGet(params: CountryService.ApiCountryGetCountryOrganizationsByCountryIdGetParams): Observable<OrganizationViewModel[]> {
    return this.ApiCountryGetCountryOrganizationsByCountryIdGetResponse(params).pipe(
        map(_r => _r.body)
    );
  }
}

export module CountryService {

  /**
   * Parameters for ApiCountryPost
   */
  export interface ApiCountryPostParams {

    vModel?: CountryRegistrationViewModel;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }

  /**
   * Parameters for ApiCountryGetByIdByIdGet
   */
  export interface ApiCountryGetByIdByIdGetParams {

    id: number;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }

  /**
   * Parameters for ApiCountryGetByCodeByCodeGet
   */
  export interface ApiCountryGetByCodeByCodeGetParams {

    code: string;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }

  /**
   * Parameters for ApiCountryByIdPut
   */
  export interface ApiCountryByIdPutParams {

    id: number;

    vModel?: CountryViewModel;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }

  /**
   * Parameters for ApiCountryByIdDelete
   */
  export interface ApiCountryByIdDeleteParams {

    id: number;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }

  /**
   * Parameters for ApiCountryGetCountryOrganizationsByCountryIdGet
   */
  export interface ApiCountryGetCountryOrganizationsByCountryIdGetParams {

    countryId: number;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }
}
