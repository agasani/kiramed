/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators/map';
import { filter } from 'rxjs/operators/filter';

import { AppointmentViewModel } from '../models/appointment-view-model';
import { FeedBack } from '../models/feed-back';
import { AppointmentCreationViewModel } from '../models/appointment-creation-view-model';

@Injectable()
export class AppointmentService extends BaseService {
  constructor(config: ApiConfiguration,
              http: HttpClient) {
    super(config, http);
  }

  /**
   * @param correlationId Correlation Id for the request
   * @return Listed successfully
   */
  ApiAppointmentGetResponse(correlationId?: string): Observable<HttpResponse<AppointmentViewModel[]>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (correlationId != null) __headers = __headers.set('correlationId', correlationId.toString());
    let req = new HttpRequest<any>(
        'GET',
        this.rootUrl + `/api/Appointment`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: AppointmentViewModel[] = null;
          _body = _resp.body as AppointmentViewModel[];
          return _resp.clone({ body: _body }) as HttpResponse<AppointmentViewModel[]>;
        })
    );
  }

  /**
   * @param correlationId Correlation Id for the request
   * @return Listed successfully
   */
  ApiAppointmentGet(correlationId?: string): Observable<AppointmentViewModel[]> {
    return this.ApiAppointmentGetResponse(correlationId).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `AppointmentService.ApiAppointmentPostParams` containing the following parameters:
   *
   * - `vModel`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully retrieved
   */
  ApiAppointmentPostResponse(params: AppointmentService.ApiAppointmentPostParams): Observable<HttpResponse<FeedBack>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = params.vModel;
    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'POST',
        this.rootUrl + `/api/Appointment`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: FeedBack = null;
          _body = _resp.body as FeedBack;
          return _resp.clone({ body: _body }) as HttpResponse<FeedBack>;
        })
    );
  }

  /**
   * @param params The `AppointmentService.ApiAppointmentPostParams` containing the following parameters:
   *
   * - `vModel`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully retrieved
   */
  ApiAppointmentPost(params: AppointmentService.ApiAppointmentPostParams): Observable<FeedBack> {
    return this.ApiAppointmentPostResponse(params).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `AppointmentService.ApiAppointmentDetailByIdGetParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully retrieved
   */
  ApiAppointmentDetailByIdGetResponse(params: AppointmentService.ApiAppointmentDetailByIdGetParams): Observable<HttpResponse<AppointmentViewModel>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'GET',
        this.rootUrl + `/api/Appointment/Detail/${params.id}`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: AppointmentViewModel = null;
          _body = _resp.body as AppointmentViewModel;
          return _resp.clone({ body: _body }) as HttpResponse<AppointmentViewModel>;
        })
    );
  }

  /**
   * @param params The `AppointmentService.ApiAppointmentDetailByIdGetParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully retrieved
   */
  ApiAppointmentDetailByIdGet(params: AppointmentService.ApiAppointmentDetailByIdGetParams): Observable<AppointmentViewModel> {
    return this.ApiAppointmentDetailByIdGetResponse(params).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `AppointmentService.ApiAppointmentGetByNationalIdByNationalIdGetParams` containing the following parameters:
   *
   * - `nationalId`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully retrieved
   */
  ApiAppointmentGetByNationalIdByNationalIdGetResponse(params: AppointmentService.ApiAppointmentGetByNationalIdByNationalIdGetParams): Observable<HttpResponse<AppointmentViewModel[]>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'GET',
        this.rootUrl + `/api/Appointment/GetByNationalId/${params.nationalId}`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: AppointmentViewModel[] = null;
          _body = _resp.body as AppointmentViewModel[];
          return _resp.clone({ body: _body }) as HttpResponse<AppointmentViewModel[]>;
        })
    );
  }

  /**
   * @param params The `AppointmentService.ApiAppointmentGetByNationalIdByNationalIdGetParams` containing the following parameters:
   *
   * - `nationalId`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully retrieved
   */
  ApiAppointmentGetByNationalIdByNationalIdGet(params: AppointmentService.ApiAppointmentGetByNationalIdByNationalIdGetParams): Observable<AppointmentViewModel[]> {
    return this.ApiAppointmentGetByNationalIdByNationalIdGetResponse(params).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `AppointmentService.ApiAppointmentByIdPutParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `vModel`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully retrieved
   */
  ApiAppointmentByIdPutResponse(params: AppointmentService.ApiAppointmentByIdPutParams): Observable<HttpResponse<FeedBack>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.vModel;
    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'PUT',
        this.rootUrl + `/api/Appointment/${params.id}`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: FeedBack = null;
          _body = _resp.body as FeedBack;
          return _resp.clone({ body: _body }) as HttpResponse<FeedBack>;
        })
    );
  }

  /**
   * @param params The `AppointmentService.ApiAppointmentByIdPutParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `vModel`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully retrieved
   */
  ApiAppointmentByIdPut(params: AppointmentService.ApiAppointmentByIdPutParams): Observable<FeedBack> {
    return this.ApiAppointmentByIdPutResponse(params).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `AppointmentService.ApiAppointmentByIdDeleteParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully retrieved
   */
  ApiAppointmentByIdDeleteResponse(params: AppointmentService.ApiAppointmentByIdDeleteParams): Observable<HttpResponse<FeedBack>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'DELETE',
        this.rootUrl + `/api/Appointment/${params.id}`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: FeedBack = null;
          _body = _resp.body as FeedBack;
          return _resp.clone({ body: _body }) as HttpResponse<FeedBack>;
        })
    );
  }

  /**
   * @param params The `AppointmentService.ApiAppointmentByIdDeleteParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully retrieved
   */
  ApiAppointmentByIdDelete(params: AppointmentService.ApiAppointmentByIdDeleteParams): Observable<FeedBack> {
    return this.ApiAppointmentByIdDeleteResponse(params).pipe(
        map(_r => _r.body)
    );
  }
}

export module AppointmentService {

  /**
   * Parameters for ApiAppointmentPost
   */
  export interface ApiAppointmentPostParams {

    vModel?: AppointmentCreationViewModel;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }

  /**
   * Parameters for ApiAppointmentDetailByIdGet
   */
  export interface ApiAppointmentDetailByIdGetParams {

    id: number;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }

  /**
   * Parameters for ApiAppointmentGetByNationalIdByNationalIdGet
   */
  export interface ApiAppointmentGetByNationalIdByNationalIdGetParams {

    nationalId: string;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }

  /**
   * Parameters for ApiAppointmentByIdPut
   */
  export interface ApiAppointmentByIdPutParams {

    id: number;

    vModel?: AppointmentViewModel;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }

  /**
   * Parameters for ApiAppointmentByIdDelete
   */
  export interface ApiAppointmentByIdDeleteParams {

    id: number;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }
}
