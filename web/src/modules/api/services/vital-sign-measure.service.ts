/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators/map';
import { filter } from 'rxjs/operators/filter';

import { VitalSignMeasureViewModel } from '../models/vital-sign-measure-view-model';
import { FeedBack } from '../models/feed-back';
import { VitalSignMeasureCreateViewModel } from '../models/vital-sign-measure-create-view-model';

@Injectable()
export class VitalSignMeasureService extends BaseService {
  constructor(config: ApiConfiguration,
              http: HttpClient) {
    super(config, http);
  }

  /**
   * @param correlationId Correlation Id for the request
   * @return Listed successfully
   */
  ApiVitalSignMeasureGetResponse(correlationId?: string): Observable<HttpResponse<VitalSignMeasureViewModel[]>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (correlationId != null) __headers = __headers.set('correlationId', correlationId.toString());
    let req = new HttpRequest<any>(
        'GET',
        this.rootUrl + `/api/VitalSignMeasure`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: VitalSignMeasureViewModel[] = null;
          _body = _resp.body as VitalSignMeasureViewModel[];
          return _resp.clone({ body: _body }) as HttpResponse<VitalSignMeasureViewModel[]>;
        })
    );
  }

  /**
   * @param correlationId Correlation Id for the request
   * @return Listed successfully
   */
  ApiVitalSignMeasureGet(correlationId?: string): Observable<VitalSignMeasureViewModel[]> {
    return this.ApiVitalSignMeasureGetResponse(correlationId).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `VitalSignMeasureService.ApiVitalSignMeasurePostParams` containing the following parameters:
   *
   * - `vModel`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Listed successfully
   */
  ApiVitalSignMeasurePostResponse(params: VitalSignMeasureService.ApiVitalSignMeasurePostParams): Observable<HttpResponse<FeedBack>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = params.vModel;
    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'POST',
        this.rootUrl + `/api/VitalSignMeasure`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: FeedBack = null;
          _body = _resp.body as FeedBack;
          return _resp.clone({ body: _body }) as HttpResponse<FeedBack>;
        })
    );
  }

  /**
   * @param params The `VitalSignMeasureService.ApiVitalSignMeasurePostParams` containing the following parameters:
   *
   * - `vModel`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Listed successfully
   */
  ApiVitalSignMeasurePost(params: VitalSignMeasureService.ApiVitalSignMeasurePostParams): Observable<FeedBack> {
    return this.ApiVitalSignMeasurePostResponse(params).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `VitalSignMeasureService.ApiVitalSignMeasureGetByTypeByTypeIdGetParams` containing the following parameters:
   *
   * - `typeId`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Listed successfully
   */
  ApiVitalSignMeasureGetByTypeByTypeIdGetResponse(params: VitalSignMeasureService.ApiVitalSignMeasureGetByTypeByTypeIdGetParams): Observable<HttpResponse<VitalSignMeasureViewModel[]>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'GET',
        this.rootUrl + `/api/VitalSignMeasure/GetByType/${params.typeId}`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: VitalSignMeasureViewModel[] = null;
          _body = _resp.body as VitalSignMeasureViewModel[];
          return _resp.clone({ body: _body }) as HttpResponse<VitalSignMeasureViewModel[]>;
        })
    );
  }

  /**
   * @param params The `VitalSignMeasureService.ApiVitalSignMeasureGetByTypeByTypeIdGetParams` containing the following parameters:
   *
   * - `typeId`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Listed successfully
   */
  ApiVitalSignMeasureGetByTypeByTypeIdGet(params: VitalSignMeasureService.ApiVitalSignMeasureGetByTypeByTypeIdGetParams): Observable<VitalSignMeasureViewModel[]> {
    return this.ApiVitalSignMeasureGetByTypeByTypeIdGetResponse(params).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `VitalSignMeasureService.ApiVitalSignMeasureDetailByIdGetParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Listed successfully
   */
  ApiVitalSignMeasureDetailByIdGetResponse(params: VitalSignMeasureService.ApiVitalSignMeasureDetailByIdGetParams): Observable<HttpResponse<VitalSignMeasureViewModel>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'GET',
        this.rootUrl + `/api/VitalSignMeasure/Detail/${params.id}`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: VitalSignMeasureViewModel = null;
          _body = _resp.body as VitalSignMeasureViewModel;
          return _resp.clone({ body: _body }) as HttpResponse<VitalSignMeasureViewModel>;
        })
    );
  }

  /**
   * @param params The `VitalSignMeasureService.ApiVitalSignMeasureDetailByIdGetParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Listed successfully
   */
  ApiVitalSignMeasureDetailByIdGet(params: VitalSignMeasureService.ApiVitalSignMeasureDetailByIdGetParams): Observable<VitalSignMeasureViewModel> {
    return this.ApiVitalSignMeasureDetailByIdGetResponse(params).pipe(
        map(_r => _r.body)
    );
  }
}

export module VitalSignMeasureService {

  /**
   * Parameters for ApiVitalSignMeasurePost
   */
  export interface ApiVitalSignMeasurePostParams {

    vModel?: VitalSignMeasureCreateViewModel;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }

  /**
   * Parameters for ApiVitalSignMeasureGetByTypeByTypeIdGet
   */
  export interface ApiVitalSignMeasureGetByTypeByTypeIdGetParams {

    typeId: number;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }

  /**
   * Parameters for ApiVitalSignMeasureDetailByIdGet
   */
  export interface ApiVitalSignMeasureDetailByIdGetParams {

    id: number;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }
}
