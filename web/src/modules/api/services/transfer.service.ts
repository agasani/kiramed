/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators/map';
import { filter } from 'rxjs/operators/filter';

import { TransferCreateViewModel } from '../models/transfer-create-view-model';

@Injectable()
export class TransferService extends BaseService {
  constructor(config: ApiConfiguration,
              http: HttpClient) {
    super(config, http);
  }

  /**
   * @param params The `TransferService.ApiTransferGetIncomingByOrgIdGetParams` containing the following parameters:
   *
   * - `orgId`:
   *
   * - `correlationId`: Correlation Id for the request
   */
  ApiTransferGetIncomingByOrgIdGetResponse(params: TransferService.ApiTransferGetIncomingByOrgIdGetParams): Observable<HttpResponse<void>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'GET',
        this.rootUrl + `/api/Transfer/GetIncoming/${params.orgId}`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'text'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: void = null;

          return _resp.clone({ body: _body }) as HttpResponse<void>;
        })
    );
  }

  /**
   * @param params The `TransferService.ApiTransferGetIncomingByOrgIdGetParams` containing the following parameters:
   *
   * - `orgId`:
   *
   * - `correlationId`: Correlation Id for the request
   */
  ApiTransferGetIncomingByOrgIdGet(params: TransferService.ApiTransferGetIncomingByOrgIdGetParams): Observable<void> {
    return this.ApiTransferGetIncomingByOrgIdGetResponse(params).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `TransferService.ApiTransferGetOutGoingByOrgIdGetParams` containing the following parameters:
   *
   * - `orgId`:
   *
   * - `correlationId`: Correlation Id for the request
   */
  ApiTransferGetOutGoingByOrgIdGetResponse(params: TransferService.ApiTransferGetOutGoingByOrgIdGetParams): Observable<HttpResponse<void>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'GET',
        this.rootUrl + `/api/Transfer/GetOutGoing/${params.orgId}`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'text'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: void = null;

          return _resp.clone({ body: _body }) as HttpResponse<void>;
        })
    );
  }

  /**
   * @param params The `TransferService.ApiTransferGetOutGoingByOrgIdGetParams` containing the following parameters:
   *
   * - `orgId`:
   *
   * - `correlationId`: Correlation Id for the request
   */
  ApiTransferGetOutGoingByOrgIdGet(params: TransferService.ApiTransferGetOutGoingByOrgIdGetParams): Observable<void> {
    return this.ApiTransferGetOutGoingByOrgIdGetResponse(params).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `TransferService.ApiTransferPostParams` containing the following parameters:
   *
   * - `vModel`:
   *
   * - `correlationId`: Correlation Id for the request
   */
  ApiTransferPostResponse(params: TransferService.ApiTransferPostParams): Observable<HttpResponse<void>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = params.vModel;
    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'POST',
        this.rootUrl + `/api/Transfer`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'text'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: void = null;

          return _resp.clone({ body: _body }) as HttpResponse<void>;
        })
    );
  }

  /**
   * @param params The `TransferService.ApiTransferPostParams` containing the following parameters:
   *
   * - `vModel`:
   *
   * - `correlationId`: Correlation Id for the request
   */
  ApiTransferPost(params: TransferService.ApiTransferPostParams): Observable<void> {
    return this.ApiTransferPostResponse(params).pipe(
        map(_r => _r.body)
    );
  }
}

export module TransferService {

  /**
   * Parameters for ApiTransferGetIncomingByOrgIdGet
   */
  export interface ApiTransferGetIncomingByOrgIdGetParams {

    orgId: number;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }

  /**
   * Parameters for ApiTransferGetOutGoingByOrgIdGet
   */
  export interface ApiTransferGetOutGoingByOrgIdGetParams {

    orgId: number;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }

  /**
   * Parameters for ApiTransferPost
   */
  export interface ApiTransferPostParams {

    vModel?: TransferCreateViewModel;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }
}
