/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators/map';
import { filter } from 'rxjs/operators/filter';

import { EncounterViewModel } from '../models/encounter-view-model';

@Injectable()
export class EncounterService extends BaseService {
  constructor(config: ApiConfiguration,
              http: HttpClient) {
    super(config, http);
  }

  /**
   * @param params The `EncounterService.ApiEncounterOrganizationIdGetParams` containing the following parameters:
   *
   * - `organizationId`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully listed
   */
  ApiEncounterOrganizationIdGetResponse(params: EncounterService.ApiEncounterOrganizationIdGetParams): Observable<HttpResponse<EncounterViewModel[]>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'GET',
        this.rootUrl + `/api/Encounter/organizationId`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: EncounterViewModel[] = null;
          _body = _resp.body as EncounterViewModel[];
          return _resp.clone({ body: _body }) as HttpResponse<EncounterViewModel[]>;
        })
    );
  }

  /**
   * @param params The `EncounterService.ApiEncounterOrganizationIdGetParams` containing the following parameters:
   *
   * - `organizationId`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully listed
   */
  ApiEncounterOrganizationIdGet(params: EncounterService.ApiEncounterOrganizationIdGetParams): Observable<EncounterViewModel[]> {
    return this.ApiEncounterOrganizationIdGetResponse(params).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `EncounterService.ApiEncounterGetByPatientByPatientIdGetParams` containing the following parameters:
   *
   * - `patientId`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully listed
   */
  ApiEncounterGetByPatientByPatientIdGetResponse(params: EncounterService.ApiEncounterGetByPatientByPatientIdGetParams): Observable<HttpResponse<EncounterViewModel[]>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'GET',
        this.rootUrl + `/api/Encounter/GetByPatient/${params.patientId}`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: EncounterViewModel[] = null;
          _body = _resp.body as EncounterViewModel[];
          return _resp.clone({ body: _body }) as HttpResponse<EncounterViewModel[]>;
        })
    );
  }

  /**
   * @param params The `EncounterService.ApiEncounterGetByPatientByPatientIdGetParams` containing the following parameters:
   *
   * - `patientId`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully listed
   */
  ApiEncounterGetByPatientByPatientIdGet(params: EncounterService.ApiEncounterGetByPatientByPatientIdGetParams): Observable<EncounterViewModel[]> {
    return this.ApiEncounterGetByPatientByPatientIdGetResponse(params).pipe(
        map(_r => _r.body)
    );
  }
}

export module EncounterService {

  /**
   * Parameters for ApiEncounterOrganizationIdGet
   */
  export interface ApiEncounterOrganizationIdGetParams {

    organizationId: number;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }

  /**
   * Parameters for ApiEncounterGetByPatientByPatientIdGet
   */
  export interface ApiEncounterGetByPatientByPatientIdGetParams {

    patientId: number;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }
}
