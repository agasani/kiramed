/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators/map';
import { filter } from 'rxjs/operators/filter';

import { EntityDemographicViewModel } from '../models/entity-demographic-view-model';
import { FeedBack } from '../models/feed-back';

@Injectable()
export class DemographicService extends BaseService {
  constructor(config: ApiConfiguration,
              http: HttpClient) {
    super(config, http);
  }

  /**
   * @param params The `DemographicService.ApiDemographicGetByIdByIdGetParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully retrieved
   */
  ApiDemographicGetByIdByIdGetResponse(params: DemographicService.ApiDemographicGetByIdByIdGetParams): Observable<HttpResponse<EntityDemographicViewModel[]>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'GET',
        this.rootUrl + `/api/Demographic/GetById/${params.id}`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: EntityDemographicViewModel[] = null;
          _body = _resp.body as EntityDemographicViewModel[];
          return _resp.clone({ body: _body }) as HttpResponse<EntityDemographicViewModel[]>;
        })
    );
  }

  /**
   * @param params The `DemographicService.ApiDemographicGetByIdByIdGetParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully retrieved
   */
  ApiDemographicGetByIdByIdGet(params: DemographicService.ApiDemographicGetByIdByIdGetParams): Observable<EntityDemographicViewModel[]> {
    return this.ApiDemographicGetByIdByIdGetResponse(params).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `DemographicService.ApiDemographicByIdPutParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `vModel`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Model found
   */
  ApiDemographicByIdPutResponse(params: DemographicService.ApiDemographicByIdPutParams): Observable<HttpResponse<FeedBack>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.vModel;
    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'PUT',
        this.rootUrl + `/api/Demographic/${params.id}`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: FeedBack = null;
          _body = _resp.body as FeedBack;
          return _resp.clone({ body: _body }) as HttpResponse<FeedBack>;
        })
    );
  }

  /**
   * @param params The `DemographicService.ApiDemographicByIdPutParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `vModel`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Model found
   */
  ApiDemographicByIdPut(params: DemographicService.ApiDemographicByIdPutParams): Observable<FeedBack> {
    return this.ApiDemographicByIdPutResponse(params).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `DemographicService.ApiDemographicPostParams` containing the following parameters:
   *
   * - `vModel`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Model found
   */
  ApiDemographicPostResponse(params: DemographicService.ApiDemographicPostParams): Observable<HttpResponse<FeedBack>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = params.vModel;
    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'POST',
        this.rootUrl + `/api/Demographic`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: FeedBack = null;
          _body = _resp.body as FeedBack;
          return _resp.clone({ body: _body }) as HttpResponse<FeedBack>;
        })
    );
  }

  /**
   * @param params The `DemographicService.ApiDemographicPostParams` containing the following parameters:
   *
   * - `vModel`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Model found
   */
  ApiDemographicPost(params: DemographicService.ApiDemographicPostParams): Observable<FeedBack> {
    return this.ApiDemographicPostResponse(params).pipe(
        map(_r => _r.body)
    );
  }
}

export module DemographicService {

  /**
   * Parameters for ApiDemographicGetByIdByIdGet
   */
  export interface ApiDemographicGetByIdByIdGetParams {

    id: number;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }

  /**
   * Parameters for ApiDemographicByIdPut
   */
  export interface ApiDemographicByIdPutParams {

    id: number;

    vModel?: EntityDemographicViewModel;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }

  /**
   * Parameters for ApiDemographicPost
   */
  export interface ApiDemographicPostParams {

    vModel?: EntityDemographicViewModel;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }
}
