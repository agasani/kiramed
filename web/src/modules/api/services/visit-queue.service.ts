/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators/map';
import { filter } from 'rxjs/operators/filter';

import { VisitQueueViewModel } from '../models/visit-queue-view-model';
import { FeedBack } from '../models/feed-back';
import { VisitQueueCreateViewModel } from '../models/visit-queue-create-view-model';

@Injectable()
export class VisitQueueService extends BaseService {
  constructor(config: ApiConfiguration,
              http: HttpClient) {
    super(config, http);
  }

  /**
   * @param params The `VisitQueueService.ApiVisitQueueGetParams` containing the following parameters:
   *
   * - `organizationId`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Retrieved successfully
   */
  ApiVisitQueueGetResponse(params: VisitQueueService.ApiVisitQueueGetParams): Observable<HttpResponse<VisitQueueViewModel[]>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (params.organizationId != null) __params = __params.set('organizationId', params.organizationId.toString());
    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'GET',
        this.rootUrl + `/api/VisitQueue`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: VisitQueueViewModel[] = null;
          _body = _resp.body as VisitQueueViewModel[];
          return _resp.clone({ body: _body }) as HttpResponse<VisitQueueViewModel[]>;
        })
    );
  }

  /**
   * @param params The `VisitQueueService.ApiVisitQueueGetParams` containing the following parameters:
   *
   * - `organizationId`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Retrieved successfully
   */
  ApiVisitQueueGet(params: VisitQueueService.ApiVisitQueueGetParams): Observable<VisitQueueViewModel[]> {
    return this.ApiVisitQueueGetResponse(params).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `VisitQueueService.ApiVisitQueuePostParams` containing the following parameters:
   *
   * - `vModel`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Created successfully
   */
  ApiVisitQueuePostResponse(params: VisitQueueService.ApiVisitQueuePostParams): Observable<HttpResponse<FeedBack>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = params.vModel;
    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'POST',
        this.rootUrl + `/api/VisitQueue`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: FeedBack = null;
          _body = _resp.body as FeedBack;
          return _resp.clone({ body: _body }) as HttpResponse<FeedBack>;
        })
    );
  }

  /**
   * @param params The `VisitQueueService.ApiVisitQueuePostParams` containing the following parameters:
   *
   * - `vModel`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Created successfully
   */
  ApiVisitQueuePost(params: VisitQueueService.ApiVisitQueuePostParams): Observable<FeedBack> {
    return this.ApiVisitQueuePostResponse(params).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `VisitQueueService.ApiVisitQueueDetailByIdGetParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Retrieved successfully
   */
  ApiVisitQueueDetailByIdGetResponse(params: VisitQueueService.ApiVisitQueueDetailByIdGetParams): Observable<HttpResponse<VisitQueueViewModel>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'GET',
        this.rootUrl + `/api/VisitQueue/Detail/${params.id}`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: VisitQueueViewModel = null;
          _body = _resp.body as VisitQueueViewModel;
          return _resp.clone({ body: _body }) as HttpResponse<VisitQueueViewModel>;
        })
    );
  }

  /**
   * @param params The `VisitQueueService.ApiVisitQueueDetailByIdGetParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Retrieved successfully
   */
  ApiVisitQueueDetailByIdGet(params: VisitQueueService.ApiVisitQueueDetailByIdGetParams): Observable<VisitQueueViewModel> {
    return this.ApiVisitQueueDetailByIdGetResponse(params).pipe(
        map(_r => _r.body)
    );
  }
}

export module VisitQueueService {

  /**
   * Parameters for ApiVisitQueueGet
   */
  export interface ApiVisitQueueGetParams {

    organizationId: number;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }

  /**
   * Parameters for ApiVisitQueuePost
   */
  export interface ApiVisitQueuePostParams {

    vModel?: VisitQueueCreateViewModel;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }

  /**
   * Parameters for ApiVisitQueueDetailByIdGet
   */
  export interface ApiVisitQueueDetailByIdGetParams {

    id: number;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }
}
