/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators/map';
import { filter } from 'rxjs/operators/filter';

import { VitalSignTypeViewModel } from '../models/vital-sign-type-view-model';
import { FeedBack } from '../models/feed-back';

@Injectable()
export class VitalSignTypeService extends BaseService {
  constructor(config: ApiConfiguration,
              http: HttpClient) {
    super(config, http);
  }

  /**
   * @param correlationId Correlation Id for the request
   * @return Listed successfully
   */
  ApiVitalSignTypeGetResponse(correlationId?: string): Observable<HttpResponse<VitalSignTypeViewModel[]>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (correlationId != null) __headers = __headers.set('correlationId', correlationId.toString());
    let req = new HttpRequest<any>(
        'GET',
        this.rootUrl + `/api/VitalSignType`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: VitalSignTypeViewModel[] = null;
          _body = _resp.body as VitalSignTypeViewModel[];
          return _resp.clone({ body: _body }) as HttpResponse<VitalSignTypeViewModel[]>;
        })
    );
  }

  /**
   * @param correlationId Correlation Id for the request
   * @return Listed successfully
   */
  ApiVitalSignTypeGet(correlationId?: string): Observable<VitalSignTypeViewModel[]> {
    return this.ApiVitalSignTypeGetResponse(correlationId).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `VitalSignTypeService.ApiVitalSignTypePostParams` containing the following parameters:
   *
   * - `vModel`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Retrieved successfully
   */
  ApiVitalSignTypePostResponse(params: VitalSignTypeService.ApiVitalSignTypePostParams): Observable<HttpResponse<FeedBack>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = params.vModel;
    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'POST',
        this.rootUrl + `/api/VitalSignType`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: FeedBack = null;
          _body = _resp.body as FeedBack;
          return _resp.clone({ body: _body }) as HttpResponse<FeedBack>;
        })
    );
  }

  /**
   * @param params The `VitalSignTypeService.ApiVitalSignTypePostParams` containing the following parameters:
   *
   * - `vModel`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Retrieved successfully
   */
  ApiVitalSignTypePost(params: VitalSignTypeService.ApiVitalSignTypePostParams): Observable<FeedBack> {
    return this.ApiVitalSignTypePostResponse(params).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `VitalSignTypeService.ApiVitalSignTypeDetailByIdGetParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Retrieved successfully
   */
  ApiVitalSignTypeDetailByIdGetResponse(params: VitalSignTypeService.ApiVitalSignTypeDetailByIdGetParams): Observable<HttpResponse<VitalSignTypeViewModel>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'GET',
        this.rootUrl + `/api/VitalSignType/Detail/${params.id}`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: VitalSignTypeViewModel = null;
          _body = _resp.body as VitalSignTypeViewModel;
          return _resp.clone({ body: _body }) as HttpResponse<VitalSignTypeViewModel>;
        })
    );
  }

  /**
   * @param params The `VitalSignTypeService.ApiVitalSignTypeDetailByIdGetParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Retrieved successfully
   */
  ApiVitalSignTypeDetailByIdGet(params: VitalSignTypeService.ApiVitalSignTypeDetailByIdGetParams): Observable<VitalSignTypeViewModel> {
    return this.ApiVitalSignTypeDetailByIdGetResponse(params).pipe(
        map(_r => _r.body)
    );
  }
}

export module VitalSignTypeService {

  /**
   * Parameters for ApiVitalSignTypePost
   */
  export interface ApiVitalSignTypePostParams {

    vModel?: VitalSignTypeViewModel;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }

  /**
   * Parameters for ApiVitalSignTypeDetailByIdGet
   */
  export interface ApiVitalSignTypeDetailByIdGetParams {

    id: number;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }
}
