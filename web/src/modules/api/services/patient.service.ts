/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators/map';
import { filter } from 'rxjs/operators/filter';

import { PatientViewModel } from '../models/patient-view-model';
import { FeedBack } from '../models/feed-back';
import { PatientRegistrationViewModel } from '../models/patient-registration-view-model';

@Injectable()
export class PatientService extends BaseService {
  constructor(config: ApiConfiguration,
              http: HttpClient) {
    super(config, http);
  }

  /**
   * @param correlationId Correlation Id for the request
   * @return Successfully listed
   */
  ApiPatientGetResponse(correlationId?: string): Observable<HttpResponse<PatientViewModel[]>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (correlationId != null) __headers = __headers.set('correlationId', correlationId.toString());
    let req = new HttpRequest<any>(
        'GET',
        this.rootUrl + `/api/Patient`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: PatientViewModel[] = null;
          _body = _resp.body as PatientViewModel[];
          return _resp.clone({ body: _body }) as HttpResponse<PatientViewModel[]>;
        })
    );
  }

  /**
   * @param correlationId Correlation Id for the request
   * @return Successfully listed
   */
  ApiPatientGet(correlationId?: string): Observable<PatientViewModel[]> {
    return this.ApiPatientGetResponse(correlationId).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `PatientService.ApiPatientPostParams` containing the following parameters:
   *
   * - `vModel`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully created
   */
  ApiPatientPostResponse(params: PatientService.ApiPatientPostParams): Observable<HttpResponse<FeedBack>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = params.vModel;
    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'POST',
        this.rootUrl + `/api/Patient`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: FeedBack = null;
          _body = _resp.body as FeedBack;
          return _resp.clone({ body: _body }) as HttpResponse<FeedBack>;
        })
    );
  }

  /**
   * @param params The `PatientService.ApiPatientPostParams` containing the following parameters:
   *
   * - `vModel`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully created
   */
  ApiPatientPost(params: PatientService.ApiPatientPostParams): Observable<FeedBack> {
    return this.ApiPatientPostResponse(params).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `PatientService.ApiPatientDetailsByIdGetParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully listed
   */
  ApiPatientDetailsByIdGetResponse(params: PatientService.ApiPatientDetailsByIdGetParams): Observable<HttpResponse<PatientViewModel[]>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'GET',
        this.rootUrl + `/api/Patient/Details/${params.id}`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: PatientViewModel[] = null;
          _body = _resp.body as PatientViewModel[];
          return _resp.clone({ body: _body }) as HttpResponse<PatientViewModel[]>;
        })
    );
  }

  /**
   * @param params The `PatientService.ApiPatientDetailsByIdGetParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully listed
   */
  ApiPatientDetailsByIdGet(params: PatientService.ApiPatientDetailsByIdGetParams): Observable<PatientViewModel[]> {
    return this.ApiPatientDetailsByIdGetResponse(params).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `PatientService.ApiPatientByIdPutParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `vModel`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully updated
   */
  ApiPatientByIdPutResponse(params: PatientService.ApiPatientByIdPutParams): Observable<HttpResponse<FeedBack>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.vModel;
    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'PUT',
        this.rootUrl + `/api/Patient/${params.id}`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: FeedBack = null;
          _body = _resp.body as FeedBack;
          return _resp.clone({ body: _body }) as HttpResponse<FeedBack>;
        })
    );
  }

  /**
   * @param params The `PatientService.ApiPatientByIdPutParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `vModel`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully updated
   */
  ApiPatientByIdPut(params: PatientService.ApiPatientByIdPutParams): Observable<FeedBack> {
    return this.ApiPatientByIdPutResponse(params).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `PatientService.ApiPatientByIdDeleteParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully deleted
   */
  ApiPatientByIdDeleteResponse(params: PatientService.ApiPatientByIdDeleteParams): Observable<HttpResponse<FeedBack>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'DELETE',
        this.rootUrl + `/api/Patient/${params.id}`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: FeedBack = null;
          _body = _resp.body as FeedBack;
          return _resp.clone({ body: _body }) as HttpResponse<FeedBack>;
        })
    );
  }

  /**
   * @param params The `PatientService.ApiPatientByIdDeleteParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Successfully deleted
   */
  ApiPatientByIdDelete(params: PatientService.ApiPatientByIdDeleteParams): Observable<FeedBack> {
    return this.ApiPatientByIdDeleteResponse(params).pipe(
        map(_r => _r.body)
    );
  }
}

export module PatientService {

  /**
   * Parameters for ApiPatientPost
   */
  export interface ApiPatientPostParams {

    vModel?: PatientRegistrationViewModel;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }

  /**
   * Parameters for ApiPatientDetailsByIdGet
   */
  export interface ApiPatientDetailsByIdGetParams {

    id: number;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }

  /**
   * Parameters for ApiPatientByIdPut
   */
  export interface ApiPatientByIdPutParams {

    id: number;

    vModel?: PatientRegistrationViewModel;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }

  /**
   * Parameters for ApiPatientByIdDelete
   */
  export interface ApiPatientByIdDeleteParams {

    id: number;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }
}
