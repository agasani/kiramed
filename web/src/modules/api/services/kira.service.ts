/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators/map';
import { filter } from 'rxjs/operators/filter';

import { Patient } from '../models/patient';

@Injectable()
export class KiraService extends BaseService {
  constructor(config: ApiConfiguration,
              http: HttpClient) {
    super(config, http);
  }

  /**
   * @param correlationId Correlation Id for the request
   * @return Success
   */
  ApiKiraGetResponse(correlationId?: string): Observable<HttpResponse<Patient[]>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (correlationId != null) __headers = __headers.set('correlationId', correlationId.toString());
    let req = new HttpRequest<any>(
        'GET',
        this.rootUrl + `/api/Kira`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: Patient[] = null;
          _body = _resp.body as Patient[];
          return _resp.clone({ body: _body }) as HttpResponse<Patient[]>;
        })
    );
  }

  /**
   * @param correlationId Correlation Id for the request
   * @return Success
   */
  ApiKiraGet(correlationId?: string): Observable<Patient[]> {
    return this.ApiKiraGetResponse(correlationId).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `KiraService.ApiKiraPostParams` containing the following parameters:
   *
   * - `value`:
   *
   * - `correlationId`: Correlation Id for the request
   */
  ApiKiraPostResponse(params: KiraService.ApiKiraPostParams): Observable<HttpResponse<void>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = params.value;
    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'POST',
        this.rootUrl + `/api/Kira`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'text'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: void = null;

          return _resp.clone({ body: _body }) as HttpResponse<void>;
        })
    );
  }

  /**
   * @param params The `KiraService.ApiKiraPostParams` containing the following parameters:
   *
   * - `value`:
   *
   * - `correlationId`: Correlation Id for the request
   */
  ApiKiraPost(params: KiraService.ApiKiraPostParams): Observable<void> {
    return this.ApiKiraPostResponse(params).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `KiraService.ApiKiraByIdGetParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Success
   */
  ApiKiraByIdGetResponse(params: KiraService.ApiKiraByIdGetParams): Observable<HttpResponse<string>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'GET',
        this.rootUrl + `/api/Kira/${params.id}`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'text'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: string = null;
          _body = _resp.body as string;
          return _resp.clone({ body: _body }) as HttpResponse<string>;
        })
    );
  }

  /**
   * @param params The `KiraService.ApiKiraByIdGetParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Success
   */
  ApiKiraByIdGet(params: KiraService.ApiKiraByIdGetParams): Observable<string> {
    return this.ApiKiraByIdGetResponse(params).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `KiraService.ApiKiraByIdPutParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `value`:
   *
   * - `correlationId`: Correlation Id for the request
   */
  ApiKiraByIdPutResponse(params: KiraService.ApiKiraByIdPutParams): Observable<HttpResponse<void>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.value;
    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'PUT',
        this.rootUrl + `/api/Kira/${params.id}`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'text'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: void = null;

          return _resp.clone({ body: _body }) as HttpResponse<void>;
        })
    );
  }

  /**
   * @param params The `KiraService.ApiKiraByIdPutParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `value`:
   *
   * - `correlationId`: Correlation Id for the request
   */
  ApiKiraByIdPut(params: KiraService.ApiKiraByIdPutParams): Observable<void> {
    return this.ApiKiraByIdPutResponse(params).pipe(
        map(_r => _r.body)
    );
  }

  /**
   * @param params The `KiraService.ApiKiraByIdDeleteParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `correlationId`: Correlation Id for the request
   */
  ApiKiraByIdDeleteResponse(params: KiraService.ApiKiraByIdDeleteParams): Observable<HttpResponse<void>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'DELETE',
        this.rootUrl + `/api/Kira/${params.id}`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'text'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: void = null;

          return _resp.clone({ body: _body }) as HttpResponse<void>;
        })
    );
  }

  /**
   * @param params The `KiraService.ApiKiraByIdDeleteParams` containing the following parameters:
   *
   * - `id`:
   *
   * - `correlationId`: Correlation Id for the request
   */
  ApiKiraByIdDelete(params: KiraService.ApiKiraByIdDeleteParams): Observable<void> {
    return this.ApiKiraByIdDeleteResponse(params).pipe(
        map(_r => _r.body)
    );
  }
}

export module KiraService {

  /**
   * Parameters for ApiKiraPost
   */
  export interface ApiKiraPostParams {

    value?: string;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }

  /**
   * Parameters for ApiKiraByIdGet
   */
  export interface ApiKiraByIdGetParams {

    id: number;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }

  /**
   * Parameters for ApiKiraByIdPut
   */
  export interface ApiKiraByIdPutParams {

    id: number;

    value?: string;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }

  /**
   * Parameters for ApiKiraByIdDelete
   */
  export interface ApiKiraByIdDeleteParams {

    id: number;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }
}
