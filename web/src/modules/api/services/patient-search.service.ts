/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators/map';
import { filter } from 'rxjs/operators/filter';

import { PatientSearchViewModel } from '../models/patient-search-view-model';

@Injectable()
export class PatientSearchService extends BaseService {
  constructor(config: ApiConfiguration,
              http: HttpClient) {
    super(config, http);
  }

  /**
   * @param params The `PatientSearchService.ApiPatientsPatientSearchBySearchInputGetParams` containing the following parameters:
   *
   * - `searchInput`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Listed successfully
   */
  ApiPatientsPatientSearchBySearchInputGetResponse(params: PatientSearchService.ApiPatientsPatientSearchBySearchInputGetParams): Observable<HttpResponse<PatientSearchViewModel[]>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    if (params.correlationId != null) __headers = __headers.set('correlationId', params.correlationId.toString());
    let req = new HttpRequest<any>(
        'GET',
        this.rootUrl + `/api/patients/PatientSearch/${params.searchInput}`,
        __body,
        {
          headers: __headers,
          params: __params,
          responseType: 'json'
        });

    return this.http.request<any>(req).pipe(
        filter(_r => _r instanceof HttpResponse),
        map(_r => {
          let _resp = _r as HttpResponse<any>;
          let _body: PatientSearchViewModel[] = null;
          _body = _resp.body as PatientSearchViewModel[];
          return _resp.clone({ body: _body }) as HttpResponse<PatientSearchViewModel[]>;
        })
    );
  }

  /**
   * @param params The `PatientSearchService.ApiPatientsPatientSearchBySearchInputGetParams` containing the following parameters:
   *
   * - `searchInput`:
   *
   * - `correlationId`: Correlation Id for the request
   *
   * @return Listed successfully
   */
  ApiPatientsPatientSearchBySearchInputGet(params: PatientSearchService.ApiPatientsPatientSearchBySearchInputGetParams): Observable<PatientSearchViewModel[]> {
    return this.ApiPatientsPatientSearchBySearchInputGetResponse(params).pipe(
        map(_r => _r.body)
    );
  }
}

export module PatientSearchService {

  /**
   * Parameters for ApiPatientsPatientSearchBySearchInputGet
   */
  export interface ApiPatientsPatientSearchBySearchInputGetParams {

    searchInput: string;

    /**
     * Correlation Id for the request
     */
    correlationId?: string;
  }
}
