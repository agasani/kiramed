import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ApiConfiguration } from './api-configuration';

import { AppointmentService } from './services/appointment.service';
import { CountryService } from './services/country.service';
import { DemographicService } from './services/demographic.service';
import { DiagnosisService } from './services/diagnosis.service';
import { EncounterService } from './services/encounter.service';
import { EntityService } from './services/entity.service';
import { EntityAddressService } from './services/entity-address.service';
import { KiraService } from './services/kira.service';
import { MedService } from './services/med.service';
import { OrganizationService } from './services/organization.service';
import { PatientService } from './services/patient.service';
import { PatientDiagnosisService } from './services/patient-diagnosis.service';
import { PatientSearchService } from './services/patient-search.service';
import { RoleService } from './services/role.service';
import { SystemActionService } from './services/system-action.service';
import { TransferService } from './services/transfer.service';
import { UserService } from './services/user.service';
import { VisitService } from './services/visit.service';
import { VisitQueueService } from './services/visit-queue.service';
import { VitalSignService } from './services/vital-sign.service';
import { VitalSignMeasureService } from './services/vital-sign-measure.service';
import { VitalSignTypeService } from './services/vital-sign-type.service';

/**
 * Module that provides instances for all API services
 */
@NgModule({
  imports: [
    HttpClientModule
  ],
  exports: [
    HttpClientModule
  ],
  declarations: [],
  providers: [
    ApiConfiguration,
    AppointmentService,
    CountryService,
    DemographicService,
    DiagnosisService,
    EncounterService,
    EntityService,
    EntityAddressService,
    KiraService,
    MedService,
    OrganizationService,
    PatientService,
    PatientDiagnosisService,
    PatientSearchService,
    RoleService,
    SystemActionService,
    TransferService,
    UserService,
    VisitService,
    VisitQueueService,
    VitalSignService,
    VitalSignMeasureService,
    VitalSignTypeService
  ],
})
export class ApiModule {
}
