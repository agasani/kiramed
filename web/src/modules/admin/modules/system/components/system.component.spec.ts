import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { SystemComponent } from './system.component';

describe('SystemComponent', () => {
  let comp: SystemComponent;
  let fixture: ComponentFixture<SystemComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SystemComponent],
      schemas: [NO_ERRORS_SCHEMA]
    });
    fixture = TestBed.createComponent(SystemComponent);
    comp = fixture.componentInstance;
  });

  it('can load instance', () => {
    expect(comp).toBeTruthy();
  });

});
