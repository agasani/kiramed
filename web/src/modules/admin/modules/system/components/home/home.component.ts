import { Component } from '@angular/core';

@Component({
  selector: 'kira-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})

export class HomeComponent {

  constructor() {
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngAfterViewInit() {
    console.log('Welcome to the dashboard');
  }
}
