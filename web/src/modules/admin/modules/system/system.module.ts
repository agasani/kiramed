﻿import { NgModule } from '@angular/core';

import { HomeComponent, SystemComponent } from './components';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';


const ROUTES: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  {
    path: '',
    component: SystemComponent,
    children: [{
      path: 'home',
      component: HomeComponent,
      data: { title: 'System dashboard', accessGuardClaims: ['default'] }
    }]
  },
];

@NgModule({
  imports: [RouterModule.forChild(ROUTES), SharedModule],
  declarations: [SystemComponent, HomeComponent]
})

export class SystemModule {
}

