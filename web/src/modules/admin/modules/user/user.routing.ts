﻿import { RouterModule, Routes } from '@angular/router';
import { UserComponent, UserInfoComponent, UsersComponent } from './components';
import {
  ApiUserDetailByEmailGetResolve,
  ApiUserOrganizationUsersByOrgIdGetResolve
} from '../../../shared/modules/resolve/user';

const ROUTES: Routes = [
  {
    path: '',
    component: UserComponent,
    children: [
      {
        path: 'list/organization/:orgId',
        component: UsersComponent,
        data: { title: 'Users', accessGuardClaims: ['default'] },
        resolve: { user_list_organization_orgid: ApiUserOrganizationUsersByOrgIdGetResolve }
      },
      {
        path: 'detail/:email/organization/:orgId',
        component: UserInfoComponent,
        data: { title: 'User detail', accessGuardClaims: ['default'] },
        resolve: { user_detail_email_organization_orgid: ApiUserDetailByEmailGetResolve }
      },
      {
        path: 'home/:email',
        component: UserInfoComponent,
        data: { title: 'User home', accessGuardClaims: ['default'] },
        resolve: { user_home_email: ApiUserDetailByEmailGetResolve }
      },
    ]
  },
];

export const userRouting = RouterModule.forChild(ROUTES);
