import { Component } from '@angular/core';
import { BaseComponent } from '../../../../../shared/components';
import { UserViewModel } from '../../../../../api/models/user-view-model';
import { UserService } from '../../../../../api/services/user.service';

@Component({
  selector: 'kira-user-info',
  templateUrl: './user-info.component.html'
})
export class UserInfoComponent extends BaseComponent {
  constructor(itemService: UserService) {
    super();
    this.itemService = itemService;
  }

  toggleLock(item: UserViewModel) {
    item.isLocked ? this.unlock(item) : this.lock(item);
  };

  private lock(item): void {
    this.itemService.ApiUserLockPost(item.email).subscribe(
        () => console.log('Locked'),
        error => this.errorMsg = error,
        () => this.successMsg = `${this.itemName} locked successfully!`
    );
  }

  private unlock(item): void {
    this.itemService.ApiUserUnlockPost(item.email).subscribe(
        () => console.log('Unlocked'),
        error => this.errorMsg = error,
        () => this.successMsg = `${this.itemName} unlocked successfully!`
    );
  }
}
