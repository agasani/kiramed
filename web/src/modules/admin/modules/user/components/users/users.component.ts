import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseComponent } from '../../../../../shared/components';
import { UserService } from '../../../../../api/services/user.service';
import { UserRegisterViewModel } from '../../../../../api/models/user-register-view-model';
import { RoleService } from '../../../../../api/services/role.service';

@Component({
  selector: 'kira-users',
  templateUrl: './users.component.html'
})
export class UsersComponent extends BaseComponent {
  orgId = +this.settings['userOrgId'];
  countryId = +this.settings['userCountryId'];
  roles: string[];

  constructor(router: Router,
              route: ActivatedRoute,
              itemService: UserService,
              private roleService: RoleService,
              private fb: FormBuilder) {
    super();
    this.route = route;
    this.router = router;
    this.itemService = itemService;
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngOnInit() {
    this.firstUrlPart = `organization/${this.orgId}/user`;
    this.redirect = true;
    this.itemName = 'User';
    this.pageSize = 2;
    this.roleService.ApiRoleGetOrgRolesByOrgIdGet({ orgId: this.orgId }).subscribe(
        orgRoles => {
          for (const idx in orgRoles) this.hasProperty(orgRoles, idx) &&
          this.roles.push(orgRoles[idx].name);
        },
        error => console.error('Error organization roles listing ', error)
    );
    this.getObservable = this.itemService.ApiUserOrganizationUsersByOrgIdGet(this.orgId);
    this.postObservable = this.item && this.itemService.ApiUserPost(this.item);
  }

  ngAfterViewInit() {
    try {
      /**/
    } catch (e) {
      /**/
    }
  }

  initCreateForm(): void {
    this.formGroup = this.fb.group({
      userName: ['', [Validators.required, Validators.minLength(6)]],
      firstName: ['', Validators.required],
      middleName: [''],
      secondName: ['', Validators.required],
      nationalId: ['', Validators.required],
      email: ['', Validators.required],
      phoneNumber: ['', Validators.required],
      roles: [[], Validators.required],
    });
  }

  createUser(item: UserRegisterViewModel): void {
    item.countryId = this.countryId;
    item.organizationId = this.orgId;
    item.parentId = 0;
    this.postObservable = this.itemService.ApiRolePost(item);
    this.create();
  }
}
