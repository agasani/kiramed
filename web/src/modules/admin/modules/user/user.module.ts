﻿import { NgModule } from '@angular/core';
import { userRouting } from './user.routing';
import { SharedModule } from '../../../shared/shared.module';
import { UserComponent, UserInfoComponent, UsersComponent } from './components';
import { UserService } from '../../../api/services/user.service';

@NgModule({
  imports: [userRouting, SharedModule],
  declarations: [UserComponent, UserInfoComponent, UsersComponent],
  providers: [UserService]
})

export class UserModule {
}
