import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseComponent } from '../../../../../shared/components';
import { CountryService } from '../../../../../api/services/country.service';
import { RouteSegment } from '../../../../../shared/services';
import { CountryRegistrationViewModel } from '../../../../../api/models/country-registration-view-model';

@Component({
  selector: 'kira-countries',
  templateUrl: './countries.component.html'
})
export class CountriesComponent extends BaseComponent {

  item: CountryRegistrationViewModel;

  constructor(_router: Router,
              _route: ActivatedRoute,
              countryService: CountryService,
              private fb: FormBuilder,
              private segment: RouteSegment) {
    super();
    this.itemService = countryService;
    this.router = _router;
    this.route = _route;
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngOnInit() {
    this.is_system = this.segment.path(0) === 'system';
    this.firstUrlPart = 'country/detail';
    this.redirect = true;
    this.itemName = 'Country';
    this.resolveData = this.route.snapshot.data['country_list'];
    this.getAll(this.resolveData);
  }

  initCreateForm(): void {
    this.formGroup = this.fb.group({
      name: ['', [Validators.required, Validators.maxLength(30)]],
      code: ['', [Validators.required, Validators.maxLength(3)]],
      abbreviation: ['', Validators.required],
      officialLanguage: [''],
      currency: [''],
      adminFirstName: ['', Validators.required],
      adminSecondName: ['', Validators.required],
      adminNationalId: ['', Validators.required],
      adminEmail: ['', Validators.required],
      adminPhoneNumber: ['', Validators.required],
    });
  }
}
