import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { BaseComponent } from '../../../../../shared/components';
import { RouteSegment } from '../../../../../shared/services';
import { CountryService } from '../../../../../api/services/country.service';
import { CountryViewModel } from '../../../../../api/models/country-view-model';

@Component({
  selector: 'kira-country-info',
  templateUrl: './country-info.component.html'
})
export class CountryInfoComponent extends BaseComponent {

  item: CountryViewModel;

  constructor(private _route: ActivatedRoute,
              private _router: Router,
              private _itemService: CountryService,
              private fb: FormBuilder,
              private segment: RouteSegment) {
    super();
    this.itemService = _itemService;
    this.route = _route;
    this.router = _router;
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngOnInit(): void {
    this.redirect = true;
    this.itemName = 'Country';
    this.is_details = this.segment.path(1) === 'detail';
    this.itemId = this.route.snapshot.parent.params['id'];

    this.resolveData = this.is_details ? this.route.snapshot.data['country_detail_id'] : this.route.snapshot.data['country_home_id'];

    this.getItem(this.resolveData);

    this.redirectUponDelete = 'country/list';
    this.deleteObservable = this.itemService.ApiCountryByIdDelete({ id: this.itemId });
    this.putObservable = this.itemService.ApiCountryByIdPut({ id: this.itemId, vModel: this.item });
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngAfterViewInit(): void {
    this.alertMessageTrigger();
  }

  initEditForm(item: CountryViewModel): void {
    this.formGroup = this.fb.group({
      name: [item.name, [Validators.required, Validators.maxLength(30)]],
      abbreviation: [item.abbreviation, Validators.required],
      code: [item.code, [Validators.required, Validators.maxLength(3)]],
      officialLanguage: [item.officialLanguage],
      currency: [item.currency],
    });
  }
}
