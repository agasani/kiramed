﻿import { NgModule } from '@angular/core';
import { CountriesComponent, CountryComponent, CountryInfoComponent, } from './components';
import { countryRouting } from './country.routing';
import { SharedModule } from '../../../shared/shared.module';
import { CountryService } from '../../../api/services/country.service';

@NgModule({
  imports: [countryRouting, SharedModule],
  declarations: [CountryComponent, CountriesComponent, CountryInfoComponent],
  providers: [CountryService],
})

export class CountryModule {
}
