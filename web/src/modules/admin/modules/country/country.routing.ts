﻿import { CountriesComponent, CountryComponent, CountryInfoComponent } from './components';
import { RouterModule, Routes } from '@angular/router';
import { ApiCountryGetByIdByIdGetResolve, ApiCountryGetResolve } from '../../../shared/modules/resolve/country';

const ROUTES: Routes = [
  {
    path: '',
    component: CountryComponent,
    children: [
      {
        path: 'list',
        component: CountriesComponent,
        data: { title: 'Countries', accessGuardClaims: ['default'] },
        resolve: { country_list: ApiCountryGetResolve }
      },
      {
        path: 'home/:id',
        component: CountryInfoComponent,
        data: { title: 'Country home', accessGuardClaims: ['default'] },
        resolve: { country_home_id: ApiCountryGetByIdByIdGetResolve }
      },
      {
        path: 'detail/:id',
        component: CountryInfoComponent,
        data: { title: 'Country detail', accessGuardClaims: ['default'] },
        resolve: { country_detail_id: ApiCountryGetByIdByIdGetResolve }
      }
    ]
  },
];

export const countryRouting = RouterModule.forChild(ROUTES);