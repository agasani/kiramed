import { AfterViewChecked, Component } from '@angular/core';
import { ParentToRole } from '../../models/parent-to-role';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseComponent } from '../../../../../shared/components';
import { RouteSegment } from '../../../../../shared/services';
import { RoleService } from '../../../../../api/services/role.service';
import { RoleViewModel } from '../../../../../api/models/role-view-model';

@Component({
  selector: 'kira-role-info',
  templateUrl: './role-info.component.html'
})
export class RoleInfoComponent extends BaseComponent implements AfterViewChecked {

  parent: ParentToRole;

  constructor(_route: ActivatedRoute,
              _router: Router,
              roleService: RoleService,
              private fb: FormBuilder,
              private segment: RouteSegment) {
    super();
    this.itemService = roleService;
    this.route = _route;
    this.router = _router;
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngOnInit(): void {
    this.redirect = true;
    this.itemName = 'Role';
    this.parent = new ParentToRole;
    this.parent.type = this.segment.path(3);

    if (this.parent.type === 'system')
      this.resolveData = this.route.snapshot.data['role_detail_roleid_system'];
    else if (this.parent.type === 'country')
      this.resolveData = this.route.snapshot.data['role_detail_roleid_country_id'];
    else this.resolveData = this.route.snapshot.data['role_detail_roleid_organization_orgid'];

    this.getItem(this.resolveData);

    this.parent.id = this.route.snapshot.parent.params['id'];
    this.redirectUponDelete = this.parent.type === 'system' ? 'role/list/system' : `role/list/${this.parent.type}/${this.parent.id}`;
    this.deleteObservable = this.itemService.ApiRoleByRoleIdDelete({ roleId: this.itemId });
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngAfterViewInit(): void {
    this.alertMessageTrigger();
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngAfterViewChecked() {
    try {
      $('.ui.search.dropdown').dropdown()
    } catch {
      console.log('Error in search dropdown!')
    }
  }

  initEditForm(item: RoleViewModel): void {
    this.formGroup = this.fb.group({
      name: [item.name, Validators.required],
      description: [item.description, [Validators.required, Validators.minLength(0), Validators.maxLength(100)]],
      claims: [item.claims],
    });
  }

  editRole(role: RoleViewModel, roleId: string) {
    this.putObservable = this.itemService.ApiRoleByRoleIdPut({ roleId: roleId, model: role });
    this.edit();
  }
}
