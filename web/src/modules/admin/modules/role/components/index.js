"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(require("./roles/roles.component"));
__export(require("./role-info/role-info.component"));
__export(require("./role.component"));
//# sourceMappingURL=index.js.map