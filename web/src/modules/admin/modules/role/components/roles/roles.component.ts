import { AfterViewChecked, Component } from '@angular/core';
import { ParentToRole } from '../../models/parent-to-role';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseComponent } from '../../../../../shared/components';
import { RouteSegment } from '../../../../../shared/services';
import { RoleService } from '../../../../../api/services/role.service';
import { RoleViewModel } from '../../../../../api/models/role-view-model';

@Component({
  selector: 'kira-roles',
  templateUrl: './roles.component.html',
})
export class RolesComponent extends BaseComponent implements AfterViewChecked {

  private parent: ParentToRole;
  private actionsList: string[] = ['one', 'two', 'three'];

  constructor(_router: Router,
              _route: ActivatedRoute,
              roleService: RoleService,
              private fb: FormBuilder,
              private segment: RouteSegment) {
    super();
    this.itemService = roleService;
    this.router = _router;
    this.route = _route;
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngAfterViewInit(): void {
    this.alertMessageTrigger();
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngAfterViewChecked() {
    try {
      $('.ui.search.dropdown').dropdown()
    } catch {
      console.log('Error in search dropdown!')
    }
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngOnInit() {
    this.itemName = 'Role';
    this.respDataIdProperty = 'roleId';
    this.redirect = true;
    this.parent = new ParentToRole;
    this.parent.type = this.segment.path(2);
    switch (this.parent.type) {
      case'system':
        this.resolveData = this.route.snapshot.data['role_list_system'];
        this.parent.typeId = 3;
        break;
      case'country':
        this.resolveData = this.route.snapshot.data['role_list_country_id'];
        this.parent.id = this.route.snapshot.parent.params['id'];
        this.parent.typeId = 1;
        break;
      default:
        this.resolveData = this.route.snapshot.data['role_list_organization_orgid'];
        this.parent.id = this.route.snapshot.parent.params['orgId'];
        this.parent.typeId = 2;
    }
    this.firstUrlPart = 'role/detail';
    this.secondUrlPart = this.parent.typeId === 3 ? 'system' : `${this.parent.type}/${this.parent.id}`;

    this.getAll(this.resolveData);
  }

  initCreateForm(): void {
    this.formGroup = this.fb.group({
      name: ['', Validators.required],
      description: ['', [Validators.required, Validators.minLength(0), Validators.maxLength(100)]],
      claims: [['']],
    });
  }

  createRole(item: RoleViewModel): void {
    item.type = this.parent.typeId;
    item.organizationId = item.type !== 3 ? this.parent.id : -1;
    this.postObservable = this.itemService.ApiRolePost(item);
    this.create();
  }
}
