﻿import { NgModule } from '@angular/core';

import { RoleComponent, RoleInfoComponent, RolesComponent } from './components';
import { SharedModule } from '../../../shared/shared.module';
import { roleRouting } from './role.routing';
import { RoleService } from '../../../api/services/role.service';

@NgModule({
  imports: [roleRouting, SharedModule],
  declarations: [RoleComponent, RolesComponent, RoleInfoComponent],
  providers: [RoleService],
})

export class RoleModule {
}
