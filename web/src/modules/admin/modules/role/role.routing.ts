import { RoleInfoComponent, RolesComponent } from './components';
import { RouterModule, Routes } from '@angular/router';
import { RoleComponent } from './components/role.component';
import {
  ApiRoleCountryRolesByCountryIdGetResolve,
  ApiRoleDetailsByRoleIdGetResolve,
  ApiRoleGetOrgRolesByOrgIdGetResolve,
  ApiRoleGlobalRolesGetResolve
} from '../../../shared/modules/resolve/role';

const ROUTES: Routes = [
  {
    path: '',
    component: RoleComponent,
    children: [
      {
        path: 'list/system',
        component: RolesComponent,
        data: { title: 'System roles', accessGuardClaims: ['default'] },
        resolve: { role_list_system: ApiRoleGlobalRolesGetResolve }
      },
      {
        path: 'list/country/:countryId',
        component: RolesComponent,
        data: { title: 'Country roles', accessGuardClaims: ['default'] },
        resolve: { role_list_country_id: ApiRoleCountryRolesByCountryIdGetResolve }
      },
      {
        path: 'list/organization/:orgId',
        component: RolesComponent,
        data: { title: 'Organization roles', accessGuardClaims: ['default'] },
        resolve: { role_list_organization_orgid: ApiRoleGetOrgRolesByOrgIdGetResolve }
      },
      {
        path: 'detail/:roleId/system',
        component: RoleInfoComponent,
        data: { title: 'System role detail', accessGuardClaims: ['default'] },
        resolve: { role_detail_roleid_system: ApiRoleDetailsByRoleIdGetResolve }
      },
      {
        path: 'detail/:roleId/country/:id',
        component: RoleInfoComponent,
        data: { title: 'Country role detail', accessGuardClaims: ['default'] },
        resolve: { role_detail_roleid_country_id: ApiRoleDetailsByRoleIdGetResolve }
      },
      {
        path: 'detail/:roleId/organization/:orgId',
        component: RoleInfoComponent,
        data: { title: 'Organization role detail', accessGuardClaims: ['default'] },
        resolve: { role_detail_roleid_organization_orgid: ApiRoleDetailsByRoleIdGetResolve }
      },
    ]
  }
];

export const roleRouting = RouterModule.forChild(ROUTES);