﻿import { NgModule } from '@angular/core';

import { OrganizationComponent, OrganizationInfoComponent, OrganizationsComponent } from './components';
import { SharedModule } from '../../../shared/shared.module';
import { organizationRouting } from './organization.routing';
import { OrganizationService } from '../../../api/services/organization.service';

@NgModule({
  imports: [organizationRouting, SharedModule],
  declarations: [OrganizationComponent, OrganizationsComponent, OrganizationInfoComponent],
  providers: [OrganizationService]
})

export class OrganizationModule {
}
