﻿import { RouterModule, Routes } from '@angular/router';
import { OrganizationComponent, OrganizationInfoComponent, OrganizationsComponent } from './components';
import { ApiCountryGetCountryOrganizationsByCountryIdGetResolve } from '../../../shared/modules/resolve/country';
import {
  ApiOrganizationDetailByOrgIdGetResolve,
  ApiOrganizationGetResolve
} from '../../../shared/modules/resolve/organization';

const ROUTES: Routes = [
  {
    path: '',
    component: OrganizationComponent,
    children: [
      {
        path: 'list/country/:countryId',
        component: OrganizationsComponent,
        data: { title: 'Country organizations', accessGuardClaims: ['default'] },
        resolve: { organization_list_country_id: ApiCountryGetCountryOrganizationsByCountryIdGetResolve }
      },
      {
        path: 'list/parent-organization/:orgId',
        component: OrganizationsComponent,
        data: { title: 'Children organizations', accessGuardClaims: ['default'] },
        resolve: { organization_list_parentorganization_orgid: ApiOrganizationGetResolve }
      },
      {
        path: 'detail/:orgId/country/:countryId',
        component: OrganizationInfoComponent,
        data: { title: 'Country organization detail', accessGuardClaims: ['default'] },
        resolve: { organization_detail_orgid_country_countryid: ApiOrganizationDetailByOrgIdGetResolve }
      },
      {
        path: 'detail/:orgId/parent-organization/:orgId',
        component: OrganizationInfoComponent,
        data: { title: 'Child organization detail', accessGuardClaims: ['default'] },
        resolve: { organization_detail_orgid_parentorganization_orgid: ApiOrganizationDetailByOrgIdGetResolve }
      },
      {
        path: 'home/:orgId',
        component: OrganizationInfoComponent,
        data: { title: 'Organization home', accessGuardClaims: ['default'] },
        resolve: { organization_home_orgid: ApiOrganizationDetailByOrgIdGetResolve }
      },
    ]
  }
];

export const organizationRouting = RouterModule.forChild(ROUTES);
