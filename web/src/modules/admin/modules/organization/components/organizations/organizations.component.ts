import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseComponent } from '../../../../../shared/components';
import { OrganizationService } from '../../../../../api/services/organization.service';
import { RouteSegment } from '../../../../../shared/services';
import { OrganizationRegistrationViewModel } from '../../../../../api/models/organization-registration-view-model';
import '../../../../../../extensions/rxjs-extensions';

@Component({
  selector: 'kira-organizations',
  templateUrl: './organizations.component.html'
})
export class OrganizationsComponent extends BaseComponent {

  item: OrganizationRegistrationViewModel;

  orgType: string;
  parentId: string;

  constructor(private _router: Router,
              private _route: ActivatedRoute,
              private _itemService: OrganizationService,
              private fb: FormBuilder,
              private segment: RouteSegment) {
    super();
    this.itemService = _itemService;
    this.router = _router;
    this.route = _route;
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngOnInit() {
    this.redirect = true;
    this.itemName = 'Organization';
    this.pageSize = 2;
    this.firstUrlPart = `organization/detail`;
    this.orgType = this.segment.path(2);

    if (this.orgType == 'country') {
      // Country organizations
      this.parentId = this.route.snapshot.params['countryId'];
      this.resolveData = this.route.snapshot.data['organization_list_country_id'];
    } else {
      // Children organizations under a super organization
      this.parentId = this.route.snapshot.params['orgId'];
      this.resolveData = this.route.snapshot.data['organization_list_parentorganization_orgid'].filter(o => o.parentOrganizationId == this.parentId);
    }

    this.secondUrlPart = `${this.orgType}/${this.parentId}`;
    this.getAll(this.resolveData);
  }

  initCreateForm(): void {
    this.formGroup = this.fb.group({
      firstName: ['', [Validators.required, Validators.maxLength(100)]],
      secondName: ['', [Validators.required, Validators.maxLength(25)]],
      nationalId: ['', Validators.required],
      email: ['', Validators.required],
      phoneNumber: ['', Validators.required],
    });
  }
}
