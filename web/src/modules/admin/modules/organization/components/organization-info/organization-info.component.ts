import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { BaseComponent } from '../../../../../shared/components';
import { OrganizationService } from '../../../../../api/services/organization.service';
import { RouteSegment } from '../../../../../shared/services';
import { OrganizationViewModel } from '../../../../../api/models/organization-view-model';

@Component({
  selector: 'kira-organization-info',
  templateUrl: './organization-info.component.html'
})
export class OrganizationInfoComponent extends BaseComponent {

  item: OrganizationViewModel;
  orgType: string;
  parentId: string;

  constructor(private _route: ActivatedRoute,
              private _router: Router,
              private _itemService: OrganizationService,
              private fb: FormBuilder,
              private segment: RouteSegment) {
    super();
    this.itemService = _itemService;
    this.route = _route;
    this.router = _router;
    this.redirectUponDelete = 'system/countries';
    this.redirect = true;
    //
    this.itemName = 'Organization';
    this.is_details = this.segment.path(1) === 'detail';

    this.orgType = 'None, we are on home!';

    if (this.is_details) {
      this.orgType = this.segment.path(3);
      if (this.orgType == 'country') {
        this.itemId = this.route.snapshot.params['orgId'];
        this.parentId = this.route.snapshot.params['countryId'];
        this.redirectUponDelete = `organization/list/country/${this.parentId}`;
        this.resolveData = this.route.snapshot.data['organization_detail_orgid_country_countryid'];
      }
      else {
        this.itemId = this.segment.path(2); //this.route.snapshot.params['orgId'];
        this.parentId = this.segment.path(4);
        this.redirectUponDelete = `organization/list/parent-organization/${this.parentId}`;
        this.resolveData = this.route.snapshot.data['organization_detail_orgid_parentorganization_orgid'];
      }
    } else {
      this.itemId = this.route.snapshot.params['orgId'];
      this.resolveData = this.route.snapshot.data['organization_home_orgid'];
    }

    console.log('ITEM ID: ' + this.itemId + ', PARENT ID: ' + this.parentId + ', PARENT TYPE: ' + this.orgType);
    this.getItem(this.resolveData);

    this.deleteObservable = this.itemService.ApiOrganizationByIdDelete({ id: this.itemId });
    this.putObservable = this.itemService.ApiOrganizationByIdPut({ id: this.itemId, vModel: this.item });
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngOnInit(): void {
    console.log('this is it: ', this.itemId);
    // this.getItem(+this.itemId)
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngAfterViewInit(): void {
    this.alertMessageTrigger();
  }

  initEditForm(item: OrganizationViewModel): void {
    this.formGroup = this.fb.group({
      name: [this.item.name, [Validators.required, Validators.maxLength(100)]],
      shortName: [this.item.shortName, [Validators.required, Validators.maxLength(25)]],
      nationalId: [this.item.nationalId, Validators.required],
    });
  }
}
