"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(require("./organization.component"));
__export(require("./organizations/organizations.component"));
__export(require("./organization-info/organization-info.component"));
//# sourceMappingURL=index.js.map