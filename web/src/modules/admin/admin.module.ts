﻿import { NgModule } from '@angular/core';
import { AdminComponent } from './components';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../shared/guards';
import { SharedModule } from '../shared/shared.module';

const ROUTES: Routes = [
  {
    path: '',
    canActivate: [AuthGuard],
    component: AdminComponent,
    children: [
      { path: 'system', loadChildren: './modules/system/system.module#SystemModule' },
      { path: 'country', loadChildren: './modules/country/country.module#CountryModule' },
      { path: 'organization', loadChildren: './modules/organization/organization.module#OrganizationModule' },
      { path: 'role', loadChildren: '././modules/role/role.module#RoleModule' },
      { path: 'user', loadChildren: './modules/user/user.module#UserModule' },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(ROUTES), SharedModule],
  declarations: [AdminComponent],
})

export class AdminModule {
}
