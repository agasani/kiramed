import { Component } from '@angular/core';
import { BaseComponent } from '../../shared/components';

@Component({
  selector: 'kira-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['../sass/admin.scss']
})

export class AdminComponent extends BaseComponent {

  constructor() {
    super()
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.sideBarMenuTrigger();
  }
}
