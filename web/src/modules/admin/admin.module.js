"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length,
        r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", {value: true});
var core_1 = require("@angular/core");
var components_1 = require("./components");
var router_1 = require("@angular/router");
var guards_1 = require("../shared/guards");
var shared_module_1 = require("../shared/shared.module");
var ROUTES = [
    {
        path: '',
        canActivate: [guards_1.AuthGuard],
        component: components_1.AdminComponent,
        children: [
            {path: 'system', loadChildren: './modules/system/system.module#SystemModule'},
            {path: 'country', loadChildren: './modules/country/country.module#CountryModule'},
            {path: 'organization', loadChildren: './modules/organization/organization.module#OrganizationModule'},
            {path: 'role', loadChildren: '././modules/role/role.module#RoleModule'},
            {path: 'user', loadChildren: './modules/user/user.module#UserModule'},
        ]
    }
];
var AdminModule = /** @class */ (function () {
    function AdminModule() {
    }

    AdminModule = __decorate([
        core_1.NgModule({
            imports: [router_1.RouterModule.forChild(ROUTES), shared_module_1.SharedModule],
            declarations: [components_1.AdminComponent],
        })
    ], AdminModule);
    return AdminModule;
}());
exports.AdminModule = AdminModule;
//# sourceMappingURL=admin.module.js.map