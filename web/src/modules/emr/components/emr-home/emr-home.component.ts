import { Component } from '@angular/core';
import { AuthService } from '../../../shared/services';
import { Router } from '@angular/router';
import { BaseComponent } from '../../../shared/components';

@Component({
  selector: 'kira-emr-home',
  templateUrl: './emr-home.component.html'
})
export class EmrHomeComponent extends BaseComponent {

  isClinical: boolean = false;

  constructor(private auth: AuthService, router: Router) {
    super();
    this.router = router;
  }

  ngOnInit() {
    // is.admin.sys > is.admin.ctr > is.admin.org > is.clinical > is.pharma > is.adm.stk > is.labtech > is.biller > is.acct > is.insurer > is.pat
    if (!!this.userClaims) {
      const s = new Set(this.userClaims);
      if (s.has('is.admin.sys')) this.router.navigate(['/system']);
      else if (s.has('is.admin.ctr')) this.router.navigate(['/country', this.userCountryId]);
      else if (s.has('is.admin.org')) this.router.navigate(['/organization', this.userOrgId]);
      else if (s.has('is.clinical')) this.isClinical = true;
    } else this.auth.logout();
  }
}
