import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'kira-emr-admin',
  templateUrl: './emr-admin.component.html',
  styleUrls: ['./emr-admin.component.scss']
})
export class EmrAdminComponent implements OnInit {
  public pageTitle: string;

  constructor() {
  }

  ngOnInit() {
    this.pageTitle = 'EMR Page for Doctors!';
    console.log('Admin comp!');
  }

}
