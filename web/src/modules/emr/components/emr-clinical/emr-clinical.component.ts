import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'kira-emr-clinical',
  templateUrl: './emr-clinical.component.html',
  styleUrls: ['./emr-clinical.component.scss']
})
export class EmrClinicalComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
    console.log('Clinical comp!');
  }

}
