import { Component } from '@angular/core';
import { BaseComponent } from '../../shared/components';
import { PageHeaderService } from '../../shared/services/page-header.service';

@Component({
  selector: 'kira-emr',
  templateUrl: './emr.component.html'
})
export class EmrComponent extends BaseComponent {
  constructor(headerService: PageHeaderService) {
    super();
    this.header = headerService;
  }

  ngAfterViewInit() {
    this.sideBarMenuTrigger();
    this.setHeader({ type: 1, content: 'EMR Home page!' });
  }
}
