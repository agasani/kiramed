export * from './emr.component';
export * from './emr-admin/emr-admin.component';
export * from './emr-clinical/emr-clinical.component';
export * from './emr-home/emr-home.component';
export * from './patient-search/patient-search.component';
