﻿import { Component } from '@angular/core';
import { BaseComponent } from '../../../shared/components';
import { ApiConfiguration } from '../../../api/api-configuration';
import { PatientSearchViewModel } from '../../../api/models/patient-search-view-model';

@Component({
  selector: 'kira-patient-search',
  templateUrl: './patient-search.component.html',
  styleUrls: ['../../../clinical/sass/clinical.scss'],
})

export class PatientSearchComponent extends BaseComponent {

  constructor(private config: ApiConfiguration) {
    super();
  }

  ngAfterViewInit() {
    const settingsUrl: string = `${this.config.rootUrl}/api/patients/PatientSearch/{$query}`;
    const apiSettings: SemanticUI.ApiSettings = {
      url: settingsUrl,
      onResponse: function (serverResponse: PatientSearchViewModel[] | null) {
        // Abort on null result
        if (!serverResponse) return;

        const response: any = {};
        response.items = [];
        $.each(serverResponse, (index: any, resp: PatientSearchViewModel) => {

          // Limit to 8 patients display on search
          const maxResults = 8;
          if (index >= maxResults) return false;
          // Add and reformat the response object
          response.items.push({
            name: `${resp.firstName} ${resp.lastName}`,
            html_url: `#/patient-view/${resp.id}`,
            description: resp.dob
          })
        });
        return response;
      }
    };
    const searchFields: any = {
      results: 'items',
      title: 'name',
      url: 'html_url'
    };
    const searchSettings: SemanticUI.SearchSettings = {
      fields: searchFields,
      minCharacters: 3,
      apiSettings: apiSettings
    };
    $('.ui.search').search(searchSettings);
  }
}
