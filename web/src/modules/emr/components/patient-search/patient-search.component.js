"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({__proto__: []} instanceof Array && function (d, b) {
            d.__proto__ = b;
        }) ||
        function (d, b) {
            for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
        };
    return function (d, b) {
        extendStatics(d, b);

        function __() {
            this.constructor = d;
        }

        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length,
        r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", {value: true});
var core_1 = require("@angular/core");
var components_1 = require("../../../shared/components");
var PatientSearchComponent = /** @class */ (function (_super) {
    __extends(PatientSearchComponent, _super);

    function PatientSearchComponent(config) {
        var _this = _super.call(this) || this;
        _this.config = config;
        return _this;
    }

    PatientSearchComponent.prototype.ngAfterViewInit = function () {
        var settingsUrl = this.config.rootUrl + "/api/patients/PatientSearch/{$query}";
        var apiSettings = {
            url: settingsUrl,
            onResponse: function (serverResponse) {
                // Abort on null result
                if (!serverResponse)
                    return;
                var response = {};
                response.items = [];
                $.each(serverResponse, function (index, resp) {
                    // Limit to 8 patients display on search
                    var maxResults = 8;
                    if (index >= maxResults)
                        return false;
                    // Add and reformat the response object
                    response.items.push({
                        name: resp.firstName + " " + resp.lastName,
                        html_url: "#/patient-view/" + resp.id,
                        description: resp.dob
                    });
                });
                return response;
            }
        };
        var searchFields = {
            results: 'items',
            title: 'name',
            url: 'html_url'
        };
        var searchSettings = {
            fields: searchFields,
            minCharacters: 3,
            apiSettings: apiSettings
        };
        $('.ui.search').search(searchSettings);
    };
    PatientSearchComponent = __decorate([
        core_1.Component({
            selector: 'kira-patient-search',
            templateUrl: './patient-search.component.html',
            styleUrls: ['../../../clinical/sass/clinical.scss'],
        })
    ], PatientSearchComponent);
    return PatientSearchComponent;
}(components_1.BaseComponent));
exports.PatientSearchComponent = PatientSearchComponent;
//# sourceMappingURL=patient-search.component.js.map