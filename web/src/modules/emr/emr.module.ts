import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import {
  EmrAdminComponent,
  EmrClinicalComponent,
  EmrComponent,
  EmrHomeComponent,
  PatientSearchComponent
} from './components';
import { AuthGuard } from '../shared/guards';

const ROUTES: Routes = [{
  path: '',
  canActivate: [AuthGuard],
  component: EmrComponent,
  children: [
    { path: 'emr', component: EmrHomeComponent, data: { title: 'Electronic Medical Record Home' } },
    { path: 'patient-create', loadChildren: './modules/patient-create/patient-create.module#PatientCreateModule' }
  ],
}
];

@NgModule({
  imports: [RouterModule.forChild(ROUTES), SharedModule],
  declarations: [EmrComponent, EmrHomeComponent, EmrAdminComponent, EmrClinicalComponent, PatientSearchComponent]
})
export class EmrModule {
}
