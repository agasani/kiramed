export const STEPS = {
  personal: 'personal',
  demographic: 'demographic',
  contact: 'contact',
  address: 'address',
  result: 'result'
};
