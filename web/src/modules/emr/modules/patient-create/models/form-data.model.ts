import { PatientRegistrationViewModel } from '../../../../api/models/patient-registration-view-model';
import { EntityDemographicViewModel } from '../../../../api/models/entity-demographic-view-model';
import { EntityContactViewModel } from '../../../../api/models/entity-contact-view-model';
import { EntityAddressViewModel } from '../../../../api/models/entity-address-view-model';

export class Personal {
  // Required
  countryId = 0;
  nationalPatientId = 'XXXXXXXXXXXX'; // [Required][StringLength(12)]
  medicalRecordNumber = 'XXXXXXXXXX'; //[Required][StringLength(10)]
  parentId = 0;
  firstName = '';
  secondName = '';
  nationalId = '';
  email = '';
  phoneNumber = '';
  // Optional
  deathDate = '';
  tracknetNumber = '';
  allergyNote = '';
  nonClinicalNote = '';
  acceptedHIE = false;
  noKnownAllergy = false;
  noKnownMed = false;
  forceCreate = false;
  middleName = '';
}

export class Demographic implements EntityDemographicViewModel {
  dob = (new Date()).toISOString();
  id = 0;
  secondaryLanguage = '';
  primaryLanguage = '';
  nationalID = '';
  entityId = 0;
  sex = '';
  martalStatus = '';
  religion = '';
  education = '';
  income = 0;
}

export class Contact implements EntityContactViewModel {
  id = 0;
  contactTypeId = 0;
  value = '';
  label = '';
  beginDate = (new Date()).toISOString();
  endDate = '';
  deactivate = false;
}

export class Address implements EntityAddressViewModel {
  timeZoneId = 0;
  id = 0;
  directions = '';
  address1 = '';
  address2 = '';
  city = '';
  state = '';
  location = '';
  timeZone = 0;
  label = '';
  hasDaylightSaving = false;
  zip = '';
  district = '';
  isMailingAddress = false;
  country = '';
  beginDate = (new Date()).toISOString();
  endDate = '';
  deactivate = false;
}

export class FormData extends Personal implements PatientRegistrationViewModel {

  demographic = new Demographic;
  contacts = [new Contact];
  addresses = [new Address];

  clear() {
    // PERSONAL
    this.tracknetNumber = '';
    this.deathDate = '';
    this.allergyNote = '';
    this.nonClinicalNote = '';
    this.acceptedHIE = false;
    this.noKnownAllergy = false;
    this.noKnownMed = false;
    this.firstName = '';
    this.secondName = '';
    this.nationalId = '';
    this.email = '';
    this.phoneNumber = '';
    this.middleName = '';

    // DEMOGRAPHIC
    this.demographic.secondaryLanguage = '';
    this.demographic.primaryLanguage = '';
    this.demographic.nationalID = this.nationalId;
    this.demographic.dob = (new Date()).toISOString();
    this.demographic.sex = '';
    this.demographic.martalStatus = '';
    this.demographic.religion = '';
    this.demographic.education = '';
    this.demographic.income = 0;
    // CONTACT
    this.contacts.forEach((contact: Contact): void => {
      contact.value = '';
      contact.label = '';
      contact.endDate = '';
    });
    // ADDRESS
    this.addresses.forEach((address: Address): void => {
      address.label = '';
      address.directions = '';
      address.address1 = '';
      address.address2 = '';
      address.city = '';
      address.state = '';
      address.location = '';
      address.zip = '';
      address.district = '';
      address.country = '';
      address.endDate = '';
    });
  }
}
