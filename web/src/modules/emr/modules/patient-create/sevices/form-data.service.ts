import { Injectable } from '@angular/core';
import { Address, Contact, Demographic, FormData, Personal, STEPS } from '../models';
import { WorkflowService } from './workflow.service';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';

@Injectable()
export class FormDataService {

  public formData: FormData;
  public isPersonalFormValid: boolean;
  public isDemographicFormValid: boolean;
  public isContactFormValid: boolean;
  public isAddressFormValid: boolean;

  constructor(private workflowService: WorkflowService, private fb: FormBuilder) {
    this.formData = new FormData();
    this.isPersonalFormValid = false;
    this.isDemographicFormValid = false;
    this.isContactFormValid = false;
    this.isAddressFormValid = false;
  }

  getPersonal(): FormGroup {
    // Return the Personal data
    return this.fb.group({
      deathDate: this.formData.deathDate,
      allergyNote: this.formData.allergyNote,
      nonClinicalNote: this.formData.nonClinicalNote,
      acceptedHIE: [this.formData.acceptedHIE, Validators.required],
      noKnownAllergy: [this.formData.noKnownAllergy, Validators.required],
      noKnownMed: [this.formData.noKnownMed, Validators.required],
      firstName: [this.formData.firstName, Validators.required],
      secondName: [this.formData.secondName, Validators.required],
      nationalId: [this.formData.nationalId, Validators.required],
      email: [this.formData.email, Validators.required],
      phoneNumber: [this.formData.phoneNumber, Validators.required],
      middleName: this.formData.middleName,
    });
  }

  setPersonal(data: Personal) {
    // Update the Personal data only when the Personal Form had been validated successfully
    this.isPersonalFormValid = true;

    this.formData.deathDate = data.deathDate;
    this.formData.allergyNote = data.allergyNote;
    this.formData.medicalRecordNumber = data.medicalRecordNumber;///
    this.formData.tracknetNumber = data.tracknetNumber;///
    this.formData.nonClinicalNote = data.nonClinicalNote;
    this.formData.acceptedHIE = data.acceptedHIE;
    this.formData.noKnownAllergy = data.noKnownAllergy;
    this.formData.noKnownMed = data.noKnownMed;
    this.formData.nationalPatientId = data.nationalPatientId;///
    this.formData.forceCreate = data.forceCreate;///
    this.formData.countryId = data.countryId;///
    this.formData.firstName = data.firstName;
    this.formData.secondName = data.secondName;
    this.formData.nationalId = data.nationalId;
    this.formData.parentId = data.parentId;///
    this.formData.email = data.email;
    this.formData.phoneNumber = data.phoneNumber;
    this.formData.middleName = data.middleName;

    // Validate Personal Step in Workflow
    this.workflowService.validateStep(STEPS.personal);
  }

  getDemographic(): FormGroup {
    // Return the demographic type
    return this.fb.group({
      secondaryLanguage: this.formData.demographic.secondaryLanguage,
      primaryLanguage: this.formData.demographic.primaryLanguage,
      dOB: [this.formData.demographic.dob, Validators.required],
      sex: this.formData.demographic.sex, // Gender required
      martalStatus: this.formData.demographic.martalStatus,
      religion: this.formData.demographic.religion,
      education: this.formData.demographic.education,
      income: [this.formData.demographic.income, Validators.required], //Income not required
    });
  }

  setDemographic(data: Demographic) {
    // Update the demographic type only when the Demographic Form had been validated successfully
    this.isDemographicFormValid = true;

    this.formData.demographic.entityId = data.entityId;///
    this.formData.demographic.secondaryLanguage = data.secondaryLanguage;
    this.formData.demographic.primaryLanguage = data.primaryLanguage;
    this.formData.demographic.nationalID = this.formData.nationalId;
    this.formData.demographic.dob = data.dob;
    this.formData.demographic.sex = data.sex;
    this.formData.demographic.martalStatus = data.martalStatus;
    this.formData.demographic.religion = data.religion;
    this.formData.demographic.education = data.education;
    this.formData.demographic.income = data.income;

    // Validate Work Step in Workflow
    this.workflowService.validateStep(STEPS.demographic);
  }

  getContacts(): FormArray {
    // Return the Contacts data
    const contacts: FormArray = this.fb.array([]);
    for (let i = 0; i < this.formData.contacts.length; i++) {
      const item = this.formData.contacts[i];
      const contact: FormGroup = this.fb.group({
        value: item.value,
        label: item.label,
        endDate: item.endDate,
      });
      contacts.push(contact);
    }
    return contacts;
  }

  setContacts(data: Contact[]) {
    // Update the contact type only when the Contact Form had been validated successfully
    this.isContactFormValid = true;

    this.formData.contacts = data;

    // Validate Contact Step in Workflow
    this.workflowService.validateStep(STEPS.contact);
  }

  getAddresses(): FormArray {
    // Return the Address data
    const addresses: FormArray = this.fb.array([]);
    for (let i = 0; i < this.formData.addresses.length; i++) {
      const item = this.formData.addresses[i];
      const address: FormGroup = this.fb.group({
        label: item.label,
        directions: item.directions,
        address1: item.address1,
        address2: item.address2,
        city: item.city,
        state: item.state,
        location: item.location,
        zip: item.zip,
        district: item.district,
        country: [item.country, Validators.required],
        endDate: item.endDate,
      });
      addresses.push(address);
    }
    return addresses;
  }

  setAddresses(data: Address[]) {
    // Update the Address data only when the Address Form had been validated successfully
    this.isAddressFormValid = true;

    this.formData.addresses = data;

    // Validate Address Step in Workflow
    this.workflowService.validateStep(STEPS.address);
  }

  getFormData(): FormData {
    // Return the entire Form Data
    return this.formData;
  }

  resetFormData(): FormData {
    // Reset the workflow
    this.workflowService.resetSteps();

    // Return the form data after all this.* members had been reset
    this.formData.clear();

    this.isPersonalFormValid
        = this.isDemographicFormValid
        = this.isContactFormValid
        = this.isAddressFormValid
        = false;

    return this.formData;
  }

  isFormValid() {
    // Return true if all forms had been validated successfully; otherwise, return false
    return this.isPersonalFormValid
        && this.isDemographicFormValid
        && this.isContactFormValid
        && this.isAddressFormValid;
  }

}
