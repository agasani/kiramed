import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {
  AddressComponent,
  ContactComponent,
  DemographicComponent,
  PatientCreateComponent,
  PersonalComponent,
  ResultComponent
} from './components';
import { SharedModule } from '../../../shared/shared.module';
import { FormDataService, WorkflowGuard, WorkflowService } from './sevices';

const ROUTES: Routes = [
  {
    path: '',
    component: PatientCreateComponent,
    children: [
      { path: '', redirectTo: 'personal', pathMatch: 'full' },
      { path: 'personal', component: PersonalComponent },
      { path: 'demographic', component: DemographicComponent, canActivate: [WorkflowGuard] },
      { path: 'contact', component: ContactComponent, canActivate: [WorkflowGuard] },
      { path: 'address', component: AddressComponent, canActivate: [WorkflowGuard] },
      { path: 'result', component: ResultComponent, canActivate: [WorkflowGuard] },
      { path: '**', component: PersonalComponent }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(ROUTES), SharedModule],
  declarations: [
    PatientCreateComponent,
    AddressComponent,
    ContactComponent,
    DemographicComponent,
    PersonalComponent,
    ResultComponent,
  ],
  providers: [
    WorkflowGuard,
    { provide: FormDataService, useClass: FormDataService },
    { provide: WorkflowService, useClass: WorkflowService }
  ]
})

export class PatientCreateModule {
}
