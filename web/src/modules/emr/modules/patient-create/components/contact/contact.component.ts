import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormDataService } from '../../sevices/form-data.service';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { SharedService } from '../../../../../shared/services/shared-service';
import { Contact } from '../../models';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'kira-create-clinical-patient-info-contact',
  templateUrl: './contact.component.html'
})
export class ContactComponent implements OnInit {

  title = 'Patient contact information';
  baseUrl: string = '/patient-create';
  contactsArray: FormArray;
  contactForm: FormGroup;

  constructor(private router: Router, private formDataService: FormDataService, private fb: FormBuilder, private _sharedService: SharedService) {
  }

  ngOnInit() {
    this.contactForm = this.fb.group({ 'contacts': this.formDataService.getContacts() });
    this.contactsArray = <FormArray>this.contactForm.controls['contacts'];
    console.log('Contact form loaded!');
  }

  addContact(): void {
    this.contactsArray.push(this.fb.group({
      value: '',
      label: '',
      endDate: ''
    }))
  }

  removeContact(i: number): void {
    this.contactsArray.removeAt(i);
  }

  save(form: any): boolean {
    if (!form.valid) {
      return false;
    }
    this._sharedService.emitChange(3);

    let data: Contact[] = this.contactsArray.value;
    for (let i = 0; i < data.length; i++) {
      data[i].beginDate = (new Date()).toISOString();
      data[i].endDate = '';
      data[i].deactivate = false;
      switch (data[i].label) {
        case 'phone':
          data[i].contactTypeId = 1;
          break;

        case 'email':
          data[i].contactTypeId = 2;
          break;

        case 'twitter':
          data[i].contactTypeId = 3;
          break;

        default:
          data[i].contactTypeId = null;
      }
    }

    this.formDataService.setContacts(data);

    return true;
  }

  goToPrevious() {
    // Navigate to the demographic page
    this.router.navigate([`${this.baseUrl}/demographic`]);
  }

  goToNext(form: any) {
    if (this.save(form)) {
      // Navigate to the address page
      this.router.navigate([`${this.baseUrl}/address`]);
    }
  }

}
