import { Component, OnInit } from '@angular/core';
import { FormDataService } from '../../sevices/form-data.service';
import { Router } from '@angular/router';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SharedService } from '../../../../../shared/services/shared-service';
import { Address } from '../../models';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'kira-create-clinical-patient-info-address',
  templateUrl: './address.component.html'
})
export class AddressComponent implements OnInit {

  title = 'Patient address information';
  baseUrl: string = '/patient-create';
  addressesArray: FormArray;
  addressForm: FormGroup;

  constructor(private router: Router, private formDataService: FormDataService, private fb: FormBuilder, private _sharedService: SharedService) {
  }

  ngOnInit() {
    this.addressForm = this.fb.group({ 'addresses': this.formDataService.getAddresses() });
    this.addressesArray = <FormArray>this.addressForm.controls['addresses'];
    console.log('Address form loaded!');
  }

  addAddress(): void {
    this.addressesArray.push(this.fb.group({
      label: '',
      directions: '',
      address1: '',
      address2: '',
      city: '',
      state: '',
      location: '',
      zip: '',
      district: '',
      country: ['', Validators.required],
      endDate: '',
    }));
  }

  removeAddress(i: number): void {
    this.addressesArray.removeAt(i);
  }

  save(form: any): boolean {
    if (!form.valid) {
      return false;
    }
    this._sharedService.emitChange(4);

    let data: Address[] = this.addressesArray.value;
    for (let i = 0; i < data.length; i++) {
      data[i].timeZone = 1;
      data[i].timeZoneId = 1;
      data[i].hasDaylightSaving = false;
      data[i].isMailingAddress = true;
      data[i].beginDate = (new Date()).toISOString();
      data[i].endDate = '';
      data[i].deactivate = false;
    }

    this.formDataService.setAddresses(data);

    return true;
  }

  goToPrevious() {
    // Navigate to the contact page
    this.router.navigate([`${this.baseUrl}/contact`]);
  }

  goToNext(form: any) {
    if (this.save(form)) {
      // Navigate to the result page
      this.router.navigate([`${this.baseUrl}/result`]);
    }
  }
}
