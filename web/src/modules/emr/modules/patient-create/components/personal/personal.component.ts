import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup } from '@angular/forms';
import { Personal } from '../../models';
import { SharedService } from '../../../../../shared/services';
import { FormDataService } from '../../sevices';
import { randomIntFromInterval } from '../../../../../../helpers';

@Component({
  selector: 'kira-create-clinical-patient-info-personal',
  templateUrl: './personal.component.html'
})
export class PersonalComponent implements OnInit, AfterViewInit {
  title = 'Patient personal information.';
  personalForm: FormGroup;
  controls: any;
  baseUrl: string = '/patient-create';

  constructor(private router: Router, private formDataService: FormDataService, private _sharedService: SharedService) {
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngOnInit() {
    this.personalForm = this.formDataService.getPersonal();
    this.controls = this.personalForm.controls;
    console.log('Personal form loaded!');
  }

  ngAfterViewInit() {
    $('.ui.checkbox').checkbox();
  }

  save(form: any): boolean {
    if (!form.valid) {
      return false;
    }
    this._sharedService.emitChange(1);

    let data: Personal = this.personalForm.value;
    data.deathDate = null;
    data.medicalRecordNumber = '';
    data.tracknetNumber = null;
    data.nationalPatientId = '';
    data.forceCreate = false;
    data.countryId = randomIntFromInterval(1, 3);
    data.parentId = randomIntFromInterval(1, 3);

    this.formDataService.setPersonal(data);
    return true;
  }

  goToNext(form: any) {
    if (this.save(form)) {
      // Navigate to the demographic page
      this.router.navigate([`${this.baseUrl}/demographic`]);
    }
  }
}
