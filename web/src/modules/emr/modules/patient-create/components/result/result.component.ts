import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { FormDataService } from '../../sevices';
import { FormData } from '../../models';
import { BaseComponent } from '../../../../../shared/components';
import { PatientService } from '../../../../../api/services/patient.service';

@Component({
  selector: 'kira-create-clinical-patient-info-result',
  templateUrl: './result.component.html'
})
export class ResultComponent extends BaseComponent {

  title = 'Patient information ready to be submitted';
  @Input() formData: FormData;
  isFormValid: boolean = false;

  constructor(private formDataService: FormDataService,
              private _router: Router,
              private _itemService: PatientService) {
    super();
    this.itemService = _itemService;
    this.router = _router;
    this.firstUrlPart = '/patient-view/';
    this.redirect = true;
  }

  ngOnInit() {
    this.formData = this.formDataService.getFormData();

    this.isFormValid = this.formDataService.isFormValid();
    console.log('Validated: ', this.isFormValid);

    console.log('Result feature loaded!');
  }

  submit() {
    this.postObservable = this.itemService.ApiPatientPost(this.formData);
    this.create();
    this.formData = this.formDataService.resetFormData();
    this.isFormValid = false;
  }
}
