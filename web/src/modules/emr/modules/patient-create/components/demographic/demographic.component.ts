import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormDataService } from '../../sevices/form-data.service';
import { FormGroup } from '@angular/forms';
import { SharedService } from '../../../../../shared/services/shared-service';
import { Demographic } from '../../models';
import { randomIntFromInterval } from '../../../../../../helpers';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'kira-create-clinical-patient-info-demographic',
  templateUrl: './demographic.component.html'
})

// DATEPICKER
// https://codepen.io/SaadRegal/pen/ZOABQr
// https://jsbin.com/hubanufuva/1/edit?html,js,output
export class DemographicComponent implements OnInit, AfterViewInit {

  title = 'Patient demographic information';
  demographicForm: FormGroup;
  controls: any;
  baseUrl: string = '/patient-create';

  constructor(private router: Router, private formDataService: FormDataService, private _sharedService: SharedService) {
  }

  ngOnInit() {
    this.demographicForm = this.formDataService.getDemographic();
    this.controls = this.demographicForm.controls;
    console.log('Demographic form loaded!');
  }

  ngAfterViewInit() {
    $('.ui.dropdown').dropdown();
  }

  save(form: any): boolean {
    if (!form.valid) {
      return false;
    }
    this._sharedService.emitChange(2);

    let data: Demographic = this.demographicForm.value;
    data.entityId = randomIntFromInterval(1, 3);

    this.formDataService.setDemographic(data);
    return true;
  }

  goToPrevious() {
    // Navigate to the personal page
    this.router.navigate([`${this.baseUrl}/personal`]);
  }

  goToNext(form: any) {
    if (this.save(form)) {
      // Navigate to the contact page
      this.router.navigate([`${this.baseUrl}/contact`]);
    }
  }

}
