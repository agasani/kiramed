import { Component, Input, OnInit } from '@angular/core';
import { FormData } from '../models';
import { FormDataService } from '../sevices';
import { Subscription } from 'rxjs/Subscription';
import { HasStepsComponent } from '../../../../shared/components';

@Component({
  selector: 'kira-clinical-patient-create',
  templateUrl: './patient-create.component.html'
})

export class PatientCreateComponent extends HasStepsComponent implements OnInit {
  @Input() formData: FormData;
  baseUrl: string = '/patient-create';
  subscription: Subscription;
  checkable: boolean;
  private title = 'Clinical patient create form';

  constructor(private formDataService: FormDataService) {
    super();
    this.steps = [
      {
        'title': 'Personal',
        'descr': 'Personal Info',
        'icon': 'user',
        'url': `${this.baseUrl}/personal`,
        'active': true
      },
      {
        'title': 'Demographic',
        'descr': 'Demographic Info',
        'icon': 'pie chart',
        'url': `${this.baseUrl}/demographic`,
        'active': true
      },
      {
        'title': 'Contact',
        'descr': 'Contact Info',
        'icon': 'mail',
        'url': `${this.baseUrl}/contact`,
        'active': true
      },
      {
        'title': 'Address',
        'descr': 'Address Info',
        'icon': 'map',
        'url': `${this.baseUrl}/address`,
        'active': true
      },
    ];
    this.review = {
      'title': 'Result',
      'icon': 'unordered list',
      'url': `${this.baseUrl}/result`,
      'active': true
    };
    this.head = { 'title': 'Info steps', 'icon': 'ellipsis vertical' };
    this.nav = { 'steps': this.steps, 'review': this.review, 'head': this.head };
    this.checkable = true;
  }

  ngOnInit() {
    this.formData = this.formDataService.getFormData();
    console.log(`${this.title} loaded!`);
  }
}
