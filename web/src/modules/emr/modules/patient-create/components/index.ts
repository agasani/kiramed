export * from './patient-create.component'
export * from './result/result.component';
export * from './personal/personal.component';
export * from './demographic/demographic.component';
export * from './contact/contact.component';
export * from './address/address.component';
