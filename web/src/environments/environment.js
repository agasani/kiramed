"use strict";
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
exports.__esModule = true;
exports.environment = {
    production: false,
    debug: true,
    testing: true,
    urls: {
        authority: 'http://localhost:5000',
        host: 'http://localhost:8080',
        // apiServer: 'http://kiraservices.azurewebsites.net',
        // apiServer: 'http://api20171001054222.azurewebsites.net/'
        apiServer: 'http://localhost:5000',
        useHttps: false,
        authConfig: {
            authority: 'http://localhost:5000',
            tokenEndpoint: 'http://localhost:5000/connect/token',
            endSession: 'http://localhost:5000/connect/endsession',
            clientId: 'KiraWeb',
            audienceId: '',
            clientSecret: '564ht32io8#479ierye@578gk5te6gu9',
            grantType: 'password',
            scope: 'KirApi offline_access openid profile',
            redirect_url: 'http://localhost:8080/callback',
            response_type: 'id_token token',
            post_logout_redirect_uri: 'http://localhost:8080'
        }
    },
    httpErrors: {
        // HTTP Error codes doc:
        // https://tools.ietf.org/html/rfc7231#section-6.5.1
        0: { 'msg': 'Server is not available' },
        400: { 'msg': 'Bad request' },
        401: { 'msg': 'Not Authorized' },
        404: { 'msg': 'Page not Found' },
        500: { 'msg': 'Internal server error' }
    },
    idleTimeout: 45 * 60
};
