export const environment = {
  production: true,
  debug: false,
  testing: false,
  urls: {
    authority: '',
    host: '',
    apiServer: 'http://kiramed.azurewebsites.net/'
  },
  useHttps: false
};
