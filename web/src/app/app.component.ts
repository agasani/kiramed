import { Component } from '@angular/core';
import { LoaderService } from '../modules/shared/components/loader/loader.service';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { AutoUnsubscribe } from '../modules/shared/decorators/auto-unsubscribe';
import '../extensions/rxjs-extensions';
import { PageHeader, PageHeaderService } from '../modules/shared/services/page-header.service';

@AutoUnsubscribe()
@Component({
  selector: 'kira-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})

export class AppComponent {
  header: PageHeader;
  isLoading = false;

  constructor(private loaderService: LoaderService,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              private titleService: Title,
              private pageHeaderService: PageHeaderService) {
  }

  ngOnInit() {
    // Loader before page content is ready
    this.loaderService.loaderState
        .map(state => state.show)
        .subscribe(loading => this.isLoading = loading);

    // Page HTML Title set dynamically
    this.router.events
        .filter((event) => event instanceof NavigationEnd)
        .map(() => this.activatedRoute)
        .map((route) => {
          while (route.firstChild) route = route.firstChild;
          return route;
        })
        .filter((route) => route.outlet === 'primary')
        .mergeMap((route) => route.data)
        .subscribe((event) => this.titleService.setTitle(event['title']));

    // Individual page header set dynamically
    this.pageHeaderService.header.subscribe((val: PageHeader) => this.header = val);
  }

  ngOnDestroy() {
  }
}
