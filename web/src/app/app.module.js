"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length,
        r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", {value: true});
var platform_browser_1 = require("@angular/platform-browser");
var core_1 = require("@angular/core");
var app_component_1 = require("./app.component");
var kira_module_1 = require("../modules/kira/kira.module");
var clinical_module_1 = require("../modules/clinical/clinical.module");
var http_1 = require("@angular/common/http");
var admin_module_1 = require("../modules/admin/admin.module");
var patient_module_1 = require("../modules/patient/patient.module");
var app_routing_1 = require("./app.routing");
var angular2_jwt_1 = require("angular2-jwt");
var shared_module_1 = require("../modules/shared/shared.module");
var core_2 = require("@ng-idle/core");
var emr_module_1 = require("../modules/emr/emr.module");
var services_1 = require("../modules/shared/services");
var jwt_interceptor_1 = require("../interceptors/jwt.interceptor");
var api_url_interceptor_1 = require("../interceptors/api.url.interceptor");
var api_configuration_1 = require("../modules/api/api-configuration");
var api_module_1 = require("../modules/api/api.module");
var http_client_interceptor_1 = require("../interceptors/http.client.interceptor");
var AppModule = /** @class */ (function () {
    function AppModule() {
    }

    AppModule = __decorate([
        core_1.NgModule({
            imports: [
                platform_browser_1.BrowserModule,
                http_1.HttpClientModule,
                core_2.NgIdleModule.forRoot(),
                api_module_1.ApiModule,
                app_routing_1.appRouting,
                shared_module_1.SharedModule.forRoot(),
                kira_module_1.KiraModule,
                admin_module_1.AdminModule,
                emr_module_1.EmrModule,
                clinical_module_1.ClinicalModule,
                patient_module_1.PatientModule,
            ],
            declarations: [app_component_1.AppComponent],
            providers: [
                angular2_jwt_1.JwtHelper,
                api_configuration_1.ApiConfiguration,
                services_1.AuthConfiguration,
                [
                    {
                        provide: core_1.APP_INITIALIZER,
                        useFactory: services_1.initApiConfiguration,
                        deps: [api_configuration_1.ApiConfiguration],
                        multi: true
                    },
                    {
                        provide: http_1.HTTP_INTERCEPTORS,
                        useClass: http_client_interceptor_1.HttpClientInterceptor,
                        deps: [core_1.Injector],
                        multi: true
                    },
                    {
                        provide: http_1.HTTP_INTERCEPTORS,
                        useClass: jwt_interceptor_1.JwtInterceptor,
                        deps: [core_1.Injector],
                        multi: true
                    },
                    {
                        provide: http_1.HTTP_INTERCEPTORS,
                        useClass: api_url_interceptor_1.ApiUrlInterceptor,
                        deps: [core_1.Injector],
                        multi: true
                    },
                ],
            ],
            bootstrap: [app_component_1.AppComponent]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map