import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, Injector, NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { KiraModule } from '../modules/kira/kira.module';
import { ClinicalModule } from '../modules/clinical/clinical.module';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { AdminModule } from '../modules/admin/admin.module';
import { PatientModule } from '../modules/patient/patient.module';
import { appRouting } from './app.routing';
import { JwtHelper } from 'angular2-jwt';
import { SharedModule } from '../modules/shared/shared.module';
import { NgIdleModule } from '@ng-idle/core';
import { EmrModule } from '../modules/emr/emr.module';
import { AuthConfiguration, initApiConfiguration } from '../modules/shared/services';
import { JwtInterceptor } from '../interceptors/jwt.interceptor';
import { ApiUrlInterceptor } from '../interceptors/api.url.interceptor';
import { ApiConfiguration } from '../modules/api/api-configuration';
import { ApiModule } from '../modules/api/api.module';
import { HttpClientInterceptor } from '../interceptors/http.client.interceptor';

@NgModule({
  imports: [
    BrowserModule,
    HttpClientModule,
    NgIdleModule.forRoot(),
    ApiModule,
    appRouting,
    SharedModule.forRoot(),
    KiraModule,
    AdminModule,
    EmrModule,
    ClinicalModule,
    PatientModule,
  ],
  declarations: [AppComponent],
  providers: [
    JwtHelper,
    ApiConfiguration,
    AuthConfiguration,
    [
      { provide: APP_INITIALIZER, useFactory: initApiConfiguration, deps: [ApiConfiguration], multi: true },
      { provide: HTTP_INTERCEPTORS, useClass: HttpClientInterceptor, deps: [Injector], multi: true },
      { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, deps: [Injector], multi: true },
      { provide: HTTP_INTERCEPTORS, useClass: ApiUrlInterceptor, deps: [Injector], multi: true },
    ],
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
