"use strict";
Object.defineProperty(exports, "__esModule", {value: true});
var router_1 = require("@angular/router");
exports.appRouting = router_1.RouterModule.forRoot([{
    path: '',
    loadChildren: '../modules/kira/kira.module#KiraModule'
}], {useHash: true});
//# sourceMappingURL=app.routing.js.map