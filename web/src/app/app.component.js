"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length,
        r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", {value: true});
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var auto_unsubscribe_1 = require("../modules/shared/decorators/auto-unsubscribe");
require("../extensions/rxjs-extensions");
var AppComponent = /** @class */ (function () {
    function AppComponent(loaderService, router, activatedRoute, titleService, pageHeaderService) {
        this.loaderService = loaderService;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.titleService = titleService;
        this.pageHeaderService = pageHeaderService;
        this.isLoading = false;
    }

    AppComponent.prototype.ngOnInit = function () {
        var _this = this;
        // Loader before page content is ready
        this.loaderService.loaderState
            .map(function (state) {
                return state.show;
            })
            .subscribe(function (loading) {
                return _this.isLoading = loading;
            });
        // Page HTML Title set dynamically
        this.router.events
            .filter(function (event) {
                return event instanceof router_1.NavigationEnd;
            })
            .map(function () {
                return _this.activatedRoute;
            })
            .map(function (route) {
                while (route.firstChild)
                    route = route.firstChild;
                return route;
            })
            .filter(function (route) {
                return route.outlet === 'primary';
            })
            .mergeMap(function (route) {
                return route.data;
            })
            .subscribe(function (event) {
                return _this.titleService.setTitle(event['title']);
            });
        // Individual page header set dynamically
        this.pageHeaderService.header.subscribe(function (val) {
            return _this.header = val;
        });
    };
    AppComponent.prototype.ngOnDestroy = function () {
    };
    AppComponent = __decorate([
        auto_unsubscribe_1.AutoUnsubscribe(),
        core_1.Component({
            selector: 'kira-root',
            templateUrl: './app.component.html',
            styleUrls: ['./app.component.scss'],
        })
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map