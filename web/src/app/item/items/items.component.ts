import { Component, OnInit } from '@angular/core';
import { Item } from '../item';
import { Router } from '@angular/router';
import { ItemService } from '../item.service';

@Component({
  selector: 'login-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.scss']
})
export class ItemsComponent implements OnInit {

  items: Item[];
  selectedItem: Item;
  newItem: Item;

  constructor(private router: Router, private itemService: ItemService) {
  }

  ngOnInit() {
    this.itemService.getItems().then(items => this.items = items);
    // this.newItem = new Item();
  }

  createItem(item: Item): void {
    this.itemService.createItem(item)
        .then(item => {
          this.items.push(item);
          this.selectedItem = null;
        });
  }

  deleteItem(item: Item): void {
    this.itemService
        .deleteItem(item)
        .then(() => {
          this.items = this.items.filter(b => b !== item);
          if (this.selectedItem === item) {
            this.selectedItem = null;
          }
        });
  }

  showInfo(item: Item): void {
    this.selectedItem = item;
    this.router.navigate(['/information', this.selectedItem.id]);
  }
}
