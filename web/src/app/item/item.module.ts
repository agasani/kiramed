import { NgModule } from '@angular/core';
import { ItemsComponent } from './items/items.component';
import { ItemInfoComponent } from './item-info/item-info.component';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: 'information/:id', component: ItemInfoComponent },
  { path: 'items', component: ItemsComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class ItemModule {
}
