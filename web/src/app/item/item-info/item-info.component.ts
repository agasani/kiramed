import { Component, OnInit } from '@angular/core';
import { Item } from '../item';
import { ItemService } from '../item.service';
import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'login-item-info',
  templateUrl: './item-info.component.html',
  styleUrls: ['./item-info.component.scss']
})
export class ItemInfoComponent implements OnInit {

  item: Item;

  constructor(private itemService: ItemService,
              private route: ActivatedRoute,
              private location: Location) {
  }

  ngOnInit(): void {
    this.route.params.switchMap(
        (params: Params) => this.itemService.getItem(+params['id']))
        .subscribe(item => this.item = item);
  }

  updateItem(): void {
    this.itemService.updateItem(this.item);
    this.goBack();
  }

  goBack(): void {
    this.location.back();
  }
}
