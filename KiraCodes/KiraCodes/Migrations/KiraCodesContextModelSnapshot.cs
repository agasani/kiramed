﻿// <auto-generated />
using KiraCodes.Enums;
using KiraCodes.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.EntityFrameworkCore.Storage.Internal;
using System;

namespace KiraCodes.Migrations
{
    [DbContext(typeof(KiraCodesContext))]
    partial class KiraCodesContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.0.1-rtm-125")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("KiraCodes.Codes.Allergies.Models.AllergyCategory", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("DateTimeCreated");

                    b.Property<DateTime>("IsDeleted");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("AllergyCategory");
                });

            modelBuilder.Entity("KiraCodes.Codes.Allergies.Models.AllergyReaction", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AllergyID");

                    b.Property<DateTime>("DateTimeCreated");

                    b.Property<string>("Description");

                    b.Property<DateTime>("IsDeleted");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("AllergyID");

                    b.ToTable("AllergyReaction");
                });

            modelBuilder.Entity("KiraCodes.Codes.Allergies.Models.AllergyTreatment", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AllergyID");

                    b.Property<DateTime>("DateTimeCreated");

                    b.Property<string>("Description");

                    b.Property<DateTime>("IsDeleted");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("AllergyID");

                    b.ToTable("AllergyTreatment");
                });

            modelBuilder.Entity("KiraCodes.Codes.Allergies.Models.AllergyTrigger", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AllergyID");

                    b.Property<DateTime>("DateTimeCreated");

                    b.Property<string>("Description");

                    b.Property<DateTime>("IsDeleted");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.HasIndex("AllergyID");

                    b.ToTable("AllergyTrigger");
                });

            modelBuilder.Entity("KiraCodes.Codes.Symptoms.Models.Symptom", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("DateTimeCreated");

                    b.Property<DateTime>("IsDeleted");

                    b.HasKey("Id");

                    b.ToTable("Symptom");
                });

            modelBuilder.Entity("KiraCodes.Models.Allergy", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CategoryID");

                    b.Property<DateTime>("DateTimeCreated");

                    b.Property<DateTime>("IsDeleted");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.HasIndex("CategoryID");

                    b.ToTable("Allergy");
                });

            modelBuilder.Entity("KiraCodes.Models.AllergySymptom", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AllergyID");

                    b.Property<DateTime>("DateTimeCreated");

                    b.Property<string>("Description");

                    b.Property<DateTime>("IsDeleted");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<int?>("SymptomId");

                    b.HasKey("Id");

                    b.HasIndex("AllergyID");

                    b.HasIndex("SymptomId");

                    b.ToTable("AllergySymptom");
                });

            modelBuilder.Entity("KiraCodes.Models.Coding", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CodingID");

                    b.Property<int>("CodingType");

                    b.Property<DateTime>("DateTimeCreated");

                    b.Property<DateTime?>("EffectiveDate");

                    b.Property<DateTime>("IsDeleted");

                    b.Property<string>("Name");

                    b.Property<string>("StringCodingID");

                    b.Property<DateTime?>("TerminationDate");

                    b.HasKey("Id");

                    b.ToTable("Coding");
                });

            modelBuilder.Entity("KiraCodes.Models.Diagnosis", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Code");

                    b.Property<string>("CodeFullName");

                    b.Property<int>("CodingCategory");

                    b.Property<int>("CodingID");

                    b.Property<DateTime>("DateTimeCreated");

                    b.Property<DateTime>("IsDeleted");

                    b.Property<bool>("IsLeaf");

                    b.Property<string>("Name");

                    b.Property<string>("ParentDiagnosisCode");

                    b.Property<string>("Prefix");

                    b.HasKey("Id");

                    b.HasIndex("CodingID");

                    b.ToTable("Diagnosis");
                });

            modelBuilder.Entity("KiraCodes.Models.Medication", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("AdministrationID");

                    b.Property<string>("Code");

                    b.Property<int>("CodingID");

                    b.Property<DateTime>("DateTimeCreated");

                    b.Property<DateTime>("IsDeleted");

                    b.Property<string>("Name");

                    b.Property<string>("Prefix");

                    b.Property<string>("Size");

                    b.HasKey("Id");

                    b.HasIndex("CodingID");

                    b.ToTable("Medication");
                });

            modelBuilder.Entity("KiraCodes.Models.MedicationAdministration", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Abbreviation");

                    b.Property<DateTime>("DateTimeCreated");

                    b.Property<DateTime>("IsDeleted");

                    b.Property<int?>("MedicationId");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.HasIndex("MedicationId");

                    b.ToTable("MedicationAdministration");
                });

            modelBuilder.Entity("KiraCodes.Models.MedicationSideEffect", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("DateTimeCreated");

                    b.Property<string>("Description");

                    b.Property<DateTime>("IsDeleted");

                    b.Property<int?>("MedicationId");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.HasIndex("MedicationId");

                    b.ToTable("MedicationSideEffect");
                });

            modelBuilder.Entity("KiraCodes.Models.Procedure", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Code");

                    b.Property<int>("CodingID");

                    b.Property<DateTime>("DateTimeCreated");

                    b.Property<DateTime>("IsDeleted");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.HasIndex("CodingID");

                    b.ToTable("Procedure");
                });

            modelBuilder.Entity("KiraCodes.Codes.Allergies.Models.AllergyReaction", b =>
                {
                    b.HasOne("KiraCodes.Models.Allergy", "Allergy")
                        .WithMany("Reactions")
                        .HasForeignKey("AllergyID")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("KiraCodes.Codes.Allergies.Models.AllergyTreatment", b =>
                {
                    b.HasOne("KiraCodes.Models.Allergy", "Allergy")
                        .WithMany("Treatments")
                        .HasForeignKey("AllergyID")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("KiraCodes.Codes.Allergies.Models.AllergyTrigger", b =>
                {
                    b.HasOne("KiraCodes.Models.Allergy", "Allergy")
                        .WithMany("Triggers")
                        .HasForeignKey("AllergyID")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("KiraCodes.Models.Allergy", b =>
                {
                    b.HasOne("KiraCodes.Codes.Allergies.Models.AllergyCategory", "Category")
                        .WithMany("Allergies")
                        .HasForeignKey("CategoryID")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("KiraCodes.Models.AllergySymptom", b =>
                {
                    b.HasOne("KiraCodes.Models.Allergy", "Allergy")
                        .WithMany("Symptoms")
                        .HasForeignKey("AllergyID")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("KiraCodes.Codes.Symptoms.Models.Symptom", "Symptom")
                        .WithMany()
                        .HasForeignKey("SymptomId");
                });

            modelBuilder.Entity("KiraCodes.Models.Diagnosis", b =>
                {
                    b.HasOne("KiraCodes.Models.Coding", "Coding")
                        .WithMany()
                        .HasForeignKey("CodingID")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("KiraCodes.Models.Medication", b =>
                {
                    b.HasOne("KiraCodes.Models.Coding", "Coding")
                        .WithMany()
                        .HasForeignKey("CodingID")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("KiraCodes.Models.MedicationAdministration", b =>
                {
                    b.HasOne("KiraCodes.Models.Medication")
                        .WithMany("Administrations")
                        .HasForeignKey("MedicationId");
                });

            modelBuilder.Entity("KiraCodes.Models.MedicationSideEffect", b =>
                {
                    b.HasOne("KiraCodes.Models.Medication", "Medication")
                        .WithMany("SideEffects")
                        .HasForeignKey("MedicationId");
                });

            modelBuilder.Entity("KiraCodes.Models.Procedure", b =>
                {
                    b.HasOne("KiraCodes.Models.Coding", "Coding")
                        .WithMany()
                        .HasForeignKey("CodingID")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}
