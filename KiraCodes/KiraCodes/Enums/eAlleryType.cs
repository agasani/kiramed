using System.ComponentModel;

public enum eAllergyType
{
    [Description("Food")] 
    Food = 0,
    [Description("Drug")]
    Drug = 1,
    [Description("Environmental")]
    Environmental = 2
}