﻿namespace KiraCodes.Enums
{
    public enum eICDCodingCategory
    {
        Disease = 0,
        Disorder = 1,
        Pregnancy = 3,
        Symptomns = 4,
        Injury = 5,
        Poisoning = 6,
        Abnormality = 7
    }
}
