﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KiraCodes.Enums
{
    public enum eICDCoding
    {
        ICD10 = 1,
        ICD9 = 2
    }
}
