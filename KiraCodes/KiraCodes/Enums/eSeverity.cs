using System.ComponentModel;

public enum eSeverity
{
    [Description("VeryMild")]
    VeryMild = 0,
    [Description("Mild")]
    Mild = 1,
    [Description("Moderate")]
    Moderate = 1,
    [Description("Severe")]
    Severe = 2
}