﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace KiraCodes.Enums
{
    public enum eCodingType
    {
        [Description("ICD")]
        ICD = 1 ,
        [Description("Drug")]
        Drug = 2,
        [Description("Procedure")]
        Procedure = 3
    }
}
