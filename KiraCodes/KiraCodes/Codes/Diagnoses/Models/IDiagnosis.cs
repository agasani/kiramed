﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KiraCodes.Enums;
using KiraCodes.Models;

namespace KiraCodes.Models
{
    public interface IDiagnosis
    {
         string Name { get; set; }
         string CodeFullName { get; set; }
         string Code { get; set; }
         string Prefix { get; set; }
         int CodingID { get; set; }
         Coding Coding { get; set; }
         eICDCodingCategory CodingCategory { get; set; }

         bool IsLeaf { get; set; }
         string ParentDiagnosisCode { get; set; }
    }
}
