﻿using System.ComponentModel.DataAnnotations.Schema;
using KiraCodes.Base.Models;
using KiraCodes.Enums;

namespace KiraCodes.Models
{
    [Table("Diagnosis")]
    public class Diagnosis : KiraEntity,  IDiagnosis, ICodable
    { 
        public string Name { get; set; }
        public string CodeFullName { get; set; }
        public string Code { get; set; }
        public string Prefix { get; set; }
        public int CodingID { get; set; }
        public virtual Coding Coding { get; set; }
        public eICDCodingCategory CodingCategory { get; set; }

        //hierarchy
        public bool IsLeaf { get; set; }
        public string ParentDiagnosisCode { get; set; }

        //Information columns
    }  
}
