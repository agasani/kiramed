﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using KiraCodes.Base.Models;
using KiraCodes.ScrappingServices.ICDDiagnosis.Services;
using KiraCodes.Services;
using static KiraCodes.Utils.ControllerFilters;

namespace KiraCodes.Controllers
{
    [Route("api/[controller]")]
    [ValidateModelAttribute]
    public class DiagnosisController : Controller
    {
        private readonly IDiagnosisService _diagnosisService;

        public DiagnosisController(IDiagnosisService diagnosisService)
        {
            _diagnosisService = diagnosisService;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var feedback = new FeedBack();
            var results = await _diagnosisService.GetAllAsync();
            var diagViewModels = results.Select(d => new DiagnosisViewModel(d)).ToList();
            if (diagViewModels.Any())
            {
                
                return Ok(diagViewModels);
            }

            feedback.err = true;
            feedback.message = "No encounters were found";
            return NotFound(feedback);
        }

        [HttpGet("Search/{search}")]
        public async Task<IActionResult> Search(string search)
        {
            var feedback = new FeedBack();

            if (string.IsNullOrWhiteSpace(search))
            {
                feedback.err = true;
                feedback.message = $"Bad request.";
                feedback.data = "";
                return BadRequest(feedback);
            }
            if (search.Length > 50)
            {
                feedback.err = true;
                feedback.message = "The search query is very long. No results found.";
                feedback.data = "";
                return BadRequest(feedback);
            }

            var results = await _diagnosisService.Search(search);
            var diagnoses = results.Select(d => new DiagnosisViewModel(d)).ToList();

            if (!results.Any())
            {
                feedback.err = true;
                feedback.message = "No results were found";
                return BadRequest(feedback);
            }
            return Ok(diagnoses);
        }

        [HttpPost]
        public async Task<IActionResult> Scrap()
        {
            var scrap = new ICDDiagnosisScrapService();

            var scrapResult = await scrap.ScrapAsync();

            var results = await _diagnosisService.LoadyMany(scrapResult);

            return Ok();
        }

    }
}
