﻿using System.Threading.Tasks;
using KiraCodes.Db;
using KiraCodes.Base.Models;
using KiraCodes.Models;
using KiraCodes.Repositories;
using System.Collections.Generic;
using System.Linq;

namespace KiraCodes.Services
{
    public class DiagnosisService : DiagnosisRepository , IDiagnosisService
    {
        public DiagnosisService(KiraCodesContext context) : base(context)
        {

        }

        public async Task<IList<Diagnosis>> Search(string search)
        {
            var searchString = search.ToLower();

            var results = await GetAsync(d => d.Name.ToLower().Contains(searchString) ||
                                              d.Code.ToLower().Contains(searchString) ||
                                              d.CodeFullName.ToLower().Contains(searchString) ||
                                              searchString.Contains(d.Prefix));

            return results.ToList();
        }
    }
}
