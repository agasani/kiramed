
using KiraCodes.Enums;
using KiraCodes.Models;
using KiraCodes.Utils;
public class DiagnosisViewModel
{
        public DiagnosisViewModel(Diagnosis diag)
        {
            CodingName = diag.Coding.Name;
            CodingId = diag.CodingID;
            Name = diag.Name;
            CodeFullName = diag.CodeFullName;
            Code = diag.Code;
            Prefix = diag.Prefix;
            Id = diag.Id;          
        }

        public int Id {get;set;}
        public string CodingName { get; set; }
        public int CodingId { get; set; }
        public string Name { get; set; }
        public string CodeFullName { get; set; }
        public string Code { get; set; }
        public string Prefix { get; set; }
}