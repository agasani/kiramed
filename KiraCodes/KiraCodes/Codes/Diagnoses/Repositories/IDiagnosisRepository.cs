﻿using System.Collections.Generic;
using System.Threading.Tasks;
using KiraCodes.Base;
using KiraCodes.Base.Models;
using KiraCodes.Models;

namespace KiraCodes.Repositories
{
    public interface IDiagnosisRepository : IKiraRepository<Diagnosis>, ISyncableRepository<Diagnosis>
    {
        Task<bool> LoadyMany(IList<Diagnosis> codes);
    }
}
