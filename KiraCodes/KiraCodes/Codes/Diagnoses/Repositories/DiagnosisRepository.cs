﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using KiraCodes.Db;
using KiraCodes.Base.Models;
using KiraCodes.Models;
using Microsoft.EntityFrameworkCore;

namespace KiraCodes.Repositories
{
    public class DiagnosisRepository : KiraRepository<Diagnosis>, IDiagnosisRepository
    {
        private readonly KiraCodesContext _codesContext;
        public DiagnosisRepository(KiraCodesContext context) : base(context)
        {
            _codesContext = context;
        }

        public override async Task<IEnumerable<Diagnosis>> GetAsync(Expression<Func<Diagnosis, bool>> expression)
        {
            try
            {
                var results = await _codesContext.Diagnoses.Where(d => d.IsDeleted > DateTime.Now).Where(expression).Include(d => d.Coding).ToListAsync();
                return results;
            }
            catch (Exception)
            {
                return new List<Diagnosis>();
            }
           
        }

        public override async Task<IEnumerable<Diagnosis>> GetAllAsync()
        {
            try
            {
                var results = await _codesContext.Diagnoses.Where(d => d.IsDeleted > DateTime.Now).Include(d => d.Coding).ToListAsync();
                return results;
            }
            catch (Exception e)
            {
                return new List<Diagnosis>();
            }
        }

        public Task<bool> CreateBulkAsync(IList<Diagnosis> items)
        {
            throw new NotImplementedException();
        }

        public Task<bool> DeleteBulkAsync(IList<Diagnosis> items)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> LoadyMany(IList<Diagnosis> codes)
        {
           await _codesContext.Diagnoses.AddRangeAsync(codes);
           var result = await _codesContext.SaveChangesAsync();
           return result > 0;
        }
    }
}
