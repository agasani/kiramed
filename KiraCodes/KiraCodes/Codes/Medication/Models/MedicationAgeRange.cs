﻿using System.ComponentModel.DataAnnotations.Schema;
using KiraCodes.Base.Models;

namespace KiraCodes.Models
{
    [Table("MedicationAgeRange")]
    public class MedicationAgeRange : KiraEntity
    {
        public virtual Medication Medication { get; set; }
        public string Size { get; set; }
    }
}
