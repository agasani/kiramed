﻿using System.ComponentModel.DataAnnotations.Schema;
using KiraCodes.Base.Models;

namespace KiraCodes.Models
{
    [Table("MedicationAdministration")]
    public class MedicationAdministration : KiraEntity
    {
        public string Name { get; set; }
        public string Abbreviation { get; set; }
    }
}
