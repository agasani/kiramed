using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using KiraCodes.Base.Models;

namespace KiraCodes.Models
{
    [Table("Medication")]
    public class Medication : KiraEntity, ICodable
    {
        public string Name { get; set; }
        public string Size { get; set; }
        public string AdministrationID { get; set; }
        public string Code { get; set; }
        public string Prefix { get; set; }
        public int CodingID { get; set; }
        public Coding Coding { get; set; }
        public virtual List<MedicationAdministration> Administrations { get; set; }
        public virtual List<MedicationSideEffect> SideEffects { get; set; }
    } 
}
