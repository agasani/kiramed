﻿using System.ComponentModel.DataAnnotations.Schema;
using KiraCodes.Base.Models;

namespace KiraCodes.Models
{
    [Table("MedicationSideEffect")]
    public class MedicationSideEffect : KiraEntity
    {
        public virtual Medication Medication { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
