﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KiraCodes.Models;

namespace KiraCodes.Codes.Medication.ViewModels
{
    public class MedicationViewModel
    {
        public string Name { get; set; }
        public string CodeFullName { get; set; }
        public string Code { get; set; }
        public string Prefix { get; set; }
        public int CodingID { get; set; }
        public Coding Coding { get; set; }
    }
}
