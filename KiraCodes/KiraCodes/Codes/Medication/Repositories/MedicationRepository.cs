﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KiraCodes.Base.Models;
using KiraCodes.Models;

namespace KiraCodes.Repositories
{
    public class MedicationRepository : KiraRepository<Medication>, IMedicationRepository
    {
        public MedicationRepository(KiraCodesContext context) : base(context)
        {

        }
    }
}
