using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using KiraCodes.Base.Models;
using KiraCodes.Services;
using static KiraCodes.Utils.ControllerFilters;

namespace KiraCodes.Controllers
{
    [Route("api/[controller]")]
    [ValidateModelAttribute]
    public class AllergyController : Controller
    {
        private readonly IAllergyService _allergyService;
        public AllergyController(IAllergyService allergyService)
        {
            _allergyService = allergyService;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var feedback = new FeedBack();
            var results = await _allergyService.GetAllAsync();
            var allergyViewModels = results.Select(a => new AllergyViewModel(a)).ToList();
            if (allergyViewModels.Any())
            {
                
                return Ok(allergyViewModels);
            }

            feedback.err = true;
            feedback.message = "No allergies were found";
            return NotFound(feedback);
        }

        [HttpGet("GetByCategoryId/{categoryid}")]
        public async Task<IActionResult> GetByType(int categoryid)
        {
            var feedback = new FeedBack();
            var results = await _allergyService.GetAsync(a => (short)a.CategoryID == categoryid);
            var allergyViewModels = results.Select(a => new AllergyViewModel(a)).ToList();
            if (allergyViewModels.Any())
            {
                
                return Ok(allergyViewModels);
            }

            feedback.err = true;
            feedback.message = "No allergies were found";
            return NotFound(feedback);
        }

        [HttpGet("Search/{search}")]
        public async Task<IActionResult> Search(string search)
        {
            var feedback = new FeedBack();

            if (string.IsNullOrWhiteSpace(search))
            {
                feedback.err = true;
                feedback.message = $"Bad request.";
                feedback.data = "";
                return BadRequest(feedback);
            }
            if (search.Length > 50)
            {
                feedback.err = true;
                feedback.message = "The search query is very long. No results found.";
                feedback.data = "";
                return BadRequest(feedback);
            }

            var results = await _allergyService.Search(search);
            var allergyViewModels = results.Select(a => new AllergyViewModel(a)).ToList();

            if (!results.Any())
            {
                feedback.err = true;
                feedback.message = "No results were found";
                return BadRequest(feedback);
            }
            return Ok(allergyViewModels);
        }

    }
}
