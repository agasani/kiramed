using System.Threading.Tasks;
using KiraCodes.Base.Models;
using KiraCodes.Models;

namespace KiraCodes.Repositories
{
    public interface IAllergyRepository : IKiraRepository<Allergy>
    {
        
    }
}
