using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using KiraCodes.Db;
using KiraCodes.Base.Models;
using KiraCodes.Models;
using Microsoft.EntityFrameworkCore;

namespace KiraCodes.Repositories
{
    public class AllergyRepository : KiraRepository<Allergy>, IAllergyRepository
    {
        private readonly KiraCodesContext _codesContext;

        public AllergyRepository(KiraCodesContext context) : base(context)
        {
            _codesContext = context;
        }

        public override async Task<IEnumerable<Allergy>> GetAllAsync()
        {
            try
            {
                var results = await _codesContext.Allergies.Where(d => d.IsDeleted > DateTime.Now).Include(d => d.Category).ToListAsync();
                return results;
            }
            catch (Exception)
            {
                return new List<Allergy>();
            }
        }

        public override async Task<IEnumerable<Allergy>> GetAsync(Expression<Func<Allergy, bool>> expression)
        {
            try
            {
                var results = await _codesContext.Allergies.Where(d => d.IsDeleted > DateTime.Now).Where(expression).Include(d => d.Category).ToListAsync();
                return results;
            }
            catch (Exception)
            {
                return new List<Allergy>();
            }
        }
    }

}
