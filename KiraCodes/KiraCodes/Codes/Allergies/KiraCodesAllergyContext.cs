﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KiraCodes.Codes.Allergies.Models;
using KiraCodes.Models;
using Microsoft.EntityFrameworkCore;

namespace KiraCodes.Models
{
    public partial class KiraCodesContext
    {
        public DbSet<AllergyCategory> AllergyCategories { get; set; }
        public DbSet<AllergyTreatment> AllergyTreatments { get; set; }
        public DbSet<AllergyReaction> AllergyReactions { get; set; }
        public DbSet<AllergySymptom> AllergySymptoms { get; set; }
        public DbSet<AllergyTrigger> AllergyTriggers { get; set; }


    }
}
