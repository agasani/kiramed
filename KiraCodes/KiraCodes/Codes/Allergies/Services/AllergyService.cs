using System.Threading.Tasks;
using KiraCodes.Db;
using KiraCodes.Base.Models;
using KiraCodes.Models;
using KiraCodes.Repositories;
using System.Collections.Generic;
using System.Linq;

namespace KiraCodes.Services
{
    public class AllergyService : AllergyRepository , IAllergyService
    {
        public AllergyService(KiraCodesContext context) : base(context)
        {

        }

        public async Task<IList<Allergy>> Search(string search)
        {
            var results =  await GetAsync(a => a.Name.ToLower().Contains(search.ToLower()) || a.Category.Name.Contains(search.ToLower()));
            return results.ToList();
        }
    }
}
