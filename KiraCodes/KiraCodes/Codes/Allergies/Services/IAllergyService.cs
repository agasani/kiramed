using System.Threading.Tasks;
using KiraCodes.Db;
using KiraCodes.Base.Models;
using KiraCodes.Models;
using KiraCodes.Repositories;
using  System.Collections.Generic;

namespace KiraCodes.Services
{
    public interface IAllergyService : IAllergyRepository
    {
        Task<IList<Allergy>> Search(string search);
    }
}
