using System.Collections.Generic;
using System.Linq;
using KiraCodes.Codes.Allergies.Models;
using KiraCodes.Models;
using KiraCodes.ViewModels;

public class AllergyViewModel
{
        public AllergyViewModel(Allergy allergy)
        {
           Id = allergy.Id;
           Name = allergy.Name;
           CategoryID = allergy.CategoryID;
           CategoryName = allergy.Category.Name;
           Reactions = allergy.Reactions.Select(r => new AllergyItemViewModel<AllergyReaction>(r)).ToList();
           Symptoms = allergy.Symptoms.Select(r => new AllergyItemViewModel<AllergySymptom>(r)).ToList();
           Treatments = allergy.Treatments.Select(r => new AllergyItemViewModel<AllergyTreatment>(r)).ToList();
           Triggers = allergy.Triggers.Select(r => new AllergyItemViewModel<AllergyTrigger>(r)).ToList();

        }

       public int Id {get;set;}
       public int CategoryID { get; set; }
       public string CategoryName { get; set; }
       public string Name {get;set;}
       public IList<AllergyItemViewModel<AllergyReaction>> Reactions { get; set; }
       public IList<AllergyItemViewModel<AllergySymptom>> Symptoms { get; set; }
       public IList<AllergyItemViewModel<AllergyTreatment>> Treatments { get; set; }
       public IList<AllergyItemViewModel<AllergyTrigger>> Triggers { get; set; }

}