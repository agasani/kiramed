﻿using KiraCodes.Models;

namespace KiraCodes.ViewModels
{
    public class AllergyItemViewModel<T> where T : IAllergyItem
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int Id { get; set; }

        public AllergyItemViewModel(T item)
        {
            Name = item.Name;
            Description = item.Description;
            Id = item.AllergyID; //TODO fix the id
        }
    }
}
