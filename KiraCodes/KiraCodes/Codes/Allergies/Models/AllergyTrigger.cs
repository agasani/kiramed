﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using KiraCodes.Base.Models;
using KiraCodes.Models;

namespace KiraCodes.Codes.Allergies.Models
{
    [Table("AllergyTrigger")]
    public class AllergyTrigger : KiraEntity, IAllergyItem
    {
        public virtual Allergy Allergy { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int AllergyID { get; set; }
    }
}
