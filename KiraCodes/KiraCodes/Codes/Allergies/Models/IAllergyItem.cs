﻿namespace KiraCodes.Models
{
    public interface IAllergyItem
    {
        Allergy Allergy { get; set; }
        string Name { get; set; }
        string Description { get; set;}
        int AllergyID { get; set; }
    }
}
