﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using KiraCodes.Base.Models;
using KiraCodes.Models;

namespace KiraCodes.Codes.Allergies.Models
{
    [Table("AllergyTreatment")]
    public class AllergyTreatment : KiraEntity, IAllergyItem
    {
        [Required]
        public virtual Allergy Allergy { get; set; }
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        public int AllergyID { get; set; }
    }
}
