using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using KiraCodes.Base.Models;
using KiraCodes.Codes.Allergies.Models;
using KiraCodes.Enums;

namespace KiraCodes.Models
{
    [Table("Allergy")]
    public class Allergy : KiraEntity
    {
       public virtual AllergyCategory Category {get;set;}
       public int CategoryID { get; set; }
       public string Name {get;set;}
       public virtual List<AllergyReaction> Reactions { get; set; } = new List<AllergyReaction>();
       public virtual List<AllergySymptom> Symptoms { get; set; } = new List<AllergySymptom>();
       public virtual List<AllergyTreatment> Treatments { get; set; } = new List<AllergyTreatment>();
       public virtual List<AllergyTrigger> Triggers { get; set; } = new List<AllergyTrigger>();
    }  
}
