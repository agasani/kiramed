﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using KiraCodes.Base.Models;
using KiraCodes.Models;

namespace KiraCodes.Codes.Allergies.Models
{
    [Table("AllergyCategory")]
    public class AllergyCategory : KiraEntity
    {
        public string Name { get; set; }
        public virtual List<Allergy> Allergies { get; set; }
    }
}
