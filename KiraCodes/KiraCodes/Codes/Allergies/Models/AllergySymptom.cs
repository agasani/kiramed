﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using KiraCodes.Base.Models;
using KiraCodes.Codes.Symptoms.Models;

namespace KiraCodes.Models
{
    [Table("AllergySymptom")]
    public class AllergySymptom : KiraEntity, IAllergyItem
    {
        [Required]
        public virtual Allergy Allergy { get; set; }
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        public int AllergyID { get; set; }
        public virtual Symptom Symptom { get; set; }
    }
}
