using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using KiraCodes.Base.Models;
using KiraCodes.ScrappingServices.ICDDiagnosis.Services;
using static KiraCodes.Utils.ControllerFilters;

namespace KiraCodes.Controllers
{
    [Route("api/[controller]")]
    [ValidateModelAttribute]
    public class CodingController : Controller
    {
        private readonly ICodingRepository _codingRepository;
        public CodingController(ICodingRepository codingRepository)
        {
            _codingRepository = codingRepository;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {

           


            var feedback = new FeedBack();
            var results = await _codingRepository.GetAllAsync();
            var codeViewModels = results.Select(a => new CodingViewModel(a)).ToList();
            if (codeViewModels.Any())
            {               
                return Ok(codeViewModels);
            }

            feedback.err = true;
            feedback.message = "No codes were found";
            return NotFound(feedback);
        }

        [HttpGet("GetById/{id}")]
        public async Task<IActionResult> GetById(short id)
        {          
            var feedback = new FeedBack();
            var results = await _codingRepository.GetAsync(c => c.Id == id);
            var codeViewModels = results.Select(a => new CodingViewModel(a)).ToList();
            if (codeViewModels.Any())
            {
                
                return Ok(codeViewModels);
            }

            feedback.err = true;
            feedback.message = "No codes were found";
            return NotFound(feedback);
        }

        [HttpGet("GetByCodingId/{codingId}")]
        public async Task<IActionResult> GetByCodingId(short codingId)
        {
            var feedback = new FeedBack();
            var results = await _codingRepository.GetAsync(c => c.CodingID == codingId);
            var codeViewModels = results.Select(a => new CodingViewModel(a)).ToList();
            if (codeViewModels.Any())
            {

                return Ok(codeViewModels);
            }

            feedback.err = true;
            feedback.message = "No codes were found";
            return NotFound(feedback);
        }

        // [HttpGet("Search/{search}")]
        // public async Task<IActionResult> Search(string search)
        // {
        //     var feedback = new FeedBack();

        //     if (string.IsNullOrWhiteSpace(search))
        //     {
        //         feedback.err = true;
        //         feedback.message = $"Bad request.";
        //         feedback.data = "";
        //         return BadRequest(feedback);
        //     }
        //     if (search.Length > 50)
        //     {
        //         feedback.err = true;
        //         feedback.message = "The search query is very long. No results found.";
        //         feedback.data = "";
        //         return BadRequest(feedback);
        //     }

        //     var results = await _codingRepository.Search(search);
        //     var CodeViewModels = results.Select(a => new CodingViewModel(a)).ToList();

        //     if (!results.Any())
        //     {
        //         feedback.err = true;
        //         feedback.message = "No results were found";
        //         return BadRequest(feedback);
        //     }
        //     return Ok(CodeViewModels);
        // }

    }
}
