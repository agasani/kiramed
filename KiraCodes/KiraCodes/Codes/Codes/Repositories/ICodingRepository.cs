using KiraCodes.Base.Models;
using KiraCodes.Models;

public interface ICodingRepository : IKiraRepository<Coding>
{
    
}