using KiraCodes.Base.Models;
using KiraCodes.Db;
using KiraCodes.Models;

public class CodingRepository : KiraRepository<Coding>, ICodingRepository
{
    public CodingRepository(KiraCodesContext context) : base(context)
    {

    }
}