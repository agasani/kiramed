using System;
using KiraCodes.Enums;
using KiraCodes.Models;
using KiraCodes.Utils;

public class CodingViewModel
{
    public int Id {get;set;}
    public int CodeID {get;set;}
    public string StringCodeID {get;set;}
    public string Name {get;set;}
    public DateTime? EffectiveDate {get;set;}
    public DateTime? TerminationDate {get;set;}
    public eCodingType CodingType { get; set; }
    public string CodingTypeName { get; set; }


    public CodingViewModel(Coding model)
    {
        Id = model.Id;
        CodeID = model.CodingID;
        StringCodeID = model.StringCodingID;
        Name = model.Name;
        EffectiveDate = model.EffectiveDate?? DateTime.MinValue;
        TerminationDate = model.TerminationDate?? DateTime.MaxValue;
        CodingType = model.CodingType;
        CodingTypeName = model.CodingType.GetDescription();
    }
}