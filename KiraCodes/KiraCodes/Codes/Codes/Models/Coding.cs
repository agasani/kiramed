using System;
using System.ComponentModel.DataAnnotations.Schema;
using KiraCodes.Base.Models;
using KiraCodes.Enums;

namespace KiraCodes.Models
{
    [Table("Coding")]
    public class Coding : KiraEntity
    {
        public int CodingID {get;set;}
        public string StringCodingID {get;set;}
        public string Name {get;set;}
        public DateTime? EffectiveDate {get;set;}
        public DateTime? TerminationDate{get;set;}
        public eCodingType CodingType { get; set; }
    }
}