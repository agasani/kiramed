﻿using KiraCodes.Enums;

namespace KiraCodes.Models
{
    public interface ICodable
    {
        int CodingID { get; set; }
        Coding Coding { get; set; }
        string Name { get; set; }
        string Code { get; set; }
    }
}
