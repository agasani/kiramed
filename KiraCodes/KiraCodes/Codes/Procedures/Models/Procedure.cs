﻿using System.ComponentModel.DataAnnotations.Schema;
using KiraCodes.Base.Models;

namespace KiraCodes.Models
{
    [Table("Procedure")]
    public class Procedure : KiraEntity, ICodable
    {
        public int CodingID { get; set; }
        public Coding Coding { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
    }
}
