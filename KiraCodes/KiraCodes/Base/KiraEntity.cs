﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace KiraCodes.Base.Models
{
    public class KiraEntity
    {
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public DateTime IsDeleted { get; set; }  = DateTime.MaxValue;
        public DateTime DateTimeCreated { get; set; }  = DateTime.Now;
    } 
}
