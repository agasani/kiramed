﻿using System;

namespace KiraCodes.Base.Models
{
    public interface IKiraEntity
    {
       int Id { get; set; }
       DateTime IsDeleted { get; set; } 
       DateTime DateTimeCreated { get; set; } 
    }
}
