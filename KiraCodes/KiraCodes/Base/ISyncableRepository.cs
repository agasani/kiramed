﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KiraCodes.Base.Models;

namespace KiraCodes.Base
{
    public interface ISyncableRepository<T> where T: KiraEntity
    {
        Task<bool> CreateBulkAsync(IList<T> items);
        Task<bool> DeleteBulkAsync(IList<T> items);
    }
}
