﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KiraCodes.Base.Models
{
    public class FeedBack
    {
        public bool err { get; set; } = false;
        public string message { get; set; } = "";
        public string data { get; set; } = "{}";
    }
}
