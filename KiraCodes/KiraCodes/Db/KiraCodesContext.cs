﻿
using KiraCodes.Codes.Symptoms.Models;
using Microsoft.EntityFrameworkCore;

namespace KiraCodes.Models
{
    public partial class KiraCodesContext : DbContext
    {
        public KiraCodesContext(DbContextOptions<KiraCodesContext> options) : base(options)  { }

        public DbSet<Diagnosis> Diagnoses { get; set; }
        public DbSet<Allergy> Allergies { get; set; }
        public DbSet<Coding> Codings { get; set; }
        public DbSet<Medication> Medications { get; set; }
        public DbSet<MedicationSideEffect> MedicationSideEffects { get; set; }
        public DbSet<MedicationAdministration> MedicationAdministrations { get; set; }
        public DbSet<Symptom> Symptoms { get; set; }
        public DbSet<Procedure> Procedures { get; set; }
    }
}
