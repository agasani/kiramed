﻿
using System;
using System.Linq;
using KiraCodes.Codes.Allergies.Models;
using KiraCodes.Enums;
using KiraCodes.Models;

namespace KiraCodes.Db
{
    public class DbInitializer
    {
        public static void Init(KiraCodesContext codeContext)
        {
            //codeContext.Database.EnsureDeleted();
           // codeContext.Database.EnsureCreated();

            if (codeContext.Diagnoses.Any())
            {
                return;
            }

            var coding1 = new Coding
            {
                Name = "ICD10",
                CodingID = 1,
                StringCodingID = "ICD10",
                EffectiveDate = new DateTime(2013, 1, 1),
                TerminationDate = DateTime.MaxValue,
                CodingType = eCodingType.ICD
            };

            var coding2 = new Coding
            {
                Name = "ICD9",
                CodingID = 2,
                StringCodingID = "ICD9",
                EffectiveDate = new DateTime(2007, 1, 1),
                TerminationDate = new DateTime(2012,12,31),
                CodingType = eCodingType.ICD
            };

            codeContext.Codings.AddRange(coding1, coding2);


            var diag1 = new Diagnosis
            {
                Code = "S90.572A",
                Name = "Other superficial bite of ankle left ankle initial encounter",
                CodeFullName = "S90.572A - Other superficial bite of ankle left ankle initial encounter ",
                Prefix = "S90",
                Coding = coding1,
                CodingID = coding1.CodingID
            };

            var diag2 = new Diagnosis
            {
                Code = "S90572D",
                Name = "Other superficial bite of ankle left ankle subsequent encounter",
                CodeFullName = "S90.572D - Other superficial bite of ankle left ankle subsequent encounter",
                Prefix = "S90",
                Coding = coding2,
                CodingID = coding2.CodingID
            };

            var diag3 = new Diagnosis
            {
                Code = "S90572S",
                Name = "Other superficial bite of ankle left ankle sequela",
                CodeFullName = "S90.572S - Other superficial bite of ankle left ankle sequela",
                Prefix = "S90",
                Coding = coding1,
                CodingID = coding1.CodingID
            };

            codeContext.Diagnoses.AddRange(diag1, diag2, diag3);


            #region AllergyInitializations     
            
             //reactions

 
            //symptoms

            //var symp1 = new AllergySymptom()
            //{
  
            //    Name = "Durg 1"
            //};

            //treatments

            //categories

            var category1 = new AllergyCategory
            {
                Name = "Food"
            };

            var category2 = new AllergyCategory
            {
                Name = "Skin"
            };

            var category3 = new AllergyCategory
            {
                Name = "Pet"
            };

            codeContext.AllergyCategories.AddRange(category3, category2, category1);


            var allergy2 = new Allergy
            {
                Category = category2,
                Name = "Eczema"

            };

            var allergy1 = new Allergy
            {
                 Category = category1,
                 Name = "Mik"
            };

            var allergy3 = new Allergy
            {
               Category = category3,
                Name = "Cat"
            };

            codeContext.Allergies.AddRange(allergy1, allergy2, allergy3);

            #endregion

            codeContext.SaveChanges();
       
        }

        public static void DropAllTables(KiraCodesContext context)
        {
            context.Database.EnsureDeleted();
            context.Database.EnsureCreated();
        }
    }
}
