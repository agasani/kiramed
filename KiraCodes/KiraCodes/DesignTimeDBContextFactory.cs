﻿using System.IO;
using KiraCodes.Db;
using KiraCodes.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<KiraCodesContext>
{
    public KiraCodesContext CreateDbContext(string[] args)
    {
        IConfigurationRoot configuration = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json")
            .Build();

        var builder = new DbContextOptionsBuilder<KiraCodesContext>();

        var connectionString = configuration.GetConnectionString("AzureConnection");

        builder.UseSqlServer(connectionString);

        return new KiraCodesContext(builder.Options);
    }
}