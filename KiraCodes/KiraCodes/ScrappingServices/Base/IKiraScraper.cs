﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace KiraCodes.ScrappingServices.Base
{
    public interface IKiraScraper<T> where T : IScrapable
    {
        Task<IList<T>> ScrapPageAsync(string url);
    }
}
