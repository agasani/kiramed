﻿using System.Collections.Generic;
using System.Threading.Tasks;
using KiraCodes.Base.Models;

namespace KiraCodes.ScrappingServices.Base
{
    public interface IScrapService<T> where T : KiraEntity
    {
        Task<IList<T>> SynchronizeAsync();
        Task<IList<T>> ScrapAsync();
    }
}
