﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using KiraCodes.Models;
using KiraCodes.ScrappingServices.Base;
using KiraCodes.ScrappingServices.ICDDiagnosis.Logic;
using KiraCodes.ScrappingServices.ICDDiagnosis.Models;
using KiraCodes.Services;

namespace KiraCodes.ScrappingServices.ICDDiagnosis.Services
{
    public class ICDDiagnosisScrapService : IScrapService<Diagnosis>
    {
        private readonly IDiagnosisService _diagnosisService;

        public ICDDiagnosisScrapService()
        {
            //_diagnosisService = diagnosisService;
        }

        public Task<IList<Diagnosis>> SynchronizeAsync()
        {
          
            throw new NotImplementedException();
        }

        public async  Task<IList<Diagnosis>> ScrapAsync()
        {
            var scraper = new DiagnosisScraper();
            var results = await scraper.ScrapPageAsync("");

            return results.Select(MapDiag.ToDiag).ToList();
        }
    }

    public static class MapDiag
    {
        public static Diagnosis ToDiag(ScrapDiagnosis scrapDiag)
        {
            var diag = new Diagnosis
            {
                Coding = scrapDiag.Coding,
                CodeFullName = scrapDiag.CodeFullName,
                Prefix = scrapDiag.Prefix,
                Name = scrapDiag.Name,
                CodingID = scrapDiag.CodingID,
                IsLeaf = scrapDiag.IsLeaf,
                ParentDiagnosisCode = scrapDiag.ParentDiagnosisCode,
                CodingCategory = scrapDiag.CodingCategory,
                Code = scrapDiag.Code
            };
            return diag;
        }
    }
}
