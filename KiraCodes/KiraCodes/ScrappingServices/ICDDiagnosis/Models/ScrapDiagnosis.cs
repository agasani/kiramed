﻿using KiraCodes.Enums;
using KiraCodes.Models;
using KiraCodes.ScrappingServices.Base;

namespace KiraCodes.ScrappingServices.ICDDiagnosis.Models
{
    public class ScrapDiagnosis : IScrapable, IDiagnosis
    {
        public string Name { get; set; }
        public string CodeFullName { get; set; }
        public string Code { get; set; }
        public string Prefix { get; set; }
        public int CodingID { get; set; }
        public Coding Coding { get; set; }
        public eICDCodingCategory CodingCategory { get; set; }
        public bool IsLeaf { get; set; }
        public string ParentDiagnosisCode { get; set; }
    }
}
