﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using KiraCodes.Enums;
using KiraCodes.Models;
using KiraCodes.ScrappingServices.Base;
using KiraCodes.ScrappingServices.ICDDiagnosis.Models;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace KiraCodes.ScrappingServices.ICDDiagnosis.Logic
{
    public class DiagnosisScraper : IKiraScraper<ScrapDiagnosis>
    {
        private readonly ChromeDriver _chromeDriver;

        public DiagnosisScraper()
        {
            _chromeDriver = new ChromeDriver(@"C:\Users\jmasengesho\Documents\workspace\kiramed\KiraCodes\KiraCodes\bin\Debug\netcoreapp2.0");
        }
        public async Task<IList<ScrapDiagnosis>> ScrapPageAsync(string url)
        {
           /// ParseLeafPage();

            var entry = "http://www.icd10data.com/ICD10CM/Codes";
            var codes = new List<ScrapDiagnosis>();
            var currentUrl = new UrlDTO
            {
                Url = entry,
                ParentCode = ""
            };

            var urls = new List<UrlDTO>
            {
                currentUrl
            };

           // var testLeaft = ParseLeafPage();
            try
            {
                while (urls.Count > 0)
                {
                    var newCodes = ParsePage(urls[0], urls);
                    codes.AddRange(newCodes);
                }
            }
            catch (Exception e)
            {
                var test = e;
            }

            return codes;
        }

        private IEnumerable<ScrapDiagnosis> ParsePage(UrlDTO currentUrl, List<UrlDTO> urlDTOs)
        {
            var codes = new List<ScrapDiagnosis>();
            _chromeDriver.Navigate().GoToUrl(currentUrl.Url);

            var cats = _chromeDriver.FindElementByXPath("/html/body/div[2]/div/ul");

            var categories = cats.FindElements(By.TagName("li"));

            var urls = new List<UrlDTO>();

            foreach (var cat in categories)
            {
                var link = cat.FindElement(By.TagName("a"));
                var code = link.Text;
                var codeName = cat.Text.Replace(code, "");
                var href = link.GetAttribute("href");

                urls.Add(new UrlDTO{Url = href, ParentCode = code});

                var diag = new ScrapDiagnosis
                {
                    Name = codeName,
                    Code = code,
                    CodeFullName = cat.Text,
                    IsLeaf = false,
                    Coding = new Coding(),
                    CodingCategory = eICDCodingCategory.Disease,
                    CodingID = 1,
                    ParentDiagnosisCode = currentUrl.ParentCode,
                    Prefix = code.Substring(0, 3)
                };

                codes.Add(diag);                 
            }

            urlDTOs.Remove(currentUrl);
            urlDTOs.AddRange(urls);

            return codes;
        }

       // private IEnumerable<ScrapDiagnosis> ParseLeafPage(UrlDTO currentUrl, List<UrlDTO> urlDTOs)
        private IEnumerable<ScrapDiagnosis> ParseLeafPage()
        {
            var codes = new List<ScrapDiagnosis>();
            _chromeDriver.Navigate().GoToUrl("http://www.icd10data.com/ICD10CM/Codes/S00-T88/S00-S09/S00-");

            try
            {
                var catss = _chromeDriver.FindElementByXPath("/html/body/div[2]/div/ul");
            }
            catch(Exception e)
            {

            }

            var cats = _chromeDriver.FindElementByXPath("/html/body/div[2]/div/ul");

            var categories = cats.FindElements(By.ClassName("glyphicon glyphicon-triangle-right danger"));

            var urls = new List<UrlDTO>();

            foreach (var cat in categories)
            {
                var link = cat.FindElement(By.TagName("a"));
                var code = link.Text;
                var codeName = cat.Text.Replace(code, "");
                var href = link.GetAttribute("href");

                urls.Add(new UrlDTO { Url = href, ParentCode = code });

                var diag = new ScrapDiagnosis
                {
                    Name = codeName,
                    Code = code,
                    CodeFullName = cat.Text,
                    IsLeaf = false,
                    Coding = new Coding(),
                    CodingCategory = eICDCodingCategory.Disease,
                    CodingID = 1,
                    ParentDiagnosisCode = "",
                    Prefix = code.Substring(0, 3)
                };

                codes.Add(diag);
            }

            return codes;
        }
    }

    public class UrlDTO
    {
        public string Url { get; set; }
        public string ParentCode { get; set; }
    }
}
