﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KiraCodes.Utils
{
    public static class RandomPasswordGenerator
    {
      
        private static Random random = new Random();

        public static string GeneratePassword(int length)
        {
            var password = string.Empty;
            const string chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            const string digits = "0123456789";

            password = new string(Enumerable.Repeat(chars, 4)
              .Select(s => s[random.Next(s.Length)]).ToArray());
            password += new string(Enumerable.Repeat(digits, 2)
              .Select(s => s[random.Next(s.Length)]).ToArray());

            return Shuffle(password);
        }

        public static string Shuffle(string str)
        {
            char[] array = str.ToCharArray();
            Random rng = new Random();
            int n = array.Length;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                var value = array[k];
                array[k] = array[n];
                array[n] = value;
            }
            return new string(array);
        }
    }
}
