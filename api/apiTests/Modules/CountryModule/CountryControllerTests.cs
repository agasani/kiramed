﻿using System.Linq;
using System.Collections.Generic;
using Xunit;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using api.Db;
using api.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using api;
using Newtonsoft.Json;
using api.Controllers;
using api.Services;
using apiTests.TestSetUp;

namespace apiTests.Modules.CountryModule
{
    public class CountryControllerTests : UserServiceTestSeUp
    {
        private readonly IEnumerator<Country> _itirator;
        private readonly CountryController _controller;

        public CountryControllerTests(TestFixture<Startup> fixture) : base(fixture)
        {
            _itirator = getTestCountries().GetEnumerator();
            _controller = new CountryController(_countryRepository, _userService);
        }

        public IEnumerable<Country> getTestCountries()
        {
            var co1 = new Country { Name = "Rwanda", Abbreviation = "RW", Code = "250", Id = 1, OfficialLanguage =  "English", Currency = "Rwandan Francs"};
            var co2 = new Country { Name = "Uganda", Abbreviation = "UG", Code = "254", Id = 1, OfficialLanguage = "English", Currency = "Shillings" };

            var countries = new List<Country> { co1, co2 };


            for (var idx = 0; idx < 3; idx++)
            {
                yield return countries[idx]; ;
            }
        }

        public api.Models.Country getTestCountry()
        {
            if (this._itirator.MoveNext())
                return this._itirator.Current;

            return null;
        }

        [Fact]
        public void OnGet_ReturnsAListOfCountryVModels()
        {
            var countryContr = _controller;;

            var result = countryContr.Get().Result;



            var okResult = Assert.IsType<OkObjectResult>(result);
            var returnPatients = Assert.IsType<List<CountryViewModel>>(okResult.Value);
            Assert.Equal(true, returnPatients.Any());

        }

        [Fact]
        public async  Task OnGetId_ReturnsAOrgVModel()
        {
            var countryContr = _controller;
            var coResult = await _countryRepository.GetAllAsync();

            var result = countryContr.GetById(coResult.FirstOrDefault().Id).Result;

            var okResult = Assert.IsType<OkObjectResult>(result);
            var returnPat = Assert.IsType<CountryViewModel>(okResult.Value);
        }

        [Fact]
        public async Task OnGetOrgCountrCode_ReturnsACountryVModel()
        {
            var countryContr = _controller;
            var coResult = await _countryRepository.GetAllAsync();

            var result = countryContr.GetByCode(coResult.FirstOrDefault().Code).Result;

            var okResult = Assert.IsType<OkObjectResult>(result);
            var returnPat = Assert.IsType<CountryViewModel>(okResult.Value);
        }

        [Fact]
        public void OnGetIdIfCountryIdNE_ReturnsHttpNotFound()
        {
            var countryContr = _controller;
            var testId = 911;

            var result = countryContr.GetById(testId).Result;

            var notFoundResult = Assert.IsType<NotFoundObjectResult>(result);
            var returnFeedback = Assert.IsType<FeedBack>(notFoundResult.Value);
            Assert.Equal(true, returnFeedback.err);
        }

        [Fact]
        public void OnGetIdIfCountryCodedNE_ReturnsHttpNotFound()
        {
            var countryContr = _controller;
            var testId = "USA";

            var result = countryContr.GetByCode(testId).Result;

            var notFoundResult = Assert.IsType<NotFoundObjectResult>(result);
            var returnFeedback = Assert.IsType<FeedBack>(notFoundResult.Value);
            Assert.Equal(true, returnFeedback.err);
        }

        [Fact]
        public async Task OnCreate_ReturnsOkResultNFeedbackWithCreatedCountryId()
        {
            var countryContr = _controller;
            var model = new CountryRegistrationViewModel
            {
                Name = "Tanzania",
                Code = "TZ",
                Abbreviation = "TZD",
                OfficialLanguage = "Swahili",
                Currency = "Shillings",
                AdminEmail = "Jmasengesho@kiramed.com",
                AdminPhoneNumber =  "4444",
                AdminFirstName = "Joe",
                AdminSecondName = "Mas",
                AdminNationalId = "34565432"
                
            };
            var result = countryContr.Post(model).Result;
            var okResult = Assert.IsType<OkObjectResult>(result);
            var feedback = Assert.IsType<FeedBack>(okResult.Value);
            Assert.Contains("id", feedback.data.ToLower());
            var data = JsonConvert.DeserializeObject<Dictionary<string, int>>(feedback.data);
            var coId = data["Id"];
            var created = await _countryRepository.FindAsync(coId);
            _countryRepository.Delete(created);
        }

        [Fact]
        public void OnCreate_ReturnsBadRequest_GivenInvalidModel()
        {
            var countryContr = _controller;
            countryContr.ModelState.AddModelError("Test Error", "This is just a test");
            var co = getTestCountry();
            var model = new CountryRegistrationViewModel(co);
            var result = countryContr.Post(model).Result;
            var badRequest = Assert.IsType<BadRequestObjectResult>(result);
            var returnFeedback = Assert.IsType<FeedBack>(badRequest.Value);
            Assert.Equal(true, returnFeedback.err);

        }

        [Fact]
        public async Task OnCreate_ShouldCheckIfCountryExists_AndReturnCountryId()
        {

            var countryContr = _controller;
            var model = new CountryRegistrationViewModel
            {
                Name = "Tanzania",
                Code = "TZ",
                Abbreviation = "TZD",
                OfficialLanguage = "Swahili",
                Currency = "Shillings"
            };
            var result = countryContr.Post(model).Result;

            var okResult = Assert.IsType<OkObjectResult>(result);
            var feedback = Assert.IsType<FeedBack>(okResult.Value);
            var data = JsonConvert.DeserializeObject<Dictionary<string, int>>(feedback.data);
            var coId = data["Id"];
            var created = await _countryRepository.FindAsync(coId);
            var createdResult = countryContr.Post(model).Result;
            var badRequest = Assert.IsType<BadRequestObjectResult>(createdResult);
            var newFeedback = Assert.IsType<FeedBack>(badRequest.Value);
            Assert.Equal(true, newFeedback.err);
            Assert.Contains("exists", newFeedback.message.ToLower());
            var newData = JsonConvert.DeserializeObject(newFeedback.data);
            Assert.Contains("id", newFeedback.data.ToLower());
            _countryRepository.Delete(created);

        }

        [Fact]
        public async void OnCreate_ShouldCheckIfOrganizationExists_IfMarkedDelReturnOK()
        {
            var countryContr = _controller;
            var model = new CountryRegistrationViewModel
            {
                Name = "Tanzania",
                Code = "TZ",
                Abbreviation = "TZD",
                OfficialLanguage = "Swahili",
                Currency = "Shillings"
            };
            var result = countryContr.Post(model).Result;

            var okResult = Assert.IsType<OkObjectResult>(result);
            var feedback = Assert.IsType<FeedBack>(okResult.Value);
            var data = JsonConvert.DeserializeObject<Dictionary<string, int>>(feedback.data);
            var coId = data["Id"];
            var created = await _countryRepository.FindAsync(coId);
            created.IsDeleted = DateTime.Now;
            await _countryRepository.UpdateAsync(created);

            var recreateResult = countryContr.Post(model).Result;
            var okrecreateResult = Assert.IsType<OkObjectResult>(recreateResult);
            var recreateFeedback = Assert.IsType<FeedBack>(okrecreateResult.Value);
            _countryRepository.Delete(created);
        }


        [Fact]
        public async Task  OnDelete_OrgShouldBeMarkedDeleted()
        {
            var results = await _countryRepository.GetAllAsync();
            var co = results.FirstOrDefault(c => c.IsDeleted > DateTime.Now);
            var contr = _controller;
            var result = contr.Delete(co.Id).Result;

            var updated = await _countryRepository.FindAsync(co.Id);

            Assert.Null(updated);
        }

        [Fact]
        public void OnDelete_ReturnsBadRequestIfOrgNE()
        {
            var coId = 911;
            var contr = _controller;
            var result = contr.Delete(coId).Result;

            var rst = Assert.IsType<BadRequestObjectResult>(result);
            var feedback = Assert.IsType<FeedBack>(rst.Value);
            Assert.Equal(true, feedback.err);
        }

        [Fact]
        public async Task OnUpdate_ReturnsBadRequestOnInValidateModel()
        {
            var contr = _controller;
            contr.ModelState.AddModelError("Test Error", "This is just a test");
            var rt = await _countryRepository.GetAllAsync();
            var co = rt.FirstOrDefault();
            var model = new CountryViewModel(co);
            var result = contr.Put(co.Id, model).Result;
            var badRequest = Assert.IsType<BadRequestObjectResult>(result);
            var feedback = Assert.IsType<FeedBack>(badRequest.Value);
            Assert.Equal(true, feedback.err);
        }

    }
}
