﻿using api;
using api.Auth;
using api.Auth.Roles;
using api.Db;
using api.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.TestHost;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using api.Controllers;
using Microsoft.VisualStudio.Web.CodeGeneration;
using Xunit;
using static apiTests.Modules.RoleModule.RoleControllerTests;

namespace apiTests.Modules.RoleModule
{
    public class RoleControllerTests : IClassFixture<TestFixture<Startup>>
    {

        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<ApplicationRole> _roleManager;
        private readonly IRoleRepository _roleRepository;
        private readonly KiraContext _context;


        private IEnumerator<ApplicationUser> _itirator;
        public RoleControllerTests(TestFixture<Startup> fixture)
        {
            var db = new DbContextOptionsBuilder<KiraContext>();
            db.UseInMemoryDatabase();
            _context = new KiraContext(db.Options);
            _context.Database.EnsureDeleted();

            var userStore = (IUserStore<ApplicationUser>)fixture.Server.Host.Services.GetService(typeof(IUserStore<ApplicationUser>));
            var roleStore = (IRoleStore<ApplicationRole>)fixture.Server.Host.Services.GetService(typeof(IRoleStore<ApplicationRole>));
            var passwordhasher = (IPasswordHasher<ApplicationUser>)fixture.Server.Host.Services.GetService(typeof(IPasswordHasher<ApplicationUser>));
            var passwordvalidator = (IEnumerable<IPasswordValidator<ApplicationUser>>)fixture.Server.Host.Services.GetService(typeof(IEnumerable<IPasswordValidator<ApplicationUser>>));
            var rolevalidator = (IEnumerable<IRoleValidator<ApplicationRole>>)fixture.Server.Host.Services.GetService(typeof(IEnumerable<IRoleValidator<ApplicationRole>>));
            var uservalidator = (IEnumerable<IUserValidator<ApplicationUser>>)fixture.Server.Host.Services.GetService(typeof(IEnumerable<IUserValidator<ApplicationUser>>));
            var optionAccess = (IOptions<IdentityOptions>)fixture.Server.Host.Services.GetService(typeof(IOptions<IdentityOptions>));
            var httpContextAccessor = (IHttpContextAccessor)fixture.Server.Host.Services.GetService(typeof(IHttpContextAccessor));
            var userPrincipleFactory = (IUserClaimsPrincipalFactory<ApplicationUser>)fixture.Server.Host.Services.GetService(typeof(IUserClaimsPrincipalFactory<ApplicationUser>));
            var userLogger = (ILogger<UserManager<ApplicationUser>>)fixture.Server.Host.Services.GetService(typeof(ILogger<UserManager<ApplicationUser>>));
            // var signInLogger = (ILogger<SignInManager<ApplicationUser>>)fixture.Server.Host.Services.GetService(typeof(ILogger<SignInManager<ApplicationUser>>));
            var roleLogger = (ILogger<RoleManager<ApplicationRole>>)fixture.Server.Host.Services.GetService(typeof(ILogger<RoleManager<ApplicationRole>>));
            var lookUpNormalizer = (ILookupNormalizer)fixture.Server.Host.Services.GetService(typeof(ILookupNormalizer));
            var serviceProvider = (IServiceProvider)fixture.Server.Host.Services.GetService(typeof(IServiceProvider));
            var identityErrorDescriber = (IdentityErrorDescriber)fixture.Server.Host.Services.GetService(typeof(IdentityErrorDescriber));

            _userManager = new UserManager<ApplicationUser>(userStore,
                optionAccess, passwordhasher, uservalidator, passwordvalidator, lookUpNormalizer, identityErrorDescriber, serviceProvider, userLogger);
            // _singInManager = new SignInManager<ApplicationUser>(_userManager, httpContextAccessor, userPrincipleFactory, optionAccess, signInLogger);
            _roleManager = new RoleManager<ApplicationRole>(roleStore, rolevalidator, lookUpNormalizer, identityErrorDescriber, roleLogger, httpContextAccessor);

            var systemActionRepo = new SystemActionRepository(_context);
            _roleRepository = new RoleRepository(_roleManager, _context, systemActionRepo);
            // _singInManager = new FakeSignInManager();

            DbInitializer.Init(_context, true);
            //AuthInitializer.Init(_userManager, _roleManager, _context);
           // AuthInitializer.Init(_idDbContext, _userManager, _roleManager, _context);


            //_itirator = getTesApplicationUsers().GetEnumerator();
        }



        //tests

        [Fact]
        public void OnCreateNewRole_ReturnsOkFeedBack()
        {
            var roleContr = new RoleController(_userManager, _roleRepository) ;
            var role = _roleRepository.FindByNameAsync("Nurse").Result;
            var newRole = new RoleViewModel
            {
                Name = "CHUK-Admin",
                Description = "Organization Admin",
                CreatedDate = DateTime.Now,
                OrganizationId = 1
            };

            //delete this role if exists
            var deleteIfExisits = _roleRepository.FindByNameAsync(newRole.Name).Result;
            if (deleteIfExisits != null)
            {
                var result1 = _roleManager.DeleteAsync(deleteIfExisits).Result;
            }

            var result = roleContr.Post(newRole).Result;

            var okResult = Assert.IsType<OkObjectResult>(result);
            var feedback = Assert.IsType<FeedBack>(okResult.Value);
            Assert.Contains("roleid", feedback.data.ToLower());
            var createdRole = _roleManager.FindByNameAsync(newRole.Name).Result;
            Assert.Equal(newRole.Name, createdRole.Name);
            Assert.Equal(newRole.Description, createdRole.Description);

            //delete the role
            var resultCleanUp = _roleRepository.DeleteAsync(createdRole).Result;
        }

        [Fact]
        public void OnCreateNewRole_ShouldHaveZeroUsers()
        {
            var roleContr = new RoleController(_userManager, _roleRepository);
            var role = _roleManager.FindByNameAsync("Nurse").Result;
            var newRole = new RoleViewModel
            {
                Name = "CHUK-Admin",
                Description = "Organization Admin",
                CreatedDate = DateTime.Now,
                OrganizationId = 1
            };

            //delete this role if exists
            var deleteIfExisits = _roleManager.FindByNameAsync(newRole.Name).Result;
            if (deleteIfExisits != null)
            {
                var result1 = _roleManager.DeleteAsync(deleteIfExisits).Result;
            }

            var result = roleContr.Post(newRole).Result;

            var okResult = Assert.IsType<OkObjectResult>(result);
            var feedback = Assert.IsType<FeedBack>(okResult.Value);
            Assert.Contains("roleid", feedback.data.ToLower());
            var createdRole = _roleManager.FindByNameAsync(newRole.Name).Result;
            Assert.Equal(createdRole.Users.Count, 0);

            //delete the role
            var resultCleanUp = _roleManager.DeleteAsync(createdRole).Result;
        }

        [Fact]
        public void OnCreateNewRole_ShouldHaveDescriptionAndOrg()
        {
            var roleContr = new RoleController(_userManager, _roleRepository);
            var role = _roleManager.FindByNameAsync("Nurse").Result;
            var newRole = new RoleViewModel
            {
                Name = "CHUK-Admin",
                Description = "Organization Admin",
                CreatedDate = DateTime.Now,
                OrganizationId = 1
            };

            //delete this role if exists
            var deleteIfExisits = _roleManager.FindByNameAsync(newRole.Name).Result;
            if (deleteIfExisits != null)
            {
                var result1 = _roleManager.DeleteAsync(deleteIfExisits).Result;
            }

            var result = roleContr.Post(newRole).Result;

            var okResult = Assert.IsType<OkObjectResult>(result);
            var feedback = Assert.IsType<FeedBack>(okResult.Value);
            Assert.Contains("roleid", feedback.data.ToLower());
            var createdRole = _roleManager.FindByNameAsync(newRole.Name).Result;
            Assert.NotNull(createdRole.CreatedDate);
            Assert.NotNull(createdRole.Description);
            Assert.NotNull(createdRole.OrganizationId);

            //delete the user
            var resultCleanUp = _roleManager.DeleteAsync(createdRole).Result;
        }

        [Fact]
        public void OnUpdateRole_ShouldBeUpdated()
        {
            var roleContr = new RoleController(_userManager, _roleRepository);
            var role = _roleManager.FindByNameAsync("Nurse").Result;
            var newRole = new RoleViewModel
            {
                Name = "CHUK-Admin",
                Description = "Organization Admin",
                CreatedDate = DateTime.Now,
                OrganizationId = 1
            };

            //delete this role if exists
            var deleteIfExisits = _roleManager.FindByNameAsync(newRole.Name).Result;
            if (deleteIfExisits != null)
            {
                var result1 = _roleManager.DeleteAsync(deleteIfExisits).Result;
            }
            var result = roleContr.Post(newRole).Result;

            var okResult = Assert.IsType<OkObjectResult>(result);
            var feedback = Assert.IsType<FeedBack>(okResult.Value);

            var data = JsonConvert.DeserializeObject<Dictionary<string, string>>(feedback.data);
            var roleId = data["RoleId"];
            var createdRole = _roleManager.FindByIdAsync(roleId).Result;

            var oldName = createdRole.Name;
            var oldDescription = createdRole.Description;
            //update 
            var updatedRole = new RoleViewModel
            {
                Name = "CHUK-Administration",
                Description = "Organization Administration",
            };

            var updatedResult =  roleContr.Put(roleId, updatedRole).Result;

            var okUpdateResult = Assert.IsType<OkObjectResult>(updatedResult);
            feedback = Assert.IsType<FeedBack>(okUpdateResult.Value);

            var UpdatedData = JsonConvert.DeserializeObject<Dictionary<string, string>>(feedback.data);
            roleId = data["RoleId"];
            var updated = _roleManager.FindByIdAsync(roleId).Result;

            Assert.NotEqual(oldName, updated.Name);
            Assert.NotEqual(oldDescription, updated.Description);
            Assert.Equal(updated.Name, "CHUK-Administration");
            Assert.Equal(updated.Description, "Organization Administration");

            //delete the user
            var resultCleanUp = _roleManager.DeleteAsync(updated).Result;

        }

        [Fact]
        public void OnGet_ShouldReturnRoleViewModels()
        {
            var roleContr = new RoleController(_userManager, _roleRepository);

            var newRole = new RoleViewModel
            {
                Name = "CHUK-Admin",
                Description = "Organization Admin",
                CreatedDate = DateTime.Now,
                OrganizationId = 1
            };

            //delete this role if exists
            var deleteIfExisits = _roleManager.FindByNameAsync(newRole.Name).Result;
            if (deleteIfExisits != null)
            {
                var result1 = _roleManager.DeleteAsync(deleteIfExisits).Result;
            }
            var resultCreate = roleContr.Post(newRole).Result;
            var result = roleContr.GetOrgRoles(1).Result;
            var okResult = Assert.IsType<OkObjectResult>(result);
            var roles = Assert.IsType<List<RoleViewModel>>(okResult.Value);
            Assert.Equal(true, roles.Count > 0);
            var resultCleanUp = _roleManager.DeleteAsync(_roleRepository.FindById(roles.FirstOrDefault().RoleId)).Result;
        }

        [Fact]
        public void OnGetId_ReturnsARoleViewModel()
        {
            var roleContr = new RoleController(_userManager, _roleRepository);

            var newRole = new RoleViewModel
            {
                Name = "CHUK-Admin",
                Description = "Organization Admin",
                CreatedDate = DateTime.Now,
                OrganizationId = 1
            };

            //delete this role if exists
            var deleteIfExisits = _roleManager.FindByNameAsync(newRole.Name).Result;
            if (deleteIfExisits != null)
            {
                var result1 = _roleManager.DeleteAsync(deleteIfExisits).Result;
            }
            var resultCreate = roleContr.Post(newRole).Result;

            var result = roleContr.GetOrgRoles(1).Result;

            var okRolesResult = Assert.IsType<OkObjectResult>(result);
            var roles = Assert.IsType<List<RoleViewModel>>(okRolesResult.Value);
            Assert.Equal(true, roles.Count > 0);
            var rst = roleContr.Details(roles.ToArray()[0].RoleId).Result;

            var okResult = Assert.IsType<OkObjectResult>(rst);
            var returnRole = Assert.IsType<RoleViewModel>(okResult.Value);
            var resultCleanUp = _roleManager.DeleteAsync(_roleRepository.FindById(roles.FirstOrDefault().RoleId)).Result;

        }

        [Fact]
        public async void OnAddActions_RoleShouldHaveClaims()
        {
            var roleContr = new RoleController(_userManager, _roleRepository);        
            var newRole = new RoleViewModel
            {
                Name = "CHUK-Admin",
                Description = "Organization Admin",
                CreatedDate = DateTime.Now,
                OrganizationId = 1
        
            };

            //delete this role if exists
            var deleteIfExisits = _roleRepository.FindByNameAsync(newRole.Name).Result;
            if (deleteIfExisits != null)
            {
                var result1 = _roleManager.DeleteAsync(deleteIfExisits).Result;
            }

            var result = roleContr.Post(newRole).Result;

            var okResult = Assert.IsType<OkObjectResult>(result);
            var feedback = Assert.IsType<FeedBack>(okResult.Value);
            Assert.Contains("roleid", feedback.data.ToLower());
            var createdRole = _roleManager.FindByNameAsync(newRole.Name).Result;
            Assert.Equal(newRole.Name, createdRole.Name);
            Assert.Equal(newRole.Description, createdRole.Description);

            //add claims
          
            var newAction = new SystemAction{ActionName = "Patients.View", ControllerName = "Patients"};
            _context.SystemActions.Add(newAction);
            _context.SaveChanges();
            var actions = _context.SystemActions.Where(a => a.ActionName.Contains("Patients")).Select(a => a.ActionName).ToList();
            var actionViewModel = new RoleActionViewModel(actions);
            var actionResult = await roleContr.AddActions(createdRole.Id, actionViewModel);
            createdRole = _roleRepository.FindByName(createdRole.Name);
            Assert.NotEmpty(createdRole.Claims);
            Assert.Equal(createdRole.Claims.FirstOrDefault().ClaimValue.ToString(), "Patients.View");

            //delete the role
            var resultCleanUp = roleContr.Delete(createdRole.Id).Result;
        }

        [Fact]
        public async void OnRemoveActions_RoleShouldHaveNoClaims()
        {
            var roleContr = new RoleController(_userManager, _roleRepository);
            var newRole = new RoleViewModel
            {
                Name = "CHUK-Admin",
                Description = "Organization Admin",
                CreatedDate = DateTime.Now,
                OrganizationId = 1
            };

            //delete this role if exists
            var deleteIfExisits = _roleRepository.FindByNameAsync(newRole.Name).Result;
            if (deleteIfExisits != null)
            {
                var result1 = _roleManager.DeleteAsync(deleteIfExisits).Result;
            }

            var result = roleContr.Post(newRole).Result;

            var okResult = Assert.IsType<OkObjectResult>(result);
            var feedback = Assert.IsType<FeedBack>(okResult.Value);
            Assert.Contains("roleid", feedback.data.ToLower());
            var createdRole = _roleManager.FindByNameAsync(newRole.Name).Result;
            Assert.Equal(newRole.Name, createdRole.Name);
            Assert.Equal(newRole.Description, createdRole.Description);

            //add claims

            var newAction = new SystemAction { ActionName = "Patients.View", ControllerName = "Patients" };
            _context.SystemActions.Add(newAction);
            _context.SaveChanges();
            var actions = _context.SystemActions.Where(a => a.ActionName.Contains("Patients")).Select(a => a.ActionName).ToList();
            var actionViewModel = new RoleActionViewModel(actions);

            var actionResult = await roleContr.AddActions(createdRole.Id, actionViewModel);

            createdRole = _roleRepository.FindByName(createdRole.Name);
            Assert.NotEmpty(createdRole.Claims);
            Assert.Equal(createdRole.Claims.FirstOrDefault().ClaimValue.ToString(), "Patients.View");

            var removeActionResults = await roleContr.RemoveActions(createdRole.Id, actionViewModel);
            Assert.Empty(createdRole.Claims);
            //delete the role
            var resultCleanUp = roleContr.Delete(createdRole.Id).Result;
        }

        //[Fact]
        public void OnDelete_ReturnsOkResult()
        {
            var roleContr = new RoleController(_userManager, _roleRepository);
            var result = roleContr.GetOrgRoles(1).Result;
            var okRolesResult = Assert.IsType<OkObjectResult>(result);
            var roles = Assert.IsType<List<RoleViewModel>>(okRolesResult.Value);
            Assert.Equal(true, roles.Count > 0);
            var rst = roleContr.Delete(roles.ToArray()[0].RoleId).Result;
            var okResult = Assert.IsType<OkObjectResult>(rst);
        }

        public class TestFixture<TStartup> : IDisposable where TStartup : class
        {
            public readonly TestServer Server;
            private readonly HttpClient _client;


            public TestFixture()
            {
                var builder = new WebHostBuilder()
                    .UseContentRoot($"..\\..\\..\\..\\..\\api\\api\\")
                    .UseStartup<TStartup>();

                Server = new TestServer(builder);
                _client = new HttpClient();
            }


            public void Dispose()
            {
                _client.Dispose();
                Server.Dispose();
            }
        }
    }   
}
