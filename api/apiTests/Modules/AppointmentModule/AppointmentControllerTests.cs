﻿using System.Linq;
using System.Collections.Generic;
using Xunit;
using Moq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using api.Db;
using api.Models;
using api.Modules.PatientModule.Controllers;
using Microsoft.AspNetCore.Mvc;
using System;
using Newtonsoft.Json;
using api.ViewModels;
using api.Controllers;
using api.Repositories;
using api.Services;
using api.Utils;

namespace apiTests.Modules
{
    public class AppointmentControllerTests
    {
        private KiraContext _context;
        private IEnumerator<Appointment> _itirator;
        private AppointmentRepository _appointmentRepository;
        private readonly AppointmentService _appointmentService;
        private readonly VisitQueueRepository _visitQueueRepository;


        public AppointmentControllerTests()
        {
            var db = new DbContextOptionsBuilder<KiraContext>();
            db.UseInMemoryDatabase();
            _context = new KiraContext(db.Options);
            _context.Database.EnsureDeleted();

            _appointmentRepository = new AppointmentRepository(_context);
            _visitQueueRepository = new VisitQueueRepository(_context);
            _appointmentService = new AppointmentService(_appointmentRepository, _visitQueueRepository);

            DbInitializer.Init(_context, true);
        
            _itirator = GetTestAppointments().GetEnumerator();
        }

        public IEnumerable<Appointment> GetTestAppointments()
        {
            var appoint1 = new Appointment
            {
                DateTimeCreated = new DateTime(2017, 8, 6),
                IsDeleted = DateTime.MaxValue,
                ArrivalDateTime = new DateTime(2018, 12, 1),
                OrganizationId = 1,
                FirstName = "Joseph",
                LastName = "Masengesho",
                Email = "Jmasengesho@kiramed.com",
                PhoneNumber = "4056029804",
                VisitReasons = new List<VisitReason> { new VisitReason { RelatedKnownIssue = 10, Reason = "I been shitting on myself!" } },
                Relationship = ePatientRelationShip.Self
            };

            var appoint2 = new Appointment
            {
                DateTimeCreated = new DateTime(2017, 8, 6),
                IsDeleted = DateTime.MaxValue,
                ArrivalDateTime = new DateTime(2017, 11, 1),
                OrganizationId = 1,
                FirstName = "Kamana",
                LastName = "Tanazi",
                Email = "Jmasengesho@inyarwanda.com",
                PhoneNumber = "4056028888",
                VisitReasons = new List<VisitReason> { new VisitReason { RelatedKnownIssue = 10, Reason = "My father cant remember my name" } },
                Relationship = ePatientRelationShip.Parent
            };

            var appointments = new List<Appointment> { appoint1, appoint2 };
            

            for (var idx = 0; idx < 3; idx++)
            {
                yield return appointments[idx]; ;
            }
        }

        public Appointment getTestAppointment()
        {
            if (this._itirator.MoveNext())
                return this._itirator.Current;

            return null;
        }

        [Fact]
        public async Task OnGet_ReturnsAListOfAppointmentVModels()
        {
            var appointmentContr = new AppointmentController(_appointmentService);

            var result = await appointmentContr.Get();
            var okResult = Assert.IsType<OkObjectResult>(result);
            var returnPatients = Assert.IsType<List<AppointmentViewModel>>(okResult.Value);
            Assert.Equal(true, returnPatients.Any());

        }

        [Fact]
        public async Task OnGetId_ReturnsAOrgVModel()
        {
            var appointmentContr = new AppointmentController(_appointmentService);
            var appointment = await _appointmentService.FindAsync(1);

            var result = appointmentContr.Detail(appointment.Id).Result;

            var okResult = Assert.IsType<OkObjectResult>(result);
            var returnPat = Assert.IsType<AppointmentViewModel>(okResult.Value);
        }


        [Fact]
        public void OnGetIdIfAppointmentIdNE_ReturnsHttpNotFound()
        {
            var appointmentContr = new AppointmentController(_appointmentService);
            var testId = 911;

            var result = appointmentContr.Detail(testId).Result;

            var notFoundResult = Assert.IsType<NotFoundObjectResult>(result);
            var returnFeedback = Assert.IsType<FeedBack>(notFoundResult.Value);
            Assert.Equal(true, returnFeedback.err);
        }

        [Fact]
        public void OnGetIdIfAppointmentIDNE_ReturnsHttpNotFound()
        {
            var appointmentContr = new AppointmentController(_appointmentService);
            var testId = 11111;

            var result = appointmentContr.Detail(testId).Result;

            var notFoundResult = Assert.IsType<NotFoundObjectResult>(result);
            var returnFeedback = Assert.IsType<FeedBack>(notFoundResult.Value);
            Assert.Equal(true, returnFeedback.err);
        }

        [Fact]
        public async Task OnCreate_ReturnsOkResultNFeedbackWithCreatedAppointmentId()
        {
            var appointmentContr = new AppointmentController(_appointmentService);
            var apt  = getTestAppointment();
            var model = new AppointmentCreationViewModel(apt);
            var result = appointmentContr.Post(model).Result;
            var okResult = Assert.IsType<OkObjectResult>(result);
            var feedback = Assert.IsType<FeedBack>(okResult.Value);
            Assert.Contains("id", feedback.data.ToLower());
            var data = JsonConvert.DeserializeObject<Dictionary<string, int>>(feedback.data);
            var coId = data["id"];
            var created = await _appointmentService.FindAsync(coId);
            await appointmentContr.Delete(created.Id);
        }

        [Fact]
        public void OnCreate_ReturnsBadRequest_GivenInvalidModel()
        {
            var appointmentContr = new AppointmentController(_appointmentService);
            appointmentContr.ModelState.AddModelError("Test Error", "This is just a test");
            var apt = getTestAppointment();
            var model = new AppointmentCreationViewModel(apt);
            var result = appointmentContr.Post(model).Result;
            var badRequest = Assert.IsType<BadRequestObjectResult>(result);
            var returnFeedback = Assert.IsType<FeedBack>(badRequest.Value);
            Assert.Equal(true, returnFeedback.err);

        }

        [Fact]
        public async Task OnCreate_ShouldCheckIfAppointmentExists_AndReturnAppointmentId()
        {

            var appointmentContr = new AppointmentController(_appointmentService);
            var apt = getTestAppointment();
            var model = new AppointmentCreationViewModel(apt);

            var result = appointmentContr.Post(model).Result;

            var okResult = Assert.IsType<OkObjectResult>(result);
            var feedback = Assert.IsType<FeedBack>(okResult.Value);
            var data = JsonConvert.DeserializeObject<Dictionary<string, int>>(feedback.data);
            var coId = data["id"];

            var created = await _appointmentService.FindAsync(coId);

            var createdResult = appointmentContr.Post(model).Result;

            var badRequest = Assert.IsType<BadRequestObjectResult>(createdResult);
            var newFeedback = Assert.IsType<FeedBack>(badRequest.Value);
            Assert.Equal(true, newFeedback.err);

            Assert.Contains("you already have an appointment scheduled around that time.", newFeedback.message.ToLower());
            var newData = JsonConvert.DeserializeObject(newFeedback.data);
            Assert.Contains("id", newFeedback.data.ToLower());
            _appointmentRepository.Delete(created);

        }

        //[Fact]
        //public async void OnCreate_ShouldCheckIfAppointmentExists_IfMarkedDelReturnOK()
        //{
        //    var appointmentContr = new AppointmentController(_appointmentService);
        //    var apt = getTestAppointment();
        //    var model = new AppointmentCreationViewModel(apt);
        //    var result = appointmentContr.Post(model).Result;

        //    var okResult = Assert.IsType<OkObjectResult>(result);
        //    var feedback = Assert.IsType<FeedBack>(okResult.Value);
        //    var data = JsonConvert.DeserializeObject<Dictionary<string, int>>(feedback.data);
        //    var coId = data["id"];
        //    var created = await  _appointmentService.FindAsync(coId);
        //    created.IsDeleted = DateTime.Now;

        //    await _appointmentService.UpdateAsync(created);

        //    var recreateResult = appointmentContr.Post(model).Result;
        //    var okrecreateResult = Assert.IsType<OkObjectResult>(recreateResult);
        //    var recreateFeedback = Assert.IsType<FeedBack>(okrecreateResult.Value);
        //    await appointmentContr.Delete(created.Id);
        //}


        [Fact]
        public async Task OnDelete_OrgShouldBeMarkedDeleted()
        {
            var co =  _appointmentService.GetAsync(c => c.IsDeleted > DateTime.Now).Result.FirstOrDefault();
            var contr = new AppointmentController(_appointmentService);
            var result = contr.Delete(co.Id).Result;

            var updated = await _appointmentService.FindAsync(co.Id);

            var rst = Assert.IsType<OkObjectResult>(result);
            var feedback = Assert.IsType<FeedBack>(rst.Value);
            Assert.NotEqual(updated.IsDeleted, DateTime.MaxValue);
        }

        [Fact]
        public async Task OnUpdate_AppointmentDateShouldBeChanged()
        {
            var apt = _appointmentService.GetAsync(c => c.IsDeleted > DateTime.Now).Result.FirstOrDefault();
            var contr = new AppointmentController(_appointmentService);
            var oldDate = apt.ArrivalDateTime;
            apt.ArrivalDateTime = DateTime.Now;
            var newModel = new AppointmentViewModel(apt);

            var updatedResult = await contr.Put(apt.Id, newModel);
            var updated = await _appointmentService.FindAsync(apt.Id);

            var rst = Assert.IsType<OkObjectResult>(updatedResult);
            var feedback = Assert.IsType<FeedBack>(rst.Value);
            Assert.NotEqual(updated.ArrivalDateTime, oldDate);
        }

        [Fact]
        public async Task OnUpdate_PhoneNumberShouldBeChanged()
        {
            var apt = _appointmentService.GetAsync(c => c.IsDeleted > DateTime.Now).Result.FirstOrDefault();
            var contr = new AppointmentController(_appointmentService);
            var oldNumber = apt.PhoneNumber;
            apt.PhoneNumber = "250-567-5678";
            var newModel = new AppointmentViewModel(apt);

            var updatedResult = await contr.Put(apt.Id, newModel);
            var updated = await _appointmentService.FindAsync(apt.Id);

            var rst = Assert.IsType<OkObjectResult>(updatedResult);
            var feedback = Assert.IsType<FeedBack>(rst.Value);
            Assert.NotEqual(updated.PhoneNumber, oldNumber);
        }


        //[Fact]
        //public async Task OnUpdate_NewVisitReasonShouldBeAdded()
        //{
        //    var apt = _appointmentService.GetAsync(c => c.IsDeleted > DateTime.Now).Result.FirstOrDefault();
        //    var contr = new AppointmentController(_appointmentService);
        //    var oldNumber = apt.PhoneNumber;
        //    apt.PhoneNumber = "250-567-5678";
        //    var newModel = new AppointmentViewModel(apt);

        //    var updatedResult = await contr.Put(apt.Id, newModel);
        //    var updated = await _appointmentService.FindAsync(apt.Id);

        //    var rst = Assert.IsType<OkObjectResult>(updatedResult);
        //    var feedback = Assert.IsType<FeedBack>(rst.Value);
        //    Assert.NotEqual(updated.PhoneNumber, oldNumber);
        //}

        [Fact]
        public void OnDelete_ReturnsBadRequestIfCountryNE()
        {
            var coId = 911;
            var contr = new AppointmentController(_appointmentService);
            var result = contr.Delete(coId).Result;

            var rst = Assert.IsType<BadRequestObjectResult>(result);
            var feedback = Assert.IsType<FeedBack>(rst.Value);
            Assert.Equal(true, feedback.err);
        }

        [Fact]
        public async Task OnUpdate_ReturnsBadRequestOnInValidateModel()
        {
            var contr = new AppointmentController(_appointmentService);
            contr.ModelState.AddModelError("Test Error", "This is just a test");
            var co = _appointmentService.GetAllAsync().Result.FirstOrDefault();
            var model = new AppointmentViewModel(co);
            var result = await contr.Put(co.Id, model);
            var badRequest = Assert.IsType<BadRequestObjectResult>(result);
            var feedback = Assert.IsType<FeedBack>(badRequest.Value);
            Assert.Equal(true, feedback.err);
        }

    }
}
