﻿using System.Linq;
using System.Collections.Generic;
using Xunit;
using Moq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using api.Db;
using api.Models;
using api.Modules.PatientModule.Controllers;
using Microsoft.AspNetCore.Mvc;
using System;
using api.Repositories;

namespace apiTests.Modules.PatientModule
{
    public class PatientSearchControllerTests
    {
        private KiraContext _context;
        private IEnumerator<Patient> _itirator;
        private readonly PatientRepository _patientRepository;
        public PatientSearchControllerTests()
        {
            var db = new DbContextOptionsBuilder<KiraContext>();
            db.UseInMemoryDatabase();
            _context = new KiraContext(db.Options);
            _context.Database.EnsureDeleted();

            DbInitializer.Init(_context, true);

            _itirator = getTestPatients().GetEnumerator();

            _patientRepository = new PatientRepository(_context);
        }

        public IEnumerable<Patient> getTestPatients()
        {
            var dem = new EntityDemographic { Sex = "F", DOB = new DateTime(1945, 1, 1), NationalID = "5670485555" };
            var dem2 = new EntityDemographic { Sex = "M", DOB = new DateTime(1944, 12, 12), NationalID = "9978586998" };
            var dem3 = new EntityDemographic { Sex = "M", DOB = new DateTime(1933, 1, 1), NationalID = "564766672" };
            var dem4 = new EntityDemographic { Sex = "F", DOB = new DateTime(1922, 12, 12), NationalID = "9008586998" };
            var dem5 = new EntityDemographic { Sex = "M", DOB = new DateTime(1991, 1, 1), NationalID = "56477772" };
            var dem6 = new EntityDemographic { Sex = "F", DOB = new DateTime(1919, 12, 12), NationalID = "9971026998" };
            var dem7 = new EntityDemographic { Sex = "F", DOB = new DateTime(1916, 1, 1), NationalID = "560029572" };
            var dem8 = new EntityDemographic { Sex = "F", DOB = new DateTime(1923, 12, 12), NationalID = "3228586998" };
            var dem9 = new EntityDemographic { Sex = "M", DOB = new DateTime(1996, 1, 1), NationalID = "564739511" };
            var dem10 = new EntityDemographic { Sex = "M", DOB = new DateTime(1991, 12, 12), NationalID = "9978509438" };

            var addr = new EntityAddress { Country = "RW", Address1 = "Kamutwe Zone 1", City = "Byo", Label = "Home", District = "Muhoro" };

            var ent1 = new Entity { ExternalId = "ID969", Type = "P", Category = "P", Firstname = "Poko", Lastname = "Atta", Demographic = dem, Addresses = new List<EntityAddress> { addr } };
            var ent2 = new Entity { ExternalId = "ID648", Type = "P", Category = "P", Firstname = "Timba", Lastname = "Kaka", Demographic = dem2 };
            var ent3 = new Entity { ExternalId = "ID922", Type = "P", Category = "P", Firstname = "Jean", Lastname = "Noma", Demographic = dem3 };
            var ent4 = new Entity { ExternalId = "ID633", Type = "P", Category = "P", Firstname = "Papi", Lastname = "Kaka", Demographic = dem4 };
            var ent5 = new Entity { ExternalId = "ID944", Type = "P", Category = "P", Firstname = "Polete", Lastname = "Noma", Demographic = dem5 };
            var ent6 = new Entity { ExternalId = "ID655", Type = "P", Category = "P", Firstname = "Keba", Lastname = "Kaka", Demographic = dem6 };
            var ent7 = new Entity { ExternalId = "ID966", Type = "P", Category = "P", Firstname = "Nayo", Lastname = "Noma", Demographic = dem7 };
            var ent8 = new Entity { ExternalId = "ID677", Type = "P", Category = "P", Firstname = "Mwana", Lastname = "Kaka", Demographic = dem8 };
            var ent9 = new Entity { ExternalId = "ID988", Type = "P", Category = "P", Firstname = "Mboka", Lastname = "Noma", Demographic = dem9 };
            var ent10 = new Entity { ExternalId = "ID699", Type = "P", Category = "P", Firstname = "Pepe", Lastname = "Kaka", Demographic = dem10 };

            var pat1 = new api.Models.Patient { MedicalRecordNumber = "6738457646", AllergyNote = "Not too bad of alrg.", Entity = ent1, NationalPatientId = "BU685730456" };
            var pat2 = new api.Models.Patient { MedicalRecordNumber = "674113322", AllergyNote = "No note.", Entity = ent2, NationalPatientId = "RW7757489222" };
            var pat3 = new api.Models.Patient { MedicalRecordNumber = "6738457633", AllergyNote = "Not too bad of alrg.", Entity = ent3, NationalPatientId = "RW7757489333" };
            var pat4 = new api.Models.Patient { MedicalRecordNumber = "6741144", AllergyNote = "No note.", Entity = ent4, NationalPatientId = "RW7757484444" };
            var pat5 = new api.Models.Patient { MedicalRecordNumber = "6738457555", AllergyNote = "Not too bad of alrg.", Entity = ent5, NationalPatientId = "RW7757455555" };
            var pat6 = new api.Models.Patient { MedicalRecordNumber = "67411666", AllergyNote = "No note.", Entity = ent6, NationalPatientId = "RW7757486666" };
            var pat7 = new api.Models.Patient { MedicalRecordNumber = "6738457677", AllergyNote = "Not too bad of alrg.", Entity = ent7, NationalPatientId = "RW7757489777" };
            var pat8 = new api.Models.Patient { MedicalRecordNumber = "674113388", AllergyNote = "No note.", Entity = ent10, NationalPatientId = "RW7757488888" };
            var pat9 = new api.Models.Patient { MedicalRecordNumber = "6738457699", AllergyNote = "Not too bad of alrg.", Entity = ent8, NationalPatientId = "RW77574899999" };
            var pat10 = new api.Models.Patient { MedicalRecordNumber = "674113300", AllergyNote = "No note.", Entity = ent9, NationalPatientId = "RW7757489000" };

            var patients = new List<api.Models.Patient>() { pat1, pat2, pat3, pat4, pat5, pat6, pat7, pat8, pat9, pat10 };

            for (var idx = 0; idx < 10; idx++)
            {
                yield return patients[idx];
            }
        }

        public Patient getTestPat()
        {
            if (this._itirator.MoveNext())
                return this._itirator.Current;

            return null;
        }

        [Fact]
        public async void OnSearchAvailableFirstName_ReturnsListOfPatSearchVMode()
        {
            var patSearchContr = new PatientSearchController(_patientRepository);
            var searchString = "Yohana";

            var actualResults = await patSearchContr.Search(searchString);
            var expecedResults = new PatientSearchViewModel(_context.Patients.FirstOrDefault(p => p.MedicalRecordNumber == "345822940"));

            var OkResult = Assert.IsType<OkObjectResult>(actualResults);
            var returnPatients = Assert.IsType<List<PatientSearchViewModel>>(OkResult.Value);

            Assert.Equal(returnPatients.FirstOrDefault().Id, expecedResults.Id);
            Assert.Equal(returnPatients.FirstOrDefault().EntityId, expecedResults.EntityId);

        }

        [Fact]
        public async void OnSearchAvailableLastName_ReturnsListOfPatSearchVModel()
        {
            var patSearchContr = new PatientSearchController(_patientRepository);
            var searchString = "matayo";

            var actualResults = await patSearchContr.Search(searchString);
            var expecedResults = new PatientSearchViewModel(_context.Patients.FirstOrDefault(p => p.MedicalRecordNumber == "345822940"));

            var OkResult = Assert.IsType<OkObjectResult>(actualResults);
            var returnPatients = Assert.IsType<List<PatientSearchViewModel>>(OkResult.Value);

            Assert.Equal(returnPatients.FirstOrDefault().Id, expecedResults.Id);
            Assert.Equal(returnPatients.FirstOrDefault().EntityId, expecedResults.EntityId);
        }

        [Fact]
        public async void OnSearchNotAvailableName_ReturnsNotFoundResponse()
        {
            var patSearchContr = new PatientSearchController(_patientRepository);
            var searchString = "Matayu";

            var actualResults = await patSearchContr.Search(searchString);
            var OkResult = Assert.IsType<NotFoundObjectResult>(actualResults);
        }

        [Fact]
        public async void OnSearchWhiteSpace_ReturnsBadRequestResponse()
        {
            var patSearchContr = new PatientSearchController(_patientRepository);
            var searchString = " ";
            var actualResults = await patSearchContr.Search(searchString);
            var OkResult = Assert.IsType<BadRequestObjectResult>(actualResults);
        }

        [Fact]
        public async void OnSearchEmptyStiring_ReturnsBadRequestResponse()
        {
            var patSearchContr = new PatientSearchController(_patientRepository);
            var searchString = "";
            var actualResults = await patSearchContr.Search(searchString);
            var OkResult = Assert.IsType<BadRequestObjectResult>(actualResults);
        }

        [Fact]
        public async void OnSearchVeryLongString_ReturnsBadRequestResponse()
        {
            var patSearchContr = new PatientSearchController(_patientRepository);
            var searchString = " But I must explain to you how all this mistaken idea of denouncing of a pleasure and praising pain " +
                "was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of " +
                "the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself";
            var actualResults = await patSearchContr.Search(searchString);
            var OkResult = Assert.IsType<BadRequestObjectResult>(actualResults);
        }

        [Fact]
        public async void OnSearchAvailableNationalPatientID_ReturnsListOfPatSearchVModel()
        {
            var patSearchContr = new PatientSearchController(_patientRepository);
            var searchString = "rW7757";
            var actualResults = await patSearchContr.Search(searchString);
            var expecedResults = new List<PatientSearchViewModel>();
            var OkResult = Assert.IsType<OkObjectResult>(actualResults);
            var returnPatients = Assert.IsType<List<PatientSearchViewModel>>(OkResult.Value);
            foreach (var pat in await _context.Patients.Where(p => p.NationalPatientId.ToLower().Contains(searchString.ToLower())).ToListAsync())
            {
                expecedResults.Add(new PatientSearchViewModel(pat));
            }
            ComparePatientSearchViewModel(returnPatients, expecedResults);
        }

        [Fact]
        public async void OnSearchAvailablePatientTrackNumber_ReturnsListOfPatSearchVModel()
        {
            var patSearchContr = new PatientSearchController(_patientRepository);
            var searchString = "37434";

            var actualResults = await patSearchContr.Search(searchString);
            var expecedResults = new List<PatientSearchViewModel>();

            var OkResult = Assert.IsType<OkObjectResult>(actualResults);
            var returnPatients = Assert.IsType<List<PatientSearchViewModel>>(OkResult.Value);

            foreach (var pat in _context.Patients.Where(p => p.TracknetNumber.Contains(searchString)).ToList())
            {
                expecedResults.Add(new PatientSearchViewModel(pat));
            }

            ComparePatientSearchViewModel(returnPatients, expecedResults);
        }

        [Fact]
        public async void OnSearchAvailablePatientDBOFormat1_ReturnsListOfPatSearchVModel()
        {
            var patSearchContr = new PatientSearchController(_patientRepository);
            var searchString = "12/12/1970";

            var actualResults = await patSearchContr.Search(searchString);
            var expecedResults = new List<PatientSearchViewModel>();

            var OkResult = Assert.IsType<OkObjectResult>(actualResults);
            var returnPatients = Assert.IsType<List<PatientSearchViewModel>>(OkResult.Value);

            foreach (var pat in _context.Patients.Where(p => p.MedicalRecordNumber.Contains("666666666")).ToList())
            {
                expecedResults.Add(new PatientSearchViewModel(pat));
            }
            ComparePatientSearchViewModel(returnPatients, expecedResults);
        }

        [Fact]
        public async void OnSearchAvailablePatientDBOFormat2_ReturnsListOfPatSearchVModel()
        {
            var patSearchContr = new PatientSearchController(_patientRepository);
            var searchString = "12-12-1970";

            var actualResults = await patSearchContr.Search(searchString);
            var expecedResults = new List<PatientSearchViewModel>();

            var OkResult = Assert.IsType<OkObjectResult>(actualResults);
            var returnPatients = Assert.IsType<List<PatientSearchViewModel>>(OkResult.Value);

            foreach (var pat in _context.Patients.Where(p => p.MedicalRecordNumber.Contains("666666666")).ToList())
            {
                expecedResults.Add(new PatientSearchViewModel(pat));
            }
            ComparePatientSearchViewModel(returnPatients, expecedResults);
        }

        [Fact]
        public async void OnSearchAvailablePatientDBOFormat3_ReturnsListOfPatSearchVModel()
        {
            var patSearchContr = new PatientSearchController(_patientRepository);
            var searchString = "1970/12/12";

            var actualResults = await patSearchContr.Search(searchString);
            var expecedResults = new List<PatientSearchViewModel>();

            var OkResult = Assert.IsType<OkObjectResult>(actualResults);
            var returnPatients = Assert.IsType<List<PatientSearchViewModel>>(OkResult.Value);

            foreach (var pat in _context.Patients.Where(p => p.MedicalRecordNumber.Contains("666666666")).ToList())
            {
                expecedResults.Add(new PatientSearchViewModel(pat));
            }
            ComparePatientSearchViewModel(returnPatients, expecedResults);
        }

        [Fact]
        public async void OnSearchAvailablePatientDBOFormat4_ReturnsListOfPatSearchVModel()
        {
            var patSearchContr = new PatientSearchController(_patientRepository);
            var searchString = "1933/06/06";

            var actualResults = await patSearchContr.Search(searchString);
            var expecedResults = new List<PatientSearchViewModel>();

            var OkResult = Assert.IsType<OkObjectResult>(actualResults);
            var returnPatients = Assert.IsType<List<PatientSearchViewModel>>(OkResult.Value);

            foreach (var pat in _context.Patients.Where(p => p.MedicalRecordNumber.Contains("999999999")).ToList())
            {
                expecedResults.Add(new PatientSearchViewModel(pat));
            }
            ComparePatientSearchViewModel(returnPatients, expecedResults);
        }

        [Fact]
        public async void OnSearchAvailablePatientDBOWrongDateFormat_ReturnsListOfPatSearchVModel()
        {
            var patSearchContr = new PatientSearchController(_patientRepository);
            var searchString = "1933/18/06";

            var actualResults = await patSearchContr.Search(searchString);

            var OkResult = Assert.IsType<NotFoundObjectResult>(actualResults);

        }

        [Fact]
        public async void OnSearchAvailableMedicalRecordNumber_ReturnsListOfPatSearchVModel()
        {
            var patSearchContr = new PatientSearchController(_patientRepository);
            var searchString = "999999";

            var actualResults = await patSearchContr.Search(searchString);
            var expecedResults = new List<PatientSearchViewModel>();

            var OkResult = Assert.IsType<OkObjectResult>(actualResults);
            var returnPatients = Assert.IsType<List<PatientSearchViewModel>>(OkResult.Value);

            foreach (var pat in _context.Patients.Where(p => p.MedicalRecordNumber.Contains("999999999")).ToList())
            {
                expecedResults.Add(new PatientSearchViewModel(pat));
            }
            ComparePatientSearchViewModel(returnPatients, expecedResults);
        }

        [Fact]
        public async void OnSearchAvailableNationalID_ReturnsListOfPatSearchVModel()
        {
            var patSearchContr = new PatientSearchController(_patientRepository);
            var searchString = "6748505453";

            var actualResults = await patSearchContr.Search(searchString);
            var expecedResults = new List<PatientSearchViewModel>();

            var OkResult = Assert.IsType<OkObjectResult>(actualResults);
            var returnPatients = Assert.IsType<List<PatientSearchViewModel>>(OkResult.Value);

            foreach (var pat in _context.Patients.Where(p => p.MedicalRecordNumber.Contains("999999999")).ToList())
            {
                expecedResults.Add(new PatientSearchViewModel(pat));
            }
            ComparePatientSearchViewModel(returnPatients, expecedResults);
        }

        [Fact]
        public async void OnSearchAvailablePhoneNumber_ReturnsListOfPatSearchVModel()
        {
            var patSearchContr = new PatientSearchController(_patientRepository);
            var searchString = "405-435-99999";

            var actualResults = await patSearchContr.Search(searchString);
            var expecedResults = new List<PatientSearchViewModel>();

            var OkResult = Assert.IsType<OkObjectResult>(actualResults);
            var returnPatients = Assert.IsType<List<PatientSearchViewModel>>(OkResult.Value);

            foreach (var pat in _context.Patients.Where(p => p.MedicalRecordNumber.Contains("345822940")).ToList())
            {
                expecedResults.Add(new PatientSearchViewModel(pat));
            }
            ComparePatientSearchViewModel(returnPatients, expecedResults);
        }

        [Fact]
        public async void OnSearchShortString_ReturnsListOfPatSearchVModel()
        {
            var patSearchContr = new PatientSearchController(_patientRepository);
            var searchString = "AP";

            var actualResults = await patSearchContr.Search(searchString);

            var OkResult = Assert.IsType<NotFoundObjectResult>(actualResults);

        }

        [Fact]
        public async void SearchPatientMedicalRecNumberAndNatinalPatientIDTest()
        {
            var patSearchContr = new PatientSearchController(_patientRepository);
            var searchString = "111111";

            //update a patient medicalrecnumber 
            var testPat1 = (await _context.Patients.FirstOrDefaultAsync(p => p.MedicalRecordNumber.Contains("999999999")));
            testPat1.MedicalRecordNumber = testPat1.MedicalRecordNumber + searchString;

            var testPat2 = (await _context.Patients.FirstOrDefaultAsync(p => p.MedicalRecordNumber.Contains("34582294")));
            testPat2.MedicalRecordNumber = testPat2.MedicalRecordNumber + searchString;

            //search patients
            var actualResults = await patSearchContr.Search(searchString);
            var expecedResults = new List<PatientSearchViewModel>();

            var OkResult = Assert.IsType<OkObjectResult>(actualResults);
            var returnPatients = Assert.IsType<List<PatientSearchViewModel>>(OkResult.Value);

            foreach (var pat in _context.Patients.Where(p => p.MedicalRecordNumber.Contains("999999999") || p.MedicalRecordNumber.Contains("34582294")).ToList())
            {
                expecedResults.Add(new PatientSearchViewModel(pat));
            }
            testPat1.MedicalRecordNumber = "999999999";
            testPat2.MedicalRecordNumber = "34582294";
            ComparePatientSearchViewModel(returnPatients, expecedResults);

        }

        #region
        public void ComparePatientSearchViewModel(List<PatientSearchViewModel> actual, List<PatientSearchViewModel> expected)
        {
            Assert.Equal(actual.Count(), expected.Count());
            foreach (var exp in expected)
            {
                var act = actual.FirstOrDefault(a => a.Id == exp.Id);
                Assert.NotNull(act);
                Assert.Equal(exp.EntityId, act.EntityId);
            }
        }

        #endregion
    }
}