﻿using System.Linq;
using System.Collections.Generic;
using Xunit;
using Moq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using api.Db;
using api.Models;
using api.Modules.PatientModule.Controllers;
using Microsoft.AspNetCore.Mvc;
using System;
using Newtonsoft.Json;
using api.ViewModels;
using api.Controllers;
using api.Utils;
using api.Services;
using apiTests. TestSetUp;
using api;

namespace apiTests.Modules.OrganizationModule
{
    public class OrganizationControllerTests : UserServiceTestSeUp
    {
       
        private IEnumerator<Organization> _itirator;
        
        private IVisitService _visitService;

        private readonly OrganizationController _orgController;


        public OrganizationControllerTests(TestSetUp.TestFixture<Startup> fixture) : base(fixture)
        {
         
            _orgController = new OrganizationController(_organizationRepository, _context, _organizationService);
            _itirator = getTestVisitanizations().GetEnumerator();
        }

        public IEnumerable<Organization> getTestVisitanizations()
        {
            var orgCountry = new Country { Name = "Rwandaa", Abbreviation = "RW", Code = "250", Id = 1, };


            var contactType1 = new ContactType { Name = "Phone", Type = 1 };
            var contactType2 = new ContactType { Name = "Email", Type = 2 };
            var contactType3 = new ContactType { Name = "Twitter", Type = 3 };

            string org1Id = IDGenerator.GenerateAlphaNumericID(10);
            string org2Id = IDGenerator.GenerateAlphaNumericID(10); 
            string org3Id = IDGenerator.GenerateAlphaNumericID(10); 

            var orgAddr1 = new EntityAddress { Country = orgCountry.Abbreviation, Address1 = "Kamutwe Zone 1", City = "Kigali", Label = "Home", District = "Gasabo", Id =1};
            var orgAddr2 = new EntityAddress { Country = orgCountry.Abbreviation, Address1 = "Kamuduha Quartier 123", City = "Kigali", Label = "Home", District = "Gasabo", Id = 2 };
            var orgAddr3 = new EntityAddress { Country = orgCountry.Abbreviation, Address1 = "Mbalaba 432", City = "Kigali", Label = "Home", District = "Gasabo", Id = 3 };

            var orgDem1 = new EntityDemographic { Sex = "", DOB = new DateTime(1990, 1, 1), NationalID = org1Id, Id = 1};
            var orgDem2 = new EntityDemographic { Sex = "", DOB = new DateTime(1990, 1, 1), NationalID = org2Id, Id = 2 };
            var orgDem3 = new EntityDemographic { Sex = "", DOB = new DateTime(1990, 1, 1), NationalID = org3Id, Id = 3};

            var org1Cont1 = new EntityContact {  Value = "405-111-99967",  Id = 1 , ContactTypeId = 1};
            var org1Cont2 = new EntityContact {  Value = "admin@kigeme.com", Id = 2, ContactTypeId = 2 };

            var org2Cont1 = new EntityContact {  Value = "405-222-99999", Id = 3, ContactTypeId = 1 };
            var org2Cont2 = new EntityContact {  Value = "admin@kibagabaga.com", Id = 4, ContactTypeId = 2};

            var org3Cont1 = new EntityContact {  Value = "405-333-99999", Id = 5, ContactTypeId = 1 };
            var org3Cont2 = new EntityContact {  Value = "admin@Munini.com", Id = 6 , ContactTypeId = 2 };

            var orgEnt1 = new Entity {Id = 1, ExternalId = org1Id, Type = "O", Category = "P", Firstname = "Kibagabaga", Lastname = "Kibagabaga", Addresses = new List<EntityAddress> { orgAddr1 }, Contacts = new List<EntityContact> { org1Cont1, org1Cont2 }, Demographic = orgDem1 };

            var orgEnt2 = new Entity {Id = 2, ExternalId = org2Id, Type = "O", Category = "P", Firstname = "Kigeme", Lastname = "Kigeme", Addresses = new List<EntityAddress> { orgAddr2 }, Contacts = new List<EntityContact> { org2Cont1, org2Cont2 }, Demographic = orgDem2 };

            var orgEnt3 = new Entity {Id = 3, ExternalId = org3Id, Type = "O", Category = "P", Firstname = "Munini", Lastname = "Munini", Addresses = new List<EntityAddress> { orgAddr3 }, Contacts = new List<EntityContact> { org3Cont1, org3Cont2 }, Demographic = orgDem3 };

            var org1 = new Organization {Id =1, Name = orgEnt1.Lastname, OrganizationType = 1, ShortName = orgEnt1.Firstname, Entity = orgEnt1, CountryId = orgCountry.Id, NationalId = org1Id };
            var org2 = new Organization {Id =2, Name = orgEnt2.Lastname, OrganizationType = 1, ShortName = orgEnt2.Firstname, Entity = orgEnt2, CountryId = orgCountry.Id, NationalId = org2Id };
            var org3 = new Organization {Id =3, Name = orgEnt3.Lastname, OrganizationType = 1, ShortName = orgEnt3.Firstname, Entity = orgEnt3, CountryId = orgCountry.Id, NationalId = org3Id };
 

            var organizations = new List<Organization> { org1, org2, org3 };


            for (var idx = 0; idx < 3; idx++)
            {
                yield return organizations[idx]; ;
            }
        }

        public api.Models.Organization getTestVisit()
        {
            if (this._itirator.MoveNext())
                return this._itirator.Current;

            return null;
        }

        [Fact]
        public void OnGet_ReturnsAListOfOrgVModels()
        {
            var orgContr = _orgController;

            var rst = orgContr.Get().Result;

            Assert.IsType<List<OrganizationViewModel>>(rst);
            Assert.Equal(true, rst.Count() > 0);

        }

        [Fact]
        public async Task OnGetId_ReturnsAOrgVModel()
        {
            var orgContr = _orgController;
            var org = await _organizationRepository.GetAllAsync();

            var result = orgContr.Detail(org.FirstOrDefault().Id).Result;

            var okResult = Assert.IsType<OkObjectResult>(result);
            var returnPat = Assert.IsType<OrganizationViewModel>(okResult.Value);
        }

        [Fact]
        public async Task OnGetOrgNationalId_ReturnsAOrgVModel()
        {
            var orgContr = _orgController;
            var org = await _organizationRepository.GetAllAsync();

            var result = orgContr.GetByNationalId(org.FirstOrDefault().NationalId).Result;

            var okResult = Assert.IsType<OkObjectResult>(result);
            var returnPat = Assert.IsType<OrganizationViewModel>(okResult.Value);
        }

        [Fact]
        public void OnGetIdIfOrgIdNE_ReturnsHttpNotFound()
        {
            var orgContr = _orgController;
            var testId = 911;

            var result = orgContr.Detail(testId).Result;

            var notFoundResult = Assert.IsType<NotFoundObjectResult>(result);
            var returnFeedback = Assert.IsType<FeedBack>(notFoundResult.Value);
            Assert.Equal(true, returnFeedback.err);
        }

        [Fact]
        public void OnGetIdIfOrgNationalIdNE_ReturnsHttpNotFound()
        {
            var orgContr = _orgController;
            var testId = 911;

            var result = orgContr.Detail(testId).Result;

            var notFoundResult = Assert.IsType<NotFoundObjectResult>(result);
            var returnFeedback = Assert.IsType<FeedBack>(notFoundResult.Value);
            Assert.Equal(true, returnFeedback.err);
        }

        [Fact]
        public void OnCreate_ReturnsOkResultNFeedbackWithCreatedOrgId()
        {
            var orgContr = _orgController;
            var org = getTestVisit();
            var model = new OrganizationRegistrationViewModel(org);

            var result = orgContr.Post(model).Result;

            var okResult = Assert.IsType<OkObjectResult>(result);
            var feedback = Assert.IsType<FeedBack>(okResult.Value);
            Assert.Contains("organizationid", feedback.data.ToLower());
        }
        [Fact]
        public void OnCreate_ReturnsOkResultNFeedbackWithCreatedOrgId2()
        {
            var orgContr = _orgController;
            var org = getTestVisit();
            var model = new OrganizationRegistrationViewModel
            {
                FirstName =  org.Entity.Firstname,
                SecondName =  org.Entity.Lastname,
                NationalId =  org.Entity.Demographic.NationalID,
                PhoneNumber = EntityHelper.GetEntityPhoneNumber(org.Entity),
                Email = EntityHelper.GetEntityEmail(org.Entity),
                CountryId = org.CountryId
                
            };

            var result = orgContr.Post(model).Result;

            var okResult = Assert.IsType<OkObjectResult>(result);
            var feedback = Assert.IsType<FeedBack>(okResult.Value);
            Assert.Contains("organizationid", feedback.data.ToLower());
        }

        [Fact]
        public async void OnCreate_OrgNationalIdShouldBeGenerated()
        {
            var orgContr = _orgController;
            var org = getTestVisit();
            var model = new OrganizationRegistrationViewModel(org);
            var result = orgContr.Post(model).Result;

            var okResult = Assert.IsType<OkObjectResult>(result);
            var feedback = Assert.IsType<FeedBack>(okResult.Value);
            var data = JsonConvert.DeserializeObject<Dictionary<string, int>>(feedback.data);
            var orgId = data["OrganizationId"];
            var created = await _organizationRepository.FindAsync(orgId);
            Assert.Equal(org.NationalId, created.NationalId);
            Assert.NotEqual(org.OrgSystemId, created.OrgSystemId);
            Assert.NotNull(created.OrgSystemId);
            Assert.True(created.NationalId.All(char.IsLetterOrDigit));
            Assert.Equal(10, created.OrgSystemId.Length);
        }

        [Fact]
        public async void OnCreate_ShouldCreateEntForOrg()
        {
            var orgContr = _orgController;
            var org = getTestVisit();
            var model = new OrganizationRegistrationViewModel(org);

            var result = orgContr.Post(model).Result;

            var okResult = Assert.IsType<OkObjectResult>(result);
            var feedback = Assert.IsType<FeedBack>(okResult.Value);
            var data = JsonConvert.DeserializeObject<Dictionary<string, int>>(feedback.data);
            var orgId = data["OrganizationId"];
            var created = await _organizationRepository.FindAsync(orgId);
            Assert.NotNull(created.Entity);
            Assert.NotSame(org.Entity, created.Entity);
        }

        [Fact]
        public async void OnCreate_shouldCreateEntDemForOrg()
        {
            var orgContr = _orgController;
            var org = getTestVisit();
            var model = new OrganizationRegistrationViewModel(org);

            var result = orgContr.Post(model).Result;

            var okResult = Assert.IsType<OkObjectResult>(result);
            var feedback = Assert.IsType<FeedBack>(okResult.Value);
            var data = JsonConvert.DeserializeObject<Dictionary<string, int>>(feedback.data);
            var orgId = data["OrganizationId"];
            var created = await _organizationRepository.FindAsync(orgId);
            Assert.NotNull(created.Entity);
            Assert.NotNull(created.Entity.Demographic);
            Assert.NotSame(org.Entity.Demographic, created.Entity.Demographic);
        }

        [Fact]
        public void OnCreate_ReturnsBadRequest_GivenInvalidModel()
        {
            var orgContr = _orgController;
            orgContr.ModelState.AddModelError("Test Error", "This is just a test");
            var org = getTestVisit();
            var model = new OrganizationRegistrationViewModel(org);

            var result = orgContr.Post(model).Result;

            var badRequest = Assert.IsType<BadRequestObjectResult>(result);
            var returnFeedback = Assert.IsType<FeedBack>(badRequest.Value);
            Assert.Equal(true, returnFeedback.err);
        }

        [Fact]
        public async void OnCreate_ShouldCheckIfOrganizationExists_AndReturnOrgId()
        {

            var orgContr = _orgController;
            var org = getTestVisit();
            var model = new OrganizationRegistrationViewModel(org);
            var result = orgContr.Post(model).Result;

            var okResult = Assert.IsType<OkObjectResult>(result);
            var feedback = Assert.IsType<FeedBack>(okResult.Value);
            var data = JsonConvert.DeserializeObject<Dictionary<string, int>>(feedback.data);
            var orgId = data["OrganizationId"];
            var created = await _organizationRepository.FindAsync(orgId);

            this._context.Entry(created).Reference(p => p.Entity).Load();
            this._context.Entry(created.Entity).Reference(e => e.Demographic).Load();
            this._context.Entry(created.Entity).Collection(e => e.Contacts).Load();
            this._context.Entry(created.Entity).Collection(e => e.Addresses).Load();

            var orgCountry = new Country { Name = "Rwanda", Abbreviation = "RW", Code = "250", Id = 2, };

            var contactType1 = new ContactType { Name = "Phone", Type = 1 };
            var contactType2 = new ContactType { Name = "Email", Type = 2 };
            var contactType3 = new ContactType { Name = "Twitter", Type = 3 };

            string org1Id = IDGenerator.GenerateAlphaNumericID(10); //"a123ddff"; //


            var orgAddr1 = new EntityAddress { Country = orgCountry.Abbreviation, Address1 = "Kamutwe Zone 1", City = "Kigali", Label = "Home", District = "Gasabo", Id = 1 };

            var orgDem1 = new EntityDemographic { Sex = "", DOB = new DateTime(1990, 1, 1), NationalID = org1Id, Id = 1 };

            var org1Cont1 = new EntityContact {  Value = "405-111-99999", Id = 1 , ContactTypeId = 1};
            var org1Cont2 = new EntityContact {  Value = "admin@chuk.com", Id = 2 , ContactTypeId = 2};

            var orgEnt1 = new Entity { Id = 100, ExternalId = org1Id, Type = "O", Category = "P", Firstname = "CHUK", Lastname = "CHUK", Addresses = new List<EntityAddress> { orgAddr1 }, Contacts = new List<EntityContact> { org1Cont1, org1Cont2 }, Demographic = orgDem1 };

            var org1 = new Organization { Id = 100, Name = orgEnt1.Lastname, OrganizationType = 1, ShortName = orgEnt1.Firstname, Entity = orgEnt1, CountryId = orgCountry.Id, NationalId = org1Id };

            org1.Name = created.Name;
            org1.ShortName = created.ShortName;
            var newModel = new OrganizationRegistrationViewModel(org1);


            var createdResult = orgContr.Post(newModel).Result;


            var badRequest = Assert.IsType<BadRequestObjectResult>(createdResult);
            var newFeedback = Assert.IsType<FeedBack>(badRequest.Value);
            Assert.Equal(true, newFeedback.err);
            Assert.Contains("exists", newFeedback.message.ToLower());
            var newData = JsonConvert.DeserializeObject(newFeedback.data);
            Assert.Contains("organizationid", newFeedback.data.ToLower());

        }

        //[Fact]
        public async Task OnCreate_ShouldCheckIfOrganizationExists_IfMarkedDelReturnOK()
        {

            var orgContr = _orgController;
            var org = getTestVisit();
            var model = new OrganizationRegistrationViewModel(org);
            var result = orgContr.Post(model).Result;

            var okResult = Assert.IsType<OkObjectResult>(result);
            var feedback = Assert.IsType<FeedBack>(okResult.Value);
            var data = JsonConvert.DeserializeObject<Dictionary<string, int>>(feedback.data);
            var orgId = data["OrganizationId"];
            var created = await _organizationRepository.FindAsync(orgId);

            this._context.Entry(created).Reference(p => p.Entity).Load();
            this._context.Entry(created.Entity).Reference(e => e.Demographic).Load();
            this._context.Entry(created.Entity).Collection(e => e.Contacts).Load();
            this._context.Entry(created.Entity).Collection(e => e.Addresses).Load();

            var orgCountry = new Country { Name = "Rwanda", Abbreviation = "RW", Code = "250", Id = 2, };

   
            string org1Id = IDGenerator.GenerateAlphaNumericID(10); //"a123ddff"; //


            var orgAddr1 = new EntityAddress { Country = orgCountry.Abbreviation, Address1 = "Kamutwe Zone 1", City = "Kigali", Label = "Home", District = "Gasabo", Id = 1 };

            var orgDem1 = new EntityDemographic { Sex = "", DOB = new DateTime(1990, 1, 1), NationalID = org1Id, Id = 1 };

            var org1Cont1 = new EntityContact {  Value = "405-111-99999", Id = 1 , ContactTypeId  = 1};
            var org1Cont2 = new EntityContact {  Value = "admin@chuk.com", Id = 2 , ContactTypeId = 2};

            var orgEnt1 = new Entity { Id = 100, ExternalId = org1Id, Type = "O", Category = "P", Firstname = "CHUK", Lastname = "CHUK", Addresses = new List<EntityAddress> { orgAddr1 }, Contacts = new List<EntityContact> { org1Cont1, org1Cont2 }, Demographic = orgDem1 };

            var org1 = new Organization { Id = 100, Name = orgEnt1.Lastname, OrganizationType = 1, ShortName = orgEnt1.Firstname, Entity = orgEnt1, CountryId = orgCountry.Id, NationalId = org1Id };

            //org1.Entity.ExternalId = created.Entity.ExternalId;
            created.IsDeleted = DateTime.Now;
            await _organizationRepository.UpdateAsync(created);
            org1.NationalId = created.NationalId;


            var newModel = new OrganizationRegistrationViewModel(org1);

            var createdResult = orgContr.Post(newModel).Result;

            var okRequest = Assert.IsType<OkObjectResult>(createdResult);
            var rst = Assert.IsType<FeedBack>(okRequest.Value);
            Assert.Equal(false, rst.err);
        }


        //TODO do contact
        [Fact]
        public async void OnCreate_CreateContactsForEntity()
        {
            var orgContr = _orgController;
            var org = getTestVisit();
            var model = new OrganizationRegistrationViewModel(org);

            var result = orgContr.Post(model).Result;

            var okResult = Assert.IsType<OkObjectResult>(result);
            var feedback = Assert.IsType<FeedBack>(okResult.Value);
            var data = JsonConvert.DeserializeObject<Dictionary<string, int>>(feedback.data);
            var orgId = data["OrganizationId"];
            var created =await  _organizationRepository.FindAsync(orgId);

            _context.Entry<Organization>(created).Reference<Entity>(p => p.Entity).Load();
            _context.Entry<Entity>(created.Entity).Collection<EntityContact>(e => e.Contacts).Load();

            Assert.NotNull(created.Entity);
            Assert.Equal(2, created.Entity.Contacts.Count);

            //check if the user was assigned an email
            var emailContact = created.Entity.Contacts.FirstOrDefault(c => c.ContactTypeId == (short)ContactTypes.Email).Value;
            Assert.Equal(emailContact, model.Email);

            //check if the user was assigned a phone number 
            var phoneContact = created.Entity.Contacts.FirstOrDefault(c => c.ContactTypeId == (short)ContactTypes.Phone).Value;
            Assert.Equal(phoneContact, model.PhoneNumber);
        }

        [Fact]
        public async void OnCreate_CreateAddressesForEntity()
        {
            var orgContr = _orgController;
            var org = getTestVisit();
            var model = new OrganizationRegistrationViewModel(org);


            var result = orgContr.Post(model).Result;

            var okResult = Assert.IsType<OkObjectResult>(result);
            var feedback = Assert.IsType<FeedBack>(okResult.Value);
            var data = JsonConvert.DeserializeObject<Dictionary<string, int>>(feedback.data);
            var orgId = data["OrganizationId"];
            var created = await _organizationRepository.FindAsync(orgId);
            _context.Entry<Organization>(created).Reference<Entity>(p => p.Entity).Load();
            _context.Entry<Entity>(created.Entity).Collection<EntityContact>(e => e.Contacts).Load();

            Assert.NotNull(created.Entity);
            Assert.NotNull(created.Entity.Addresses.FirstOrDefault());
        }
        //[Fact]
        public async Task OnUpdate_AddressGetsUpdated()
        {
            var orgContr = _orgController;
            var orgResult = await _organizationRepository.GetAllAsync();
            var org = orgResult.FirstOrDefault();
            this._context.Entry<Organization>(org).Reference(p => p.Entity).Load();
            this._context.Entry<Entity>(org.Entity).Reference(e => e.Demographic).Load();
            this._context.Entry<Entity>(org.Entity).Collection<EntityContact>(e => e.Contacts).Load();
            this._context.Entry<Entity>(org.Entity).Collection<EntityAddress>(e => e.Addresses).Load();

            var addr1 = "123 Wewe";
            var city = "Kandahal";
            var country = "ZM";
            var district = "Amagepho";
            var label = "Work";

            var updatedAddress = org.Entity.Addresses.FirstOrDefault();
            var addressVModel = new EntityAddressViewModel(updatedAddress);
            addressVModel.Address1 = addr1;
            addressVModel.City = city;
            addressVModel.Country = country;
            addressVModel.District = district;
            addressVModel.Label = label;
            addressVModel.Deactivate = true;
            updatedAddress.Update(addressVModel);

                    
            var result = orgContr.Put(org.Id, new OrganizationViewModel(org));

            var okResult = Assert.IsType<OkObjectResult>(result);
            var feedback = Assert.IsType<FeedBack>(okResult.Value);
            var data = JsonConvert.DeserializeObject<Dictionary<string, int>>(feedback.data);
            var orgId = data["OrganizationId"];
            var created = await  _organizationRepository.FindAsync(orgId);
            _context.Entry<Organization>(created).Reference<Entity>(p => p.Entity).Load();
            _context.Entry<Entity>(created.Entity).Collection<EntityAddress>(e => e.Addresses).Load();

            Assert.NotNull(created.Entity);
            var updatedAddr = created.Entity.Addresses.FirstOrDefault();
            Assert.NotNull(updatedAddress);

            Assert.Equal(addr1, updatedAddr.Address1);
            Assert.Equal(city, updatedAddr.City);
            Assert.Equal(country, updatedAddr.Country);
            Assert.Equal(district, updatedAddr.District);
            Assert.Equal(label, updatedAddr.Label);
            Assert.NotNull(updatedAddr.EndDate);
        }
        //[Fact]
        //public void OnUpdate_ContactGetsUpdated()
        //{
        //    var orgContr = new OrganizationController(_context, _organizationRepository);
        //    var org = _context.Organizations.FirstOrDefault();
        //    this._context.Entry<Organization>(org).Reference(p => p.Entity).Load();
        //    this._context.Entry<Entity>(org.Entity).Reference(e => e.Demographic).Load();
        //    this._context.Entry<Entity>(org.Entity).Collection<EntityContact>(e => e.Contacts).Load();
        //    this._context.Entry<Entity>(org.Entity).Collection<EntityAddress>(e => e.Addresses).Load();
        //    var model = new OrganizationRegistrationViewModel(org);

        //    var contactVModel = new EntityContactViewModel(org.Entity.Contacts.FirstOrDefault());

        //    var value = "email@gmail.com";
        //    var label = "email";
        //    var typeId = (int)ContactTypes.Email;
        //    contactVModel.Value = value;
        //    contactVModel.Label = label;
        //    contactVModel.TypeId = typeId;
        //    contactVModel.Deactivate = true;

        //    model.EntityModel.Contacts = new List<EntityContactViewModel> { contactVModel };

        //    var result = orgContr.Put(org.Id, model).Result;

        //    var okResult = Assert.IsType<OkObjectResult>(result);
        //    var feedback = Assert.IsType<FeedBack>(okResult.Value);
        //    var data = JsonConvert.DeserializeObject<Dictionary<string, int>>(feedback.data);
        //    var patId = data["OrganizationId"];
        //    var created = _context.Organizations.Find(patId);
        //    _context.Entry<Organization>(created).Reference<Entity>(p => p.Entity).Load();
        //    _context.Entry<Entity>(created.Entity).Collection<EntityContact>(e => e.Contacts).Load();

        //    Assert.NotNull(created.Entity);
        //    var updated = created.Entity.Contacts.FirstOrDefault();
        //    Assert.NotNull(updated);

        //    Assert.Equal(value, updated.Value);
        //    Assert.Equal(label, updated.Label);
        //    Assert.Equal(typeId, updated.ContactTypeId);
        //    Assert.NotNull(updated.EndDate);
        //}
        [Fact]
        public async Task OnDelete_OrgShouldBeMarkedDeleted()
        {
            var org = await _organizationRepository.GetAllAsync();
            var contr = _orgController;
            var result = contr.Delete(org.First().Id).Result;

            var updated =  await _organizationRepository.FindAsync(org.FirstOrDefault().Id);

            var rst = Assert.IsType<OkObjectResult>(result);
            var feedback = Assert.IsType<FeedBack>(rst.Value);
            Assert.NotEqual(updated.IsDeleted, DateTime.MaxValue);
        }

        [Fact]
        public void OnDelete_ReturnsBadRequestIfOrgNE()
        {
            var orgId = 911;
            var contr = _orgController;
            var result = contr.Delete(orgId).Result;

            var rst = Assert.IsType<BadRequestObjectResult>(result);
            var feedback = Assert.IsType<FeedBack>(rst.Value);
            Assert.Equal(true, feedback.err);
        }

        //[Fact]
        //public void OnUpdate_ReturnsBadRequestOnInValidateModel()
        //{
        //    var orgContr = new OrganizationController(_context, _organizationRepository);
        //    orgContr.ModelState.AddModelError("Test Error", "This is just a test");
        //    var org = _context.Organizations.FirstOrDefault();
        //    this._context.Entry<api.Models.Organization>(org).Reference(p => p.Entity).Load();
        //    this._context.Entry<Entity>(org.Entity).Reference(e => e.Demographic).Load();
        //    this._context.Entry<Entity>(org.Entity).Collection<EntityContact>(e => e.Contacts).Load();
        //    this._context.Entry<Entity>(org.Entity).Collection<EntityAddress>(e => e.Addresses).Load();

        //    var model = new OrganizationRegistrationViewModel(org);

        //    var result = orgContr.Put(org.Id, model).Result;

        //    var badRequest = Assert.IsType<BadRequestObjectResult>(result);
        //    var feedback = Assert.IsType<FeedBack>(badRequest.Value);
        //    Assert.Equal(true, feedback.err);
        //}

        [Fact]
        public async void OnUpdate_OrgGetsUpdated()
        {
            var controller = _orgController;
            var org = _context.Organizations.FirstOrDefault();
            this._context.Entry<api.Models.Organization>(org).Reference(p => p.Entity).Load();
            this._context.Entry<Entity>(org.Entity).Reference(e => e.Demographic).Load();
            this._context.Entry<Entity>(org.Entity).Collection<EntityContact>(e => e.Contacts).Load();
            this._context.Entry<Entity>(org.Entity).Collection<EntityAddress>(e => e.Addresses).Load();

            var model = new OrganizationViewModel(org);
            var newName = "Nyabugogo Clinic";
            var newShortName = "NC";
            var newNationalId = IDGenerator.GenerateAlphaNumericID(10);

            model.Name = newName;
            model.ShortName = newShortName;
            model.NationalId = newNationalId;


            var result = controller.Put(org.Id, model).Result;
            var updatedOrg =await  _organizationRepository.FindAsync(org.Id);

            Assert.Equal(newName, updatedOrg.Name);
            Assert.Equal(newShortName, updatedOrg.ShortName);
            // Assert.Equal(newNationalId,updatedOrg.NationalId);
        }

        //[Fact]
        //public void OnUpdate_PatDontGetUpdated()
        //{
        //    var controller = new OrganizationController(_context, _organizationRepository);
        //    var org = _context.Organizations.FirstOrDefault();
        //    this._context.Entry<api.Models.Organization>(org).Reference(p => p.Entity).Load();
        //    this._context.Entry<Entity>(org.Entity).Reference(e => e.Demographic).Load();
        //    this._context.Entry<Entity>(org.Entity).Collection<EntityContact>(e => e.Contacts).Load();
        //    this._context.Entry<Entity>(org.Entity).Collection<EntityAddress>(e => e.Addresses).Load();

        //    var model = new OrganizationRegistrationViewModel(org);
        //    var newNationalId = IDGenerator.GenerateAlphaNumericID(10);  //not editable
        //    model.OrgModel.NationalId = newNationalId;


        //    var result = controller.Put(org.Id, model).Result;
        //    var updatedOrg = _context.Organizations.Find(org.Id);

        //    Assert.NotEqual(newNationalId, org.NationalId);
        //    // Assert.Equal(newNationalId,updatedOrg.NationalId);

        //}

        //[Fact]
        //public void OnUpdate_EntGetsUpdated()
        //{
        //    var controller = new OrganizationController(_context, _organizationRepository);
        //    var org = _context.Organizations.FirstOrDefault();
        //    this._context.Entry<api.Models.Organization>(org).Reference(p => p.Entity).Load();
        //    this._context.Entry<Entity>(org.Entity).Reference(e => e.Demographic).Load();
        //    this._context.Entry<Entity>(org.Entity).Collection<EntityContact>(e => e.Contacts).Load();
        //    this._context.Entry<Entity>(org.Entity).Collection<EntityAddress>(e => e.Addresses).Load();

        //    var model = new OrganizationRegistrationViewModel(org);
        //    var firsname = "Kibazi Hospital";
        //    var lastname = "KH";
        //    var extId = IDGenerator.GenerateAlphaNumericID(10);
        //    var activ = false;
        //    var orgId = 9;

        //    model.EntityModel.Firstname = firsname;
        //    model.EntityModel.Lastname = lastname;
        //    model.EntityModel.IsActive = activ;
        //    model.EntityModel.ExternalId = extId;
        //    model.EntityModel.OrganizationId = orgId;

        //    var result = controller.Put(org.Id, model).Result;
        //    var updatedPat = _context.Organizations.Find(org.Id);

        //    Assert.Equal(firsname, updatedPat.Entity.Firstname);
        //    Assert.Equal(lastname, updatedPat.Entity.Lastname);
        //    Assert.Equal(activ, updatedPat.Entity.IsActive);
        //    Assert.Equal(extId, updatedPat.Entity.ExternalId);
        //}


    }
}
