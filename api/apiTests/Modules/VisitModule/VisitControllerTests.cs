﻿using System.Linq;
using System.Collections.Generic;
using Xunit;
using Microsoft.EntityFrameworkCore;
using api.Db;
using api.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using Newtonsoft.Json;
using api.Controllers;
using api.Utils;

namespace apiTests.Modules.VisitModule
{
    public class VisitControllerTests
    {
        private KiraContext _context;
        private IEnumerator<api.Models.Visit> _itirator;
        public VisitControllerTests()
        {
            var db = new DbContextOptionsBuilder<KiraContext>();
            db.UseInMemoryDatabase();
            _context = new KiraContext(db.Options);
            _context.Database.EnsureDeleted();

            DbInitializer.Init(_context, true);

            _itirator = getTestVisits().GetEnumerator();
        }

        public IEnumerable<Visit> getTestVisits()
        {
            var orgCountry = new Country { Name = "Rwandaa",  Abbreviation = "RW", Code = "250", Id = 1, };


            var contactType1 = new ContactType { Name = "Phone", Type = 1 };
            var contactType2 = new ContactType { Name = "Email", Type = 2 };
            var contactType3 = new ContactType { Name = "Twitter", Type = 3 };

            string org1Id = IDGenerator.GenerateAlphaNumericID(10);
            string org2Id = IDGenerator.GenerateAlphaNumericID(10);
            string org3Id = IDGenerator.GenerateAlphaNumericID(10);

            var orgAddr1 = new EntityAddress { Country = orgCountry.Abbreviation, Address1 = "Kamutwe Zone 1", City = "Kigali", Label = "Home", District = "Gasabo", Id = 1 };
            var orgAddr2 = new EntityAddress { Country = orgCountry.Abbreviation, Address1 = "Kamuduha Quartier 123", City = "Kigali", Label = "Home", District = "Gasabo", Id = 2 };
            var orgAddr3 = new EntityAddress { Country = orgCountry.Abbreviation, Address1 = "Mbalaba 432", City = "Kigali", Label = "Home", District = "Gasabo", Id = 3 };

            var orgDem1 = new EntityDemographic { Sex = "", DOB = new DateTime(1990, 1, 1), NationalID = org1Id, Id = 1 };
            var orgDem2 = new EntityDemographic { Sex = "", DOB = new DateTime(1990, 1, 1), NationalID = org2Id, Id = 2 };
            var orgDem3 = new EntityDemographic { Sex = "", DOB = new DateTime(1990, 1, 1), NationalID = org3Id, Id = 3 };

            var org1Cont1 = new EntityContact { Value = "405-111-99967", Id = 1 };
            var org1Cont2 = new EntityContact { Value = "admin@kigeme.com", Id = 2 };

            var org2Cont1 = new EntityContact { Value = "405-222-99999", Id = 3 };
            var org2Cont2 = new EntityContact { Value = "admin@kibagabaga.com", Id = 4 };

            var org3Cont1 = new EntityContact { Value = "405-333-99999", Id = 5 };
            var org3Cont2 = new EntityContact { Value = "admin@Munini.com", Id = 6 };

            var orgEnt1 = new Entity { Id = 1, ExternalId = org1Id, Type = "O", Category = "P", Firstname = "Kibagabaga", Lastname = "Kibagabaga", Addresses = new List<EntityAddress> { orgAddr1 }, Contacts = new List<EntityContact> { org1Cont1, org1Cont2 }, Demographic = orgDem1 };

            var orgEnt2 = new Entity { Id = 2, ExternalId = org2Id, Type = "O", Category = "P", Firstname = "Kigeme", Lastname = "Kigeme", Addresses = new List<EntityAddress> { orgAddr2 }, Contacts = new List<EntityContact> { org2Cont1, org2Cont2 }, Demographic = orgDem2 };

            var orgEnt3 = new Entity { Id = 3, ExternalId = org3Id, Type = "O", Category = "P", Firstname = "Munini", Lastname = "Munini", Addresses = new List<EntityAddress> { orgAddr3 }, Contacts = new List<EntityContact> { org3Cont1, org3Cont2 }, Demographic = orgDem3 };


            var org1 = new Organization { Id = 1, Name = orgEnt1.Lastname, OrganizationType = 1, ShortName = orgEnt1.Firstname, Entity = orgEnt1, CountryId = orgCountry.Id};
            var org2 = new Organization { Id = 2, Name = orgEnt2.Lastname, OrganizationType = 1, ShortName = orgEnt2.Firstname, Entity = orgEnt2, CountryId = orgCountry.Id};
            var org3 = new Organization { Id = 3, Name = orgEnt3.Lastname, OrganizationType = 1, ShortName = orgEnt3.Firstname, Entity = orgEnt3, CountryId = orgCountry.Id};

            var organizations = new List<Organization> { org1, org2, org3 };

            var dem = new EntityDemographic { Sex = "F", DOB = new DateTime(1945, 1, 1), NationalID = "5670485555" };
            var dem2 = new EntityDemographic { Sex = "M", DOB = new DateTime(1944, 12, 12), NationalID = "9978586998" };
            var dem3 = new EntityDemographic { Sex = "M", DOB = new DateTime(1933, 1, 1), NationalID = "564766672" };


            var addr = new EntityAddress { Country = "RW", Address1 = "Kamutwe Zone 1", City = "Byo", Label = "Home", District = "Muhoro" };

            var contact = new EntityContact
            {
                Value = "07830574",
                BeginDate = DateTime.Today,
                Label = "Phone",
               ContactTypeId = (int)ContactTypes.Phone
            };

            var ent1 = new Entity
            {
                ExternalId = "ID969",
                Type = "P",
                Category = "P",
                Firstname = "Poko",
                Lastname = "Atta",
                Demographic = dem,
                Addresses = new List<EntityAddress> { addr },
                Contacts = new List<EntityContact> { contact }
            };
            var ent2 = new Entity { ExternalId = "ID648", Type = "P", Category = "P", Firstname = "Timba", Lastname = "Kaka", Demographic = dem2 };
            var ent3 = new Entity { ExternalId = "ID922", Type = "P", Category = "P", Firstname = "Jean", Lastname = "Noma", Demographic = dem3 };

            var pat1 = new Patient { MedicalRecordNumber = "6738457646", AllergyNote = "Not too bad of alrg.", Entity = ent1, NationalPatientId = "BU685730456" };
            var pat2 = new Patient { MedicalRecordNumber = "674113322", AllergyNote = "No note.", Entity = ent2, NationalPatientId = "RW7757489222" };
            var pat3 = new Patient { MedicalRecordNumber = "6738457633", AllergyNote = "Not too bad of alrg.", Entity = ent3, NationalPatientId = "RW7757489333" };


            var visit1 = new Visit { IsPatientEnsured = false, IsFollowUp = false, IsReferral = false, OrganizationId = org1.Id, PatientId = pat1.Id, ReceptionistId = "", VisitDateTime = DateTime.Now };
            var visit2 = new Visit { IsPatientEnsured = false, IsFollowUp = false, IsReferral = false, OrganizationId = org2.Id, PatientId = pat2.Id, ReceptionistId = "", VisitDateTime = DateTime.Now };
            var visit3= new Visit { IsPatientEnsured = false, IsFollowUp = false, IsReferral = false, OrganizationId = org3.Id, PatientId = pat3.Id, ReceptionistId = "", VisitDateTime = DateTime.Now };

            var visits = new List<Visit> { visit1, visit2, visit3 };

            for (var idx = 0; idx < 3; idx++)
            {
                yield return visits[idx]; ;
            }
        }

        public api.Models.Visit getTestVisit()
        {
            if (this._itirator.MoveNext())
                return this._itirator.Current;

            return null;
        }

        [Fact]
        public void OnGetOrgVisits_ReturnsAListOfVisitsVModels()
        {
            var visitContr = new VisitController(_context);

            var rst = visitContr.GetOrgVisits(1).Result;

            var okResult = Assert.IsType<OkObjectResult>(rst);
            var returnPatients = Assert.IsType<List<AppointmentViewModel>>(okResult.Value);
            Assert.Equal(true, returnPatients.Any());

        }

        [Fact]
        public void OnGetId_ReturnsAVisitVModel()
        {
            var visitContr = new VisitController(_context);
            var visit = _context.Visits.FirstOrDefault();

            var result = visitContr.Detail(visit.Id).Result;

            var okResult = Assert.IsType<OkObjectResult>(result);
            var returnPat = Assert.IsType<VisitViewModel>(okResult.Value);
        }

        [Fact]
        public void OnGetPatientVisits_ReturnsAVisitsVModel()
        {
            var visitContr = new VisitController(_context);
            var visit = _context.Visits.FirstOrDefault();

            var result = visitContr.GetPatientVisits(visit.PatientId).Result;

            var okResult = Assert.IsType<OkObjectResult>(result);
            var returnPat = Assert.IsType<List<VisitViewModel>>(okResult.Value);
        }

        [Fact]
        public void OnGetIdIfVisitIdNE_ReturnsHttpNotFound()
        {
            var visitContr = new VisitController(_context);
            var testId = 911;

            var result = visitContr.Detail(testId).Result;

            var notFoundResult = Assert.IsType<NotFoundObjectResult>(result);
            var returnFeedback = Assert.IsType<FeedBack>(notFoundResult.Value);
            Assert.Equal(true, returnFeedback.err);
        }

        [Fact]
        public void OnGetPatientVisitIfNoVisits_ReturnsHttpNotFound()
        {
            var visitContr = new VisitController(_context);
            var patient = _context.Patients.FirstOrDefaultAsync(p => p.MedicalRecordNumber == "666666666");   
            var result = visitContr.GetPatientVisits(patient.Id).Result;
            var notFoundResult = Assert.IsType<NotFoundObjectResult>(result);
            var returnFeedback = Assert.IsType<FeedBack>(notFoundResult.Value);
            Assert.Equal(true, returnFeedback.err);
        }

        [Fact]
        public void OnGetPatientVisitIfPatientNE_ReturnsHttpNotFound()
        {
            var visitContr = new VisitController(_context);
            var testPatientId = -1;
            var result = visitContr.GetPatientVisits(testPatientId).Result;

            var notFoundResult = Assert.IsType<NotFoundObjectResult>(result);
            var returnFeedback = Assert.IsType<FeedBack>(notFoundResult.Value);
            Assert.Equal(true, returnFeedback.err);
        }

        [Fact]
        public void OnCreate_ReturnsOkResultNFeedbackWithCreatedVisitId()
        {
            var visitContr = new VisitController(_context);
            var visit = getTestVisit();
            var model = new VisitRegistrationViewModel(visit);
            var patient =  _context.Patients.FirstOrDefault();

            model.PatientId = patient.Id;
            var result = visitContr.Post(model).Result;

            var okResult = Assert.IsType<OkObjectResult>(result);
            var feedback = Assert.IsType<FeedBack>(okResult.Value);
            Assert.Contains("visitid", feedback.data.ToLower());

        }

        [Fact]
        public void OnCreate_PatientShouldExist()
        {

            var visitContr = new VisitController(_context);
            var visit = getTestVisit();
            var model = new VisitRegistrationViewModel(visit);
            var patient = _context.Patients.FirstOrDefault();

            model.PatientId = patient.Id;
            var result = visitContr.Post(model).Result;

            var okResult = Assert.IsType<OkObjectResult>(result);
            var feedback = Assert.IsType<FeedBack>(okResult.Value);
            var data = JsonConvert.DeserializeObject<Dictionary<string, int>>(feedback.data);

            var visitId = data["VisitId"];
            var createdVisit = _context.Visits.Find(visitId);

            Assert.Equal(patient.Id, createdVisit.PatientId);
        }

        [Fact]
        public void OnCreate_OrganizationShouldExist()
        {
            var visitContr = new VisitController(_context);
            var visit = getTestVisit();
            var model = new VisitRegistrationViewModel(visit);
            var patient = _context.Patients.FirstOrDefault();
            var organization = _context.Organizations.FirstOrDefault();

            model.PatientId = patient.Id;
            model.OrganizationId = organization.Id;
            var result = visitContr.Post(model).Result;

            var okResult = Assert.IsType<OkObjectResult>(result);
            var feedback = Assert.IsType<FeedBack>(okResult.Value);
            var data = JsonConvert.DeserializeObject<Dictionary<string, int>>(feedback.data);

            var visitId = data["VisitId"];
            var createdVisit = _context.Visits.Find(visitId);

            Assert.Equal(organization.Id, createdVisit.OrganizationId);
        }

        [Fact]
        public void OnCreate_ReturnsBadRequest_GivenInvalidModel()
        {
            var visitContr = new VisitController(_context);
            visitContr.ModelState.AddModelError("Test Error", "This is just a test");
            var visit = getTestVisit();
            var model = new VisitRegistrationViewModel(visit);

            var result = visitContr.Post(model).Result;

            var badRequest = Assert.IsType<BadRequestObjectResult>(result);
            var returnFeedback = Assert.IsType<FeedBack>(badRequest.Value);
            Assert.Equal(true, returnFeedback.err);
        }

        [Fact]
        public void OnCreate_ShouldCheckIfPatientExists_IfMarkedDelReturnBadRequest()
        {
            //var visitContr = new VisitController(_context);
            //var visit = getTestVisit();
            //var model = new VisitRegistrationViewModel(visit);
            //var patient = _context.Patients.FirstOrDefault();

            //model.PatientId = patient.Id;

            ////delete patient
            //var patientContr = new PatientController(_context);
            //var deletePatResult =  patientContr.Delete(patient.Id);
            ////
            //var result = visitContr.Post(model).Result;

            //var badRequest = Assert.IsType<BadRequestObjectResult>(result);
            //var returnFeedback = Assert.IsType<FeedBack>(badRequest.Value);
            //Assert.Equal(true, returnFeedback.err);
        }

        //[Fact]
        //public void OnUpdate_IsReferralGetsUpdated()
        //{
        //    var orgContr = new VisitController(_context);
        //    var org = _context.Visits.FirstOrDefault();
        //    this._context.Entry<Visit>(org).Reference(p => p.Entity).Load();
        //    this._context.Entry<Entity>(org.Entity).Reference(e => e.Demographic).Load();
        //    this._context.Entry<Entity>(org.Entity).Collection<EntityContact>(e => e.Contacts).Load();
        //    this._context.Entry<Entity>(org.Entity).Collection<EntityAddress>(e => e.Addresses).Load();
        //    var model = new VisitRegistrationViewModel(org);

        //    var contactVModel = new EntityContactViewModel(org.Entity.Contacts.FirstOrDefault());

        //    var value = "email@gmail.com";
        //    var label = "email";
        //    var typeId = (int)ContactTypes.Email;
        //    contactVModel.Value = value;
        //    contactVModel.Label = label;
        //    contactVModel.TypeId = typeId;
        //    contactVModel.Deactivate = true;

        //    model.EntityModel.Contacts = new List<EntityContactViewModel> { contactVModel };

        //    var result = orgContr.Put(org.Id, model).Result;

        //    var okResult = Assert.IsType<OkObjectResult>(result);
        //    var feedback = Assert.IsType<FeedBack>(okResult.Value);
        //    var data = JsonConvert.DeserializeObject<Dictionary<string, int>>(feedback.data);
        //    var patId = data["VisitId"];
        //    var created = _context.Visits.Find(patId);
        //    _context.Entry<Visit>(created).Reference<Entity>(p => p.Entity).Load();
        //    _context.Entry<Entity>(created.Entity).Collection<EntityContact>(e => e.Contacts).Load();

        //    Assert.NotNull(created.Entity);
        //    var updated = created.Entity.Contacts.FirstOrDefault();
        //    Assert.NotNull(updated);

        //    Assert.Equal(value, updated.Value);
        //    Assert.Equal(label, updated.Label);
        //    Assert.Equal(typeId, updated.ContactTypeId);
        //    Assert.NotNull(updated.EndDate);
        //}
        //[Fact]
        //public void OnDelete_OrgShouldBeMarkedDeleted()
        //{
        //    var org = _context.Visits.FirstOrDefault();
        //    var contr = new VisitController(_context);
        //    var result = contr.Delete(org.Id).Result;

        //    var updated = _context.Visits.Find(org.Id);

        //    var rst = Assert.IsType<OkObjectResult>(result);
        //    var feedback = Assert.IsType<FeedBack>(rst.Value);
        //    Assert.NotEqual(updated.IsDeleted, DateTime.MaxValue);
        //}

        //[Fact]
        //public void OnDelete_ReturnsBadRequestIfOrgNE()
        //{
        //    var orgId = 911;
        //    var contr = new VisitController(_context);
        //    var result = contr.Delete(orgId).Result;

        //    var rst = Assert.IsType<BadRequestObjectResult>(result);
        //    var feedback = Assert.IsType<FeedBack>(rst.Value);
        //    Assert.Equal(true, feedback.err);
        //}

        //[Fact]
        //public void OnUpdate_ReturnsBadRequestOnInValidateModel()
        //{
        //    var orgContr = new VisitController(_context);
        //    orgContr.ModelState.AddModelError("Test Error", "This is just a test");
        //    var org = _context.Visits.FirstOrDefault();
        //    this._context.Entry<api.Models.Visit>(org).Reference(p => p.Entity).Load();
        //    this._context.Entry<Entity>(org.Entity).Reference(e => e.Demographic).Load();
        //    this._context.Entry<Entity>(org.Entity).Collection<EntityContact>(e => e.Contacts).Load();
        //    this._context.Entry<Entity>(org.Entity).Collection<EntityAddress>(e => e.Addresses).Load();

        //    var model = new VisitRegistrationViewModel(org);

        //    var result = orgContr.Put(org.Id, model).Result;

        //    var badRequest = Assert.IsType<BadRequestObjectResult>(result);
        //    var feedback = Assert.IsType<FeedBack>(badRequest.Value);
        //    Assert.Equal(true, feedback.err);
        //}

        //[Fact]
        //public void OnUpdate_OrgGetsUpdated()
        //{
        //    var controller = new VisitController(_context);
        //    var org = _context.Visits.FirstOrDefault();
        //    this._context.Entry<api.Models.Visit>(org).Reference(p => p.Entity).Load();
        //    this._context.Entry<Entity>(org.Entity).Reference(e => e.Demographic).Load();
        //    this._context.Entry<Entity>(org.Entity).Collection<EntityContact>(e => e.Contacts).Load();
        //    this._context.Entry<Entity>(org.Entity).Collection<EntityAddress>(e => e.Addresses).Load();

        //    var model = new VisitRegistrationViewModel(org);
        //    var newName = "Nyabugogo Clinic";
        //    var newShortName = "NC";
        //    var newNationalId = IDGenerator.GenerateAlphaNumericID(10);

        //    model.OrgModel.Name = newName;
        //    model.OrgModel.ShortName = newShortName;
        //    model.OrgModel.VisitNationalId = newNationalId;


        //    var result = controller.Put(org.Id, model).Result;
        //    var updatedOrg = _context.Visits.Find(org.Id);

        //    Assert.Equal(newName, updatedOrg.Name);
        //    Assert.Equal(newShortName, updatedOrg.ShortName);
        //    // Assert.Equal(newNationalId,updatedOrg.VisitNationalId);
        //}

        //[Fact]
        //public void OnUpdate_PatDontGetUpdated()
        //{
        //    var controller = new VisitController(_context);
        //    var org = _context.Visits.FirstOrDefault();
        //    this._context.Entry<api.Models.Visit>(org).Reference(p => p.Entity).Load();
        //    this._context.Entry<Entity>(org.Entity).Reference(e => e.Demographic).Load();
        //    this._context.Entry<Entity>(org.Entity).Collection<EntityContact>(e => e.Contacts).Load();
        //    this._context.Entry<Entity>(org.Entity).Collection<EntityAddress>(e => e.Addresses).Load();

        //    var model = new VisitRegistrationViewModel(org);
        //    var newNationalId = IDGenerator.GenerateAlphaNumericID(10);  //not editable
        //    model.OrgModel.VisitNationalId = newNationalId;


        //    var result = controller.Put(org.Id, model).Result;
        //    var updatedOrg = _context.Visits.Find(org.Id);

        //    Assert.NotEqual(newNationalId, org.VisitNationalId);
        //    // Assert.Equal(newNationalId,updatedOrg.VisitNationalId);

        //}

        //[Fact]
        //public void OnUpdate_EntGetsUpdated()
        //{
        //    var controller = new VisitController(_context);
        //    var org = _context.Visits.FirstOrDefault();
        //    this._context.Entry<api.Models.Visit>(org).Reference(p => p.Entity).Load();
        //    this._context.Entry<Entity>(org.Entity).Reference(e => e.Demographic).Load();
        //    this._context.Entry<Entity>(org.Entity).Collection<EntityContact>(e => e.Contacts).Load();
        //    this._context.Entry<Entity>(org.Entity).Collection<EntityAddress>(e => e.Addresses).Load();

        //    var model = new VisitRegistrationViewModel(org);
        //    var firsname = "Kibazi Hospital";
        //    var lastname = "KH";
        //    var extId = IDGenerator.GenerateAlphaNumericID(10);
        //    var activ = false;
        //    var orgId = 9;

        //    model.EntityModel.Firstname = firsname;
        //    model.EntityModel.Lastname = lastname;
        //    model.EntityModel.IsActive = activ;
        //    model.EntityModel.ExternalId = extId;
        //    model.EntityModel.VisitId = orgId;

        //    var result = controller.Put(org.Id, model).Result;
        //    var updatedPat = _context.Visits.Find(org.Id);

        //    Assert.Equal(firsname, updatedPat.Entity.Firstname);
        //    Assert.Equal(lastname, updatedPat.Entity.Lastname);
        //    Assert.Equal(activ, updatedPat.Entity.IsActive);
        //    Assert.Equal(extId, updatedPat.Entity.ExternalId);
        //}

        //[Fact]
        //public void OnUpdate_EntDontGetUpdated()
        //{
        //    var controller = new VisitController(_context);
        //    var pat = _context.Visits.FirstOrDefault();
        //    this._context.Entry<api.Models.Visit>(pat).Reference(p => p.Entity).Load();
        //    this._context.Entry<Entity>(pat.Entity).Reference(e => e.Demographic).Load();
        //    this._context.Entry<Entity>(pat.Entity).Collection<EntityContact>(e => e.Contacts).Load();
        //    this._context.Entry<Entity>(pat.Entity).Collection<EntityAddress>(e => e.Addresses).Load();

        //    var model = new RegistrationViewModel(pat);
        //    var spc = "tst";
        //    var typ = "C";
        //    var cat = "X";
        //    var etrDat = new DateTime(1944, 5, 13);

        //    model.EntityModel.Specialty = spc; 
        //    model.EntityModel.Type = typ;
        //    model.EntityModel.Category = cat;
        //    model.EntityModel.EntryDate = etrDat;

        //    var result = controller.Put(pat.Id, model).Result;
        //    var updatedPat = _context.Visits.Find(pat.Id);

        //    Assert.NotEqual(spc, updatedPat.Entity.Specialty);
        //    Assert.NotEqual(typ, updatedPat.Entity.Type);
        //    Assert.NotEqual(cat, updatedPat.Entity.Category);
        //    Assert.NotEqual(etrDat, updatedPat.Entity.EntryDate);
        //}

        //[Fact]
        //public void OnUpdate_EntDemGetUpdated()
        //{
        //    var controller = new VisitController(_context);
        //    var pat = _context.Visits.FirstOrDefault();
        //    this._context.Entry<api.Models.Visit>(pat).Reference(p => p.Entity).Load();
        //    this._context.Entry<Entity>(pat.Entity).Reference(e => e.Demographic).Load();
        //    this._context.Entry<Entity>(pat.Entity).Collection<EntityContact>(e => e.Contacts).Load();
        //    this._context.Entry<Entity>(pat.Entity).Collection<EntityAddress>(e => e.Addresses).Load();

        //    var model = new RegistrationViewModel(pat);

        //    var nid = "123456789";
        //    var prmLan = "RU";
        //    var ndLan = "FR";
        //    var sex = "M";
        //    var mrtStatus = "S";
        //    var rlg = "C";
        //    var inc = 100000;
        //    var bday = new DateTime(1969, 6, 9);

        //    model.EntityModel.Demographic.NationalID = nid;
        //    model.EntityModel.Demographic.PrimaryLanguage = prmLan;
        //    model.EntityModel.Demographic.SecondaryLanguage = ndLan;
        //    model.EntityModel.Demographic.Sex = sex;
        //    model.EntityModel.Demographic.MartalStatus = mrtStatus;
        //    model.EntityModel.Demographic.Religion = rlg;
        //    model.EntityModel.Demographic.Income = inc;
        //    model.EntityModel.Demographic.DOB = bday;

        //    var result = controller.Put(pat.Id, model).Result;
        //    var updatedPat = _context.Visits.Find(pat.Id);

        //    Assert.Equal(nid, updatedPat.Entity.Demographic.NationalID);
        //    Assert.Equal(prmLan, updatedPat.Entity.Demographic.PrimaryLanguage);
        //    Assert.Equal(ndLan, updatedPat.Entity.Demographic.SecondaryLanguage);
        //    Assert.Equal(sex, updatedPat.Entity.Demographic.Sex);
        //    Assert.Equal(mrtStatus, updatedPat.Entity.Demographic.MartalStatus);
        //    Assert.Equal(rlg, updatedPat.Entity.Demographic.Religion);
        //    Assert.Equal(inc, updatedPat.Entity.Demographic.Income);
        //    Assert.Equal(bday, updatedPat.Entity.Demographic.DOB);
        //}

        //[Fact]
        //public void OnUpdate_EntDemDontGetUpdated()
        //{
        //    var controller = new VisitController(_context);
        //    var pat = _context.Visits.FirstOrDefault();
        //    this._context.Entry<api.Models.Visit>(pat).Reference(p => p.Entity).Load();
        //    this._context.Entry<Entity>(pat.Entity).Reference(e => e.Demographic).Load();
        //    this._context.Entry<Entity>(pat.Entity).Collection<EntityContact>(e => e.Contacts).Load();
        //    this._context.Entry<Entity>(pat.Entity).Collection<EntityAddress>(e => e.Addresses).Load();

        //    var model = new RegistrationViewModel(pat);

        //    var demId = 777;
        //    model.EntityModel.Demographic.Id = demId;

        //    var result = controller.Put(pat.Id, model).Result;
        //    var updatedPat = _context.Visits.Find(pat.Id);

        //    Assert.NotEqual(demId, updatedPat.Entity.DemographicId);
        //}
    }
}
