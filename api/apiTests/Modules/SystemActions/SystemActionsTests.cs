﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using api.Auth;
using api.Auth.Roles;
using api.Controllers;
using api.Db;
using api.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Xunit;
using Xunit.Sdk;

namespace apiTests.Modules.SystemActions
{
    public class SystemActionsTests
    {
        public readonly SystemActionRepository _actionRepository;
        public readonly KiraContext _context;


        public SystemActionsTests()
        {
            var db = new DbContextOptionsBuilder<KiraContext>();
            db.UseInMemoryDatabase();
            _context = new KiraContext(db.Options);
            _context.Database.EnsureDeleted();

            DbInitializer.Init(_context, true);
            _actionRepository = new SystemActionRepository(_context);
        }

        [Fact]
        public void OnCreateNewAction_ReturnsOkFeedBack()
        {
            var newActionModel = new ActionViewModel
            {
                ActionName = "View",
                ControllerName = "Patients",
                ClaimNumber = 0,
            };
            var newActionName = new SystemAction(newActionModel).ActionName;

            var actionCtr = new SystemActionController(_actionRepository);
            var result = actionCtr.Post(newActionModel);

            var okResult = Assert.IsType<OkObjectResult>(result.Result);
            var feedback = Assert.IsType<FeedBack>(okResult.Value);
            Assert.Contains("actionname", feedback.data.ToLower());

            var action = _actionRepository.FindByName(newActionName);
            Assert.Equal(action.ActionName, newActionName);
            Assert.Equal(action.ControllerName, newActionModel.ControllerName);

            //delete the role
            _actionRepository.Delete(action);
        }

        [Fact]
        public void OnCreateNewAction_ReturnsBadFeedBackIfExist()
        {
            var newActionModel = new ActionViewModel
            {
                ActionName = "View",
                ControllerName = "Patients",
                ClaimNumber = 0,
            };
            var newActionName = new SystemAction(newActionModel).ActionName;

            var actionCtr = new SystemActionController(_actionRepository);
            var result = actionCtr.Post(newActionModel);

            var okResult = Assert.IsType<OkObjectResult>(result.Result);
            var feedback = Assert.IsType<FeedBack>(okResult.Value);
            Assert.Contains("actionname", feedback.data.ToLower());

            var action = _actionRepository.FindByName(newActionName);

            var reCreateResult = actionCtr.Post(newActionModel);

            var badRequest = Assert.IsType<BadRequestObjectResult>(reCreateResult.Result);
            var returnFeedback = Assert.IsType<FeedBack>(badRequest.Value);
            Assert.Equal(true, returnFeedback.err);

            //delete the role
            _actionRepository.Delete(action);
        }

        [Fact]
        public void OnDelete_ReturnsOkFeedBackIfExistis()
        {
            var newActionModel = new ActionViewModel
            {
                ActionName = "View",
                ControllerName = "Patients",
                ClaimNumber = 0,
            };
            var newActionName = new SystemAction(newActionModel).ActionName;

            var actionCtr = new SystemActionController(_actionRepository);
            var result = actionCtr.Post(newActionModel);

            var action = _actionRepository.FindByName(newActionName);
            //delete the role
            var deleteResult = actionCtr.Delete(action.ActionName);
            var isDeleted = _actionRepository.FindByName(action.ActionName);
            Assert.Null(isDeleted);
        }

        [Fact]
        public void OnGetSystemActions_ReturnsActionsVModel()
        {
            var actionContr = new SystemActionController(_actionRepository);
            var result = actionContr.Get();
            var okResult = Assert.IsType<List<ActionViewModel>>(result.Result);
        }

    }
}
