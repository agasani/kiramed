﻿using System.Collections.Generic;
using api.Auth;
using api.Models;
using System;
using Xunit;
using System.Linq;
using System.Threading.Tasks;
using api;
using Microsoft.AspNetCore.Mvc;
using api.Controllers;
using api.ViewModels;
using apiTests.TestSetUp;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;

namespace apiTests.Modules.UserModule
{
    public class AccountControllerTests : UserServiceTestSeUp
    { 
        internal IEnumerator<ApplicationUser> _itirator;
        private UserController _controller { get; }

      
        public AccountControllerTests(TestSetUp.TestFixture<Startup> fixture) : base(fixture)
        {          
            _controller = new UserController(_userService,_roleRepository);       
            _itirator = getTesApplicationUsers().GetEnumerator();
            
        }

        public IEnumerable<ApplicationUser> getTesApplicationUsers()
        {
            var dem = new EntityDemographic { Sex = "F", DOB = new DateTime(1945, 1, 1), NationalID = "5670485555" };
            var dem2 = new EntityDemographic { Sex = "M", DOB = new DateTime(1944, 12, 12), NationalID = "9978586998" };
            var dem3 = new EntityDemographic { Sex = "M", DOB = new DateTime(1933, 1, 1), NationalID = "564766672" };
            var dem4 = new EntityDemographic { Sex = "F", DOB = new DateTime(1922, 12, 12), NationalID = "9008586998" };
            var dem5 = new EntityDemographic { Sex = "M", DOB = new DateTime(1991, 1, 1), NationalID = "56477772" };
            var dem6 = new EntityDemographic { Sex = "F", DOB = new DateTime(1919, 12, 12), NationalID = "9971026998" };
            var dem7 = new EntityDemographic { Sex = "F", DOB = new DateTime(1916, 1, 1), NationalID = "560029572" };
            var dem8 = new EntityDemographic { Sex = "F", DOB = new DateTime(1923, 12, 12), NationalID = "3228586998" };
            var dem9 = new EntityDemographic { Sex = "M", DOB = new DateTime(1996, 1, 1), NationalID = "564739511" };
            var dem10 = new EntityDemographic { Sex = "M", DOB = new DateTime(1991, 12, 12), NationalID = "9978509438" };

            var addr = new EntityAddress { Country = "RW", Address1 = "Kamutwe Zone 1", City = "Byo", Label = "Home", District = "Muhoro" };

            var contact = new EntityContact
            {
                Value = "07830574",
                BeginDate = DateTime.Today,
                Label = "Phone",
                ContactTypeId = (int)ContactTypes.Phone 
            };

            var country = new Country { Name = "Rwanda",  Abbreviation ="RW", Code = "250" };

            var ent1 = new Entity
            {
                ExternalId = "ID969",
                Type = "P",
                Category = "P",
                Firstname = "Poko",
                Lastname = "Atta",
                Demographic = dem,
                Addresses = new List<EntityAddress> { addr },
                Contacts = new List<EntityContact> { contact }
            };
            var ent2 = new Entity { ExternalId = "ID648", Type = "P", Category = "P", Firstname = "Timba", Lastname = "Kaka", Demographic = dem2 };
            var ent3 = new Entity { ExternalId = "ID922", Type = "P", Category = "P", Firstname = "Jean", Lastname = "Noma", Demographic = dem3 };
            var ent4 = new Entity { ExternalId = "ID633", Type = "P", Category = "P", Firstname = "Papi", Lastname = "Kaka", Demographic = dem4 };
            var ent5 = new Entity { ExternalId = "ID944", Type = "P", Category = "P", Firstname = "Polete", Lastname = "Noma", Demographic = dem5 };
            var ent6 = new Entity { ExternalId = "ID655", Type = "P", Category = "P", Firstname = "Keba", Lastname = "Kaka", Demographic = dem6 };

            var ent7 = new Entity { ExternalId = "ID655", Type = "P", Category = "P", Firstname = "Keba", Lastname = "Kaka", Demographic = dem6 };


           // var org1 = new ApplicationU { Name = "CHUK", VisitId = "CHUK", EntityId = ent7.Id, CountryId = country.Id };
            //var role1 = new ApplicationRole { Name = "Nurse", VisitId = org1.Id, };
            var user1 = new ApplicationUser { UserName = "joemasengesho1", Email = "joemasengesho@inyarwanda.com", CreatedDate = DateTime.Now,  ModifiedDate = DateTime.Now, EntityId = ent1.Id };
            var user2 = new ApplicationUser { UserName = "joemasengesho2", Email = "joemasengesho2@kiramd.com", CreatedDate = DateTime.Now, ModifiedDate = DateTime.Now, EntityId = ent2.Id };
            var user3 = new ApplicationUser { UserName = "joemasengesho3", Email = "joemasengesho3@kiramd.com", CreatedDate = DateTime.Now, ModifiedDate = DateTime.Now, EntityId = ent3.Id };
            var user4 = new ApplicationUser { UserName = "joemasengesho4", Email = "joemasengesho4@inyarwanda.com", CreatedDate = DateTime.Now, ModifiedDate = DateTime.Now, EntityId = ent4.Id };
            var user5 = new ApplicationUser { UserName = "joemasengesho5", Email = "joemasengesho5@kiramd.com", CreatedDate = DateTime.Now, ModifiedDate = DateTime.Now, EntityId = ent5.Id };
            var user6 = new ApplicationUser { UserName = "joemasengesho6", Email = "joemasengesho6@kiramd.com", CreatedDate = DateTime.Now, ModifiedDate = DateTime.Now, EntityId = ent6.Id };
                     
            var users = new List<ApplicationUser>() { user1, user2, user3, user4, user5, user6};

            for (var idx = 0; idx < 6; idx++)
            {
                yield return users[idx];
            }
        }

        public ApplicationUser getTesApplicationUser()
        {
            if (this._itirator.MoveNext())
                return this._itirator.Current;

            return null;
        }

        [Fact]
        public async Task OnRegisterNewUser_ReturnsOkFeedBack()
        {
            var role =  _roleRepository.Roles(1).Result.FirstOrDefault();
            var newUser = new UserRegisterViewModel
            {
                UserName = "JMaombi",
                FirstName = "Joseph",
                SecondName = "Masengesh0",
                Email = "Jmasengesho@inyarwanda.com",
                NationalId = "1122334422679",
                PhoneNumber = "405-602-9804",
                Roles = new List<string> { role.Name },
                Password = "#Password111E"
            };

            //delete this user if exists
            var deleteIfExisits = await _userService.GetByEmailAsync(newUser.Email);
            if (deleteIfExisits != null)
            {
                var result1 = _userService.Delete(deleteIfExisits).Result;
            }

            var result = _controller.Post(newUser).Result;

            var okResult = Assert.IsType<OkObjectResult>(result);
            var feedback = Assert.IsType<FeedBack>(okResult.Value);
            Assert.Contains("username", feedback.data.ToLower());
            var createdUser = await _userService.GetByEmailAsync(newUser.Email);
            Assert.Equal(newUser.Email, createdUser.Email);
            //delete the user
            var resultCleanUp = _userService.Delete(createdUser).Result;
        }

        [Fact]
        public async Task OnRegisterNewUser_UserShouldBeInTheOrganization()
        {
            var role = _roleRepository.Roles(1).Result.FirstOrDefault();
            var newUser = new UserRegisterViewModel
            {
                UserName = "JMaombi",
                FirstName = "Joseph",
                SecondName = "Masengesh0",
                Email = "Jmasengesho@inyarwanda.com",
                NationalId = "1122334422679",
                PhoneNumber = "405-602-9804",
                Roles = new List<string> { role.Name },
                Password = "#Password111E",
                OrganizationId = 1
            };
            
            var deleteIfExisits = await _userService.GetByEmailAsync(newUser.Email);
            if (deleteIfExisits != null)
            {
                var result1 = _userService.Delete(deleteIfExisits).Result;
            }

            var result = _controller.Post(newUser).Result;
            var okResult = Assert.IsType<OkObjectResult>(result);
            var feedback = Assert.IsType<FeedBack>(okResult.Value);
            var createdUser = await _userService.GetByEmailAsync(newUser.Email);

            var userOrganization = _context.UserOrganizations.FirstOrDefault(u => u.UserId == createdUser.UserId && u.OrganizationId == 1);
            Assert.NotNull(userOrganization);
            //delete the user
            var resultCleanUp = _userService.Delete(createdUser).Result;
        }

        [Fact]
        public async Task OnRegisterNewUserWithTwoRoles_UserShouldBeAssignedBothRoles()
        {
            //var role = _roleRepository.Roles(1).Result.FirstOrDefault();
            var role1 = new ApplicationRole { Name = "Nurse One", OrganizationId = 1, Description = " Nurse Role " };
            var role1Result = _roleRepository.Create(role1);

            var role2 = new ApplicationRole { Name = "Nurse Two", OrganizationId = 1, Description = " Nurse Role " };
            var role2Result = _roleRepository.Create(role2);

            var newUser = new UserRegisterViewModel
            {
                UserName = "JMaombi",
                FirstName = "Joseph",
                SecondName = "Masengesh0",
                Email = "Jmasengesho@inyarwanda.com",
                NationalId = "1122334422679",
                PhoneNumber = "405-602-9804",
                Roles = new List<string> { role1.Name , role2.Name},
                Password = "#Password111E",
                OrganizationId = 1
            };

            var deleteIfExisits = await _userService.GetByEmailAsync(newUser.Email);
            if (deleteIfExisits != null)
            {
                var result1 = _userService.Delete(deleteIfExisits).Result;
            }

            var result = _controller.Post(newUser).Result;
            var okResult = Assert.IsType<OkObjectResult>(result);
            var feedback = Assert.IsType<FeedBack>(okResult.Value);
            var createdUser = await _userService.GetByEmailAsync(newUser.Email);

            var userOrganizations = _context.UserOrganizations.Where(u => u.UserId == createdUser.UserId && u.OrganizationId == 1).ToList();
            Assert.NotNull(userOrganizations);
            Assert.Equal(userOrganizations.Count, 2);

            //delete the user
            var resultCleanUp = _userService.Delete(createdUser).Result;
        }


        [Fact]
        public async Task OnAddUserToOrg_UserShouldBeAssigneRolesAndBeInOrg()
        {
            //var role = _roleRepository.Roles(1).Result.FirstOrDefault();
            var role1 = new ApplicationRole { Name = "Nurse One", OrganizationId = 1, Description = " Nurse Role " };
            var role1Result = _roleRepository.Create(role1);

            var role2 = new ApplicationRole { Name = "Nurse Two", OrganizationId = 1, Description = " Nurse Role " };
            var role2Result = _roleRepository.Create(role2);

            var newUser = new UserRegisterViewModel
            {
                UserName = "JMaombi",
                FirstName = "Joseph",
                SecondName = "Masengesh0",
                Email = "Jmasengesho@inyarwanda.com",
                NationalId = "1122334422679",
                PhoneNumber = "405-602-9804",
                Roles = new List<string> { role1.Name, role2.Name },
                Password = "#Password111E",
                OrganizationId = 1
            };

            var deleteIfExisits = await _userService.GetByEmailAsync(newUser.Email);
            if (deleteIfExisits != null)
            {
                var result1 = _userService.Delete(deleteIfExisits).Result;
            }

            var result = _controller.Post(newUser).Result;
            var okResult = Assert.IsType<OkObjectResult>(result);
            var feedback = Assert.IsType<FeedBack>(okResult.Value);
            var createdUser =  await _userService.GetByEmailAsync(newUser.Email);


            var userOrganizations = _context.UserOrganizations.Where(u => u.UserId == createdUser.UserId && u.OrganizationId == 1).ToList();
            Assert.NotNull(userOrganizations);
            Assert.Equal(userOrganizations.Count, 2);

            //check if this user is in thir org

            var orgUsers =  await _userService.GetOrganizationUsersAsync(1);
            Assert.NotEqual(0, orgUsers.Count());
            Assert.Equal(createdUser.Email, orgUsers.FirstOrDefault(u => u.Email == createdUser.Email).Email);
            //delete the user
            var resultCleanUp = _userService.Delete(createdUser).Result;
            _roleRepository.Delete(role1);
            _roleRepository.Delete(role2);
        }

        [Fact]
        public async Task OnRegister_ReturnBadREquestIfUserExists()
        {
            var role = await _roleRepository.FindByNameAsync("Rw Nurse");
            var newUser = new UserRegisterViewModel
            {
                UserName = "JMaombi",
                FirstName = "Joseph",
                SecondName = "Masengesh0",
                Email = "Jmasengesho@inyarwanda.com",
                NationalId = "1122334422679",
                PhoneNumber = "405-602-9804",
                Roles = new List<string> { role.Name },
                Password = "#Password111E"
            };

            //delete this user if exists
            var deleteIfExisits = await _userService.GetByEmailAsync(newUser.Email);
            if (deleteIfExisits != null)
            {
                var result1 = _userService.Delete(deleteIfExisits).Result;
            }

            var result = _controller.Post(newUser).Result;
       
            var resultRecreate = _controller.Post(newUser).Result;

            var badRequestResult = Assert.IsType<BadRequestObjectResult>(resultRecreate);
            var feedback = Assert.IsType<FeedBack>(badRequestResult.Value);


            //delete the user
            var createdUser = await _userService.GetByEmailAsync(newUser.Email);
            var resultCleanUp = _userService.Delete(createdUser).Result;
        }

        [Fact]
        public async Task OnRegister_ReturnOkResultWithAmessageIfAnyRoleDoesNotExists()
        {
          //var role = _roleRepository.FindByNameAsync("Nurse");
            var newUser = new UserRegisterViewModel
            {
                UserName = "JMaombi",
                FirstName = "Joseph",
                SecondName = "Masengesh0",
                Email = "Jmasengesho@inyarwanda.com",
                NationalId = "1122334422679",
                PhoneNumber = "405-602-9804",
                Roles = new List<string> { "FakeRole" },
                Password = "#Password111E"
            };

            var deleteIfExisits = await _userService.GetByEmailAsync(newUser.Email);
            if (deleteIfExisits != null)
            {
                var result1 = _userService.Delete(deleteIfExisits).Result;
            }

            var result = _controller.Post(newUser).Result;

            var okResult = Assert.IsType<OkObjectResult>(result);
            var feedback = Assert.IsType<FeedBack>(okResult.Value);
            Assert.Contains("username", feedback.data.ToLower());
            Assert.Contains("the role you are trying to assign does not exist", feedback.message.ToLower());


            //delete the user
            var createdUser = await _userService.GetByEmailAsync(newUser.Email);
            var resultCleanUp = _userService.Delete(createdUser).Result;
        }

        [Fact]
        public async void OnRegisterNewUser_UserShouldHaveAnEntity()
        {
            var role = await _roleRepository.FindByNameAsync("Rw Nurse");

            var newUser = new UserRegisterViewModel
            {
                UserName = "NGildas",
                FirstName = "Gildas",
                SecondName = "Niyigena",
                Email = "gildniy05@gmail.com",
                NationalId = "11111111111",
                PhoneNumber = "405-602-1111",
                Roles = new List<string> { role.Name },
                Password = "#Password111E",
                OrganizationId = 1
            };

            var deleteIfExisits = await _userService.GetByEmailAsync(newUser.Email);
            if (deleteIfExisits != null)
            {
                var result1 = _userService.Delete(deleteIfExisits).Result;
            }

            var result = _controller.Post(newUser).Result;
            var okResult = Assert.IsType<OkObjectResult>(result);
            var feedback = Assert.IsType<FeedBack>(okResult.Value);
            var createdUser = await _userService.GetByEmailAsync(newUser.Email);

            var userEntity = createdUser.Entity;
            Assert.NotNull(userEntity);
            Assert.Equal(userEntity.ExternalId, newUser.NationalId);
            Assert.Equal(userEntity.Firstname, newUser.FirstName);
            Assert.Equal(userEntity.Lastname, newUser.SecondName);
            Assert.NotNull(userEntity.Demographic);
            Assert.Equal(2, userEntity.Contacts.Count);

            //check if the user was assigned an email
            var emailContact = userEntity.Contacts.FirstOrDefault(c => c.ContactTypeId == (short)ContactTypes.Email).Value;
            Assert.Equal(emailContact, newUser.Email);

            //check if the user was assigned a phone number 
            var phoneContact = userEntity.Contacts.FirstOrDefault(c => c.ContactTypeId == (short)ContactTypes.Phone).Value;
            Assert.Equal(phoneContact, newUser.PhoneNumber);

            //
            //delete the user
            var resultCleanUp = _userService.Delete(createdUser).Result;
        }

        [Fact]
        public async  void OnLogin_ReturnOkResult()
        {
           // var userContr = new AccountController(_userManager, _singInManager, _roleManager, _context, _userRepository, _roleRepository);
            var role = _roleRepository.FindByNameAsync("Rw Nurse").Result;

            var newUser = new UserRegisterViewModel
            {
                UserName = "JMaombi",
                FirstName = "Joseph",
                SecondName = "Masengesh0",
                Email = "Jmasengesho@inyarwanda.com",
                NationalId = "1122334422679",
                PhoneNumber = "405-602-9804",
                Roles = new List<string> { role.Name },
                Password = "#Password111E",
                OrganizationId = 1
            };

            var deleteIfExisits = await _userService.GetByEmailAsync(newUser.Email);
            if (deleteIfExisits != null)
            {
                var result1 = _userService.Delete(deleteIfExisits).Result;
            }

            var result = _controller.Post(newUser).Result;
            var createdUser = await _userService.GetByEmailAsync(newUser.Email);
            var loginViewModel = new UserLoginViewModel
            {
                UserName = createdUser.Email,
                Password = newUser.Password
            };

            //login
            var loginrResult = await _controller.Login(loginViewModel);
            var okResult = Assert.IsType<OkObjectResult>(loginrResult);
            var feedback = Assert.IsType<FeedBack>(okResult.Value);
          
            var resultCleanUp = _userService.Delete(createdUser).Result;
        }

        [Fact]
        public async void OnCreate_ReturnsBadRequest_GivenInvalidModel()
        { 
            _controller.ModelState.AddModelError("Test Error", "This is just a test");
            var role = await _roleRepository.FindByNameAsync("Rw Nurse");
           

            var newUser = new UserRegisterViewModel
            {
                UserName = "JMaombi",
                FirstName = "Joseph",
                SecondName = "Masengesh0",
                Email = "Jmasengesho@inyarwanda.com",
                NationalId = "1122334422679",
                PhoneNumber = "405-602-9804",
                Roles = new List<string> { role.Name },
                Password = "#Password111E",
                OrganizationId = 1
            };

            var deleteIfExisits = await _userService.GetByEmailAsync(newUser.Email);
            if (deleteIfExisits != null)
            {
                var result1 = _userService.Delete(deleteIfExisits).Result;
            }

            var result = _controller.Post(newUser).Result;
  
            var badRequest = Assert.IsType<BadRequestObjectResult>(result);
            var returnFeedback = Assert.IsType<FeedBack>(badRequest.Value);
            Assert.Equal(true, returnFeedback.err);
        }

        [Fact]
        public async Task OnUpdateUser_ReturnOkayFeedbackIfUserExisits()
        {
            var role = _roleRepository.Roles(1).Result.FirstOrDefault();
            var newUser = new UserRegisterViewModel
            {
                UserName = "JMaombii",
                FirstName = "Joseeph",
                SecondName = "Masengesh01",
                Email = "Jmasengesho@stratadecision.com",
                NationalId = "112233442",
                PhoneNumber = "405-602-9806",
                Roles = new List<string> { role.Name },
                Password = "#Password111E",
                OrganizationId = 1
            };

            var deleteIfExisits = await _userService.GetByEmailAsync(newUser.Email);
            if (deleteIfExisits != null)
            {
                var result1 = _userService.Delete(deleteIfExisits).Result;
            }

            var result = _controller.Post(newUser).Result;
            var okResult = Assert.IsType<OkObjectResult>(result);
            var feedback = Assert.IsType<FeedBack>(okResult.Value);
            var createdUser = await _userService.GetByEmailAsync(newUser.Email);

            var updateModel = new EntityViewModel(createdUser.Entity);
            var newFName = "Joe";
            var newPhone = "2503450676";
            //updateModel.FirstName = newFName;
            //updateModel.PhoneNumber = newPhone;

            updateModel.Firstname = newFName;
            

            var updateResult = await _controller.Put(createdUser.UserId, updateModel);

            var updatedUser = await _userService.GetByPhoneNumberAsync("2503450676");
            Assert.NotNull(updatedUser);
            Assert.Equal(newFName, updatedUser.FirstName);
           // Assert.Equal(newPhone, updatedUser.PhoneNumber);


            var resultCleanUp = _userService.Delete(createdUser).Result;
        }

        [Fact]
        public async Task OnCreateNew_ReturnNeededInfo()
        {
            var result = await _controller.Create();           
            var okResult = Assert.IsType<OkObjectResult>(result);
            var feedback = Assert.IsType<FeedBack>(okResult.Value);
            var data = JsonConvert.DeserializeObject<NewUserViewModel>(feedback.data);
            Assert.Equal(data.Country, new Dictionary<int, string>(){{1, "Rwanda"}});
            Assert.True(data.Roles.Count>0);
            Assert.NotNull(result);

        }

        [Fact]
        public async Task OnGet_ReturnUserViewModel()
        {
            var result = await _controller.Get();
            var okResult = Assert.IsType<OkObjectResult>(result);
            var feedback = Assert.IsType<List<User>>(okResult.Value);


        }
      
    }
  
}
