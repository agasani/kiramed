﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using api;
using api.Auth;
using api.Auth.ErrorHandlers;
using api.Models;
using apiTests.Modules.UserModule;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.TestHost;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using Xunit;

namespace apiTests.Modules.UserModules
{
    public class  IdentityTestHelper : IClassFixture<TestFixture<Startup>>
    {
        private readonly IUserStore<ApplicationUser> userStore;
        private readonly IRoleStore<ApplicationRole> roleStore;
        private readonly IEnumerable<IPasswordValidator<ApplicationUser>> passwordvalidators;
        private readonly IEnumerable<IRoleValidator<ApplicationRole>> roleValidator;
        private readonly IEnumerable<IUserValidator<ApplicationUser>> userValidators;
        private readonly IOptions<IdentityOptions> optionAccess;
        private readonly IHttpContextAccessor httpContextAccessor;
        private readonly ILogger<UserManager<ApplicationUser>> userLogger;
        private readonly ILogger<RoleManager<ApplicationRole>> roleLogger;
        private readonly ILookupNormalizer lookUpNormalizer;
        private readonly IServiceProvider serviceProvider;
       private readonly IdentityErrorDescriber identityErrorDescriber;

        public IdentityTestHelper(TestFixture<Startup> fixture)
        {     
            var users = new List<UserModel>
            {
                new UserModel
                {
                    UserName = "Test",
                    Id = Guid.NewGuid().ToString(),
                    Email = "test@test.it"
                }

            }.AsQueryable();

            var fakeUserManager = new Mock<FakeUserManager>();

            fakeUserManager.Setup(x => x.Users)
                .Returns(users);

            fakeUserManager.Setup(x => x.DeleteAsync(It.IsAny<UserModel>()))
                .ReturnsAsync(IdentityResult.Success);
            fakeUserManager.Setup(x => x.CreateAsync(It.IsAny<UserModel>(), It.IsAny<string>()))
                .ReturnsAsync(IdentityResult.Success);
            fakeUserManager.Setup(x => x.UpdateAsync(It.IsAny<UserModel>()))
                .ReturnsAsync(IdentityResult.Success);


            // _userRepository = new NewUserRepository(fakeUserManager.Object, _context, _roleRepository);


            var mapper = (IMapper)fixture.Server.Host.Services.GetService(typeof(IMapper));
            var errorHandler = (IErrorHandler)fixture.Server.Host.Services.GetService(typeof(IErrorHandler));
            var passwordhasher = (IPasswordHasher<ApplicationUser>)fixture.Server.Host.Services.GetService(typeof(IPasswordHasher<ApplicationUser>));


            var uservalidator = new Mock<IUserValidator<ApplicationUser>>();
            uservalidator.Setup(x => x.ValidateAsync(It.IsAny<UserManager<ApplicationUser>>(), It.IsAny<ApplicationUser>()))
                .ReturnsAsync(IdentityResult.Success);
            var passwordvalidator = new Mock<IPasswordValidator<ApplicationUser>>();
            passwordvalidator.Setup(x => x.ValidateAsync(It.IsAny<UserManager<ApplicationUser>>(), It.IsAny<ApplicationUser>(), It.IsAny<string>()))
                .ReturnsAsync(IdentityResult.Success);

            var signInManager = new Mock<FakeSignInManager>();

            signInManager.Setup(
                    x => x.PasswordSignInAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<bool>()))
                .ReturnsAsync(Microsoft.AspNetCore.Identity.SignInResult.Success);

           
            userStore = (IUserStore<ApplicationUser>)fixture.Server.Host.Services.GetService(typeof(IUserStore<ApplicationUser>));
            roleStore = (IRoleStore<ApplicationRole>)fixture.Server.Host.Services.GetService(typeof(IRoleStore<ApplicationRole>));
            //    var passwordhasher = (IPasswordHasher<ApplicationUser>)fixture.Server.Host.Services.GetService(typeof(IPasswordHasher<ApplicationUser>));
            passwordvalidators = (IEnumerable<IPasswordValidator<ApplicationUser>>)fixture.Server.Host.Services.GetService(typeof(IEnumerable<IPasswordValidator<ApplicationUser>>));
            roleValidator = (IEnumerable<IRoleValidator<ApplicationRole>>)fixture.Server.Host.Services.GetService(typeof(IEnumerable<IRoleValidator<ApplicationRole>>));
            userValidators = (IEnumerable<IUserValidator<ApplicationUser>>)fixture.Server.Host.Services.GetService(typeof(IEnumerable<IUserValidator<ApplicationUser>>));
            optionAccess = (IOptions<IdentityOptions>)fixture.Server.Host.Services.GetService(typeof(IOptions<IdentityOptions>));
            httpContextAccessor = (IHttpContextAccessor)fixture.Server.Host.Services.GetService(typeof(IHttpContextAccessor));
            //    var userPrincipleFactory = (IUserClaimsPrincipalFactory<ApplicationUser>)fixture.Server.Host.Services.GetService(typeof(IUserClaimsPrincipalFactory<ApplicationUser>));
            userLogger = (ILogger<UserManager<ApplicationUser>>)fixture.Server.Host.Services.GetService(typeof(ILogger<UserManager<ApplicationUser>>));
            //    var signInLogger = (ILogger<SignInManager<ApplicationUser>>)fixture.Server.Host.Services.GetService(typeof(ILogger<SignInManager<ApplicationUser>>));
            roleLogger = (ILogger<RoleManager<ApplicationRole>>)fixture.Server.Host.Services.GetService(typeof(ILogger<RoleManager<ApplicationRole>>));
            lookUpNormalizer = (ILookupNormalizer)fixture.Server.Host.Services.GetService(typeof(ILookupNormalizer));
            serviceProvider = (IServiceProvider)fixture.Server.Host.Services.GetService(typeof(IServiceProvider));
            identityErrorDescriber = (IdentityErrorDescriber)fixture.Server.Host.Services.GetService(typeof(IdentityErrorDescriber));

        }

    }

    public class TestFixture<TStartup> : IDisposable where TStartup : class
    {
        public readonly TestServer Server;
        private readonly HttpClient _client;

        public TestFixture()
        {
            var builder = new WebHostBuilder()
                .UseContentRoot($"..\\..\\..\\..\\..\\api\\api\\")
                .UseStartup<TStartup>();

            Server = new TestServer(builder);
            _client = new HttpClient();
        }

        public void Dispose()
        {
            _client.Dispose();
            Server.Dispose();
        }

    }

    #region  Identity Services Helpers
    public class FakeUserManager : UserManager<ApplicationUser>
    {
        public FakeUserManager()
            : base(new Mock<IUserStore<ApplicationUser>>().Object,
                new Mock<IOptions<IdentityOptions>>().Object,
                new Mock<IPasswordHasher<ApplicationUser>>().Object,
                new IUserValidator<ApplicationUser>[0],
                new IPasswordValidator<ApplicationUser>[0],
                new Mock<ILookupNormalizer>().Object,
                new Mock<IdentityErrorDescriber>().Object,
                new Mock<IServiceProvider>().Object,
                new Mock<ILogger<UserManager<ApplicationUser>>>().Object)
        { }
    }

    public class FakeSignInManager : SignInManager<ApplicationUser>
    {
        public FakeSignInManager()
            : base(new Mock<FakeUserManager>().Object,
                new HttpContextAccessor(),
                new Mock<IUserClaimsPrincipalFactory<ApplicationUser>>().Object,
                new Mock<IOptions<IdentityOptions>>().Object,
                new Mock<ILogger<SignInManager<ApplicationUser>>>().Object)
        { }
    }
    #endregion

}
