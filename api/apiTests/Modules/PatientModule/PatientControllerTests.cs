﻿using System.Linq;
using System.Collections.Generic;
using Xunit;
using Microsoft.EntityFrameworkCore;
using api.Db;
using api.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using api.Controllers;
using api.Repositories;
using api.Services;
using Newtonsoft.Json;
using api.ViewModels;

namespace apiTests.Modules.PatientModule
{
    public class PatientControllerTests
    {
        private KiraContext _context;
        private IEnumerator<api.Models.Patient> _itirator;
        private readonly PatientRepository _patientRepository;
        private readonly PatientService _patientService;
        private readonly CountryRepository _countryRepository;
        public PatientControllerTests()
        {
            var db = new DbContextOptionsBuilder<KiraContext>();
            db.UseInMemoryDatabase();
            _context = new KiraContext(db.Options);
            _context.Database.EnsureDeleted();

            DbInitializer.Init(_context, true);

            _itirator = getTestPatients().GetEnumerator();

            _patientRepository = new PatientRepository(_context);
            _patientService = new PatientService(_context, _patientRepository);
            _countryRepository = new CountryRepository(_context, new OrganizationRepository(_context));

        }

        public IEnumerable<api.Models.Patient> getTestPatients()
        {
            var dem = new EntityDemographic { Sex = "F", DOB = new DateTime(1945, 1, 1), NationalID = "5670485555" };
            var dem2 = new EntityDemographic { Sex = "M", DOB = new DateTime(1944, 12, 12), NationalID = "9978586998" };
            var dem3 = new EntityDemographic { Sex = "M", DOB = new DateTime(1933, 1, 1), NationalID = "564766672" };
            var dem4 = new EntityDemographic { Sex = "F", DOB = new DateTime(1922, 12, 12), NationalID = "9008586998" };
            var dem5 = new EntityDemographic { Sex = "M", DOB = new DateTime(1991, 1, 1), NationalID = "56477772" };
            var dem6 = new EntityDemographic { Sex = "F", DOB = new DateTime(1919, 12, 12), NationalID = "9971026998" };
            var dem7 = new EntityDemographic { Sex = "F", DOB = new DateTime(1916, 1, 1), NationalID = "560029572" };
            var dem8 = new EntityDemographic { Sex = "F", DOB = new DateTime(1923, 12, 12), NationalID = "3228586998" };
            var dem9 = new EntityDemographic { Sex = "M", DOB = new DateTime(1996, 1, 1), NationalID = "564739511" };
            var dem10 = new EntityDemographic { Sex = "M", DOB = new DateTime(1991, 12, 12), NationalID = "9978509438" };

            var addr = new EntityAddress { Country = "RW", Address1 = "Kamutwe Zone 1", City = "Byo", Label = "Home", District = "Muhoro" };

            var contact = new EntityContact
            {
                Value = "07830574",
                BeginDate = DateTime.Today,
                Label = "Phone",
                ContactTypeId = 1
               // ContactType = new ContactType { Name = "Phone", Type = (int)ContactTypes.Phone }
            };


            var ent1 = new Entity { ExternalId = "ID969", Type = "P", Category = "P",
                Firstname = "Poko", Lastname = "Atta", Demographic = dem,
                Addresses = new List<EntityAddress> { addr }, Contacts=new List<EntityContact> { contact ,   new EntityContact { ContactTypeId = 2, Value = "pat0@kira.com" } } };
            var ent2 = new Entity { ExternalId = "ID648", Type = "P", Category = "P", Firstname = "Timba", Lastname = "Kaka", Demographic = dem2, Contacts = new List<EntityContact> { new EntityContact { ContactTypeId = 1, Value = "04566442" }, new EntityContact { ContactTypeId = 2, Value = "pat1@kira.com" } } };
            var ent3 = new Entity { ExternalId = "ID922", Type = "P", Category = "P", Firstname = "Jean", Lastname = "Noma", Demographic = dem3, Contacts = new List<EntityContact> { new EntityContact { ContactTypeId = 1, Value = "04566443" }, new EntityContact { ContactTypeId = 2, Value = "pat2@kira.com" } } };
            var ent4 = new Entity { ExternalId = "ID633", Type = "P", Category = "P", Firstname = "Papi", Lastname = "Kaka", Demographic = dem4, Contacts = new List<EntityContact> { new EntityContact { ContactTypeId = 1, Value = "04566442=3" }, new EntityContact { ContactTypeId = 2, Value = "pat3@kira.com" } } };
            var ent5 = new Entity { ExternalId = "ID944", Type = "P", Category = "P", Firstname = "Polete", Lastname = "Noma", Demographic = dem5, Contacts = new List<EntityContact> { new EntityContact { ContactTypeId = 1, Value = "04566444" }, new EntityContact { ContactTypeId = 2, Value = "pat4@kira.com" } } };
            var ent6 = new Entity { ExternalId = "ID655", Type = "P", Category = "P", Firstname = "Keba", Lastname = "Kaka", Demographic = dem6, Contacts = new List<EntityContact> { new EntityContact { ContactTypeId = 1, Value = "04566445" }, new EntityContact { ContactTypeId = 2, Value = "pat5@kira.com" } } };
            var ent7 = new Entity { ExternalId = "ID966", Type = "P", Category = "P", Firstname = "Nayo", Lastname = "Noma", Demographic = dem7 , Contacts = new List<EntityContact> { new EntityContact { ContactTypeId = 1, Value = "04566446" }, new EntityContact { ContactTypeId = 2, Value = "pat6@kira.com" } } };
            var ent8 = new Entity { ExternalId = "ID677", Type = "P", Category = "P", Firstname = "Mwana", Lastname = "Kaka", Demographic = dem8, Contacts = new List<EntityContact> { new EntityContact { ContactTypeId = 1, Value = "04566447" }, new EntityContact { ContactTypeId = 2, Value = "pat7@kira.com" } } };
            var ent9 = new Entity { ExternalId = "ID988", Type = "P", Category = "P", Firstname = "Mboka", Lastname = "Noma", Demographic = dem9, Contacts = new List<EntityContact> { new EntityContact { ContactTypeId = 1, Value = "04566448" }, new EntityContact { ContactTypeId = 2, Value = "pat8@kira.com" } } };
            var ent10 = new Entity { ExternalId = "ID699", Type = "P", Category = "P", Firstname = "Pepe", Lastname = "Kaka", Demographic = dem10, Contacts = new List<EntityContact> { new EntityContact { ContactTypeId = 1, Value = "04566449" }, new EntityContact { ContactTypeId = 2, Value = "pat9@kira.com" } } };
            
            var pat1 = new api.Models.Patient { CountryId = 1 ,MedicalRecordNumber = "6738457646", AllergyNote = "Not too bad of alrg.", Entity = ent1, NationalPatientId = "BU685730456" };
            var pat2 = new api.Models.Patient {CountryId = 1, MedicalRecordNumber = "674113322", AllergyNote = "No note.", Entity = ent2, NationalPatientId = "RW7757489222" };
            var pat3 = new api.Models.Patient {CountryId = 1, MedicalRecordNumber = "6738457633", AllergyNote = "Not too bad of alrg.", Entity = ent3, NationalPatientId = "RW7757489333" };
            var pat4 = new api.Models.Patient {CountryId = 1, MedicalRecordNumber = "6741144", AllergyNote = "No note.", Entity = ent4, NationalPatientId = "RW7757484444" };
            var pat5 = new api.Models.Patient {CountryId = 1, MedicalRecordNumber = "6738457555", AllergyNote = "Not too bad of alrg.", Entity = ent5, NationalPatientId = "RW7757455555" };
            var pat6 = new api.Models.Patient {CountryId = 1, MedicalRecordNumber = "67411666", AllergyNote = "No note.", Entity = ent6, NationalPatientId = "RW7757486666" };
            var pat7 = new api.Models.Patient {CountryId = 1, MedicalRecordNumber = "6738457677", AllergyNote = "Not too bad of alrg.", Entity = ent7, NationalPatientId = "RW7757489777" };
            var pat8 = new api.Models.Patient {CountryId = 1, MedicalRecordNumber = "674113388", AllergyNote = "No note.", Entity = ent10, NationalPatientId = "RW7757488888" };
            var pat9 = new api.Models.Patient {CountryId = 1, MedicalRecordNumber = "6738457699", AllergyNote = "Not too bad of alrg.", Entity = ent8, NationalPatientId = "RW77574899999" };
            var pat10 = new api.Models.Patient{CountryId = 1,  MedicalRecordNumber = "674113300", AllergyNote = "No note.", Entity = ent9, NationalPatientId = "RW7757489000" };

            var patients = new List<api.Models.Patient>() { pat1, pat2, pat3, pat4, pat5, pat6, pat7, pat8, pat9, pat10 };

            for(var idx=0; idx<10; idx++)
            {
                yield return patients[idx];
            }
        }

        public api.Models.Patient  getTestPat()
        {
            if(this._itirator.MoveNext())
                return this._itirator.Current;
            
            return null;
        }

        //public Mock<DbSet<Pat>> getPatMockSet()
        //{
        //    var data = getMockPatient().AsQueryable();

        //    var mockSet = new Mock<DbSet<Pat>>();
        //    mockSet.As<IQueryable<Pat>>().Setup(m => m.Provider).Returns(data.Provider);
        //    mockSet.As<IQueryable<Pat>>().Setup(m => m.Expression).Returns(data.Expression);
        //    mockSet.As<IQueryable<Pat>>().Setup(m => m.ElementType).Returns(data.ElementType);
        //    mockSet.As<IQueryable<Pat>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());
            
        //    return mockSet;
        //}

        [Fact]
        public void OnGet_ReturnsAListOfPatVModels()
        {
            var patContr = new PatientController(_patientService);

            var result = patContr.Get().Result;

            var okResult = Assert.IsType<OkObjectResult>(result);
            var returnPatients = Assert.IsType<List<PatientViewModel>>(okResult.Value);
            Assert.Equal(true, returnPatients.Any());
        
        }

        [Fact]
        public void OnGetId_ReturnsAPatVModel()
        {
            var patContr = new PatientController(_patientService);
            var pat = _context.Patients.FirstOrDefault();

            var result = patContr.Details(pat.Id).Result;

            var okResult = Assert.IsType<OkObjectResult>(result);
            var returnPat = Assert.IsType<PatientViewModel>(okResult.Value);
        }

        [Fact]
        public void OnGetIdIfPatNE_ReturnsHttpNotFound()
        {
            var patContr = new PatientController(_patientService);
            var testId = 911;

            var result = patContr.Details(testId).Result;

            var notFoundResult = Assert.IsType<NotFoundObjectResult>(result);
            var returnFeedback = Assert.IsType<FeedBack>(notFoundResult.Value);
            Assert.Equal(true, returnFeedback.err);
        }

        [Fact]
        public void OnCreate_ReturnsOkResultNFeedbackWithCreatedPatId()
        {
            var patContr = new PatientController(_patientService, _countryRepository);
            var pat = getTestPat();
            var model = new PatientRegistrationViewModel(pat);
          
            var result = patContr.Post(model).Result;


            var okResult = Assert.IsType<OkObjectResult>(result);
            var feedback = Assert.IsType<FeedBack>(okResult.Value);
            Assert.Contains("patientid", feedback.data.ToLower());

        }

        [Fact]
        public async void OnCreate_NPIShouldBeGenerated()
        {
            //var search = new KiraPatientSearchRepository();
            //var resultSearch = await search.Search("Z0ZAF");

            var patContr = new PatientController(_patientService, _countryRepository);
            var pat = getTestPat();
            var model = new PatientRegistrationViewModel(pat);
            
            var result = patContr.Post(model).Result;

            var okResult = Assert.IsType<OkObjectResult>(result);
            var feedback = Assert.IsType<FeedBack>(okResult.Value);
            var data = JsonConvert.DeserializeObject<Dictionary<string, int>>(feedback.data);
            var patId = data["PatientId"];
            var created = _context.Patients.Find(patId);
            Assert.NotEqual(pat.NationalPatientId, created.NationalPatientId);
            Assert.NotNull(created.NationalPatientId);
            Assert.Equal(11, created.NationalPatientId.Length);

        }

        [Fact]
        public void OnCreate_TrackNetIdShouldBeGenerated()
        {
            var patContr = new PatientController(_patientService, _countryRepository);
            var pat = getTestPat();
            var model = new PatientRegistrationViewModel(pat);
          
            var result = patContr.Post(model).Result;

            var okResult = Assert.IsType<OkObjectResult>(result);
            var feedback = Assert.IsType<FeedBack>(okResult.Value);
            var data = JsonConvert.DeserializeObject<Dictionary<string, int>>(feedback.data);
            var patId = data["PatientId"];
            var created = _context.Patients.Find(patId);
            Assert.NotEqual(pat.TracknetNumber, created.TracknetNumber);
            Assert.NotNull(created.TracknetNumber);
        }

        [Fact]
        public void OnCreate_shouldCreateEntForPat()
        {
            var patContr = new PatientController(_patientService, _countryRepository);
            var pat = getTestPat();
            var model = new PatientRegistrationViewModel(pat);
            var result = patContr.Post(model).Result;

            var okResult = Assert.IsType<OkObjectResult>(result);
            var feedback = Assert.IsType<FeedBack>(okResult.Value);
            var data = JsonConvert.DeserializeObject<Dictionary<string, int>>(feedback.data);
            var patId = data["PatientId"];
            var created = _context.Patients.Find(patId);
            Assert.NotNull(created.Entity);
            Assert.NotSame(pat.Entity, created.Entity);
        }

        [Fact]
        public void OnCreate_shouldCreateEntDemForPat()
        {
            var patContr = new PatientController(_patientService, _countryRepository);
            var pat = getTestPat();
            var model = new PatientRegistrationViewModel(pat);
      
            var result = patContr.Post(model).Result;

            var okResult = Assert.IsType<OkObjectResult>(result);
            var feedback = Assert.IsType<FeedBack>(okResult.Value);
            var data = JsonConvert.DeserializeObject<Dictionary<string, int>>(feedback.data);
            var patId = data["PatientId"];
            var created = _context.Patients.Find(patId);
            Assert.NotNull(created.Entity);
            Assert.NotNull(created.Entity.Demographic);
            Assert.NotSame(pat.Entity.Demographic, created.Entity.Demographic);

        }

        [Fact]
        public void OnCreate_ReturnsBadRequest_GivenInvalidModel()
        {
            var patContr = new PatientController(_patientService);
            patContr.ModelState.AddModelError("Test Error", "This is just a test");
            var pat = getTestPat();

            var model = new PatientRegistrationViewModel(pat);
           
            var result = patContr.Post(model).Result;

            var badRequest = Assert.IsType<BadRequestObjectResult>(result);
            var returnFeedback = Assert.IsType<FeedBack>(badRequest.Value);
            Assert.Equal(true, returnFeedback.err);
        }

        [Fact]
        public void OnCreate_ShouldCheckIfPatientExists_AndReturnPatId()
        {
            var dem = new EntityDemographic { Sex = "M", DOB = new DateTime(1990, 1, 1), NationalID = "123456789" };
            var contact1 = new EntityContact { Value = "jmasengesho11@gmail.com", ContactTypeId = (short)ContactTypes.Email };
            var contact2 = new EntityContact { Value = "4056029804", ContactTypeId = (short)ContactTypes.Phone };



            var ent = new Entity { Type = "P", Category = "P", Firstname = "Yohana", Lastname = "Matayo", Demographic = dem, Contacts = { contact2, contact1 } };
            var pat = new Patient { CountryId = 1, AllergyNote = "The patient is allergic to beans farts.", Entity = ent };
            var model = new PatientRegistrationViewModel(pat);
            var patContr = new PatientController(_patientService, _countryRepository);

            var result = patContr.Post(model).Result;

            var badRequest = Assert.IsType<BadRequestObjectResult>(result);
            var feedback = Assert.IsType<FeedBack>(badRequest.Value);
            Assert.Equal(true, feedback.err);
            Assert.Contains("patient already exists", feedback.message.ToLower());
            var data = JsonConvert.DeserializeObject(feedback.data);
            Assert.Contains("patientid", feedback.data.ToLower());
        }

        [Fact]
        public async void OnCreate_ShouldCheckIfPatientExists_IfMarkedDelReturnOK()
        {
            var pat = await _patientService.FindByNationalIdAsync("123456789");
            this._context.Entry(pat).Reference(p => p.Entity).Load();
            this._context.Entry<Entity>(pat.Entity).Reference(e => e.Demographic).Load();
            this._context.Entry<Entity>(pat.Entity).Collection<EntityContact>(e => e.Contacts).Load();
            this._context.Entry<Entity>(pat.Entity).Collection<EntityAddress>(e => e.Addresses).Load();

            var model = new PatientRegistrationViewModel(pat);

            var patContr = new PatientController(_patientService, _countryRepository);

            pat.IsDeleted = DateTime.Now;
            await _context.SaveChangesAsync();

            var result = patContr.Post(model).Result;

            var okRequest = Assert.IsType<OkObjectResult>(result);
            var rst = Assert.IsType<FeedBack>(okRequest.Value);
            Assert.Equal(false, rst.err);
        }

        ////TODO do contact
        [Fact]
        public void OnCreate_CreateContactsForEntity()
        {
            var patContr = new PatientController(_patientService, _countryRepository);
            var pat = getTestPat();
            var model = new PatientRegistrationViewModel(pat);

            var result = patContr.Post(model).Result;

            var okResult = Assert.IsType<OkObjectResult>(result);
            var feedback = Assert.IsType<FeedBack>(okResult.Value);
            var data = JsonConvert.DeserializeObject<Dictionary<string, int>>(feedback.data);
            var patId = data["PatientId"];
            var created = _context.Patients.Find(patId);
            _context.Entry<Patient>(created).Reference<Entity>(p => p.Entity).Load();
            _context.Entry<Entity>(created.Entity).Collection<EntityContact>(e => e.Contacts).Load();

            Assert.NotNull(created.Entity);
            Assert.NotNull(created.Entity.Contacts.FirstOrDefault());
        }

        [Fact]
        public void OnCreate_CreateAddressesForEntity()
        {
            var patContr = new PatientController(_patientService, _countryRepository);
            var pat = getTestPat();
            var model = new PatientRegistrationViewModel(pat);

            var result = patContr.Post(model).Result;

            var okResult = Assert.IsType<OkObjectResult>(result);
            var feedback = Assert.IsType<FeedBack>(okResult.Value);
            var data = JsonConvert.DeserializeObject<Dictionary<string, int>>(feedback.data);
            var patId = data["PatientId"];
            var created = _context.Patients.Find(patId);
            _context.Entry<Patient>(created).Reference<Entity>(p => p.Entity).Load();
            _context.Entry<Entity>(created.Entity).Collection<EntityAddress>(e => e.Addresses).Load();
            Assert.NotNull(created.Entity);
            Assert.NotNull(created.Entity.Addresses.FirstOrDefault());
        }
        [Fact]
        public void OnUpdate_AddressGetsUpdated()
        {
            var patContr = new PatientController(_patientService, _countryRepository);
            var pat = _context.Patients.FirstOrDefault();
            this._context.Entry<Patient>(pat).Reference(p => p.Entity).Load();
            this._context.Entry<Entity>(pat.Entity).Reference(e => e.Demographic).Load();
            this._context.Entry<Entity>(pat.Entity).Collection<EntityContact>(e => e.Contacts).Load();
            this._context.Entry<Entity>(pat.Entity).Collection<EntityAddress>(e => e.Addresses).Load();

            var model = new PatientRegistrationViewModel(pat);

            var addr1 = "123 Wewe";
            var city = "Kandahal";
            var country = "ZM";
            var district = "Amagepho";
            var label = "Work";

            var addressVModel = new EntityAddressViewModel(pat.Entity.Addresses.FirstOrDefault());
            addressVModel.Address1 = addr1;
            addressVModel.City = city;
            addressVModel.Country = country;
            addressVModel.District = district;
            addressVModel.Label = label;
            addressVModel.Deactivate = true;

            model.Addresses = new List<EntityAddressViewModel> { addressVModel };

            var result = patContr.Put(pat.Id, model).Result;

            var okResult = Assert.IsType<OkObjectResult>(result);
            var feedback = Assert.IsType<FeedBack>(okResult.Value);
            var data = JsonConvert.DeserializeObject<Dictionary<string, int>>(feedback.data);
            var patId = data["PatientId"];
            var created = _context.Patients.Find(patId);
            _context.Entry<Patient>(created).Reference<Entity>(p => p.Entity).Load();
            _context.Entry<Entity>(created.Entity).Collection<EntityAddress>(e => e.Addresses).Load();

            Assert.NotNull(created.Entity);
            var updatedAddress = created.Entity.Addresses.FirstOrDefault();
            Assert.NotNull(updatedAddress);

            Assert.Equal(addr1, updatedAddress.Address1);
            Assert.Equal(city, updatedAddress.City);
            Assert.Equal(country, updatedAddress.Country);
            Assert.Equal(district, updatedAddress.District);
            Assert.Equal(label, updatedAddress.Label);
            Assert.NotNull(updatedAddress.EndDate);
        }
        [Fact]
        public void OnUpdate_ContactGetsUpdated()
        {
            var patContr = new PatientController(_patientService, _countryRepository);
            var pat = _context.Patients.FirstOrDefault();
            this._context.Entry<Patient>(pat).Reference(p => p.Entity).Load();
            this._context.Entry<Entity>(pat.Entity).Reference(e => e.Demographic).Load();
            this._context.Entry<Entity>(pat.Entity).Collection<EntityContact>(e => e.Contacts).Load();
            this._context.Entry<Entity>(pat.Entity).Collection<EntityAddress>(e => e.Addresses).Load();
            var model = new PatientRegistrationViewModel(pat);

            var contactVModel = new EntityContactViewModel(pat.Entity.Contacts.FirstOrDefault());

            var value = "email@gmail.com";
            var label = "email";
            var contactTypeId = (short)ContactTypes.Email;
            contactVModel.Value = value;
            contactVModel.Label = label;
            contactVModel.Deactivate = true;
            contactVModel.ContactTypeId = contactTypeId;
           

            model.Contacts = new List<EntityContactViewModel> { contactVModel };

            var result = patContr.Put(pat.Id, model).Result;

            var okResult = Assert.IsType<OkObjectResult>(result);
            var feedback = Assert.IsType<FeedBack>(okResult.Value);
            var data = JsonConvert.DeserializeObject<Dictionary<string, int>>(feedback.data);
            var patId = data["PatientId"];
            var created = _context.Patients.Find(patId);
            _context.Entry<Patient>(created).Reference<Entity>(p => p.Entity).Load();
            _context.Entry<Entity>(created.Entity).Collection<EntityContact>(e => e.Contacts).Load();

            Assert.NotNull(created.Entity);
            var updated = created.Entity.Contacts.FirstOrDefault();
            Assert.NotNull(updated);

            Assert.Equal(value, updated.Value);
            Assert.Equal(label, updated.Label);
            Assert.Equal(contactTypeId, updated.ContactTypeId);
            Assert.NotNull(updated.EndDate);
        }
        [Fact]
        public void OnDelete_UserShouldBeMarkedDeleted()
        {
            var pat = _context.Patients.FirstOrDefault();
            var contr = new PatientController(_patientService);
            var result = contr.Delete(pat.Id).Result;

            var updated = _context.Patients.Find(pat.Id);

            var rst = Assert.IsType<OkObjectResult>(result);
            var feedback = Assert.IsType<FeedBack>(rst.Value);
            Assert.True(updated.IsDeleted < DateTime.Now);
        }

        [Fact]
        public void OnDelete_ReturnsBadRequestIfPatNE()
        {
            var patId = 911;
            var contr = new PatientController(_patientService);
            var result = contr.Delete(patId).Result;

            var rst = Assert.IsType<BadRequestObjectResult>(result);
            var feedback = Assert.IsType<FeedBack>(rst.Value);
            Assert.Equal(true, feedback.err);
        }

        [Fact]
        public void OnUpdate_ReturnsBadRequestOnInValidateModel()
        {
            var patContr = new PatientController(_patientService);
            patContr.ModelState.AddModelError("Test Error", "This is just a test");
            var pat = _context.Patients.FirstOrDefault();
            this._context.Entry<api.Models.Patient>(pat).Reference(p => p.Entity).Load();
            this._context.Entry<Entity>(pat.Entity).Reference(e => e.Demographic).Load();
            this._context.Entry<Entity>(pat.Entity).Collection<EntityContact>(e => e.Contacts).Load();
            this._context.Entry<Entity>(pat.Entity).Collection<EntityAddress>(e => e.Addresses).Load();

            var model = new PatientRegistrationViewModel(pat);

            var result = patContr.Put(pat.Id, model).Result;

            var badRequest = Assert.IsType<BadRequestObjectResult>(result);
            var feedback = Assert.IsType<FeedBack>(badRequest.Value);
            Assert.Equal(true, feedback.err);
        }

        [Fact]
        public void OnUpdate_PatGetsUpdated()
        {
            var controller = new PatientController(_patientService);
            var pat = _context.Patients.FirstOrDefault();
            this._context.Entry<api.Models.Patient>(pat).Reference(p => p.Entity).Load();
            this._context.Entry<Entity>(pat.Entity).Reference(e => e.Demographic).Load();
            this._context.Entry<Entity>(pat.Entity).Collection<EntityContact>(e => e.Contacts).Load();
            this._context.Entry<Entity>(pat.Entity).Collection<EntityAddress>(e => e.Addresses).Load();

            var model = new PatientRegistrationViewModel(pat);
            var deathDat = new DateTime(1935, 3, 8);
            var allergyNote = "This is a test allergy note";
            var nonCliNote = "This is a test non-clinical note";
            var patHIEFlg = false;
            var noKwnAlr = true;
            var noKwnMeds = true;

            model.DeathDate = deathDat;
            model.AllergyNote = allergyNote;
            model.NonClinicalNote = nonCliNote;
            model.AcceptedHIE = patHIEFlg;
            model.NoKnownAllergy = noKwnAlr;
            model.NoKnownMed = noKwnMeds;

            var result = controller.Put(pat.Id, model).Result;
            var updatedPat = _context.Patients.Find(pat.Id);

            Assert.Equal(deathDat, updatedPat.DeathDate);
            Assert.Equal(allergyNote, updatedPat.AllergyNote);
            Assert.Equal(nonCliNote, updatedPat.NonClinicalNote);
            Assert.Equal(patHIEFlg, updatedPat.AcceptedHIE);
            Assert.Equal(noKwnAlr, updatedPat.NoKnownAllergy);
            Assert.Equal(noKwnMeds, updatedPat.NoKnownMed);

        }

        [Fact]
        public void OnUpdate_PatDontGetUpdated()
        {
            var controller = new PatientController(_patientService);
            var pat = _context.Patients.FirstOrDefault();
            this._context.Entry<api.Models.Patient>(pat).Reference(p => p.Entity).Load();
            this._context.Entry<Entity>(pat.Entity).Reference(e => e.Demographic).Load();
            this._context.Entry<Entity>(pat.Entity).Collection<EntityContact>(e => e.Contacts).Load();
            this._context.Entry<Entity>(pat.Entity).Collection<EntityAddress>(e => e.Addresses).Load();

            var model = new PatientRegistrationViewModel(pat);

            var entId = 911;
            var demId = 12345;
            var medRecNum = "98765";
            var npi = "RWA1234567890";
            var tracNum = "123456789"; //should this be editable???

           // model.EntityId = entId;
            model.MedicalRecordNumber = medRecNum;
            model.NationalPatientId = npi;
            model.TracknetNumber = tracNum;

            var result = controller.Put(pat.Id, model).Result;
            var updatedPat = _context.Patients.Find(pat.Id);

            Assert.NotEqual(entId, updatedPat.EntityId);
   
            Assert.NotEqual(medRecNum, updatedPat.MedicalRecordNumber);
            Assert.NotEqual(npi, updatedPat.NationalPatientId);

        }

        [Fact]
        public void OnUpdate_EntGetsUpdated()
        {
            var controller = new PatientController(_patientService);
            var pat = _context.Patients.FirstOrDefault();
            this._context.Entry<api.Models.Patient>(pat).Reference(p => p.Entity).Load();
            this._context.Entry<Entity>(pat.Entity).Reference(e => e.Demographic).Load();
            this._context.Entry<Entity>(pat.Entity).Collection<EntityContact>(e => e.Contacts).Load();
            this._context.Entry<Entity>(pat.Entity).Collection<EntityAddress>(e => e.Addresses).Load();

            var model = new PatientRegistrationViewModel(pat);
            var firsname = "testfirstname";
            var lastname = "testlastname";
            var extId = "123";

            model.FirstName = firsname;
            model.SecondName = lastname;
            model.NationalId = extId;
            

            var result = controller.Put(pat.Id, model).Result;
            var updatedPat = _context.Patients.Find(pat.Id);

            Assert.Equal(firsname, updatedPat.Entity.Firstname);
            Assert.Equal(lastname, updatedPat.Entity.Lastname);
            //Assert.Equal(activ, updatedPat.Entity.IsActive);
            Assert.Equal(extId, updatedPat.Entity.ExternalId);
        }

        [Fact]
        public void OnUpdate_EntDontGetUpdated()
        {
            var controller = new PatientController(_patientService);
            var pat = _context.Patients.FirstOrDefault();
            this._context.Entry<api.Models.Patient>(pat).Reference(p => p.Entity).Load();
            this._context.Entry<Entity>(pat.Entity).Reference(e => e.Demographic).Load();
            this._context.Entry<Entity>(pat.Entity).Collection<EntityContact>(e => e.Contacts).Load();
            this._context.Entry<Entity>(pat.Entity).Collection<EntityAddress>(e => e.Addresses).Load();

            var model = new PatientRegistrationViewModel(pat);
            var spc = "tst";
            var typ = "C";
            var cat = "X";
            var etrDat = new DateTime(1944, 5, 13);

            //model.Specialty = spc;
            //model.Type = typ;
            //model.Category = cat;
            //model.EntryDate = etrDat;
                              
            var result = controller.Put(pat.Id, model).Result;
            var updatedPat = _context.Patients.Find(pat.Id);

            Assert.NotEqual(spc, updatedPat.Entity.Specialty);
            Assert.NotEqual(typ, updatedPat.Entity.Type);
            Assert.NotEqual(cat, updatedPat.Entity.Category);
            Assert.NotEqual(etrDat, updatedPat.Entity.EntryDate);
        }

        [Fact]
        public void OnUpdate_EntDemGetUpdated()
        {
            var controller = new PatientController(_patientService);
            var pat = _context.Patients.FirstOrDefault();
            this._context.Entry<api.Models.Patient>(pat).Reference(p => p.Entity).Load();
            this._context.Entry<Entity>(pat.Entity).Reference(e => e.Demographic).Load();
            this._context.Entry<Entity>(pat.Entity).Collection<EntityContact>(e => e.Contacts).Load();
            this._context.Entry<Entity>(pat.Entity).Collection<EntityAddress>(e => e.Addresses).Load();

            var model = new PatientRegistrationViewModel(pat);

            var nid = "123456789";
            var prmLan = "RU";
            var ndLan = "FR";
            var sex = "M";
            var mrtStatus = "S";
            var rlg = "C";
            var inc = 100000;
            var bday = new DateTime(1969, 6, 9);

            model.Demographic.NationalID = nid;
            model.Demographic.PrimaryLanguage = prmLan;
            model.Demographic.SecondaryLanguage = ndLan;
            model.Demographic.Sex = sex;
            model.Demographic.MartalStatus = mrtStatus;
            model.Demographic.Religion = rlg;
            model.Demographic.Income = inc;
            model.Demographic.DOB = bday;

            var result = controller.Put(pat.Id, model).Result;
            var updatedPat = _context.Patients.Find(pat.Id);

            Assert.Equal(nid, updatedPat.Entity.Demographic.NationalID);
            Assert.Equal(prmLan, updatedPat.Entity.Demographic.PrimaryLanguage);
            Assert.Equal(ndLan, updatedPat.Entity.Demographic.SecondaryLanguage);
            Assert.Equal(sex, updatedPat.Entity.Demographic.Sex);
            Assert.Equal(mrtStatus, updatedPat.Entity.Demographic.MartalStatus);
            Assert.Equal(rlg, updatedPat.Entity.Demographic.Religion);
            Assert.Equal(inc, updatedPat.Entity.Demographic.Income);
            Assert.Equal(bday, updatedPat.Entity.Demographic.DOB);
        }

        [Fact]
        public void OnUpdate_EntDemDontGetUpdated()
        {
            var controller = new PatientController(_patientService);
            var pat = _context.Patients.FirstOrDefault();
            this._context.Entry<api.Models.Patient>(pat).Reference(p => p.Entity).Load();
            this._context.Entry<Entity>(pat.Entity).Reference(e => e.Demographic).Load();
            this._context.Entry<Entity>(pat.Entity).Collection<EntityContact>(e => e.Contacts).Load();
            this._context.Entry<Entity>(pat.Entity).Collection<EntityAddress>(e => e.Addresses).Load();

            var model = new PatientRegistrationViewModel(pat);

            var demId = 777;
            model.Demographic.Id = demId;

            var result = controller.Put(pat.Id, model).Result;
            var updatedPat = _context.Patients.Find(pat.Id);
        }



       

    }

}
