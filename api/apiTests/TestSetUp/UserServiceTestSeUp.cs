﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using api;
using api.Auth;
using api.Auth.ErrorHandlers;
using api.Auth.Roles;
using api.Controllers;
using api.Db;
using api.Models;
using api.Services;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.TestHost;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using Xunit;

namespace apiTests.TestSetUp
{
    public class UserServiceTestSeUp : IClassFixture<TestFixture<Startup>>
    {
        internal KiraContext _context;
        internal IDdbContext _idDbContext;
        internal IUserRepository _userRepository { get; }
        internal IUserService _userService { get; }
        internal readonly IRoleRepository _roleRepository;
        internal readonly RoleManager<ApplicationRole> _roleManager;
        internal readonly UserManager<ApplicationUser> _userManager;
        internal readonly ICountryRepository _countryRepository;
        internal readonly IEmailService _emailService;
        internal readonly IOrganizationRepository _organizationRepository;
        internal readonly IOrganizationService _organizationService;
        internal readonly IVisitService _visitService;

        public UserServiceTestSeUp(TestFixture<Startup> fixture)
        {
            var db = new DbContextOptionsBuilder<KiraContext>();
            db.UseInMemoryDatabase();
            _context = new KiraContext(db.Options);
            _context.Database.EnsureDeleted();

            var idDb = new DbContextOptionsBuilder<IDdbContext>();
            idDb.UseInMemoryDatabase();
            _idDbContext = new IDdbContext(idDb.Options);
            _idDbContext.Database.EnsureDeleted();

            var users = new List<UserModel>
            {
                new UserModel
                {
                    UserName = "Test",
                    Id = Guid.NewGuid().ToString(),
                    Email = "test@test.it"
                }

            }.AsQueryable();

            var fakeUserManager = new Mock<FakeUserManager>();

            fakeUserManager.Setup(x => x.Users)
                .Returns(users);

            fakeUserManager.Setup(x => x.DeleteAsync(It.IsAny<UserModel>()))
                .ReturnsAsync(IdentityResult.Success);
            fakeUserManager.Setup(x => x.CreateAsync(It.IsAny<UserModel>(), It.IsAny<string>()))
                .ReturnsAsync(IdentityResult.Success);
            fakeUserManager.Setup(x => x.UpdateAsync(It.IsAny<UserModel>()))
                .ReturnsAsync(IdentityResult.Success);


            var mapper = (IMapper)fixture.Server.Host.Services.GetService(typeof(IMapper));
            var errorHandler = (IErrorHandler)fixture.Server.Host.Services.GetService(typeof(IErrorHandler));
            var passwordhasher = (IPasswordHasher<ApplicationUser>)fixture.Server.Host.Services.GetService(typeof(IPasswordHasher<ApplicationUser>));


            var uservalidator = new Mock<IUserValidator<ApplicationUser>>();
            uservalidator.Setup(x => x.ValidateAsync(It.IsAny<UserManager<ApplicationUser>>(), It.IsAny<ApplicationUser>()))
                .ReturnsAsync(IdentityResult.Success);
            var passwordvalidator = new Mock<IPasswordValidator<ApplicationUser>>();
            passwordvalidator.Setup(x => x.ValidateAsync(It.IsAny<UserManager<ApplicationUser>>(), It.IsAny<ApplicationUser>(), It.IsAny<string>()))
                .ReturnsAsync(IdentityResult.Success);

            var signInManager = new Mock<FakeSignInManager>();

            signInManager.Setup(
                    x => x.PasswordSignInAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<bool>()))
                .ReturnsAsync(Microsoft.AspNetCore.Identity.SignInResult.Success);
      
            _userManager = (UserManager<ApplicationUser>)fixture.Server.Host.Services.GetService(typeof(UserManager<ApplicationUser>));
            _roleManager = (RoleManager<ApplicationRole>)fixture.Server.Host.Services.GetService(typeof(RoleManager<ApplicationRole>));
            
            //SERVICES CONFIGURATIONS
            
            _roleRepository = new RoleRepository(_roleManager, _context);
            _userRepository = new UserRepository(_userManager, _roleManager, _context);
            _emailService = new EmailService();
            _organizationRepository = new OrganizationRepository(_context);
            _organizationService =  new OrganizationService(_organizationRepository, _userService, _visitService);
            _countryRepository = new CountryRepository(_context, _organizationRepository);

            _userService = new UserService(_userRepository, signInManager.Object, _roleRepository, _countryRepository , _emailService, uservalidator.Object, passwordvalidator.Object, passwordhasher);
            
           
            DbInitializer.Init(_context, true);
            AuthInitializer.Init(_idDbContext, _userManager, _roleManager, _context);
        }
    }

    public class FakeUserManager : UserManager<ApplicationUser>
    {
        public FakeUserManager()
            : base(new Mock<IUserStore<ApplicationUser>>().Object,
                new Mock<IOptions<IdentityOptions>>().Object,
                new Mock<IPasswordHasher<ApplicationUser>>().Object,
                new IUserValidator<ApplicationUser>[0],
                new IPasswordValidator<ApplicationUser>[0],
                new Mock<ILookupNormalizer>().Object,
                new Mock<IdentityErrorDescriber>().Object,
                new Mock<IServiceProvider>().Object,
                new Mock<ILogger<UserManager<ApplicationUser>>>().Object)
        { }
    }

    public class FakeSignInManager : SignInManager<ApplicationUser>
    {
        public FakeSignInManager()
            : base(new Mock<FakeUserManager>().Object,
                new HttpContextAccessor(),
                new Mock<IUserClaimsPrincipalFactory<ApplicationUser>>().Object,
                new Mock<IOptions<IdentityOptions>>().Object,
                new Mock<ILogger<SignInManager<ApplicationUser>>>().Object)
        { }
    }


    public class TestFixture<TStartup> : IDisposable where TStartup : class
    {
        public readonly TestServer Server;
        private readonly HttpClient _client;

        public TestFixture()
        {
            var builder = new WebHostBuilder()
                .UseContentRoot($"..\\..\\..\\..\\..\\api\\api\\")
                .UseStartup<TStartup>();

            Server = new TestServer(builder);
            _client = new HttpClient();
        }

        public void Dispose()
        {
            _client.Dispose();
            Server.Dispose();
        }

    }
}
