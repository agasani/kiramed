﻿using System.Security.Claims;

namespace api.Auth
{
    public class ApplicationClaim : Claim
    {
       public ApplicationClaim(string type, string value) : base(type, value)
        {
           
        }
        public string ActionName { get; set; }  //this attribute was added because we do not want one-to-one relationship between a role and a claim (claim caint exist witout a role)
    }
}
