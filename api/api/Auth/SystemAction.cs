﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using api.Auth.Roles;
using api.Models;

namespace api.Auth
{
    [Table("SystemActions")]
    public class SystemAction : KiraEntity
    {
        public int Id { get; set; }
        public int ClaimNumber { get; set; }
        public string ActionName { get; set; }
        public string ControllerName { get; set; }
        public DateTime DateTimeCreated { get; set; }

        public SystemAction(ActionViewModel model)
        {
            ActionName = model.ControllerName + "." + model.ActionName;
            ControllerName = model.ControllerName;
            DateTimeCreated = DateTime.Now;
            ClaimNumber = 0;
        }

        public SystemAction() { }
    }
}
