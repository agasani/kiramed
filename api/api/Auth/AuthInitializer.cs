﻿using System;
using System.Collections.Generic;
using System.Linq;
using api.Auth.Roles;
using Microsoft.AspNetCore.Identity;
using api.Db;
using api.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;

namespace api.Auth
{
    public static class AuthInitializer
    {
  
        public static void ResetTables(IDdbContext context, bool isTest = false)
        {
         
            context.Database.EnsureDeleted();
            context.Database.EnsureCreated();
        }

        public static void Init(IDdbContext idDbContext, UserManager<ApplicationUser> userManager,
            RoleManager<ApplicationRole> roleManager, KiraContext kiraContext, bool isTest = false)
        {
            // ResetTables(kiraContext, true);

            var _userManager = userManager;
            // var _roleManager = roleManager;
            if (_userManager.Users.Any()) return;

            #region Get countries

            //set up country system roles
            var co = kiraContext.Countries.FirstOrDefault(c => c.Abbreviation == "RW");

            var globalSystemAdmin = new ApplicationRole
            {
                Name = "System  Admin",
                OrganizationId = -1,
                Description = " Global Country Admin ",
                Type = (short)eRoleType.Global
            };
            var globalSystemAdminResult = roleManager.CreateAsync(globalSystemAdmin).Result;



            var globalCountryAdmin = new ApplicationRole
            {
                Name = "Country Admin",
                OrganizationId = -1,
                Description = " Global Country Admin ",
                Type = (short)eRoleType.Global
            };
            var globalCountryAdminResult = roleManager.CreateAsync(globalCountryAdmin).Result;


            var globalOrganizationAdmin = new ApplicationRole
            {
                Name = "Organization Admin",
                OrganizationId = -1,
                Description = " Global Organization Admin ",
                Type = (short)eRoleType.Global
            };
            var globalOrganizationAdminResult = roleManager.CreateAsync(globalOrganizationAdmin).Result;


            var globalNurse = new ApplicationRole
            {
                Name = "Global Nurse",
                OrganizationId = -1,
                Description = " Global Nurse Role ",
                Type = (short)eRoleType.Global
            };
            var globalNurseResult = roleManager.CreateAsync(globalNurse).Result;

            var globalDoctor = new ApplicationRole
            {
                Name = "Global Doctor",
                OrganizationId = -1,
                Description = "Global Doctor Role ",
                Type = (short)eRoleType.Global
            };
            var globalDoctorResult = roleManager.CreateAsync(globalDoctor).Result;



            var countryNurse = new ApplicationRole
            {
                Name = "Rw Nurse",
                OrganizationId = co.Id,
                Description = " Rwanda Nurse Role ",
                Type = (short)eRoleType.Country
            };
            var nurseResult = roleManager.CreateAsync(countryNurse).Result;

            var countryDoctor = new ApplicationRole
            {
                Name = "Rw Doctor",
                OrganizationId = co.Id,
                Description = "Rwanda Doctor Role ",
                Type = (short)eRoleType.Country
            };
            var doctorResult = roleManager.CreateAsync(countryDoctor).Result;

            var countryAccountant = new ApplicationRole
            {
                Name = "Rw Accountant",
                OrganizationId = co.Id,
                Description = "Rwanda Accountat Role ",
                Type = (short)eRoleType.Country
            };
            var accountantResult = roleManager.CreateAsync(countryAccountant).Result;

            var countryLabTech = new ApplicationRole
            {
                Name = "Rw Lab Technician",
                OrganizationId = co.Id,
                Description = "Rwandan Lab Technitian Role ",
                Type = (short)eRoleType.Country
            };
            var labTechResult = roleManager.CreateAsync(countryLabTech).Result;

            var countryReceptionist = new ApplicationRole
            {
                Name = "Rw Receptionist",
                OrganizationId = co.Id,
                Description = "Rwandan Receptionist Role ",
                Type = (short)eRoleType.Country
            };
            var receptionistResult = roleManager.CreateAsync(countryReceptionist).Result;


            #endregion

            #region Get Organizations

            var org = kiraContext.Organizations.Find(1);
            //set up country org roles

            var orgNurse = new ApplicationRole
            {
                Name = "Org 1 Nurse",
                OrganizationId = org.Id,
                Description = " Rwanda Nurse Role ",
                Type = (short)eRoleType.Organization
            };
            var orgNurseResult = roleManager.CreateAsync(orgNurse).Result;

            #endregion

            #region Users Initialization

            var dem1 = new EntityDemographic { Sex = "M", DOB = new DateTime(1990, 1, 1), NationalID = "9922334455" };
            var dem2 = new EntityDemographic { Sex = "M", DOB = new DateTime(1970, 12, 12), NationalID = "9933445566" };
            var dem3 = new EntityDemographic { Sex = "F", DOB = new DateTime(1933, 6, 6), NationalID = "9944556677" };

            var addr1 = new EntityAddress { Country = "RW", Address1 = "Kamutwe Zone 1", City = "Byo", Label = "Home", District = "Muhoro" };
            var addr2 = new EntityAddress { Country = "TZ", Address1 = "Kamuduha Quartier 123", City = "Rugobe", Label = "Home", District = "Kagugu" };
            var addr3 = new EntityAddress { Country = "UG", Address1 = "Mbalaba 432", City = "Mbalala", Label = "Home", District = "South" };


            var entCont1 = new EntityContact { ContactTypeId = (short)  ContactTypes.Phone, Value = "405-435-99999" };
            var entCont2 = new EntityContact { ContactTypeId = (short)ContactTypes.Email, Value = "doctor1@inyarwanda.com" };
            var entCont3 = new EntityContact { Value = "405-435-6666", ContactTypeId = (short)ContactTypes.Phone, BeginDate = DateTime.Today };


            var ent1 = new Entity { ExternalId = "ID1234", Type = "P", Category = "P", Firstname = "Yohana", Lastname = "Matayo", Demographic = dem1, Addresses = new List<EntityAddress> { addr1 }, Contacts = new List<EntityContact> { entCont1, entCont2, entCont3 } };
            kiraContext.Entities.Add(ent1);
            var ent2 = new Entity { ExternalId = "ID8065", Type = "P", Category = "P", Firstname = "Kamana", Lastname = "Kalisa", Demographic = dem3, Addresses = new List<EntityAddress> { addr2 } };
            kiraContext.Entities.Add(ent2);
            var ent3 = new Entity { ExternalId = "ID4565", Type = "P", Category = "P", Firstname = "kananga", Lastname = "Siba", Demographic = dem2, Addresses = new List<EntityAddress> { addr3 } };
            kiraContext.Entities.Add(ent3);

            kiraContext.SaveChanges();

            //nurse
            var user1 = new ApplicationUser { UserName = "joemasengesho1", Email = "joemasengesho@inyarwanda.com", CreatedDate = DateTime.Now, ModifiedDate = DateTime.Now, EntityId = ent1.Id };
            var result =  userManager.CreateAsync(user1, "Password1").Result;
            if (result.Succeeded)
            {
                var rst = _userManager.AddToRoleAsync(user1, countryNurse.Name).Result;
                var userOrg = new UserOrganization {RoleId = countryNurse.Id, OrganizationId = org.Id, StartDate = user1.CreatedDate, UserId = user1.Id, EndDate = new DateTime(9999, 12, 31) };
                kiraContext.UserOrganizations.Add(userOrg);
                kiraContext.SaveChanges();
            }

            //doctor
            var user2 = new ApplicationUser { UserName = "joemasengesho2", Email = "joemasengesho2@kiramd.com", CreatedDate = DateTime.Now, ModifiedDate = DateTime.Now, EntityId = ent2.Id };
            var result2 =  _userManager.CreateAsync(user2, "Password1").Result;
            if (result2.Succeeded)
            {
                var rst = _userManager.AddToRoleAsync(user2, countryDoctor.Name).Result;
                var userOrg = new UserOrganization { RoleId = countryDoctor.Id, OrganizationId = org.Id, StartDate = user2.CreatedDate, UserId = user2.Id, EndDate = new DateTime(9999, 12, 31) };
                kiraContext.UserOrganizations.Add(userOrg);
                kiraContext.SaveChanges();
            }
            //receptionist
            var user3 = new ApplicationUser { UserName = "joemasengesho3", Email = "joemasengesho3@kiramd.com", CreatedDate = DateTime.Now, ModifiedDate = DateTime.Now, EntityId = ent3.Id };
            var result3 =  userManager.CreateAsync(user3, "Password1").Result;
            if (result3.Succeeded)
            {
                var rst = _userManager.AddToRoleAsync(user3, countryReceptionist.Name).Result;
                var userOrg = new UserOrganization { RoleId = countryReceptionist.Id, OrganizationId = org.Id, StartDate = user3.CreatedDate, UserId = user3.Id, EndDate = new DateTime(9999, 12, 31) };
                kiraContext.UserOrganizations.Add(userOrg);
                kiraContext.SaveChanges();
            }
        }
      #endregion

        public static void Clear<T>(this DbSet<T> dbSet) where T : class
        {
            dbSet.RemoveRange(dbSet);
        }
    }
}
