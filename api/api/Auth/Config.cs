﻿using IdentityServer4;
using IdentityServer4.Models;
using IdentityServer4.Test;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace api.Auth
{
    public class Config
    {
        public static IEnumerable<ApiResource> GetApiResources()
        {
            var kira = new ApiResource("KirApi", "KiraMed Api");
            kira.UserClaims = new List<string> { ClaimTypes.AuthorizationDecision };

            return new List<ApiResource> { kira };
        }

        public static IEnumerable<Client> getClients(List<Claim> claims)
        {
            return new List<Client>
            {
                new Client
                {
                    ClientId = "KiraWeb",
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPassword,
                    ClientSecrets =
                    {
                        new Secret("564ht32io8#479ierye@578gk5te6gu9".Sha256())
                    },
                    AllowedScopes = { "KirApi",
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        IdentityServerConstants.StandardScopes.OfflineAccess,
                        IdentityServerConstants.StandardScopes.Profile
                    }
                    , AllowOfflineAccess = true
                    , AccessTokenLifetime = 2700
                    //TODO get the list of claims from the config file..
                    , Claims = claims
                    , UpdateAccessTokenClaimsOnRefresh = true
                    //, AlwaysSendClientClaims = true
                    , AlwaysIncludeUserClaimsInIdToken = true
                    ,AllowedCorsOrigins = new List<string>{ "http://localhost:55158", "http://localhost:8080", @"chrome-extension://fhbjgbiflinjbdggehcddcbncdddomop" }
                }
            };
        }

        public static IEnumerable<IdentityResource> GetIdentityResources()
        {
            return new List<IdentityResource>
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile()/*,
                new IdentityResource {
                    Name = "auth",
                    Required = true,
                    DisplayName = "auth",
                    Emphasize = true,
                    UserClaims = new List<string> { ClaimTypes.AuthorizationDecision }
                }*/
            };
        }

        public static List<TestUser> GetUsers()
        {
            return new List<TestUser>
            {
                new TestUser
                {
                    SubjectId = "1",
                    Username = "kira",
                    Password = "akabanga",
                    Claims = new List<Claim>
                    {
                        new Claim("patient", "own"),
                        new Claim("patientinfo", "all")
                    }
                },
                new TestUser
                {
                    SubjectId = "2",
                    Username = "tester",
                    Password = "letmein",
                    Claims = new List<Claim>
                    {
                        new Claim("patient", "own"),
                        new Claim("patientinfo", "all")
                    }
                },
                new TestUser
                {
                    SubjectId = "3",
                    Username = "bobiller",
                    Password = "thebiller",
                    Claims = new List<Claim>
                    {
                        new Claim("billing", "123"),
                        new Claim("budget", "all")
                    }
                }
            };
        }
        
    }
}
