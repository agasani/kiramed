﻿using api.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api.Auth
{
    public class ApplicationUser: IdentityUser
    {
        //put custom properties here..
        public int EntityId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? IsDeleted { get; set; } = DateTime.MaxValue;
        public DateTime? IsLocked { get; set; } = DateTime.MaxValue;
        public DateTime ModifiedDate { get; set; }
 

        public ApplicationUser(User user)
        {
            //EntityId = user.Entity.Id;
            UserName = user.UserName;
            Email = user.Email;
            PhoneNumber = user.PhoneNumber;
        }

        public ApplicationUser()
        {

        }
    }

   

    public enum UserTypes { Patient = 1, Nurse = 1, Physician =3 , LabTechnician = 4, Receiptionist = 5, Biller = 6 };
}
