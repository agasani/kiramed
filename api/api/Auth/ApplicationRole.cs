﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using api.Auth.Roles;

namespace api.Auth
{
    public class ApplicationRole : IdentityRole
    {
        public int OrganizationId { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]      
        [Required]
        public string Description { get; set; }
        public DateTime CreatedDate { get; set; } = DateTime.Now;
        public DateTime ModifiedDate { get; set; } = DateTime.Now;
        [Required]
        public short Type { get; set; }

        public void Update(RoleViewModel role)
        {
            Name = role?.Name;
            Description = role?.Description;
            ModifiedDate = DateTime.Now;
        }
    }
}
