﻿using api.Auth.Roles;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace api.Auth
{
    public class RoleViewModel
    {
        /// <summary>
        /// Role required string name
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Role required max-length 100 string description
        /// </summary>
        [Required]
        [StringLength(100)]
        public string Description { get; set; }

        /// <summary>
        /// Role string roleId
        /// </summary>
        public string RoleId { get; set; }

        /// <summary>
        /// Role parent string organization id
        /// </summary>
        [Required]
        public int OrganizationId { get; set; }

        /// <summary>
        /// Role created date
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Role modified date
        /// </summary>
        public DateTime ModifiedDate { get; set; }

        /// <summary>
        /// Role string type
        /// </summary>
        public short Type { get; set; }

        /// <summary>
        /// Role integer id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Role array of string claims
        /// </summary>
        public List<string> Claims { get; set; }

        public RoleViewModel() { }

        public RoleViewModel (ApplicationRole role)
        {
            Name = role.Name;
            Description = role.Description;
            RoleId = role.Id;
            OrganizationId = role.OrganizationId;
            CreatedDate = role.CreatedDate;
            ModifiedDate = role.ModifiedDate;
            Type = role.Type;
            Id = 0;
        }
    }
}
