﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Linq.Expressions;
using api.Models;
using Microsoft.AspNetCore.Identity;

namespace api.Auth.Roles
{
    public interface IRoleRepository
    {
        Task<IList<ApplicationRole>> Roles(int orgId);
        Task<IList<ApplicationRole>> Roles();
        Task<ApplicationRole> FindByIdAsync(string roleId);
        Task<ApplicationRole> FindByNameAsync(string roleId);
        Task<IdentityResult> CreateAsync(ApplicationRole role);
        Task<IdentityResult> AddActionsAsync(ApplicationRole role, IList<string> roleActions);
        Task<IdentityResult> RemoveActionsAsync(ApplicationRole role, IList<string> roleActions);
        Task<IdentityResult> AddActionAsync(ApplicationRole role, string roleAction);
        Task<IdentityResult> RemoveActionAsync(ApplicationRole role, string roleAction);
        Task<Claim> GetRoleClaim(ApplicationRole role, string action);
        ApplicationRole FindById(string roleId);
        ApplicationRole FindByName(string roleId);
        IdentityResult Create(ApplicationRole role);
        IdentityResult Update(ApplicationRole role);
        Task<IdentityResult> UpdateAsync(ApplicationRole role);
        IdentityResult Delete(ApplicationRole role);
        Task<IdentityResult> DeleteAsync(ApplicationRole role);
        Task<bool> HasClaim(ApplicationRole role, string claimAction);
        List<short> GetRoleTypes();
        Task<IEnumerable<ApplicationRole>> GetAsync(Expression<Func<ApplicationRole, bool>> expression);
        Task<IEnumerable<ApplicationRole>> GetCountryRoles(int countryId);
        Task<IEnumerable<ApplicationRole>> GetOrganizationRolesAsync(int orgId);
        Task<IEnumerable<ApplicationRole>> GetGlobalRoles();

        Task AddClaimAsync(ApplicationRole role, string claim);

    }
}
