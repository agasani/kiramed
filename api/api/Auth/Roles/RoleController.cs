﻿using api.Auth.Roles;
using api.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api.Auth;
using System.Security.Claims;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System.Net;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace api.Controllers
{
    [Route("api/[controller]")]
    public class RoleController : Controller
    {
        private readonly IRoleRepository _roleRepo;

        public RoleController(UserManager<ApplicationUser> userManager, IRoleRepository roleRepo)
        {
            _roleRepo = roleRepo;
        }

        /// <summary>Create a new role</summary>
        /// <returns>returns RoleViewModel object on success and Feedback object on Failor</returns>
        /// <param name="model">The RoleViewModel object to be created.</param>
        /// <response code="400">Bad request</response>
        /// <response code="200">Successfully created</response>
        [HttpPost]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, typeof(FeedBack))]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(FeedBack))]
        public async Task<IActionResult> Post([FromBody]RoleViewModel model)
        {
            var feedback = new FeedBack();

            Console.WriteLine("Model: " + JsonConvert.SerializeObject(model));
            if (!ModelState.IsValid)
            {
                feedback.err = true;
                feedback.message = "Invalid Model: " + string.Join(", ", ModelState.Values.Where(e => e.Errors.Count > 0)
                    .SelectMany(e => e.Errors).Select(e => e.ErrorMessage).ToArray());
                return BadRequest(feedback);
            }
            //check role type
           
            var roleTypes = _roleRepo.GetRoleTypes();
            if(!roleTypes.Contains(model.Type))
            {
                feedback.err = true;
                feedback.message = "The role type does not exists";
                return BadRequest(feedback);
            }

            var newRole = new ApplicationRole
            {
                Name = model.Name,
                Description = model.Description,
                CreatedDate = DateTime.Now,
                ModifiedDate = DateTime.Now,
                OrganizationId = model.OrganizationId,
                Type = model.Type            
            };

            // Add claims/actions to newRole
            foreach(var claimValue in model.Claims) {
                var roleClaim = new IdentityRoleClaim<string>();
                roleClaim.ClaimType = ClaimTypes.AuthorizationDecision;
                roleClaim.ClaimValue = claimValue;
                newRole.Claims.Add(roleClaim);
            }

            //check if role exists
            var role = await _roleRepo.FindByNameAsync(model.Name);
            if (role != null)
            {
                feedback.err = true;
                feedback.message = "The role with same name already exists";
                feedback.data = JsonConvert.SerializeObject(new Dictionary<string, string> { { "RoleId", role.Id } });
                return BadRequest(feedback);
            }

            var result = await _roleRepo.CreateAsync(newRole);
            if (!result.Succeeded)
            {
                feedback.err = true;
                feedback.message = "The role cannot be created at this time. Country or organization might not exist!";
                return BadRequest(feedback);
            }

            feedback.err = false;
            feedback.message = "The role was successfully created.";
            feedback.data = JsonConvert.SerializeObject(new Dictionary<string, string> { { "RoleId", newRole.Id } });
            return Ok(feedback);
        }

        /// <summary>Update role by unique (string) roleId</summary>
        /// <param name="roleId">The roleId of Object to be updated.</param>
        /// <param name="model">The updated RoleViewModel object.</param>
        /// <returns>returns Feedback object on success and Feedback object on Failor</returns>
        /// <response code="400">Bad request</response>
        /// <response code="200">Successfully updated</response>
        [HttpPut("{roleId}")]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, typeof(FeedBack))]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(FeedBack))] 
        public async Task<IActionResult> Put([FromRoute]string roleId, [FromBody]RoleViewModel model)
        {
            var feedback = new FeedBack();

            if (!ModelState.IsValid)
            {
                feedback.err = true;
                feedback.message = "Invalid Model: " + string.Join(", ", ModelState.Values.Where(e => e.Errors.Count > 0)
                    .SelectMany(e => e.Errors).Select(e => e.ErrorMessage).ToArray());
                return BadRequest(feedback);
            }

            //check if role exists
            var role = await _roleRepo.FindByIdAsync(roleId);
            if (role == null)
            {
                feedback.err = true;
                feedback.message = "The role does not exists";
                return BadRequest(feedback);
            }

            role.Name = model.Name;
            role.Description = model.Description;
            role.ModifiedDate = DateTime.Now;

            var result = await _roleRepo.UpdateAsync(role);
            if (!result.Succeeded)
            {
                feedback.err = true;
                feedback.message = "The role cannot be updated at this time";
                return BadRequest(feedback);
            }

            feedback.err = false;
            feedback.message = "The role was successfully updated.";
            feedback.data = JsonConvert.SerializeObject(new Dictionary<string, string> { { "RoleId", role.Id } });
            return Ok(feedback);
        }

        /// <summary>Delete role by unique (string) roleId</summary>
        /// <param name="roleId">The roleId of Object to be deleted.</param>
        /// <returns>returns Feedback object on success and Feedback object on Failor</returns>
        /// <response code="400">Bad request</response>
        /// <response code="200">Successfully deleted</response>
        [HttpDelete("{roleId}")]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, typeof(FeedBack))]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(FeedBack))]
        public async Task<IActionResult> Delete([FromRoute]string roleId)
        {
            var feedback = new FeedBack();

            if (!ModelState.IsValid)
            {
                feedback.err = true;
                feedback.message = "Invalid Model: " + string.Join(", ", ModelState.Values.Where(e => e.Errors.Count > 0)
                    .SelectMany(e => e.Errors).Select(e => e.ErrorMessage).ToArray());
                return BadRequest(feedback);
            }

            //check if role exists
            var role = await _roleRepo.FindByIdAsync(roleId);
            if (role == null)
            {
                feedback.err = true;
                feedback.message = "The role does not exists";
                return BadRequest(feedback);
            }


            var result = await _roleRepo.DeleteAsync(role);
            if (!result.Succeeded)
            {
                feedback.err = true;
                feedback.message = "The role cannot be deleted at this time";
                return BadRequest(feedback);
            }

            feedback.err = false;
            feedback.message = "The role was successfully deleted.";
            return Ok(feedback);
        }

        /// <summary>List organizations roles by unique (integer) orgId</summary>
        /// <param name="orgId">The orgId of parent Organization of objects to be listed.</param>
        /// <returns>returns an array of RoleViewModel objects on success and Feedback object on Failor</returns>
        /// <response code="400">Bad request</response>
        /// <response code="200">Successfully deleted</response>
        [HttpGet("GetOrgRoles/{orgId}")]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, typeof(FeedBack))]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(IEnumerable<RoleViewModel>))]
        public async Task<IActionResult> GetOrgRoles([FromRoute]int orgId)
        {
            var feedback = new FeedBack();


            if (!ModelState.IsValid)
            {
                feedback.err = true;
                feedback.message = "Invalid Model: " + string.Join(", ", ModelState.Values.Where(e => e.Errors.Count > 0)
                                       .SelectMany(e => e.Errors).Select(e => e.ErrorMessage).ToArray());
                return BadRequest(feedback);
            }

            var roles = await _roleRepo.GetOrganizationRolesAsync(orgId);
            var applicationRoles = roles as ApplicationRole[] ?? roles.ToArray();
            if (!applicationRoles.Any())
            {
                feedback.err = true;
                feedback.message = $@"No roles were found for organization Id: {orgId} ";
                return BadRequest(feedback);
            }

            var returnRoles = applicationRoles.Select(role => new RoleViewModel(role))
                .ToList();

            return Ok(returnRoles);
        }

        ///// <summary>Add actions to an existing role</summary>
        ///// <response code="400">Bad request</response>
        ///// <response code="200">Actions added to the role</response>
        //[HttpPost("AddActions")]
        //[Consumes("application/x-www-form-urlencoded")]
        //[SwaggerResponse((int)HttpStatusCode.BadRequest, typeof(FeedBack))]
        //[SwaggerResponse((int)HttpStatusCode.OK, typeof(FeedBack))]
        //public async Task<IActionResult> AddActions([FromForm]string roleId, [FromBody]RoleActionViewModel model)
        //{
        //    var feedback = new FeedBack();

        //    if (!ModelState.IsValid)
        //    {
        //        feedback.err = true;
        //        feedback.message = "Invalid Model: " + string.Join(", ", ModelState.Values.Where(e => e.Errors.Count > 0)
        //                               .SelectMany(e => e.Errors).Select(e => e.ErrorMessage).ToArray());
        //        return BadRequest(feedback);
        //    }

        //    // check if role exists
        //    var role = await _roleRepo.FindByIdAsync(roleId);
        //    if (role == null)
        //    {
        //        feedback.err = true;
        //        feedback.message = "The role does not exists";
        //        return BadRequest(feedback);
        //    }
        //    if (model.Actions.Count == 0)
        //    {
        //        feedback.err = true;
        //        feedback.message = "No action was specified";
        //        return BadRequest(feedback);
        //    }

        //    var result = await _roleRepo.AddActionsAsync(role, model.Actions);
        //    if (!result.Succeeded)
        //    {
        //        feedback.err = true;
        //        feedback.message = "Cannot add actions at this time";
        //        return BadRequest(feedback);
        //    }

        //    feedback.err = false;
        //    feedback.message = "Successfully added actions";
        //    return Ok(feedback);
        //}

        ///// <summary>Remove actions from an existing role</summary>
        ///// <response code="400">Bad request</response>
        ///// <response code="200">Actions removed to the role</response>
        //[HttpPost("RemoveActions")]
        //[Consumes("application/x-www-form-urlencoded")]
        //[SwaggerResponse((int)HttpStatusCode.BadRequest, typeof(FeedBack))]
        //[SwaggerResponse((int)HttpStatusCode.OK, typeof(FeedBack))]
        //public async Task<IActionResult> RemoveActions([FromForm]string roleId, [FromBody]RoleActionViewModel model)
        //{
        //    var feedback = new FeedBack();

        //    if (!ModelState.IsValid)
        //    {
        //        feedback.err = true;
        //        feedback.message = "Invalid Model: " + string.Join(", ", ModelState.Values.Where(e => e.Errors.Count > 0)
        //                               .SelectMany(e => e.Errors).Select(e => e.ErrorMessage).ToArray());
        //        return BadRequest(feedback);
        //    }

        //    //check if role exists
        //    var role = await _roleRepo.FindByIdAsync(roleId);
        //    if (role == null)
        //    {
        //        feedback.err = true;
        //        feedback.message = "The role does not exists";
        //        return BadRequest(feedback);
        //    }
        //    if (model.Actions.Count == 0)
        //    {
        //        feedback.err = true;
        //        feedback.message = "No action was specified";
        //        return BadRequest(feedback);
        //    }

        //    var result = await _roleRepo.RemoveActionsAsync(role, model.Actions);
        //    if (!result.Succeeded)
        //    {
        //        feedback.err = true;
        //        feedback.message = "Cannot remove actions at this time";
        //        return BadRequest(feedback);
        //    }

        //    feedback.err = false;
        //    feedback.message = "Successfully removed actions";
        //    return Ok(feedback);
        //}

        /// <summary>Retrieve role by unique (string) roleId</summary>
        /// <param name="roleId">The roleId of Object to be retrieved.</param>
        /// <returns>returns RoleViewModel object on success and Feedback object on Failor</returns>
        /// <response code="400">Bad request</response>
        /// <response code="200">Successfully retrieved</response>
        [HttpGet("Details/{roleId}")]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, typeof(FeedBack))]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(RoleViewModel))]
        public async Task<IActionResult> Details([FromRoute]string roleId)
        {
            var feedback = new FeedBack();

            if (!ModelState.IsValid)
            {
                feedback.err = true;
                feedback.message = "Invalid Model: " + string.Join(", ", ModelState.Values.Where(e => e.Errors.Count > 0)
                                       .SelectMany(e => e.Errors).Select(e => e.ErrorMessage).ToArray());
                return BadRequest(feedback);
            }
          
            var role = await _roleRepo.FindByIdAsync(roleId);
            if (role != null) return Ok(new RoleViewModel(role));
            feedback.err = true;
            feedback.message = "The role does not exists";
            return BadRequest(feedback);
        }

        /// <summary>List all roles</summary>
        /// <returns>returns array of RoleViewModel objects on success and Feedback object on Failor</returns>
        /// <response code="400">Bad request</response>
        /// <response code="200">Successfully listed</response>
        [HttpGet]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, typeof(FeedBack))]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(IEnumerable<RoleViewModel>))]
        public async Task<IActionResult> Get()
        {
            var feedback = new FeedBack();


            if (!ModelState.IsValid)
            {
                feedback.err = true;
                feedback.message = "Invalid Model: " + string.Join(", ", ModelState.Values.Where(e => e.Errors.Count > 0)
                                       .SelectMany(e => e.Errors).Select(e => e.ErrorMessage).ToArray());
                return BadRequest(feedback);
            }

           
            var roles =  await _roleRepo.Roles();
            var applicationRoles = roles as ApplicationRole[] ?? roles.ToArray();
            if (!applicationRoles.Any())
            {
                feedback.err = true;
                feedback.message = "No roles were found";
                return BadRequest(feedback);
            }

            var returnRoles = applicationRoles.Select(role => new RoleViewModel(role))
                .ToList();

            return Ok(returnRoles);
        }

        /// <summary>List country roles by unique (integer) countryId</summary>
        /// <param name="countryId">The countryId of parent Country of objects to be listed.</param>
        /// <returns>returns array of RoleViewModel objects on success and Feedback object on Failor</returns>
        /// <response code="400">Bad request</response>
        /// <response code="200">Successfully listed</response>
        [HttpGet("CountryRoles/{countryId}")]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, typeof(FeedBack))]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(IEnumerable<RoleViewModel>))]
        public async Task<IActionResult> GetCountryRoles([FromRoute]int countryId)
        {
            var feedback = new FeedBack();


            if (!ModelState.IsValid)
            {
                feedback.err = true;
                feedback.message = "Invalid Model: " + string.Join(", ", ModelState.Values.Where(e => e.Errors.Count > 0)
                                       .SelectMany(e => e.Errors).Select(e => e.ErrorMessage).ToArray());
                return BadRequest(feedback);
            }

            var roles = await _roleRepo.GetCountryRoles(countryId);
            var applicationRoles = roles as ApplicationRole[] ?? roles.ToArray();
            if (!applicationRoles.Any())
            {
                feedback.err = true;
                feedback.message = $@"No roles were found for country Id: {countryId} ";
                return BadRequest(feedback);
            }

            var returnRoles = applicationRoles.Select(role => new RoleViewModel(role))
                .ToList();

            return Ok(returnRoles);
        }

        /// <summary>List global roles</summary>
        /// <returns>returns array of RoleViewModel objects on success and Feedback object on Failor</returns>
        /// <response code="400">Bad request</response>
        /// <response code="200">Successfully listed</response>
        [HttpGet("GlobalRoles")]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, typeof(FeedBack))]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(IEnumerable<RoleViewModel>))]
        public async Task<IActionResult> GetSystemRoles()
        {
            var feedback = new FeedBack();
            if (!ModelState.IsValid)
            {
                feedback.err = true;
                feedback.message = "Invalid Model: " + string.Join(", ", ModelState.Values.Where(e => e.Errors.Count > 0)
                                       .SelectMany(e => e.Errors).Select(e => e.ErrorMessage).ToArray());
                return BadRequest(feedback);
            }

            var roles = await _roleRepo.GetGlobalRoles();
            var applicationRoles = roles as ApplicationRole[] ?? roles.ToArray();
            if (!applicationRoles.Any())
            {
                feedback.err = true;
                feedback.message = "No global roles were found";
                return BadRequest(feedback);
            }

            var returnRoles = applicationRoles.Select(role => new RoleViewModel(role))
                .ToList();

            return Ok(returnRoles);
        }
    }
}