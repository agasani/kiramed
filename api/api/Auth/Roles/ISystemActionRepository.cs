﻿using System.Collections.Generic;
using api.Models;

namespace api.Auth.Roles
{
    public interface ISystemActionRepository : IKiraRepository<SystemAction>
    {
        SystemAction FindByName(string name);
        List<SystemAction> GetActions(IList<string> actionNames);
    }
}
