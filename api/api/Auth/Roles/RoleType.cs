﻿using System.ComponentModel;

namespace api.Auth.Roles
{
    public enum eRoleType
    {
        [Description("Country")]
        Country = 1,
        [Description("Organization")]
        Organization = 2,
        [Description("Global")]
        Global = 3
    }
}
