﻿using System.Collections.Generic;
using System.Linq;

namespace api.Auth.Roles
{
    public class RoleActionViewModel
    {
        public List<string> Actions { get; set; }

        public RoleActionViewModel(IEnumerable<string> actions)
        {
            Actions = actions.ToList();
        }

        public RoleActionViewModel() { }
    }
}
