﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api.Auth;
using api.Auth.Roles;
using api.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Net;

namespace api.Controllers
{
    [Route("api/[controller]")]
    public class SystemActionController : Controller
    {
        public readonly ISystemActionRepository _actionRepository;

        public SystemActionController(ISystemActionRepository actionRepository)
        {
            _actionRepository = actionRepository;
        }

        /// <summary>Register a new system action</summary>
        /// <response code="400">Bad request</response>
        /// <response code="200">Created successfully</response>
        [HttpPost]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, typeof(FeedBack))]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(FeedBack))]
        public async Task<IActionResult> Post([FromBody]ActionViewModel model)
        {
            var feedback = new FeedBack();

            if (!ModelState.IsValid)
            {
                feedback.err = true;
                feedback.message = "Invalid Model: " + string.Join(", ", ModelState.Values.Where(e => e.Errors.Count > 0)
                                       .SelectMany(e => e.Errors).Select(e => e.ErrorMessage).ToArray());
                return BadRequest(feedback);
            }

            var newAction = new SystemAction(model);
           
            //check if action exisit
            var action = _actionRepository.FindByName(newAction.ActionName);
            if (action != null)
            {
                feedback.err = true;
                feedback.message = "The action already exists";
                feedback.data = JsonConvert.SerializeObject(new Dictionary<string, string> { { "actionname", action.ActionName } });
                return BadRequest(feedback);
            }

            var result = await _actionRepository.CreateAsync(newAction);
            if (result == 0)
            {
                feedback.err = true;
                feedback.message = "The action cannot be created at this time";
                return BadRequest(feedback);
            }

            feedback.err = false;
            feedback.message = "The action was successfully created.";
            feedback.data = JsonConvert.SerializeObject(new Dictionary<string, string> { { "actionname", model.ActionName } });
            return Ok(feedback);
        }

        //[HttpPost("{roleid}")]
        //public async Task<IActionResult> Put(string roleId, [FromBody] RoleViewModel model)
        //{
        //    var feedback = new FeedBack();

        //    if (!ModelState.IsValid)
        //    {
        //        feedback.err = true;
        //        feedback.message = "Invalid Model: " + string.Join(", ", ModelState.Values.Where(e => e.Errors.Count > 0)
        //                               .SelectMany(e => e.Errors).Select(e => e.ErrorMessage).ToArray());
        //        return BadRequest(feedback);
        //    }

        //    //check if role exisit
        //    var role = await _roleRepo.FindByIdAsync(roleId);
        //    if (role == null)
        //    {
        //        feedback.err = true;
        //        feedback.message = "The role does not exists";
        //        return BadRequest(feedback);
        //    }

        //    role.Name = model.Name;
        //    role.Description = model.Description;
        //    role.ModifiedDate = DateTime.Now;

        //    var result = await _roleRepo.UpdateAsync(role);
        //    if (!result.Succeeded)
        //    {
        //        feedback.err = true;
        //        feedback.message = "The role cannot be updated at this time";
        //        return BadRequest(feedback);
        //    }

        //    feedback.err = false;
        //    feedback.message = "The role was successfully updated.";
        //    feedback.data = JsonConvert.SerializeObject(new Dictionary<string, string> { { "RoleId", role.Id } });
        //    return Ok(feedback);
        //}

        /// <summary>Delete a system action by unique (string) actionName</summary>
        /// <response code="400">Bad request</response>
        /// <response code="200">Deleted successfully</response>
        [HttpDelete("{actionName}")]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, typeof(FeedBack))]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(FeedBack))]
        public async Task<IActionResult> Delete([FromRoute]string actionName)
        {
            var feedback = new FeedBack();
            var action =  _actionRepository.FindByName(actionName);

            if (action == null)
            {
                feedback.err = true;
                feedback.message = "Deleted!";
                return BadRequest(feedback);
            }
         
            _actionRepository.Delete(action);
            feedback.err = false;
            feedback.message = "Action Successfully deleted";
            feedback.data = JsonConvert.SerializeObject(new Dictionary<string, string> { { "actionname", actionName } });

            return Ok(feedback);
        }
        
        [HttpGet]
        public  async Task<List<ActionViewModel>> Get()
        {
            var actions = await _actionRepository.GetAllAsync();
            var systemActions = actions as SystemAction[] ?? actions.ToArray();
            return !systemActions.Any() ? null : systemActions.Select(action => new ActionViewModel(action)).OrderBy(a => a.ActionName).ToList();
        }

        //[HttpGet("roleid")]
        //public async Task<IActionResult> Get(string roleId)
        //{
        //    var feedback = new FeedBack();
        //    var model = new RoleViewModel();

        //    var role = await _roleRepo.FindByIdAsync(roleId);
        //    if (role == null)
        //    {
        //        feedback.err = true;
        //        feedback.message = "The role does not exists";
        //        return BadRequest(feedback);
        //    }

        //    model.Name = role.Name;
        //    model.Description = role.Description;
        //    model.OrganizationId = role.OrganizationId;
        //    model.RoleId = role.Id;

        //    return Ok(model);
        //}

    }
}
