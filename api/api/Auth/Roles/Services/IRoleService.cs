﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace api.Auth.Roles
{
    public interface IRoleService
    {
        Task<IEnumerable<ApplicationRole>> GetRoles(int orgId);
        Task<ApplicationRole> FindByIdAsync(string roleId);
        Task<ApplicationRole> FindByNameAsync(string roleId);
        Task<IdentityResult> CreateAsync(ApplicationRole role);
        ApplicationRole FindById(string roleId);
        ApplicationRole FindByName(string roleId);
        Task<IdentityResult> Create(ApplicationRole role, IList<SystemAction> roleActions);
        IdentityResult Update(ApplicationRole role);
        Task<IdentityResult> UpdateAsync(ApplicationRole role);
        IdentityResult Delete(ApplicationRole role);
        Task<IdentityResult> DeleteAsync(ApplicationRole role);
    }
}
