﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace api.Auth.Roles.Services
{
    public class RoleService : IRoleService
    {
        private readonly IRoleRepository _roleRepository;

        public RoleService(IRoleRepository roleRepository)
        {
            _roleRepository = roleRepository;
        }

        public IdentityResult Create(ApplicationRole role)
        {
            throw new NotImplementedException();
        }

        public async Task<IdentityResult> Create(ApplicationRole role, IList<SystemAction> roleClaims)
        {
            throw new NotImplementedException();
        }

      
        public Task<IdentityResult> CreateAsync(ApplicationRole role)
        {
            throw new NotImplementedException();
        }

        public IdentityResult Delete(ApplicationRole role)
        {
            throw new NotImplementedException();
        }

        public Task<IdentityResult> DeleteAsync(ApplicationRole role)
        {
            throw new NotImplementedException();
        }

        public ApplicationRole FindById(string roleId)
        {
            throw new NotImplementedException();
        }

        public Task<ApplicationRole> FindByIdAsync(string roleId)
        {
            throw new NotImplementedException();
        }

        public ApplicationRole FindByName(string roleId)
        {
            throw new NotImplementedException();
        }

        public Task<ApplicationRole> FindByNameAsync(string roleId)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<ApplicationRole>> GetRoles(int orgId)
        {
            throw new NotImplementedException();
        }

        public IdentityResult Update(ApplicationRole role)
        {
            throw new NotImplementedException();
        }

        public Task<IdentityResult> UpdateAsync(ApplicationRole role)
        {
            throw new NotImplementedException();
        }
    }
}
