﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using api.Db;
using api.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace api.Auth.Roles
{
    public class SystemActionRepository : ISystemActionRepository
    {
        public readonly KiraContext _context;
        public SystemActionRepository( KiraContext context)
        {
            _context = context;
        }
         
        public int Create(SystemAction action)
        {
            _context.SystemActions.Add(action);
            _context.SaveChanges();

            return action.Id;
        }

        public async Task<int> CreateAsync(SystemAction action)
        {
            await _context.SystemActions.AddAsync(action);
            await _context.SaveChangesAsync();
            return action.Id;
        }

        public void Delete(SystemAction action)
        {
            _context.SystemActions.Remove(action);
            _context.SaveChanges();
        }

        public SystemAction Exists(SystemAction action)
        {
            throw new NotImplementedException();
        }

        public async Task<int> UpdateAsync(SystemAction action)
        {
           await  _context.SaveChangesAsync();
            return action.Id;
        }

        public Task<SystemAction> ExistsAsync(SystemAction action)
        {
            throw new NotImplementedException();
        }

        public SystemAction Find(int id)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<SystemAction>> Get()
        {
            throw new NotImplementedException();
        }

        public Task<SystemAction> FindAsync(int id)
        {
            throw new NotImplementedException();
        }
        public SystemAction FindByName(string name)
        {
            return  _context.SystemActions.FirstOrDefault(a => a.ActionName == name);
        }

        public List<SystemAction> GetActions(IList<string> actionNames)
        {
            return actionNames.Select(FindByName).ToList();
        }

        public IEnumerable<SystemAction> GetAll(int countryId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<SystemAction> GetAll()
        {
            return _context.SystemActions.ToList();
        }

        public async Task<IEnumerable<SystemAction>> GetAllAsync()
        {
            return await  _context.SystemActions.ToListAsync();
        }

        public int Update(SystemAction action)
        {
            _context.SaveChanges();
            return action.Id;
        }

        public IEnumerable<SystemAction> Get(Expression<Func<SystemAction, bool>> expression)
        {
            return _context.SystemActions.Where(expression);
        }

        public async Task<IEnumerable<SystemAction>> GetAsync(Expression<Func<SystemAction, bool>> expression)
        {
            return await _context.SystemActions.Where(expression).ToListAsync();
        }
    }
}
