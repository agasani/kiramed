﻿using System;

namespace api.Auth.Roles
{
    public class ActionViewModel
    {
        public int Id { get; set; }
        public int ClaimNumber { get; set; }
        public string ActionName { get; set; }
        public string ControllerName { get; set; }
        public DateTime DateTimeCreated { get; set; }
    

        public ActionViewModel(SystemAction action)
        {
            ClaimNumber = action.ClaimNumber;
            ActionName = action.ActionName;
            ControllerName = action.ControllerName;
            DateTimeCreated = action.DateTimeCreated;
        }

        public ActionViewModel() { }
    }


}