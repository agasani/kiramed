﻿using api.Auth.Roles;
using api.Db;
using api.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Castle.DynamicProxy.Generators.Emitters.SimpleAST;
using Microsoft.EntityFrameworkCore;

namespace api.Auth
{
    public class RoleRepository : IRoleRepository 
    {
        public readonly RoleManager<ApplicationRole> _roleManager;
        public readonly KiraContext _context;
        public readonly ISystemActionRepository _actionRepo;
 
        public RoleRepository(RoleManager<ApplicationRole> roleManager, KiraContext kiraContext, ISystemActionRepository actionRepository = null)
        {
            //TODO pass in the organization and UserOrg services instead of the entire context
            _roleManager = roleManager;
            _context = kiraContext;
            _actionRepo = actionRepository;
        }

        public async  Task<IList<ApplicationRole>> Roles(int orgId)
        {
            var org = await _context.Organizations.FindAsync(orgId);
            return org == null ? null : _roleManager.Roles.Where(r => r.OrganizationId == -1 && r.OrganizationId == orgId && r.Type == (short)eRoleType.Organization || r.OrganizationId == org.CountryId && r.Type == (short)eRoleType.Country).ToList();
        }

        public async Task<IList<ApplicationRole>> Roles()
        {
           
            var roles = _roleManager.Roles.ToList();
            
            return roles;
        }

        public async Task<ApplicationRole> FindByIdAsync(string roleId)
        {     
            var role = await _roleManager.FindByIdAsync(roleId);
            return role;
        }

        public async Task<ApplicationRole> FindByNameAsync(string roleId)
        {
            var role = await _roleManager.FindByNameAsync(roleId);
            //_roleManager.AddClaimAsync(role)
            return role;
        }

        public async Task<IdentityResult> AddActionsAsync(ApplicationRole newRole, IList<string> systemActions)
        {    
            foreach (var action in systemActions)
            {
                var addAction = await AddActionAsync(newRole, action);
                if(!addAction.Succeeded) return IdentityResult.Failed();
            }       
            return IdentityResult.Success;
        }

        public ApplicationRole FindById(string roleId)
        {
            var role =  _roleManager.FindByIdAsync(roleId).Result;
            return role;
        }

        public  ApplicationRole FindByName(string roleId)
        {
            var role = _roleManager.FindByNameAsync(roleId).Result;
            return role;
        }

        public IdentityResult Create(ApplicationRole newRole)
        {
            var org = _context.Organizations.FirstOrDefault(o => o.Id == newRole.OrganizationId);
            return org == null ? IdentityResult.Failed() : _roleManager.CreateAsync(newRole).Result;
        }
        public IdentityResult Delete(ApplicationRole newRole)
        {
            return _roleManager.DeleteAsync(newRole).Result;
        }

        public async Task<IdentityResult> DeleteAsync(ApplicationRole newRole)
        {
            return await _roleManager.DeleteAsync(newRole);
        }

        public async Task<IdentityResult> UpdateAsync(ApplicationRole newRole)
        {
            return await _roleManager.UpdateAsync(newRole);
        }
        public IdentityResult Update(ApplicationRole newRole)
        {
            return _roleManager.UpdateAsync(newRole).Result;
        }

        public async Task<IdentityResult> CreateAsync(ApplicationRole role)
        {
            if (role.Type == (short) eRoleType.Country)
            {
                var country = await _context.Countries.FirstOrDefaultAsync(co => co.Id == role.OrganizationId && co.IsDeleted > DateTime.Now);
                if (country == null)
                {
                    return IdentityResult.Failed();
                }
            }

            if (role.Type == (short)eRoleType.Organization)
            {
                var org = await _context.Organizations.FirstOrDefaultAsync(o => o.Id == role.OrganizationId && o.IsDeleted > DateTime.Now);
                if (org == null)
                {
                    return IdentityResult.Failed();
                }
            }
          
            return await _roleManager.CreateAsync(role);
        }

        public async Task<IdentityResult> AddActionAsync(ApplicationRole role, string roleAction)
        {
            var systemAction = _actionRepo.FindByName(roleAction);
            if (systemAction == null) return IdentityResult.Failed();
            //check if it has the claim already
            var newClaim = new Claim(ClaimTypes.AuthorizationDecision, systemAction.ActionName);
            if (await HasClaim(role, roleAction)) return IdentityResult.Failed();
            var claimResult = await _roleManager.AddClaimAsync(role, newClaim);
            return !claimResult.Succeeded ? IdentityResult.Failed() : IdentityResult.Success;
        }

        public async Task<IdentityResult> RemoveActionAsync(ApplicationRole role, string roleAction)
        {
            var systemAction = _actionRepo.FindByName(roleAction);
            if (systemAction == null) return IdentityResult.Failed(); ;
            //check if it has claim already
            if (!await HasClaim(role, roleAction)) return IdentityResult.Failed();
            var claimRole = await GetRoleClaim(role, roleAction);
            if(claimRole == null) return IdentityResult.Failed();
            var claimResult = await _roleManager.RemoveClaimAsync(role, claimRole);
            return !claimResult.Succeeded ? IdentityResult.Failed() : IdentityResult.Success;
        }

        public async Task<bool> HasClaim(ApplicationRole role, string claimAction)
        {
            var roleClaims = await _roleManager.GetClaimsAsync(role);
            return roleClaims.Select(c => c.Value).ToList().Contains(claimAction);        
        }

        public async Task<IdentityResult> RemoveActionsAsync(ApplicationRole role, IList<string> roleActions)
        {
            foreach (var action in roleActions)
            {
                var addAction = await RemoveActionAsync(role, action);
                if (!addAction.Succeeded) return IdentityResult.Failed();
            }
            return IdentityResult.Success;
        }

        public async Task<Claim> GetRoleClaim(ApplicationRole role, string action)
        {
            var claims = await _roleManager.GetClaimsAsync(role);
            return claims.FirstOrDefault(a => a.Value == action);
        }

        public List<short> GetRoleTypes()
        { 
            return new List<short>() {(short)eRoleType.Organization, (short)eRoleType.Country, (short)eRoleType.Global};
        }

        public async Task<IEnumerable<ApplicationRole>> GetCountryRoles(int countryId)
        {
            return  await GetAsync(r => r.Type == 3 || (r.Type == 1 && r.OrganizationId == countryId));
        }

        public async Task<IEnumerable<ApplicationRole>> GetOrganizationRolesAsync(int orgId)
        {
          var organization =  await _context.Organizations.FindAsync(orgId);

          return  await GetAsync(r => r.Type == 3 ||  (r.Type == 1 && r.OrganizationId == organization.CountryId) || (r.Type == 2 && r.OrganizationId == orgId));
        }

        public async Task<IEnumerable<ApplicationRole>> GetGlobalRoles()
        {
            return await GetAsync(r => r.Type == 3);
        }

        public async Task<IEnumerable<ApplicationRole>> GetAsync(Expression<Func<ApplicationRole, bool>> expression)
        {
            return await _roleManager.Roles.Where(expression).ToListAsync();
        }
        public async Task AddClaimAsync(ApplicationRole role, string claim){
            
            await _roleManager.AddClaimAsync(role, new Claim(ClaimTypes.AuthorizationDecision, claim));
        }
    }
}
