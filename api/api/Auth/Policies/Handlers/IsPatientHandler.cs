using System;
using api.Auth.Policies.Requirements;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using System.Threading.Tasks;
using api.Models;

namespace api.Auth.Policies.Handlers
{
    public class IsPatientHandler: AuthorizationHandler<AccessPatientInfoRequirement, User>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, AccessPatientInfoRequirement requirement, User user) 
        {
            // TODO add user.Id == context.userId OR user.NationalId = context.NationalId, the userId needs to be included to current user context
            if(context.User.HasClaim(c => c.Type == ClaimTypes.AuthorizationDecision && context.User.Identity.IsAuthenticated ))
            {
                context.Succeed(requirement);
            }

            return Task.CompletedTask;
        }
    }
}