using System;
using Microsoft.AspNetCore.Authorization;

namespace api.Auth.Policies.Requirements
{
    public class AccessPatientInfoRequirement: IAuthorizationRequirement { }
    // Requirement for a patient to see to their own information
    public class IsPatientRequirement: AccessPatientInfoRequirement
    {
        public int Id { get; private set; }

        public IsPatientRequirement(int id) {
            this.Id = id;
        }
    }
}