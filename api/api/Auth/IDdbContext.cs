﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api.Auth
{
    public class IDdbContext: IdentityDbContext<ApplicationUser, ApplicationRole, string>
    {
        public IDdbContext(DbContextOptions<IDdbContext> options): base(options)
        {

        }
    }
}
