﻿using System;
using api.Models;

namespace api.Utils
{
    public static class DateTimeUtils
    {
        public static AgeDTO GetAge(eAgeUnits unit, DateTime dob)
        {
            int age;
            switch (unit)
            {
                case eAgeUnits.Days:
                    age = GetDaysDifference(dob);
                    break;
                case eAgeUnits.Weeks:
                    age = GetWeeksDifference(dob);
                    break;
                case eAgeUnits.Months:
                    age = GetMonthDifference(DateTime.Now, dob);
                    break;
                case eAgeUnits.Years:
                    age = GetYearDifference(dob);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(unit), unit, null);
            }

            return new AgeDTO {Age = age, AgeUnit = unit};
        }


        public static AgeDTO GetAge(DateTime dob)
        {
            int age;
            age = GetYearDifference(dob);

            if (age > 1)
            {
                return new AgeDTO {Age = age, AgeUnit = eAgeUnits.Years};
            }

            age = GetMonthDifference(DateTime.Now, dob);

            if (age < 24 && age > 1)
            {
                return new AgeDTO {Age = age, AgeUnit = eAgeUnits.Months};
            }

            return new AgeDTO {Age = GetDaysDifference(dob), AgeUnit = eAgeUnits.Days};
        }

        public static int GetMonthDifference(DateTime startDate, DateTime endDate)
        {
            var monthsApart = 12 * (startDate.Year - endDate.Year) + startDate.Month - endDate.Month;
            return Math.Abs(monthsApart);
        }

        public static int GetYearDifference(DateTime date)
        {
            var now = DateTime.Today;
            var age = now.Year - date.Year;
            if (date > now.AddYears(-age)) age--;

            return age;
        }

        public static int GetDaysDifference(DateTime date)
        {
            return  (DateTime.Now - date).Days;
        }

        public static int GetWeeksDifference(DateTime date)
        {
            return (int) (DateTime.Now - date).TotalDays / 7;
        }
    }


    //helpers

    public class AgeDTO
    {
        public int Age { get; set; }
        public eAgeUnits AgeUnit { get; set; }
    }


}
