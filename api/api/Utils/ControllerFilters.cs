﻿
using System.Linq;
using System.Threading.Tasks;
using api.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace api.Utils
{
    public class ControllerFilters
    {
        public class ValidateModelAttribute : ActionFilterAttribute
        {
            public override void OnActionExecuting(ActionExecutingContext context)
            {
                if (!context.ModelState.IsValid)
                {
                    context.Result = new BadRequestObjectResult(context.ModelState);
                }
            }
        }

        // //public class ValidateEntityExistsAttribute : TypeFilterAttribute
        // //{
        // //    public ValidateAuthorExistsAttribute() : base(typeof(ValidateEntityExistsFilterImpl))
        // //    {

        // //    }

        // //    private class ValidateEntityExistsFilterImpl<T> : IAsyncActionFilter where T : KiraEntity
        // //    {
        // //        private readonly IKiraRepository<T> _kiraRepository;

        // //        public ValidateEntityExistsFilterImpl(IKiraRepository<T> kiraRepository)
        // //        {
        // //            _kiraRepository = kiraRepository;
        // //        }
        // //        public async Task OnActionExecutionAsync(ActionExecutingContext context,
        // //            ActionExecutionDelegate next)
        // //        {
        // //            if (context.ActionArguments.ContainsKey("id"))
        // //            {
        // //                var id = context.ActionArguments["id"] as int?;
        // //                if (id.HasValue)
        // //                {
        // //                    if ((await _kiraRepository.GetAllAsync()).All(a => a.Id != id.Value))
        // //                    {
        // //                        context.Result = new NotFoundObjectResult(id.Value);
        // //                        return;
        // //                    }
        // //                }
        // //            }
        // //            await next();
        // //        }
        // //    }
        // //}
    }
}
