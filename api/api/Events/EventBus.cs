using System;
using System.Collections.Generic;

namespace api.Events
{
    public static class EventBus 
    {
        public static Dictionary<string, List<IEventHandler>> topics = new Dictionary<string, List<IEventHandler>>();
        public static  void subscribe(string topic, IEventHandler handler)
        {
            Console.WriteLine("Subscribing to: " + topic);
            if(topics.ContainsKey(topic))
            {
                handler.Event = topic;
                topics[topic].Add(handler);
            }
            else
            {
                topics[topic] = new List<IEventHandler>() { handler };
            }
        }
        public static void unsubscribe(string topic, IEventHandler handler)
        {
            if(topics.ContainsKey(topic))
            {
                topics[topic].Remove(handler);
            }
        }
        public static void broadcast(string topic, string messagePayload)
        {
            foreach(var handler in topics[topic])
            {
                handler.Execute(messagePayload);
            }
        }
    }
}