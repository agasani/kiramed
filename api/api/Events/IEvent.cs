using System;

namespace api.Events
{
    public interface IEvent
    {
        string Name { get; }
        string PayloadJson { get; set; }
    }
}