using System;
using System.Collections.Generic;

namespace api.Models
{
    // This is the base event model eventhandler in general expect, you can create your own all just inherit from this
    public class EmailMessage: IBaseEventMessage
    {
        string Type { get; set; } = "Email";
        public string Firstname { get; set; }

        public string Secondname { get; set; }

        public string Middlename { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }
        public string From { get; set; }
        public List<string> To {get; set; }
        public string Subject { get; set; }

        public string Body {get; set; }

        public bool IsHtml { get; set; }

        // TODO add more common properties
    }
}