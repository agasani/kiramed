using System;
using System.Collections.Generic;

namespace api.Models
{
    // This is the base event model eventhandler in general expect, you can create your own all just inherit from this
    public class IBaseEventMessage: KiraEntity
    {
        public string Timestamp { get; set; } = DateTime.Now.ToString("yyyyMMddHHmmssffff");
        public string Name { get; set; }
        public string Event { get; set; }
        public string Payload { get; set; }
        public string Type { get; set; }
        public string CurrentUserId { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
    }
}