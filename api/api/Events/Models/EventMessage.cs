using System;
using System.Collections.Generic;

namespace api.Models
{
    // This is the base event model eventhandler in general expect, you can create your own all just inherit from this
    public class EventMessage: IBaseEventMessage
    {
        // Whether the action was a success or failed
        public string Status { get; set; }

        string Type { get; set; } = "GenericEvent";
    }
}