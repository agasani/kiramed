using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace api.Models
{
    [Table("Metrics")]
    public class MetricsEntity: IBaseEventMessage 
    {
        public string Type { get; set; } = "Metrics";
     }
}