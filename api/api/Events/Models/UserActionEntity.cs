using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace api.Models
{

    // This is the base event model eventhandler in general expect, you can create your own all just inherit from this
    [Table("UserActions")]
    public class UserActionEntity: IBaseEventMessage
    {
        public string SubjectIds { get; set; }
        // Whether the action was a success or failed
        public string Status { get; set; }
        public string Type { get; set; } = "UserAction";
    }
}