using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace api.Models
{
    [Table("Logs")]
    public class LogEntity: IBaseEventMessage
    {
        public string Type { get; set; } = "Log";
        
        //TODO add message attribute to LogEntity
    }
}