using System;

namespace api.Events
{
    enum MetricsEvents { 
        // Generic Metrics Event, to be used for metrics with no customer event handlers, whose purpose is solely logging
        // Use name attribute to differenciate actions
        Metrics,
        // User Metrics
        FailedLogin, PatientWithoutInsurance  }
}