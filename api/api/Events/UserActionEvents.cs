using System;

namespace api.Events
{
    enum UserActionEvents {
            // Generic UserAction Event, use this for a userAction that doesn't have a customer handler, that's intended for just logging. 
            // Use name attribute to differenciate actions
            UserAction,
            // User events 
            Loggin, Logout
            // More Events here
        
        }
}