using System;

namespace api.Events
{
    enum GenericEvents {
            // User events 
            UserCreated, UserDeleted, UserUpdated, UserReactivated 
            // More Events here
        
        }
}