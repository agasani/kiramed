using System;

namespace api.Events
{
    enum LogEvents { ErrorLog, FatalLog, WarningLog, InfoLog, DebugLog }
}