using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using api.Services;
using api.Db;
using api.Models;

namespace api.Events
{
    public class UserActionEventHandler: IEventHandler
    {
        public readonly KiraContext _context;
        public UserActionEventHandler(KiraContext context) 
        {
            this._context = context;
        }
        public string Name { get; set; }
        public string Event { get; set; }
        private List<string> Events = new List<string> { LogEvents.ErrorLog.ToString(), LogEvents.WarningLog.ToString(), 
                                        LogEvents.InfoLog.ToString(), LogEvents.DebugLog.ToString() };

        public async void Execute(string payload)
        {
            try {
                Console.WriteLine("UserActionEventHandler CALLED. The payload is:  " + payload);
                var action = JsonConvert.DeserializeObject<UserActionEntity>(payload); 

                await this._context.UserActions.AddAsync(action);

            } catch(Exception e) {
                Console.WriteLine("Failed to EXECUTE UserActionEventHandler for payload: " + payload);
            }
            
        }
        public List<string> GetEvents()
        {
            return Events;
        }
    }
}