using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using api.Services;
using api.Models;

namespace api.Events
{
    public class EmailerHandler: IEventHandler
    {
        public string Name { get; set; }
        public string Event { get; set; }
        IEmailService emailService;

        private List<string> Events = new List<string> { };
    
        public EmailerHandler(IEmailService emailer)
        {
            this.emailService = emailer;
            Console.WriteLine("In the constructor emailer is: " + this.emailService.ToString());
        }
        public async void Execute(string payload)
        {
            try {
                
                Console.WriteLine("The payload is: " + payload);
                
                var email = JsonConvert.DeserializeObject<EmailMessage>(payload);
                foreach(var to in email.To)
                {
                    await this.emailService.SendMailAsync(email.From, to, email.Subject, email.Body);
                }
            } catch(Exception e) {
                Console.WriteLine("Failed to EXECUTE EmailerHandler for this payload: " + payload);
            }
        }
        public List<string> GetEvents()
        {
            return Events;
        }
    }
}