using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using api.Services;

namespace api.Events
{
    public class InviteNewUserHandler: IEventHandler
    {
        public string Name { get; set; }
        public string Event { get; set; }
        IEmailService emailService;

        private List<string> Events = new List<string> { GenericEvents.UserCreated.ToString(), GenericEvents.UserReactivated.ToString() };

        public InviteNewUserHandler(IEmailService emailer)
        {
            this.emailService = emailer;
            Console.WriteLine("In the constructor emailer is: " + this.emailService.ToString());
        }
        public async void Execute(string payload)
        {
            try {
                Console.WriteLine("InviteNewUserhandler CALLED. The payload is:  " + payload);
            
                var message = JsonConvert.DeserializeObject<Dictionary<string, string>>(payload);
                var user = JsonConvert.DeserializeObject(message["user"]);
                var password = message["Password"];

                //TODO find out why this has been raising exceptions and uncomment it out..
                //await this.emailService.UserInvitationEmailAsync(message.Email, message.Password);  

            } catch(Exception e) {
                Console.WriteLine("Failed to EXECUTE InviteNewUserHandler for payload: " + payload);
            }
            
        }
        public List<string> GetEvents()
        {
            return Events;
        }
    }
}