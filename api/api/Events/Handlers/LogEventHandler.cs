using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using api.Services;
using api.Db;
using api.Models;

namespace api.Events
{
    public class LogEventHandler: IEventHandler
    {
        public readonly KiraContext _context;
        public LogEventHandler(KiraContext context) 
        {
            this._context = context;
        }
        public string Name { get; set; }
        public string Event { get; set; }
        private List<string> Events = Enum.GetValues(typeof(LogEvents)).OfType<string>().ToList();

        public async void Execute(string payload)
        {
            try {
                Console.WriteLine("LogEventHandler CALLED. The payload is:  " + payload);
                var log = JsonConvert.DeserializeObject<LogEntity>(payload); 

                await this._context.Logs.AddAsync(log);

            } catch(Exception e) {
                Console.WriteLine("Failed to EXECUTE LogEventHandler for payload: " + payload);
            }
            
        }
        public List<string> GetEvents()
        {
            return Events;
        }
    }
}