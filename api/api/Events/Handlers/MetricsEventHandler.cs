using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using api.Services;
using api.Db;
using api.Models;

namespace api.Events
{
    public class MetricsEventHandler: IEventHandler
    {
        public readonly KiraContext _context;
        public MetricsEventHandler(KiraContext context) 
        {
            this._context = context;
        }
        public string Name { get; set; }
        public string Event { get; set; }
        private List<string> Events = Enum.GetValues(typeof(MetricsEvents)).OfType<string>().ToList();

        public async void Execute(string payload)
        {
            try {
                Console.WriteLine("UserActionEventHandler CALLED. The payload is:  " + payload);
                var metric = JsonConvert.DeserializeObject<MetricsEntity>(payload); 
                
                await this._context.Metrics.AddAsync(metric);

            } catch(Exception e) {
                Console.WriteLine("Failed to EXECUTE UserActionEventHandler for payload: " + payload);
            }
            
        }
        public List<string> GetEvents()
        {
            return Events;
        }
    }
}