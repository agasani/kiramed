using System;
using System.Collections.Generic;

namespace api.Events
{
    public interface IEventHandler
    {
        string Event { get; set; }
        string Name { get; set; }
        List<string> GetEvents();
        void Execute(string payload);
    }
}