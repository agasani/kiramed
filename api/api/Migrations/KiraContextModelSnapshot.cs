﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using api.Db;
using api.Models;
using api.Enums;

namespace api.Migrations
{
    [DbContext(typeof(KiraContext))]
    partial class KiraContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.1")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("api.Auth.SystemAction", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ActionName");

                    b.Property<int>("ClaimNumber");

                    b.Property<string>("ControllerName");

                    b.Property<DateTime>("DateTimeCreated");

                    b.Property<DateTime>("IsDeleted");

                    b.HasKey("Id");

                    b.ToTable("SystemActions");
                });

            modelBuilder.Entity("api.Models.Admission", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Acuity")
                        .HasMaxLength(2);

                    b.Property<string>("AdmissionCondition")
                        .HasMaxLength(2);

                    b.Property<DateTime>("CaseOpenDate");

                    b.Property<string>("DischargeCondition")
                        .HasMaxLength(2);

                    b.Property<string>("DischargeDisposition")
                        .HasMaxLength(2);

                    b.Property<string>("DischargeReason")
                        .HasMaxLength(2);

                    b.Property<bool>("IsAdmission");

                    b.Property<bool>("IsAdmitted");

                    b.Property<string>("LevelOfCare")
                        .HasMaxLength(2);

                    b.Property<DateTime>("PlannedDischargeDate");

                    b.Property<string>("ReasonNotAdmitted")
                        .HasMaxLength(2);

                    b.Property<DateTime>("ReasonNotAdmittedDate");

                    b.HasKey("Id");

                    b.ToTable("Admission");
                });

            modelBuilder.Entity("api.Models.AdmissionDiagnosis", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AdmissionId");

                    b.Property<int>("DiagnosisSequence");

                    b.HasKey("Id");

                    b.HasIndex("AdmissionId");

                    b.ToTable("AdmissionDiagnosis");
                });

            modelBuilder.Entity("api.Models.AdmissionDiagnosisDetail", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AdmissionId");

                    b.Property<int>("DiagnosisSequence");

                    b.HasKey("Id");

                    b.HasIndex("AdmissionId");

                    b.ToTable("AdmissionDiagnosisDetail");
                });

            modelBuilder.Entity("api.Models.AdmissionFacility", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AdmissionId");

                    b.HasKey("Id");

                    b.HasIndex("AdmissionId");

                    b.ToTable("AdmissionFacility");
                });

            modelBuilder.Entity("api.Models.AgeRange", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("DateTimeCreated");

                    b.Property<DateTime>("IsDeleted");

                    b.Property<double>("MaximumAge");

                    b.Property<string>("MeasureType");

                    b.Property<double>("MinimumAge");

                    b.Property<string>("RangeName");

                    b.Property<int>("Unit");

                    b.HasKey("Id");

                    b.ToTable("AgeRanges");
                });

            modelBuilder.Entity("api.Models.AllergyReference", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.HasKey("Id");

                    b.ToTable("AllergyReference");
                });

            modelBuilder.Entity("api.Models.Appointment", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("ArrivalDateTime");

                    b.Property<int>("CarePlanId");

                    b.Property<DateTime>("DateTimeCreated");

                    b.Property<DateTime>("DateTimeModified");

                    b.Property<string>("Email");

                    b.Property<string>("FirstName")
                        .IsRequired();

                    b.Property<DateTime>("IsDeleted");

                    b.Property<string>("LastName")
                        .IsRequired();

                    b.Property<string>("NationalId")
                        .IsRequired();

                    b.Property<int>("OrganizationId");

                    b.Property<int>("PatientId");

                    b.Property<string>("PhoneNumber")
                        .IsRequired();

                    b.Property<string>("ProviderId");

                    b.Property<int>("Relationship");

                    b.Property<bool>("RequireInitialPayment");

                    b.Property<int>("Status");

                    b.Property<int>("Type");

                    b.Property<int>("VisitFollowUpId");

                    b.Property<int>("VisitType");

                    b.HasKey("Id");

                    b.ToTable("Appointments");
                });

            modelBuilder.Entity("api.Models.ContactType", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.Property<int>("Type");

                    b.HasKey("Id");

                    b.ToTable("ContactType");
                });

            modelBuilder.Entity("api.Models.Country", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Abbreviation")
                        .IsRequired();

                    b.Property<string>("Code")
                        .IsRequired();

                    b.Property<string>("Currency");

                    b.Property<DateTime>("DateTimeCreated");

                    b.Property<DateTime>("IsDeleted");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(30);

                    b.Property<string>("OfficialLanguage");

                    b.HasKey("Id");

                    b.ToTable("Country");
                });

            modelBuilder.Entity("api.Models.Diagnosis", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Code");

                    b.Property<string>("CodeFullName");

                    b.Property<DateTime>("DateTimeCreated");

                    b.Property<DateTime>("IsDeleted");

                    b.Property<string>("Name");

                    b.Property<string>("Prefix");

                    b.Property<int>("Version");

                    b.Property<string>("VersionName");

                    b.HasKey("Id");

                    b.ToTable("Diagnoses");
                });

            modelBuilder.Entity("api.Models.Encounter", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("AdmissionDate");

                    b.Property<string>("AdmittingPhysicianID")
                        .IsRequired();

                    b.Property<DateTime>("DateTimeCreated");

                    b.Property<DateTime>("IsDeleted");

                    b.Property<int>("OrganizationId");

                    b.Property<int>("PatientId");

                    b.Property<int>("VisitId");

                    b.HasKey("Id");

                    b.HasIndex("PatientId");

                    b.ToTable("Encouter");
                });

            modelBuilder.Entity("api.Models.Entity", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Category");

                    b.Property<int>("CountryId");

                    b.Property<DateTime>("DateTimeCreated");

                    b.Property<int>("EntityDemographicId");

                    b.Property<int?>("EntityDemographicId1");

                    b.Property<DateTime>("EntryDate");

                    b.Property<string>("ExternalId");

                    b.Property<string>("Firstname");

                    b.Property<bool>("IsActive");

                    b.Property<DateTime>("IsDeleted");

                    b.Property<string>("Lastname");

                    b.Property<string>("Middlename");

                    b.Property<string>("Specialty")
                        .HasMaxLength(3);

                    b.Property<string>("Type")
                        .HasMaxLength(2);

                    b.HasKey("Id");

                    b.HasIndex("EntityDemographicId1");

                    b.ToTable("Entity");
                });

            modelBuilder.Entity("api.Models.EntityAddress", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Address1");

                    b.Property<string>("Address2");

                    b.Property<DateTime>("BeginDate");

                    b.Property<string>("City");

                    b.Property<string>("Country")
                        .IsRequired()
                        .HasMaxLength(2);

                    b.Property<DateTime>("DateTimeCreated");

                    b.Property<string>("Directions");

                    b.Property<string>("District");

                    b.Property<DateTime>("EndDate");

                    b.Property<int>("EntityId");

                    b.Property<bool>("HasDaylightSaving");

                    b.Property<DateTime>("IsDeleted");

                    b.Property<bool>("IsMailingAddress");

                    b.Property<string>("Label");

                    b.Property<string>("Location");

                    b.Property<string>("State");

                    b.Property<int>("TimeZone");

                    b.Property<int>("TimeZoneId");

                    b.Property<string>("Zip");

                    b.HasKey("Id");

                    b.HasIndex("EntityId");

                    b.ToTable("EntityAddress");
                });

            modelBuilder.Entity("api.Models.EntityContact", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("BeginDate");

                    b.Property<short>("ContactTypeId");

                    b.Property<DateTime?>("EndDate");

                    b.Property<int>("EntityId");

                    b.Property<string>("Label");

                    b.Property<string>("Value");

                    b.HasKey("Id");

                    b.HasIndex("EntityId");

                    b.ToTable("EntityContact");
                });

            modelBuilder.Entity("api.Models.EntityDemographic", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("DOB");

                    b.Property<DateTime>("DateTimeCreated");

                    b.Property<string>("Education")
                        .HasMaxLength(2);

                    b.Property<int>("EntityId");

                    b.Property<decimal>("Income");

                    b.Property<DateTime>("IsDeleted");

                    b.Property<string>("MartalStatus")
                        .HasMaxLength(2);

                    b.Property<string>("NationalID")
                        .IsRequired();

                    b.Property<string>("PrimaryLanguage")
                        .HasMaxLength(2);

                    b.Property<string>("Religion")
                        .HasMaxLength(2);

                    b.Property<string>("SecondaryLanguage")
                        .HasMaxLength(2);

                    b.Property<string>("Sex")
                        .HasMaxLength(2);

                    b.HasKey("Id");

                    b.HasAlternateKey("NationalID");

                    b.ToTable("EntityDemographic");
                });

            modelBuilder.Entity("api.Models.Location", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CountryId");

                    b.Property<int>("EntityId");

                    b.Property<DateTime>("IsDeleted");

                    b.Property<bool>("IsParent");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.Property<int>("OrganizationId");

                    b.Property<string>("OrganizationNationalId")
                        .IsRequired()
                        .HasMaxLength(10);

                    b.Property<int>("OrganizationType");

                    b.Property<int>("ParentOrganizationId");

                    b.Property<DateTime>("SetUpDate");

                    b.Property<string>("ShortName")
                        .IsRequired()
                        .HasMaxLength(25);

                    b.HasKey("Id");

                    b.HasIndex("EntityId");

                    b.ToTable("Location");
                });

            modelBuilder.Entity("api.Models.LogEntity", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Action");

                    b.Property<string>("Controller");

                    b.Property<string>("CurrentUserId");

                    b.Property<DateTime>("DateTimeCreated");

                    b.Property<string>("Event");

                    b.Property<DateTime>("IsDeleted");

                    b.Property<string>("Name");

                    b.Property<string>("Payload");

                    b.Property<string>("Timestamp");

                    b.Property<string>("Type");

                    b.HasKey("Id");

                    b.ToTable("Logs");
                });

            modelBuilder.Entity("api.Models.Medication", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("BeginDate");

                    b.Property<string>("Classification");

                    b.Property<DateTime>("EndDate");

                    b.Property<string>("FinancialResponsable");

                    b.Property<bool>("IsActinRequired");

                    b.Property<bool>("IsCustom");

                    b.Property<bool>("IsHighRisk");

                    b.Property<DateTime>("LastUpdateDate");

                    b.Property<int>("MedicationCustomId");

                    b.Property<string>("MedicationName");

                    b.Property<int>("MedicationNameId");

                    b.Property<char>("NewChangeExists");

                    b.Property<DateTime>("OrderDate");

                    b.Property<bool>("OverTheCounter");

                    b.Property<int>("PatientId");

                    b.Property<string>("RelatedDiagnosis");

                    b.Property<bool>("RelatedToTerminalIllness");

                    b.Property<string>("ScreenDescription");

                    b.Property<int>("ScreeningCount");

                    b.HasKey("Id");

                    b.HasIndex("PatientId");

                    b.ToTable("Medication");
                });

            modelBuilder.Entity("api.Models.MetricsEntity", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Action");

                    b.Property<string>("Controller");

                    b.Property<string>("CurrentUserId");

                    b.Property<DateTime>("DateTimeCreated");

                    b.Property<string>("Event");

                    b.Property<DateTime>("IsDeleted");

                    b.Property<string>("Name");

                    b.Property<string>("Payload");

                    b.Property<string>("Timestamp");

                    b.Property<string>("Type");

                    b.HasKey("Id");

                    b.ToTable("Metrics");
                });

            modelBuilder.Entity("api.Models.Organization", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CountryId");

                    b.Property<DateTime>("DateTimeCreated");

                    b.Property<int>("EntityId");

                    b.Property<DateTime>("IsDeleted");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.Property<string>("NationalId")
                        .IsRequired()
                        .HasMaxLength(10);

                    b.Property<string>("OrgSystemId");

                    b.Property<int>("OrganizationType");

                    b.Property<int>("ParentOrganizationId")
                        .ValueGeneratedOnAddOrUpdate()
                        .HasComputedColumnSql("[Id]");

                    b.Property<string>("ShortName")
                        .IsRequired()
                        .HasMaxLength(25);

                    b.HasKey("Id");

                    b.HasAlternateKey("NationalId");

                    b.HasIndex("EntityId");

                    b.ToTable("Organization");
                });

            modelBuilder.Entity("api.Models.Patient", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("AcceptedHIE");

                    b.Property<string>("AllergyNote");

                    b.Property<int>("CountryId");

                    b.Property<DateTime>("DateTimeCreated");

                    b.Property<DateTime?>("DeathDate");

                    b.Property<int>("EntityId");

                    b.Property<DateTime>("IsDeleted");

                    b.Property<string>("MedicalRecordNumber")
                        .IsRequired()
                        .HasMaxLength(10);

                    b.Property<string>("NationalPatientId")
                        .IsRequired()
                        .HasMaxLength(12);

                    b.Property<bool>("NoKnownAllergy");

                    b.Property<bool>("NoKnownMed");

                    b.Property<string>("NonClinicalNote");

                    b.Property<string>("TracknetNumber");

                    b.HasKey("Id");

                    b.HasIndex("EntityId");

                    b.ToTable("Patient");
                });

            modelBuilder.Entity("api.Models.PatientAllergy", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("PatientId");

                    b.HasKey("Id");

                    b.HasIndex("PatientId");

                    b.ToTable("PatientAllergy");
                });

            modelBuilder.Entity("api.Models.PatientChart", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AdmissionId");

                    b.Property<DateTime>("AuthorCompletionDate");

                    b.Property<int>("AuthorizedPersonnelId");

                    b.Property<DateTime>("CompletionDate");

                    b.Property<DateTime>("CreationDate");

                    b.Property<string>("Description");

                    b.Property<string>("Discipline");

                    b.Property<string>("DocumentFormat");

                    b.Property<string>("DocumentId");

                    b.Property<string>("DocumentSubType");

                    b.Property<DateTime>("EntryDateTime");

                    b.Property<bool>("IsActive");

                    b.Property<DateTime?>("IsDeleted");

                    b.Property<DateTime>("ModificationDateTime");

                    b.Property<string>("ModifiedByUserId");

                    b.Property<int>("PatientId");

                    b.Property<DateTime?>("PatientSignedDate");

                    b.Property<int>("PersonnelId");

                    b.Property<DateTime?>("PhysicianSignedDate");

                    b.Property<string>("UserId");

                    b.Property<string>("Version");

                    b.HasKey("Id");

                    b.ToTable("PatientChart");
                });

            modelBuilder.Entity("api.Models.PatientDiagnosis", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Code");

                    b.Property<int>("CodingVersion");

                    b.Property<string>("CreatedBy")
                        .IsRequired();

                    b.Property<DateTime>("DateTimeCreated");

                    b.Property<string>("Description");

                    b.Property<int>("EncounterId");

                    b.Property<DateTime>("IsDeleted");

                    b.Property<string>("Name");

                    b.Property<int>("Rank");

                    b.Property<int>("Type");

                    b.Property<DateTime>("VisitDateTime");

                    b.HasKey("Id");

                    b.HasIndex("EncounterId");

                    b.ToTable("PatientDiagnoses");
                });

            modelBuilder.Entity("api.Models.PatientMedication", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<char>("DosageType");

                    b.Property<string>("ICDCodeSet");

                    b.Property<bool>("IsCompoundMed");

                    b.Property<string>("MedNameType");

                    b.Property<string>("MedPrescriptionNumber");

                    b.Property<int>("MedicationId");

                    b.Property<string>("MiscellaneousNote");

                    b.Property<DateTime>("OrderDate");

                    b.Property<bool>("OverTheCounter");

                    b.Property<int>("PrefferedPharmacy");

                    b.Property<string>("Refill");

                    b.Property<string>("RelatedDiagnosis");

                    b.Property<string>("Source");

                    b.Property<int>("UserMedId");

                    b.HasKey("Id");

                    b.HasIndex("MedicationId");

                    b.ToTable("PatientMedication");
                });

            modelBuilder.Entity("api.Models.Transfer", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("ArrivalDateTime");

                    b.Property<DateTime>("DateTimeCreated");

                    b.Property<int?>("EncounterId");

                    b.Property<DateTime>("IsDeleted");

                    b.Property<string>("NationalPatientId");

                    b.Property<int>("OrganizationId");

                    b.Property<int>("OriginOrganizationId");

                    b.Property<int?>("PatientId");

                    b.Property<int>("Relationship");

                    b.Property<string>("TransferPhysicianUserId");

                    b.Property<int>("VisitType");

                    b.HasKey("Id");

                    b.HasIndex("EncounterId");

                    b.HasIndex("PatientId");

                    b.ToTable("Transfer");
                });

            modelBuilder.Entity("api.Models.UserActionEntity", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Action");

                    b.Property<string>("Controller");

                    b.Property<string>("CurrentUserId");

                    b.Property<DateTime>("DateTimeCreated");

                    b.Property<string>("Event");

                    b.Property<DateTime>("IsDeleted");

                    b.Property<string>("Name");

                    b.Property<string>("Payload");

                    b.Property<string>("Status");

                    b.Property<string>("SubjectIds");

                    b.Property<string>("Timestamp");

                    b.Property<string>("Type");

                    b.HasKey("Id");

                    b.ToTable("UserActions");
                });

            modelBuilder.Entity("api.Models.UserOrganization", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("EndDate");

                    b.Property<int>("OrganizationId");

                    b.Property<string>("RoleId");

                    b.Property<DateTime>("StartDate");

                    b.Property<string>("UserId");

                    b.HasKey("Id");

                    b.ToTable("UserOrganization");
                });

            modelBuilder.Entity("api.Models.Visit", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AppointmentId");

                    b.Property<DateTime>("AssignedADoctorAt");

                    b.Property<DateTime>("AssignedANurseAt");

                    b.Property<DateTime>("DateTimeCreated");

                    b.Property<string>("DoctorId");

                    b.Property<int>("EncounterId");

                    b.Property<DateTime>("IsDeleted");

                    b.Property<bool>("IsFollowUp");

                    b.Property<bool>("IsPatientEnsured");

                    b.Property<bool>("IsReferral");

                    b.Property<string>("NurserId");

                    b.Property<int>("OrganizationId");

                    b.Property<int>("PatientId");

                    b.Property<string>("ReceptionistId")
                        .IsRequired();

                    b.Property<int>("Status");

                    b.Property<DateTime>("VisitDateTime");

                    b.Property<string>("VisitReason")
                        .IsRequired()
                        .HasMaxLength(300);

                    b.HasKey("Id");

                    b.ToTable("Visit");
                });

            modelBuilder.Entity("api.Models.VisitHistoryLog", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AppointmentId");

                    b.Property<DateTime>("ArchiveDateTime");

                    b.Property<DateTime>("AssignedADoctorAt");

                    b.Property<DateTime>("AssignedANurseAt");

                    b.Property<DateTime>("DateTimeCreated");

                    b.Property<string>("DoctorId");

                    b.Property<int>("EncounterId");

                    b.Property<DateTime>("IsDeleted");

                    b.Property<bool>("IsFollowUp");

                    b.Property<bool>("IsPatientEnsured");

                    b.Property<bool>("IsReferral");

                    b.Property<string>("NurserId");

                    b.Property<int>("OrganizationId");

                    b.Property<int>("PatientId");

                    b.Property<string>("ReceptionistId");

                    b.Property<int>("Status");

                    b.Property<DateTime>("VisitDateTime");

                    b.Property<int>("VisitOutcome");

                    b.Property<string>("VisitReason");

                    b.HasKey("Id");

                    b.ToTable("VisitHistoryLog");
                });

            modelBuilder.Entity("api.Models.VisitQueue", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("ArrivalDateTime");

                    b.Property<DateTime>("DateTimeCreated");

                    b.Property<string>("FirstName");

                    b.Property<DateTime>("IsDeleted");

                    b.Property<string>("LastName");

                    b.Property<int>("OrganizationId");

                    b.Property<int>("QueueOrderNumber")
                        .ValueGeneratedOnAddOrUpdate()
                        .HasComputedColumnSql("[Id]");

                    b.Property<int>("Relationship");

                    b.Property<int>("VisitType");

                    b.HasKey("Id");

                    b.ToTable("VisitQueue");
                });

            modelBuilder.Entity("api.Models.VisitReason", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("AppointmentId");

                    b.Property<string>("Reason");

                    b.Property<int>("RelatedKnownIssue");

                    b.HasKey("Id");

                    b.HasIndex("AppointmentId");

                    b.ToTable("VisitReason");
                });

            modelBuilder.Entity("api.Models.VitalSign", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("DateTimeCreated");

                    b.Property<int>("EncounterId");

                    b.Property<DateTime>("IsDeleted");

                    b.Property<string>("Notes");

                    b.Property<int>("Severity");

                    b.Property<double>("Value");

                    b.Property<DateTime>("VisitDateTime");

                    b.Property<int?>("VisitHistoryLogId");

                    b.Property<int?>("VisitId");

                    b.Property<int>("VitalSignTypeId");

                    b.HasKey("Id");

                    b.HasIndex("EncounterId");

                    b.HasIndex("VisitHistoryLogId");

                    b.HasIndex("VisitId");

                    b.HasIndex("VitalSignTypeId");

                    b.ToTable("VitalSigns");
                });

            modelBuilder.Entity("api.Models.VitalSignMeasure", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AgeRange");

                    b.Property<DateTime>("DateTimeCreated");

                    b.Property<DateTime>("IsDeleted");

                    b.Property<double>("MaxAge");

                    b.Property<decimal>("MaxValue");

                    b.Property<double>("MinAge");

                    b.Property<decimal>("MinValue");

                    b.Property<int>("VitalSignTypeId");

                    b.HasKey("Id");

                    b.HasIndex("VitalSignTypeId");

                    b.ToTable("VitalSignMeasure");
                });

            modelBuilder.Entity("api.Models.VitalSignType", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("DateTimeCreated");

                    b.Property<string>("Description")
                        .IsRequired();

                    b.Property<DateTime>("IsDeleted");

                    b.Property<bool>("IsUsed");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<string>("Unit")
                        .IsRequired();

                    b.HasKey("Id");

                    b.ToTable("VitalSignType");
                });

            modelBuilder.Entity("api.Models.AdmissionDiagnosis", b =>
                {
                    b.HasOne("api.Models.Admission", "Admission")
                        .WithMany()
                        .HasForeignKey("AdmissionId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("api.Models.AdmissionDiagnosisDetail", b =>
                {
                    b.HasOne("api.Models.Admission", "Admission")
                        .WithMany()
                        .HasForeignKey("AdmissionId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("api.Models.AdmissionFacility", b =>
                {
                    b.HasOne("api.Models.Admission", "Admission")
                        .WithMany()
                        .HasForeignKey("AdmissionId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("api.Models.Encounter", b =>
                {
                    b.HasOne("api.Models.Patient", "Patient")
                        .WithMany()
                        .HasForeignKey("PatientId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("api.Models.Entity", b =>
                {
                    b.HasOne("api.Models.EntityDemographic", "Demographic")
                        .WithMany()
                        .HasForeignKey("EntityDemographicId1");
                });

            modelBuilder.Entity("api.Models.EntityAddress", b =>
                {
                    b.HasOne("api.Models.Entity")
                        .WithMany("Addresses")
                        .HasForeignKey("EntityId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("api.Models.EntityContact", b =>
                {
                    b.HasOne("api.Models.Entity")
                        .WithMany("Contacts")
                        .HasForeignKey("EntityId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("api.Models.Location", b =>
                {
                    b.HasOne("api.Models.Entity", "Entity")
                        .WithMany()
                        .HasForeignKey("EntityId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("api.Models.Medication", b =>
                {
                    b.HasOne("api.Models.Patient", "Patient")
                        .WithMany("Meds")
                        .HasForeignKey("PatientId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("api.Models.Organization", b =>
                {
                    b.HasOne("api.Models.Entity", "Entity")
                        .WithMany()
                        .HasForeignKey("EntityId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("api.Models.Patient", b =>
                {
                    b.HasOne("api.Models.Entity", "Entity")
                        .WithMany()
                        .HasForeignKey("EntityId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("api.Models.PatientAllergy", b =>
                {
                    b.HasOne("api.Models.Patient", "Patient")
                        .WithMany()
                        .HasForeignKey("PatientId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("api.Models.PatientDiagnosis", b =>
                {
                    b.HasOne("api.Models.Encounter", "Encounter")
                        .WithMany()
                        .HasForeignKey("EncounterId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("api.Models.PatientMedication", b =>
                {
                    b.HasOne("api.Models.Medication", "Medication")
                        .WithMany()
                        .HasForeignKey("MedicationId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("api.Models.Transfer", b =>
                {
                    b.HasOne("api.Models.Encounter", "Encounter")
                        .WithMany()
                        .HasForeignKey("EncounterId");

                    b.HasOne("api.Models.Patient", "Patient")
                        .WithMany()
                        .HasForeignKey("PatientId");
                });

            modelBuilder.Entity("api.Models.VisitReason", b =>
                {
                    b.HasOne("api.Models.Appointment")
                        .WithMany("VisitReasons")
                        .HasForeignKey("AppointmentId");
                });

            modelBuilder.Entity("api.Models.VitalSign", b =>
                {
                    b.HasOne("api.Models.Encounter", "Encounter")
                        .WithMany()
                        .HasForeignKey("EncounterId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("api.Models.VisitHistoryLog")
                        .WithMany("VitalSigns")
                        .HasForeignKey("VisitHistoryLogId");

                    b.HasOne("api.Models.Visit")
                        .WithMany("VitalSigns")
                        .HasForeignKey("VisitId");

                    b.HasOne("api.Models.VitalSignType", "Type")
                        .WithMany()
                        .HasForeignKey("VitalSignTypeId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("api.Models.VitalSignMeasure", b =>
                {
                    b.HasOne("api.Models.VitalSignType", "Type")
                        .WithMany()
                        .HasForeignKey("VitalSignTypeId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
