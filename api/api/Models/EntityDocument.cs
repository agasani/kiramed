﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace api.Models
{
    [Table("EntityDocument")]
    public class EntityDocument: IEntity
    {
        public int Id { get; set; }
    }
}
