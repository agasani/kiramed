﻿using System.Linq;
using api.Db;
using api.ViewModels;

namespace api.Models
{
    public static class EntityHelper
    {
        public static Entity CreateEntity(EntityRegistrationViewModel vModel, KiraContext context)
        {
            //entity creation: you dont need all the fields to create an entity
            //all other crap are provided as updates.

            var newEntity = new Entity()
            {
                Firstname = vModel.FirstName,
                Lastname = vModel.SecondName,
                ExternalId = vModel.NationalId
            };

            //add phone contact
            var contactType1 = new ContactType { Name = "Phone", Type = 1 };
            var phoneContact = new EntityContact()
            {
                ContactTypeId = (short)ContactTypes.Phone,
                Value = vModel.PhoneNumber,
               // ContactType = contactType1
            };
            newEntity.Contacts.Add(phoneContact);

            //add email contact
            var contactType2 = new ContactType { Name = "Email", Type = 2 };
            var emailContact = new EntityContact()
            {
                ContactTypeId = (short)ContactTypes.Email,
                Value = vModel.Email,
                //ContactType = contactType2
            };

            newEntity.Contacts.Add(emailContact);

            var demographic = new EntityDemographic
            {
                NationalID = vModel.NationalId
            };

            newEntity.Demographic = demographic;

            var entity = vModel.CheckIfEntityExists(context);
            if (entity == null)
            {
                context.Entities.Add(newEntity);
                context.SaveChanges();
                return newEntity;
            }
            else
            {
                //Update(newEntity);
                return newEntity;
            }
        }

        public static string GetEntityEmail(Entity entity)
        {
            var email = entity.Contacts.FirstOrDefault(c => c.ContactTypeId == (short) ContactTypes.Email);
            if (email != null)
                return entity.Contacts.Any()
                    ? email.Value
                    : null;
            return null;
        }
           
        public static string GetEntityPhoneNumber(Entity entity)
        {
            var phoneNumber = entity.Contacts.FirstOrDefault(c => c.ContactTypeId == (short)ContactTypes.Phone);
            if (phoneNumber != null)
                return entity.Contacts.Any()
                    ? phoneNumber.Value
                    : null;
            return null;
        }

        public static string GetEntityViewModelEmail(EntityViewModel entity)
        {
            var email = entity.Contacts.FirstOrDefault(c => c.ContactTypeId == (short)ContactTypes.Email);
            if (email != null)
                return entity.Contacts.Any()
                    ? email.Value
                    : null;
            return null;
        }

        public static string GetEntityViewModelPhoneNumber(EntityViewModel entity)
        {
            var phoneNumber = entity.Contacts.FirstOrDefault(c => c.ContactTypeId == (short)ContactTypes.Phone);
            if (phoneNumber != null)
                return entity.Contacts.Any()
                    ? phoneNumber.Value
                    : null;
            return null;
        }
    }
}
