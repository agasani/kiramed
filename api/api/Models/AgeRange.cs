﻿using System.ComponentModel.DataAnnotations.Schema;

namespace api.Models
{
    [Table("AgeRanges")]
    public class AgeRange : KiraEntity
    {
        public string MeasureType { get; set; }
        public string RangeName { get; set; }
        public eAgeUnits Unit { get; set; }
        public double MinimumAge { get; set; }
        public double MaximumAge { get; set; }
    }
}
