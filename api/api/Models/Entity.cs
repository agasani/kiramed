﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using api.Auth;
using api.ViewModels;
using Nest;

namespace api.Models
{
    [Table("Entity")]
    [ElasticsearchType(Name ="Entity")]
    public class Entity : KiraEntity
    {
        public Entity() { }
        public Entity(EntityViewModel vModel)
        {
            if (vModel == null)
                return;
            IsActive = (bool) vModel?.IsActive;
            Category = vModel?.Category;
            EntryDate = DateTime.Today;
            ExternalId = vModel?.ExternalId;
            Firstname = vModel?.Firstname;
            Lastname = vModel?.Lastname;
            Middlename = vModel?.Middlename;
            Specialty = vModel?.Specialty;
            Type = vModel?.Type;
            Demographic = new EntityDemographic(vModel.Demographic);
            foreach (var addr in vModel?.Addresses)
            {
                var address = new EntityAddress(addr);
                Addresses.Add(address);
            }
            foreach (var cont in vModel?.Contacts)
            {
                var contact = new EntityContact(cont);
                Contacts.Add(contact);
            }
        }
        //Exteranal ID eg. National ID number or Hospital ID number
        public string ExternalId { get; set; }
        [StringLength(2)]
        public string Type { get; set; }
        //Category eg. P for Patient
        public string Category { get; set; }
        public string Lastname { get; set; }
        public string Firstname { get; set; }
        public string Middlename { get; set; } = "";
        //Organization ID 
        //public int OrganizationId { get; set; }
        //Account Status 1 for active and 0 for inactive
        public bool IsActive { get; set; } = true;  //TODO rename to IsDeleted for consistency?
        public DateTime EntryDate { get; set; } = DateTime.Now;

        [Required]
        public int EntityDemographicId { get; set; }
        //Spacialty
        [StringLength(3)]
        public string Specialty { get; set; } = "";
        public EntityDemographic Demographic { get; set; } = new EntityDemographic();
        public List<EntityContact> Contacts { get; set; } = new List<EntityContact>();
        public List<EntityAddress> Addresses { get; set; } = new List<EntityAddress>();
        [Required]
        public int CountryId { get; set; }

        public Entity(UserRegisterViewModel user)  //used at user registration
        {
           Firstname = user.FirstName;
           Lastname = user.SecondName;
           ExternalId = user.NationalId;         
           Contacts.AddRange(EntityContact.CreateContacts(user.PhoneNumber, user.Email));
           Demographic = EntityDemographic.CreateEntityDemographic(user.NationalId);
        }
        public void Update(EntityViewModel vModel)
        {
            if (vModel == null)
                return;
            Firstname = vModel.Firstname;
            Lastname = vModel?.Lastname;
            IsActive = (bool) vModel?.IsActive;
            Middlename = vModel?.Middlename;

            if (!string.IsNullOrEmpty(vModel?.ExternalId))
                ExternalId = vModel?.ExternalId;

            Demographic.Update(vModel?.Demographic);

            foreach (var ctct in vModel?.Contacts)
            {
                if (!ctct.IsValid()) continue;
                var toUpdate = Contacts.FirstOrDefault(c => c.Id == ctct.Id);
                if (toUpdate == null)
                    Contacts.Add(new EntityContact(ctct));
                else
                    toUpdate.Update(ctct);
            }

            foreach (var addr in vModel?.Addresses)
            {
                if (!addr.IsValid()) continue;
                var toUpdate = Addresses.FirstOrDefault(c => c.Id == addr.Id);
                if (toUpdate == null)
                    Addresses.Add(new EntityAddress(addr));
                else
                    toUpdate.Update(addr);
            }
        }

        public void Update(EntityRegistrationViewModel vModel)
        {
            if (vModel == null)
                return;
            Firstname = vModel?.FirstName;
            Lastname = vModel?.SecondName;
            //IsActive = vModel?.IsActive ?? true;
            if (!string.IsNullOrEmpty(vModel.NationalId))
                ExternalId = vModel?.NationalId;

            Demographic.Update(vModel?.Demographic);

            foreach (var ctct in vModel?.Contacts)
            {
                if (!ctct.IsValid()) continue;
                var toUpdate = Contacts.FirstOrDefault(c => c.Id == ctct.Id);
                if (toUpdate == null)
                    Contacts.Add(new EntityContact(ctct));
                else
                    toUpdate.Update(ctct);
            }

            foreach (var addr in vModel?.Addresses)
            {
                if (!addr.IsValid()) continue;
                var toUpdate = Addresses.FirstOrDefault(c => c.Id == addr.Id);
                if (toUpdate == null)
                    Addresses.Add(new EntityAddress(addr));
                else
                    toUpdate.Update(addr);
            }
        }

        public void Update(UserRegisterViewModel user)  //used when registering a deleted user
        {
            if (user == null)
                return;
            Firstname = user?.FirstName;
            Lastname = user?.SecondName;
           
            if (!string.IsNullOrEmpty(user?.NationalId))
                ExternalId = user?.NationalId;
            Demographic.NationalID = user?.NationalId;
            Contacts.FirstOrDefault(c => c.ContactTypeId == (short) ContactTypes.Phone).Value = user?.PhoneNumber;
            Contacts.FirstOrDefault(c => c.ContactTypeId == (short) ContactTypes.Email).Value = user?.Email;
        }
        public Entity(EntityRegistrationViewModel vModel)
        {
            Firstname = vModel.FirstName;
            Lastname = vModel.SecondName;
            ExternalId = vModel.NationalId;

            Demographic = vModel.Demographic.NationalID == null ? EntityDemographic.CreateEntityDemographic(vModel.NationalId) : new EntityDemographic(vModel.Demographic);

            if (vModel.Addresses.Count != 0)
            {
                foreach (var addr in vModel.Addresses)
                {
                    var address = new EntityAddress(addr);
                    Addresses.Add(address);
                }
            }

            if(vModel.Contacts.Count != 0)
            {
                foreach (var cont in vModel.Contacts)
                {
                    var contact = new EntityContact(cont);
                    Contacts.Add(contact);
                }
            }
            else
            {
                Contacts.AddRange(EntityContact.CreateContacts(vModel.PhoneNumber,vModel.Email));
            }                     
        }
        public Entity(string firstName, string secondName, string phoneNumber, string email, string nationalId)  //used at user registration
        {
            Firstname = firstName;
            Lastname = secondName;
            ExternalId =nationalId;
            Contacts.AddRange(EntityContact.CreateContacts(phoneNumber, email));         
            Demographic = EntityDemographic.CreateEntityDemographic(nationalId);
        }
    }

}
