﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api.Models
{
    public interface IKiraEntity
    {
       int Id { get; set; }
       DateTime IsDeleted { get; set; } 
       DateTime DateTimeCreated { get; set; } 
    }
}
