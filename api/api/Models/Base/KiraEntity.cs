﻿using System;

namespace api.Models
{
    public class KiraEntity: IEntity
    {
        public int Id { get; set; }
        public DateTime IsDeleted { get; set; }  = DateTime.MaxValue;
        public DateTime DateTimeCreated { get; set; }  = DateTime.Now;
    } 
}
