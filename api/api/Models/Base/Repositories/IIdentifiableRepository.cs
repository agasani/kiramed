﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api.Models
{
    public interface IIdentifiableRepository<T> where T : IEntity
    {
        Task<T> FindByPhoneNumberAsync(string phoneNumber);
        T FindByPhoneNumber(string phoneNumber);

        Task<T> FindByEmailAsync(string email);
        T FindByEmail(string email);

        //TIN numbers for business entities and national ids for people
        Task<T> FindByNationalIdAsync(string nationlaId);
        T FindByNationalId(string nationalId);

        //system created unique ids
        Task<T> FindBySystemIdAsync(string systemId);
        T FindBySystemId(string systemId);

        Task<T> FindByFullNameAsync(string [] name);
        IEnumerable<T> FindByFullName(string fName, string lName);
    }
}
