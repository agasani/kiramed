﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using api.Db;
using Microsoft.EntityFrameworkCore;

namespace api.Models
{
    public abstract class KiraRepository <T> : IKiraRepository<T> where T : KiraEntity
    {
        private readonly KiraContext _context;
        private readonly DbSet<T> _entities;

        protected KiraRepository(KiraContext context)
        {
            _context = context;
            _entities = _context.Set<T>();
        }

        public virtual async Task<int> CreateAsync(T entity)
        {
            await _entities.AddAsync(entity);
            await _context.SaveChangesAsync();
            return entity.Id;
        }

        public virtual void Delete(T entity)
        {
           entity.IsDeleted = DateTime.Now;
            _context.SaveChangesAsync();
        }

        public virtual async Task<T> FindAsync(int id)
        {
            var entity = await _entities.FindAsync(id);         
            if (entity != null && entity.IsDeleted > DateTime.Now) return entity;
            return null;
        }     

        public virtual async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _entities.Take(100).Where(e => e.IsDeleted > DateTime.Now).ToListAsync();
        }

        public virtual async Task<IEnumerable<T>> GetAsync(Expression<Func<T, bool>> expression)
        {
            try
            {
                var result = await _context.Set<T>().Where(e => e.IsDeleted > DateTime.Now).Where(expression).ToListAsync();
                return result;
            }
            catch (NullReferenceException)
            {
                return new List<T>();
            }         
        }
        public virtual async Task<int> UpdateAsync(T entity)
        {
            return await _context.SaveChangesAsync() != 0 ? entity.Id : 0;
        }
    }
}
