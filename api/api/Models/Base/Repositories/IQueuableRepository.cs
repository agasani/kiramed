using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using api.Base.Models;

namespace api.Models
{
    public interface IQueuableRepository<T> where T : IQueuable
    {
        Task<IEnumerable<T>> LoadQueueAsync(Expression<Func<T, bool>> expression);
    }
}