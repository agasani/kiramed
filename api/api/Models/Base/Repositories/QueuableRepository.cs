using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using api.Base.Models;

namespace api.Models
{
    public abstract class QueuableRepository<T> where T : Queuable
    {
        public abstract Task<IEnumerable<T>> LoadQueueAsync(Expression<Func<T, bool>> expression);
    }
}