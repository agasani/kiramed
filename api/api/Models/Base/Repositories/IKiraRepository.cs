﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using api.Models.Base;

namespace api.Models
{
    public interface IKiraRepository<T> where T : KiraEntity
    {
        //get the entire list by country or all
        Task<IEnumerable<T>> GetAllAsync();

        Task<IEnumerable<T>> GetAsync(Expression<Func<T, bool>> expression);

        //finnd by the id
        Task<T> FindAsync(int id);

        //created
        Task<int> CreateAsync(T entity);

        //delete
        void Delete(T entity);

        //update
        Task<int> UpdateAsync(T entity);

    }
}
