﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using api.Models;

namespace api.Services
{
    public abstract class KiraService<T> : IKiraService<T> where T : KiraEntity 
    {
        private readonly IKiraRepository<T> _kiraRepository;

        protected KiraService(IKiraRepository<T> kiraRepository)
        {
            _kiraRepository = kiraRepository;
        }

        public async Task<int> CreateAsync(T entity)
        {
            return await _kiraRepository.CreateAsync(entity);
        }

        public virtual async Task<T> FindAsync(int id)
        {
            return await _kiraRepository.FindAsync(id);
        }

        public virtual async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _kiraRepository.GetAllAsync();
        }

        public  virtual async Task<IEnumerable<T>> GetAsync(Expression<Func<T, bool>> expression)
        {
            return await _kiraRepository.GetAsync(expression);
        }

        public async Task<int> UpdateAsync(T entity)
        {
            return await _kiraRepository.UpdateAsync(entity);
        }
    }
}
