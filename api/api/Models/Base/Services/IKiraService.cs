﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using api.Models;

namespace api.Services
{
    public interface IKiraService<T> where T: KiraEntity
    {
        Task<int> CreateAsync(T entity);
        Task<int> UpdateAsync(T entity);
        Task<IEnumerable<T>> GetAllAsync();
        Task<T> FindAsync(int id);
        Task<IEnumerable<T>> GetAsync(Expression<Func<T, bool>> expression);
    }
}
