using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using api.Models;

namespace api.Base.Models
{
    public interface IQueuable
    {
        int QueueOrderNumber { get; set; }
        //DateTime DequeuedDateTime []
    }
}