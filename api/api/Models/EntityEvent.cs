﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace api.Models
{
    [Table("EntityEvent")]
    public class EntityEvent: IEntity
    {
        public int Id { get; set; }
        public int EntId { get; set; }
        public Entity Entity { get; set; }
    }
}
