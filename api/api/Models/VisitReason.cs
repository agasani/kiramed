﻿namespace api.Models
{
    public class VisitReason: IEntity
    {
        public int Id { get; set; }
        public string Reason { get; set; }
        public int RelatedKnownIssue { get; set; } = 0;
    }
}
