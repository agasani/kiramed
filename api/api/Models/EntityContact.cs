﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using api.ViewModels;

namespace api.Models
{
    public enum ContactTypes { Phone = 1, Email = 2, Twitter = 3, Facebook  = 4};
    //Entity Contact eg. Phone, Email etc.
    [Table("EntityContact")]
    public class EntityContact: IEntity
    {
        public EntityContact() { }
        public EntityContact(EntityContactViewModel vModel)
        {
            this.ContactTypeId = (short)vModel.ContactTypeId;
            this.Value = vModel?.Value;
            this.Label = vModel?.Label;
           // this.ContactType = vModel?.ContactType;
        }

        public int Id { get; set; }
        public int EntityId { get; set; }
        //public Entity Entity { get; set; }
        public short ContactTypeId { get; set; }
        //public ContactType ContactType { get; set; }
        public string Value { get; set; }
        public string Label { get; set; }
        public DateTime BeginDate { get; set; } = DateTime.Today;
        public DateTime? EndDate { get; set; }
        public void Update(EntityContactViewModel conta)
        {
            if (conta.Deactivate)
                this.EndDate = DateTime.Now;
            this.Value = conta.Value;
            this.Label = conta.Label;
            this.ContactTypeId = conta.ContactTypeId;
        }

        public static IList<EntityContact> CreateContacts(string phoneNumber, string email)
        {
            var entityContacts = new List<EntityContact>();

            var phoneContact = new EntityContact
            {
                ContactTypeId = (short)ContactTypes.Phone,
                Value = phoneNumber
                //ContactType = contactType1
            };
            entityContacts.Add(phoneContact);

            //add email contact
            var emailContact = new EntityContact
            {
                ContactTypeId = (short)ContactTypes.Email,
                Value = email
                // ContactType = contactType2
            };

            entityContacts.Add(emailContact);

            return entityContacts;
        }

    }

    [Table("ContactType")]
    public class ContactType : IEntity
    {
        public int Id { get; set; }
        public int Type { get; set; }
        public string Name { get; set; }
    }

    
}
