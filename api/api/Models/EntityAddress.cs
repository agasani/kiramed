﻿using api.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace api.Models
{
    [Table("EntityAddress")]
    public class EntityAddress: KiraEntity
    {
        public int Id { get; set; }
        [Required]
        public int EntityId { get; set; }
       // public Entity Entity { get; set; }
        public string Label { get; set; }
        public string Directions { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Location { get; set; }
        public int TimeZone { get; set; }
        public int TimeZoneId { get; set; }
        public bool HasDaylightSaving { get; set; }
        public string Zip { get; set; }
        public string District { get; set; }
        public bool IsMailingAddress { get; set; }
        [Required]
        [StringLength(2)]
        public string Country { get; set; }
        public DateTime BeginDate { get; set; } = DateTime.Now;
        public DateTime EndDate { get; set; }
        public void Update(EntityAddressViewModel vModel)
        {
            this.Label = vModel.Label;
            this.Directions = vModel.Directions;
            this.Address1 = vModel.Address1;
            this.Address2 = vModel.Address2;
            this.City = vModel.City;
            this.State = vModel.State;
            this.Location = vModel.Location;
            this.TimeZone = vModel.TimeZone;
            this.TimeZoneId = vModel.TimeZoneId;
            this.HasDaylightSaving = vModel.HasDaylightSaving;
            this.Zip = vModel.Zip;
            this.District = vModel.District;
            this.IsMailingAddress = vModel.IsMailingAddress;
            this.BeginDate = vModel.BeginDate;
            if (vModel.Deactivate)
                this.EndDate = DateTime.Today;
            this.Country = vModel.Country;
        }
        public EntityAddress() { }
        public EntityAddress(EntityAddressViewModel vModel)
        {
            this.Label = vModel.Label;
            this.Directions = vModel.Label;
            this.Address1 = vModel.Address1;
            this.Address2 = vModel.Address2;
            this.City = vModel.City;
            this.State = vModel.State;
            this.Location = vModel.Location;
            this.TimeZone = vModel.TimeZone;
            this.TimeZoneId = vModel.TimeZoneId;
            this.HasDaylightSaving = vModel.HasDaylightSaving;
            this.Zip = vModel.Zip;
            this.District = vModel.District;
            this.IsMailingAddress = vModel.IsMailingAddress;
            this.Country = vModel.Country;
        }
    }

}
