﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using api.ViewModels;

namespace api.Auth
{
    public class UserRegisterViewModel
    {

        [Required]
        [MinLength(6)]
        public string UserName { get; set; }

        public string Password { get; set; }   //not needed for employee creation  
        [Required]
        public int CountryId { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string SecondName { get; set; }
        [Required]
        public string NationalId { get; set; }
        public int ParentId { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        [Phone]
        public string PhoneNumber { get; set; }
        [Required]
        public List<string> Roles { get; set; }
        public DateTime StartDate { get; set; }
        public int OrganizationId { get; set; }
        public string MiddleName { get; set; }

        public UserRegisterViewModel(AdminRegistrationViewModel model)
        {
            FirstName = model.AdminFirstName;
            SecondName = model.AdminSecondName;
            UserName = Utils.UserNameCreator.UserName(FirstName, SecondName);
            Email = model.AdminEmail;
            PhoneNumber = model.AdminPhoneNumber;
            NationalId = model.AdminNationalId;
            Roles = new List<string> {"Country Admin"};
            StartDate = DateTime.Now;
            OrganizationId = CountryId;
        }

        public UserRegisterViewModel()
        {
            
        }
    }


}
