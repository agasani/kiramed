﻿using api.Db;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Nest;

namespace api.Models
{
    public interface IEntityRegistrationViewModel<T> where T : IEntity
    {
        [Required]
        int CountryId { get; set; }
        [Required]
        [StringLength(50)]
        string FirstName { get; set; }
        [Required]
        [StringLength(100)]
        string SecondName { get; set; }
        [Required]
        string NationalId { get; set; }
        int ParentId { get; set; }
        string Email { get; set; }
        string PhoneNumber { get; set; }
        T CheckIfEntityExists(KiraContext context);

    }
}
