﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using api.ViewModels;

namespace api.Models
{
    [Table("EntityDemographic")]
    public class EntityDemographic: KiraEntity
    {
        public EntityDemographic() { }
        public EntityDemographic(EntityDemographicViewModel vModel)
        {
            this.DOB = vModel.DOB;
            this.Education = vModel.Education;
            this.Income = vModel.Income;
            this.MartalStatus = vModel.MartalStatus;
            this.NationalID = vModel.NationalID;
            this.PrimaryLanguage = vModel.PrimaryLanguage;
            this.Religion = vModel.Religion;
            this.SecondaryLanguage = vModel.SecondaryLanguage;
            this.Sex = vModel.Sex;
        }

        [StringLength(2)]
        public string SecondaryLanguage { get; set; }
        [Required]
        public int EntityId { get; set; }
        [StringLength(2)]
        public string PrimaryLanguage { get; set; }
        //National Identifier aka ssn 
        public string NationalID { get; set; }
        public DateTime DOB { get; set; }
        [StringLength(2)]
        public string Sex { get; set; }
        //Martal Status
        [StringLength(2)]
        public string MartalStatus { get; set; }
        [StringLength(2)]
        public string Religion { get; set; }
        [StringLength(2)]
        public string Education { get; set; }
        //Income
        public decimal Income { get; set; }
        
        public void Update(EntityDemographicViewModel vModel)
        {
            if (!string.IsNullOrEmpty(vModel.NationalID))
                this.NationalID = vModel.NationalID;
            if (!string.IsNullOrEmpty(vModel.PrimaryLanguage))
                this.PrimaryLanguage = vModel.PrimaryLanguage;
            if (!string.IsNullOrEmpty(vModel.SecondaryLanguage))
                this.SecondaryLanguage = vModel.SecondaryLanguage;
            if (!string.IsNullOrEmpty(vModel.Sex))
                this.Sex = vModel.Sex;
            if (!string.IsNullOrEmpty(vModel.MartalStatus))
                this.MartalStatus = vModel.MartalStatus;
            if (!string.IsNullOrEmpty(vModel.Religion))
                this.Religion = vModel.Religion;
            if (vModel.Income > 0)
                this.Income = vModel.Income;
            this.DOB = vModel.DOB;
            this.EntityId = vModel.EntityId;
        }

        public static EntityDemographic CreateEntityDemographic(string nationalId)
        {
            var demographic = new EntityDemographic
            {
                NationalID = nationalId
            };

            return demographic;
        }
    }

}
