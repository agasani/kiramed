﻿// ReSharper disable once RedundantUsingDirective
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace api.Models
{
    [Table("AllergyReference")]
    public class AllergyReference:IEntity
    {
        public int Id { get; set; }
    }

}
