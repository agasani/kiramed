﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using api.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using api.Auth;
using AutoMapper.Configuration;
using Microsoft.EntityFrameworkCore.Metadata;

namespace api.Db
{
    public partial class KiraContext
    {
        public KiraContext(DbContextOptions<KiraContext> options) : base(options){}
        public DbSet<Entity> Entities { get; set; }
        //public DbSet<MedicationReference> MedReferences { get; set; }
        public DbSet<AllergyReference> AllergyReferences { get; set; }
        public DbSet<EntityContact> EntityContacts { get; set; }
        public DbSet<ContactType> EntityContactTypes { get; set; }
        public DbSet<EntityDemographic> Demographics { get; set; }
        public DbSet<SystemAction> SystemActions { get; set; }
        public DbSet<AgeRange> VitalSignAgeRanges { get; set; }
        public DbSet<LogEntity> Logs {get; set;}
        public DbSet<MetricsEntity> Metrics { get; set; }
        public DbSet<UserActionEntity> UserActions { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<EntityDemographic>()
                .HasAlternateKey(d => d.NationalID);

            modelBuilder.Entity<Organization>()
               .Property(p => p.ParentOrganizationId)
               .HasComputedColumnSql("[Id]");
            modelBuilder.Entity<VisitQueue>()
                .Property(p => p.QueueOrderNumber)
                .HasComputedColumnSql("[Id]");
            modelBuilder.Entity<Organization>()
              .HasAlternateKey(d => d.NationalId);

            modelBuilder.Entity<Transfer>()
                .HasOne(d => d.Patient)
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict);

        }
    }
    public class KiraContext<T>: IdentityDbContext<ApplicationUser>
    {
        public KiraContext(DbContextOptions<KiraContext<ApplicationUser>> options) : base(options) { }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<EntityDemographic>()
                .HasAlternateKey(d => d.NationalID);

            modelBuilder.Entity<Organization>()
               .Property(p => p.ParentOrganizationId)
               .HasComputedColumnSql("[Id]");

            modelBuilder.Entity<VisitQueue>()
                .Property(p => p.QueueOrderNumber)
                .HasComputedColumnSql("[Id]");

            modelBuilder.Entity<Organization>()
             .HasAlternateKey(d => d.NationalId);

            modelBuilder.Entity<Transfer>()
                .HasOne(d => d.Patient)
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict);


        }
    }
}
