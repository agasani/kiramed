﻿using System;
using System.Collections.Generic;
using System.Linq;
using api.Auth;
using api.Enums;
using api.Models;

namespace api.Db
{
    public class DbInitializer
    {
        public static void Init(KiraContext context, bool isTest = false)
        {
           //context.Database.EnsureDeleted();
           //context.Database.EnsureCreated();
 
            if (context.Entities.Any())
            {
                return;
            }
            
            //TODO create seed data here

            #region Country Db Initialization

            var country = new Country
            {
                Name = "Rwanda",
                Abbreviation = "RW",
                Code = "250",
                OfficialLanguage = "English",
                Currency = "RWF"
            };

            #endregion

            #region Patient Initialization

            var dem = new EntityDemographic {Sex = "M", DOB = new DateTime(1990, 1, 1), NationalID = "123456789"};
            var dem2 = new EntityDemographic {Sex = "M", DOB = new DateTime(1970, 12, 12), NationalID = "987654321"};
            var dem3 = new EntityDemographic {Sex = "F", DOB = new DateTime(1933, 6, 6), NationalID = "6748505453"};

            var addr = new EntityAddress
            {
                Country = "RW",
                Address1 = "Kamutwe Zone 1",
                City = "Byo",
                Label = "Home",
                District = "Muhoro"
            };
            var addr2 = new EntityAddress
            {
                Country = "TZ",
                Address1 = "Kamuduha Quartier 123",
                City = "Rugobe",
                Label = "Home",
                District = "Kagugu"
            };
            var addr3 = new EntityAddress
            {
                Country = "UG",
                Address1 = "Mbalaba 432",
                City = "Mbalala",
                Label = "Home",
                District = "South"
            };

            var entCont1 = new EntityContact {Value = "405-435-99999", ContactTypeId = 1};
            var entCont2 = new EntityContact {Value = "yohana@kiramed.com", ContactTypeId = 2 };

            var entCont3 = new EntityContact {Value = "405-435-5555", Label = "Phone", BeginDate = DateTime.Today, ContactTypeId = 1 };
            var entCont4 = new EntityContact { Value = "kamana@kiramed", ContactTypeId =  2 };

            var entCont5 = new EntityContact { Value = "078456634", ContactTypeId = 1 };
            var entCont6 = new EntityContact { Value = "kananga@kiramed.com", Label = "Email", BeginDate = DateTime.Today, ContactTypeId = 2 };


            var ent1 = new Entity
            {
                ExternalId = "ID123",
                Type = "P",
                Category = "P",
                Firstname = "Yohana",
                Lastname = "Matayo",
                Demographic = dem,
               // EntityDemographicId = dem.Id,
                Addresses = new List<EntityAddress> {addr},
                Contacts = new List<EntityContact> {entCont1, entCont2}
            };
            context.Entities.Add(ent1);
            var ent2 = new Entity
            {
                ExternalId = "ID806",
                Type = "P",
                Category = "P",
                Firstname = "Kamana",
                Lastname = "Kalisa",
                Demographic = dem2,
                //EntityDemographicId = dem2.Id,
                Addresses = new List<EntityAddress> {addr2},
                Contacts = new List<EntityContact> { entCont3, entCont4 }
            };
            context.Entities.Add(ent2);
            var ent3 = new Entity
            {
                ExternalId = "ID456",
                Type = "P",
                Category = "P",
                Firstname = "kananga",
                Lastname = "Siba",
                Demographic = dem3,
                //EntityDemographicId = dem3.Id,
                Addresses = new List<EntityAddress> {addr3},
                Contacts = new List<EntityContact> { entCont5, entCont6 }
            };
            context.Entities.Add(ent3);

          

            var pat1 = new Patient
            {
                MedicalRecordNumber = "345822940",
                AllergyNote = "The patient is allergic to beans farts.",
                Entity = ent1,
                NationalPatientId = "RW7757489331",
                TracknetNumber = "437434",
                CountryId =  1
            };
            context.Patients.Add(pat1);
            var pat2 = new Patient
            {
                MedicalRecordNumber = "999999999",
                AllergyNote = "The patient is allergic to not working.",
                Entity = ent2,
                NationalPatientId = "RW7757489339",
                TracknetNumber = "5647933",
                CountryId = 1
            };
            context.Patients.Add(pat2);
            var pat3 = new Patient
            {
                MedicalRecordNumber = "666666666",
                AllergyNote = "This patient has been deleted.",
                Entity = ent3,
                IsDeleted = DateTime.Now,
                NationalPatientId = "RW7757489330",
                TracknetNumber = "774893",
                CountryId = 1
            };
            context.Patients.Add(pat3);

            context.SaveChanges();

            var med1 = new Medication
            {
                MedicationName = "Oxicocin",
                MedicationNameId = 12,
                BeginDate = DateTime.Now,
                Classification = "Donno",
                OverTheCounter = true,
                OrderDate = DateTime.Now,
                Patient = pat1
            };
            context.Medications.Add(med1);
            var med2 = new Medication
            {
                MedicationName = "Palaxitopol",
                MedicationNameId = 13,
                Classification = "Donno",
                OverTheCounter = false,
                OrderDate = DateTime.Now,
                Patient = pat1 
            };
            context.Medications.Add(med2);

            dem.EntityId = ent1.Id;
            dem2.EntityId = ent2.Id;
            dem3.EntityId = ent3.Id;

            context.SaveChanges();


            #endregion

            #region Organization Db Initialization

            var orgCountry = country;
            context.Countries.Add(orgCountry);
            context.SaveChanges();

            string org1Id = Utils.IDGenerator.GenerateAlphaNumericID(10); //"a123ddff"; //
            string org2Id = Utils.IDGenerator.GenerateAlphaNumericID(10); // "c123ddff"; // 
            string org3Id = Utils.IDGenerator.GenerateAlphaNumericID(10); //"b123ddff"; //

            var orgAddr1 = new EntityAddress
            {
                Country = orgCountry.Abbreviation,
                Address1 = "Kamutwe Zone 1",
                City = "Kigali",
                Label = "Home",
                District = "Gasabo"
            };
            var orgAddr2 = new EntityAddress
            {
                Country = orgCountry.Abbreviation,
                Address1 = "Kamuduha Quartier 123",
                City = "Kigali",
                Label = "Home",
                District = "Gasabo"
            };
            var orgAddr3 = new EntityAddress
            {
                Country = orgCountry.Abbreviation,
                Address1 = "Mbalaba 432",
                City = "Kigali",
                Label = "Home",
                District = "Gasabo"
            };

            var orgDem1 = new EntityDemographic {Sex = "", DOB = new DateTime(1990, 1, 1), NationalID = org1Id};
            var orgDem2 = new EntityDemographic {Sex = "", DOB = new DateTime(1990, 1, 1), NationalID = org2Id};
            var orgDem3 = new EntityDemographic {Sex = "", DOB = new DateTime(1990, 1, 1), NationalID = org3Id};

            var org1Cont1 = new EntityContact {Value = "405-111-99999"};
            var org1Cont2 = new EntityContact {Value = "admin@chuk.com"};

            var org2Cont1 = new EntityContact {Value = "405-222-99999"};
            var org2Cont2 = new EntityContact {Value = "admin@kmh.com"};

            var org3Cont1 = new EntityContact {Value = "405-333-99999"};
            var org3Cont2 = new EntityContact {Value = "admin@dkh.com"};

            var orgEnt1 = new Entity
            {
                ExternalId = org1Id,
                Type = "O",
                Category = "P",
                Firstname = "CHUK",
                Lastname = "CHUK",
                Addresses = new List<EntityAddress> {orgAddr1},
                Contacts = new List<EntityContact> {org1Cont1, org1Cont2},
                Demographic = orgDem1
            };
            context.Entities.Add(orgEnt1);
            var orgEnt2 = new Entity
            {
                ExternalId = org2Id,
                Type = "O",
                Category = "P",
                Firstname = "KMH",
                Lastname = "KMH",
                Addresses = new List<EntityAddress> {orgAddr2},
                Contacts = new List<EntityContact> {org2Cont1, org2Cont2},
                Demographic = orgDem2
            };
            context.Entities.Add(orgEnt2);
            var orgEnt3 = new Entity
            {
                ExternalId = org3Id,
                Type = "O",
                Category = "P",
                Firstname = "DKH",
                Lastname = "DKH",
                Addresses = new List<EntityAddress> {orgAddr3},
                Contacts = new List<EntityContact> {org3Cont1, org3Cont2},
                Demographic = orgDem3
            };
            context.Entities.Add(orgEnt3);

          

            var org1 = new Organization
            {
                Name = orgEnt1.Lastname,
                OrganizationType = 1,
                ShortName = orgEnt1.Firstname,
               
                Entity = orgEnt1,
                CountryId = orgCountry.Id,
                NationalId = org1Id
            };
            //org1.Entity = orgEnt1;
            context.Organizations.Add(org1);
            var org2 = new Organization
            {
                Name = orgEnt2.Lastname,
                OrganizationType = 1,
                ShortName = orgEnt2.Firstname,
               
                Entity = orgEnt2,
                CountryId = orgCountry.Id,
                NationalId = org2Id
            };
            // org1.Entity = orgEnt2;
            context.Organizations.Add(org2);
            var org3 = new Organization
            {
                Name = orgEnt3.Lastname,
                OrganizationType = 1,
                ShortName = orgEnt3.Firstname,
               
                Entity = orgEnt3,
                CountryId = orgCountry.Id,
                NationalId = org3Id
            };
            //org1.Entity = orgEnt3;
            context.Organizations.Add(org3);
            context.SaveChanges();


            #endregion

            #region Visits db Initialization

            var visit1 = new Visit
            {
                IsPatientEnsured = true,
                IsFollowUp = false,
                IsReferral = false,
                OrganizationId = org1.Id,
                PatientId = pat1.Id,
                ReceptionistId = "",
                VisitDateTime = DateTime.Now,
                VisitReason = "Pregnancy"
            };
            context.Visits.Add(visit1);
            var visit2 = new Visit
            {
                IsPatientEnsured = true,
                IsFollowUp = false,
                IsReferral = false,
                OrganizationId = org2.Id,
                PatientId = pat2.Id,
                ReceptionistId = "",
                VisitDateTime = DateTime.Now,
                VisitReason = "Pregnancy"
            };
            context.Visits.Add(visit2);
            var visit3 = new Visit
            {
                IsPatientEnsured = false,
                IsFollowUp = false,
                IsReferral = false,
                OrganizationId = org3.Id,
                PatientId = pat1.Id,
                ReceptionistId = "",
                VisitDateTime = DateTime.Now,
                VisitReason = "Pregnancy"
            };
            context.Visits.Add(visit3);

            context.SaveChanges();

            #endregion

            #region Appointments Db Initializations

            var appoint1 = new Appointment()
            {
                DateTimeCreated = new DateTime(2017, 8, 4),
                IsDeleted = DateTime.MaxValue,
                ArrivalDateTime = new DateTime(2017,9,1),
                OrganizationId = 1,
                FirstName = "Joseph",
                LastName =  "Masengesho",
                Email = "Jmasengesho@inyarwanda.com",
                PhoneNumber =  "4056029804",
                NationalId = "22222222",
                VisitReasons = new List<VisitReason>{new VisitReason{RelatedKnownIssue = 10, Reason = "I been shitting on myself!"}},
                Relationship = ePatientRelationShip.Self              
            };

            var appoint2 = new Appointment()
            {
                DateTimeCreated = new DateTime(2017, 8, 1),
                IsDeleted = DateTime.MaxValue,
                ArrivalDateTime = new DateTime(2017, 9, 1),
                OrganizationId = 1,
                FirstName = "Kamana",
                LastName = "Tanazi",
                Email = "Jmasengesho@inyarwanda.com",
                PhoneNumber = "4056029804",
                NationalId =  "11111111",
                VisitReasons = new List<VisitReason> { new VisitReason { RelatedKnownIssue = 10, Reason = "My father cant remember my name" } },
                Relationship = ePatientRelationShip.Parent
            };

            context.Appointments.AddRange(new List<Appointment> {appoint1, appoint2});
            context.SaveChanges();
            #endregion

            #region System actions db Initializations

            var organizationActions = new List<SystemAction>
            {
                new SystemAction
                {
                    ActionName = "Organizations.Create",
                    ControllerName = "Organizations",
                    DateTimeCreated = DateTime.Now
                },
                new SystemAction
                {
                    ActionName = "Organizations.ViewAll",
                    ControllerName = "Organizations",
                    DateTimeCreated = DateTime.Now
                },
                new SystemAction
                {
                    ActionName = "Organizations.View",
                    ControllerName = "Organizations",
                    DateTimeCreated = DateTime.Now
                },
                new SystemAction
                {
                    ActionName = "Organizations.Delete",
                    ControllerName = "Organizations",
                    DateTimeCreated = DateTime.Now
                },
                new SystemAction
                {
                    ActionName = "Organizations.AddUser",
                    ControllerName = "Organizations",
                    DateTimeCreated = DateTime.Now
                }
            };

            context.SystemActions.AddRange(organizationActions);

            var userActions = new List<SystemAction>
            {
                new SystemAction
                {
                    ActionName = "Users.Create",
                    ControllerName = "Users",
                    DateTimeCreated = DateTime.Now
                },
                new SystemAction
                {
                    ActionName = "Users.Update",
                    ControllerName = "Users",
                    DateTimeCreated = DateTime.Now
                },
                new SystemAction
                {
                    ActionName = "User.AssignRole",
                    ControllerName = "Users",
                    DateTimeCreated = DateTime.Now
                }           
            };
            context.SystemActions.AddRange(userActions);
            context.SaveChanges();


            #endregion

            #region VisitQueue Db Initializations
            var visitQueue1 = new VisitQueue
            {
                OrganizationId = 1,
                FirstName = "Matayo",
                LastName = "Kalisa",
                ArrivalDateTime = DateTime.Now,
                VisitType = eVisitType.Walkin,
                Relationship = ePatientRelationShip.Self
            };

            var visitQueue2 = new VisitQueue
            {
                OrganizationId = 2,
                FirstName = "Dancilla",
                LastName = "Nyandwi",
                ArrivalDateTime = DateTime.Now.AddMinutes(10),
                VisitType = eVisitType.Walkin,
                Relationship = ePatientRelationShip.Child
            };

            var visitQueue3 = new VisitQueue
            {
                OrganizationId = 1,
                FirstName = "John",
                LastName = "Minani",
                ArrivalDateTime = DateTime.Now.AddHours(1),
                VisitType = eVisitType.Walkin,
                Relationship = ePatientRelationShip.Child
            };

            context.VisitQueue.AddRange(visitQueue1, visitQueue2, visitQueue3);

            context.SaveChanges();

            #endregion

            #region Transfer Db Initializations

            var transfer1 = new Transfer
            {
                NationalPatientId = "RW7757489339",
                ArrivalDateTime = DateTime.Now.AddHours(18),
                Relationship = ePatientRelationShip.Self,
                OrganizationId = 1,
                OriginOrganizationId = 2,
            };

            var transfer2 = new Transfer
            {
                NationalPatientId = "RW7757489339",
                ArrivalDateTime = DateTime.Now.AddHours(18),
                Relationship = ePatientRelationShip.Self,
                OrganizationId = 2,
                OriginOrganizationId = 1,
            };

            context.Transfers.AddRange(transfer2, transfer1);

          

            #endregion
             
            #region Encounter Db Initialization
              
            var encounter1 = new Encounter
            {
                Patient = pat1,
                OrganizationId = org1.Id,
                AdmittingPhysicianID = ""
            };
             var encounter2 = new Encounter
            {
                Patient = pat2,
                OrganizationId = org1.Id,
                AdmittingPhysicianID = ""
             };

             var encounter3 = new Encounter
            {
                Patient = pat1,
                OrganizationId = org1.Id,
                AdmittingPhysicianID = ""
             };

           context.Encounters.AddRange(encounter1, encounter2, encounter3);           
           context.SaveChanges();

            #endregion

            #region VitalSigns Db Initialization

            //Vital sign types
            var vitalSignType1 = new VitalSignType
            {
                Name = "Respiratory Rate",
                Description =  "Respiratory Rate",
                Unit = "Breaths/min"
            };

            var vitalSignType2 = new VitalSignType
            {
                Name = "Temperature",
                Description =  "Body temperature",
                Unit = "C"
            };

            var vitalSignType3 = new VitalSignType
            {
                Name = "Blood Pressure",
                Description =  "Blood Pressure",
                Unit = "mmh Hg"
            };

           context.VitalSignTypes.AddRange(vitalSignType1, vitalSignType2, vitalSignType3);
           context.SaveChanges();
            #endregion

            #region Vital Sign Measure Initializations
            
            var vitalSignMeasure1 = new VitalSignMeasure
            {
               VitalSignTypeId = 1,
               Type = vitalSignType1,
               AgeRange = eAgeUnits.Years,
               MinAge = 3,
               MaxAge = 9,
               MinValue = 20,
               MaxValue = 30
            };

            var vitalSignMeasure2 = new VitalSignMeasure
            {
               VitalSignTypeId = 1,
               Type = vitalSignType1,
               AgeRange = eAgeUnits.Years,
               MinAge = 10,
               MaxAge = 15,
               MinValue = 16,
               MaxValue = 22
            };

            var vitalSignMeasure3 = new VitalSignMeasure
            {
               VitalSignTypeId = 1,
               Type = vitalSignType1,
               AgeRange = eAgeUnits.Years,
               MinAge = 16,
               MaxAge = 90,
               MinValue = 15m,
               MaxValue = 20m
            };

             var vitalSignMeasure4 = new VitalSignMeasure
            {
               VitalSignTypeId = 2,
               Type = vitalSignType2,
               AgeRange = eAgeUnits.Years,
               MinAge = 3,
               MaxAge = 9,
               MinValue = 36.9m,
               MaxValue = 37.5m
            };

            var vitalSignMeasure5 = new VitalSignMeasure
            {
               VitalSignTypeId = 2,
               Type = vitalSignType2,
               AgeRange = eAgeUnits.Years,
               MinAge = 10,
               MaxAge = 15,
               MinValue = 36.4m,
               MaxValue = 37m
            };

            var vitalSignMeasure6 = new VitalSignMeasure
            {
               VitalSignTypeId = 2,
               Type = vitalSignType2,
               AgeRange = eAgeUnits.Years,
               MinAge = 16,
               MaxAge = 90,
               MinValue = 36.4m,
               MaxValue = 37.1m
            };

            context.VitalSignMeasures.AddRange(vitalSignMeasure1, vitalSignMeasure2, vitalSignMeasure3, vitalSignMeasure4, vitalSignMeasure5, vitalSignMeasure6);
            context.SaveChanges();

            #endregion

            #region Vital Signs Initializations
              
            var vitalSign1 = new VitalSign
            {
              VitalSignTypeId = 1,
              Type =  vitalSignType1,
              Encounter = encounter1,
              Value = 25,
            };

             var vitalSign2 = new VitalSign
            {
              VitalSignTypeId = 2,
              Type =  vitalSignType2,
              Encounter = encounter1,
              Value = 36,
            };

            context.VitalSigns.AddRange(vitalSign1, vitalSign2);
            context.SaveChanges();
            #endregion

            #region Diagnosis Db Initializations

            var diagnosis1 = new PatientDiagnosis
            {
                Encounter = encounter1,
                CreatedBy =  "",
                Description = "Diagnosis one",
                Type = eDiagnosisType.Admitting,
                Rank = 1,
                Code = "S90.572A",
                CodingVersion = eKiraCoding.ICD10,
                Name = "Other superficial bite of ankle left ankle initial encounter"

            };

            var diagnosis2 = new PatientDiagnosis
            {
                Encounter = encounter1,
                CreatedBy = "",
                Description = "Diagnosis two",
                Type = eDiagnosisType.Discharging,
                Rank = 2,
                Code = "S90572D",
                CodingVersion = eKiraCoding.ICD10,
                Name = "Other superficial bite of ankle left ankle subsequent encounter"
            };

            var diagnosis3 = new PatientDiagnosis
            {
                Encounter = encounter1,
                CreatedBy = "",
                Description = "Diagnosis three",
                Type = eDiagnosisType.Principle,
                Rank = 3,
                Code = "S90572S",
                CodingVersion = eKiraCoding.ICD10,
                Name = "Other superficial bite of ankle left ankle sequela"

            };

            context.PatientDiagnoses.AddRange(diagnosis1, diagnosis2, diagnosis3);
            context.SaveChanges();


            #endregion

            #region Add Diagnosis References  Initialization

            var diag1 = new Diagnosis
            {
                Code = "S90.572A",
                Version = eKiraCoding.ICD10,
                Name = "Other superficial bite of ankle left ankle initial encounter",
                CodeFullName = "S90.572A - Other superficial bite of ankle left ankle initial encounter ",
                Prefix = "S90"
            };

            var diag2 = new Diagnosis
            {
                Code = "S90572D",
                Version = eKiraCoding.ICD10,
                Name = "Other superficial bite of ankle left ankle subsequent encounter",
                CodeFullName = "S90.572D - Other superficial bite of ankle left ankle subsequent encounter",
                Prefix = "S90"
            };

            var diag3 = new Diagnosis
            {
                Code = "S90572S",
                Version = eKiraCoding.ICD10,
                Name = "Other superficial bite of ankle left ankle sequela",
                CodeFullName = "S90.572S - Other superficial bite of ankle left ankle sequela",
                Prefix = "S90"
            };

            context.Diagnoses.AddRange(diag1, diag2, diag3);

            context.SaveChanges();

            #endregion

        } 

        public static void DropAllTables(KiraContext context)
        {
            context.Database.EnsureDeleted();
            context.Database.EnsureCreated();
        }
        
    }
}
