﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using api.Db;
using api.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using api.Auth;
using Microsoft.AspNetCore.Identity;
using System.Net;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace api.Controllers
{
    [Route("api/[controller]")]
   
    public class KiraController : Controller
    {
        KiraContext _kiraContect;
        UserManager<ApplicationUser> _userManager;
        RoleManager<ApplicationRole> _roleManager;

        public KiraController(KiraContext kiraCtx, UserManager<ApplicationUser> userManager, RoleManager<ApplicationRole> roleManager)
        {
            this._kiraContect = kiraCtx;
            this._userManager = userManager;
            this._roleManager = roleManager;
        }

        // GET api/kira
        /// <summary>
        /// List for kira
        /// </summary>
        [HttpGet]        
        public async Task<IEnumerable<Patient>> Get()
        {
            var pats = await this._kiraContect.Patients.ToListAsync();
            return pats;
        }

        // GET api/kira/5
        /// <summary>
        /// Retrieve for kira by unique (integer) id
        /// </summary>
        [HttpGet("{id}")]
        [Authorize(Policy = "patient.level2")]
        public async Task<string> Get([FromRoute]int id)
        {
            //do async work here
            await Task.Delay(1);
            return "value";
        }

        // POST api/kira
        /// <summary>
        /// Register for kira
        /// </summary>
        [HttpPost]
        public async Task Post([FromBody]string value)
        {
            //do async work here
            await Task.Delay(1);
        }

        // PUT api/kira/5
        /// <summary>
        /// Update for kira by unique (integer) id
        /// </summary>
        [HttpPut("{id}")]
        public async Task Put([FromRoute]int id, [FromBody]string value)
        {
            //do async work here
            await Task.Delay(1);
        }

        // DELETE api/kira/5
        /// <summary>
        /// Delete for kira by unique (integer) id
        /// </summary>
        [HttpDelete("{id}")]
        public async Task Delete([FromRoute]int id)
        {
            //do async work here
            await Task.Delay(1);
        }
    }
}
