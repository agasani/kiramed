﻿using Microsoft.EntityFrameworkCore;
using api.Models;

namespace api.Db
{
    public partial class KiraContext : DbContext
    {
        public DbSet<Organization> Organizations { get; set; }
        public DbSet<Location> Locations { get; set; }
    }
}