﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api.Models
{
    public interface IOrganizationRepository : IKiraRepository<Organization>
    {
        Task<Organization> ExistsAsync(Organization organization);
        Task<IEnumerable<Organization>> LoadEntityAsync(IList<Organization> organizations);
    }
}
