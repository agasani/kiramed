﻿using api.Db;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using api.Services;
using api.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace api.Models
{
    public class OrganizationRepository : KiraRepository<Organization>, IOrganizationRepository
    {
        public readonly KiraContext _context;
        //public readonly IUserService _userService;

        public OrganizationRepository(KiraContext context) : base(context)
        {
            _context = context;
        }
       
        public override async Task<IEnumerable<Organization>> GetAllAsync()
        {
            var orgs = await base.GetAllAsync();       
            return await LoadEntityAsync(orgs.ToList());
        }

        public override async Task<IEnumerable<Organization>> GetAsync(Expression<Func<Organization, bool>> expression)
        {
            var orgs = await  base.GetAsync(expression);         
           return  await LoadEntityAsync(orgs.ToList());
        }

        public override async Task<Organization> FindAsync(int entityId)
        {
            var org = await base.FindAsync(entityId);
            var restult = await LoadEntityAsync(new List<Organization> { org });
            return restult.FirstOrDefault();
        }

        public Organization FindByNationalId(string nationlaId)
        {
            return _context.Organizations.FirstOrDefault(e => e.NationalId == nationlaId);
        }

        public override async Task<int> CreateAsync(Organization organization)
        {
            var country = await _context.Countries.FirstOrDefaultAsync(co => co.Id == organization.CountryId && co.IsDeleted > DateTime.Now);
            if (country == null)
            {
                return 0;
            }

            await _context.Organizations.AddAsync(organization);
            await _context.SaveChangesAsync();
            return organization.Id;                   
        }

        public async Task<Organization> ExistsAsync(Organization organization)
        {
            var org = await _context.Organizations.FirstOrDefaultAsync(o => o.NationalId == organization.NationalId
                                                                 || (o.ShortName == organization.ShortName && organization.Name == o.Name));

            return org;
        }

        public Organization Exists(Organization organization)
        {
            var org = _context.Organizations.FirstOrDefault(o => o.NationalId == organization.NationalId            
                || (o.ShortName == organization.ShortName && organization.Name == o.Name));

            return org;
        }

        public async Task<IEnumerable<Organization>> LoadEntityAsync(IList<Organization> orgs)
        {
            foreach (var org in orgs)
            {
                await _context.Entry(org).Reference(p => p.Entity).LoadAsync();
                await _context.Entry(org.Entity).Reference(e => e.Demographic).LoadAsync();
                await _context.Entry(org.Entity).Collection(e => e.Contacts).LoadAsync();
                await _context.Entry(org.Entity).Collection(e => e.Addresses).LoadAsync();
            }
            return orgs;
        }
    }
}
