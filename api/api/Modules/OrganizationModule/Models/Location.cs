﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace api.Models
{
    [Table("Location")]
    public class Location 
    {
        [Required]
        public int Id { get; set; }
        public int EntityId { get; set; }
        public Entity Entity { get; set; }
        public int CountryId { get; set; }
        [Required]
        [StringLength(100)]
        public string Name { get; set; }
        [Required]
        [StringLength(25)]
        public string ShortName { get; set; }
        [StringLength(10)]
        [Required]
        public string OrganizationNationalId { get; set; }
        public DateTime SetUpDate { get; set; } = DateTime.Now;
        public DateTime IsDeleted { get; set; } = DateTime.MaxValue;
        public bool IsParent { get; set; } = false;
        public int ParentOrganizationId { get; set; }
        public int OrganizationType { get; set;}

        public int OrganizationId { get; set; }
        //public Organization Organization { get; set; }

        public Location() { }
        private void SetParentOrganization()
        {
            this.ParentOrganizationId = OrganizationId;
        }
    }
}
