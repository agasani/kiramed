﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using api.ViewModels;
using api.Db;

namespace api.Models
{
    [Table("Organization")]
    public class Organization : KiraEntity
    {
        [Required]
        public Entity Entity { get; set; }
        public int CountryId { get; set; }
        [Required]
        [StringLength(100)]
        public string Name { get; set; }
        [Required]
        [StringLength(25)]
        public string ShortName { get; set; } 
        [StringLength(10)]
        [Required]
        public string NationalId { get; set; }
        public string OrgSystemId { get; set; }
        public int ParentOrganizationId { get; set; } 
        public int OrganizationType { get; set; }

        public Organization () { }
        public Organization(OrganizationViewModel vModel)
        {
            CountryId = vModel.CountryId;
            Name = vModel.Name;
            IsDeleted = vModel.IsDeleted;
            NationalId = vModel.NationalId;
            ShortName = vModel.ShortName;       
        }

        public Organization(OrganizationRegistrationViewModel vModel)
        {
            CountryId = vModel.CountryId;
            Name = vModel.SecondName;
            NationalId = vModel.NationalId;
            OrgSystemId = Utils.IDGenerator.GenerateAlphaNumericID(10);
            ShortName = vModel.FirstName;
        }

        public void Update(OrganizationViewModel vModel)
        {
            if (!string.IsNullOrEmpty(vModel.Name))
                this.Name = vModel.Name;

            if (!string.IsNullOrEmpty(vModel.ShortName))
                this.ShortName = vModel.ShortName;

            if (!string.IsNullOrEmpty(vModel.NationalId))
                this.NationalId = vModel.NationalId;


            //TODO make sure to update future configurations here 
            //Other configurations will come in the organizationViewModel or OrganizationConfigure          
        }
    }
}
