﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api.Models
{
    public interface IOrganization: IKiraEntity
    {
       // int EntityId { get; set; }
        Entity Entity { get; set; }
        int CountryId { get; set; }
        int ParentOrganizationId { get; }
        //public virtual ICollection<Location> Locations { get; set; }
        int OrganizationType { get; set; }
    }
}
