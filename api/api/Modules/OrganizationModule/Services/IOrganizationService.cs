﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using api.Models;
using api.Services;
using Castle.DynamicProxy.Generators.Emitters.SimpleAST;

namespace api.Services
{
    public interface IOrganizationService: IKiraService<Organization>
    {
        Task<IEnumerable<User>> GetOrganizationUsers(int orgId);
        Task<IEnumerable<VisitViewModel>> GetOrganizationVisits(Expression<Func<Visit, bool>> expression);
        Task<IEnumerable<PatientViewModel>> GetOrganizationPatientAsync(Expression<Func<Visit, bool>> expression);
        Task<IEnumerable<VisitViewModel>> GetOrganizationVisitsAsync(Expression<Func<Visit, bool>> expression);

    }
}
