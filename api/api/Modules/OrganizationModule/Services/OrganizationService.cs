﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using api.Models;
using api.Repositories;

namespace api.Services
{
    public class OrganizationService : KiraService<Organization>, IOrganizationService
    {
        private readonly IUserService _userService;
        private readonly IVisitService _visitService;
        private readonly IOrganizationRepository _organizationRepository;
        

        public OrganizationService(IOrganizationRepository organizationRepository, IUserService userService, IVisitService visitService) : base(organizationRepository)
        {
            _userService = userService;
            _visitService = visitService;
            _organizationRepository = organizationRepository;
        }

        public async Task<IEnumerable<VisitViewModel>> GetOrganizationVisitsAsync(Expression<Func<Visit, bool>> expression)
        {
            return await _visitService.GetOrganizationVisitsAsync(expression);
        }

        public async Task<IEnumerable<PatientViewModel>> GetOrganizationPatientAsync(Expression<Func<Visit, bool>> expression)
        {
            return await _visitService.GetOrganizationPatientsAsync(expression);
        }

        public async Task<IEnumerable<User>> GetOrganizationUsers(int orgId)
        {
            return await _userService.GetOrganizationUsersAsync(orgId);
        }

        public async Task<IEnumerable<VisitViewModel>> GetOrganizationVisits(Expression<Func<Visit, bool>> expression)
        {
            return await _visitService.GetOrganizationVisitsAsync(expression);
        }
    }
}
