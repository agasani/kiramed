﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api.Db;
using api.Models;
using api.Services;
using api.ViewModels;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Net;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace api.Controllers
{
    [Route("api/[controller]")]
    [EnableCors("default")]
    public class OrganizationController : Controller
    {
        KiraContext _kiraContext;

        readonly IOrganizationRepository _organizationRepository;
        readonly IOrganizationService _organizationService;

        public OrganizationController(IOrganizationRepository organizationRepository, KiraContext kiraContext, IOrganizationService organizationService)
        {
            _kiraContext = kiraContext;
            _organizationRepository = organizationRepository;
            _organizationService = organizationService; 
        }

        //this method will more likely be used by admins

        /// <summary>List all organizations</summary>
        /// <response code="400">Bad request</response>
        /// <response code="200">Successfully listed</response>
        [HttpGet]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, typeof(FeedBack))]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(IEnumerable<OrganizationViewModel>))]
        public async Task<IEnumerable<OrganizationViewModel>> Get()
        {
            var model = new List<OrganizationViewModel>();

            var organizations = await _organizationRepository.GetAllAsync();

            foreach (var org in organizations)
            {
                var vMod = new OrganizationViewModel(org);
                model.Add(vMod);
            }
            return model;
        }

        /// <summary>Get organization by unique (integer) orgId</summary>
        /// <response code="404">Model not found</response>
        /// <response code="200">Successfully retrieved</response>
        [HttpGet("Detail/{orgId}")]
        [SwaggerResponse((int)HttpStatusCode.NotFound, typeof(FeedBack))]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(OrganizationViewModel))]
        public async Task<IActionResult>Detail([FromRoute]int orgId)
        {
            var org = await _organizationRepository.FindAsync(orgId);
            if (org == null)
            {
                var feedback = new FeedBack { err = true, message = $"Organization with Org Id {orgId} does not exist.", data = "" };
                return NotFound(feedback);
            }

            var vMod = new OrganizationViewModel(org);
            return Ok(vMod);
        }

        // [HttpGet("Users/{organizationId}")]
        // public async Task<IActionResult> Users(int organizationId)
        // {
        //     var feedback = new FeedBack();
        //     var users =  await _organizationRepository.Users(organizationId);
        //     if(users != null) return OK(users);
        //     feedback.err = true;
        //     feedback.message = "No user was found";
        //     return NotFound(feedback);
        // }

        /// <summary>Get organization by unique (integer) nationalId</summary>
        /// <response code="404">Model not found</response>
        /// <response code="200">Successfully retrieved</response>
        [HttpGet("GetByNationalId/{nationalId}")]
        [SwaggerResponse((int)HttpStatusCode.NotFound, typeof(FeedBack))]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(OrganizationViewModel))]
        public async Task<IActionResult> GetByNationalId([FromRoute]string nationalId)
        {
            var result  = await _organizationRepository.GetAsync(o => o.NationalId == nationalId);
            var org = result.ToList();
            if (!org.Any())
            {
                var feedback = new FeedBack { err = true, message = $"Organization with {nationalId} does not exist.", data = "" };
                return NotFound(feedback);
            }

            var vMod = new OrganizationViewModel(org.FirstOrDefault());
            return Ok(vMod);
        }

        /// <summary>Get organization current patients by unique (integer) orgId</summary>
        /// <response code="404">Model not found</response>
        /// <response code="200">Successfully retrieved</response>
        [HttpGet("GetcurrentPatients/{orgId}")]
        [SwaggerResponse((int)HttpStatusCode.NotFound, typeof(FeedBack))]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(IEnumerable<VisitViewModel>))]
        public async Task<IActionResult> GetcurrentPatients([FromRoute]int orgId)
        {
            var patients  = await _organizationService.GetOrganizationVisitsAsync(v => v.OrganizationId == orgId && v.Status != eVisitStatus.Discharged);
            //var org = result.ToList();
            if (!patients.Any())
            {
                var feedback = new FeedBack { err = true, message = $"No current patients were found.", data = "" };
                return NotFound(feedback);
            }

            return Ok(patients);
        }

        /// <summary>Get organization waiting patients by unique (integer) orgId</summary>
        /// <response code="404">Model not found</response>
        /// <response code="200">Successfully retrieved</response>
        [HttpGet("GetWaitingPatients/{orgId}")]
        [SwaggerResponse((int)HttpStatusCode.NotFound, typeof(FeedBack))]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(IEnumerable<VisitViewModel>))]
        public async Task<IActionResult> GetWaitingPatients([FromRoute]int orgId)
        {
            var patients  = await _organizationService.GetOrganizationVisitsAsync(v => v.OrganizationId == orgId && v.Status == eVisitStatus.Waiting);
            //var org = result.ToList();
            if (!patients.Any())
            {
                var feedback = new FeedBack { err = true, message = $"No current patients were found.", data = "" };
                return NotFound(feedback);
            }

            return Ok(patients);
        }

        /// <summary>Get organization admitted patients by unique (integer) orgId</summary>
        /// <response code="404">Model not found</response>
        /// <response code="200">Successfully retrieved</response>
        [HttpGet("GetAdmittedPatients/{orgId}")]
        [SwaggerResponse((int)HttpStatusCode.NotFound, typeof(FeedBack))]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(IEnumerable<VisitViewModel>))]
        public async Task<IActionResult> GetAdmittedPatients([FromRoute]int orgId)
        {
            var patients  = await _organizationService.GetOrganizationVisitsAsync(v => v.OrganizationId == orgId && v.Status == eVisitStatus.Admitted);
            //var org = result.ToList();
            if (!patients.Any())
            {
                var feedback = new FeedBack { err = true, message = $"No current patients were found.", data = "" };
                return NotFound(feedback);
            }

            return Ok(patients);
        }

        /// <summary>Register a new organization</summary>
        /// <response code="400">Bad request</response>
        /// <response code="200">Successfully created</response>
        [HttpPost]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, typeof(FeedBack))]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(FeedBack))]
        public async Task<IActionResult> Post([FromBody]OrganizationRegistrationViewModel vModel)

        {
            var feedback = new FeedBack();

            if (!ModelState.IsValid)
            {
                feedback.err = true;
                feedback.message = "Invalid Model.";
                feedback.data = JsonConvert.SerializeObject(ModelState.Values.SelectMany(e => e.Errors).Select(x => x.ErrorMessage));
                return BadRequest(feedback);
            }

            var orgViewModel = vModel;
            var orgEntity = new Entity(orgViewModel);
            var newOrg = new Organization(orgViewModel);
            newOrg.Entity = orgEntity;

            var organization =  await _organizationRepository.ExistsAsync(newOrg);
            if (organization != null)
            {
                if (organization.IsDeleted > DateTime.Now)
                {
                    feedback.err = true;
                    feedback.message = "Organization already exists with similar information.";
                    feedback.data = JsonConvert.SerializeObject(new Dictionary<string, int> { { "OrganizationId", organization.Id } });
                    return BadRequest(feedback);
                }
            }

            var result = await _organizationRepository.CreateAsync(newOrg);
            if (result == 0)
            {
                feedback.err = true;
                feedback.message = "Organization cannot be created. The country might not exist.";
                return BadRequest(feedback);
            }
            feedback.err = false;
            feedback.message = "Organization created successfully";
            feedback.data = JsonConvert.SerializeObject(new Dictionary<string, int> { { "OrganizationId", newOrg.Id } });
            return Ok(feedback);
        }

        /// <summary>Update an organization</summary>
        /// <response code="400">Bad request</response>
        /// <response code="200">Successfully updated</response>
        [HttpPut("{id}")]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, typeof(FeedBack))]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(FeedBack))]
        public async Task<IActionResult> Put([FromRoute]int id, [FromBody]OrganizationViewModel vModel)
        {
            var feedback = new FeedBack();

            if (!ModelState.IsValid)
            {
                feedback.err = true;
                feedback.message = "Invalid Model.";
                feedback.data = JsonConvert.SerializeObject(ModelState.Values.SelectMany(e => e.Errors).Select(x => x.ErrorMessage));

                return BadRequest(feedback);
            }

            var org = await _organizationRepository.FindAsync(id);

            if (org == null)
            {
                feedback.err = true;
                feedback.message = "Invalid Request.";
                return BadRequest(feedback);
            }

            await _kiraContext.Entry(org).Reference(p => p.Entity).LoadAsync();
            await _kiraContext.Entry(org.Entity).Reference(e => e.Demographic).LoadAsync();
            await _kiraContext.Entry(org.Entity).Collection(e => e.Contacts).LoadAsync();
            await _kiraContext.Entry(org.Entity).Collection(e => e.Addresses).LoadAsync();

            //update the user here

            org.Update(vModel);

            await _organizationRepository.UpdateAsync(org);
            //do the update 

            feedback.err = false;
            feedback.message = "Organization info updated successfully";
            feedback.data = JsonConvert.SerializeObject(new Dictionary<string, int> { { "OrganizationId", id } });

            return Ok(feedback);
        }

        /// <summary>Delete an organization</summary>
        /// <response code="400">Bad request</response>
        /// <response code="200">Successfully deleted</response>
        [HttpDelete("{id}")]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, typeof(FeedBack))]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(FeedBack))]
        public async Task<IActionResult> Delete([FromRoute]int id)
        {
            var feedback = new FeedBack();
            var org = await _organizationRepository.FindAsync(id);

            if (org == null)
            {
                feedback.err = true;
                feedback.message = "Deleted!";
                return BadRequest(feedback);
            }

            org.IsDeleted = DateTime.Now;
            await _organizationRepository.UpdateAsync(org);

            // TODO Mark deleted all stuff related to this entity

            feedback.err = false;
            feedback.message = "Organization Successfully deleted";
            feedback.data = JsonConvert.SerializeObject(new Dictionary<string, int> { { "OrganizationId", org.Id } });

            return Ok(feedback);
        }
    }
}
