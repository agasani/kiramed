﻿using System.Collections.Generic;
using System.Linq;
using api.Models;
using Castle.Components.DictionaryAdapter;

namespace api.ViewModels
{
    public class OrganizationRegistrationViewModel : EntityRegistrationViewModel
    {
        //No additional fields or overrides
        public OrganizationRegistrationViewModel(Organization org)
        {
            CountryId = org.CountryId;
            FirstName = org.ShortName;
            SecondName = org.Name;
            ParentId = org.ParentOrganizationId;
            NationalId = org.NationalId;
            PhoneNumber = EntityHelper.GetEntityPhoneNumber(org.Entity);
            Email = EntityHelper.GetEntityEmail(org.Entity);

            var demographic = org.Entity.Demographic.NationalID != null
                ? org.Entity.Demographic
                : EntityDemographic.CreateEntityDemographic(NationalId);

            Demographic = new EntityDemographicViewModel(demographic);

            var contacts = org.Entity.Contacts.Count == 0
                ? EntityContact.CreateContacts(PhoneNumber, Email).ToList()
                : org.Entity.Contacts;
            foreach (var co in contacts)
            {
                Contacts.Add(new EntityContactViewModel(co));
            }

            if (org.Entity.Addresses.Count == 0) return;
            foreach (var addr in org.Entity.Addresses)
            {
                Addresses.Add(new EntityAddressViewModel(addr));
            }
        }

        public OrganizationRegistrationViewModel() { }

    }
}
