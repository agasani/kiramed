﻿using System;
using api.Models;
using System.ComponentModel.DataAnnotations;

namespace api.ViewModels
{
    public class OrganizationViewModel
    {
        public int Id { get; set; }
        public int EntityId { get; set; }
        public int CountryId { get; set; }
        [Required]
        [StringLength(100)]
        public string Name { get; set; }
        [Required]
        [StringLength(25)]
        public string ShortName { get; set; }
        public string NationalId { get; set; }
        public DateTime IsDeleted { get; set; }
        public DateTime DateTimeCreated { get; set; }
        public int ParentOrganizationId { get; set; }
        public string OrgSystemId { get; set; }
        public int SystemId { get; set; }
        

        public OrganizationViewModel() { }
        public OrganizationViewModel(Organization org)
        {
            Name = org.Name;
            ShortName = org.ShortName;
            ParentOrganizationId = org.ParentOrganizationId;
            NationalId = org.NationalId;
            IsDeleted = org.IsDeleted;
            CountryId = org.CountryId;
            DateTimeCreated = org.DateTimeCreated;
            Id = org.Id;
            OrgSystemId = org.OrgSystemId;
            EntityId = org.Entity != null ? org.Entity.Id : 0;
        }
    }
}
