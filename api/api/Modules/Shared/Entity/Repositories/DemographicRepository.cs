﻿

using api.Db;
using api.Models;

namespace api.Modules.Shared.Entity.Repositories
{
    public class DemographicRepository : KiraRepository<EntityDemographic>, IDemographicRepository
    {
        public DemographicRepository(KiraContext context) : base(context)
        {

        }
    }
}
