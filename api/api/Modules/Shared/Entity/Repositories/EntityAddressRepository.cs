﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api.Db;
using api.Models;
using api.Repositories;

namespace api.Repositories
{
    public class EntityAddressRepository : KiraRepository<EntityAddress>, IEntityAddressRepository
    {
        public EntityAddressRepository(KiraContext context) : base(context)
        {
        }
    }
}
