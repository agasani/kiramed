﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api.Db;
using api.Models;
using Microsoft.EntityFrameworkCore;

namespace api.Modules.Shared.Entity.Repositories
{
    public class EntityRepository : KiraRepository<Models.Entity>, IEntityRepository
    {
        private readonly KiraContext _kiraContext;

        public EntityRepository(KiraContext context) : base(context)
        {
            _kiraContext = context;
        }

        public override async Task<Models.Entity> FindAsync(int id)
        {
            var results = await _kiraContext.Entities.Where(e => e.Id == id && e.IsDeleted > DateTime.Now).Include(e => e.Demographic)
                .Include(e => e.Addresses).Include(e => e.Contacts).ToListAsync();

            return results.FirstOrDefault();
        }
    }
}
