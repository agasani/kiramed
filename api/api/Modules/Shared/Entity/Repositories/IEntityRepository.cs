﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api.Models;

namespace api.Modules.Shared.Entity.Repositories
{
    public interface IEntityRepository : IKiraRepository<Models.Entity>
    {

    }
}
