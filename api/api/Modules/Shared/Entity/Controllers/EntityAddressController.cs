﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api.Models;
using api.Repositories;
using api.Utils;
using api.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Net;

namespace api.Controllers
{
    [Route("api/[controller]")]
    [ControllerFilters.ValidateModelAttribute]
    public class EntityAddressController : Controller
    {
        private readonly IEntityAddressRepository _addressRepository;

        public EntityAddressController(IEntityAddressRepository addressRepository)
        {
            _addressRepository = addressRepository;
        }

        /// <summary>Get an entity address by unique (integer) id</summary>
        /// <response code="200">Successfully retrieved</response>
        /// <response code="404">Model not found</response>
        [HttpGet("GetEntityById/{id}")]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(IEnumerable<EntityAddressViewModel>))]
        [SwaggerResponse((int)HttpStatusCode.NotFound, typeof(FeedBack))]
        public async Task<IActionResult> GetEntityById([FromRoute]int id)
        {
            var result = await _addressRepository.FindAsync(id);
            var entity = new EntityAddressViewModel(result);
            if (result != null) return Ok(entity);
            var feedback = new FeedBack { err = true, message = $"Address with Id {id} does not exist.", data = "" };
            return NotFound(feedback);
        }

        /// <summary>Update an entity address by unique (integer) id</summary>
        /// <response code="404">Model not found</response>
        /// <response code="200">Successfully updated</response>
        [HttpPut("{id}")]
        [SwaggerResponse((int)HttpStatusCode.NotFound, typeof(FeedBack))]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(FeedBack))]
        public async Task<IActionResult> Put([FromRoute]int id, [FromBody]EntityAddressViewModel vModel)
        {
            var feedback = new FeedBack();

            var address = await _addressRepository.FindAsync(id);

            if (address == null)
            {
                feedback.err = true;
                feedback.message = "Invalid Request.";
                return BadRequest(feedback);
            }

            address.Update(vModel);

            await _addressRepository.UpdateAsync(address);

            feedback.err = false;
            feedback.message = "Address info updated successfully";
            feedback.data = JsonConvert.SerializeObject(new Dictionary<string, int> { { "Address", id } });

            return Ok(feedback);
        }
    }
}
