﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api.Models;
using api.Modules.Shared.Entity.Repositories;
using api.Utils;
using api.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Net;

namespace api.Controllers
{
    [Route("api/[controller]")]
    [ControllerFilters.ValidateModel]
    public class DemographicController : Controller
    {
        private readonly IDemographicRepository _demographicRepository;
        private readonly IEntityRepository _entityRepository;

        public DemographicController(IDemographicRepository demographicRepository, IEntityRepository entityRepository)
        {
            _demographicRepository = demographicRepository;
            _entityRepository = entityRepository;
        }

        /// <summary>Retrieve a demographic by unique (integer) id</summary>
        /// <response code="200">Successfully retrieved</response>
        /// <response code="404">Model not found</response>
        [HttpGet("GetById/{id}")]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(IEnumerable<EntityDemographicViewModel>))]
        [SwaggerResponse((int)HttpStatusCode.NotFound, typeof(FeedBack))]
        public async Task<IActionResult> GetById([FromRoute]int id)
        {
            var result = await _demographicRepository.FindAsync(id);
            var entity = new EntityDemographicViewModel(result);
            if (result != null) return Ok(entity);
            var feedback = new FeedBack { err = true, message = $"A demographic with Id {id} does not exist.", data = "" };
            return NotFound(feedback);
        }

        /// <summary>Update a demographic by unique (integer) id</summary>
        /// <response code="400">Bad request</response>
        /// <response code="200">Model found</response>
        [HttpPut("{id}")]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, typeof(FeedBack))]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(FeedBack))]
        public async Task<IActionResult> Put([FromRoute]int id,[FromBody]EntityDemographicViewModel vModel)
        {
            var feedback = new FeedBack();

            var demographic = await _demographicRepository.FindAsync(id);

            if (demographic == null)
            {
                feedback.err = true;
                feedback.message = "Invalid Request.";
                return BadRequest(feedback);
            }

            demographic.Update(vModel);

            await _demographicRepository.UpdateAsync(demographic);

            feedback.err = false;
            feedback.message = "Address info updated successfully";
            feedback.data = JsonConvert.SerializeObject(new Dictionary<string, int> { { "Address", id } });

            return Ok(feedback);
        }

        /// <summary>Register a new demographic</summary>
        /// <response code="400">Bad request</response>
        /// <response code="200">Model found</response>
        [HttpPost]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, typeof(FeedBack))]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(FeedBack))]
        public async Task<IActionResult> Post([FromBody]EntityDemographicViewModel vModel)
        {
            var feedback = new FeedBack();

            var newDemographic = new EntityDemographic(vModel);

            var entity = await _entityRepository.FindAsync(vModel.EntityId);
            if (entity == null)
            {

                feedback.err = true;
                feedback.message = "Entity does not exist.";
                return BadRequest(feedback);
            }
         
            var result = await _demographicRepository.CreateAsync(newDemographic);

            if (result != 0)
            {
                feedback.err = false;
                feedback.message = eFeedbackMessages.ObjectCreatedSuccessfully.GetDescription();
                feedback.data = JsonConvert.SerializeObject(new Dictionary<string, int> { { "Id", result } });
                return Ok(feedback);
            }
            feedback.err = true;
            feedback.message = "Creating the demographic failed";
            return BadRequest(feedback);

        }
    }
}
