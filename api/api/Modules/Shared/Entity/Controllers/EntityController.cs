﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api.Models;
using api.Modules.Shared.Entity.Repositories;
using api.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Net;

namespace api.Controllers
{
    [Route("api/[controller]")]
    public class EntityController : Controller
    {
        private readonly IEntityRepository _entityRepository;

        public EntityController(IEntityRepository entityRepository)
        {
            _entityRepository = entityRepository;
        }

        /// <summary>Retrieve an entity by unique (integer) id</summary>
        /// <response code="200">Successfully retrieved</response>
        /// <response code="404">Model not found</response>
        [HttpGet("GetEntityById/{id}")]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(EntityViewModel))]
        [SwaggerResponse((int)HttpStatusCode.NotFound, typeof(FeedBack))]
        public async Task<IActionResult> GetEntityById([FromRoute]int id)
        {
            var result = await _entityRepository.FindAsync(id);
            var entity = new EntityViewModel(result);
            if (result != null) return Ok(entity);
            var feedback = new FeedBack {err = true, message = $"Entity with Id {id} does not exist.", data = ""};
            return NotFound(feedback);
        }
    }
}
