﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api.Models
{
    public class CarePlan
    {
       public int Id { get; set; }
       public  string PlanName { get; set; }
       public eFrenquency VisitFrenquency { get; set; }
       public int NumberOfVisits { get; set; }
       public string VisitReminderMessage { get; set; }
    }
}
