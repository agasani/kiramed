﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace api.Models
{
    [Table("AdmissionFacility")]
    public class AdmissionFacility: IEntity
    {
        public int Id { get; set; }
        [Required]
        public int AdmissionId { get; set; }
        public Admission Admission { get; set; }
        //Etc
    }
}
