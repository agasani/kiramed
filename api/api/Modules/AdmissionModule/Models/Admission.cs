﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace api.Models
{
    //Admission
    [Table("Admission")]
    public class Admission: IEntity
    {
        public int Id { get; set; }
        //Admission Flag
        public bool IsAdmission { get; set; }
        //Case Open Date
        public DateTime CaseOpenDate { get; set; } = DateTime.Now;
        public bool IsAdmitted { get; set; }
        //Admission condition
        [StringLength(2)]
        public string AdmissionCondition { get; set; }
        //Level of Care condition
        [StringLength(2)]
        public string LevelOfCare { get; set; }
        //Acuity
        [StringLength(2)]
        public string Acuity { get; set; }
        //Reason not admitted
        [StringLength(2)]
        public string ReasonNotAdmitted { get; set; }
        //Planned discharde date
        public DateTime PlannedDischargeDate { get; set; }
        //Discharge Condition
        [StringLength(2)]
        public string DischargeCondition { get; set; }
        //Discharge Disposition
        [StringLength(2)]
        public string DischargeDisposition { get; set; }
        //Discharge Reason
        [StringLength(2)]
        public string DischargeReason { get; set; }
        //Reason Not admitted Date
        public DateTime ReasonNotAdmittedDate { get; set; }

    }

}
