﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using api.Models;

namespace api.Db
{
    public partial class KiraContext : DbContext
    {
        public DbSet<Admission> Admissions { get; set; }
        public DbSet<AdmissionDiagnosis> AdmissionDiagnoses { get; set; }
        public DbSet<AdmissionDiagnosisDetail> AdmissionDiagnosisDetails { get; set; }
        public DbSet<AdmissionFacility> AdmissionFacility { get; set; }
    }
}
