﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api.Db;
using api.Models;
using api.Services;
using Microsoft.AspNetCore.Mvc;
using Nest;
using Newtonsoft.Json;
using System.Net;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace api.Controllers
{
    [Route("api/[controller]")]
    public class PatientController : Controller
    {
       // KiraContext _context;
        private readonly IPatientService _patientService;

        private readonly ICountryRepository _countryRepository;
        //private read only IElasticClient _elasticClient;
      
        public PatientController(IPatientService patientService, ICountryRepository countryRepository = null)
        {
            //_context = patCtx;
            _patientService = patientService;
            _countryRepository = countryRepository;
            //_elasticClient = elasticClient;

        }

        /// <summary>List all patients</summary>
        /// <response code="404">Model not found</response>
        /// <response code="200">Successfully listed</response>
        [HttpGet]
        [SwaggerResponse((int)HttpStatusCode.NotFound, typeof(FeedBack))]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(IEnumerable<PatientViewModel>))]
        public async Task<IActionResult> Get()
        {
            var feedback = new FeedBack();

            var results = await _patientService.GetAllAsync();
            var pats = results.ToList();
            if (pats.Any())
            {
                var patients = pats.Select(pat => new PatientViewModel(pat)).ToList();
                return Ok(patients);
            }

            feedback.err = false;
            feedback.message = "No patients were found";
            return NotFound(feedback);
        }

        /// <summary>Get patient info by (integer) id</summary>
        /// <response code="404">Model not found</response>
        /// <response code="200">Successfully listed</response>
        [HttpGet("Details/{id}")]
        [SwaggerResponse((int)HttpStatusCode.NotFound, typeof(FeedBack))]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(IEnumerable<PatientViewModel>))]
        public async Task<IActionResult> Details([FromRoute]int id)
        {
            var result = await _patientService.FindAsync(id);
            var pat = new PatientViewModel(result);
            if (result != null) return Ok(pat);
            var feedback = new FeedBack { err = true, message = $"Patient with Id {id} does not exist.", data = "" };
            return NotFound(feedback);
        }

        /// <summary>Register a new patient</summary>
        /// <response code="400">Bad request</response>
        /// <response code="200">Successfully created</response>
        [HttpPost]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, typeof(FeedBack))]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(FeedBack))]
        public async Task<IActionResult> Post([FromBody] PatientRegistrationViewModel vModel)
        {
            var feedback = new FeedBack();

            if (!ModelState.IsValid)
            {
                feedback.err = true;
                feedback.message = "Invalid Model.";
                feedback.data = JsonConvert.SerializeObject(ModelState.Values.SelectMany(e => e.Errors).Select(x => x.ErrorMessage));

                return BadRequest(feedback);
            }

            //check if the country exists
            var country =  await _countryRepository.FindAsync(vModel.CountryId);
            if (country == null)
            {
                feedback.err = true;
                feedback.message = "The country does not exist.";
                feedback.data = ModelState.Values.SelectMany(e => e.Errors).ToString();
                return BadRequest(feedback);
            }

            var newPat = new Patient(vModel) {Entity = new Entity(vModel)};
            //check if the patient exists
            var existingPatient = await _patientService.ExistsAsync(newPat);
            if (existingPatient != null)
            {
                if (existingPatient.IsDeleted > DateTime.Now)
                {
                    feedback.err = true;
                    feedback.message = "Patient already exists.";
                    feedback.data =
                        JsonConvert.SerializeObject(new Dictionary<string, int> {{"PatientId", existingPatient.Id}});                   
                    return BadRequest(feedback);
                }  

                //For now just update the entity              
                existingPatient.IsDeleted = DateTime.MaxValue;
                existingPatient.Entity.Update(vModel);
                var updateResult = await _patientService.UpdateAsync(existingPatient);
                feedback.err = false;
                feedback.message = "Patient was updated.";
                feedback.data =
                    JsonConvert.SerializeObject(new Dictionary<string, int> { { "PatientId", existingPatient.Id } });              
                return Ok(feedback);
            }

            var result = await _patientService.CreateAsync(newPat);
            if (result != 0)
            {
                //var search = new KiraPatientSearchRepository();
                //await search.AddPatientDocument(newPat);

               feedback.err = false;
               feedback.message = "Patient successfully created.";
               feedback.data = JsonConvert.SerializeObject(new Dictionary<string, int> {{"PatientId", result}});
               return Ok(feedback);
            }

            feedback.err = true;
            feedback.message = "Error creating the patient!";
            return BadRequest(feedback);
        }

        /// <summary>Update a patient</summary>
        /// <response code="400">Bad request</response>
        /// <response code="200">Successfully updated</response>
        [HttpPut("{id}")]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, typeof(FeedBack))]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(FeedBack))]
        public async Task<IActionResult> Put([FromRoute]int id, [FromBody] PatientRegistrationViewModel vModel)  //not the registration
        {
            var feedback = new FeedBack();

            if (!ModelState.IsValid)
            {
                feedback.err = true;
                feedback.message = "Invalid Model.";
                feedback.data = ModelState.Values.SelectMany(e => e.Errors).ToString();

                return BadRequest(feedback);
            }

            var pat = _patientService.GetAsync(p => p.Id == id).Result.FirstOrDefault();

            if (pat == null)
            {
                feedback.err = true;
                feedback.message = "Not patient was found.";
                return BadRequest(feedback);
            }

            pat.Update(vModel);
            pat.Entity.Update(vModel);

            await _patientService.UpdateAsync(pat);

            feedback.err = false;
            feedback.message = "Patient info updated successfully";
            feedback.data = JsonConvert.SerializeObject(new Dictionary<string, int> { { "PatientId", id } });

            return Ok(feedback);
        }

        /// <summary>Delete a patient</summary>
        /// <response code="400">Bad request</response>
        /// <response code="200">Successfully deleted</response>
        [HttpDelete("{id}")]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, typeof(FeedBack))]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(FeedBack))]
        public async Task<IActionResult> Delete([FromRoute]int id)
        {
            var feedback = new FeedBack();
            var pat = await _patientService.FindAsync(id);
            if (pat == null)
            {
                feedback.err = true;
                feedback.message = "Patient does not exist";
                return BadRequest(feedback);
            }

            pat.IsDeleted = DateTime.Now;
            await _patientService.UpdateAsync(pat);

            feedback.err = false;
            feedback.message = "Patient Successfully updated";
            feedback.data = JsonConvert.SerializeObject(new Dictionary<string, int> { { "PatientId", pat.Id } });

            return Ok(feedback);
        }

        //[HttpGet("Search/{search}")]
        //public async Task<IActionResult> Search([FromRoute]string search)
        //{
        //    var feedback = new FeedBack();

        //    if (string.IsNullOrWhiteSpace(search))
        //    {
        //        feedback.err = true;
        //        feedback.message = $"Bad request.";
        //        feedback.data = "";
        //        return BadRequest(feedback);
        //    }
        //    if (search.Length > 50)
        //    {
        //        feedback.err = true;
        //        feedback.message = "The search query is very long. No results found.";
        //        feedback.data = "";
        //        return BadRequest(feedback);
        //    }

        //    var results = await _patientService.Search(search);

        //    if (!results.Any())
        //    {
        //        feedback.err = true;
        //        feedback.message = "No results were found";
        //        return BadRequest(feedback);
        //    }
        //    return OK(results);
        //}
    }

}
