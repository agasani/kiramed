﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using api.Models;
using api.Db;
using Microsoft.EntityFrameworkCore;

namespace api.Modules.PatientModule.Controllers
{
    [Route("api/patients/[controller]")]
    public class MedController
    {
        KiraContext _kiraContect;

        public MedController(KiraContext kiraCtx)
        {
            this._kiraContect = kiraCtx;
        }

        /// <summary>
        /// List for med
        /// </summary>
        [HttpGet]
        public async Task<IEnumerable<string>> Get()
        {
            //do async work here
            await Task.Delay(1);
            return new string[] { "Meds1", "Meds2" };
        }

        /// <summary>
        /// Retrieve for med by unique (integer) id
        /// </summary>
        [HttpGet("{id}")]
        public async Task<string> Get([FromRoute]int id)
        {
            //do async work here
            await Task.Delay(1);
            return "value";
        }

        /// <summary>
        /// Register for med
        /// </summary>
        [HttpPost]
        public async Task Post([FromBody]string value)
        {
            //do async work here
            await Task.Delay(1);
        }

        /// <summary>
        /// Update for med by unique (integer) id
        /// </summary>
        [HttpPut("{id}")]
        public async Task Put([FromRoute]int id, [FromBody]string value)
        {
            //do async work here
            await Task.Delay(1);
        }

        /// <summary>
        /// Delete for med by unique (integer) id
        /// </summary>
        [HttpDelete("{id}")]
        public async Task Delete([FromRoute]int id)
        {
            //do async work here
            await Task.Delay(1);
        }
    }

}
