﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api.Models;

namespace api.Repositories
{
   public  interface IPatientRepository : IIdentifiableRepository<Patient>, IKiraRepository<Patient>
    {
        Task<Patient> FindByMedicalRecordNumber(string recordNumber);
        Task<IEnumerable<Patient>> GetByOrganizationAsync(int organizationId);
        Task<IEnumerable<Patient>> GetByUserAsync(string userId);
        Task<Patient> ExistsAsync(Patient pat);

    }
}
