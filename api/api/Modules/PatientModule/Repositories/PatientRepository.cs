﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using api.Db;
using api.Models;
using api.Repositories;
using api.ViewModels;
using Microsoft.EntityFrameworkCore;

namespace api.Repositories
{
    public class PatientRepository :  KiraRepository<Patient>, IPatientRepository
    { 
        public readonly KiraContext _context;

        public PatientRepository (KiraContext context) : base(context)
        {
            _context = context;
        }
       
        public override async Task<IEnumerable<Patient>> GetAllAsync()
        {
            var patients = await _context.Patients.Include(d => d.Entity).ThenInclude(e => e.Contacts).ToListAsync();

            foreach (var pat in patients)
            {
                await _context.Entry(pat).Reference(p => p.Entity).LoadAsync();
                await _context.Entry(pat.Entity).Reference(e => e.Demographic).LoadAsync();             
                await _context.Entry(pat.Entity).Collection(e => e.Contacts).LoadAsync();             
                await _context.Entry(pat.Entity).Collection(e => e.Addresses).LoadAsync();
            }
            return patients;
        }

        public override async Task<IEnumerable<Patient>> GetAsync(Expression<Func<Patient, bool>> expression)
        {
            var patients = await _context.Patients.Include(d => d.Entity).ThenInclude(e => e.Contacts).Where(expression).ToListAsync();

            patients = patients.Where(p => p.IsDeleted > DateTime.Now).ToList();
            
            foreach (var pat in patients)
            {
                await _context.Entry(pat).Reference(p => p.Entity).LoadAsync();
                await _context.Entry(pat.Entity).Reference(e => e.Demographic).LoadAsync();
                await _context.Entry(pat.Entity).Collection(e => e.Contacts).LoadAsync();
                await _context.Entry(pat.Entity).Collection(e => e.Addresses).LoadAsync();
            }
            return patients;
        }

        public override async Task<Patient> FindAsync(int patientId)
        {
            var pat = _context.Patients.Find(patientId);
            if (pat == null || pat.IsDeleted < DateTime.Now) return null;
            await _context.Entry(pat).Reference(p => p.Entity).LoadAsync();
            await _context.Entry(pat.Entity).Reference(e => e.Demographic).LoadAsync();
            await _context.Entry(pat.Entity).Collection(e => e.Contacts).LoadAsync();
            await _context.Entry(pat.Entity).Collection(e => e.Addresses).LoadAsync();
            
            return pat;
        }

        private async Task<Patient> FindWithDeletesAsync(int patientId)
        {
            var pat = _context.Patients.Find(patientId);
            if (pat == null) return null;
            await _context.Entry(pat).Reference(p => p.Entity).LoadAsync();
            await _context.Entry(pat.Entity).Reference(e => e.Demographic).LoadAsync();
            await _context.Entry(pat.Entity).Collection(e => e.Contacts).LoadAsync();
            await _context.Entry(pat.Entity).Collection(e => e.Addresses).LoadAsync();

            return pat;
        }

        public async Task<Patient> FindByPhoneNumberAsync(string phoneNumber)
        {
            var patient = await GetAsync(p => p.Entity.Contacts.Any(c => c.Value == phoneNumber));
            return patient.FirstOrDefault();
        }

        public async Task<Patient> FindByEmailAsync(string email)
        {
            var patient = await GetAsync(p => p.Entity.Contacts.Any(c => c.Value == email));
            return patient.FirstOrDefault();
        }

        public async Task<Patient> FindByNationalIdAsync(string nationlaId)
        {
            var patient = await GetAsync(p => p.Entity.Demographic.NationalID == nationlaId);
            return patient.FirstOrDefault();
        }

        public async Task<Patient> FindByFullNameAsync(string [] names)
        {
            var patient = await GetAsync(p => p.Entity.Firstname == names[0] && p.Entity.Lastname == names[1]);
            return patient.FirstOrDefault();
        }

        public Patient FindByPhoneNumber(string phoneNumber)
        {
            throw new NotImplementedException();
        }

        public Patient FindByEmail(string email)
        {
            throw new NotImplementedException();
        }

        public Patient FindByNationalId(string nationlaId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Patient> FindByFullName(string fName, string lName)
        {
            throw new NotImplementedException();
        }
    
  
        public async Task<Patient> FindByMedicalRecordNumber(string recordNumber)
        {
            var patient = await GetAsync(p => p.MedicalRecordNumber == recordNumber);
            return patient.FirstOrDefault();
        }

        public async Task<Patient> ExistsAsync(Patient pat)
        {
            var patientEntity = _context.Entities.FirstOrDefault(e => e.ExternalId == pat.Entity.ExternalId
                                                                      || (e.Demographic.NationalID == pat.Entity.Demographic.NationalID)
                                                                      || (e.Firstname == pat.Entity.Firstname && e.Lastname == pat.Entity.Lastname)
                                                                      || (e.Contacts.Any(c => (ContactTypes)c.ContactTypeId == ContactTypes.Email && c.Value == pat.Entity.Contacts.FirstOrDefault(r => (ContactTypes)r.ContactTypeId == ContactTypes.Email).Value))
                                                                      || (e.Contacts.Any(c => (ContactTypes)c.ContactTypeId == ContactTypes.Phone && c.Value == pat.Entity.Contacts.FirstOrDefault(r => (ContactTypes)r.ContactTypeId == ContactTypes.Phone).Value)));

            if (patientEntity == null) return null;
            return await FindWithDeletesAsync(patientEntity.Id);       
        }

        public Task<Patient> FindBySystemIdAsync(string systemId)
        {
            throw new NotImplementedException();
        }

        public Patient FindBySystemId(string systemId)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<Patient>> GetByOrganizationAsync(int organizationId)
        {

            //TODO Implement a logic to return current organization patients: Might patients who have open encounter at this organization
            throw new NotImplementedException();
        }

        public Task<IEnumerable<Patient>> GetByUserAsync(string userId)
        {
            //TODO Implement a logic to return patients who are being treated by a given nurse or doctor
            throw new NotImplementedException();
        }
    }
}
