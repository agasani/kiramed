﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using api.Db;
using api.Models;
using api.Repositories;

namespace api.Services
{
public class PatientService : PatientRepository, IPatientService
{
    private readonly IPatientRepository _patientRepository;

    public PatientService(KiraContext context, IPatientRepository patientRepository) : base(context)
    {
        _patientRepository = patientRepository;
    }

    public Task<List<PatientViewModel>> Get(int countryId)
    {
        throw new NotImplementedException();
    }

   public override async Task<Patient> FindAsync(int id)
    {
            return await _patientRepository.FindAsync(id);
    }
   
    public Task<List<PatientViewModel>> GetOrganizationPatients(int organizationId)
    {
        throw new NotImplementedException();
    }

    public Task<List<PatientViewModel>> GetUserPatients(int userId)
    {
        throw new NotImplementedException();
    }

    public override async Task<IEnumerable<Patient>> GetAllAsync()
    {
        return await _patientRepository.GetAllAsync();
    }

    public async Task<IEnumerable<PatientSearchViewModel>> Search(string searchInput)
    {
        var _searchResult = new List<PatientSearchViewModel>();

        int minStringLen = 3;
        int maxStringLen = 50; //this can be checked in Javascript 

        if (string.IsNullOrWhiteSpace(searchInput))
        {
            return _searchResult;
        }
        if (searchInput.Length > maxStringLen)
        {
            return _searchResult;
        }

        var search = Regex.Replace(searchInput, @"[^\w\@\-\\\/]", "", RegexOptions.None);

        //if its an email, save yourself time
        bool isEmail = (Regex.IsMatch(search,
            @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
            @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
            RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250)));

        //if its an all chars, save yourself time, it is more likely a first name or last name 
        bool isAllChars = search.All(Char.IsLetter);

        //if its an date, save yourself time
        if (DateTime.TryParse(search, out DateTime dob))
        {
            var dateSearchResult = SearchDate(dob).Result.ToList();
            if (dateSearchResult.Any())
            {
                _searchResult.AddRange(dateSearchResult);
            }
        }
        else if (isEmail)
        {
            var emailResult = SearchEmail(search).Result.ToList();
            if (emailResult.Any())
            {
                _searchResult.AddRange(emailResult);
            }
        }
        else if (isAllChars && search.Length >= minStringLen)
        {
            var nameResults = SearchNames(search).Result.ToList();
            if (nameResults.Any())
            {
                _searchResult.AddRange(nameResults);
            }

            var alphaNumericResults =
                SearchAlphaNumericIDs(search).Result.ToList(); //all string can be part of an alpha numeric field
            if (alphaNumericResults.Any())
            {
                _searchResult.AddRange(alphaNumericResults);
            }

            var emailResults = SearchEmail(search).Result.ToList(); //all string can be part of an email
            if (emailResults.Any())
            {
                _searchResult.AddRange(emailResults);
            }
        }
        else if (int.TryParse(search, out int num) ) //if its an int, save yourself time, it is more likely an id or entityID 
        {
            var idRestuls = SearchNumericIDs(num).Result.ToList();
            if (idRestuls.Any())
            {
                _searchResult.AddRange(idRestuls);
            }
            if (search.Length >= minStringLen)
            {
                var alphaNumericResults = SearchAlphaNumericIDs(search).Result.ToList();
                if (alphaNumericResults.Any())
                {
                    _searchResult.AddRange(alphaNumericResults);
                }

                var phoneNumbercResults = SearchPhoneNumber(search).Result.ToList(); //a phone number can look like an int
                if (phoneNumbercResults.Any())
                {
                    _searchResult.AddRange(phoneNumbercResults);
                }
            }
        }
        else
        {
            if (search.Length >= minStringLen)
            {
                var cleanPhoneNumber = search.Replace("-", "").Replace(".", "");
                var isPhoneNumber = cleanPhoneNumber.All(char.IsDigit);
                if (isPhoneNumber
                ) //if it is all digits, it can only be a cell phone: dont search phone will char stuff
                {
                    _searchResult.AddRange(await SearchPhoneNumber(cleanPhoneNumber));
                }
                _searchResult.AddRange(await SearchAlphaNumericIDs(search));
            }

        }

        return _searchResult.Distinct(); //return results if found
    }

    #region Search Helpers
    //can search all string patient fields (now search names and external ID ()
    private async Task<IEnumerable<PatientSearchViewModel>> SearchNames(string search)
    {
        search = search.ToLower();
        var results = new List<PatientSearchViewModel>();

        var patients =
            await _patientRepository.GetAsync(p => p.Entity.Firstname.ToLower().Contains(search) ||
                                                    p.Entity.Lastname.ToLower().Contains(search) ||
                                                    p.Entity.ExternalId.ToLower().Contains(search));

        foreach (var pat in patients)
        {
            results.Add(new PatientSearchViewModel(pat));
        }
        return results;
    } 

    //can search all alpha numerical patient fields (now ID and EntityId)
    private async Task<IEnumerable<PatientSearchViewModel>> SearchNumericIDs(int search)
    {
        var results = new List<PatientSearchViewModel>();
        var patients = await _patientRepository.GetAsync(p => p.EntityId == search);
        foreach (var pat in patients)
        {
            results.Add(new PatientSearchViewModel(pat));
        }
        return results;
    }

    //can search all patient dates (now: just DOB)
    private async Task<IEnumerable<PatientSearchViewModel>> SearchDate(DateTime date)
    {
        var results = new List<PatientSearchViewModel>();
        var patients = await _patientRepository.GetAsync(p => p.Entity.Demographic.DOB == date);
        foreach (var pat in patients)
        {
            results.Add(new PatientSearchViewModel(pat));
        }
        return results;
    }

    //can search alpha numeric fields (now: Medical Record Number and National Patient ID, and TrackNet Number, and patient National ID) 
    private async Task<IEnumerable<PatientSearchViewModel>> SearchAlphaNumericIDs(string search)
    {
        search = search.ToLower();
        var results = new List<PatientSearchViewModel>();

        //search patient table
        var patients =
            await _patientRepository.GetAsync(p => p.MedicalRecordNumber.ToLower().Contains(search) ||
                                                    p.NationalPatientId.ToLower().Contains(search) ||
                                                    p.TracknetNumber.ToLower().Contains(search));
        foreach (var pat in patients)
        {
            results.Add(new PatientSearchViewModel(pat));
        }

        //search EntityDemographic for NationalID

        patients = await _patientRepository.GetAsync(
            p => p.Entity.Demographic.NationalID.ToLower().Contains(search));
        foreach (var pat in patients)
        {
            results.Add(new PatientSearchViewModel(pat));
        }
        return results;
    }

    //can search all string patient fields (now search names and external ID ()
    private async Task<IEnumerable<PatientSearchViewModel>> SearchPhoneNumber(string search)
    {
        var patients =
            await _patientRepository.GetAsync(p => EntityHelper.GetEntityPhoneNumber(p.Entity).Replace("-", "").Replace(".", "").Contains(search));
        return patients.Select(pat => new PatientSearchViewModel(pat)).ToList();
    }

    private async Task<IEnumerable<PatientSearchViewModel>> SearchEmail(string search)
    {
        var results = new List<PatientSearchViewModel>();
        var patients = await _patientRepository.GetAsync( p => EntityHelper.GetEntityEmail(p.Entity).Contains(search));
        foreach (var pat in patients)
        {
            results.Add(new PatientSearchViewModel(pat));
        }
        return results;
    }

    #endregion
}
}
