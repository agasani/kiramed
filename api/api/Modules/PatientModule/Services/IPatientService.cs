﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using api.Models;
using api.Repositories;

namespace api.Services
{
    public interface IPatientService : IPatientRepository
    {
        Task<List<PatientViewModel>> Get(int countryId);
        Task<List<PatientViewModel>> GetOrganizationPatients(int organizationId);
        Task<List<PatientViewModel>> GetUserPatients(int userId);
        //Task<PatientViewModel> GetByPatientId(int patientId);
        new Task<IEnumerable<Patient>> GetAllAsync();
        Task<IEnumerable<PatientSearchViewModel>> Search(string search);

    }
}
