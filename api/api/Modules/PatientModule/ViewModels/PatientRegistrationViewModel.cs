﻿using api.Db;
using api.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace api.Models
{
    public class PatientRegistrationViewModel : EntityRegistrationViewModel
    {
   
        //Death Date
        public DateTime? DeathDate { get; set; }
        [Required]
        [StringLength(10)]
        public string MedicalRecordNumber { get; set; }
        //Tracknet Id
        public string TracknetNumber { get; set; }
        //Allergy Note
        [MinLength(500)]
        public string AllergyNote { get; set; }
        //Non-Clinical Note
        [MinLength(500)]
        public string NonClinicalNote { get; set; }
        //Consent for HIE
        public bool AcceptedHIE { get; set; } = true;
        //Active Ack that patient has no allergies
        public bool NoKnownAllergy { get; set; }
        //Active Ack that patient has no meds
        public bool NoKnownMed { get; set; }
        //National Patient Identification Number
        [Required]
        [StringLength(12)]
        public string NationalPatientId { get; set; }
        //This forces creation of a new Patient regardless the fact that the system suspect that the patient already exists
        public bool ForceCreate { get; set; } = false;
  
        public override Entity CheckIfEntityExists(KiraContext context)
        {
            var patient = context.Entities.FirstOrDefault(e => e.ExternalId == NationalId
                || e.Demographic.NationalID == NationalId
                || (e.Firstname == FirstName && e.Lastname == SecondName)
                || e.Contacts.Any(c => (ContactTypes)c.ContactTypeId == ContactTypes.Email && c.Value == Email)
                || e.Contacts.Any(c => (ContactTypes)c.ContactTypeId == ContactTypes.Phone && c.Value == PhoneNumber));

            return patient;
        }

        public PatientRegistrationViewModel() { }

        public PatientRegistrationViewModel(Patient pat)
        {
            FirstName = pat.Entity.Firstname;
            SecondName = pat.Entity.Lastname;
            NationalId = pat.Entity.Demographic.NationalID;
            CountryId = pat.CountryId;
            Email = pat.Entity.Contacts.FirstOrDefault(c => c.ContactTypeId == 2).Value;
            PhoneNumber = pat.Entity.Contacts.FirstOrDefault(c => c.ContactTypeId == 1).Value;
            DeathDate = pat?.DeathDate;
            AllergyNote = pat.AllergyNote;
            NonClinicalNote = pat.NonClinicalNote;
            AcceptedHIE = pat.AcceptedHIE;
            NoKnownAllergy = pat.NoKnownAllergy;
            NoKnownMed = pat.NoKnownMed;
            MiddleName = pat.Entity.Middlename;
            CountryId = pat.CountryId;

            Demographic = new EntityDemographicViewModel(pat.Entity.Demographic);
            foreach (var addr in pat.Entity.Addresses)
            {
                var address = new EntityAddressViewModel(addr);
                Addresses.Add(address);
            }
            foreach (var cont in pat.Entity.Contacts)
            {
                var contact = new EntityContactViewModel(cont);
                Contacts.Add(contact);
            }
        }
        
    }
}
