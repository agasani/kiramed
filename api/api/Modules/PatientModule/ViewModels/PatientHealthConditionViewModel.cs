﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace api.Models
{
    public class PatientHealthConditionViewModel
    {
        public DateTime? DeathDate { get; set; }
        //Allergy Note
        [MinLength(500)]
        public string AllergyNote { get; set; }
        //Non-Clinical Note
        [MinLength(500)]
        public string NonClinicalNote { get; set; }
        //Consent for HIE
        public bool AcceptedHIE { get; set; } = true;
        //Active Ack that patient has no allergies
        public bool NoKnownAllergy { get; set; } = false;
        //Active Ack that patient has no meds
        public bool NoKnownMed { get; set; } = false;
    }
}
