﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace api.Models
{
    public class PatientSearchViewModel
    {

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Id { get; set; }
        public int EntityId { get; set; }
        public int DemographicId { get; set; }
        public string NationalID {get;set;}
        public DateTime DOB {get;set;}

        public PatientSearchViewModel() { }
        public PatientSearchViewModel(Patient patient)
        {
            FirstName = patient.Entity.Firstname;
            LastName =  patient.Entity.Lastname;
            Id = patient.Id;
            EntityId = patient.EntityId;
            DemographicId = patient.Entity.Demographic.Id;
            NationalID = patient.Entity.Demographic.NationalID;
            DOB = patient.Entity.Demographic.DOB;
        }
    }
}