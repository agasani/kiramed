﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using api.ViewModels;

namespace api.Models
{
    public class PatientViewModel
    {
        public PatientViewModel() { }
        public PatientViewModel(Patient patient)
        {
            Id = patient.Id;
            EntityId = patient.EntityId;
            DeathDate = patient.DeathDate;
            NationalPatientId = patient.NationalPatientId;
            MedicalRecordNumber = patient.MedicalRecordNumber;
            TracknetNumber = patient.TracknetNumber;
            AllergyNote = patient.AllergyNote;
            NoKnownAllergy = patient.NoKnownAllergy;
            NonClinicalNote = patient.NonClinicalNote;
            AcceptedHIE = patient.AcceptedHIE;
            NoKnownMed = patient.NoKnownMed;
            IsDeleted = patient.IsDeleted;
            FirstName = patient.Entity.Firstname;
            LastName = patient.Entity.Lastname;


            var emailContact = patient.Entity.Contacts.FirstOrDefault(c => c.ContactTypeId == (short)ContactTypes.Email);

            if (emailContact != null)
            {
                Email = emailContact.Value;
            }

            var phoneContact = patient.Entity.Contacts.FirstOrDefault(c => c.ContactTypeId == (short)ContactTypes.Phone);

            if (phoneContact != null)
            {
                PhoneNumber = phoneContact.Value;
            }

            NationalId = patient.Entity.Demographic.NationalID ?? patient.Entity.ExternalId;
            IsDeleted = patient.IsDeleted;
            DOB = patient.Entity.Demographic.DOB;
            Sex = patient.Entity.Demographic.Sex;

            Entity = new EntityViewModel(patient.Entity);
        }

        public int Id { get; set; }
        public int EntityId { get; set; }
        //public Entity Entity { get; set; }
        //Death Date
        public DateTime? DeathDate { get; set; }
        [Required]
        [StringLength(10)]
        public string MedicalRecordNumber { get; set; }
        //Tracknet Id
        public string TracknetNumber { get; set; }
        //Allergy Note
        [MinLength(500)]
        public string AllergyNote { get; set; }
        //Non-Clinical Note
        [MinLength(500)]
        public string NonClinicalNote { get; set; }
        //Consent for HIE
        public bool AcceptedHIE { get; set; } = true;
        //Active Ack that patient has no allergies
        public bool NoKnownAllergy { get; set; }
        //Active Ack that patient has no meds
        public bool NoKnownMed { get; set; }
        //Deleted Flag
        public DateTime IsDeleted { get; set; }
        //National Patient Identification Number
        [Required]
        [StringLength(12)]
        public string NationalPatientId { get; set; }
        //This forces creation of a new Patient regardless the fact that the system suspect that the patient already exists
        public DateTime DOB { get; set; }
        public string Sex { get; set; }

        public EntityViewModel Entity { get; set; }
 

       // public string UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string NationalId { get; set; }
    }

}
