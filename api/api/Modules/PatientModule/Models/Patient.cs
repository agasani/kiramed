﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using api.Utils;
using Nest;

namespace api.Models
{
    [Table("Patient")]
    [ElasticsearchType(Name = "patient")]
    public class Patient : KiraEntity
    {
        [Required]
        public int EntityId { get; set; }
        public int CountryId { get; set; }
        public Entity Entity { get; set; }
        //Death Date
        public DateTime? DeathDate { get; set; }
        [Required]
        [StringLength(10)]
        public string MedicalRecordNumber { get; set; }
        //Tracknet Id
        public string TracknetNumber { get; set; }
        //Allergy Note
        [MinLength(500)]
        public string AllergyNote { get; set; }
        //Non-Clinical Note
        [MinLength(500)]
        public string NonClinicalNote { get; set; }
        //Consent for HIE
        public bool AcceptedHIE { get; set; } = true;
        //Active Ack that patient has no allergies
        public bool NoKnownAllergy { get; set; }
        //Active Ack that patient has no meds
        public bool NoKnownMed { get; set; }
        //Deleted Flag
        public ICollection<Medication> Meds { get; set; }
        //National Patient Identification Number
        [Required]
        [StringLength(12)]
        public string NationalPatientId { get; set; }

        public void Update(PatientViewModel vModel)
        {
            if (vModel.DeathDate.HasValue)
                DeathDate = vModel.DeathDate;
            if (!string.IsNullOrEmpty(vModel.AllergyNote))
                AllergyNote = vModel.AllergyNote;
            if (!string.IsNullOrEmpty(vModel.NonClinicalNote))
                NonClinicalNote = vModel.NonClinicalNote;
            AcceptedHIE = vModel.AcceptedHIE;
            NoKnownAllergy = vModel.NoKnownAllergy;
            NoKnownMed = vModel.NoKnownMed;
        }

        public void Update(PatientRegistrationViewModel vModel)
        {
            if (vModel.DeathDate.HasValue)
                DeathDate = vModel.DeathDate;
            if (!string.IsNullOrEmpty(vModel.AllergyNote))
                AllergyNote = vModel.AllergyNote;
            if (!string.IsNullOrEmpty(vModel.NonClinicalNote))
                NonClinicalNote = vModel.NonClinicalNote;
            AcceptedHIE = vModel.AcceptedHIE;
            NoKnownAllergy = vModel.NoKnownAllergy;
            NoKnownMed = vModel.NoKnownMed;
        }

        public Patient() { }
        public Patient(PatientViewModel model, string country)
        {
            GenerateNationalPatientId(country);
            GenerateMedicalRecordNumber();
            GetTracknetNumber();
            AllergyNote = model.AllergyNote;
            DeathDate = model.DeathDate;
            IsDeleted = model.IsDeleted;
            NoKnownAllergy = model.NoKnownAllergy;
            NoKnownMed = model.NoKnownMed;
            NonClinicalNote = model.NonClinicalNote;
            AcceptedHIE = model.AcceptedHIE;
        }

        public Patient(PatientRegistrationViewModel model)
        {
            DeathDate = model?.DeathDate;
            //Health Condition
            AllergyNote = model?.AllergyNote;
            DeathDate = model?.DeathDate;
            NoKnownAllergy = model.NoKnownAllergy;
            NoKnownMed = model.NoKnownMed;
            NonClinicalNote = model.NonClinicalNote;
            AcceptedHIE = model.AcceptedHIE;
            

            //IDs
            NationalPatientId = IDGenerator.GenerateNationalPatientID(10, model.CountryId.ToString()) ;     //recreate
            MedicalRecordNumber = IDGenerator.GenerateAlphaNumericID(10);// GenerateMedicalRecordNumber();  //recreate
            TracknetNumber = MedicalRecordNumber; // GetTracknetNumber();                    //recreate
            AllergyNote = "";        
            DeathDate = DateTime.MaxValue;
            NonClinicalNote = "";
        }

        private void GenerateNationalPatientId(string country)
        {
            NationalPatientId = GenerateNationalPatientIdPublic(country);
        }

        public string GenerateNationalPatientIdPublic(string country)
        {
            var rnd = new Random(DateTime.Now.Millisecond);
            var theRnd = "";
            do
            {
                theRnd += rnd.Next().ToString();
            } while (theRnd.Length < 10);

            return country.ToUpper() + theRnd;
        }

        private void GenerateMedicalRecordNumber()
        {
            MedicalRecordNumber = GenerateMedicalRecordNumberPublic();
        }

        private string GenerateMedicalRecordNumberPublic()
        {
            return IDGenerator.GenerateAlphaNumericID(10);
        }

        private void GetTracknetNumber()
        {
            //TODO find out how to get or generate Trac Number
            TracknetNumber = GenerateMedicalRecordNumberPublic();
        }
        
    }

}
