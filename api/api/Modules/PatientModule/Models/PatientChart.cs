﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace api.Models
{
    //Patient Chart
    [Table("PatientChart")]
    public class PatientChart: IEntity
    {
        public int Id { get; set; }
        public int AdmissionId { get; set; }
        [Required]
        public int PatientId { get; set; }
        //Authorized Personnel Id
        public int AuthorizedPersonnelId { get; set; }
        public string DocumentId { get; set; }
        //Document Format
        public string DocumentFormat { get; set; }
        //Discipline
        public string Discipline { get; set; }
        //Creation Date
        public DateTime CreationDate { get; set; }
        //Completion Date
        public DateTime CompletionDate { get; set; }
        //VersionName
        public string Version { get; set; }
        //Description
        public string Description { get; set; }
        public string UserId { get; set; }
        //Entry Date Time
        public DateTime EntryDateTime { get; set; } = DateTime.Now;
        //Status
        public bool IsActive { get; set; }
        //Personnel Id
        public int PersonnelId { get; set; }
        //Document SubType
        public string DocumentSubType { get; set; }
        //Author Completion
        public DateTime AuthorCompletionDate { get; set; }
        //Modification Date
        public DateTime ModificationDateTime { get; set; }
        //Modification User Id
        public string ModifiedByUserId { get; set; }
        //Deleted Flag
        public DateTime? IsDeleted { get; set; }
        //Physician Electronic Signature indicator
        public DateTime? PhysicianSignedDate { get; set; }
        //Patient Signature Date
        public DateTime? PatientSignedDate { get; set; }

    }

}
