﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using api.Models;

namespace api.Db
{
    public partial class KiraContext : DbContext
    {
        public DbSet<Patient> Patients { get; set; }
        public DbSet<PatientChart> PatientCharts { get; set; }

    }
}
