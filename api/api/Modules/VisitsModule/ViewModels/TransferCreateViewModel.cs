﻿using System;
using api.Enums;

namespace api.Models
{
    public class TransferCreateViewModel
    {
        public DateTime ArrivalDateTime { get; set; }
        public int ReceivingOrganizationId { get; set; }
        public ePatientRelationShip Relationship { get; set; }
        public int OriginOrganizationId { get; set; }
        public string NationalPatientId { get; set; }
        public string TransferPhysicianUserId { get; set; } 
        public int PatientId {get;set;} = -1; //Why didnt you set it to zero
        public int EncounterId {get;set;} = -1;
    }
}