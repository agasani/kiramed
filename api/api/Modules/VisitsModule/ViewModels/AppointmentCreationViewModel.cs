﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Server.Kestrel.Internal.Http;

namespace api.Models
{
    public class AppointmentCreationViewModel
    {
        public DateTime DateTimeModified { get; set; }
        [Required]
        public DateTime AppointmentDate { get; set; }
        [Required]
        public int OrganizationId { get; set; }
        public eAppointmentStatus Status { get; set; } = eAppointmentStatus.Pending;
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        public string Email { get; set; }
        [Required]
        public string PhoneNumber { get; set; }
        [Required]
        public string NationalId { get; set; }
        public bool RequireInitialPayment { get; set; } = false;
        //[Required]
        public List<VisitReason> VisitReasons { get; set; }
        public short PatientRelationship { get; set; }
        //public int CarePlanId { get; set; }
        //public string ProvideId { get; set; }
        //public int PatientId { get; set; }
        //public int VisitFollowUpId { get; set; }
        //public eAppointmentTypes Type { get; set; } 

        public AppointmentCreationViewModel(Appointment appointment)
        {
            
            AppointmentDate = appointment.ArrivalDateTime;
            OrganizationId = appointment.OrganizationId;          
            FirstName = appointment.FirstName;
            LastName = appointment.LastName;
            Email = appointment.Email;
            PhoneNumber = appointment.PhoneNumber;
            NationalId = appointment.NationalId;
            PatientRelationship = (short)appointment.Relationship;
            VisitReasons = VisitReasons;
            //PatientId = appointment.PatientId;
            //ProvideId = appointment.ProviderId;
            //VisitFollowUpId = appointment.VisitFollowUpId;
            //CarePlanId = appointment.CarePlanId;
            //Type = appointment.Type;

        }
    }
}
