﻿using System;
using api.Enums;
using api.Models;

namespace api.ViewModels
{
    public class VisitQueueViewModel
    {
       public  int QueueOrderNumber { get; set; }
       public DateTime ArrivalDateTime { get; set; }
       public string FirstName { get; set; }
       public string LastName { get; set; }
       public string PatientId { get; set; }
       public eVisitType VisitType { get; set; }
       public ePatientRelationShip Relationship { get; set; } = ePatientRelationShip.Self;

        public VisitQueueViewModel(VisitQueue visit)
        {
            QueueOrderNumber = visit.QueueOrderNumber;
            ArrivalDateTime = visit.ArrivalDateTime;
            FirstName = visit.FirstName;
            LastName = visit.LastName;
            VisitType = visit.VisitType;
            Relationship = visit.Relationship;
        }
       
    }
}
 