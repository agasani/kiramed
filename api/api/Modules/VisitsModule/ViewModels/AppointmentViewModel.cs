﻿

using System;
using System.Buffers;
using System.Collections.Generic;

namespace api.Models
{
    public class AppointmentViewModel
    {
        public DateTime DateTimeModified { get; set; }
        public DateTime AppointmentDate { get; set; }
        public int OrganizationId { get; set; }
        public eAppointmentStatus Status { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string NationalId { get; set; }
        public bool RequireInitialPayment { get; set; } = false;
        public List<VisitReason> VisitReasons { get; set; }
        public ePatientRelationShip Relationship { get; set; }
        public int Id { get; set; }
        public DateTime IsDeleted { get; set; }
        public DateTime DateTimeCreated { get; set; }

        public AppointmentViewModel() { }

        public AppointmentViewModel(Appointment appointment)
        {
            Id = appointment.Id;
            AppointmentDate = appointment.ArrivalDateTime;
            OrganizationId = appointment.OrganizationId;
            Status = appointment.Status;
            FirstName = appointment.FirstName;
            LastName = appointment.LastName;
            Email = appointment.Email;
            PhoneNumber = appointment.PhoneNumber;
            NationalId = appointment.NationalId;
            Relationship = appointment.Relationship;
            VisitReasons = VisitReasons;
            IsDeleted = appointment.IsDeleted;
        }
    }
}
