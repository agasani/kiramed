﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api.Enums;
using api.Utils;

namespace api.Models
{
    public class TransferViewModel
    {
        public DateTime DateTimeCreated { get; set; }
        public DateTime ArrivalDateTime { get; set; }
        public int ReceivingOrganizationId { get; set; }
        public string ReceivingOrganizationName {get;set;}
        public ePatientRelationShip Relationship { get; set; }
        public string RelationshipName {get;set;} 
        public int OriginOrganizationId { get; set; }
        public string OriginOrganizationName {get;set;}
        public string NationalPatientId { get; set; }
        public string TransferPhysicianUserId { get; set; }
        public string TransferPhysicianUserName {get;set;}
        public string PatientFirstName {get;set;}
        public string PatientLastName {get;set;}
        public int? EncounterID {get;set;}

        public TransferViewModel(Transfer transfer)
        {
            ArrivalDateTime = transfer.ArrivalDateTime;
            ReceivingOrganizationId = transfer.OrganizationId;
            ReceivingOrganizationName = "";
            Relationship = transfer.Relationship;
            RelationshipName = transfer.Relationship.GetDescription();
            OriginOrganizationId = transfer.OriginOrganizationId;
            OriginOrganizationName = "";
            NationalPatientId = transfer.NationalPatientId;
            TransferPhysicianUserId = transfer.TransferPhysicianUserId;
            TransferPhysicianUserName = "";           
            DateTimeCreated = transfer.DateTimeCreated;
            PatientFirstName = "";//transfer.Patient.Entity.FirstName;
            PatientLastName = ""; //transfer.Patient.Entity.LastName;
            EncounterID = transfer.EncounterId;
        }
    }
}
