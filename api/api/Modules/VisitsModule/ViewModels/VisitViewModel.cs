﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace api.Models
{
    public class VisitViewModel
    {
        public int Id { get; set; }
        [Required]
        public int OrganizationId { get; set; }
        [Required]
        public int PatientId { get; set; }
        public bool IsReferral { get; set; }
        public bool IsFollowUp { get; set; }
        [Required]
        public bool IsPatientEnsured { get; set; }
        [Required]
        public string ReceptionistId { get; set; }
        public string NurserId { get; set; }
        public string DoctorId { get; set; }
        public int EncounterId { get; set; }
        [Required]
        public DateTime VisitDateTime { get; set; }
        [Required]
        [StringLength(300)]
        public string VisitReason { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MedicalRecordNumber { get; set; }
        public string TrackNetNumber { get; set; }
        public int AppointmentId { get; set; }
        public eVisitStatus Status { get; set; } = eVisitStatus.Waiting;

        public VisitViewModel() { }

        public VisitViewModel(Visit visit, Patient patient)
        {
            Id = visit.Id;
            OrganizationId = visit.OrganizationId;
            PatientId = patient.Id;
            FirstName = patient.Entity.Firstname;
            LastName = patient.Entity.Lastname;
            MedicalRecordNumber = patient.MedicalRecordNumber;
            VisitDateTime = visit.VisitDateTime;
            VisitReason = visit.VisitReason;
            ReceptionistId = visit.ReceptionistId;
            NurserId = visit.NurserId;
            DoctorId = visit.DoctorId;
            IsReferral = visit.IsReferral;
            IsFollowUp = visit.IsFollowUp;
            IsPatientEnsured = visit.IsPatientEnsured;
            TrackNetNumber = patient.TracknetNumber;
            AppointmentId = visit.AppointmentId;
            Status = visit.Status;
        }
    }
}
