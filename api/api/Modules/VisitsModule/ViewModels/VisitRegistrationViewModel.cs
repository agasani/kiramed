﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace api.Models
{
    public class VisitRegistrationViewModel
    {
        public int Id { get; set; }
        [Required]
        public int OrganizationId { get; set; }
        [Required]
        public int PatientId { get; set; }
        public bool IsReferral { get; set; }
        public bool IsFollowUp { get; set; }
        [Required]
        public bool IsPatientEnsured { get; set; }
        [Required]
        public string ReceptionistId { get; set; }
        [Required]
        public DateTime VisitDateTime { get; set; }
        [Required]
        [StringLength(300)]
        public string VisitReason { get; set; }
        public string NurserId { get; set; }
        public string DoctorId { get; set; }
        public int EncounterId { get; set; }
        public DateTime ScheduledDateTime { get; set; } = DateTime.MinValue;
        public bool Ischeduled { get; set; } = false;


        public VisitRegistrationViewModel() { }

        public VisitRegistrationViewModel(Visit visit)  //used mostly for testing
        {
            OrganizationId = visit.OrganizationId;
            PatientId = visit.PatientId;
            IsReferral = visit.IsReferral;
            IsFollowUp = visit.IsFollowUp;
            ReceptionistId = visit.ReceptionistId;
            VisitReason = visit.VisitReason;
            VisitDateTime = DateTime.Now;
            IsPatientEnsured = visit.IsPatientEnsured;
            NurserId = visit.NurserId;
            DoctorId = visit.DoctorId;
            EncounterId = visit.EncounterId;
        }
    }
}
