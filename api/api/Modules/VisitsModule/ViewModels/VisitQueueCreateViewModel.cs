﻿using System;
using api.Enums;

namespace api.Models
{
    public class VisitQueueCreateViewModel
    {
        public int OrganizationId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime ArrivedAt { get; set; } = DateTime.Now;
        public eVisitType VisitType { get; set; } = eVisitType.Walkin;
        public ePatientRelationShip Relationship { get; set; } = ePatientRelationShip.Self;
    }
}
