﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api.Db;
using api.Models;
using api.Repositories;

namespace api.Services
{
    public class TransferService : TransferRepository, ITransferService
    {
        private readonly IVisitQueueRepository _visitQueueRepository;

        public TransferService(KiraContext context, IVisitQueueRepository visitQueueRepository) : base(context)
        {
            _visitQueueRepository = visitQueueRepository;
        }

        public async  Task<int> AddToQueueAsync(TransferQueueDTO transfer)
        {
            return await  _visitQueueRepository.CreateAsync(new VisitQueue(transfer));
        }
    }
}
