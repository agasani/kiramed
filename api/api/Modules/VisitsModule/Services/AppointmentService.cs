﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api.Models;
using api.Repositories;

namespace api.Services
{
    public class AppointmentService : KiraService<Appointment>, IAppointmentService
    {
        //If we decide to process payments at appointment creation,
        //we will need to inject the ITransaction service (or whatever payment service we build)
        //Fucking awesome

        public readonly IAppointmentRepository _appointmentRepository;
        public readonly IVisitQueueRepository _visitQueueRepository;

        public AppointmentService(IAppointmentRepository appointmentRepository, IVisitQueueRepository visitQueueRepository) : base(appointmentRepository)
        {
            _appointmentRepository = appointmentRepository;
            _visitQueueRepository = visitQueueRepository;
        }

        public Task<bool> CheckinAsync(Appointment appointment)
        {
            throw new NotImplementedException();
        }

        public async  Task<Appointment> ExistsAsync(Appointment appointment)
        {
            return await _appointmentRepository.ExistsAsync(appointment);
        }

        public Appointment Exists(Appointment appointment)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<Appointment>> GetByNationalIdAsync(string nationalId)
        {
            return await _appointmentRepository.GetByNationalIdAsync(nationalId);
        }

        public async Task<IEnumerable<Appointment>> CreateAppointSeriesAsync(AppointmentCreationViewModel model)
        {
            var careplan = GetCarePlanDetails(1);//model.CarePlanId);
            var appointments = new List<Appointment>();
            for (var i = 0; i <= careplan.NumberOfVisits; i++)
            {
                var appointment = new Appointment(model)
                {
                    ArrivalDateTime =
                        model.AppointmentDate.AddDays(i * VisitFrequencyConverter(careplan.VisitFrenquency))
                };
                appointments.Add(appointment);
            }
            return appointments;
        }

        public IEnumerable<Appointment> CreateAppointSeries(AppointmentCreationViewModel model)
        {
            throw new NotImplementedException();
        }

        public async Task<int> AddToQueueAsync(Appointment appointment)
        {          
            var result = await _visitQueueRepository.CreateAsync(new VisitQueue(appointment));
            return result;
        }

        #region Temporary Helper

        public CarePlan GetCarePlanDetails(int carePlainId)
        {
            var care = new CarePlan
            {
                NumberOfVisits = 5,
                VisitFrenquency = eFrenquency.Monthly
            };
            return care;
        }

        public int VisitFrequencyConverter(eFrenquency frenquency)
        {
            switch (frenquency)
            {
                case eFrenquency.Weekly:
                    return 7;
                case eFrenquency.Monthly:
                    return 30;
                case eFrenquency.Quarterly:
                    return 90;
                case eFrenquency.Once:
                    break;
                case eFrenquency.Daily:
                    break;
                case eFrenquency.Annually:
                    break;
                case eFrenquency.BiWeekly:
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(frenquency), frenquency, null);
            }
            return 30;
        }
        #endregion


    }
}
