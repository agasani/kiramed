﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api.Models;
using api.Repositories;

namespace api.Services
{
    public interface ITransferService : ITransferRepository
    {
        Task<int> AddToQueueAsync(TransferQueueDTO transfer);
    }
}
