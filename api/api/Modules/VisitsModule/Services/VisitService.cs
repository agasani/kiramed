﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using api.Models;
using api.Repositories;

namespace api.Services
{
    public class VisitService : KiraService<Visit>, IVisitService
    {
        private readonly IVisitRepository _visitRepository;
        private readonly IPatientRepository _patientRepository;
        private readonly IAppointmentService _appointmentService;
        private readonly IUserService _userService;
        private readonly IInsuranceService _insuranceService;
        private readonly IBarcodeService _barcodeService;



        public VisitService(IVisitRepository visitRepository
                            , IPatientRepository patientRepository
                            , IAppointmentService appointmentService
                            , IUserService userService
                           ) : base(visitRepository)
        {
            _visitRepository = visitRepository;
            _patientRepository = patientRepository;
            _appointmentService = appointmentService;
            _userService = userService;
        }   

        public async Task<VisitViewModel> GetVisitViewModelAsync(Visit visit, Patient patient = null)
        {
            var pat = patient ?? await _patientRepository.FindAsync(visit.PatientId);
           
            return new VisitViewModel(visit, pat);        
        }

        public async Task<IEnumerable<Visit>> PatientVisitsAsync(int patientId)
        {
            return await _visitRepository.GetAsync(v => v.PatientId == patientId);
        }

        public Task<Visit> ScanLabelAsync(object scandata)
        {
            throw new NotImplementedException();
        }

        public Task<object> PrintLabelAsync(object data)
        {
            throw new NotImplementedException();
        }

        public Task<object> TakePhotoAsync(string url)
        {
            throw new NotImplementedException();
        }

        public Task<bool> AddVisitServiceAsync(int visitId, string serviceName)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<Visit>> GetPastVisitAsync(Expression<Func<Visit, bool>> expression)
        {
            //this method can be called by specifiying attributes like start and end dates
            return await _visitRepository.GetAsync(expression);
        }

        public Task<InsurancePolicy> ValidateInsuranceAsync(string policyNumber)
        {
            throw new NotImplementedException();
        }

        public Task<bool> PrioritizeServiceAsync(int visitId, string serviceName)
        {
            throw new NotImplementedException();
        }

        public Task<bool> LinkVisitsAsync(int currentVisitId, int otherVisit)
        {
            throw new NotImplementedException();
        }

        public async Task<int> AddVitalSignAsync(int visitId, VitalSign vitalSign)
        {
            var visit = await _visitRepository.FindAsync(visitId);
            visit.VitalSigns.Add(vitalSign);
            return await _visitRepository.UpdateAsync(visit);
        }

        public async Task<int> AssignToNurseAsync(int visitId, string nurseUserId)
        {
            var visit = await  _visitRepository.FindAsync(visitId);
             var user = await _userService.GetByEmailAsync("joemasengesho2@kiramd.com");
            visit.AssignedANurseAt = DateTime.Now;
             visit.NurserId = user.UserId;
            return await _visitRepository.UpdateAsync(visit);
        }

        public async Task<int> AssignToDoctorAsync(int visitId, string doctorUserId)
        {
            var visit = await _visitRepository.FindAsync(visitId);
            var user = await _userService.GetByEmailAsync("joemasengesho2@kiramd.com");
            visit.AssignedADoctorAt = DateTime.Now;
            visit.DoctorId = user.UserId;
            return await _visitRepository.UpdateAsync(visit);
        }

       //this method can be called to get organization visits (by dates, by user)
        public async Task<IEnumerable<VisitViewModel>> GetOrganizationVisitsAsync(Expression<Func<Visit, bool>> expression)
        {
           var results = await _visitRepository.GetAsync(expression);
           var patients = await _patientRepository.GetAsync(p => results.Any(r => r.PatientId == p.Id));
           return results.Select(v => new VisitViewModel(v, patients.FirstOrDefault(p => p.Id == v.PatientId)));
        }
        
        //this method can be called to get an orgization patients (by visit status, by doctor, by date or anything)
        public async Task<IEnumerable<PatientViewModel>> GetOrganizationPatientsAsync(Expression<Func<Visit, bool>> expression)
        {
           var results = await _visitRepository.GetAsync(expression);
           var patients = await _patientRepository.GetAsync(p => results.Any(r => r.PatientId == p.Id));
           return patients.Select(p => new PatientViewModel(p));
        }

        public Task<bool> ArchiveAsync(Visit visit)
        {
            throw new NotImplementedException();
        }

        public bool Archive(Visit visit)
        {
            throw new NotImplementedException();
        }

        public Task<bool> ArchiveBulkAsync(IList<Visit> visits)
        {
            throw new NotImplementedException();
        }

        public bool ArchiveBulk(IList<Visit> visits)
        {
            throw new NotImplementedException();
        }
    }
}
