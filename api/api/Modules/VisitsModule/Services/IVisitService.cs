﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using api.Models;
using api.Services;

namespace api.Services
{
    public interface IVisitService : IKiraService<Visit>
    {
        Task<VisitViewModel> GetVisitViewModelAsync(Visit visit, Patient patient = null);
        Task<IEnumerable<Visit>> PatientVisitsAsync(int patientId);
       // Task<IEnumerable<PatientSearchViewModel>> SearchPatientAsync(string search);
        Task<Visit> ScanLabelAsync(object scandata);
       // Task<int> RegisterNewPatientAsync(PatientRegistrationViewModel patient);
        //Task<int> UpdateAsync(Patient patient);
        //Task<Patient> FindPatientByTrackNumberAsync(string trackNumber);
        Task<object> PrintLabelAsync(object data);
        Task<object> TakePhotoAsync(string url);
        Task<bool> AddVisitServiceAsync(int visitId, string serviceName);
       // Task<int> CreateFromAppointmentAsync(Appointment appointment);
        Task<IEnumerable<Visit>> GetPastVisitAsync(Expression<Func<Visit, bool>> expression);
        Task<InsurancePolicy> ValidateInsuranceAsync(string policyNumber);
        Task<bool> PrioritizeServiceAsync(int visitId, string serviceName);
        Task<bool> LinkVisitsAsync (int currentVisitId, int otherVisit);
        Task<int> AddVitalSignAsync (int visitId, VitalSign vitalSign);
        Task<int> AssignToNurseAsync (int visitId, string nurseUserId);
        Task<int> AssignToDoctorAsync(int visitId, string doctorUserId);
       // Task<User> ValidateUser(string userId);
        Task<IEnumerable<VisitViewModel>> GetOrganizationVisitsAsync(Expression<Func<Visit, bool>> expression);
        Task<IEnumerable<PatientViewModel>> GetOrganizationPatientsAsync(Expression<Func<Visit, bool>> expression);
        Task<bool> ArchiveAsync(Visit visit);
        bool Archive(Visit visit);
        Task<bool> ArchiveBulkAsync(IList<Visit> visits);
        bool ArchiveBulk(IList<Visit> visits);

    }
}
