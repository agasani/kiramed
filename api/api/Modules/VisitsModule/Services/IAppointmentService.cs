﻿using System;
using System.Buffers;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api.Models;

namespace api.Services
{
    public interface IAppointmentService : IKiraService<Appointment>
    {
        Task<bool> CheckinAsync(Appointment appointment);
        Task<Appointment> ExistsAsync(Appointment appointment);
        Appointment Exists(Appointment appointment);
        Task<IEnumerable<Appointment>> GetByNationalIdAsync(string nationalId);
        Task<IEnumerable<Appointment>> CreateAppointSeriesAsync(AppointmentCreationViewModel model);
        IEnumerable<Appointment> CreateAppointSeries(AppointmentCreationViewModel model);
        Task<int> AddToQueueAsync(Appointment appointment);

    }
}
