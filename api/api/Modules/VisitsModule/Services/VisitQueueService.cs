using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api.Models;
using api.Repositories;
using System.Linq.Expressions;
using api.Db;

namespace api.Services
{
    public class VisitQueueService : VisitQueueRepository, IVisitQueueService
    {
        public readonly IVisitQueueRepository _visitQueueRepositoy;

        public VisitQueueService(KiraContext context, IVisitQueueRepository visitQueueRepository) : base(context)
        {
            _visitQueueRepositoy = visitQueueRepository;         
        }

        //public async Task<IEnumerable<VisitQueue>> GetPendingAppontmentsAsync(Expression<Func<Appointment, bool>> expression)
        //{
        //   var visitQueues = new List<VisitQueue>();

        //   var results = await _appointmentRepository.GetAsync(expression);
        //   if(results.any())
        //   {
        //       visitQueues.AddRange(results.Select(a =>  new VisitQueue(a)));
        //   }

        //    return visitQueues;
        //}


        //public async Task<IEnumerable<VisitQueue>> GetPendingTransfersAsync(Expression<Func<Transfer, bool>> expression)
        //{
        //   var visitQueues = new IList<VisitQueue>();

        //   var results = await _transferRepository.GetAsync(expression);
        //   if(results.any())
        //   {
        //       foreach (var transfer in results)
        //       {
        //           var transferQueue = new TransferQueueDTO(new TransferViewModel(t));
        //           visitQueues.Add(new VisitQueue(transferQueue()));                  
        //       }             
        //   }
        //    return visitQueues;
        //}
    }
}