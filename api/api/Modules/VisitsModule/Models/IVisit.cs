﻿using System;
using System.Collections.Generic;

namespace api.Models
{
    public interface IVisit
    {
        int AppointmentId { get; set; }
        DateTime AssignedADoctorAt { get; set; }
        DateTime AssignedANurseAt { get; set; }
        string DoctorId { get; set; }
        int EncounterId { get; set; }
        bool IsFollowUp { get; set; }
        bool IsPatientEnsured { get; set; }
        bool IsReferral { get; set; }
        string NurserId { get; set; }
        int OrganizationId { get; set; }
        int PatientId { get; set; }
        string ReceptionistId { get; set; }
        eVisitStatus Status { get; set; }
        DateTime VisitDateTime { get; set; }
        string VisitReason { get; set; }
        List<VitalSign> VitalSigns { get; set; }
    }
}