﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using api.Base.Models;
using api.Enums;
using api.ViewModels;

namespace api.Models
{
    /// <summary>
    /// This table holds patients appointments, walkins, and transfers before they are served by the receptionist
    /// </summary>
    [Table("VisitQueue")]
    public class VisitQueue : KiraEntity, IVisitQueue, IQueuable
    {
        public int OrganizationId { get; set; }
        public int QueueOrderNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime ArrivalDateTime { get; set; } = DateTime.Now;
        public eVisitType VisitType { get; set; } = eVisitType.Walkin;
        public ePatientRelationShip Relationship { get; set; } = ePatientRelationShip.Self;



        public VisitQueue(VisitQueueCreateViewModel vModel)
        {
            OrganizationId = vModel.OrganizationId;
            FirstName = vModel.FirstName;
            LastName = vModel.LastName;
            VisitType = vModel.VisitType;
            Relationship = vModel.Relationship;
        }

        public VisitQueue(Appointment apt)
        {
            OrganizationId = apt.OrganizationId;
            FirstName = apt.FirstName;
            LastName = apt.LastName;
            VisitType = eVisitType.Appointment;
            Relationship = apt.Relationship;
            ArrivalDateTime = apt.ArrivalDateTime;
        }

         public VisitQueue(TransferQueueDTO transfer)
        {
            OrganizationId = transfer.OrganizationId;
            FirstName = transfer.FirstName;
            LastName = transfer.LastName;
            VisitType = eVisitType.Appointment;
            Relationship = transfer.Relationship;
            ArrivalDateTime = transfer.ArrivalDateTime;
        }

        public VisitQueue()
        {
            
        }
    }
}
