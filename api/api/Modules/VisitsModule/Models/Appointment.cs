﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using api.Enums;


namespace api.Models
{
    [Table("Appointments")]
    public class Appointment : KiraEntity, IVisitQueue
    {
        public DateTime DateTimeModified { get; set; }
        [Required]
        public DateTime ArrivalDateTime { get; set; }
        [Required]
        public int OrganizationId { get; set; }
        public eAppointmentStatus Status { get; set; } = eAppointmentStatus.Pending;
        [Required]
        public eAppointmentTypes Type { get; set; } = eAppointmentTypes.NormalVisit;
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        public string Email { get; set; }
        [Required]
        public string PhoneNumber { get; set; }
        [Required]
        public string NationalId { get; set; }
        public bool RequireInitialPayment { get; set; } = false;
        //[Required]
        public List<VisitReason> VisitReasons { get; set; }
        public ePatientRelationShip Relationship { get; set; } = ePatientRelationShip.Self;
        public eVisitType VisitType { get; set; } = eVisitType.Appointment;

        public int PatientId { get; set; } = 0;
        public int CarePlanId { get; set; } = 0;
        public string ProviderId { get; set; } = null;
        public int VisitFollowUpId { get; set; } = 0;


        public Appointment() { }

        public Appointment(AppointmentCreationViewModel model)
        {
            FirstName = model?.FirstName;
            LastName = model?.LastName;
            Email = model?.Email;
            PhoneNumber = model?.PhoneNumber;
            NationalId = model?.NationalId;
            if (model != null) Relationship = (ePatientRelationShip) model.PatientRelationship;
            VisitReasons = model?.VisitReasons;
            DateTimeModified = DateTime.Now;
            DateTimeCreated = DateTimeModified;
            IsDeleted = DateTime.MaxValue;
            if (model?.AppointmentDate != null) ArrivalDateTime = (DateTime) model?.AppointmentDate;
        }

        public void Update(AppointmentViewModel appointment)
        {
            FirstName = appointment?.FirstName;
            LastName = appointment?.LastName;
            Email = appointment?.Email;
            PhoneNumber = appointment?.PhoneNumber;
            if (appointment?.Relationship != null) Relationship = (ePatientRelationShip) appointment?.Relationship;
            if (appointment?.AppointmentDate != null) ArrivalDateTime = (DateTime) appointment?.AppointmentDate;
            if (appointment?.IsDeleted != null) IsDeleted = (DateTime) appointment?.IsDeleted;
        }
    }
}
