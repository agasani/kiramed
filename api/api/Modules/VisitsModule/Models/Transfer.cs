﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using api.Enums;
using api.Models;
using Microsoft.EntityFrameworkCore.Metadata.Conventions.Internal;

namespace api.Models
{
    [Table("Transfer")]
    public class Transfer : KiraEntity, IVisitQueue
    {
        public DateTime ArrivalDateTime { get; set; }
        public int OrganizationId { get; set; }
        public ePatientRelationShip Relationship { get; set; }
        public eVisitType VisitType { get; set; } = eVisitType.Transfer;
        public int OriginOrganizationId { get; set; }
        public string NationalPatientId { get; set; }
       // [Required]
        public int? PatientId {get;set;}

        public virtual Patient Patient {get;set;}
       // [Required]
        public int? EncounterId {get;set;}
        public virtual Encounter Encounter {get;set;} 

        //Todo
        //Details about the transfer
        //Reason, codes, and al of that
        //

        public string TransferPhysicianUserId { get; set; }


        public Transfer(TransferCreateViewModel vModel)
        {
            ArrivalDateTime = vModel.ArrivalDateTime;
            PatientId = vModel.PatientId;
            EncounterId = vModel.EncounterId;
            OrganizationId = vModel.ReceivingOrganizationId;
            Relationship = vModel.Relationship;
            VisitType = eVisitType.Transfer;
            OriginOrganizationId = vModel.OriginOrganizationId;
            NationalPatientId = vModel.NationalPatientId;
        }

        public Transfer()
        {
            
        }
    }
}
