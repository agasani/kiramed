﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace api.Models
{
    [Table("Visit")]
    public class Visit : KiraEntity, IVisit
    {
        [Required]
        public int OrganizationId { get; set; }
        [Required]
        public int PatientId { get; set; }
        public bool IsReferral {get;set;}
        public bool IsFollowUp { get; set; }
        [Required]
        public bool IsPatientEnsured { get; set; }
        [Required]
        public string ReceptionistId { get; set; }
        public string NurserId { get; set; } = string.Empty;
        public string DoctorId { get; set; } = string.Empty;
        public int EncounterId { get; set; } = 0;
        [Required]
        public DateTime VisitDateTime { get; set; }
        public DateTime AssignedANurseAt { get; set; } = DateTime.MaxValue;
        public DateTime AssignedADoctorAt { get; set; } = DateTime.MaxValue;
        [Required]
        [StringLength(300)]
        public string VisitReason { get; set; }  //TODO provide a list of many visits reasons like prenancy, malaria, to make it simple
        public int AppointmentId { get; set; }
        public eVisitStatus Status {get; set; } = eVisitStatus.Waiting;
        public List<VitalSign> VitalSigns { get; set; }

        public Visit() { }

        public Visit(VisitViewModel vModel)
        {
            OrganizationId = vModel.OrganizationId;
            PatientId = vModel.PatientId;
            IsReferral = vModel.IsReferral;
            IsFollowUp = vModel.IsFollowUp;
            ReceptionistId = vModel.ReceptionistId;
            VisitDateTime = vModel.VisitDateTime;
            VisitReason = vModel.VisitReason;
            NurserId = vModel.NurserId;
            DoctorId = vModel.DoctorId;
            AppointmentId = vModel.AppointmentId;
            Status = vModel.Status;
        }

        public Visit(VisitRegistrationViewModel vModel)
        {
            OrganizationId = vModel.OrganizationId;
            PatientId = vModel.PatientId;
            IsReferral = vModel.IsReferral;
            IsFollowUp = vModel.IsFollowUp;
            ReceptionistId = vModel.ReceptionistId;
            VisitDateTime = vModel.VisitDateTime;
            VisitReason = vModel.VisitReason;
            NurserId = vModel?.NurserId;
            DoctorId = vModel?.DoctorId; 
        }

        public void Update(VisitRegistrationViewModel vModel)
        {
            if (vModel == null)
                return;
            NurserId = vModel?.NurserId;
            DoctorId = vModel?.DoctorId;
            VisitReason = vModel?.VisitReason;
            IsReferral = vModel?.IsReferral ?? false;
            IsFollowUp = vModel?.IsFollowUp ?? false;
            IsPatientEnsured = vModel?.IsPatientEnsured ?? false;
        }
    }
}
