﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using api.Enums;

namespace api.Models
{
    [Table("VisitHistoryLog")]
    public class VisitHistoryLog : KiraEntity, IVisit
    {
        public DateTime ArchiveDateTime { get; set; } = DateTime.Now;
        public eVisitOutcome VisitOutcome { get; set; }
        public int AppointmentId { get; set; }
        public DateTime AssignedADoctorAt { get; set; }
        public DateTime AssignedANurseAt { get; set; }
        public string DoctorId { get; set; }
        public int EncounterId { get; set; }
        public bool IsFollowUp { get; set; }
        public bool IsPatientEnsured { get; set; }
        public bool IsReferral { get; set; }
        public string NurserId { get; set; }
        public int OrganizationId { get; set; }
        public int PatientId { get; set; }
        public string ReceptionistId { get; set; }
        public eVisitStatus Status { get; set; }
        public DateTime VisitDateTime { get; set; }
        public string VisitReason { get; set; }
        public List<VitalSign> VitalSigns { get; set; }
    }
}
