﻿using System;
using api.Enums;

namespace api.Models
{
    public interface IVisitQueue
    {
        DateTime ArrivalDateTime { get; set; }
        int OrganizationId { get; set; }
        ePatientRelationShip Relationship { get; set; }
        eVisitType VisitType { get; set; }
    }
}