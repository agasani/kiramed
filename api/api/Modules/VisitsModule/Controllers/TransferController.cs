using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api.Models;
using api.Repositories;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace api.Controllers
{
    [Route("api/[controller]")]
    public class TransferController : Controller
    {
        public readonly ITransferRepository _transferRepository;

        public TransferController(ITransferRepository transferRepository)
        {
            _transferRepository = transferRepository;
        }

        [HttpGet("GetIncoming/{orgId}")]
        public async Task<IActionResult> GetIncoming([FromRoute]int orgId)
        { 

            var feedback = new FeedBack();
            var results = await _transferRepository.GetAsync(t => t.OrganizationId == orgId);
            var transfers = results.ToList();
            if (transfers.Any())
            {
                var transferViewModels = transfers.Select(vmodel => new TransferViewModel(vmodel)).ToList();
                return Ok(transferViewModels);
            }

            feedback.err = true;
            feedback.message = "No transfer were found";
            return NotFound(feedback);
        }

        [HttpGet("GetOutGoing/{orgId}")]
        public async Task<IActionResult> GetOutGoing([FromRoute]int orgId)
        {

            var feedback = new FeedBack();
            var results = await _transferRepository.GetAsync(t => t.OriginOrganizationId == orgId);
            var transfers = results.ToList();
            if (transfers.Any())
            {
                var transferViewModels = transfers.Select(vmodel => new TransferViewModel(vmodel)).ToList();
                return Ok(transferViewModels);
            }

            feedback.err = true;
            feedback.message = "No transfer were found";
            return NotFound(feedback);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]TransferCreateViewModel vModel)
        {
            var feedback = new FeedBack();

            if (!ModelState.IsValid)
            {
                feedback.err = true;
                feedback.message = "Invalid Model.";
                feedback.data = JsonConvert.SerializeObject(ModelState.Values.SelectMany(e => e.Errors).Select(x => x.ErrorMessage));
                return BadRequest(feedback);
            }

            var transfer = new Transfer(vModel);

            var result = await _transferRepository.CreateAsync(transfer);

            if (result == 0)
            {
                feedback.err = true;
                feedback.message = "The transfer cannot be created now.";
                return BadRequest(feedback);
            }

            feedback.err = false;
            feedback.message = "Transfer created successfully";
            feedback.data = JsonConvert.SerializeObject(new Dictionary<string, int> { { "Transfer Id", result } });
            return Ok(feedback);
        }
    }
}