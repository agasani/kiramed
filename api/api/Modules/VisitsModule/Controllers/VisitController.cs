﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using api.Models;
using api.Db;
using api.Services;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using api.ViewModels;
using api.Repositories;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Net;

namespace api.Controllers
{
    [Route("api/[controller]")]
    public class VisitController : Controller
    {
        private readonly IVisitService _visitService;
        private readonly IPatientRepository _patientRepo;

        public VisitController(KiraContext kiraContext, IVisitService visitService = null, IPatientRepository patientRepo = null)
        {
            //_kiraContext = kiraContext;
            _visitService = visitService;
            _patientRepo = patientRepo;
        }

        /// <summary>List visits within an organization by unique (integer) orgId</summary>
        /// <response code="404">Model not found</response>
        /// <response code="200">Listed successfully</response>
        [HttpGet("GetOrgVisits/{orgId}")]
        [SwaggerResponse((int)HttpStatusCode.NotFound, typeof(FeedBack))]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(IEnumerable<VisitViewModel>))]
        public async Task<IActionResult> GetOrgVisits([FromRoute]int orgId)
        {
            var visits = await _visitService.GetOrganizationVisitsAsync(v => v.OrganizationId == orgId);

            if (!visits.Any())
            {
                var feedback = new FeedBack { err = true, message = "No visit was found", data = "" };
                return NotFound(feedback);
            }

            return Ok(visits);
        }

        /// <summary>Get visit by unique (integer) visitId</summary>
        /// <response code="404">Model not found</response>
        /// <response code="200">Retrieved successfully</response>
        [HttpGet("Detail/{visitId}")]
        [SwaggerResponse((int)HttpStatusCode.NotFound, typeof(FeedBack))]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(VisitViewModel))]
        public async Task<IActionResult> Detail([FromRoute]int visitId)
        {
            var visit = await _visitService.FindAsync(visitId);
            if (visit == null)
            {
                var feedback = new FeedBack() { err = true, message = $"A visit Id with {visitId} does not exist.", data = "" };
                return NotFound(feedback);
            }
            var vMod = new VisitViewModel(visit, await _patientRepo.FindAsync(visit.PatientId));
            return Ok(vMod);
        }

        /// <summary>List patient visits by unique (integer) patientId</summary>
        /// <response code="404">Model not found</response>
        /// <response code="200">Listed successfully</response>
        [HttpGet("GetPatientVisits/{patientId}")]
        [SwaggerResponse((int)HttpStatusCode.NotFound, typeof(FeedBack))]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(IEnumerable<VisitViewModel>))]
        public async Task<IActionResult> GetPatientVisits([FromRoute]int patientId)
        {
            var patient = await _patientRepo.FindAsync(patientId);

            if (patient == null)
            {
                var feedback = new FeedBack() { err = true, message = $"Patient with Id  {patientId} does not exist.", data = "" };
                return NotFound(feedback);
            }

            var visits = await _visitService.PatientVisitsAsync(patientId);
            if (!visits.Any())
            {
                var feedback = new FeedBack() { err = true, message = $"Patient with Id  {patientId} does not have visits.", data = "" };
                return NotFound(feedback);
            }

            var visitsModel = visits.Select(v => new VisitViewModel(v, patient));

            return Ok(visitsModel);
        }

        /// <summary>Register a new visit</summary>
        /// <response code="400">Bad request</response>
        /// <response code="200">Created successfully</response>
        [HttpPost]
        [SwaggerResponse((int)HttpStatusCode.NotFound, typeof(FeedBack))]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(FeedBack))]
        public async Task<IActionResult> Post([FromBody]VisitRegistrationViewModel vModel)
        {
            var feedback = new FeedBack();

            if (!ModelState.IsValid)
            {
                feedback.err = true;
                feedback.message = "Invalid Model.";
                feedback.data = JsonConvert.SerializeObject(ModelState.Values.SelectMany(e => e.Errors).Select(x => x.ErrorMessage));
                return BadRequest(feedback);
            }

            //check if patient exist
            var patient = await _patientRepo.FindAsync(vModel.PatientId);

            if (patient == null || patient.IsDeleted < DateTime.Now)
            {

                feedback.err = true;
                feedback.message = "No patient was found. Visit cannot be created.";
                feedback.data = "";
                return BadRequest(feedback);
            }

            //check if receptionist exist

            var visit = new Visit(vModel);
            var result = await _visitService.CreateAsync(visit);
            if (result != 0)
            {
                feedback.data = JsonConvert.SerializeObject(new Dictionary<string, int> { { "VisitId", visit.Id } });
                return Ok(feedback);
            }

            feedback.err = true;
            feedback.message = "The visit cannot be created at this time.";
            feedback.data = "";
            return BadRequest(feedback);
        }


        /// <summary>Delete visit by unique (integer) id</summary>
        /// <response code="400">Bad request</response>
        /// <response code="200">Deleted successfully</response>
        [HttpDelete("{id}")]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, typeof(FeedBack))]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(FeedBack))]
        public async Task<IActionResult> Delete([FromRoute]int id)
        {
            var feedback = new FeedBack();
            var visit = await _visitService.FindAsync(id);

            if (visit == null)
            {
                feedback.err = true;
                feedback.message = "No visit was found!";
                return BadRequest(feedback);
            }

            visit.IsDeleted = DateTime.Now;
            visit.Status = eVisitStatus.Cancelled;
            var updateResult = await _visitService.UpdateAsync(visit);
            if (updateResult != 0)
            {
                feedback.err = false;
                feedback.message = "Visit Successfully updated";
                feedback.data = JsonConvert.SerializeObject(new Dictionary<string, int> { { "Visit", visit.Id } });
                return Ok(feedback);
            }
            //ToDo Mark deleted all stuff related to this entity

            feedback.err = true;
            feedback.message = "Visit cannot be updated. Please try later";
            feedback.data = JsonConvert.SerializeObject(new Dictionary<string, int> { { "Visit", visit.Id } });

            return BadRequest(feedback);
        }

        /// <summary>Update visit by unique (integer) id</summary>
        /// <response code="400">Bad request</response>
        /// <response code="200">Updated successfully</response>
        [HttpPut("{id}")]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, typeof(FeedBack))]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(FeedBack))]
        public async Task<IActionResult> Put([FromRoute]int id, [FromBody]VisitRegistrationViewModel vModel)
        {
            var feedback = new FeedBack();

            if (!ModelState.IsValid)
            {
                feedback.err = true;
                feedback.message = "Invalid Model.";
                feedback.data = JsonConvert.SerializeObject(ModelState.Values.SelectMany(e => e.Errors).Select(x => x.ErrorMessage));

                return BadRequest(feedback);
            }
            var visit = await _visitService.FindAsync(id);

            if (visit == null)
            {
                feedback.err = true;
                feedback.message = "Invalid Request.";
                return BadRequest(feedback);
            }

            visit.Update(vModel);

            var result = await _visitService.UpdateAsync(visit);
            if (result == 0)
            {
                feedback.err = true;
                feedback.message = "Visit cannot be updated. Please try later";
                feedback.data = JsonConvert.SerializeObject(new Dictionary<string, int> { { "Visit", visit.Id } });

                return BadRequest(feedback);
            }
            feedback.err = false;
            feedback.message = "Visit info updated successfully";
            feedback.data = JsonConvert.SerializeObject(new Dictionary<string, int> { { "Id", id } });
            return Ok(feedback);
        }
    }
}
