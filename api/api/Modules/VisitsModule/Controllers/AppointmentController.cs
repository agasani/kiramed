﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api.Models;
using api.Services;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Net;

namespace api.Controllers
{
    [Route("api/[controller]")]
    public class AppointmentController : Controller
    {
        private readonly IAppointmentService _appointmentService;

        public AppointmentController(IAppointmentService appointmentService)
        {
            _appointmentService = appointmentService;
        }

        /// <summary>List all appointments</summary>
        /// <response code="200">Listed successfully</response>
        /// <response code="404">Successfully listed</response>
        [HttpGet]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(IEnumerable<AppointmentViewModel>))]
        [SwaggerResponse((int)HttpStatusCode.NotFound, typeof(FeedBack))]
        public async Task<IActionResult> Get()
        {
            var feedback = new FeedBack();
            var appointments = await _appointmentService.GetAllAsync();
            if (appointments.Any())
            {
                var appointmentsModels = appointments.Select(appointment => new AppointmentViewModel(appointment)).ToList();
                return Ok(appointmentsModels);
            }

            feedback.err = true;
            feedback.message = "No appointments were found";
            return NotFound(feedback);
        }

        /// <summary>Get appointment by unique (integer) id</summary>
        /// <response code="200">Successfully retrieved</response>
        /// <response code="404">Model not found</response>
        [HttpGet("Detail/{id}")]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(AppointmentViewModel))]
        [SwaggerResponse((int)HttpStatusCode.NotFound, typeof(FeedBack))]
        public async Task<IActionResult> Detail([FromRoute]int id)
        {
            var feedback = new FeedBack();
            var appointment = await _appointmentService.FindAsync(id);
            if (appointment != null) return Ok(new AppointmentViewModel(appointment));
            feedback.err = true;
            feedback.message = $"Appointment with id {id} does not exists";
            return NotFound(feedback);
        }

        /// <summary>List of appointments by unique (string) nationalId</summary>
        /// <response code="200">Successfully retrieved</response>
        /// <response code="404">Model not found</response>
        [HttpGet("GetByNationalId/{nationalId}")]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(IEnumerable<AppointmentViewModel>))]
        [SwaggerResponse((int)HttpStatusCode.NotFound, typeof(FeedBack))]
        public async Task<IActionResult> GetByNationalId([FromRoute]string nationalId)
        {
            var feedback = new FeedBack();
            var appointments = await _appointmentService.GetByNationalIdAsync(nationalId);
            if (appointments.Any())
            {
                var appointmentsModels = appointments.Select(appointment => new AppointmentViewModel(appointment)).ToList();
                return Ok(appointmentsModels);
            }
            feedback.err = true;
            feedback.message = $"No appointment for national id {nationalId} was found";
            return NotFound(feedback);
        }

        /// <summary>Register a new appointment</summary>
        /// <response code="400">Bad request</response>
        /// <response code="200">Successfully retrieved</response>
        [HttpPost]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, typeof(FeedBack))]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(FeedBack))]
        public async Task<IActionResult> Post([FromBody]AppointmentCreationViewModel vModel)
        {
            var feedback = new FeedBack();

            if (!ModelState.IsValid)
            {
                feedback.err = true;
                feedback.message = "Invalid Model.";
                feedback.data = JsonConvert.SerializeObject(ModelState.Values.SelectMany(e => e.Errors).Select(x => x.ErrorMessage));
                return BadRequest(feedback);
            }
          
            var appointment = new Appointment(vModel);

            var exist = await _appointmentService.ExistsAsync(new Appointment(vModel));

            if (exist != null)
            {
                feedback.err = true;
                feedback.message = "You already have an appointment scheduled around that time.";
                feedback.data = JsonConvert.SerializeObject(new Dictionary<string, int> { { "id", appointment.Id } });
                return BadRequest(feedback);
            }
            var result = await _appointmentService.CreateAsync(appointment);

            if (result == 0)
            {
                feedback.err = true;
                feedback.message = "Appointment cannot be created now.";
                return BadRequest(feedback);
            }

            feedback.err = false;
            feedback.message = "Appointment created successfully";
            feedback.data = JsonConvert.SerializeObject(new Dictionary<string, int> { { "id", appointment.Id } });
            return Ok(feedback);
        }

        /// <summary>Update an appointment</summary>
        /// <response code="400">Bad request</response>
        /// <response code="200">Successfully retrieved</response>
        [HttpPut("{id}")]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, typeof(FeedBack))]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(FeedBack))]        
        public async Task<IActionResult> Put([FromRoute]int id, [FromBody]AppointmentViewModel vModel)
        {
            var feedback = new FeedBack();

            if (!ModelState.IsValid)
            {
                feedback.err = true;
                feedback.message = "Invalid Model.";
                feedback.data = JsonConvert.SerializeObject(ModelState.Values.SelectMany(e => e.Errors).Select(x => x.ErrorMessage));

                return BadRequest(feedback);
            }
            var apt =await  _appointmentService.FindAsync(id);

            if (apt == null)
            {
                feedback.err = true;
                feedback.message = "Invalid Request.";
                return BadRequest(feedback);
            }

            apt.Update(vModel);

            await _appointmentService.UpdateAsync(apt);

            feedback.err = false;
            feedback.message = "Appointment info updated successfully";
            feedback.data = JsonConvert.SerializeObject(new Dictionary<string, int> { { "Id", id } });
            return Ok(feedback);
        }

        /// <summary>Delete an appointment by unique (int) id</summary>
        /// <response code="400">Bad request</response>
        /// <response code="200">Successfully retrieved</response>
        [HttpDelete("{id}")]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, typeof(FeedBack))]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(FeedBack))]
        public async Task<IActionResult> Delete([FromRoute]int id)
        {
            var feedback = new FeedBack();
            var appointment = await _appointmentService.FindAsync(id);

            if (appointment == null)
            {
                feedback.err = true;
                feedback.message = "No appointment was found!";
                return BadRequest(feedback);
            }

            appointment.IsDeleted = DateTime.Now;
            var updateResult = await _appointmentService.UpdateAsync(appointment);
            if(updateResult != 0)
            {
                feedback.err = false;
                feedback.message = "Appointment Successfully updated";
                feedback.data = JsonConvert.SerializeObject(new Dictionary<string, int> { { "Appointment", appointment.Id } });
                return Ok(feedback);
            }
            //ToDo Mark deleted all stuff related to this entity

            feedback.err = true;
            feedback.message = "Appointment cannot be updated. Please try later";
            feedback.data = JsonConvert.SerializeObject(new Dictionary<string, int> { { "Appointment", appointment.Id } });

            return BadRequest(feedback);
        }


    }
}
