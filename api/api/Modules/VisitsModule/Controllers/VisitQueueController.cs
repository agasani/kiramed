using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api.Models;
using api.Services;
using api.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Net;

namespace api.Controllers
{
    [Route("api/[controller]")]
    public class VisitQueueController : Controller
    {
        public readonly IVisitQueueService _visitQueueService;

        public VisitQueueController(IVisitQueueService visitQueueService)
        {
            _visitQueueService = visitQueueService;
        }

        /// <summary>List all visit-queues within an organization by unique (integer) organizationId</summary>
        /// <response code="200">Retrieved successfully</response>
        /// <response code="404">Model not found</response>
        [HttpGet]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(IEnumerable<VisitQueueViewModel>))]
        [SwaggerResponse((int)HttpStatusCode.NotFound, typeof(FeedBack))]
        public async Task<IActionResult> Get([FromQuery]int organizationId)
        {
            var feedback = new FeedBack();

            //walkings
            var result = await _visitQueueService.GetAsync(v => v.OrganizationId == organizationId);
            var visitsQueues = result.ToList();

            //appointments
            if (visitsQueues.Any())
            {
                var visitQueueModels = visitsQueues.Select(v => new VisitQueueViewModel(v)).ToList();
                return Ok(visitQueueModels);
            }

            feedback.err = true;
            feedback.message = "No visits were found";
            return NotFound(feedback);
        }

        /// <summary>Get visit-queue by unique (integer) id</summary>
        /// <response code="200">Retrieved successfully</response>
        /// <response code="404">Model not found</response>
        [HttpGet("Detail/{id}")]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(VisitQueueViewModel))]
        [SwaggerResponse((int)HttpStatusCode.NotFound, typeof(FeedBack))]
        public async Task<IActionResult> Detail([FromRoute]int id)
        {
            var feedback = new FeedBack();
            var visit = await _visitQueueService.FindAsync(id);
            if (visit != null) return Ok(new VisitQueueViewModel(visit));
            feedback.err = true;
            feedback.message = $"Visit with id {id} does not exisits";
            return NotFound(feedback);
        }

        /// <summary>Register a new visit-queue</summary>
        /// <response code="400">Bad request</response>
        /// <response code="200">Created successfully</response>
        [HttpPost]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, typeof(FeedBack))]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(FeedBack))]
        public async Task<IActionResult> Post([FromBody]VisitQueueCreateViewModel vModel)
        {
            var feedback = new FeedBack();

            if (!ModelState.IsValid)
            {
                feedback.err = true;
                feedback.message = "Invalid Model.";
                feedback.data = JsonConvert.SerializeObject(ModelState.Values.SelectMany(e => e.Errors).Select(x => x.ErrorMessage));
                return BadRequest(feedback);
            }

            var visit = new VisitQueue(vModel);

            var result = await _visitQueueService.CreateAsync(visit);

            if (result == 0)
            {
                feedback.err = true;
                feedback.message = "Visit cannot be created now.";
                return BadRequest(feedback);
            }

            feedback.err = false;
            feedback.message = "Visit created successfully";
            feedback.data = JsonConvert.SerializeObject(new Dictionary<string, int> { { "Order Number", result } });
            return Ok(feedback);
        }
    } 
}