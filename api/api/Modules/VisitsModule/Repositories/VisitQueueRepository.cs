﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using api.Db;
using api.Models;

namespace api.Repositories
{
    public class VisitQueueRepository : KiraRepository<VisitQueue>, IVisitQueueRepository
    {
        private readonly KiraContext _context;

        public VisitQueueRepository(KiraContext context) : base(context)
        {
            _context = context;
        }

        public async Task<int> CreateBulkAsync(IEnumerable<IVisitQueue> visits)
        {
           var visitQueues = visits.Cast<VisitQueue>().ToList();
           _context.VisitQueue.AddRange(visitQueues);
           return await _context.SaveChangesAsync();
        }

        public async  Task<IEnumerable<VisitQueue>> LoadQueueAsync(Expression<Func<VisitQueue, bool>> expression)
        {
            var results = await GetAsync(expression);

            //TODO apply any needed work for loading visit queues;

            return results.ToList().OrderBy(x => x.QueueOrderNumber);
        }
    }
}
