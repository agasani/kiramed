﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api.Db;
using api.Models;

namespace api.Repositories
{
    public class VisityHistoryLogRepository : KiraRepository<VisitHistoryLog>, IVisityHistoryLogRepository
    {
        public VisityHistoryLogRepository(KiraContext context) : base(context)
        {
           
        }
    }
}
