﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api.Db;
using api.Models;

namespace api.Repositories
{
    public class AppointmentRepository : KiraRepository<Appointment>, IAppointmentRepository
    {
        private readonly KiraContext _context;

        public AppointmentRepository(KiraContext context): base(context)
        {

        }

        public async Task<Appointment> ExistsAsync(Appointment appointment)
        {
            var apt = await GetAsync(
                a => (a.NationalId == appointment.NationalId || a.PhoneNumber == appointment.PhoneNumber) && Math.Abs(appointment.ArrivalDateTime.Subtract(a.ArrivalDateTime).TotalHours) <= 2);
            return apt.FirstOrDefault();
        }

        public Appointment Exists(Appointment appointment)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<Appointment>> GetByNationalIdAsync(string nationalId)
        {
            return await GetAsync(a => a.NationalId == nationalId);
        }
    }
}
