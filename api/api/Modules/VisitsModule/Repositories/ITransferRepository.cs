﻿using api.Models;

namespace api.Repositories
{
    public interface ITransferRepository : IKiraRepository<Transfer>
    {
    }
}
