﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api.Db;
using api.Models;

namespace api.Repositories
{
    public class VisitRepository : KiraRepository<Visit>, IVisitRepository
    {
        public VisitRepository(KiraContext context) : base(context)
        {

        }
    }
}
