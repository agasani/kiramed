﻿using System.Collections.Generic;
using System.Threading.Tasks;
using api.Models;

namespace api.Repositories
{
    public interface IVisitQueueRepository : IKiraRepository<VisitQueue>, IQueuableRepository<VisitQueue>
    {
        Task<int> CreateBulkAsync(IEnumerable<IVisitQueue> visits);
    }
}
