﻿using api.Db;
using api.Models;

namespace api.Repositories
{
    public class TransferRepository : KiraRepository<Transfer>, ITransferRepository
    {
        public TransferRepository(KiraContext context) : base(context)
        {

        }
    }
}
