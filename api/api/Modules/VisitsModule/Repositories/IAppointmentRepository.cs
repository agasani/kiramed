﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api.Models;

namespace api.Repositories
{
    public interface IAppointmentRepository : IKiraRepository<Appointment>
    {       
        //check if it exists
        Task<Appointment> ExistsAsync(Appointment appointment);
        Appointment Exists(Appointment appointment);
        Task<IEnumerable<Appointment>> GetByNationalIdAsync(string nationalId);
    }
}
