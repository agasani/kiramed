﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using api.Models;

namespace api.Db
{
    public partial class KiraContext
    {
        public DbSet<Visit> Visits { get; set; }
        public DbSet<VisitQueue> VisitQueue { get; set; }
        public DbSet<VisitHistoryLog> VisitHistoryLogs { get; set; }
        public DbSet<Appointment> Appointments { get; set; }
        public DbSet<Transfer> Transfers { get; set; }
    }
}