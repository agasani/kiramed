using api.Enums;

namespace api.Models
{
    public class TransferQueueDTO: VisitQueue
    {      
        public TransferQueueDTO() { }

        public TransferQueueDTO(TransferViewModel transfer)
        {
            OrganizationId = transfer.ReceivingOrganizationId;         
            FirstName = transfer.PatientFirstName;
            LastName = transfer.PatientLastName;
            VisitType = eVisitType.Appointment;
            Relationship = transfer.Relationship;
            ArrivalDateTime = transfer.ArrivalDateTime;
        }
    }
}
