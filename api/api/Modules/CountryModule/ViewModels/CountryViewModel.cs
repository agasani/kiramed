﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace api.Models
{
    public class CountryViewModel
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(30)]
        public string Name { get; set; }
        [Required]
        public string Code { get; set; }
        [Required]
        public string Abbreviation { get; set; }
        public string OfficialLanguage { get; set; }
        public string Currency { get; set; }
        public bool IsDeleted { get; set; }

        public CountryViewModel(Country country)
        {
            Id = country.Id;
            Name = country.Name;
            Code = country.Code;
            Abbreviation = country.Abbreviation;
            OfficialLanguage = country.OfficialLanguage;
            Currency = country.Currency;
            IsDeleted = country.IsDeleted <= DateTime.Now;
        }
    }
}
