﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using api.ViewModels;

namespace api.Models
{
    public class CountryRegistrationViewModel : AdminRegistrationViewModel
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(30)]
        public string Name { get; set; }
        [Required]
        public string Code { get; set; }
        [Required]
        public string Abbreviation { get; set; }
        public string OfficialLanguage { get; set; }
        public string Currency { get; set; }

        public CountryRegistrationViewModel(Country country)
        {
            Name = country.Name;
            Code = country.Code;
            Abbreviation = country.Abbreviation;
            OfficialLanguage = country.OfficialLanguage;
            Currency = country.Currency;
        }

        public  CountryRegistrationViewModel () {}

    }
}
