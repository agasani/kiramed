﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Nest;

namespace api.Models
{
    [Table("Country")]
    [ElasticsearchType(Name = "Country")]
    public class Country : KiraEntity
    {
        [Required]
        [MaxLength(30)]
        public string Name { get; set; }
        [Required]
        public string Code { get; set; }
        [Required]
        public string Abbreviation { get; set; }
        public string OfficialLanguage { get; set; }
        public string Currency { get; set; }

        public Country() { }
        public Country(CountryViewModel model)
        {
            Id = model.Id;
            Name = model.Name;
            Code = model.Code;
            Abbreviation = model.Abbreviation;
            OfficialLanguage = model.OfficialLanguage;
            Currency = model.Currency;
        }

        public Country(CountryRegistrationViewModel vModel)
        {
            Name = vModel.Name;
            Code = vModel.Code;
            Abbreviation = vModel.Abbreviation;
            OfficialLanguage = vModel.OfficialLanguage;
            Currency = vModel.Currency;    
            
        }

        public void Update(CountryViewModel vModel)
        {
            if (!string.IsNullOrEmpty(vModel.Name))
                this.Name = vModel.Name;
            if (!string.IsNullOrEmpty(vModel.Code))
                this.Code = vModel.Code;
            if (!string.IsNullOrEmpty(vModel.Abbreviation))
                this.Abbreviation = vModel.Abbreviation;
            if (!string.IsNullOrEmpty(vModel.OfficialLanguage))
                this.OfficialLanguage = vModel.OfficialLanguage;
            if (!string.IsNullOrEmpty(vModel.Currency))
                this.Currency = vModel.Currency;      
        }
    }
}
