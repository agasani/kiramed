﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api.Models
{
    public interface ICountryRepository : IKiraRepository<Country>
    {
        Task<Country> GetByCountryCodeAsync(string countryCode);
        Task<IEnumerable<Organization>> GetCountryOrganizations(int countryId);
        Task<Country> ExistsAsync(Country country);
    }
}
