﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using api.Db;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace api.Models
{
    public class CountryRepository : KiraRepository<Country>, ICountryRepository
    {
        public readonly KiraContext _context;
        public readonly IOrganizationRepository _organizationRepository;

        public CountryRepository(KiraContext context, IOrganizationRepository organizationRepository ) : base(context)
        {
            _context = context;
            _organizationRepository = organizationRepository;
        }

        public async Task<Country> ExistsAsync(Country country)
        {
            var co = await _context.Countries.FirstOrDefaultAsync(
                c => c.Code == country.Code);
            return co;
        }

        public async  Task<Country> GetByCountryCodeAsync(string countryCode)
        {
           var country = await _context.Countries.FirstOrDefaultAsync(e => e.Code == countryCode);         
           return country;
        }

        public async Task<IEnumerable<Organization>> GetCountryOrganizations(int countryId)
        {
            return await _organizationRepository.GetAsync(o => o.CountryId == countryId);

        }
    }
}
