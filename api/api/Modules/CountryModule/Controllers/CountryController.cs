﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using api.Auth;
using api.Services;
using api.Utils;
using api.ViewModels;
using System.Net;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace api.Controllers
{
    [Route("api/[controller]")]
    [ControllerFilters.ValidateModel]
    public class CountryController : Controller
    {
        private readonly ICountryRepository _countryRepository;
        private readonly IUserService _userService;

        public CountryController(ICountryRepository countryRepository, IUserService userService)
        {
            _countryRepository = countryRepository;
            _userService = userService;
        }

        //this method will more likely be used by admins

        /// <summary>List all countries</summary>
        /// <response code="404">Model not found</response>
        /// <response code="200">Successfully listed</response>
        [HttpGet]
        [SwaggerResponse((int)HttpStatusCode.NotFound, typeof(FeedBack))]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(IEnumerable<CountryViewModel>))]
        public async Task<IActionResult> Get()
        {
            var results = await _countryRepository.GetAllAsync();
            if (!results.Any())
            {
                var feedback = new FeedBack() { err = true, message = "No Country was found.", data = "" };
                return NotFound(feedback);
            }

            var countries = results.Select(co => new CountryViewModel(co)).ToList();
            return Ok(countries);
        }

        /// <summary>Retrieve country by unique (integer) id</summary>
        /// <response code="404">Model not found</response>
        /// <response code="200">Successfully retrieved</response>
        [HttpGet("GetById/{id}")]
        [SwaggerResponse((int)HttpStatusCode.NotFound, typeof(FeedBack))]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(CountryViewModel))]
        public async Task<IActionResult> GetById([FromRoute]int id)
        {
            var co = await _countryRepository.FindAsync(id);
            if (co == null)
            {
                var feedback = new FeedBack() { err = true, message = $"Country with {id} does not exist.", data = "" };
                return NotFound(feedback);
            }

            var vMod = new CountryViewModel(co);
            return Ok(vMod);
        }

        /// <summary>Retrieve country by unique (string) code</summary>
        /// <response code="404">Model not found</response>
        /// <response code="200">Successfully retrieved</response>
        [HttpGet("GetByCode/{code}")]
        [SwaggerResponse((int)HttpStatusCode.NotFound, typeof(FeedBack))]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(CountryViewModel))]
        public async Task<IActionResult> GetByCode([FromRoute]string code)
        {
            var co = await _countryRepository.GetByCountryCodeAsync(code);
            if (co == null)
            {
                var feedback = new FeedBack() { err = true, message = $" Country with {code} does not exist.", data = "" };
                return NotFound(feedback);
            }
            var vMod = new CountryViewModel(co);
            return Ok(vMod);
        }

        /// <summary>Create a new country</summary>
        /// <response code="400">Bad request</response>
        /// <response code="200">Successfully created</response>
        [HttpPost]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, typeof(FeedBack))]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(FeedBack))]
        public async Task<IActionResult> Post([FromBody]CountryRegistrationViewModel vModel)
        {
            var feedback = new FeedBack();

            if (!ModelState.IsValid)
            {
                feedback.err = true;

                feedback.message = eFeedbackMessages.InvalidModel.GetDescription();
                feedback.data = JsonConvert.SerializeObject(ModelState.Values.SelectMany(e => e.Errors).Select(x => x.ErrorMessage));
                return BadRequest(feedback);
            }

            var newCountry = new Country(vModel);

            var country = await _countryRepository.ExistsAsync(newCountry);
            if (country != null)
            {
                if (country.IsDeleted > DateTime.Now)
                {
                    feedback.err = true;
                    feedback.message = "Country already exists with similar information.";
                    feedback.data = JsonConvert.SerializeObject(new Dictionary<string, int> { { "Id", country.Id } });
                    return BadRequest(feedback);
                }
            }

            var result = await _countryRepository.CreateAsync(newCountry);
            if (result != 0)
            {
                feedback.err = false;
                feedback.message = eFeedbackMessages.ObjectCreatedSuccessfully.GetDescription();

                var adminRegisterViewModel = new UserRegisterViewModel(vModel);
                adminRegisterViewModel.OrganizationId = result;

                var adminFeedBack = await _userService.CreateAsync(adminRegisterViewModel);
                var feedbackDict = new Dictionary<string, string> { { "id", result.ToString() } };

                var adminFeedbackDict = JsonConvert.DeserializeObject<Dictionary<string, string>>(adminFeedBack.data);
                string adminId = "", adminUsername = "";
                adminFeedbackDict.TryGetValue("id", out adminId);
                adminFeedbackDict.TryGetValue("username", out adminUsername);
                feedbackDict.Add("adminId", adminId);
                feedbackDict.Add("adminUsername", adminUsername);

                feedback.message += "; " + adminFeedBack.message;
                feedback.data = JsonConvert.SerializeObject(feedbackDict);
            }
            else
            {
                feedback.err = true;
                feedback.message = "Creating the country failed";
            }

            return Ok(feedback);
        }

        /// <summary>Update country by unique (integer) id</summary>
        /// <response code="400">Bad request</response>
        /// <response code="200">Successfully updated</response>
        [HttpPut("{id}")]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, typeof(FeedBack))]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(FeedBack))]
        public async Task<IActionResult> Put([FromRoute]int id, [FromBody]CountryViewModel vModel)
        {
            var feedback = new FeedBack();

            var co = await _countryRepository.FindAsync(id);

            if (co == null)
            {
                feedback.err = true;
                feedback.message = "Invalid Request.";
                return BadRequest(feedback);
            }

            co.Update(vModel);

            await _countryRepository.UpdateAsync(co);

            feedback.err = false;
            feedback.message = "Country info updated successfully";
            feedback.data = JsonConvert.SerializeObject(new Dictionary<string, int> { { "Id", id } });
            return Ok(feedback);
        }

        /// <summary>Delete country by unique (integer) id</summary>
        /// <response code="400">Bad request</response>
        /// <response code="200">Successfully deleted</response>
        [HttpDelete("{id}")]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, typeof(FeedBack))]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(FeedBack))]
        public async Task<IActionResult> Delete([FromRoute]int id)
        {
            var feedback = new FeedBack();
            var co = await _countryRepository.FindAsync(id);

            if (co == null)
            {
                feedback.err = true;
                feedback.message = "Deleted!";
                return BadRequest(feedback);
            }

            co.IsDeleted = DateTime.Now;
            await _countryRepository.UpdateAsync(co);

            feedback.err = false;
            feedback.message = "Country Successfully updated";
            feedback.data = JsonConvert.SerializeObject(new Dictionary<string, int> { { "Id", co.Id } });

            return Ok(feedback);
        }

        /// <summary>List country roles by unique (integer) countryId</summary>
        /// <response code="404">Model not found</response>
        /// <response code="200">Successfully listed</response>
        [HttpGet("GetCountryOrganizations/{countryId}")]
        [SwaggerResponse((int)HttpStatusCode.NotFound, typeof(FeedBack))]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(IEnumerable<OrganizationViewModel>))]
        public async Task<IActionResult> GetCountryOrganizations([FromRoute]int countryId)
        {
            var feedback = new FeedBack();
            var country = await _countryRepository.FindAsync(countryId);
            if (country == null)
            {
                feedback.err = true;
                feedback.message = "The country does not exist";
                return NotFound(feedback);
            }
            var orgs = await _countryRepository.GetCountryOrganizations(countryId);
            var organizations = orgs.Select(org => new OrganizationViewModel(org)).ToList();
            if (organizations.Count != 0) return Ok(organizations);
            feedback.err = true;
            feedback.message = "No organization was found!";
            return NotFound(feedback);
        }
    }
}
