﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api.Db;
using api.Enums;
using api.Models;
using api.Repositories;
using api.Utils;

namespace api.Services
{
    public class VitalSignService : VitalSignRepository, IVitalSignService
    {
        private readonly IVitalSignMeasureRepository _vitalSignMeasures;

        public VitalSignService(KiraContext context, IVitalSignMeasureRepository vitalSignMeasures) : base(context)
        {
            _vitalSignMeasures = vitalSignMeasures;
        }

        public async Task<string> EvaluateAsync(double age, eAgeUnits ageUnit, int vitalSignTypeId, decimal value)
        {
            var measure = await _vitalSignMeasures.GetAsync(m => m.VitalSignTypeId == vitalSignTypeId && m.AgeRange == ageUnit && m.MinAge <= age && m.MaxAge >= age);
            var vitalSignMeasure = measure.FirstOrDefault();

            if (vitalSignMeasure != null && vitalSignMeasure.MinValue > value)
            {
                return eSeverity.SevereLow.GetDescription();
            }

            if (vitalSignMeasure != null && vitalSignMeasure.MaxValue < value)
            {
                return eSeverity.SevereHigh.GetDescription();
            }
            return eSeverity.Normal.GetDescription();
        }
    }
}
