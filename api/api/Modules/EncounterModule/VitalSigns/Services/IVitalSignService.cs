﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api.Models;
using api.Repositories;

namespace api.Services
{
    public interface IVitalSignService : IVitalSignRepository
    {
        Task<string> EvaluateAsync(double age, eAgeUnits ageUnit, int vitalSignTypeId, decimal value);
    }
}
