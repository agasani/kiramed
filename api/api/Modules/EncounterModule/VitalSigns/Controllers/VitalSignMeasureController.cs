﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api.Models;
using api.Repositories;
using api.Utils;
using api.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Net;

namespace api.Controllers
{
    [Route("api/[controller]")]
    public class VitalSignMeasureController : Controller
    {      
        public readonly IVitalSignMeasureRepository _vitalSignMeasureRepository;
        public readonly IVitalSignTypeRepository _vitalSignTypeRepository;


        public VitalSignMeasureController(IVitalSignMeasureRepository vitalSignMeasureRepository, IVitalSignTypeRepository vitalSignTypeRepository)
        {
            _vitalSignMeasureRepository = vitalSignMeasureRepository;
            _vitalSignTypeRepository = vitalSignTypeRepository;
        }

        /// <summary>List all vital sign measures</summary>
        /// <response code="200">Listed successfully</response>
        /// <response code="404">Model not found</response>
        [HttpGet]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(IEnumerable<VitalSignMeasureViewModel>))]
        [SwaggerResponse((int)HttpStatusCode.NotFound, typeof(FeedBack))]
        public async Task<IActionResult> Get()
        {
            var feedback = new FeedBack();
            var result = await _vitalSignMeasureRepository.GetAllAsync();

            var vitalSignMeasures = result.ToList();
            if (vitalSignMeasures.Any())
            {
                var vitalSignModels = vitalSignMeasures.Select(v => new VitalSignMeasureViewModel(v)).ToList();
                return Ok(vitalSignModels);
            }

            feedback.err = true;
            feedback.message = "No vital signs measures were found";
            return NotFound(feedback);
        }

        /// <summary>List all vital sign measures by type with (integer) typeId</summary>
        /// <response code="200">Listed successfully</response>
        /// <response code="404">Model not found</response>
        [HttpGet("GetByType/{typeId}")]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(IEnumerable<VitalSignMeasureViewModel>))]
        [SwaggerResponse((int)HttpStatusCode.NotFound, typeof(FeedBack))]
        public async Task<IActionResult> GetByType([FromRoute]int typeId)
        {
            var feedback = new FeedBack();
            var result = await _vitalSignMeasureRepository.GetAsync(m => m.VitalSignTypeId == typeId);

            var vitalSignMeasures = result.ToList();
            if (vitalSignMeasures.Any())
            {
                var vitalSignModels = vitalSignMeasures.Select(v => new VitalSignMeasureViewModel(v)).ToList();
                return Ok(vitalSignModels);
            }

            feedback.err = true;
            feedback.message = "No vital signs measures were found";
            return NotFound(feedback);
        }

        /// <summary>Retrieve vital sign measure detail by (integer) id</summary>
        /// <response code="200">Listed successfully</response>
        /// <response code="404">Model not found</response>
        [HttpGet("Detail/{id}")]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(VitalSignMeasureViewModel))]
        [SwaggerResponse((int)HttpStatusCode.NotFound, typeof(FeedBack))]        
        public async Task<IActionResult> Detail([FromRoute]int id)
        {
            var feedback = new FeedBack();
            var result = await _vitalSignMeasureRepository.GetAsync(m => m.Id == id);

            var vitalSignMeasure = result.FirstOrDefault();

            if (vitalSignMeasure != null)
            {
                var vitalSignModel = new VitalSignMeasureViewModel(vitalSignMeasure);
                return Ok(vitalSignModel);
            }

            feedback.err = true;
            feedback.message = "No vital sign measure was found";
            return NotFound(feedback);
        }

        /// <summary>Register a new vital sign measure</summary>
        /// <response code="200">Listed successfully</response>
        /// <response code="400">Bad request</response>
        [HttpPost]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(FeedBack))]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, typeof(FeedBack))]
        public async Task<IActionResult> Post([FromBody]VitalSignMeasureCreateViewModel vModel)
        {
            var feedback = new FeedBack();

            if (!ModelState.IsValid)
            {
                feedback.err = true;

                feedback.message = eFeedbackMessages.InvalidModel.GetDescription();
                feedback.data = JsonConvert.SerializeObject(ModelState.Values.SelectMany(e => e.Errors).Select(x => x.ErrorMessage));
                return BadRequest(feedback);
            }

            var newMeasure = new VitalSignMeasure(vModel);

            var type = await _vitalSignTypeRepository.FindAsync(vModel.VitalSignTypeId);
            if (type == null)
            {

                feedback.err = true;
                feedback.message = "Vital sign type does not exist.";
                return BadRequest(feedback);
            }

            newMeasure.Type = type;

            var result = await _vitalSignMeasureRepository.CreateAsync(newMeasure);
            if (result != 0)
            {
                feedback.err = false;
                feedback.message = eFeedbackMessages.ObjectCreatedSuccessfully.GetDescription();
                feedback.data = JsonConvert.SerializeObject(new Dictionary<string, int> { { "Id", result } });
                return Ok(feedback);
            }
            feedback.err = true;
            feedback.message = "Creating the measure failed";
            return BadRequest(feedback);

        }
    } 
}
