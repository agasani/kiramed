﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api.Auth;
using api.Models;
using api.Repositories;
using api.Utils;
using api.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Net;

namespace api.Modules.EncounterModule.VitalSigns.Controllers
{
    [Route("api/[controller]")]
    [ControllerFilters.ValidateModel]
    public class VitalSignTypeController : Controller
    {
        public readonly IVitalSignTypeRepository _vitalSignTypeRepository;

        public VitalSignTypeController(IVitalSignTypeRepository vitalSignTypeRepository)
        {
            _vitalSignTypeRepository = vitalSignTypeRepository;
        }

        /// <summary>List all vital sign types</summary>
        /// <response code="200">Listed successfully</response>
        /// <response code="404">Model not found</response>
        [HttpGet]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(IEnumerable<VitalSignTypeViewModel>))]
        [SwaggerResponse((int)HttpStatusCode.NotFound, typeof(FeedBack))]
        public async Task<IActionResult> Get()
        {
            var feedback = new FeedBack();
            var result = await _vitalSignTypeRepository.GetAllAsync();

            var vitalSignTypes = result.ToList();
            if (vitalSignTypes.Any())
            {
                var vitalSignModels = vitalSignTypes.Select(v => new VitalSignTypeViewModel(v)).ToList();
                return Ok(vitalSignModels);
            }

            feedback.err = true;
            feedback.message = "No vital signs types were found";
            return NotFound(feedback);
        }

        /// <summary>Retrieve vital sign type by (integer) typeId</summary>
        /// <response code="200">Retrieved successfully</response>
        /// <response code="404">Model not found</response>
        [HttpGet("Detail/{id}")]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(VitalSignTypeViewModel))]
        [SwaggerResponse((int)HttpStatusCode.NotFound, typeof(FeedBack))]
        public async Task<IActionResult> Detail([FromRoute]int id)
        {
            var feedback = new FeedBack();
            var result = await _vitalSignTypeRepository.GetAsync(m => m.Id == id);

            var vitalSignType = result.FirstOrDefault();

            if (vitalSignType != null)
            {
                var vitalSignModel = new VitalSignTypeViewModel(vitalSignType);
                return Ok(vitalSignModel);
            }

            feedback.err = true;
            feedback.message = "No vital sign type was found";
            return NotFound(feedback);
        }

        /// <summary>Register vital sign type</summary>
        /// <response code="400">Bad request</response>
        /// <response code="200">Retrieved successfully</response>
        [HttpPost]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, typeof(FeedBack))]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(FeedBack))]
        public async Task<IActionResult> Post([FromBody]VitalSignTypeViewModel vModel)
        {
            var feedback = new FeedBack();

            //if (!ModelState.IsValid)
            //{
            //    feedback.err = true;

            //    feedback.message = eFeedbackMessages.InvalidModel.GetDescription();
            //    feedback.data = JsonConvert.SerializeObject(ModelState.Values.SelectMany(e => e.Errors).Select(x => x.ErrorMessage));
            //    return BadRequest(feedback);
            //}

            var newType = new VitalSignType(vModel);

            var type = await _vitalSignTypeRepository.ExistAsync(newType);
            if (type != null)
            {
                if (type.IsDeleted > DateTime.Now)
                {
                    feedback.err = true;
                    feedback.message = "Vital sign type already exists with similar information.";
                    feedback.data = JsonConvert.SerializeObject(new Dictionary<string, int> { { "Id", type.Id } });
                    return BadRequest(feedback);
                }
            }

            var result = await _vitalSignTypeRepository.CreateAsync(newType);
            if (result != 0)
            {
                feedback.err = false;
                feedback.message = eFeedbackMessages.ObjectCreatedSuccessfully.GetDescription();
                feedback.data = JsonConvert.SerializeObject(new Dictionary<string, int> { { "Id", result } });
                return Ok(feedback);
            }
            feedback.err = true;
            feedback.message = "Creating the country failed";
            return BadRequest(feedback);

        }
    }
}

