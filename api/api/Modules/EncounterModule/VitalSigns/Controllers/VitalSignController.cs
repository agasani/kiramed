﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api.Models;
using api.Repositories;
using api.Services;
using api.Utils;
using api.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Net;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace api.Controllers
{
    [Route("api/[controller]")]
    [ControllerFilters.ValidateModel]
    public class VitalSignController : Controller
    {
        public readonly IVitalSignService _vitalSignService;
        public readonly IVitalSignTypeRepository _vitalSignTypeRepository;
        public readonly IEncounterService _encounterService;
        public readonly IPatientRepository _patientRepository;

        public VitalSignController(IVitalSignService vitalSignService, IVitalSignTypeRepository vitalSignTypeRepository, IEncounterService encounterService, IPatientRepository patientRepository)
        {
            _vitalSignService = vitalSignService;
            _vitalSignTypeRepository = vitalSignTypeRepository;
            _encounterService = encounterService;
            _patientRepository = patientRepository;
        }

        /// <summary>List all vital signs by (integer) patientId</summary>
        /// <response code="200">Listed successfully</response>
        /// <response code="404">Model not found</response>
        [HttpGet]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(IEnumerable<VitalSignViewModel>))]
        [SwaggerResponse((int)HttpStatusCode.NotFound, typeof(FeedBack))]
        public async Task<IActionResult> Get([FromQuery]int patientId)
        {
            var feedback = new FeedBack();
            var result = await _vitalSignService.GetAsync(v => v.Encounter.PatientId == patientId);

            var vitalSigns = result.ToList();
            if (vitalSigns.Any())
            {
                var vitalSignModels = vitalSigns.Select(v => new VitalSignViewModel(v)).ToList();
                return Ok(vitalSignModels);
            }

            feedback.err = true;
            feedback.message = "No vital signs were found";
            return NotFound(feedback);
        }

        /// <summary>List all vital signs within an encounter by (integer) encounterId</summary>
        /// <response code="200">Listed successfully</response>
        /// <response code="404">Model not found</response>
        [HttpGet("GetByEncounter/{encounterId}")]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(IEnumerable<VitalSignViewModel>))]
        [SwaggerResponse((int)HttpStatusCode.NotFound, typeof(FeedBack))]
        public async Task<IActionResult> GetByEncounter([FromRoute]int encounterId)
        {
            var feedback = new FeedBack();
            var result = await _vitalSignService.GetAsync(v => v.Encounter.Id == encounterId);

            var vitalSigns = result.ToList();
            if (vitalSigns.Any())
            {
                var vitalSignModels = vitalSigns.Select(v => new VitalSignViewModel(v)).ToList();
                return Ok(vitalSignModels);
            }

            feedback.err = true;
            feedback.message = "No vital signs were found";
            return NotFound(feedback);
        }

        /// <summary>Retrieve vital sign detail by (integer) id</summary>
        /// <response code="200">Retrieved successfully</response>
        /// <response code="404">Model not found</response>
        [HttpGet("Detail/{id}")]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(VitalSignViewModel))]
        [SwaggerResponse((int)HttpStatusCode.NotFound, typeof(FeedBack))]
        public async Task<IActionResult> Detail([FromRoute]int id)
        {
            var feedback = new FeedBack();
            var result = await _vitalSignService.GetAsync(v => v.Id == id);

            var vitalSign = result.FirstOrDefault();

            if (vitalSign != null)
            {
                var vitalSignModel = new VitalSignViewModel(vitalSign);
                return Ok(vitalSignModel);
            }

            feedback.err = true;
            feedback.message = "No vital sign was found";
            return NotFound(feedback);
        }

        //Not needed because you can call VitalSigntypes get all
        //[HttpGet("New")]
        //public async Task<IActionResult> Create()
        //{
        //    var feedback = new FeedBack();
        //    var result = await _VitalSignTypeRepository.GetAllAsync();
        //    var vitalSignTypes = result.ToList();

        //    if (!vitalSignTypes.Any())
        //    {
        //        feedback.err = true;
        //        feedback.message = "Please create vital sign types first!";
        //        return BadRequest(feedback);
        //    }

        //    var vitalSignModels = vitalSignTypes.Select(v => new VitalSignTypeViewModel(v)).ToList();
        //    return Ok(vitalSignModels);
        //}

        /// <summary>Register vital sign detail</summary>
        /// <response code="400">Bad request</response>
        /// <response code="200">Saved successfully</response>
        [HttpPost]
        [SwaggerResponse((int)HttpStatusCode.NotFound, typeof(FeedBack))]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(FeedBack))]
        public async Task<IActionResult> Post([FromBody]VitalSignCreateViewModel vModel)
        {
            var feedback = new FeedBack();

            if (!ModelState.IsValid)
            {
                feedback.err = true;

                feedback.message = eFeedbackMessages.InvalidModel.GetDescription();
                feedback.data = JsonConvert.SerializeObject(ModelState.Values.SelectMany(e => e.Errors).Select(x => x.ErrorMessage));
                return BadRequest(feedback);
            }

            var newVital = new VitalSign(vModel);

            var type = await _vitalSignTypeRepository.FindAsync(vModel.VitalSignTypeId);
            if (type == null)
            {
               
                    feedback.err = true;
                    feedback.message = "Vital sign type does not exist.";
                    return BadRequest(feedback);
               
            }

            newVital.Type = type;

            var encounter = await _encounterService.FindAsync(vModel.EncounterId);

            if (encounter == null)
            {

                feedback.err = true;
                feedback.message = "Encounter does not exist.";
                return BadRequest(feedback);

            }

            newVital.Encounter = encounter;

            var result = await _vitalSignService.CreateAsync(newVital);

            if (result != 0)
            {
                var patient = await _patientRepository.FindAsync(newVital.Encounter.PatientId);
                var patientDOB = patient.Entity.Demographic.DOB;

                var age = DateTimeUtils.GetAge(patientDOB);

                var severity = await _vitalSignService.EvaluateAsync(age.Age, age.AgeUnit, newVital.VitalSignTypeId,
                    (decimal) newVital.Value);

                feedback.err = false;
                var data = new  List<object>();

                var severetyData = new Dictionary<string, string> { {"Severity", severity}};
                var entityData = new Dictionary<string, int> {{"Id", result}};

                data.Add(severetyData);
                data.Add(entityData);

                feedback.message = eFeedbackMessages.ObjectCreatedSuccessfully.GetDescription();
                feedback.data = JsonConvert.SerializeObject(data);
                return Ok(feedback);
            }
            feedback.err = true;
            feedback.message = "Creating the vital sign failed";
            return BadRequest(feedback);

        }
        
    }
}
