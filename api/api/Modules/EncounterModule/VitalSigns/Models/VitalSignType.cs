﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using api.ViewModels;

namespace api.Models
{
    [Table("VitalSignType")]
    public class VitalSignType: KiraEntity
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public string Unit { get; set; }
        public bool IsUsed { get; set; } = true;

        public VitalSignType(VitalSignTypeViewModel model)
        {
            Name = model.Name;
            Description = model.Description;
            Unit = model.Unit;
        }

        public VitalSignType()
        {
            
        }
    }
}
