﻿using System.ComponentModel.DataAnnotations.Schema;
using api.ViewModels;

namespace api.Models
{
    [Table("VitalSignMeasure")]
    public class VitalSignMeasure : KiraEntity
    {
        public int VitalSignTypeId { get; set; }
        public virtual  VitalSignType Type { get; set; }
        public eAgeUnits AgeRange { get; set; }
        public double MinAge { get; set; }
        public double MaxAge { get; set; }
        public decimal MinValue { get; set; }
        public decimal MaxValue { get; set; }

        public VitalSignMeasure()
        {
            
        }

        public VitalSignMeasure(VitalSignMeasureCreateViewModel model)
        {
            VitalSignTypeId = model.VitalSignTypeId;
            AgeRange = model.AgeRange;
            MinAge = model.MinAge;
            MaxAge = model.MaxAge;
            MinValue = model.MinValue;
            MaxValue = model.MaxValue;
        }
    }
}
