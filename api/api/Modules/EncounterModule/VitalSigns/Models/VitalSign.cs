﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using api.Enums;
using api.ViewModels;

namespace api.Models
{
    [Table("VitalSigns")]
    public class VitalSign : KiraEntity, IEncounterElement
    {
        public double Value { get; set; }
        public int VitalSignTypeId { get; set; }
        public virtual VitalSignType Type { get; set; }
        public int EncounterId { get; set; }
        public DateTime VisitDateTime { get; set; } = DateTime.Now;
        public virtual Encounter Encounter { get; set; }
        public string CreatedBy { get; set; }
        public string Notes { get; set; }
        public eMeasureSeverity Severity { get; set; }


        public VitalSign()
        {
            
        }

        public VitalSign(VitalSignCreateViewModel model)
        {
            Value = model.Value;
            VitalSignTypeId = model.VitalSignTypeId;
            EncounterId = model.EncounterId;
            Notes = model.Notes;
        }
    }
}
