﻿
using System.ComponentModel.DataAnnotations;
using api.Models;

namespace api.ViewModels
{
    public class VitalSignTypeViewModel
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public string Unit { get; set; }
        public int VitalSignTypeId { get; set; }
      //  public bool IsUsed { get; set; } 

        public VitalSignTypeViewModel(VitalSignType type)
        {
            Name = type.Name;
            Description = type.Description;
            Unit = type.Unit;
            VitalSignTypeId = type.Id;
        }

        public VitalSignTypeViewModel()
        {
            
        }
    }
}
