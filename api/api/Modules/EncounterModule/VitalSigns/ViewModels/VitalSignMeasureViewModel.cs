﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api.Models;
using api.Utils;

namespace api.ViewModels
{
    public class VitalSignMeasureViewModel
    {
        public int VitalSignTypeId { get; set; }
        public string Type { get; set; }
        public string AgeRange { get; set; }
        public double MinAge { get; set; }
        public double MaxAge { get; set; }
        public decimal MinValue { get; set; }
        public decimal MaxValue { get; set; }

        public VitalSignMeasureViewModel( VitalSignMeasure measure)
        {
            VitalSignTypeId = measure.Id;
            Type = measure.Type.Name;
            AgeRange = measure.AgeRange.GetDescription();  
            MinAge = measure.MinAge;
            MaxAge = measure.MaxAge;
            MinValue = measure.MinValue;
            MaxValue = measure.MaxValue;
        }
    }

   
}
