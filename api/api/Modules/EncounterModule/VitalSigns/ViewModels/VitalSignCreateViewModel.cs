﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace api.ViewModels
{
    public class VitalSignCreateViewModel
    {
        [Required]
        public int VitalSignTypeId { get; set; }
        [Required]
        public int EncounterId { get; set; }
        [Required]
        public double Value { get; set; }
        public string Notes { get; set; }
    }
}
