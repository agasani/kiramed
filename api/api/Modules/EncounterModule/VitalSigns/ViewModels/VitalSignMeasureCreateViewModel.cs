﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using api.Models;

namespace api.ViewModels
{
    public class VitalSignMeasureCreateViewModel
    {
        public int VitalSignTypeId { get; set; }
        [Required]
        public eAgeUnits AgeRange { get; set; }
        [Required]
        public double MinAge { get; set; }
        [Required]
        public double MaxAge { get; set; }
        [Required]
        public decimal MinValue { get; set; }
        [Required]
        public decimal MaxValue { get; set; }
    }
}
