﻿using System;
using api.Models;
using api.Utils;

namespace api.ViewModels
{
    public class VitalSignViewModel
    {
        public string Type { get; set; }
        public DateTime DateTimeTaken { get; set; }
        public string Unit { get; set; }
        public int EncounterId { get; set; }
        public double Value { get; set; }
        public string Severity { get; set; }
        public int Id { get; set; }


        public VitalSignViewModel(VitalSign vitalSign)
        {
            Type = vitalSign.Type.Name;
            DateTimeTaken = vitalSign.VisitDateTime;
            Unit = vitalSign.Type.Unit;
            EncounterId = vitalSign.EncounterId;
            Value = vitalSign.Value;
            Severity = vitalSign.Severity.GetDescription();
            Id = vitalSign.Id;
        }
    }
}
