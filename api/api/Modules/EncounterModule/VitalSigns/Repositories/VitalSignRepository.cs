﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using api.Db;
using api.Models;
using Microsoft.EntityFrameworkCore;

namespace api.Repositories
{
    public class VitalSignRepository : KiraRepository<VitalSign>, IVitalSignRepository
    {
 
        private readonly KiraContext _context;

        public VitalSignRepository(KiraContext context) : base(context)
        {
           
            _context = context;
        }

        public override async Task<IEnumerable<VitalSign>> GetAsync(Expression<Func<VitalSign, bool>> expression)
        {
            try
            {
                var result = await _context.Set<VitalSign>().Include(v => v.Encounter).Include(v => v.Type)
                    .Where(e => e.IsDeleted > DateTime.Now).Where(expression).ToListAsync();
                return result;
            }
            catch (NullReferenceException)
            {
                return new List<VitalSign>();
            }
        }
    }
}
