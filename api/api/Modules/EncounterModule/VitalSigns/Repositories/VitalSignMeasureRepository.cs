﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using api.Db;
using api.Models;
using Microsoft.EntityFrameworkCore;

namespace api.Repositories
{
    public class VitalSignMeasureRepository : KiraRepository<VitalSignMeasure>, IVitalSignMeasureRepository
    {
        private readonly KiraContext _context;

        public VitalSignMeasureRepository(KiraContext context) : base(context)
        {
            _context = context;

        }

        public override async  Task<IEnumerable<VitalSignMeasure>> GetAsync(Expression<Func<VitalSignMeasure, bool>> expression)
        {
            try
            {
                var result = await _context.Set<VitalSignMeasure>().Include(v => v.Type).Where(e => e.IsDeleted > DateTime.Now).Where(expression).ToListAsync();
                return result;
            }
            catch (NullReferenceException)
            {
                return new List<VitalSignMeasure>();
            }
        }

        public override async  Task<IEnumerable<VitalSignMeasure>> GetAllAsync()
        {
            return await _context.Set<VitalSignMeasure>().Include(m => m.Type).Take(100).Where(e => e.IsDeleted > DateTime.Now).ToListAsync();
        }
    }
}
