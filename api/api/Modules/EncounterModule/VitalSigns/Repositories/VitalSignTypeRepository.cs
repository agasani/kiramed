﻿using System;
using System.Linq;
using System.Threading.Tasks;
using api.Db;
using api.Models;

namespace api.Repositories
{
    public class VitalSignTypeRepository : KiraRepository<VitalSignType>, IVitalSignTypeRepository
    {
        public VitalSignTypeRepository(KiraContext context) : base(context)
        {

        }

        public async  Task<VitalSignType> ExistAsync(VitalSignType type)
        {
            var result = await GetAsync(t => string.Equals(t.Name, type.Name, StringComparison.CurrentCultureIgnoreCase));
            var types = result.ToList();

            return types.Any() ? types.FirstOrDefault() : null;
        }
    }
}
