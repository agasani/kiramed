using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using api.Db;
using api.Models;
using Microsoft.EntityFrameworkCore;

namespace api.Repositories
{
    public class EncounterRepository : KiraRepository<Encounter>, IEncounterRepository
    {
        private readonly KiraContext _context;
        public EncounterRepository(KiraContext context) : base(context)
        {
            _context = context;          
        }

        public override async Task<IEnumerable<Encounter>> GetAsync(Expression<Func<Encounter, bool>> expression)
        {
            try
            {
                var result = await _context.Set<Encounter>().Include(e => e.Patient).ThenInclude(c => c.Entity).ThenInclude(e => e.Contacts).Where(e => e.IsDeleted > DateTime.Now).Where(expression).ToListAsync();
                return result;
            }
            catch (Exception e)
            {
                //var error = e;
                return new List<Encounter>();
            }
        }
    }
}