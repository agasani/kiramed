using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api.Services;
using Microsoft.AspNetCore.Mvc;
using api.Models;
using api.Utils;
using api.ViewModels;
using Newtonsoft.Json;
using System.Net;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace api.Controllers
{
    [Route("api/[controller]")]
    public class EncounterController : Controller
    {
        public readonly IEncounterService _encounterService;

        public EncounterController(IEncounterService encounterService)
        {
            _encounterService = encounterService;
        }

        /// <summary>List all encounters within an organization by unique (string) organizationId</summary>
        /// <response code="404">Model not found</response>
        /// <response code="200">Successfully listed</response>
        [HttpGet("organizationId")]
        [SwaggerResponse((int)HttpStatusCode.NotFound, typeof(FeedBack))]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(IEnumerable<EncounterViewModel>))]
        public async Task<IActionResult> Get([FromRoute]int organizationId)
        {
            var feedback = new FeedBack();
            var result = await _encounterService.GetAsync(e => e.OrganizationId == organizationId);
            var encounters = result.ToList();
            if (encounters.Any())
            {
                var visitQueueModels = encounters.Select(e => new EncounterViewModel(e)).ToList();
                return Ok(visitQueueModels);
            }

            feedback.err = true;
            feedback.message = "No encounters were found";
            return NotFound(feedback);
        }

        /// <summary>List all encounters for a patient by unique (string) patientId</summary>
        /// <response code="404">Model not found</response>
        /// <response code="200">Successfully listed</response>
        [HttpGet("GetByPatient/{patientId}")]
        [SwaggerResponse((int)HttpStatusCode.NotFound, typeof(FeedBack))]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(IEnumerable<EncounterViewModel>))]
        public async Task<IActionResult> GetByPatient([FromRoute]int patientId)
        {
            var feedback = new FeedBack();
            var result = await _encounterService.GetAsync(v => v.PatientId == patientId);
            var encounters = result.ToList();
            if (encounters.Any())
            {
                var visitQueueModels = encounters.Select(e => new EncounterViewModel(e)).ToList();
                return Ok(visitQueueModels);
            }

            feedback.err = true;
            feedback.message = "No encounters were found";
            return NotFound(feedback);
        }

       
    }
}