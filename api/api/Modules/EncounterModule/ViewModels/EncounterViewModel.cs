using System;

namespace api.Models
{
    public class EncounterViewModel
    {
        public int PatientId { get; set; }
        public PatientViewModel PatientViewModel {get;set;}
        public int OrganizationId { get; set;}
        public string AdmittingPhysicianID {get;set;}
        public string AdmittingPhysicianName {get;set;}
        public DateTime AdmissionDate {get;set;}
        public int VisitId { get; set; }

        public EncounterViewModel(Encounter encounter)
         {
            PatientId = encounter.PatientId;
            PatientViewModel = new PatientViewModel(encounter.Patient);
            OrganizationId = encounter.OrganizationId;
            AdmissionDate = encounter.AdmissionDate;
            VisitId = encounter.VisitId;
         }
    }
}