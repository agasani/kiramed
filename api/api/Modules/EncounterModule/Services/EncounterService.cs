using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using api.Models;
using api.Repositories;

namespace api.Services
{
    public class EncounterService : KiraService<Encounter>, IEncounterService
    {
        public EncounterService(IEncounterRepository encounterRepository): base(encounterRepository)
        {
            
        }
    }
}