using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using api.Models;
using api.Services;
using Castle.DynamicProxy.Generators.Emitters.SimpleAST;

namespace api.Services
{
    public interface IEncounterService: IKiraService<Encounter>
    {
        
    }
}