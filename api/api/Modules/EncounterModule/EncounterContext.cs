﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using api.Models;

namespace api.Db
{
    public partial class KiraContext
    {
        public DbSet<Encounter> Encounters { get; set; }
        public DbSet<VitalSign> VitalSigns { get; set; }
        public DbSet<VitalSignMeasure> VitalSignMeasures { get; set; }
        public DbSet<VitalSignType> VitalSignTypes { get; set; }
        public DbSet<PatientDiagnosis> PatientDiagnoses { get; set; }
    }
}
