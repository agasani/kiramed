﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace api.Models
{
    [Table("Encouter")]
    public class Encounter: KiraEntity
    {
        [Required]
        public int PatientId { get; set; }
        public virtual Patient Patient { get; set; }
        [Required]
        public DateTime AdmissionDate { get; set; } = DateTime.Now;
        [Required]
        public string AdmittingPhysicianID {get;set;}
        //Clinical Data Flag
        // public char ClinicalData { get; set; }
        // //Episode type
        // [StringLength(2)]
        // public string EpisodeType { get; set; }
        // //Financial Responsible Party Relationship
        // public string FinancialResponsibleRelationship { get; set; }
         public int OrganizationId { get; set; }
         public int VisitId { get; set; }
        // //public Org Organization { get; set;  }
        // public DateTime BeginDate { get; set; }
        // public DateTime EndDate { get; set; }
        // //Refferal Date
        // public DateTime RefferalDate { get; set; }
        // //Refferal Source
        // public int RefferalSource { get; set; }
        // //Episode Id
        // public int EpisodeId { get; set; }
        // //Refferal Category
        // public string RefferalCategory { get; set; }
        // //Begining ENtry Date
        // public DateTime BeginEntryDate { get; set; }
        // public DateTime EndEntryDate { get; set; }
    }
}
