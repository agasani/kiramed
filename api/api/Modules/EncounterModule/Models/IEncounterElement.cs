﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace api.Models
{
    public interface IEncounterElement
    {
        int EncounterId { get; set; }
        DateTime VisitDateTime { get; set; }
        Encounter Encounter { get; set; }
        string CreatedBy { get; set; }
    }
}
