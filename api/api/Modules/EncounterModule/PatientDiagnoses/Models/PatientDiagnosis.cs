using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using api.Enums;
using api.ViewModels;

namespace api.Models
{
    [Table("PatientDiagnoses")]
    public class PatientDiagnosis: KiraEntity, IEncounterElement
    {
        [Required]
        public int EncounterId { get; set; }
        [Required]
        public DateTime VisitDateTime { get; set; } = DateTime.Now;
        [Required]
        public Encounter Encounter { get; set; }
        [Required]
        public string CreatedBy { get; set; }
        public string Description { get; set; }
        public int Rank { get; set; }
        public eDiagnosisType Type { get; set; } 
        [Required]
        public eKiraCoding CodingVersion { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
      

        public PatientDiagnosis(PatientDiagnosisCreateViewModel vModel)
        {
            EncounterId = vModel.EncounterId;
            CreatedBy = vModel.CreatedBy;
            Description = vModel.Description;
            Rank = vModel.Rank;
            Type = vModel.Type;
            CodingVersion = vModel.DiagnosisInfo.CodingVersion;
            Code = vModel.DiagnosisInfo.Code;
            Name = vModel.DiagnosisInfo.Name;
        }

        public PatientDiagnosis()
        {
            
        }
    }
}
