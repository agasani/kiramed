﻿using api.Db;
using api.Repositories;

namespace api.Services
{
    public class PatientDiagnosisService : PatientDiagnosisRepository, IPatientDiagnosisService
    {
        public PatientDiagnosisService(KiraContext context, IEncounterRepository encounterRepository) : base(context, encounterRepository)
        {

        }
    }
}
