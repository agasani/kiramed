﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api.Models;
using api.Services;
using api.Utils;
using api.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Net;

namespace api.Controllers
{
    [Route("api/[controller]")]
    [ControllerFilters.ValidateModel]
    public class PatientDiagnosisController : Controller
    {
        private readonly IPatientDiagnosisService _patientDiagnosisService;
        private readonly IEncounterService _encounterService;

        public PatientDiagnosisController(IPatientDiagnosisService patientDiagnosisService, IEncounterService encounterService)
        {
            _patientDiagnosisService = patientDiagnosisService;
            _encounterService = encounterService;
        }

        /// <summary>List all patient diagnoses</summary>
        /// <response code="200">Listed successfully</response>
        /// <response code="404">Model not found</response>
        [HttpGet]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(PatientDiagnosisViewModel))]
        [SwaggerResponse((int)HttpStatusCode.NotFound, typeof(FeedBack))]
        public async Task<IActionResult> Get(int encounterId)
        {
            var feedback = new FeedBack();
            var result = await _patientDiagnosisService.GetAsync(d => d.EncounterId == encounterId);

            var diagnoses = result.ToList();
            if (diagnoses.Any())
            {
                var diagnosisModels = diagnoses.Select(d => new PatientDiagnosisViewModel(d)).ToList();
                return Ok(diagnosisModels);
            }

            feedback.err = true;
            feedback.message = "No diagnoses were found";
            return NotFound(feedback);
        }

        /// <summary>Get all diagnoses by unique (integer) patientId</summary>
        /// <response code="200">Listed successfully</response>
        /// <response code="404">Model not found</response>
        [HttpGet("GetByPatient/{patientId}")]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(IEnumerable<PatientDiagnosisViewModel>))]
        [SwaggerResponse((int)HttpStatusCode.NotFound, typeof(FeedBack))]
        public async Task<IActionResult> GetByPatient([FromRoute]int patientId)
        {
            var feedback = new FeedBack();
            var result = await _patientDiagnosisService.GetAsync(d => d.Encounter.PatientId == patientId);

            var diagnoses = result.ToList();
            if (diagnoses.Any())
            {
                var diagnosisModels = diagnoses.Select(d => new PatientDiagnosisViewModel(d)).ToList();
                return Ok(diagnosisModels);
            }

            feedback.err = true;
            feedback.message = "No diagnoses were found";
            return NotFound(feedback);
        }

        /// <summary>Get diagnosis by unique (integer) id</summary>
        /// <response code="200">Listed successfully</response>
        /// <response code="404">Model not found</response>
        [HttpGet("Detail/{id}")]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(PatientDiagnosisViewModel))]
        [SwaggerResponse((int)HttpStatusCode.NotFound, typeof(FeedBack))]
        public async Task<IActionResult> Detail([FromRoute]int id)
        {
            var feedback = new FeedBack();
            var result = await _patientDiagnosisService.GetAsync(d => d.Id == id);

            var diagnosis = result.FirstOrDefault();

            if (diagnosis != null)
            {
                var diagnosisModel = new PatientDiagnosisViewModel(diagnosis);
                return Ok(diagnosisModel);
            }

            feedback.err = true;
            feedback.message = "No diagnosis was found";
            return NotFound(feedback);
        }

        /// <summary>Register a new diagnosis</summary>
        /// <response code="400">Bad request</response>
        /// <response code="200">Created successfully</response>
        [HttpPost]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, typeof(FeedBack))]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(FeedBack))]
        public async Task<IActionResult> Post([FromBody]PatientDiagnosisCreateViewModel vModel)
        {
            var feedback = new FeedBack();
            var newDiagnosis = new PatientDiagnosis(vModel);
            var encounter = await _encounterService.FindAsync(vModel.EncounterId);

            if (encounter == null)
            {
                feedback.err = true;
                feedback.message = "Encounter does not exist.";
                return BadRequest(feedback);
            }

            newDiagnosis.Encounter = encounter;

            var result = await _patientDiagnosisService.CreateAsync(newDiagnosis);

            if (result != 0)
            {
                feedback.message = eFeedbackMessages.ObjectCreatedSuccessfully.GetDescription();
                feedback.data = JsonConvert.SerializeObject(new Dictionary<string, int> { { "Id", result } });
                return Ok(feedback);
            }
            feedback.err = true;
            feedback.message = "Creating the encounter failed";
            return BadRequest(feedback);
        }
    }
}
