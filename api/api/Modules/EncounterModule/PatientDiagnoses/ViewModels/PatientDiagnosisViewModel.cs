﻿using System;
using api.Enums;
using api.Models;
using api.Utils;

namespace api.ViewModels
{
    public class PatientDiagnosisViewModel
    {
        public int Id { get; set; }
        public int EncounterId { get; set; }
        public DateTime VisitDateTime { get; set; }
        public string CreatedBy { get; set; }
        public int Rank { get; set; }
        public eDiagnosisType Type { get; set; }
        public string TypeName { get; set; }
        public eKiraCoding CodingVersion { get; set; }
        public string VersionName { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }


        public PatientDiagnosisViewModel(PatientDiagnosis vModel)
        {
            Id = vModel.Id;
            EncounterId = vModel.EncounterId;
            VisitDateTime = vModel.VisitDateTime;
            CreatedBy = vModel.CreatedBy;
            Type = vModel.Type;
            TypeName = vModel.Type.GetDescription();
            CodingVersion = vModel.CodingVersion;
            VersionName = vModel.CodingVersion.GetDescription();
            Code = vModel.Code;
            Name = vModel.Name;
        }
    }
}
