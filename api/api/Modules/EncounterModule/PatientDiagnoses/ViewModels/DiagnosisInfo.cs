﻿using api.Enums;

namespace api.ViewModels
{
    public class DiagnosisInfo
    {
        public eKiraCoding CodingVersion { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
    }
}