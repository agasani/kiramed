﻿

using api.Enums;

namespace api.ViewModels
{
    public class PatientDiagnosisCreateViewModel
    {
        public int EncounterId { get; set; }
        public string CreatedBy { get; set; }
        public string Description { get; set; }
        public eDiagnosisType Type { get; set; }
        public int Rank { get; set; }
        public DiagnosisInfo DiagnosisInfo { get; set; }
    }
}
