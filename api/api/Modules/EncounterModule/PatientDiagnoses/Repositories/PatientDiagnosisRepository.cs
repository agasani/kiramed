﻿using api.Db;
using api.Models;

namespace api.Repositories
{
    public class PatientDiagnosisRepository : KiraRepository<PatientDiagnosis>, IPatientDiagnosisRepository
    {
        private readonly IEncounterRepository _encounterRepository;

        public PatientDiagnosisRepository(KiraContext context, IEncounterRepository encounterRepository) : base(context)
        {
            _encounterRepository = encounterRepository;
        }
    }
}
