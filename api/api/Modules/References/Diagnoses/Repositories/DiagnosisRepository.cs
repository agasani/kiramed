﻿using System.Threading.Tasks;
using api.Db;
using api.Models;

namespace api.Repositories
{
    public class DiagnosisRepository : KiraRepository<Diagnosis>, IDiagnosisRepository
    {
        public DiagnosisRepository(KiraContext context) : base(context)
        {

        }

        public Task<int> RefreshAllAsync()
        {
            throw new System.NotImplementedException();
        }

        public Task<int> RefreshICD10Async()
        {
            throw new System.NotImplementedException();
        }

        public Task<int> RefreshICD9Async()
        {
            throw new System.NotImplementedException();
        }

        public Task<int> CreateBulkAsync()
        {
            throw new System.NotImplementedException();
        }
    }
}
