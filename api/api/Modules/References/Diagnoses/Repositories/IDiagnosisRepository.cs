﻿using System.Threading.Tasks;
using api.Models;

namespace api.Repositories
{
    public interface IDiagnosisRepository : IKiraRepository<Diagnosis>
    {
        Task<int> RefreshAllAsync();
        Task<int> RefreshICD10Async();
        Task<int> RefreshICD9Async();
        Task<int> CreateBulkAsync();
    }
}
