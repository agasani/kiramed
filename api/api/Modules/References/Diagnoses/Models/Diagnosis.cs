﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using api.Enums;

namespace api.Models
{
    [Table("Diagnoses")]
    public class Diagnosis : KiraEntity
    {
        public eKiraCoding Version { get; set; }
        public string VersionName { get; set; }
        public string Name { get; set; }
        public string CodeFullName { get; set; }
        public string Code { get; set; }
        public string Prefix { get; set; }
    }
}
