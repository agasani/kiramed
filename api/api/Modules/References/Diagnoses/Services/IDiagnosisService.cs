﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api.Repositories;
using api.ViewModels;

namespace api.References.Models
{
    public interface IDiagnosisService : IDiagnosisRepository
    {
        Task<IList<DiagnosisViewModel>> Search(string search);
    }
}
