﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using api.Db;
using api.Repositories;
using api.Utils;
using api.ViewModels;
using Newtonsoft.Json;

namespace api.References.Models
{
    public class DiagnosisService : DiagnosisRepository , IDiagnosisService
    {
        public DiagnosisService(KiraContext context) : base(context)
        {

        }

        public async Task<IList<DiagnosisViewModel>> Search(string search)
        {      
            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Clear();
            client.BaseAddress = new Uri("http://localhost:64288/");

            var result = await client.GetAsync($@"api/diagnosis/search/{search}");
            List<DiagnosisViewModel> diagnosis;
            using (var reader = new System.IO.StreamReader(result.Content.ReadAsStreamAsync().Result))
            {
                var responseText = reader.ReadToEnd();
                diagnosis = JsonConvert.DeserializeObject<List<DiagnosisViewModel>>(responseText);
            }
            return diagnosis;
        }
    }
}
