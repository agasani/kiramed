﻿using System.Linq;
using System.Threading.Tasks;
using api.Models;
using api.References.Models;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Net;
using System.Collections.Generic;
using api.ViewModels;

namespace api.Controllers
{
    [Route("api/[controller]")]
    public class DiagnosisController : Controller
    {
        private readonly IDiagnosisService _diagnosisService;

        public DiagnosisController(IDiagnosisService diagnosisService)
        {
            _diagnosisService = diagnosisService;
        }

        /// <summary>List some diagnosis by unique (string) search</summary>
        /// <response code="400">Bad request</response>
        /// <response code="200">Model found</response>
        [HttpGet("Search/{search}")]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, typeof(FeedBack))]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(IEnumerable<DiagnosisViewModel>))]
        public async Task<IActionResult> Search([FromRoute]string search)
        {
            var feedback = new FeedBack();

            if (string.IsNullOrWhiteSpace(search))
            {
                feedback.err = true;
                feedback.message = $"Bad request.";
                feedback.data = "";
                return BadRequest(feedback);
            }
            if (search.Length > 50)
            {
                feedback.err = true;
                feedback.message = "The search query is very long. No results found.";
                feedback.data = "";
                return BadRequest(feedback);
            }

            var results = await _diagnosisService.Search(search);

            if (!results.Any())
            {
                feedback.err = true;
                feedback.message = "No results were found";
                return BadRequest(feedback);
            }
            return Ok(results);
        }

    }
}
