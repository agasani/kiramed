
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api.Models;

namespace api.ViewModels
{
   
    public class DiagnosisViewModel
    {        public int Id {get;set;}
            public string CodingName { get; set; }
            public int CodingId { get; set; }
            public string Name { get; set; }
            public string CodeFullName { get; set; }
            public string Code { get; set; }
            public string Prefix { get; set; }

    }
}

