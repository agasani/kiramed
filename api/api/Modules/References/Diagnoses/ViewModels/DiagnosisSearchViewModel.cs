﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api.ViewModels
{
    public class DiagnosisSearchViewModel
    {
       public string Code { get; set; }
       public string Name { get; set; }
       public string VersionName { get; set; }
       public int ID { get; set; }
    }

}
