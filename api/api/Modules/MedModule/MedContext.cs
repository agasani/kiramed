﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using api.Models;

namespace api.Db
{
    public partial class KiraContext : DbContext
    {
        public DbSet<PatientMedication> PatientMeds { get; set; }
        public DbSet<Medication> Medications { get; set; }
    }
}
