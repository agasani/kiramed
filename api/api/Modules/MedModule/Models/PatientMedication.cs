﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace api.Models
{
    [Table("PatientMedication")]
    public class PatientMedication: IEntity
    {
        public int Id { get; set; }
        [Required]
        public int MedicationId { get; set; }
        public Medication Medication { get; set; }
        //Refill
        public string Refill { get; set; }
        //Related Diagnosis
        public string RelatedDiagnosis { get; set; }
        //Preferred Pharmacy
        public int PrefferedPharmacy { get; set; }
        //Misc. Note
        public string MiscellaneousNote { get; set; }
        //Med Prescription Number 
        public string MedPrescriptionNumber { get; set; }
        public string MedNameType { get; set; }
        public char DosageType { get; set; }
        public int UserMedId { get; set; }
        public string ICDCodeSet { get; set; }
        //Source: where med was entered in the system
        public string Source { get; set; }
        //Over the counter
        public bool OverTheCounter { get; set; }
        //Compound Med Flag
        public bool IsCompoundMed { get; set; }
        //Order date of medication
        public DateTime OrderDate { get; set; }
    }

}
