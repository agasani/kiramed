﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace api.Models
{
    [Table("Medication")]
    public class Medication: IEntity
    {
        public int Id { get; set; }
        [Required]
        public int PatientId { get; set; }
        public Patient Patient { get; set; }
        public int MedicationNameId { get; set; }
        public string MedicationName { get; set; }
        //Is it custom med?
        public bool IsCustom { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime LastUpdateDate { get; set; }
        //Screening count
        public int ScreeningCount { get; set; }
        //Screening description
        public string ScreenDescription { get; set; }
        //Med classification
        public string Classification { get; set; }
        //Whether Actin is required on the screening
        public bool IsActinRequired { get; set; }
        //Whether or not screening is high risk
        public bool IsHighRisk { get; set; }
        //New changed existing flag
        public char NewChangeExists { get; set; }
        //Med custom ID
        public int MedicationCustomId { get; set; }
        //Is the provider or the patient responsible
        public string FinancialResponsable { get; set; }
        //is med related to terminal illness
        public bool RelatedToTerminalIllness { get; set; }
        //Related Diagnosis
        public string RelatedDiagnosis { get; set; }
        //Over the counter indicator
        public bool OverTheCounter { get; set; }
        //Order Date of the med
        public DateTime OrderDate { get; set; }

    }

}
