﻿using System.Collections.Generic;
using System.Threading.Tasks;
using api.Models;
using api.Services;
using FluentEmail.Core.Models;

namespace api.Mailers
{
    public interface IUserMailer : IEmailService
    {
        Task<SendEmailResponse> UserInvitationEmailAsync(User user, string password);
    }
}
