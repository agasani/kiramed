﻿using System;

namespace api.Models
{
    public class UserRoleViewModel
    {
        public string Email { get; set; }
        public string Role { get; set; }
        public int OrganizationId { get; set; }
        public DateTime Date { get; set; }
    }
}
