﻿using api.Db;
using api.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace api.Models
{
    public class UserViewModel
    {

        // #region Properties
        [Required]
        public string Id { set; get; }
        [Required]
        public int EntityId { get; set; }
        //public Entity Entity { get; set; }
        [Required]
        // public List<Role> Roles { get; set; } = new List<Role>();
        [MaxLength(25)]
        public string UserName { get; set; }
        public string Email { get; set; }
        public bool IsLocked { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime LastLoggedIn { get; set; }
        public DateTime ModifiedDate { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsDisabled { get; set;}
        public List<String> Roles { get; }
        public List<string> Claims { get; }

        // #endregion

        public UserViewModel(Auth.ApplicationUser user)
        {
            Id = user.Id;
            EntityId = user.EntityId;        
            Email = user.Email;
            UserName = user.UserName;
            IsLocked = user?.IsLocked != DateTime.MaxValue;
            CreatedDate = user.CreatedDate;
            foreach( var role in user.Roles)
            {
                Roles.Add(role.ToString());
            }

            foreach (var claim in user.Claims)
            {
                Roles.Add(claim.ToString());
            }
            ModifiedDate = user.ModifiedDate;
            IsDeleted = user?.IsDeleted != DateTime.MaxValue; ;
        }
    }
}
