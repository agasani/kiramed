﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api.Models;

namespace api.ViewModels
{
    public class UserUpdateViewModel
    {
        public EntityViewModel Entity { get; set; }


        public UserUpdateViewModel () { }

        public UserUpdateViewModel(User user)
        {
            Entity = new EntityViewModel(user.Entity);
        }
    }

}
