﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api.ViewModels
{
    public class NewUserViewModel
    {
        public IList<string> Roles { get; set; }
        public Dictionary<int, string> Country { get; set; }
    }
}
