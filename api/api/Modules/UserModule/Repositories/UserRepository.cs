﻿using api.Auth;
using api.Db;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api.Models
{
    public class UserRepository : IUserRepository
    {
        private readonly UserManager<ApplicationUser> _userManager;
         private readonly RoleManager<ApplicationRole> _roleManager;
        private readonly KiraContext _context;

        public UserRepository(UserManager<ApplicationUser> userManager, RoleManager<ApplicationRole> roleManager, KiraContext context)
        {
            _userManager = userManager;
            _context = context;
            _roleManager = roleManager;
        }

        public IQueryable<ApplicationUser> Get() => _userManager.Users;

        public ApplicationUser GetByEmail(string email)
        {
            return _userManager.Users.FirstOrDefault(u => u.Email == email);
        }

        public async Task<ApplicationUser> FindByIdAsync(string userId)
        {
            return await _userManager.FindByIdAsync(userId);
        }

        public Task<IdentityResult> Create(User user, string password)
        {
            var appUser = new ApplicationUser
            {
                UserName = user.UserName,
                Email = user.Email,
                CreatedDate = DateTime.Now,
                IsDeleted = DateTime.MaxValue,
                IsLocked = DateTime.MaxValue,
                PhoneNumber = user.PhoneNumber
            };

            //check if entity exisits

            var entity = _context.Entities.FirstOrDefault(e => e.ExternalId == user.NationalId
                                                               || e.Demographic.NationalID == user.NationalId
                                                               || e.Contacts.Any(
                                                                   c => (ContactTypes) c.ContactTypeId ==
                                                                        ContactTypes.Email && c.Value == user.Email)
                                                               || e.Contacts.Any(
                                                                   c => (ContactTypes) c.ContactTypeId ==
                                                                        ContactTypes.Phone && c.Value ==
                                                                        user.PhoneNumber));

            if (entity == null)
            {
                entity = user.Entity;
                _context.Entities.Add(entity);
                _context.SaveChanges();
            }

            appUser.EntityId = entity.Id;

            return _userManager.CreateAsync(appUser, password);
        }

        public async Task<IdentityResult> Delete(User user)
        {
            var appUser = await _userManager.FindByEmailAsync(user.Email);
            appUser.IsDeleted = DateTime.Now;
            var userEntity = await GetUserEntityAsync(appUser);
            if (userEntity == null) return await _userManager.DeleteAsync(appUser);
            userEntity.IsDeleted = DateTime.Now;
            await _context.SaveChangesAsync();
            return await _userManager.DeleteAsync(appUser);
        }

        public async Task<IdentityResult> Update(User user)
        {
            var appUser = await _userManager.FindByEmailAsync(user.Email);
            return await _userManager.UpdateAsync(appUser);
        }

        public UserManager<ApplicationUser> GetUserManager()
        {
            return _userManager;
        }

        public Task<string> GetConfirmationToken(User user)
        {
            return _userManager.GenerateEmailConfirmationTokenAsync(new ApplicationUser(user));
        }

        public async Task<IdentityResult> AddUserToRole(User user, ApplicationRole role, int organizationId,
            DateTime startDate)
        {
            var appUser = GetByEmail(user.Email);
            var hasRole = _userManager.IsInRoleAsync(appUser, role.Name).Result;
            var isInOrg =
                _context.UserOrganizations.FirstOrDefault(
                    o => o.RoleId == role.Id && o.OrganizationId == organizationId && o.UserId ==appUser.Id);
            if (hasRole && isInOrg != null) return IdentityResult.Success;
            var result = IdentityResult.Success;
            if (!hasRole)
            {
                result = await _userManager.AddToRoleAsync(appUser, role.Name);
                
                // Add role's claims to the user
                var claims = _roleManager.GetClaimsAsync(role).Result;
                Console.WriteLine(role.Name + " role has: " + claims.Count() + " claims.");
                foreach(var c in claims) {
                    Console.WriteLine("CLAIM value: " + c.Value + " type: " + c.Type);
                }

                await _userManager.AddClaimsAsync(appUser, claims);
            }

            if (!result.Succeeded) return IdentityResult.Failed();
            //check if the user was already in the assignment table                   
            if (isInOrg != null) return result;
            var userOrg = new UserOrganization
            {
                StartDate = startDate,
                EndDate = new DateTime(9999, 12, 31),
                OrganizationId = organizationId,
                UserId = appUser.Id,
                RoleId = role.Id
            };
            _context.UserOrganizations.Add(userOrg);
            _context.SaveChanges();
            return result;
        }

        public ApplicationUser GetByPhoneNumber(string phone)
        {
            return _userManager.Users.FirstOrDefault(u => u.PhoneNumber == phone);
        }

        public async Task<IdentityResult> RemoveUserFromRole(User user, ApplicationRole role, int organizationId,
            DateTime endDate)
        {
            var appUser = GetByEmail(user.Email);
            var userRole =
                _context.UserOrganizations.FirstOrDefault(
                    u => u.UserId == appUser.Id && u.RoleId == role.Id && u.OrganizationId == organizationId);
            var removeUserResult = await _userManager.RemoveFromRoleAsync(appUser, role.Name);
            if (!removeUserResult.Succeeded) return IdentityResult.Failed();
            userRole.EndDate = endDate;
            _context.SaveChanges();
            return IdentityResult.Success;
        }

        public async Task<IdentityResult> LockUserAccount(User user)
        {
            var appUser = GetByEmail(user.Email);
            if (appUser.IsLocked < DateTime.MaxValue)
            {
                return IdentityResult.Success;
            }
            return await _userManager.SetLockoutEnabledAsync(appUser, true);
        }

        public async Task<IdentityResult> UnlockUserAccount(User user)
        {
            var appUser = GetByEmail(user.Email);
            if (appUser.IsLocked == DateTime.MaxValue)
            {
                return IdentityResult.Success;
            }
            appUser.IsLocked = DateTime.MaxValue;
            return await _userManager.UpdateAsync(appUser);
        }

        public async Task<IdentityResult> Update(User user, string password)
        {
            var appUser = await _userManager.FindByEmailAsync(user.Email);
            appUser.IsDeleted = DateTime.MaxValue;
            await _userManager.RemovePasswordAsync(appUser);
            await _userManager.AddPasswordAsync(appUser, password);
            return await _userManager.UpdateAsync(appUser);
        }

        public List<ApplicationUser> GetOrganizationUsers(int organizationId)
        {
            var userIds = _context.UserOrganizations
                .Where(u => u.OrganizationId == organizationId && u.EndDate > DateTime.Now).Select(u => u.UserId)
                .Distinct();
            return !userIds.Any() ? null : userIds.Select(u => Get().FirstOrDefault(r => r.Id == u)).ToList();
        }

        public async Task<IEnumerable<ApplicationUser>> GetOrganizationUsersAsync(int organizationId, string roleName = null)
        {
            var userIds = new List<string>();
            if(roleName != null)
            {
               var role = await _roleManager.FindByNameAsync(roleName); 
               userIds = await _context.UserOrganizations
                .Where(u => u.OrganizationId == organizationId && u.RoleId == role.Id && u.EndDate > DateTime.Now).Select(u => u.UserId)
                .Distinct().ToListAsync();             
            }
            else
            {
               userIds = await _context.UserOrganizations
                .Where(u => u.OrganizationId == organizationId && u.EndDate > DateTime.Now).Select(u => u.UserId)
                .Distinct().ToListAsync();
            }         
            return !userIds.Any() ? null : userIds.Select(u => Get().FirstOrDefault(r => r.Id == u)).ToList();
        }

        public List<Organization> UserOrganizations(string userId)
        {
            var orgIds = _context.UserOrganizations.Where(u => u.UserId == userId && u.EndDate > DateTime.Now)
                .Select(u => u.OrganizationId).Distinct();
            return !orgIds.Any()
                ? null
                : _context.Organizations.Where(o => orgIds.Contains(o.Id) && o.IsDeleted > DateTime.Now).ToList();
        }

        public IList<string> UserRoles(ApplicationUser user)
        {
            return _userManager.GetRolesAsync(user).Result;
        }

        public  async Task<IdentityResult> UpdateAsync(User user)
        {        
            if (user == null) return IdentityResult.Failed();
            var appUser = await _userManager.FindByIdAsync(user.UserId);
            appUser.Email = user?.Email;
            appUser.PhoneNumber = user?.PhoneNumber;
            appUser.UserName = user?.UserName;
            Entity userEntity;
            try
            {
                userEntity = await _context.Entities.FirstOrDefaultAsync(e => e.Id == user.Entity.Id);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
           
            if(userEntity == null) return IdentityResult.Failed();
            var result = await _userManager.UpdateAsync(appUser);
            if (!result.Succeeded) return IdentityResult.Failed();          
            var entityChangeResult = await _context.SaveChangesAsync();
            return entityChangeResult != 0 ? IdentityResult.Success : IdentityResult.Failed();
        }

        public async Task<Entity> GetUserEntityAsync(ApplicationUser user)
        {
           return await _context.Entities.FirstOrDefaultAsync(e => e.Id == user.EntityId && e.IsDeleted > DateTime.Now);
        }

        public async Task<int> SaveAsync()
        {
            return await _context.SaveChangesAsync();
        }
    }
}