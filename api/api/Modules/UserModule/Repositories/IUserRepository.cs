﻿using api.Auth;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api.ViewModels;

namespace api.Models
{
    public interface IUserRepository
    {
        IQueryable<ApplicationUser> Get();
        ApplicationUser GetByEmail(string email);
        ApplicationUser GetByPhoneNumber(string phone);
        Task<ApplicationUser> FindByIdAsync(string id);
        Task<IdentityResult> Create(User user, string password);
        Task<IdentityResult> Delete(User user);
        Task<IdentityResult> Update(User user);
        Task<IdentityResult> Update(User user, string password);
        UserManager<ApplicationUser> GetUserManager();
        Task<string> GetConfirmationToken(User user);
        Task<IdentityResult> AddUserToRole(User user, ApplicationRole role, int organizationId, DateTime startDate);
        Task<IdentityResult> RemoveUserFromRole(User user, ApplicationRole role, int organizationId, DateTime endDate);
        Task<IdentityResult> LockUserAccount(User user);
        Task<IdentityResult> UnlockUserAccount(User user);
        List<ApplicationUser> GetOrganizationUsers(int organizationId);
        Task<IEnumerable<ApplicationUser>> GetOrganizationUsersAsync(int organizationId, string roleName = null);
        List<Organization> UserOrganizations(string userId);
        IList<string> UserRoles(ApplicationUser user);
        Task<IdentityResult> UpdateAsync(User user);
        Task<Entity> GetUserEntityAsync(ApplicationUser user);
        Task<int> SaveAsync();


    }
}
