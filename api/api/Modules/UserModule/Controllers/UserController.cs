﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using api.Services;
using api.Auth;
using Newtonsoft.Json;
using api.Auth.Roles;
using api.Models;
using api.Utils;
using api.ViewModels;
using Microsoft.AspNetCore.Authorization;
using api.Events;
using System.Net;
using Swashbuckle.AspNetCore.SwaggerGen;
using Microsoft.AspNetCore.Cors;

namespace api.Controllers
{
    [Route("api/[controller]")]
    [EnableCors("default")]
    public class UserController : Controller
    {
        private readonly IUserService _userService;
        private readonly IRoleRepository _roleRepository;
        
        // this will be used for custom validation like checking whether the user is authorize to access current org id, isPatient 
        // the kinda granular auth we cannot encode in general claim policies etc.
        // this is used in combination with policy handlers etc.
        // TODO in addition to someone having claim to OrganizationUsers we should check if they have access to this particular org
        // this is where _authorizeService is used..
        // see imperative authorization eg. https://docs.microsoft.com/en-us/aspnet/core/security/authorization/resourcebased?tabs=aspnetcore1x
        private readonly IAuthorizationService _authorizeService;


        public UserController(IUserService usersService, IRoleRepository roleRepository, IAuthorizationService authorizationService = null)
        {
            _userService = usersService;
            _roleRepository = roleRepository;
            _authorizeService = authorizationService;
        }

        /// <summary>List all users</summary>
        /// <response code="200">Successfully listed</response>
        /// <response code="400">Bad request</response>
        [HttpGet]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(IEnumerable<UserViewModel>))]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, typeof(FeedBack))]
        public async Task<IActionResult> Get()
        {
            var feedback = new FeedBack();
            var result = await _userService.GetAsync();
            if (result.Any()) return Ok(result);
            feedback.err = true;
            feedback.message = eFeedbackMessages.NoResultsWereFound.GetDescription();
            return BadRequest(feedback);
        }

        /// <summary>List organization users by unique (integer) orgId</summary>
        /// <response code="200">Successfully listed</response>
        /// <response code="400">Bad request</response>
        [HttpGet("OrganizationUsers/{orgId}")]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(IEnumerable<UserViewModel>))]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, typeof(FeedBack))]
        public async Task<IActionResult> OrganizationUsers([FromRoute]int orgId)
        {
            var feedback = new FeedBack();
            var result = await _userService.GetOrganizationUsersAsync(orgId);
            if (result.Any()) return Ok(result);
            feedback.err = true;
            feedback.message = eFeedbackMessages.NoResultsWereFound.GetDescription();
            return BadRequest(feedback);
        }

        /// <summary>List organization users by role with unique (integer) orgId and (string) roleName</summary>
        /// <response code="200">Successfully listed</response>
        /// <response code="400">Bad request</response>
        [HttpGet("OrganizationUsersByRole/{orgId}/{roleName}")]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(IEnumerable<UserViewModel>))]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, typeof(FeedBack))]
        public async Task<IActionResult> OrganizationUsersByRole([FromRoute]int orgId, [FromRoute]string roleName)
        {
            var feedback = new FeedBack();
            var result = await _userService.GetOrganizationUsersAsync(orgId, roleName);
            if (result.Any()) return Ok(result);
            feedback.err = true;
            feedback.message = eFeedbackMessages.NoResultsWereFound.GetDescription();
            return BadRequest(feedback);
        }

        /// <summary>Logging in a user</summary>
        /// <response code="400">Bad request</response>
        /// <response code="200">Successfully listed</response>
        [HttpPost("Login")]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, typeof(FeedBack))]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(FeedBack))]
        public async Task<IActionResult> Login([FromBody]UserLoginViewModel loginModel)
        {
            var feedback = new FeedBack();
            if (!ModelState.IsValid)
            {
                feedback.err = true;
                return BadRequest(feedback);
            }

            var user = await _userService.GetByEmailAsync(loginModel.UserName);

            if (user == null)
            {
                feedback.err = true;
                feedback.message = "User does not exist";
                return BadRequest(feedback);
            }

            await _userService.SignOutAsync();
            var result = await _userService.PasswordSignInAsync(
                user, loginModel.Password, false, false);

            if (result.Succeeded)
            {
                var currentUser = User;

                feedback.err = false;
                feedback.message = "Welcome " + user.UserName + "you are signed in";
                return Ok(feedback);
            }

            feedback.err = true;
            feedback.message = "Cannot login it at this time";
            return BadRequest(feedback);

        }

        /// <summary>Get a user by unique (string) email</summary>
        /// <response code="200">Successfully retrieved</response>
        /// <response code="400">Bad request</response>
        [HttpGet("Detail/{email}")]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(UserViewModel))]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, typeof(FeedBack))]
        public async Task<IActionResult> Detail([FromRoute]string email)
        {
            var feedback = new FeedBack();
            var result = await _userService.GetByEmailAsync(email);
            if (result != null) return Ok(result);
            feedback.err = true;
            feedback.message = eFeedbackMessages.NoResultsWereFound.GetDescription();
            return BadRequest(feedback);
        }

        /// <summary>New user</summary>
        /// <response code="400">Bad request</response>
        /// <response code="200">Create form opened</response>
        [HttpGet("New")]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, typeof(FeedBack))]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(FeedBack))]
        public async Task<IActionResult> Create()
        {
            var feedback = new FeedBack();
            var roles = await _roleRepository.Roles();
            if (!roles.Any())
            {
                feedback.err = true;
                feedback.message = "No role was found. Please create a role before creating the user!";
                return BadRequest(feedback);
            }

            var country = await _userService.GetCountryAsync(1);
            var newUserViewModel = new NewUserViewModel
            {
                Roles = roles.Select(r => r.Name).ToList(),
                Country = new Dictionary<int, string>()
                {
                    {country.Id, country.Name}
                }
            };

            feedback.err = false;
            feedback.message = "Available roles";
            feedback.data = JsonConvert.SerializeObject(newUserViewModel);

            return Ok(feedback);
        }

        /// <summary>Register a new user</summary>
        /// <response code="400">Bad request</response>
        /// <response code="200">User successfully created</response>
        [HttpPost]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, typeof(FeedBack))]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(FeedBack))]
        public async Task<IActionResult> Post([FromBody]UserRegisterViewModel model)
        {
            var feedback = new FeedBack();
            if (!ModelState.IsValid)
            {
                feedback.err = true;
                feedback.message = "Invalid Model: " + string.Join(", ", ModelState.Values.Where(e => e.Errors.Count > 0)
                                       .SelectMany(e => e.Errors).Select(e => e.ErrorMessage).ToArray());
                return BadRequest(feedback);
            }

            feedback = await _userService.CreateAsync(model);

            if (feedback.err)
            {
                return BadRequest(feedback);
            }

            Console.WriteLine("Successfully registered the user: " + model.UserName);

            return Ok(feedback);

        }

        /// <summary>Add a user to role</summary>
        /// <response code="400">Bad request</response>
        /// <response code="200">User added to role</response>
        [HttpPost("AddToRole")]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, typeof(FeedBack))]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(FeedBack))]
        public async Task<IActionResult> AddToRole([FromBody]UserRoleViewModel model)
        {
            var feedback = new FeedBack();
            var user = await _userService.GetByEmailAsync(model.Email);
            var role = _roleRepository.FindByName(model.Role);

            var roleResult = await _userService.AddUserToRole(user, role, model.OrganizationId, model.Date);
            if (roleResult.Succeeded)
            {
                feedback.err = false;
                feedback.message += "User added to role successfully!";
                return Ok(feedback);
            }
            feedback.err = true;
            feedback.message += "Cannot add this user to the role at this time!";
            return BadRequest(feedback);
        }

        /// <summary>Add claims to role</summary>
        /// <response code="400">Bad request</response>
        /// <response code="200">Claims added to role</response>
        [HttpPost("addClaimsToRole")]
        [Consumes("application/x-www-form-urlencoded")]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, typeof(void))]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(void))]
        public async Task addClaimsToRole([FromForm]string roleName, [FromForm]string claims)
        {
            Console.WriteLine("The role is: " + roleName);
            var claimsList = claims.Split(',').Select(x => x.Trim());
            Console.WriteLine("The claims are: " + claimsList.Aggregate((x, y) => (x + " " + y)));

            ApplicationRole role = null;
            try {

                role = _roleRepository.FindByName(roleName);
            } catch(Exception e) {
                Console.WriteLine("Exception Thrown FINDING ROLE, message: " + e.Message);

                // Log the exception
                handleException("addClaimsToRole", e);
            }

            if(role != null){

                Console.WriteLine("ROLE found: " + role.Name);
                foreach(var c in claimsList) 
                {    
                    try {
                        await _roleRepository.AddClaimAsync(role, c);
                    } catch(Exception e) {
                        Console.WriteLine("Exception Thrown ADDING CLAIM, message: " + e.Message);

                        // Log the exception
                        handleException("addClaimsToRole", e);
                    }
                }
            } else {

                Console.WriteLine("ROLE not found!");
            }

        }

        /// <summary>Remove user from role</summary>
        /// <response code="400">Bad request</response>
        /// <response code="200">User removed from role</response>
        [HttpPost("RemoveFromRole")]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, typeof(FeedBack))]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(FeedBack))]
        public async Task<IActionResult> RemoveFromRole([FromBody]UserRoleViewModel model)
        {
            var feedback = new FeedBack();
            var user = await _userService.GetByEmailAsync(model.Email);
            var role = _roleRepository.FindByName(model.Role);

            var roleResult = await _userService.RemoveUserFromRole(user, role, model.OrganizationId, model.Date);

            if (roleResult.Succeeded)
            {
                feedback.err = false;
                feedback.message += "User removed successfully!";
                return Ok(feedback);
            }

            feedback.err = true;
            feedback.message += "Cannot remove this at this time!";
            return BadRequest(feedback);

        }

        /// <summary>Lock user</summary>
        /// <response code="404">Model not found</response>
        /// <response code="400">Bad request</response>
        /// <response code="200">User locked successfully</response>
        [HttpPost("Lock")]
        [Consumes("application/x-www-form-urlencoded")]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, typeof(FeedBack))]
        [SwaggerResponse((int)HttpStatusCode.NotFound, typeof(FeedBack))]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(FeedBack))]
        public async Task<IActionResult> Lock([FromForm]string email)
        {
            var currentUser = await _userService.GetByEmailAsync(email);
            if (currentUser == null)
            {
                var feedback = new FeedBack() { err = true, message = $" {email} does not exist.", data = "" };
                return NotFound(feedback);
            }
            var result = await _userService.LockUserAccount(currentUser);

            if (!result.Succeeded)
            {
                var feedback = new FeedBack() { err = true, message = $" locking {currentUser.UserName} failed.", data = "" };
                return BadRequest(feedback);
            }
            else
            {
                var feedback = new FeedBack() { err = false, message = $" {currentUser.UserName} was locked.", data = "" };
                return Ok(feedback);
            }
        }

        /// <summary>Lock user</summary>
        /// <response code="404">Model not found</response>
        /// <response code="400">Bad request</response>
        /// <response code="200">User unlocked successfully</response>
        [HttpPost("Unlock")]
        [Consumes("application/x-www-form-urlencoded")]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, typeof(FeedBack))]
        [SwaggerResponse((int)HttpStatusCode.NotFound, typeof(FeedBack))]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(FeedBack))]
        public async Task<IActionResult> Unlock([FromForm]string email)
        {
            var currentUser = await _userService.GetByEmailAsync(email);
            if (currentUser == null)
            {
                var feedback = new FeedBack() { err = true, message = $" {email} does not exist.", data = "" };
                return NotFound(feedback);
            }
            var result = await _userService.UnlockUserAccount(currentUser);

            if (!result.Succeeded)
            {
                var feedback = new FeedBack() { err = true, message = $" locking {currentUser.UserName} failed.", data = "" };
                return BadRequest(feedback);
            }
            else
            {
                var feedback = new FeedBack() { err = false, message = $" {currentUser.UserName} was locked.", data = "" };
                return Ok(feedback);
            }
        }

        /// <summary>Update user</summary>
        /// <response code="400">Bad request</response>
        /// <response code="200">User unlocked successfully</response>
        [HttpPut("{userId}")]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, typeof(FeedBack))]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(FeedBack))]
        public async Task<IActionResult> Put([FromRoute]string userId, [FromBody]EntityViewModel model)
        {
            var feedback = new FeedBack();
            var user = await _userService.FindByIdAsync(userId);

            if (user == null)
            {
                feedback.err = true;
                feedback.message = "User was not found";
                return BadRequest(feedback);
            }

            user.Update(model);


            var result = await _userService.UpdateAsync(user);

            if (result.Succeeded)
            {
                feedback.err = false;
                // feedback.message = $"Successfully updated {model.FirstName} account";
                return Ok(feedback);
            }
            feedback.err = true;
            feedback.message = "Couldn't update the user, please try again later";
            return BadRequest(feedback);
        }

        /// <summary>List user organizations by unique (string) userId</summary>
        /// <response code="200">Organization listed successfully</response>
        [HttpGet("GetUserOrgs/{userId}")]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(IEnumerable<OrganizationViewModel>))]
        public IEnumerable<OrganizationViewModel> GetUserOrgs([FromRoute]string userId)
        {
            return _userService.GetUserOrganizations(userId);
        }

        private void handleException(string action, Exception e)
        {
            var log = new LogEntity();
            log.Action = action;
            log.Controller = "UserController";
            log.Payload = JsonConvert.SerializeObject(e);
            log.Type = "Exception";
            EventBus.broadcast(LogEvents.ErrorLog.ToString(), JsonConvert.SerializeObject(log));
        }
    }
}
