﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using api.Auth;
using api.ViewModels;

namespace api.Models
{

    public class User : IEntity
    {
       public int Id { get; set; }
       public string UserId { get; set; }
       public string FirstName { get; set; }
       public string LastName { get; set; }
       public string MiddleName { get; set; }
       [EmailAddress]
       public string Email { get; set; }
       [Phone]
       public string PhoneNumber { get; set; }
       public string NationalId { get; set; }
       public List<string> Roles { get; set; } = new List<string>();
       public string UserName { get; set; }
       public Entity Entity { get; set; }
       public bool IsActive { get; set; }
       public List<string> Organizations { get; set; }
       
        public User(ApplicationUser user , Entity entity)
        {
            Id = user.EntityId;
            UserId = user.Id;
            FirstName = entity.Firstname;
            LastName = entity.Lastname;
            MiddleName = entity.Middlename;
            Email = user.Email;
            PhoneNumber = user.PhoneNumber;
            NationalId = entity.ExternalId;
            Entity = entity;
            UserName = user.UserName;
            IsActive = user.IsDeleted > DateTime.Now;
            foreach (var role in user.Roles)
            {
                Roles.Add(role.RoleId);
            }
        }

        public User(ApplicationUser user)
        { 
            UserName = user.UserName;
            Email = user.Email;
            PhoneNumber = user.PhoneNumber;
            UserId = user.Id;
            IsActive = user.IsDeleted > DateTime.Now;
            foreach (var role in user.Roles)
            {
                Roles.Add(role.RoleId);
            }
        }

        public User() { } //other initialisers

        public void Update(EntityViewModel model)
        {
            Entity.Update(model);
            var newEmail = EntityHelper.GetEntityViewModelEmail(model);
            var newPhone = EntityHelper.GetEntityViewModelPhoneNumber(model);
            if (Email != newEmail) Email = newEmail;
            if (PhoneNumber != newPhone) PhoneNumber = newPhone;
        }

        public void Update(UserRegisterViewModel model)
        {
            FirstName = model.FirstName;
            LastName = model.SecondName;
            Email = model.Email;
            MiddleName = model.MiddleName;
            NationalId = model.NationalId;
            PhoneNumber = model.PhoneNumber;
        }

        public User(UserRegisterViewModel model)
        {
            UserName = model.UserName;
            Email = model.Email;
            FirstName = model.FirstName;
            LastName = model.SecondName;
            MiddleName = model.MiddleName;
            NationalId = model.NationalId;
            PhoneNumber = model.PhoneNumber;

            Entity = new Entity(model);
        }
    }
}
