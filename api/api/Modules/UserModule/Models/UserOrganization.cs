﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System;
using api.Models;

namespace api.Models
{
    [Table("UserOrganization")]
    public class UserOrganization
    {
        public int Id { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int OrganizationId {get;set;}
        public string RoleId { get; set; }
        public string UserId { get; set; }
    }
}
