﻿using api.Db;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using api.Auth;
using Microsoft.EntityFrameworkCore;
using api.Utils;

namespace api.Models
{
    public class UserRepository : IEntityRepository<User>
    {
        public readonly KiraContext _context;
        public readonly UserManager<ApplicationUser> _userManager;

        public  UserRepository(KiraContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
            
        }

        public UserRepository() { }

        public async Task<User> CreateAsync(User user)
        {
            var appUser = new ApplicationUser
            {
                UserName = user.UserName,
                Email = user.Email,
                CreatedDate = DateTime.Now,
            };

            //create user entity: Hacky shit though

            var newEntity = new Entity
            {
                Firstname = user.FirstName,
                Lastname = user.LastName,
                ExternalId = user.NationalId,         
            };

            //add phone contact
            var contactType1 = new ContactType { Name = "Phone", Type = 1 };
            var phoneContact = new EntityContact()
            {
                ContactTypeId = (short)ContactTypes.Phone,
                Value = user.PhoneNumber,
                //ContactType = contactType1
            };
            newEntity.Contacts.Add(phoneContact);

            //add email contact
            var contactType2 = new ContactType { Name = "Email", Type = 2 };
            var emailContact = new EntityContact()
            {
                ContactTypeId = (short)ContactTypes.Email,
                Value = user.Email,
                //ContactType = contactType2
            };

            newEntity.Contacts.Add(emailContact);

            var demographic = new EntityDemographic
            {
                NationalID = user.NationalId
            };

            newEntity.Demographic = demographic;

            _context.Entities.Add(newEntity);

            appUser.EntityId = newEntity.Id;
            //
            var password = RandomPasswordGenerator.GeneratePassword(7);
            var result =  await _userManager.CreateAsync(appUser, password);
            if(result.Succeeded)
            {
                //var accessToken = _userManager.GenerateEmailConfirmationTokenAsync(newUser).Result;
                return new User(appUser, newEntity);
            }
            else
            {
                return null;
            }
  
        }

        public  User Create(EntityRegistrationViewModel vModel)
        {
            throw new NotImplementedException();
        }

        public void Delete(User entity)
        {
            throw new NotImplementedException();
        }

        public Task<IActionResult> DeleteAsync(User entity)
        {
            throw new NotImplementedException();
        }

        public User Find(int entityId)
        {
            throw new NotImplementedException();
        }

        public Task<User> FindAsync(int entityId)
        {
            throw new NotImplementedException();
        }

        public User FindByEmail(string email)
        {
            var user = _userManager.FindByEmailAsync(email).Result;
            var userEntity = _context.Entities.FirstOrDefault(e => e.Id == user.EntityId);
            return new User(user, userEntity);
        }

        public async Task<User> FindByEmailAsync(string email)
        {
            var user = await _userManager.FindByEmailAsync(email);
            if (user == null) return null;           
            var userEntity = await _context.Entities.FirstOrDefaultAsync(e => e.Id == user.EntityId);
            return new User(user, userEntity);    
        }

        public IEnumerable<User> FindByFullName(string fName, string lName)
        {
            throw new NotImplementedException();
        }

        public Task<User> FindByFullNameAsync(string name)
        {
            throw new NotImplementedException();
        }

        public User FindByNationalId(string nationlaId)
        {
            throw new NotImplementedException();
        }

        public Task<User> FindByNationalIdAsync(string nationlaId)
        {
            throw new NotImplementedException();
        }

        public User FindByPhoneNumber(string phoneNumber)
        {
            throw new NotImplementedException();
        }

        public Task<User> FindByPhoneNumberAsync(string phoneNumber)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<User> GetEntities(int countryId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<User> GetEntities()
        {
            throw new NotImplementedException();
        }

        public void Update(User entity)
        {
            throw new NotImplementedException();
        }

        public Task<IActionResult> UpdateAsync(User entity)
        {
            throw new NotImplementedException();
        }

        public Task<User> ExistsAsync(User entity)
        {
            throw new NotImplementedException();
        }

        public User Exists(User entity)
        {
            throw new NotImplementedException();
        }

        int IEntityRepository<User>.Create(User entity)
        {
            throw new NotImplementedException();
        }

        Entity IEntityRepository<User>.Create(EntityRegistrationViewModel vModel)
        {
            throw new NotImplementedException();
        }
    }
}
