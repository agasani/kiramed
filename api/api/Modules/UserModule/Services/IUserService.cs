﻿using api.Auth;
using api.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api.ViewModels;

namespace api.Services
{
    public interface IUserService
    {
        Task<List<User>> GetAsync();
        Task<User> FindByIdAsync(string id);
        Task<User> GetByEmailAsync(string email);
        Task<User> GetByPhoneNumberAsync(string phone);
        Task<IdentityResult> Create(User user, string password);
        Task<IdentityResult> UpdateDeletedAsync(User user);
        Task<IdentityResult> Delete(User user);
        Task<IdentityResult> Update(User user);
        Task<IdentityResult> Update(User user, string password);
        Task<IdentityResult> ValidatePassword(User user, string password);
        Task<IdentityResult> ValidateUser(User user);
        string HashPassword(User user, string password);
        Task SignOutAsync();
        Task<SignInResult> PasswordSignInAsync(User user, string password, bool lockoutOnFailure,
            bool isPersistent);
        Task<string> GetConfirmationToken(User user);
        Task<IdentityResult> AddUserToRole(User user, ApplicationRole role, int organizationId, DateTime startDate);
        Task<FeedBack> AssignInitialRolesAsync(User user, IList<string> roles, DateTime startDate, int organizationId, FeedBack feedBack);
        Task<IdentityResult> RemoveUserFromRole(User user, ApplicationRole role, int organizationId, DateTime endDate);
        Task<IdentityResult> LockUserAccount(User user);
        Task<IdentityResult> UnlockUserAccount(User user);
        Task<IEnumerable<User>> GetOrganizationUsersAsync(int organizationId, string roleName = null);
        Task<IdentityResult> UpdateAsync(User user);
        Task<Entity> GetUserEntityAsync(ApplicationUser user);
        Task<int> SaveAsync();
        Task<FeedBack> CreateAsync(UserRegisterViewModel model);
        Task<Country> GetCountryAsync(int id);
        IEnumerable<OrganizationViewModel> GetUserOrganizations(string userId);

    }
}
