﻿using api.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using api.Auth;
using Microsoft.AspNetCore.Identity;
using System;
using api.Auth.Roles;
using api.Mailers;
using api.ViewModels;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using api.Events;

namespace api.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _repository;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IRoleRepository _roleRepository;
        private readonly ICountryRepository _countryRepository;
    
        public UserService(IUserRepository repository      
            , SignInManager<ApplicationUser> signInManager
            , IRoleRepository roleRepository
            , ICountryRepository countryRepository
            , IEmailService userMailer
            , IUserValidator<ApplicationUser> userValidator = null
            , IPasswordValidator<ApplicationUser> passwordValidator = null
            , IPasswordHasher<ApplicationUser> passwordHasher = null
            )
        {
            _repository = repository;       
            _signInManager = signInManager;
            _roleRepository = roleRepository;
            _countryRepository = countryRepository;
        }

        public async Task<List<User>> GetAsync()
        {
            var returnedList = new List<User>();
            _repository.Get().ToList().ForEach(u =>
            {
                returnedList.Add(UserDetailAsync(u).Result);
            });

            return returnedList;
        }

        public async Task<User> GetByEmailAsync(string email)
        {
            var appUser = _repository.GetByEmail(email);
            return appUser == null ? null : await UserDetailAsync(appUser);
        }

        public async Task<FeedBack> CreateAsync(UserRegisterViewModel model)
        {
            var  feedback = new FeedBack();           
            IdentityResult result;
            var user = await GetByEmailAsync(model.Email);
            if (user != null && user.IsActive)
            {
                feedback.err = true;
                feedback.message = "User already exisits.";
                feedback.data = JsonConvert.SerializeObject(new Dictionary<string, string> { { "Email", user.Email } });
                return feedback;
            }

            if (user != null && !user.IsActive)
            {
               user.Update(model);
               result =  await UpdateDeletedAsync(user);
                if (result.Succeeded)
                {
                    feedback.err = false;
                    feedback.message += "Successfully registered the user!";
                    feedback.data = JsonConvert.SerializeObject(new Dictionary<string, string> { { "username", user.UserName } });

                    return await AssignInitialRolesAsync(user, model.Roles, model.StartDate, model.OrganizationId, feedback);
                }

                feedback.err = true;
                feedback.message = "User cannot be created";
                return feedback;
            }
            var newUser = new User(model);         
            var password = Utils.RandomPasswordGenerator.GeneratePassword(10);
            result = await _repository.Create(newUser, password);

            if (result.Succeeded)
            {           
                Console.WriteLine("The password for " + model.UserName + " is " + password);
                
                //Raise user created event
                EventBus.broadcast(GenericEvents.UserCreated.ToString(), JsonConvert.SerializeObject(new Dictionary<string, string>() 
                    { { "User", JsonConvert.SerializeObject(newUser) }, { "Password", password }}));  

                feedback.err = false;
                feedback.message += "Successfully registered the user!";
                feedback.data = JsonConvert.SerializeObject(new Dictionary<string, string> { { "id", newUser.Id.ToString() }, { "username", newUser.UserName } });
                return await AssignInitialRolesAsync(newUser, model.Roles, model.StartDate, model.OrganizationId, feedback);
            }

            feedback.err = true;
            feedback.message = "User cannot be created";
            return feedback;
        }

        public async  Task<Country> GetCountryAsync(int id)
        {
            return await _countryRepository.FindAsync(id);
        }

        public async Task<IdentityResult> UpdateDeletedAsync(User user)
        {
            //TODO Update entity
            var password = Utils.RandomPasswordGenerator.GeneratePassword(10);
            var result = await Update(user, password);
            if (result.Succeeded)
            {
               //Raise user reActivated event
                EventBus.broadcast(GenericEvents.UserReactivated.ToString(), JsonConvert.SerializeObject(new Dictionary<string, string>() 
                    { { "User", JsonConvert.SerializeObject(user) }, { "Password", password }}));  

            }
            return result;
        }

        public async Task<IdentityResult> Delete(User user)
        {
            return await _repository.Delete(user);
        }

        public async Task<IdentityResult> Update(User user)
        {
            return await _repository.Update(user);
        }

        public async Task<IdentityResult> ValidatePassword(User user, string password)
        {
            //var applicationUser = new ApplicationUser(user);
            //return await _passwordValidator.ValidateAsync(_repository.GetUserManager(), applicationUser, password);
            throw new NotImplementedException();
        }

        public async Task<IdentityResult> ValidateUser(User user)
        {
            //var applicationUser = new ApplicationUser(user);
            //return await _userValidator.ValidateAsync(_repository.GetUserManager(), applicationUser);
            throw new NotImplementedException();
        }

        public string HashPassword(User user, string password)
        {
            //var applicationUser =  _repository.GetByEmail(user.Email);
            //return _passwordHasher.HashPassword(applicationUser, password);
            throw new NotImplementedException();
        }

        public async Task SignOutAsync()
        {
            await _signInManager.SignOutAsync();
        }

        public async Task<SignInResult> PasswordSignInAsync(User user, string password, bool lockoutOnFailure, bool isPersistent)
        {
            var applicationUser =  new ApplicationUser(user);
            return await _signInManager.PasswordSignInAsync(applicationUser, password, isPersistent, lockoutOnFailure);
            //
        }

        public Task<string> GetConfirmationToken(User user)
        {
            return _repository.GetConfirmationToken(user);
        }

        public Task<IdentityResult> AddUserToRole(User user, ApplicationRole role, int organizationId, DateTime startDate)
        {
            return _repository.AddUserToRole(user, role, organizationId, startDate);
        }

        public async Task<FeedBack> AssignInitialRolesAsync(User user, IList<string> roles, DateTime startDate, int organizationId, FeedBack feedBack)
        {
            Console.WriteLine("In Assign initial Roles and roles are: " + roles.ToString());

            var roleSuccess = 0;
            foreach (var assigneedRole in roles)
            {
                Console.WriteLine("The role: " + assigneedRole);

                var role = await _roleRepository.FindByNameAsync(assigneedRole);
                if (role == null)
                {       
                    Console.WriteLine("Role not found: ");          
                    feedBack.message += "The role you are trying to assign does not exist";
                    continue;
                }
                var roleResult = await AddUserToRole(user, role, organizationId, startDate);
                if (roleResult.Succeeded)
                {
                    Console.WriteLine("Role added to the user successfully..");
                    roleSuccess++;
                    feedBack.message += "User asssigned role " + role.Name;
                }
                else
                {
                  Console.WriteLine("FAILED to add role to user..");
                  feedBack.message += "User couldn't be asssigned role " + role.Name;
                }
            }
            if (roleSuccess == 0) feedBack.err = true;
            return feedBack;
        }

        public async Task<User> GetByPhoneNumberAsync(string phone)
        {
            var appUser = _repository.GetByPhoneNumber(phone);
            return appUser == null ? null : await UserDetailAsync(appUser);
        }

        public Task<IdentityResult> Create(User user, string password)
        {
            throw new NotImplementedException();
        }

        public Task<IdentityResult> RemoveUserFromRole(User user, ApplicationRole role, int organizationId, DateTime EndDate)
        {
            return _repository.RemoveUserFromRole(user, role, organizationId, EndDate);
        }

        public Task<IdentityResult> LockUserAccount(User user)
        {
            return _repository.LockUserAccount(user);
        }

        public Task<IdentityResult> UnlockUserAccount(User user)
        {
            return _repository.UnlockUserAccount(user);
        }

        public Task<IdentityResult> Update(User user, string password)
        {
            return _repository.Update(user, password);
        }

        public async Task<IEnumerable<User>> GetOrganizationUsersAsync(int organizationId, string roleName = null)
        {
            var appUsers = await _repository.GetOrganizationUsersAsync(organizationId, roleName);
            return appUsers?.Select(user => GetByEmailAsync(user.Email).Result).ToList();
        }

        public async Task<IdentityResult> UpdateAsync(User user)
        {    
            return await _repository.UpdateAsync(user);
        }

        public async Task<Entity> GetUserEntityAsync(ApplicationUser user)
        {
            return await _repository.GetUserEntityAsync(user);
        }

        public async Task<int> SaveAsync()
        {
            return await _repository.SaveAsync();
        }

        public async Task<int> SaveAsync(ApplicationUser user)
        {
            return await _repository.SaveAsync();
        }

        public async Task<User> UserDetailAsync(ApplicationUser appUser)
        {
            var userEntity = await _repository.GetUserEntityAsync(appUser);
            var user = userEntity != null ? new User(appUser, userEntity) : new User(appUser);          
            var userOrgs = _repository.UserOrganizations(appUser.Id);
            if (userOrgs == null || !userOrgs.Any()) return user;
            user.Organizations = userOrgs.Select(o => o.Name).ToList();
            if (user.Roles.Count == 0) user.Roles = _repository.UserRoles(appUser).ToList();
            return user;
        }

        public async Task<User> FindByIdAsync(string id)
        {
           var appUser = await _repository.FindByIdAsync(id);
           return new User(appUser);
        }

        public IEnumerable<OrganizationViewModel> GetUserOrganizations(string userId)
        {
            var model = new List<OrganizationViewModel>();
            List<Organization> userOrgs = null;
        
            try {
                userOrgs = _repository.UserOrganizations(userId);
            } catch(Exception e) {
                Console.WriteLine("An exception happened trying to get user Orgs: " + JsonConvert.SerializeObject(e));
                handleException("GetUserOrganizations", e);
            }

            foreach (var org in userOrgs)
            {
                var vMod = new OrganizationViewModel(org);
                model.Add(vMod);
            }
            return model;
        }

        public void handleException(string action, Exception e)
        {
            var log = new LogEntity();
            log.Action = action;
            log.Controller = "UserService";
            log.Payload = JsonConvert.SerializeObject(e);
            log.Type = "Exception";
            EventBus.broadcast(LogEvents.ErrorLog.ToString(), JsonConvert.SerializeObject(log));
        }

    }
}
