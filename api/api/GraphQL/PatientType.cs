﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GraphQL.Types;

namespace api.Models
{
    public class PatientType : ObjectGraphType<Patient>
    {
        public PatientType()
        {
            Field(x => x.MedicalRecordNumber).Description("Patient Medical Record Number");
            Field(x => x.Entity.Firstname).Description("Patient first Name");
        }
    }
}
