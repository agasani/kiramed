﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api.Enums
{
    public enum eMeasureSeverity
    {
        Normal = 0,
        SevereHigh = 1,
        SevereLow = 2
    }
}
