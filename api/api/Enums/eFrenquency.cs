﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api.Models
{
    public enum eFrenquency
    {
        Once = 0,
        Daily = 1,
        Weekly = 2,
        Monthly = 3,
        Quarterly = 4,
        Annually = 5,
        BiWeekly = 6
    }
}
