﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace api.Enums
{
    public enum eSeverity
    {
        [Description("Severe")]
        Severe = 0,
        [Description("Normal")]
        Normal = 1,
        [Description("Severe Low")]
        SevereLow = 2,
        [Description("Severe High")]
        SevereHigh = 3,
        [Description("Undetermined")]
        Undetermined = 4
    }
}
