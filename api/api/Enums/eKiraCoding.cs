﻿using System.ComponentModel;

namespace api.Enums
{
    public enum eKiraCoding
    {
        [Description("ICD10")]
        ICD10 = 0,
        [Description("ICD9")]
        ICD9 = 1
    }
}
