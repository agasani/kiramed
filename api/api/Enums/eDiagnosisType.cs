﻿
using System.ComponentModel;

namespace api.Enums
{
    public enum eDiagnosisType
    {
        [Description("Admitting")]
        Admitting = 0,
        [Description("Discharing")]
        Discharging = 1,
        [Description("Principle")]
        Principle = 2
    }
}
