﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api.Models
{
    public enum eVisitStatus
    {
        Waiting = 1,
        Admitted = 2,
        Cancelled = 3,
        Discharged = 4
    }
}
