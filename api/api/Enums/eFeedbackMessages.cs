﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace api.Models
{
    public enum eFeedbackMessages
    {   
        [Description("The entity you are trying to create already exists")]
        ObjectExists = 1,
        [Description("The entity was created successfully")]
        ObjectCreatedSuccessfully = 2,
        [Description("The entity was deleted successfully")]
        ObjectDeletedSuccessfully = 3,
        [Description("The entity was updated successfully")]
        ObjectUpdatedSuccessfully = 4,
        [Description("The entity already exisits and is deleted")]
        ObjectIsDeleted = 5,
        [Description("The entity cannot be created at this time")]
        ObjectCantBeCreated = 6,
        [Description("The entity cannot be updated at this time")]
        ObjectCantBeUpdated = 7,
        [Description("The entity cannot be deleted at this time")]
        ObjectCantBeDeleted = 8,
        [Description("The entity you are trying to get does not exist")]
        ObjectDoestNotExist = 9,
        [Description("Invalide Model")]
        InvalidModel = 10,
        [Description("No records were found")]
        NoResultsWereFound = 11
    }
}
