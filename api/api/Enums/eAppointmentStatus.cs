﻿namespace api.Models
{
    public enum eAppointmentStatus
    {
       Pending = 1,
       Canceled = 2,
       Processing = 3
    }
}
