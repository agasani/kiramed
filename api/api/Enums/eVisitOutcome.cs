﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api.Enums
{
    public enum eVisitOutcome
    {
        Encounter = 0,
        Other = 1
    }
}
