﻿
namespace api.Models
{
    public enum eAgeUnits
    {
        Days = 1,
        Weeks = 2,
        Months = 3,
        Years = 4
    }
}
