﻿namespace api.Models
{
    public enum ePatientRelationShip
    {
        Self = 1,
        Child = 2,
        Spouse = 3,
        Parent = 4,
        GrandParent = 5,
        Guardian = 6,
        Prisoner = 7
    }
}
