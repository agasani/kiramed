﻿namespace api.Enums
{
    public enum eVisitType
    {
        Walkin = 0,
        Appointment = 1,
        Transfer = 2
    }
}
