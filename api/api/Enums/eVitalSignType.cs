﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api.Models
{
    public enum eVitalSignType
    {
        RespiratoryRate = 1,
        Height = 2,
        Weight = 3,
        BMI = 4,
        BMIPercentile = 5,
        BP = 6,
        Temperature = 7,
        Pulse = 8,
        O2Saturation = 9,
        Pain = 10          
    }
}
