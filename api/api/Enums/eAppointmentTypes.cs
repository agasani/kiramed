﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api.Models
{
    public enum eAppointmentTypes
    {
        NormalVisit = 1,
        VisitFollowUp = 2,
        VisitReschedule = 3,
        CarePlan = 4
    }
}
