﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace api.ViewModels
{
    public class AdminRegistrationViewModel
    {
       [Required]
       public  string AdminFirstName { get; set; }
       [Required]
       public  string AdminSecondName { get; set; }
       [Required]
       public string AdminNationalId { get; set; }  //External ID(NationalID for people and TIN Numbers for organization)
       [Required]
       [EmailAddress]
       public string AdminEmail { get; set; }
       [Required]
       [Phone]
       public string AdminPhoneNumber { get; set; }
    }
}
