﻿using api.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace api.ViewModels
{
    public class EntityAddressViewModel
    {
        public EntityAddressViewModel() { }
        public EntityAddressViewModel(EntityAddress address)
        {
            if (address == null)
                return;
            this.Id = address?.Id ?? 0;
            this.Label = address?.Label;
            this.Directions = address?.Directions;
            this.Address1 = address?.Address1;
            this.Address2 = address?.Address2;
            this.City = address?.City;
            this.State = address?.State;
            this.Location = address?.Location;
            this.TimeZone = address?.TimeZone ?? 0;
            this.TimeZoneId = address?.TimeZoneId ?? 0;
            this.HasDaylightSaving = address?.HasDaylightSaving ?? false;
            this.Zip = address?.Zip;
            this.District = address?.District;
            this.IsMailingAddress = address?.IsMailingAddress ?? false;
            this.BeginDate = address?.BeginDate ?? DateTime.Today;
            this.EndDate = address?.EndDate;
            this.Country = address?.Country;
            this.Deactivate = address?.EndDate == null ? false : true;
        }
        public int Id { get; set; }
        public string Label { get; set; }
        public string Directions { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Location { get; set; }
        public int TimeZone { get; set; }
        public int TimeZoneId { get; set; }
        public bool HasDaylightSaving { get; set; }
        public string Zip { get; set; }
        public string District { get; set; }
        public bool IsMailingAddress { get; set; }
        [Required]
        [StringLength(2)]
        public string Country { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool Deactivate { get; set; }

        public bool IsValid()
        {
            return !string.IsNullOrEmpty(this.Country);
        }
    }

}
