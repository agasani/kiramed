﻿using System;
using System.Collections.Generic;
using api.Models;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace api.ViewModels
{
    public class EntityViewModel
    {
        public EntityViewModel() { }
        public EntityViewModel(Entity entity)
        {
            if (entity == null)
                return;
            Id = entity?.Id ?? 0;
            Firstname = entity?.Firstname;
            Lastname = entity?.Lastname;
            Middlename = entity?.Middlename;
            IsActive = entity?.IsActive ?? true;
            EntryDate = (DateTime) entity?.EntryDate;
            Specialty = entity?.Specialty;
            Type = entity?.Type;
            Category = entity?.Category;
            ExternalId = entity?.ExternalId;

            Demographic = new EntityDemographicViewModel(entity.Demographic);

            foreach (var ctct in entity?.Contacts)
            {   
               Contacts.Add(new EntityContactViewModel(ctct));
            }

            foreach (var addr in entity?.Addresses)
            {              
               Addresses.Add(new EntityAddressViewModel(addr));
            }

        }
        public int Id { get; set; }
        //Exteranal ID eg. National ID number or Hospital ID number
        public string ExternalId { get; set; }
        [StringLength(2)]
        public string Type { get; set; }
        //Category eg. P for Patient
        public string Category { get; set; }
        public string Lastname { get; set; }
        public string Firstname { get; set; }
        public string Middlename { get; set; } = "";
        //Organization ID 
        public int OrganizationId { get; set; }
        //Account Status 1 for active and 0 for inactive
        public bool IsActive { get; set; } = true;
        public DateTime EntryDate { get; set; } = DateTime.Now;
        //Spacialty
        [StringLength(3)]
        public string Specialty { get; set; } = "";
        public EntityDemographicViewModel Demographic { get; set; } = new EntityDemographicViewModel();
        public List<EntityContactViewModel> Contacts { get; set; } = new List<EntityContactViewModel>();
        public List<EntityAddressViewModel> Addresses { get; set; } = new List<EntityAddressViewModel>();
    }

}
