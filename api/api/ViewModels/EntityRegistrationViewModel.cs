﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using api.Models;
using api.Db;

namespace api.ViewModels
{
    public class EntityRegistrationViewModel : IEntityRegistrationViewModel<Entity>
    {
        [Required]
        public int CountryId { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string SecondName { get; set; }
        [Required]
        public string NationalId { get; set; }  //External ID(NationalID for people and TIN Numbers for organization)
        public int ParentId { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        [Phone]
        public string PhoneNumber { get; set; }
        public string MiddleName { get; set; } = "";

        public EntityDemographicViewModel Demographic { get; set; } = new EntityDemographicViewModel();
        public List<EntityContactViewModel> Contacts { get; set; } = new List<EntityContactViewModel>();
        public List<EntityAddressViewModel> Addresses { get; set; } = new List<EntityAddressViewModel>();

        //Add one address

        public virtual Entity CheckIfEntityExists(KiraContext context)
        {
            var entity = context.Entities.FirstOrDefault(e => e.ExternalId == NationalId
                || e.Demographic.NationalID == NationalId 
                || (e.Firstname == FirstName && e.Lastname == SecondName)
                || e.Contacts.Any(c => (ContactTypes)c.ContactTypeId == ContactTypes.Email && c.Value == Email)
                || e.Contacts.Any(c => (ContactTypes)c.ContactTypeId == ContactTypes.Phone && c.Value == PhoneNumber));

            return entity;
        }
    }

}
