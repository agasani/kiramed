﻿using System;
using api.Models;
using System.ComponentModel.DataAnnotations;

namespace api.ViewModels
{
    public class EntityDemographicViewModel
    {
        public EntityDemographicViewModel() { }
        public EntityDemographicViewModel(EntityDemographic vModel)
        {
            Id = vModel.Id;
            if (vModel == null)
                return;
            this.DOB = (DateTime) vModel?.DOB;
            this.Education = vModel?.Education;
            this.Income = (decimal) vModel?.Income;
            this.MartalStatus = vModel?.MartalStatus;
            this.NationalID = vModel?.NationalID;
            this.PrimaryLanguage = vModel?.PrimaryLanguage;
            this.Religion = vModel?.Religion;
            this.SecondaryLanguage = vModel?.SecondaryLanguage;
            this.Sex = vModel?.Sex;
            EntityId = vModel.EntityId;
        }
        public int Id { get; set; }
        [StringLength(2)]
        public  int EntityId { get; set; }
        public string SecondaryLanguage { get; set; }
        [StringLength(2)]
        public string PrimaryLanguage { get; set; }
        //National Identifier aka ssn 
        public string NationalID { get; set; }
        public DateTime DOB { get; set; }
        [StringLength(2)]
        public string Sex { get; set; }
        //Martal Status
        [StringLength(2)]
        public string MartalStatus { get; set; }
        [StringLength(2)]
        public string Religion { get; set; }
        [StringLength(2)]
        public string Education { get; set; }
        //Income
        public decimal Income { get; set; }
    }

}
