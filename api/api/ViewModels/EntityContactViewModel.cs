﻿using api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api.ViewModels
{
    public class EntityContactViewModel
    {
        public EntityContactViewModel() { }
        public EntityContactViewModel(EntityContact contact)
        {
            if (contact == null)
                return;
            this.Id = contact?.Id ?? 0;
            this.Value = contact?.Value;
            this.Label = contact?.Label;
            this.ContactTypeId = contact?.ContactTypeId ?? 0;
            this.BeginDate = contact?.BeginDate ?? DateTime.Today;
            this.EndDate = contact?.EndDate;
            this.Deactivate = contact?.EndDate == null ? false : true;
           // this.ContactType = contact?.ContactType;
        }
        public int Id { get; set; }
        public short ContactTypeId { get; set; }
        public string Value { get; set; }
        public string Label { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool Deactivate { get; set; }
        //public ContactType ContactType { get; set; }
        public bool IsValid()
        {
            return !string.IsNullOrEmpty(this.Value) && !string.IsNullOrEmpty(this.Label)
                ? true : false;
        }
    }

}
