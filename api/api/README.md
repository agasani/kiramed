﻿###KIRAMED

#GuideLines

- A viewModel should not have any more information than what's required at creation of the based-on model;
	it could be a list of models though. This is to make viewModels lean and response time quick #latency.
	eg. PatViewModel should not contain Meds.

- All actions that alter a model or ViewModel should be member methods of that model/ViewModel so that there's
	one place alteration happen and controlled. Also this will help with lean and easy controller logic..

- As UI is build the documentation needs to be made for each screen added, what it does, how and any things that needs
	to be known by the user.

#Common Error

- System.Data.SqlClient.SqlException: Cannot open server "xxx" requested by the login.
	##Fix 
		Add the IP address to Database firewall to allow access to Azure services: Go to Azure portal and click on the Db
			in question and then Firewall and add the IP.

- How to fix Microsoft.Composition is not compartible with core1.1 ERROR
Edit .csproj file like this:
<PropertyGroup>    
  <TargetFramework>netcoreapp1.1</TargetFramework>
  <PackageTargetFallback>$(PackageTargetFallback);portable-win+net45+wp8+win81+wpa8</PackageTargetFallback>
</PropertyGroup>

- Access-Control-Allow-Origin: CORS is setup to allow all origins etc. If this error happens, it most likely a red herring, and it might have to do with data schemas changing and the 
	local branch not reflecting the latest changes.
		##Fix
			Get the latest from git and try again if that doesn't fix the issue look into the console to see what error is being thrown and fix it.
#Gotchas

- If you run async tests you will fail.. make sure all tests run syncronously..

- Core 1.1.0 doesn't have Lazy loading meaning eg. to get Patient.Entity one needs to explicitly load it:
	  eg. await this._context.Entry<Patient>(pat).Reference(p => p.Entity).LoadAsync();
	  eg. await this._context.Entry<Entity>(pat.Entity).Collection<EntityContact>(e => e.Contacts).LoadAsync();
	  Link: https://docs.microsoft.com/en-us/ef/core/querying/related-data
