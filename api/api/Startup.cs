﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Security.Claims;
using System.Threading.Tasks;
using api.Auth;
using api.Auth.Policies.Handlers;
using api.Auth.Roles;
using api.Db;
using api.Models;
using api.Repositories;
using api.Services;
using FluentEmail.Core;
using FluentEmail.Mailgun;
using IdentityServer4.EntityFramework.DbContexts;
using IdentityServer4.EntityFramework.Mappers;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Nest;
using NSwag.AspNetCore;
using api.Events;
using api.Modules.Shared.Entity.Repositories;
using api.References.Models;
using Swashbuckle.AspNetCore.Swagger;
using System.IO;
using Swashbuckle.AspNetCore.Examples;
using Swashbuckle.AspNetCore.SwaggerGen;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Authorization.Infrastructure;

namespace api
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var migrationAssembly = typeof(Startup).GetTypeInfo().Assembly.GetName().Name;

            //Register Db Context
            services.AddDbContext<KiraContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("AzureConnection")));

            services.AddDbContext<IDdbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("AzureConnection"), builder =>
                    builder.MigrationsAssembly(migrationAssembly)));

            // Add IdentityServer
            services.AddIdentityServer()
                .AddTemporarySigningCredential()
                .AddConfigurationStore(builder =>
                    builder.UseSqlServer(Configuration.GetConnectionString("AzureConnection"),
                    options => options.MigrationsAssembly(migrationAssembly)))
                .AddOperationalStore(builder =>
                    builder.UseSqlServer(Configuration.GetConnectionString("AzureConnection"), options =>
                    options.MigrationsAssembly(migrationAssembly)))
                .AddAspNetIdentity<ApplicationUser>();

            // Register Services for ioC
            RegisterServices(services);

            var cookieEventsOptions = new CookieAuthenticationEvents
            {
                OnRedirectToLogin = ctx =>
                {
                    if (ctx.Request.Path.StartsWithSegments("/api") &&
                        ctx.Response.StatusCode == (int)HttpStatusCode.OK)
                    {
                        ctx.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                    }
                    else if (ctx.Response.StatusCode != (int)HttpStatusCode.Unauthorized)
                    {
                        ctx.Response.Redirect(ctx.RedirectUri);
                    }
                    return Task.FromResult(0);
                }
            };

            services.AddIdentity<ApplicationUser, ApplicationRole>(options =>
                    options.Cookies.ApplicationCookie.Events = cookieEventsOptions)
               .AddEntityFrameworkStores<IDdbContext>()
               .AddDefaultTokenProviders();

            services.Configure<IdentityOptions>(options =>
            {
                options.Password.RequiredLength = 6;
                options.Password.RequireDigit = true;
                options.Password.RequireUppercase = false;
                options.Password.RequireLowercase = false;
                options.Password.RequireNonAlphanumeric = false;

                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromHours(1);
                options.Lockout.MaxFailedAccessAttempts = 5;

                options.User.RequireUniqueEmail = true;

            });

            services.AddCors(options =>
            {
                options.AddPolicy("default", builder =>
                    {
                        builder
                           .AllowAnyOrigin()
                           .AllowAnyMethod()
                           .AllowAnyHeader()
                           .AllowCredentials();
                    });
            });

            // Add framework services.
            services.AddMvc().AddJsonOptions(options =>
                {
                    options.SerializerSettings.Formatting = Newtonsoft.Json.Formatting.Indented;
                });

            var sender = new MailgunSender(
                  "sandboxc2b97cb7742d41788f514e14d5d1a7c0.mailgun.org", // Mailgun Domain
                  "key-8fc16b5848400d172ce862b3b3a5afe4" // Mailgun API Key
              );
            Email.DefaultSender = sender;

            // Setup Auth Policies
            SetupAuthPolicies(services);

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1",
                    new Info
                    {
                        Title = "KiraMed",
                        Version = "v1",
                        Description = "The API Documentation for KiraMed",
                        TermsOfService = "Leapfrogging Health care and Wellness in Africa.",
                        Contact = new Contact
                        {
                            Name = "KiraMed Group",
                            Email = "developer@kiramed.co"
                        },
                        License = new Swashbuckle.AspNetCore.Swagger.License
                        {
                            Name = "Apache 2.0",
                            Url = "http://www.apache.org/licenses/LICENSE-2.0.html"
                        }
                    }
                );


                //var basePath = PlatformServices.Default.Application.ApplicationBasePath;

                //Determine base path for the application.
                var basePath = AppContext.BaseDirectory;
                var assemblyName = Assembly.GetEntryAssembly().GetName().Name;
                var fileName = Path.GetFileName(assemblyName + ".xml");

                c.IncludeXmlComments(Path.Combine(basePath, fileName));
                c.OperationFilter<ComsumesOperationFilter>();

                c.OperationFilter<ExamplesOperationFilter>(); // [SwaggerRequestExample] & [SwaggerResponseExample]
                c.OperationFilter<DescriptionOperationFilter>(); // [Description] on Response properties
                c.OperationFilter<SecurityRequirementsOperationFilter>(); // Adds an Authorization input box to every endpoint
                c.OperationFilter<AddFileParamTypesOperationFilter>(); // Adds an Upload button to endpoints which have [AddSwaggerFileUploadButton]
                c.OperationFilter<AddHeaderOperationFilter>("correlationId", "Correlation Id for the request"); // adds any string you like to the request headers - in this case, a correlation id
                c.OperationFilter<AddResponseHeadersFilter>(); // [SwaggerResponseHeader]
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory,
            KiraContext kiraContext, IDdbContext idContext)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();


            //app.UseSwaggerUi(typeof(Startup).GetTypeInfo().Assembly, new SwaggerUiSettings());

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "KiraMed V1");
            });

            app.UseCors("default");

            app.UseMvc();

            InitIDServDbResources(app, idContext);

            app.UseIdentity();

            app.UseIdentityServer();

            app.UseDeveloperExceptionPage();

            app.UseIdentityServerAuthentication(new IdentityServerAuthenticationOptions
            {
                Authority = "http://localhost:5000",
                RequireHttpsMetadata = false,
                ApiName = "KirApi",
                AllowedScopes = new List<string> { "KirApi", "offline_access" }
            });

            //app.UseIdentityServer();

            //app.UseMvc();//routes =>
            //{
            //    routes.MapRoute("default", "{controller=Home}/{action=Index}/{id}");
            //});

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                DbInitializer.Init(kiraContext);
                var userManager = app.ApplicationServices.GetRequiredService<UserManager<ApplicationUser>>();
                app.ApplicationServices.GetRequiredService<SignInManager<ApplicationUser>>();
                var roleManager = app.ApplicationServices.GetRequiredService<RoleManager<ApplicationRole>>();
                AuthInitializer.Init(idContext, userManager, roleManager, kiraContext, true);
            }

            ConfigureEvents(app);
        }

        private void InitIDServDbResources(IApplicationBuilder app, IDdbContext idContext)
        {
            using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                serviceScope.ServiceProvider.GetRequiredService<PersistedGrantDbContext>().Database.Migrate();

                var context = serviceScope.ServiceProvider.GetRequiredService<ConfigurationDbContext>();

                context.Database.Migrate();

                if (!context.Clients.Any())
                {
                    // get all claims in the db and add them to the client
                    var claims = idContext.RoleClaims.Select(x => x.ToClaim()).ToList();
                    Console.WriteLine("Number of claims in the DB: " + claims.Count());

                    foreach (var c in claims)
                    {
                        Console.WriteLine("Claim: " + c.Value + " type: " + c.Type);
                    }

                    foreach (var client in Config.getClients(claims))
                    {
                        Console.WriteLine("Adding client: " + client.ClientName);
                        context.Clients.Add(client.ToEntity());
                    }
                    context.SaveChanges();
                }

                if (!context.IdentityResources.Any())
                {
                    foreach (var resource in Config.GetIdentityResources())
                    {
                        context.IdentityResources.Add(resource.ToEntity());
                    }
                    context.SaveChanges();
                }

                if (!context.ApiResources.Any())
                {
                    foreach (var resource in Config.GetApiResources())
                    {
                        context.ApiResources.Add(resource.ToEntity());
                    }
                    context.SaveChanges();
                }
            }
        }

        private void SetupAuthPolicies(IServiceCollection services)
        {
            #region  Role Claim Policies
            services.AddAuthorization(options =>
            {
                options.AddPolicy("view.patient.level1",
                    policy => policy.RequireClaim(ClaimTypes.AuthorizationDecision, new string[] { "view.patient.l1" }));

                options.AddPolicy("patient.level1",
                    policy => policy.RequireClaim(ClaimTypes.AuthorizationDecision, new string[] { "view.patient.l1", "edit.patient.l1" }));

                options.AddPolicy("view.patient.level2",
                    policy => policy.RequireClaim(ClaimTypes.AuthorizationDecision, new string[] { "view.patient.l2" }));

                options.AddPolicy("patient.level2",
                    policy => policy.RequireClaim(ClaimTypes.AuthorizationDecision, new string[] { "view.patient.l2", "edit.patient.l2" }));

                options.AddPolicy("view.patient.level3",
                    policy => policy.RequireClaim(ClaimTypes.AuthorizationDecision, new string[] { "view.patient.l3" }));

                options.AddPolicy("patient.level3",
                    policy => policy.RequireClaim(ClaimTypes.AuthorizationDecision, new string[] { "view.patient.l3", "edit.patient.l3" }));

                options.AddPolicy("view.patient.level4",
                    policy => policy.RequireClaim(ClaimTypes.AuthorizationDecision, new string[] { "view.patient.l4" }));

                options.AddPolicy("patient.level4",
                    policy => policy.RequireClaim(ClaimTypes.AuthorizationDecision, new string[] { "view.patient.l4", "edit.patient.l4" }));

                options.AddPolicy("view.patient.level5",
                    policy => policy.RequireClaim(ClaimTypes.AuthorizationDecision, new string[] { "view.patient.l5" }));

                options.AddPolicy("patient.level5",
                    policy => policy.RequireClaim(ClaimTypes.AuthorizationDecision, new string[] { "view.patient.l5", "edit.patient.l5" }));

            });
            #endregion
        }

        private void RegisterServices(IServiceCollection services)
        {
            //entity repositories
            services.AddScoped<IElasticClient, ElasticClient>();
            services.AddScoped<IEmailService, EmailService>();
            services.AddScoped<IPatientRepository, PatientRepository>();
            services.AddScoped<ISystemActionRepository, SystemActionRepository>();
            services.AddScoped<IRoleRepository, RoleRepository>();
            services.AddScoped<IEntityRepository, EntityRepository>();
            services.AddScoped<IDemographicRepository, DemographicRepository>();
            services.AddScoped<IEntityAddressRepository, EntityAddressRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IVisitRepository, VisitRepository>();
            services.AddScoped<IVisitQueueRepository, VisitQueueRepository>();
            services.AddScoped<IAppointmentRepository, AppointmentRepository>();
            services.AddScoped<IOrganizationRepository, OrganizationRepository>();
            services.AddScoped<ICountryRepository, CountryRepository>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IPatientService, PatientService>();
            services.AddScoped<IAppointmentService, AppointmentService>();
            services.AddScoped<IVisitService, VisitService>();
            services.AddScoped<IOrganizationService, OrganizationService>();
            services.AddScoped<ITransferRepository, TransferRepository>();
            services.AddSingleton<IAuthorizationHandler, IsPatientHandler>();
            services.AddSingleton<IEncounterRepository, EncounterRepository>();
            services.AddSingleton<IEncounterService, EncounterService>();
            services.AddSingleton<IVitalSignMeasureRepository, VitalSignMeasureRepository>();
            services.AddSingleton<IVitalSignTypeRepository, VitalSignTypeRepository>();
            services.AddSingleton<IVitalSignRepository, VitalSignRepository>();
            services.AddSingleton<IVitalSignService, VitalSignService>();
            services.AddSingleton<IVisitQueueService, VisitQueueService>();
            services.AddSingleton<IPatientDiagnosisRepository, PatientDiagnosisRepository>();
            services.AddSingleton<IPatientDiagnosisService, PatientDiagnosisService>();
            services.AddSingleton<IDiagnosisRepository, DiagnosisRepository>();
            services.AddSingleton<IDiagnosisService, DiagnosisService>();

        }

        private void ConfigureEvents(IApplicationBuilder app)
        {
            //TODO loop through event handler directory and register all the events.
            var assembly = typeof(Startup).GetTypeInfo().Assembly.GetName().Name;
            var eventHandlersDir = assembly + "/Events/Handlers";

            Console.WriteLine("EventHandler directory is: " + eventHandlersDir);

            var types = Assembly.GetEntryAssembly().GetTypes().Where(x => x.Namespace == "api.Events" && x.GetMethods().Any(t => t.Name == "Execute" && x.Name != "IEventHandler"));

            Console.WriteLine("We got " + types.Count() + " types.");

            foreach (var type in types)
            {
                Console.WriteLine("Type is: " + type.Name);

                ConstructorInfo constructor = type.GetConstructors()[0];
                if (constructor != null)
                {
                    object[] args = constructor
                        .GetParameters()
                        .Select(o => o.ParameterType)
                        .Select(o => app.ApplicationServices.GetService(o))
                        .ToArray();

                    var handler = Activator.CreateInstance(type, args) as IEventHandler;
                    foreach (var evt in handler?.GetEvents())
                    {
                        EventBus.subscribe(evt, handler);
                    }
                }
            }
        }

    }

    public class SecurityRequirementsOperationFilter : IOperationFilter
    {
        private readonly IOptions<AuthorizationOptions> authorizationOptions;

        public SecurityRequirementsOperationFilter(IOptions<AuthorizationOptions> authorizationOptions)
        {
            this.authorizationOptions = authorizationOptions;
        }

        public void Apply(Operation operation, OperationFilterContext context)
        {
            var controllerPolicies = context.ApiDescription.ControllerAttributes()
                .OfType<AuthorizeAttribute>()
                .Select(attr => attr.Policy);
            var actionPolicies = context.ApiDescription.ActionAttributes()
                .OfType<AuthorizeAttribute>()
                .Select(attr => attr.Policy);
            var policies = controllerPolicies.Union(actionPolicies).Distinct();
            var requiredClaimTypes = policies
                .Select(x => this.authorizationOptions.Value.GetPolicy(x))
                .SelectMany(x => x.Requirements)
                .OfType<ClaimsAuthorizationRequirement>()
                .Select(x => x.ClaimType);

            if (requiredClaimTypes.Any())
            {
                operation.Responses.Add("401", new Response { Description = "Unauthorized" });
                operation.Responses.Add("403", new Response { Description = "Forbidden" });

                operation.Security = new List<IDictionary<string, IEnumerable<string>>>();
                operation.Security.Add(
                    new Dictionary<string, IEnumerable<string>>
                    {
                    { "oauth2", requiredClaimTypes }
                    });
            }
        }
    }
}
