﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api.Mailers;
using api.Models;
using FluentEmail.Core;
using FluentEmail.Core.Models;

namespace api.Services
{
    public  class EmailService : IEmailService
    {
        private const string CALLBACKURL = "kiramed.com";
        private const string INVITATIONFROMEMAIL = "jmasengesho@inyarwanda.com";
        private const string INVITATIONSUBJECT = "Kiramed Invitation";

        public async Task<SendResponse> SendMailAsync(string emailFrom, string emailTo, string subject, string body)
        {
            var email = Email
                .From(emailFrom)
                .To(emailTo)
                .Subject(subject)
                .Body(body);
            return await email.SendAsync();
        }

        public async Task<SendResponse> UserInvitationEmailAsync(string email, string password)
        {
            var body = "Please confirm your account by clicking this link: <a href=\"" + CALLBACKURL + "\">link</a> ";
            body += "Your password is " + password;
            var subject = INVITATIONSUBJECT;
            var emailFrom = INVITATIONFROMEMAIL;
            return await SendMailAsync(emailFrom, email, subject, body);
        }


    }
}
