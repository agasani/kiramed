﻿using System.Threading.Tasks;
using api.Models;
using FluentEmail.Core.Models;

namespace api.Services
{
    public interface IEmailService
    {
        Task<SendResponse> SendMailAsync(string emailFrom, string emailTo, string subject, string body);
        Task<SendResponse> UserInvitationEmailAsync(string email, string password);
    }
}
