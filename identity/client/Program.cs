﻿using System;
using IdentityModel.Client;
using Newtonsoft.Json.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Text;
using System.Net.Http.Headers;
using IdentityModel;

namespace client
{
    class Program
    {
        public static void Main(string[] args) => MainAsync().GetAwaiter().GetResult();

        static async Task MainAsync()
        {
            Console.WriteLine("Client to get token from IDServer and access API!");

            await RequestAsync();

            var disco = await DiscoveryClient.GetAsync("http://localhost:5000");
            var tokenClient = new TokenClient(disco.TokenEndpoint, "KiraWeb", "564ht32io8#479ierye@578gk5te6gu9");
            var tokenResponse = await tokenClient.RequestResourceOwnerPasswordAsync("testr2", "PNx9n8", "offline_access KirApi");

            if (tokenResponse.IsError)
            {
                Console.WriteLine(tokenResponse.Error);
                return;
            }

            Console.WriteLine(tokenResponse.Json);
            Console.WriteLine("\n\n\n");

            var client = new HttpClient();

            client.SetBearerToken(tokenResponse.AccessToken);

            var resp = await client.GetAsync("http://localhost:5000/api/kira");

            if (!resp.IsSuccessStatusCode)
            {
                Console.WriteLine($"Request failed with statusCode: { resp.StatusCode }");
            }
            else
            {
                var content = await resp.Content.ReadAsStringAsync();
                Console.WriteLine(JArray.Parse(content));
            }
        }

        static async Task RequestAsync()
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var request = new HttpRequestMessage(HttpMethod.Post, "http://localhost:5000/connect/token");
            var form = new Dictionary<string, string> { { "username", "testr2" }, { "password", "PNx9n8"}, {"grant_type", "password"}
                            , { "scope", "KirApi" } };
            
            request.Content = new FormUrlEncodedContent(form);

            //Encode auth
            var encoding = Encoding.UTF8;
            var credential = "KiraWeb:564ht32io8#479ierye@578gk5te6gu9";
            var encoded = Convert.ToBase64String(encoding.GetBytes(credential));
            
            request.Headers.Add("Authorization", "Basic " + encoded);

            try
            {
                var response = await client.SendAsync(request);
                Console.WriteLine("Response in RequestAsync: " + response);
            }
            catch (Exception e)
            {

            }
        }
    }
}