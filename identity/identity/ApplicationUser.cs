﻿using System;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace identity
{
    public class ApplicationUser : IdentityUser
    {
        //put custom properties here..
        public int EntityId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? IsDelete { get; set; }
        public DateTime? IsLocked { get; set; }
        public DateTime ModifiedDate { get; set; }

    }
}
