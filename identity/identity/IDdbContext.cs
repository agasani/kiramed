﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace identity
{
    public class IDdbContext : IdentityDbContext<ApplicationUser, IdentityRole, string>
    {
        public IDdbContext(DbContextOptions<IDdbContext> options) : base(options)
        {

        }
    }
}
