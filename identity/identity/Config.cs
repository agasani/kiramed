﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;
using IdentityServer4;
using IdentityServer4.Models;
using IdentityServer4.Test;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;

namespace identity
{
    public class Config
    {
        public static IEnumerable<ApiResource> GetApiResources()
        {
            return new List<ApiResource> { new ApiResource("KirApi", "KiraMed Api") };
        }

        public static IEnumerable<Client> getClients(IConfiguration config)
        {
            var claimsSettings = config.GetSection("Claims");

            var claims = new List<Claim>();

            foreach(var c in claimsSettings.GetChildren())
            {
                claims.Add(new Claim(c.Key, c.Value));
            }
            return new List<Client>
            {
                new Client
                {
                    ClientId = "KiraWeb",
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPassword,
                    ClientSecrets =
                    {
                        new Secret("564ht32io8#479ierye@578gk5te6gu9".Sha256())
                    },
                    AllowedScopes = { "KirApi",
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        IdentityServerConstants.StandardScopes.OfflineAccess
                    }
                    , AllowOfflineAccess = true
                    , AccessTokenLifetime = 2700
                    , Claims = claims
                    , UpdateAccessTokenClaimsOnRefresh = true
                    , AlwaysSendClientClaims = true
                    , AllowedCorsOrigins = new List<string>{ "http://localhost:8080", "http://localhost:4200", @"chrome-extension://fhbjgbiflinjbdggehcddcbncdddomop" }
                }
            };
        }

        public static IEnumerable<IdentityResource> GetIdentityResources()
        {
            return new List<IdentityResource>
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile()
            };
        }

        public static List<TestUser> GetUsers()
        {
            return new List<TestUser>
            {
                new TestUser
                {
                    SubjectId = "1",
                    Username = "kira",
                    Password = "akabanga",
                    Claims = new List<Claim>
                    {
                        new Claim("patient", "own"),
                        new Claim("patientinfo", "all")
                    }
                },
                new TestUser
                {
                    SubjectId = "2",
                    Username = "tester",
                    Password = "letmein",
                    Claims = new List<Claim>
                    {
                        new Claim("patient", "own"),
                        new Claim("patientinfo", "all")
                    }
                },
                new TestUser
                {
                    SubjectId = "3",
                    Username = "bobiller",
                    Password = "thebiller",
                    Claims = new List<Claim>
                    {
                        new Claim("billing", "123"),
                        new Claim("budget", "all")
                    }
                }
            };
        }

    }
}
